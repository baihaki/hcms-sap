package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.entity.T513C;
import com.abminvestama.hcms.core.service.api.business.query.T513CQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@RestController
@RequestMapping("/api/v1/master_job")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class T513CController extends AbstractResource {

	private T513CQueryService t513cQueryService;
	
	@Autowired
	void setT513CQueryService(T513CQueryService t513cQueryService) {
		this.t513cQueryService = t513cQueryService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/page/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<T513C>> fetchPage(@PathVariable int pageNumber, 
			@RequestParam(value = "keywords", required = false) String keywords) throws Exception {
		
		Page<T513C> resultPage = null;
		
		if (StringUtils.isNotEmpty(keywords)) {
			//String formatKeywords = keywords.trim().replaceAll(" ", "&");
		    //resultPage = t513cQueryService.fetchByKeywordWithPaging(pageNumber, formatKeywords);
		} else {
		    resultPage = t513cQueryService.fetchAllWithPaging(pageNumber);
		}
		
		APIResponseWrapper<Page<T513C>> response = new APIResponseWrapper<>();
		response.setData(resultPage);
		Link self = linkTo(methodOn(T513CController.class).fetchPage(pageNumber, keywords)).withSelfRel();
		response.add(self);
		
		return response;
	}				
}