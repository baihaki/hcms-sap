package com.abminvestama.hcms.rest.api.dto.response;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.CaReqHdr;
import com.abminvestama.hcms.core.model.entity.CaReqDtl;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class CaTransactionResponseWrapper  extends ResourceSupport {

	private CaReqHdrResponseWrapper trxHdr;
	private ArrayData<CaReqDtlResponseWrapper> trxItm;
	
	private CaTransactionResponseWrapper() {}
	
	
	/**
	 * @param caReqHdr
	 * @param caReqDtl
	 */
	public CaTransactionResponseWrapper(CaReqHdr caReqHdr, CaReqDtl caReqDtl[]) {
		if (caReqDtl == null || caReqHdr == null) {
			new CaTransactionResponseWrapper();
		} else {
			
			ArrayData<CaReqDtlResponseWrapper> bunchOfCaReqDtl = new ArrayData<>();
			APIResponseWrapper<ArrayData<CaReqDtlResponseWrapper>> response = new APIResponseWrapper<>();
			response.setData(bunchOfCaReqDtl);
			
			List<CaReqDtlResponseWrapper> records = new ArrayList<>();
			
			for(int i = 0; i < caReqDtl.length; i++){
				records.add(new CaReqDtlResponseWrapper(caReqDtl[i]));
			}
			bunchOfCaReqDtl.setItems(records.toArray(new CaReqDtlResponseWrapper[records.size()]));			
			
			trxHdr = new CaReqHdrResponseWrapper(caReqHdr);
			
			this.setTrxHdr(trxHdr)
				.setTrxItm(bunchOfCaReqDtl);
		}
	}	
	
	@JsonProperty("trx_header")
	public CaReqHdrResponseWrapper getTrxHdr() {
		return trxHdr;
	}
	
	private CaTransactionResponseWrapper setTrxHdr(CaReqHdrResponseWrapper trxHdr) {
		this.trxHdr = trxHdr;
		return this;
	}
	
	
	@JsonProperty("trx_item")
	public ArrayData<CaReqDtlResponseWrapper> getTrxItm() {
		return trxItm;
	}
	
	private CaTransactionResponseWrapper setTrxItm(ArrayData<CaReqDtlResponseWrapper> bunchOfTrxItm) {
		this.trxItm = bunchOfTrxItm;
		return this;
	}
	

}

