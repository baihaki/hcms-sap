package com.abminvestama.hcms.rest.api.exception.handler;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.exception.EntityNotFoundException;
import com.abminvestama.hcms.core.exception.WorkflowNotFoundException;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionViewResolver;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@ControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger LOG = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);
	
	@ExceptionHandler(UsernameNotFoundException.class)
	@ResponseStatus(HttpStatus.NON_AUTHORITATIVE_INFORMATION)
	public ModelAndView usernameNotFoundExceptionHandler(HttpServletRequest req, UsernameNotFoundException e) {
		LOG.error("Username not found", e);
		ExceptionResponseWrapper response = new ExceptionResponseWrapper("Username not found: " + e.getMessage(),
				HttpStatus.NON_AUTHORITATIVE_INFORMATION, HttpMethod.valueOf(req.getMethod()));
		return generateViewResolver(req, new ExceptionViewResolver(response));
	}
	
	@ExceptionHandler(AuthenticationException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public ModelAndView authenticationExceptionHandler(HttpServletRequest req, AuthenticationException e) {
		LOG.error("Authentication failed", e);
		ExceptionResponseWrapper response = new ExceptionResponseWrapper("Authentication failed. Either missing or invalid token.",
				HttpStatus.UNAUTHORIZED, HttpMethod.valueOf(req.getMethod()));
		return generateViewResolver(req, new ExceptionViewResolver(response));
	}
	
	@ExceptionHandler(AuthorizationException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	public ModelAndView authorizationExceptionHandler(HttpServletRequest req, AuthorizationException e) {
		LOG.error("Authorization failed", e);
		ExceptionResponseWrapper response = new ExceptionResponseWrapper("Authorization failed." +
				(StringUtils.isEmpty(e.getMessage()) ? "" : ("Caused By: " + e.getMessage())),
				HttpStatus.UNAUTHORIZED, HttpMethod.valueOf(req.getMethod()));
		return generateViewResolver(req, new ExceptionViewResolver(response));
	}
	
	@ExceptionHandler(CannotPersistException.class)
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	public ModelAndView cannotPersistExceptionHandler(HttpServletRequest req, CannotPersistException e) {
		LOG.error("Cannot persist data", e);
		ExceptionResponseWrapper response = new ExceptionResponseWrapper("Cannot persist data. Either data corruption or internal error!",
				HttpStatus.UNPROCESSABLE_ENTITY, HttpMethod.valueOf(req.getMethod()));
		return generateViewResolver(req, new ExceptionViewResolver(response));
	}

	@ExceptionHandler(DataViolationException.class)
	@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
	public ModelAndView dataViolationExceptionHandler(HttpServletRequest req, DataViolationException e) {
		LOG.error("Data Violation.", e);
		ExceptionResponseWrapper response = new ExceptionResponseWrapper("Data Violation Exception." +
				(StringUtils.isEmpty(e.getMessage()) ? "" : ("Caused By: " + e.getMessage())),
				HttpStatus.PRECONDITION_FAILED, HttpMethod.valueOf(req.getMethod()));
		return generateViewResolver(req, new ExceptionViewResolver(response));
	}
	
	@ExceptionHandler(EntityNotFoundException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ModelAndView entityNotFoundExceptionHandler(HttpServletRequest req, EntityNotFoundException e) {
		LOG.error("Cannot find Event Log Entity.", e);
		ExceptionResponseWrapper response = new ExceptionResponseWrapper("Cannot find Event Log Entity." + 
				(StringUtils.isEmpty(e.getMessage()) ? "" : ("Caused By: " + e.getMessage())),
				HttpStatus.INTERNAL_SERVER_ERROR, HttpMethod.valueOf(req.getMethod()));
		return generateViewResolver(req, new ExceptionViewResolver(response));
	}
	
	@ExceptionHandler(WorkflowNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ModelAndView workflowNotFoundExceptionHandler(HttpServletRequest req, WorkflowNotFoundException e) {
		LOG.error("WorkflowType not found!" + (StringUtils.isEmpty(e.getMessage())? "" : " Caused by: " + e.getMessage()), e);
		ExceptionResponseWrapper response = new ExceptionResponseWrapper("Cannot find workflow." + 
				(StringUtils.isEmpty(e.getMessage()) ? "" : " Caused by: " + e.getMessage()),
				HttpStatus.NO_CONTENT, HttpMethod.valueOf(req.getMethod()), StringUtils.isEmpty(e.getField()) ? "" : e.getField());
		return generateViewResolver(req, new ExceptionViewResolver(response));
	}
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) {
		LOG.error("Internal Server Error", e);
		ExceptionResponseWrapper response = new ExceptionResponseWrapper("Internal Server Error: " + e.getMessage(),
				HttpStatus.INTERNAL_SERVER_ERROR, HttpMethod.valueOf(req.getMethod()));
		return generateViewResolver(req, new ExceptionViewResolver(response));
	}
	
	private ModelAndView generateViewResolver(HttpServletRequest req, ExceptionViewResolver exceptionViewResolver) {
		ModelAndView mv = new ModelAndView();
		mv.addObject("url", req.getRequestURL());
		mv.addObject("method", exceptionViewResolver.getExceptionResponse().getHttpMethod().name());
		mv.addObject("status", exceptionViewResolver.getExceptionResponse().getHttpStatus().value());
		mv.addObject("message", exceptionViewResolver.getExceptionResponse().getMessage());
		mv.addObject("error", exceptionViewResolver.getExceptionResponse().getHttpStatus().name());
		mv.addObject("field", exceptionViewResolver.getExceptionResponse().getField());
		mv.addObject("timestamp", new Date());
		mv.setView(exceptionViewResolver);
		return mv;
	}
}