package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.constant.ApprovalType;
import com.abminvestama.hcms.core.model.constant.SequenceType;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.Position;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.Workflow;
import com.abminvestama.hcms.core.model.entity.WorkflowStep;
import com.abminvestama.hcms.core.service.api.business.command.WorkflowStepCommandService;
import com.abminvestama.hcms.core.service.api.business.query.PositionQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.api.business.query.WorkflowQueryService;
import com.abminvestama.hcms.core.service.api.business.query.WorkflowStepQueryService;
import com.abminvestama.hcms.rest.api.dto.request.WorkflowStepRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.AuthorResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.WorkflowStepResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@RestController
@RequestMapping("/api/v2/workflow_steps")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class WorkflowStepController extends AbstractResource {

	private WorkflowStepCommandService workflowStepCommandService;
	private WorkflowStepQueryService workflowStepQueryService;
	
	private WorkflowQueryService workflowQueryService;
	private PositionQueryService positionQueryService;
	
	private UserQueryService userQueryService;
	
	private Validator workflowStepValidator;
	
	@Autowired
	void setWorkflowStepCommandService(WorkflowStepCommandService workflowStepCommandService) {
		this.workflowStepCommandService = workflowStepCommandService;
	}
	
	@Autowired
	void setWorkflowStepQueryService(WorkflowStepQueryService workflowStepQueryService) {
		this.workflowStepQueryService = workflowStepQueryService;
	}
	
	@Autowired
	void setWorkflowQueryService(WorkflowQueryService workflowQueryService) {
		this.workflowQueryService = workflowQueryService;
	}
	
	@Autowired
	void setPositionQueryService(PositionQueryService positionQueryService) {
		this.positionQueryService = positionQueryService;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("workflowStepValidator")
	void setWorkflowStepValidator(Validator workflowStepValidator) {
		this.workflowStepValidator = workflowStepValidator;
	}
	
	@Secured({AccessRole.USER_GROUP, AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<WorkflowStepResponseWrapper> fetchById(@PathVariable String id) throws Exception {
		APIResponseWrapper<WorkflowStepResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			Optional<WorkflowStep> workflowStep = workflowStepQueryService.findById(Optional.ofNullable(id));
			
			if (!workflowStep.isPresent()) {
				response.setData(null);
				response.setMessage("WorkflowType Step ID '" + id + "' does not exist!");
			} else {
				response.setData(new WorkflowStepResponseWrapper(workflowStep.get()));
			}
		} catch (Exception e) {
			throw e;
		}
		
		Link self = linkTo(methodOn(WorkflowStepController.class).fetchById(id)).withSelfRel();
		response.add(self);				
		
		return response;
	}
	
	@Secured({AccessRole.USER_GROUP, AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/page/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<List<WorkflowStepResponseWrapper>> fetchPage(@PathVariable int pageNumber) throws Exception {
		Page<WorkflowStep> resultPage = workflowStepQueryService.fetchAllWithPaging(pageNumber);
		
		List<WorkflowStepResponseWrapper> records = new ArrayList<>();
		
		APIResponseWrapper<List<WorkflowStepResponseWrapper>> response = new APIResponseWrapper<>();
		
		try {
			resultPage.forEach(workflowStep -> {
				try {
					records.add(new WorkflowStepResponseWrapper(workflowStep).setCreatedBy(
							linkTo(methodOn(UserController.class).getUserProfileById(workflowStep.getCreatedById()))
							.withRel(AuthorResponseWrapper.RESOURCE))
						.setUpdatedBy(
							linkTo(methodOn(UserController.class).getUserProfileById(workflowStep.getUpdatedById()))
							.withRel(AuthorResponseWrapper.RESOURCE))
					);
				} catch (Exception ignored) {}
			});
		} catch (Exception e) {
			throw e;
		}
		
		response.setData(records);
		Link self = linkTo(methodOn(WorkflowStepController.class).fetchPage(pageNumber)).withSelfRel();
		response.add(self);		
		
		return response;		
	}
	
	@Secured({AccessRole.USER_GROUP, AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/workflow/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<WorkflowStepResponseWrapper>> fetchByWorkflowId(@PathVariable("id") String workflowId) throws Exception {
		ArrayData<WorkflowStepResponseWrapper> bunchOfWorkflowSteps = new ArrayData<>();
		APIResponseWrapper<ArrayData<WorkflowStepResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfWorkflowSteps);
				
		try {
			Optional<Collection<WorkflowStep>> listOfWorkflowSteps = workflowStepQueryService.findByWorkflowId(workflowId);
			
			if (!listOfWorkflowSteps.isPresent() || listOfWorkflowSteps.get().isEmpty()) {
				response.setMessage("No workflow steps found for WorkflowType with ID '" + workflowId + "'!");
			} else {
				Function<Collection<WorkflowStep>, List<WorkflowStepResponseWrapper>> workflowStepResponseConverter 
					= (wsCollection) -> {
						List<WorkflowStepResponseWrapper> workflowStepResponseWrappers = new ArrayList<>();
						wsCollection.forEach(ws -> { 
							workflowStepResponseWrappers.add(new WorkflowStepResponseWrapper(ws));
						});
						return workflowStepResponseWrappers;
					};
				List<WorkflowStepResponseWrapper> records = workflowStepResponseConverter.apply(listOfWorkflowSteps.get());
				bunchOfWorkflowSteps.setItems(records.toArray(new WorkflowStepResponseWrapper[records.size()]));
			}
		} catch (Exception e) {
			throw e;
		}
		
		Link self = linkTo(methodOn(WorkflowStepController.class).fetchByWorkflowId(workflowId)).withSelfRel();
		response.add(self);
		
		return response;
	}
	
	@Secured({AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated WorkflowStepRequestWrapper request, BindingResult result) throws Exception {

		if (result.hasErrors()) {
			return new APIResponseWrapper<>(
					new ArrayData<>(ExceptionResponseWrapper.getErrors(result.getFieldErrors(), HttpMethod.POST)),
					"Validation Error!");
		}
		
		APIResponseWrapper<?> response = new APIResponseWrapper<>();
		
		Optional<User> currentUser = userQueryService.findByUsername(currentUser());	
		
		try {
			if (!currentUser.isPresent()) {
				throw new UsernameNotFoundException("No User logged in!");
			}
			
			Optional<Workflow> workflow = workflowQueryService.findById(Optional.of(request.getWorkflowId()));
			if (!workflow.isPresent()) {
				response.setMessage("WorkflowType ID '" + request.getWorkflowId() + "' does not exist!");
				response.setData(null);
				
				return response;
			} 
			
			Optional<Position> position = positionQueryService.findById(Optional.of(request.getPositionId()));
			if (!position.isPresent()) {
				response.setMessage("Position ID '" + request.getPositionId() + "' does not exist!");
				response.setData(null);
				
				return response;
			}
			
			WorkflowStep workflowStep = new WorkflowStep();
			workflowStep.setDescription(request.getDescription());
			workflowStep.setSeq(request.getSeq());
			workflowStep.setSequenceType(SequenceType.valueOf(request.getSequenceType()));
			workflowStep.setApprovalType(ApprovalType.valueOf(request.getApprovalType()));
			workflowStep.setWorkflow(workflow.get());
			workflowStep.setPosition(position.get());
			Optional<WorkflowStep> savedEntity = workflowStepCommandService.save(workflowStep, currentUser.get());
			
			WorkflowStepResponseWrapper workflowStepResponseWrapper = new WorkflowStepResponseWrapper(workflowStep)
					.setCreatedBy(currentUser.get(), linkTo(methodOn(UserController.class).getUserProfileByUsername(
							currentUser.get().getUsername())).withRel(AuthorResponseWrapper.RESOURCE))
					.setUpdatedBy(currentUser.get(), linkTo(methodOn(UserController.class).getUserProfileByUsername(
							currentUser.get().getUsername())).withRel(AuthorResponseWrapper.RESOURCE));
			
			response = new APIResponseWrapper<WorkflowStepResponseWrapper>(workflowStepResponseWrapper);
			
			response.setMessage("Successfully created new workflow step, WorkflowType Step ID = '" + 
					savedEntity.get().getId() + "'");
			
			response.add(linkTo(methodOn(WorkflowStepController.class).create(request, result)).withSelfRel());
			
		} catch (Exception e) {
			throw e;
		}
		
		return response;
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(workflowStepValidator);
	}		
}