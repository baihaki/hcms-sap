package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.5
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add fields: totalAmount, pulseRejectAmount, fuelRejectAmount</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add field: reason</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add fields: lastPrevMonth, lastThisMonth, form</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add fields: bukrs, process</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add field: kunde</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
public class PtrvHeadRequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = -2317050437190237970L;
	
	private int hdvrs;
	private long reinr;
	private String molga;
	private String morei;
	private String schem;
	private String zland;
	private String datv1;
	private String uhrv1;
	private String datb1;
	private String uhrb1;
	private String dath1;
	private String uhrh1;
	private String datr1;
	private String uhrr1;
	private String endrg;
	private String dates;
	private String times;
	private String uname;
	private String repid;
	private Long dantn;
	private Long fintn;
	private String request;
	private String travelPlan;
	private String expenses;
	private String stTrgtg;
	private String stTrgall;
	private String datReduc1;
	private String datReduc2;
	private String datv1Dienst;
	private String uhrv1Dienst;
	private String datb1Dienst;
	private String uhrb1Dienst;
	private Long abordnung;
	private String exchangeDate;
	private String zort1;
	private String kunde;

	private String bukrs;
	private String process;
	private Double lastPrevMonth;
	private Double lastThisMonth;
	private Short form;
	private String reason;
	
	private Double totalAmount;
	private Double pulseRejectAmount;
	private Double fuelRejectAmount;
	

	@JsonProperty("hdvrs")
	public int getHdvrs() {
		return hdvrs;
	}


	@JsonProperty("molga")
	public String getMolga() {
		return molga;
	}


	@JsonProperty("morei")
	public String getMorei() {
		return morei;
	}


	@JsonProperty("schem")
	public String getSchem() {
		return schem;
	}


	@JsonProperty("zland")
	public String getZland() {
		return zland;
	}


	@JsonProperty("datv1")
	public String getDatv1() {
		return datv1;
	}


	@JsonProperty("uhrv1")
	public String getUhrv1() {
		return uhrv1;
	}


	@JsonProperty("datb1")
	public String getDatb1() {
		return datb1;
	}


	@JsonProperty("uhrb1")
	public String getUhrb1() {
		return uhrb1;
	}


	@JsonProperty("dath1")
	public String getDath1() {
		return dath1;
	}


	@JsonProperty("uhrh1")
	public String getUhrh1() {
		return uhrh1;
	}


	@JsonProperty("datr1")
	public String getDatr1() {
		return datr1;
	}


	@JsonProperty("uhrr1")
	public String getUhrr1() {
		return uhrr1;
	}


	@JsonProperty("endrg")
	public String getEndrg() {
		return endrg;
	}


	@JsonProperty("dates")
	public String getDates() {
		return dates;
	}


	@JsonProperty("times")
	public String getTimes() {
		return times;
	}


	@JsonProperty("uname")
	public String getUname() {
		return uname;
	}


	@JsonProperty("repid")
	public String getRepid() {
		return repid;
	}


	@JsonProperty("dantn")
	public Long getDantn() {
		return dantn;
	}


	@JsonProperty("fintn")
	public Long getFintn() {
		return fintn;
	}


	@JsonProperty("request")
	public String getRequest() {
		return request;
	}


	@JsonProperty("travel_plan")
	public String getTravelPlan() {
		return travelPlan;
	}


	@JsonProperty("expenses")
	public String getExpenses() {
		return expenses;
	}


	@JsonProperty("st_trgtg")
	public String getStTrgtg() {
		return stTrgtg;
	}


	@JsonProperty("st_trgall")
	public String getStTrgall() {
		return stTrgall;
	}


	@JsonProperty("dat_reduc1")
	public String getDatReduc1() {
		return datReduc1;
	}


	@JsonProperty("dat_reduc2")
	public String getDatReduc2() {
		return datReduc2;
	}


	@JsonProperty("datv1_dienst")
	public String getDatv1Dienst() {
		return datv1Dienst;
	}


	@JsonProperty("uhrv1_dienst")
	public String getUhrv1Dienst() {
		return uhrv1Dienst;
	}


	@JsonProperty("datb1_dienst")
	public String getDatb1Dienst() {
		return datb1Dienst;
	}


	@JsonProperty("uhrb1_dienst")
	public String getUhrb1Dienst() {
		return uhrb1Dienst;
	}


	@JsonProperty("abordnung")
	public Long getAbordnung() {
		return abordnung;
	}


	@JsonProperty("exchange_date")
	public String getExchangeDate() {
		return exchangeDate;
	}


	@JsonProperty("zort1")
	public String getZort1() {
		return zort1;
	}
	
	
	@JsonProperty("reinr")
	public long getReinr() {
		return reinr;
	}
	
	
	@JsonProperty("kunde")
	public String getKunde() {
		return kunde;
	}
	
	
	@JsonProperty("bukrs")
	public String getBukrs() {
		return bukrs;
	}
	
	@JsonProperty("process")
	public String getProcess() {
		return process;
	}

	@JsonProperty("last_previous_month")
	public Double getLastPrevMonth() {
		return lastPrevMonth;
	}

	@JsonProperty("last_this_month")
	public Double getLastThisMonth() {
		return lastThisMonth;
	}
	
	@JsonProperty("form")
	public Short getForm() {
		return form;
	}

	@JsonProperty("reason")
	public String getReason() {
		return reason;
	}

	@JsonProperty("total_amount")
	public Double getTotalAmount() {
		return totalAmount;
	}

	@JsonProperty("voucher_reject_amount")
	public Double getPulseRejectAmount() {
		return pulseRejectAmount;
	}

	@JsonProperty("fuel_reject_amount")
	public Double getFuelRejectAmount() {
		return fuelRejectAmount;
	}

}