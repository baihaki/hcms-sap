package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.IT0105;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add Constructor of Read from EventData -> IT0105ResponseWrapper(IT0105, String, CommonServiceFactory)</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add status (DocumentStatus) field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class IT0105ResponseWrapper extends ResourceSupport {
	
	private long pernr;
	private String subty;
	private Date endda;
	private Date begda;
	
	private String subtyText;
	private String status;
	private String usrty;
	private String usrtyText;
	private String usridLong;
	
	private  IT0105ResponseWrapper() {}
	
	public IT0105ResponseWrapper(IT0105 it0105) {
		if (it0105 == null) {
			new IT0105ResponseWrapper();
		} else {
			this
			.setPernr(it0105.getId().getPernr())
			.setSubty(it0105.getSubty() != null ? 
					StringUtils.defaultString(it0105.getSubty().getId() != null ? 
							it0105.getSubty().getId().getSubty() : StringUtils.EMPTY, StringUtils.EMPTY) 
					: it0105.getId().getSubty())
			.setEndda(it0105.getId().getEndda()).setBegda(it0105.getId().getBegda())
			.setSubtyText(it0105.getSubty() != null ? StringUtils.defaultString(it0105.getSubty().getStext(), StringUtils.EMPTY) 
					: StringUtils.defaultString(it0105.getId().getSubty(), StringUtils.EMPTY))	
			.setUsrty(it0105.getUsrty() != null ? StringUtils.defaultString(it0105.getUsrty().getId().getSubty(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setUsrtyText(it0105.getUsrty() != null ? StringUtils.defaultString(it0105.getUsrty().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setUsridLong(StringUtils.defaultString(it0105.getUsridLong(), StringUtils.EMPTY))
			.setStatus(StringUtils.defaultString(it0105.getStatus().name(), StringUtils.EMPTY));
		}
	}

	public IT0105ResponseWrapper(IT0105 currentData, String eventData, CommonServiceFactory serviceFactory) throws JSONException, Exception {
		this(currentData);
		IT0105ResponseWrapper itWrapper = (IT0105ResponseWrapper) new EventDataWrapper(eventData, this).getItWrapper();
		String[] ignoreProperties = EventDataWrapper.getNullPropertyNames(itWrapper);
		BeanUtils.copyProperties(itWrapper, this, ignoreProperties);
	}
	
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	public IT0105ResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	@JsonProperty("subtype")
	public String getSubty() {
		return subty;
	}
	
	public IT0105ResponseWrapper setSubty(String subty) {
		this.subty = subty;
		return this;
	}
	
	@JsonProperty("document_status")
	public String getStatus() {
		return status;
	}
	
	public IT0105ResponseWrapper setStatus(String status) {
		this.status = status;
		return this;
	}
	
	@JsonProperty("subtype_text")
	public String getSubtyText() {
		return subtyText;
	}
	
	public IT0105ResponseWrapper setSubtyText(String subtyText) {
		this.subtyText = subtyText;
		return this;
	}
	
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	public IT0105ResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}
	
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	public IT0105ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	@JsonProperty("communication_type")
	public String getUsrty() {
		return usrty;
	}
	
	public IT0105ResponseWrapper setUsrty(String usrty) {
		this.usrty = usrty;
		return this;
	}
	
	@JsonProperty("communication_type_name")
	public String getUsrtyText() {
		return usrtyText;
	}
	
	public IT0105ResponseWrapper setUsrtyText(String usrtyText) {
		this.usrtyText = usrtyText;
		return this;
	}
	
	@JsonProperty("long_id")
	public String getUsridLong() {
		return usridLong;
	}
	
	public IT0105ResponseWrapper setUsridLong(String usridLong) {
		this.usridLong = usridLong;
		return this;
	}
}