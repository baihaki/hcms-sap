package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class WorkflowStepRequestWrapper {

	private String workflowId;
	private int seq;
	private String description;
	private String positionId;
	private String sequenceType;
	private String approvalType;
	
	WorkflowStepRequestWrapper() {}
	
	public WorkflowStepRequestWrapper(String workflowId, String positionId) {
		this.workflowId = workflowId;
		this.positionId = positionId;
	}
	
	@JsonProperty("workflow_id")
	public String getWorkflowId() {
		return workflowId;
	}
	
	@JsonProperty("sequence_no")
	public int getSeq() {
		return seq;
	}
	
	@JsonProperty("workflow_step_description")
	public String getDescription() {
		return description;
	}
	
	@JsonProperty("position_id")
	public String getPositionId() {
		return positionId;
	}
	
	@JsonProperty("sequence_type")
	public String getSequenceType() {
		return sequenceType;
	}
	
	public WorkflowStepRequestWrapper setSequenceType(String sequenceType) {
		this.sequenceType = sequenceType;
		return this;
	}
	
	@JsonProperty("approval_type")
	public String getApprovalType() {
		return approvalType;
	}
	
	public WorkflowStepRequestWrapper setApprovalType(String approvalType) {
		this.approvalType = approvalType;
		return this;
	}
}