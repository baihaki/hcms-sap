package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.IT0040;
import com.abminvestama.hcms.core.model.entity.T538T;
import com.abminvestama.hcms.core.model.entity.T591S;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.business.query.T538TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T591SQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0040RequestWrapper;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Component("it0040RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT0040RequestBuilderUtil {
	
	private T591SQueryService t591sQueryService;
	private T538TQueryService t538tQueryService;
	
	@Autowired
	void setT591SQueryService(T591SQueryService t591sQueryService) {
		this.t591sQueryService = t591sQueryService;
	}
	
	@Autowired
	void setT538TQueryService(T538TQueryService t538tQueryService) {
		this.t538tQueryService = t538tQueryService;
	}
	
	/**
	 * Compare IT0040 requested payload with an existing IT0040 in the database.
	 * Update the existing IT0040 data with the latest data comes from the requested payload. 
	 * 
	 * @param requestPayload requested data 
	 * @param it0040DB current existing IT0040 in the database.
	 * @return updated IT0040 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0040, IT0040RequestWrapper, String> compareAndReturnUpdatedData(IT0040RequestWrapper requestPayload, IT0040 it0040DB) {
		if (it0040DB == null) {
			it0040DB = new IT0040();
		} else {
			
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getItxex(), it0040DB.getItxex())) {
				it0040DB.setItxex(requestPayload.getItxex());
				requestPayload.setDataChanged(true);
			}
			
			if (StringUtils.isNotBlank(requestPayload.getLeihg())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getLeihg(), it0040DB.getLeihg() != null ? it0040DB.getLeihg().getId().getSubty() : StringUtils.EMPTY)) {
					try {
						T591SKey key = new T591SKey(SAPInfoType.EMPLOYEE_INVENTORY.infoType(), requestPayload.getLeihg());
						Optional<T591S> newT591S = t591sQueryService.findById(Optional.ofNullable(key));
						if (newT591S.isPresent()) {
							it0040DB.setLeihg(newT591S.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T591S not found... cancel the update process
					}
				}
			}
			
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getAnzkl(), it0040DB.getAnzkl() != null ? it0040DB.getAnzkl().doubleValue() : 0.0)) {
				it0040DB.setAnzkl(requestPayload.getAnzkl());
				requestPayload.setDataChanged(true);
			}		
			
			if (StringUtils.isNotBlank(requestPayload.getZeinh())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getZeinh(), it0040DB.getZeinh() != null ? it0040DB.getZeinh().getZeinh() : StringUtils.EMPTY)) {
					try {
						Optional<T538T> newT538T = t538tQueryService.findById(Optional.ofNullable(requestPayload.getZeinh()));
						if (newT538T.isPresent()) {
							it0040DB.setZeinh(newT538T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T538T not found... cancel the update process
					}
				}
			}
		}
		
		return new RequestObjectComparatorContainer<>(it0040DB, requestPayload);
	}								
}