package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.IT0014;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@JsonInclude(Include.NON_NULL)
public class IT0014ResponseWrapper extends ResourceSupport {
	
	private long pernr;
	private String subty;
	private String subtyText;
	private Date endda;
	private Date begda;
	
	private long seqnr;
	private String lgart;
	private String lgtxt;
	private String opken;
	private double betrg;
	private String waers;
	
	private IT0014ResponseWrapper() {}
	
	public IT0014ResponseWrapper(IT0014 it0014) {
		if (it0014 == null) {
			new IT0014ResponseWrapper();
		} else {
			this
			.setPernr(it0014.getId().getPernr())
			.setSubty(it0014.getSubty() != null ? 
					StringUtils.defaultString(it0014.getSubty().getId() != null ? it0014.getSubty().getId().getSubty() : StringUtils.EMPTY) : it0014.getId().getSubty())
			.setSubtyText(it0014.getSubty() != null ? StringUtils.defaultString(it0014.getSubty().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setEndda(it0014.getId().getEndda()).setBegda(it0014.getId().getBegda())
			.setSeqnr(it0014.getSeqnr() != null ? it0014.getSeqnr().longValue() : 0L)
			.setLgart(StringUtils.defaultString(it0014.getLgart().getLgart(), StringUtils.EMPTY))
			.setLgtxt(it0014.getLgart() != null ? StringUtils.defaultString(it0014.getLgart().getLgtxt(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setOpken(StringUtils.defaultString(it0014.getOpken(), StringUtils.EMPTY))
			.setBetrg(it0014.getBetrg() != null ? it0014.getBetrg().doubleValue() : 0.0)
			.setWaers(StringUtils.defaultString(it0014.getWaers(), StringUtils.EMPTY));
		}
	}
	
	/**
	 * GET Employee SSN/ID.
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	private IT0014ResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	@JsonProperty("subtype")
	public String getSubty() {
		return subty;
	}
	
	private IT0014ResponseWrapper setSubty(String subty) {
		this.subty = subty;
		return this;
	}
	
	/**
	 * GET Subtype Text.
	 * 
	 * @return
	 */
	@JsonProperty("subtype_text")
	public String getSubtyText() {
		return subtyText;
	}
	
	private IT0014ResponseWrapper setSubtyText(String subtyText) {
		this.subtyText = subtyText;
		return this;
	}
	
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	private IT0014ResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}
	
	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	private IT0014ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * GET infotype record no.
	 * 
	 * @return
	 */
	@JsonProperty("infotype_record_no")
	public long getSeqnr() {
		return seqnr;
	}
	
	private IT0014ResponseWrapper setSeqnr(long seqnr) {
		this.seqnr = seqnr;
		return this;
	}
	
	/**
	 * GET Wage Type.
	 * 
	 * @return
	 */
	@JsonProperty("wage_type")
	public String getLgart() {
		return lgart;
	}
	
	private IT0014ResponseWrapper setLgart(String lgart) {
		this.lgart = lgart;
		return this;
	}
	
	/**
	 * GET Wage Type Name/Text.
	 * 
	 * @return
	 */
	@JsonProperty("wage_type_text")
	public String getLgtxt() {
		return lgtxt;
	}
	
	private IT0014ResponseWrapper setLgtxt(String lgtxt) {
		this.lgtxt = lgtxt;
		return this;
	}
	
	/**
	 * GET Operation Indicator.
	 * 
	 * @return
	 */
	@JsonProperty("operation_indicator")
	public String getOpken() {
		return opken;
	}
	
	private IT0014ResponseWrapper setOpken(String opken) {
		this.opken = opken;
		return this;
	}
	
	/**
	 * GET Amount.
	 * 
	 * @return
	 */
	@JsonProperty("amount")
	public double getBetrg() {
		return betrg;
	}
	
	private IT0014ResponseWrapper setBetrg(double betrg) {
		this.betrg = betrg;
		return this;
	}
	
	/**
	 * GET Currency.
	 * 
	 * @return
	 */
	@JsonProperty("currency")
	public String getWaers() {
		return waers;
	}
	
	private IT0014ResponseWrapper setWaers(String waers) {
		this.waers = waers;
		return this;
	}
}