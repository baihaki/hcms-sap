package com.abminvestama.hcms.rest.api.controller;



import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.CaAdvanceCode;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.UserRole;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.CSKT;
import com.abminvestama.hcms.core.model.entity.CaOrder;
import com.abminvestama.hcms.core.model.entity.CaStlDtl;
import com.abminvestama.hcms.core.model.entity.CaStlHdr;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.PositionRelation;
import com.abminvestama.hcms.core.model.entity.CaReqHdr;
import com.abminvestama.hcms.core.model.entity.CaReqDtl;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.CaReqHdrCommandService;
import com.abminvestama.hcms.core.service.api.business.command.CaReqDtlCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0009QueryService;
import com.abminvestama.hcms.core.service.api.business.query.CaReqHdrQueryService;
import com.abminvestama.hcms.core.service.api.business.query.CaReqDtlQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.helper.CaHdrDocSoapObject;
import com.abminvestama.hcms.core.service.helper.CaReqHdrMapper;
import com.abminvestama.hcms.core.service.helper.CaReqHdrSoapObject;
import com.abminvestama.hcms.core.service.helper.CaStlDtlSoapObject;
import com.abminvestama.hcms.core.service.helper.CaStlHdrSoapObject;
//import com.abminvestama.hcms.core.service.helper.CaReqHdrSoapObject;
//import com.abminvestama.hcms.core.service.helper.CaReqDtlSoapObject;
import com.abminvestama.hcms.core.service.helper.ZmedTrxhdrMapper;
import com.abminvestama.hcms.core.service.helper.ZmedTrxhdrSoapObject;
import com.abminvestama.hcms.core.service.helper.ZmedTrxhdrsSoapObject;
import com.abminvestama.hcms.core.service.util.MailService;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.CaTransactionRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.CaReqHdrRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.CaReqDtlRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.CaTransactionResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.CaReqHdrResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ZmedTrxhdrResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.CaReqHdrRequestBuilderUtil;
import com.abminvestama.hcms.rest.api.helper.CaReqDtlRequestBuilderUtil;



/**
 * 
 * @since 3.0.0
 * @version 3.0.3 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.3</td><td>Baihaki</td><td>Add parameter to findByCriteria: periodBegin, periodEnd</td></tr>
 *     <tr><td>3.0.2</td><td>Baihaki</td><td>Add methods: findOneByReqNo, findByCriteria, findByDateWithItems</td></tr>
 *     <tr><td>3.0.1</td><td>Baihaki</td><td>Add createBulkWithItems method</td></tr>
 *     <tr><td>3.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v3/ca_req_hdr")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CaReqHdrController extends AbstractResource {
	
	private CaReqHdrCommandService caReqHdrCommandService;
	private CaReqHdrQueryService caReqHdrQueryService;
	private CaReqHdrRequestBuilderUtil caReqHdrRequestBuilderUtil;
	
	private CaReqDtlCommandService caReqDtlCommandService;
	private CaReqDtlQueryService caReqDtlQueryService;
	private CaReqDtlRequestBuilderUtil caReqDtlRequestBuilderUtil;
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	
	private MuleConsumerService muleConsumerService;
	private MailService mailService;
	private CommonServiceFactory commonServiceFactory;
	private IT0009QueryService it0009QueryService;
	
	public static final int WAITING_FOR_HEAD = 1;
	public static final int WAITING_FOR_DIRECTOR = 2;
	public static final int WAITING_FOR_GA = 3;
	public static final int WAITING_FOR_HR = 4;
	public static final int WAITING_FOR_ACCOUNTING = 5;
	public static final int WAITING_FOR_TREASURY = 6;
	public static final int WORKFLOW_COMPLETED = 7;
	public static final int WORKFLOW_COMPLETED_PAID = 8;
	public static final int WORKFLOW_CANCELED = 10;
	public static final int WORKFLOW_CANCELED_FAILED = 11;
	public static final int WORKFLOW_REJECTED = 0;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	@Autowired
	void setCaReqHdrCommandService(CaReqHdrCommandService caReqHdrCommandService) {
		this.caReqHdrCommandService = caReqHdrCommandService;
	}
	
	@Autowired
	void setCaReqHdrQueryService(CaReqHdrQueryService caReqHdrQueryService) {
		this.caReqHdrQueryService = caReqHdrQueryService;
	}
	
	@Autowired
	void setCaReqHdrRequestBuilderUtil(CaReqHdrRequestBuilderUtil caReqHdrRequestBuilderUtil) {
		this.caReqHdrRequestBuilderUtil = caReqHdrRequestBuilderUtil;
	}
	
	@Autowired
	void setCaReqDtlRequestBuilderUtil(CaReqDtlRequestBuilderUtil caReqDtlRequestBuilderUtil) {
		this.caReqDtlRequestBuilderUtil = caReqDtlRequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	void setIT0009QueryService(IT0009QueryService it0009QueryService) {
		this.it0009QueryService = it0009QueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@Autowired
	void setCaReqDtlQueryService(CaReqDtlQueryService caReqDtlQueryService) {
		this.caReqDtlQueryService = caReqDtlQueryService;
	}
	
	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}

	@Autowired
	void setCaReqDtlCommandService(CaReqDtlCommandService caReqDtlCommandService) {
		this.caReqDtlCommandService = caReqDtlCommandService;
	}

	@Autowired
	void setMailService(MailService mailService) {
		this.mailService = mailService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	private void updateDocumentStatus(List<CaReqHdr> listCaReqHdr) {
		
		List<CaReqHdr> listCaReqHdrForSync =  new ArrayList<CaReqHdr>();
		List<CaReqHdr> listCaReqHdrForCurrentSync =  new ArrayList<CaReqHdr>();
		listCaReqHdr.forEach(caReqHdr -> {
			// sync / update document status only if last sync was 12 minutes past
			if (caReqHdr.getLastSync() == null ||
					(new Date().getTime() - caReqHdr.getLastSync().getTime()) / (60 * 1000) % 60 >= 12)
			listCaReqHdrForSync.add(caReqHdr);
			listCaReqHdrForCurrentSync.add(caReqHdr);
		});
		if (!listCaReqHdrForSync.isEmpty()) {
			
			CaHdrDocSoapObject caHdrDocSoapObject = new CaHdrDocSoapObject();
			
			List<CaReqHdrSoapObject> listCaReqHdrSoapObject =  new ArrayList<CaReqHdrSoapObject>();
			for (CaReqHdr caReqHdr : listCaReqHdrForSync) {
				listCaReqHdrSoapObject.add(new CaReqHdrSoapObject(caReqHdr.getBukrs().getBukrs(), 
						CommonDateFunction.convertDateToStringY(caReqHdr.getApprovedTreasuryAt()), caReqHdr.getReqNo()));
			}
			caHdrDocSoapObject.setListCaReqHdr(listCaReqHdrSoapObject);
			
			List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.CA_DOCUMENT_FETCH.service(),
					BAPIFunction.CA_DOCUMENT_FETCH_URI.service(), caHdrDocSoapObject);
			
			for (int i = 0; i < list.size(); i++) {
				// synchronize CaReqHdr Document Status fields (in different thread) with the fetched fields from SAP
				if (i > 0 && list.get(i).get("reqno") != null) {
					CaReqHdr caReqHdrDB = listCaReqHdrForSync.get(0);
					CaReqHdr caReqHdrEntity = new CaReqHdrMapper(list.get(i)).getCaReqHdr();
					for (CaReqHdr item : listCaReqHdrForSync) {
						// compare SAP claim number (reqno) with the saved HCMS claim number
						if (list.get(i).get("reqno").equals(item.getReqNo()) &&
								StringUtils.defaultString(item.getDocno()).equals(caReqHdrEntity.getDocno())) {
							caReqHdrDB =  item;
							break;
						}
					}
					caReqHdrDB.setPaymentDate(caReqHdrEntity.getPaymentDate());
					if (StringUtils.isNotEmpty(caReqHdrEntity.getDocno()))
						caReqHdrDB.setDocno(caReqHdrEntity.getDocno());
					caReqHdrDB.setLastSync(new Date());
					caReqHdrDB.setPaymentStatus2(caReqHdrEntity.getPaymentStatus2());
					caReqHdrDB.setDocno2(caReqHdrEntity.getDocno2());
					if (caReqHdrEntity.getPaymentStatus2().equals("P")) {
						caReqHdrDB.setProcess(WORKFLOW_COMPLETED_PAID);
					}
					/*
					// Set Bank Account
					if (caReqHdrEntity.getPaymentStatus2().equals("P")) {
						// Set Employee's Main Bank Detail
						Page<IT0009> page = it0009QueryService.findByPernrAndStatus(caReqHdrDB.getPernr().longValue(),
								DocumentStatus.PUBLISHED.toString(), 1);
						if (page.hasContent() && page.getContent().get(0).getSubty().getId().getSubty().equals("0")) {
							IT0009 mainBank = page.getContent().get(0);
							caReqHdrDB.setBankName(mainBank.getBankl() != null ?
									StringUtils.defaultString(mainBank.getBankl().getBanka()) : StringUtils.EMPTY);
							caReqHdrDB.setBankAccount(StringUtils.defaultString(mainBank.getBankn()));
						}
					}
					*/
				    //caReqHdrCommandService.threadUpdate(caReqHdrDB);
				    try {
						caReqHdrCommandService.save(caReqHdrDB, null);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		
	}

	/*
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_criteria", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<CaReqHdrResponseWrapper>> findByCriteria(
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "process", required = false) String process,
			@RequestParam(value = "finance_payment_status", required = false, defaultValue = "") String status,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {

		Page<CaReqHdrResponseWrapper> responsePage = new PageImpl<CaReqHdrResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<CaReqHdrResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<CaReqHdr> resultPage = null;
		String bukrs = null;
		Long pernr = null;
		String[] processes = StringUtils.isEmpty(process) ? null : process.split(",");
		String notStatus = status;
		if (status.equalsIgnoreCase("N")) {
			notStatus = "P";
		} else if (status.equalsIgnoreCase("P")) {
			notStatus = "N";
		}
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		
		try {
			if (StringUtils.isNotEmpty(ssn)) {
				pernr = Long.valueOf(ssn);
				if (!currentUser.get().getRoles().contains(adminRole.get()) &&
						!currentUser.get().getEmployee().getPernr().equals(pernr) ) {
					throw new AuthorizationException(
							"Insufficient Privileges. You can't fetch data of other Employee.");
				}
				resultPage = (processes != null && processes.length > 1) ?
						caReqHdrQueryService.findByPernrAndProcessesWithPaging(pernr.longValue(), processes, notStatus, pageNumber) :
							caReqHdrQueryService.findByPernrAndProcessWithPaging(pernr.longValue(), process, notStatus, pageNumber);
			} else {
				// no specified ssn, MUST BE ADMIN
				if (!currentUser.get().getRoles().contains(adminRole.get())) {
					throw new AuthorizationException(
							"Insufficient Privileges. Please login as 'ADMIN'!");
				}
				bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
				resultPage = (processes != null && processes.length > 1) ?
						caReqHdrQueryService.findByBukrsAndProcessesWithPaging(bukrs, processes, notStatus, pageNumber) :
							caReqHdrQueryService.findByBukrsAndProcessWithPaging(bukrs, process, notStatus, pageNumber);
			}
			
			updateDocumentStatus(resultPage.getContent());
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			
			if (resultPage.hasContent()) {

				List<CaReqHdrResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(caReqHdr -> {
					records.add(new CaReqHdrResponseWrapper(caReqHdr));
				});
				responsePage = new PageImpl<CaReqHdrResponseWrapper>(records, caReqHdrQueryService.createPageRequest(pageNumber),
						resultPage.getTotalElements());
				response.setData(responsePage);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(CaReqHdrController.class).findByCriteria(ssn, process, notStatus, pageNumber)).withSelfRel());
		
		return response;
	}
	*/
	
	@RequestMapping(method = RequestMethod.GET, value = "/req_no/{reqNo}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<CaTransactionResponseWrapper> findOneByReqNo(@PathVariable String reqNo)
			throws Exception {
		
		APIResponseWrapper<CaTransactionResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			Optional<CaReqHdr> caReqHdrObject = caReqHdrQueryService.findById(Optional.ofNullable(reqNo));
			
			if (!caReqHdrObject.isPresent()) {
				response.setMessage("No Data Found");
				response.setData(new CaTransactionResponseWrapper(null, null));
			} else {
				ArrayList<CaReqDtl> listCaReqDtl = (ArrayList<CaReqDtl>) caReqDtlQueryService.findByReqNo(caReqHdrObject.get().getReqNo()).
						stream().collect(Collectors.toList());
				CaReqDtl[] caReqDtl = listCaReqDtl.isEmpty() ? new CaReqDtl[0] : new CaReqDtl[listCaReqDtl.size()];
				List<CaReqHdr> listOfCaReqHdr = new ArrayList<>();
				listOfCaReqHdr.add(caReqHdrObject.get());
				updateDocumentStatus(listOfCaReqHdr);
				response.setData(new CaTransactionResponseWrapper(caReqHdrObject.get(), listCaReqDtl.toArray(caReqDtl)));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(CaReqHdrController.class).findOneByReqNo(reqNo)).withSelfRel());
		return response;
	}

	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_criteria", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<CaReqHdrResponseWrapper>> findByCriteria(
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "process", required = false, defaultValue="1") String process,
			@RequestParam(value = "advance_code", required = false, defaultValue = "PC,BT,TA,RE") String advanceCode,
			@RequestParam(value = "date_begin", required = false, defaultValue = "1980-01-01") String periodBegin,
			@RequestParam(value = "date_end", required = false, defaultValue = "9999-12-31") String periodEnd,
			@RequestParam(value = "finance_payment_status", required = false, defaultValue = "") String status,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {

		Page<CaReqHdrResponseWrapper> responsePage = new PageImpl<CaReqHdrResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<CaReqHdrResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<CaReqHdr> resultPage = null;
		String bukrs = null;
		Long pernr = null;
		String[] processesString = StringUtils.isEmpty(process) ? null : process.split(",");
		String[] advanceCodes = advanceCode.split(",");
		String notStatus = status;
		if (status.equalsIgnoreCase("N")) {
			notStatus = "P";
		} else if (status.equalsIgnoreCase("P")) {
			notStatus = "N";
		}
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> headRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_HEAD"));
		Optional<Role> directorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_DIRECTOR"));
		Optional<Role> gaRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_GA"));
		Optional<Role> accountingRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ACCOUNTING"));
		Optional<Role> treasuryRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_TREASURY"));
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		Optional<Role> superiorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_SUPERIOR"));
		
		try {
			Integer[] processes = StringUtils.isEmpty(process) ? null : new Integer[processesString.length];
			if (processesString != null) {
				for (int i=0; i < processesString.length; i++) {
					processes[i] = Integer.valueOf(processesString[i]).intValue();
				}
			}
			
			Date dateBegin = CommonDateFunction.convertDateRequestParameterIntoDate(periodBegin);
			Date dateEnd = CommonDateFunction.convertDateRequestParameterIntoDate(periodEnd);
			
			if (StringUtils.isNotEmpty(ssn)) {
				pernr = Long.valueOf(ssn);
				if (!currentUser.get().getRoles().contains(treasuryRole.get()) &&
						!currentUser.get().getEmployee().getPernr().equals(pernr) ) {
					throw new AuthorizationException(
							"Insufficient Privileges. You can't fetch data of other Employee.");
				}
				resultPage = (processes != null && processes.length > 1) ?
						caReqHdrQueryService.findByPernrAndProcessesWithPaging(pernr.longValue(), processes, advanceCodes, dateBegin, dateEnd, notStatus, pageNumber) :
							caReqHdrQueryService.findByPernrAndProcessWithPaging(pernr.longValue(),
									StringUtils.isEmpty(process) ? null : Integer.valueOf(process), advanceCodes, dateBegin, dateEnd, notStatus, pageNumber);
			} else {
				// no specified ssn, MUST BE TREASURY or SUPERIOR or (GA/ADMIN/ACCOUNTING)
				if (!currentUser.get().getRoles().contains(treasuryRole.get()) && !currentUser.get().getRoles().contains(superiorRole.get()) &&
						!currentUser.get().getRoles().contains(gaRole.get()) && !currentUser.get().getRoles().contains(adminRole.get())
						&& !currentUser.get().getRoles().contains(accountingRole.get())) {
					throw new AuthorizationException(
						"Insufficient Privileges. Please login as authorized Administrator (GA/HR ADMIN/ACCOUNTING/TREASURY) or 'SUPERIOR'!");
				}
				
				// If NOT TREASURY
				if (!currentUser.get().getRoles().contains(treasuryRole.get()) && process != null && !process.equals(WORKFLOW_COMPLETED)) {
					
					// If (ROLE SUPERIOR/HEAD ONLY) can only fetch Request which have been released by User (process=1)
					if ( (currentUser.get().getRoles().contains(headRole.get()) && !process.contains(String.valueOf(WAITING_FOR_HEAD))) ||
					   (process.equals(String.valueOf(WAITING_FOR_HEAD)) && !currentUser.get().getRoles().contains(headRole.get())) ) {
						throw new AuthorizationException(
								"Insufficient Privileges. Please login as 'TREASURY' or process=".concat(String.valueOf(WAITING_FOR_HEAD)).concat(" for 'HEAD'!"));
					}
					// If (ROLE SUPERIOR/DIRECTOR ONLY) can only fetch Request which have been released by User/HEAD (process=2)
					if ( (currentUser.get().getRoles().contains(directorRole.get()) && !process.equals(String.valueOf(WAITING_FOR_DIRECTOR))) ||
					   (process.equals(String.valueOf(WAITING_FOR_DIRECTOR)) && !currentUser.get().getRoles().contains(directorRole.get())) ) {
						throw new AuthorizationException(
								"Insufficient Privileges. Please login as 'TREASURY' or process=".concat(String.valueOf(WAITING_FOR_DIRECTOR)).concat(" for 'DIRECTOR'!"));
					}
					// If (ROLE GA ONLY) can only fetch Request which have been released by HEAD/DIRECTOR (process=3)
					if ( (currentUser.get().getRoles().contains(gaRole.get()) && !process.equals(String.valueOf(WAITING_FOR_GA))) ||
					   (process.equals(String.valueOf(WAITING_FOR_GA)) && !currentUser.get().getRoles().contains(gaRole.get())) ) {
						throw new AuthorizationException(
								"Insufficient Privileges. Please login as 'TREASURY' or process=".concat(String.valueOf(WAITING_FOR_GA)).concat(" for 'GA'!"));
					}
					// If (ROLE ADMIN ONLY) can only fetch Request which have been released by GA (process=4)
					if ( (currentUser.get().getRoles().contains(adminRole.get()) && !process.equals(String.valueOf(WAITING_FOR_HR))) ||
					   (process.equals(String.valueOf(WAITING_FOR_HR)) && !currentUser.get().getRoles().contains(adminRole.get())) ) {
						throw new AuthorizationException(
								"Insufficient Privileges. Please login as 'TREASURY' or process=".concat(String.valueOf(WAITING_FOR_GA)).concat(" for 'HR ADMIN'!"));
					}
					// If (ROLE ACCOUNTING ONLY) can only fetch Request which have been released by HEAD/DIRECTOR (Cash Advance) or by HR ADMIN (Travel Advance) (process=5)
					if ( (currentUser.get().getRoles().contains(accountingRole.get()) && !process.contains(String.valueOf(WAITING_FOR_ACCOUNTING))) ||
					   (process.equals(WAITING_FOR_ACCOUNTING) && !currentUser.get().getRoles().contains(accountingRole.get())) ) {
						throw new AuthorizationException(
								"Insufficient Privileges. Please login as 'TREASURY' or process=".concat(String.valueOf(WAITING_FOR_ACCOUNTING)).concat(" for 'ACCOUNTING'!"));
					}
					
				}
				
				String superiorPosition = null;
				// If (ROLE SUPERIOR) and process=7 (PUBLISHED request) set superiorPosition parameter as additional filter,
				// Superior may fetch transaction requested by Sub-Ordinates
				if (!currentUser.get().getRoles().contains(treasuryRole.get()) && process != null && process.equals("7")) {
					superiorPosition = currentUser.get().getEmployee().getPosition().getId();
				}
				bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
				String aprovalByUserId = null;
				if (process.equals(String.valueOf(WAITING_FOR_HEAD)) || process.equals(String.valueOf(WAITING_FOR_DIRECTOR))) {
					aprovalByUserId = currentUser.get().getId();
				}
				resultPage = (processes != null && processes.length > 1) ?
						caReqHdrQueryService.findByBukrsAndProcessesWithPaging(bukrs, processes, advanceCodes, dateBegin, dateEnd, notStatus, pageNumber) :
							caReqHdrQueryService.findByBukrsAndProcessWithPaging(bukrs, StringUtils.isEmpty(process) ? null : Integer.valueOf(process),
									advanceCodes, aprovalByUserId, dateBegin, dateEnd, notStatus, superiorPosition, pageNumber);
			}
			
			updateDocumentStatus(resultPage.getContent());
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			
			if (resultPage.hasContent()) {

				List<CaReqHdrResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(caReqHdr -> {
					records.add(new CaReqHdrResponseWrapper(caReqHdr));
				});
				responsePage = new PageImpl<CaReqHdrResponseWrapper>(records, caReqHdrQueryService.createPageRequest(pageNumber),
						resultPage.getTotalElements());
				response.setData(responsePage);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(CaReqHdrController.class).findByCriteria(ssn, process, advanceCode,
			periodBegin, periodEnd, status, pageNumber)).withSelfRel());
		
		return response;
	}
	
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_between", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<CaTransactionResponseWrapper>> findByDateWithItems(
			@RequestParam(value = "date_begin", required = false, defaultValue = "1980-01-01") String dateBegin,
			@RequestParam(value = "date_end", required = false, defaultValue = "9999-12-31") String dateEnd,
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "process", required = false, defaultValue = "0,1,2,3,4,5,6,7,8") String process,
			@RequestParam(value = "advance_code", required = false, defaultValue = "PC,BT") String advanceCode) throws Exception {

		ArrayData<CaTransactionResponseWrapper> bunchOfCa = new ArrayData<>();
		APIResponseWrapper<ArrayData<CaTransactionResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfCa);
		
		String bukrs = null;
		Long pernr = null;
		String[] processesString = StringUtils.isEmpty(process) ? null : process.split(",");
		Integer[] processes = StringUtils.isEmpty(process) ? null : new Integer[processesString.length];
		if (processesString != null) {
			for (int i=0; i < processesString.length; i++) {
				processes[i] = Integer.valueOf(processesString[i]).intValue();
			}
		}
		String[] advanceCodes = advanceCode.split(",");
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> treasuryRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_TREASURY"));
		Optional<Role> directorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_DIRECTOR"));
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		Optional<Role> superiorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_SUPERIOR"));
		
		try {
			
			if (StringUtils.isNotEmpty(ssn)) {
				pernr = Long.valueOf(ssn);
				if (!currentUser.get().getRoles().contains(treasuryRole.get()) &&
						!currentUser.get().getEmployee().getPernr().equals(pernr) ) {
					throw new AuthorizationException(
							"Insufficient Privileges. You can't fetch data of other Employee.");
				}
				
			} else {
				// no specified ssn, MUST BE TREASURY / DIRECTOR 
				if (!currentUser.get().getRoles().contains(treasuryRole.get()) && !currentUser.get().getRoles().contains(directorRole.get())) {
					throw new AuthorizationException(
							"Insufficient Privileges. Please login as 'TREASURY' or 'DIRECTOR'!");
				}
				String superiorPosition = null;
				// If NOT TREASURY (ROLE DIRECTOR ONLY) set superiorPosition parameter as additional filter
				if (!currentUser.get().getRoles().contains(treasuryRole.get()) && process != null && process.equals("7")) {
					superiorPosition = currentUser.get().getEmployee().getPosition().getId();
				}
				bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
			}
			
			List<CaReqHdr> listOfCaReqHdr = new ArrayList<>();
			Optional<Collection<CaReqHdr>> collectionOfCaReqHdr = caReqHdrQueryService.findByPernrAndProcessesAndBetweenDate(pernr, processes,
					advanceCodes, CommonDateFunction.convertDateRequestParameterIntoDate(dateBegin),
					CommonDateFunction.convertDateRequestParameterIntoDate(dateEnd));
			if (collectionOfCaReqHdr.isPresent() && collectionOfCaReqHdr.get().size() > 0) {
				listOfCaReqHdr = collectionOfCaReqHdr.get().stream().collect(Collectors.toList());
				List<CaTransactionResponseWrapper> records = new ArrayList<>();
				for (CaReqHdr caReqHdr : listOfCaReqHdr) {
					
					ArrayList<CaReqDtl> listCaReqDtl = (ArrayList<CaReqDtl>) caReqDtlQueryService.findByReqNo(caReqHdr.getReqNo()).
							stream().collect(Collectors.toList());
					CaReqDtl[] caReqDtl = listCaReqDtl.isEmpty() ? new CaReqDtl[0] : new CaReqDtl[listCaReqDtl.size()];
					records.add(new CaTransactionResponseWrapper(caReqHdr, listCaReqDtl.toArray(caReqDtl)));
				}
				bunchOfCa.setItems(records.toArray(new CaTransactionResponseWrapper[records.size()]));
			} else {
				response.setMessage("No Data Found");
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(CaReqHdrController.class).findByDateWithItems(dateBegin, dateEnd, ssn, process, advanceCode)).withSelfRel());
		
		return response;
	}
	
	/*
	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated CaReqHdrRequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> caReqHdrHCMSEntity = hcmsEntityQueryService.findByEntityName(CaReqHdr.class.getSimpleName());
			if (!caReqHdrHCMSEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + CaReqHdr.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<CaReqHdr, CaReqHdrRequestWrapper, String> newObjectContainer 
					= caReqHdrRequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				
				Optional<CaReqHdr> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
				//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					newObjectContainer.getEntity().get().setPaymentStatus("N");
					savedEntity = caReqHdrCommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create CaReqHdr data. Please check your data!");
				}
				
				
				
				
				StringBuilder key = new StringBuilder("hdvrs=" + savedEntity.get().getId().getHdvrs());
				key.append(",pernr=" + savedEntity.get().getId().getPernr());
				key.append(",reinr=" + savedEntity.get().getId().getReinr());
				
				EventLog eventLog = new EventLog(caReqHdrHCMSEntity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				
				
				CaReqHdrResponseWrapper responseWrapper = new CaReqHdrResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);
				
				 Post to MuleSoft 
				
				CaReqHdr caReqHdrEntity = savedEntity.get();
				ArrayList<CaReqDtl> listCaReqDtl = (ArrayList<CaReqDtl>) caReqDtlQueryService.findByTripnumber(caReqHdrEntity.getId().getReinr()).
						stream().collect(Collectors.toList());
				
				CaReqHdrSoapObject caReqHdrSoapObject = new CaReqHdrSoapObject(caReqHdrEntity);
				List<CaReqDtlSoapObject> caReqDtlSoapObjects = new ArrayList<CaReqDtlSoapObject>();
				
				for (int i=0; i<listCaReqDtl.size(); i++) {
					caReqDtlSoapObjects.add(new CaReqDtlSoapObject(listCaReqDtl.get(i)));
				}
				caReqHdrSoapObject.setTripReceipts(caReqDtlSoapObjects);

				HashMap<String, String> resultMap = (HashMap<String, String>) muleConsumerService.
						postToSAPSync(BAPIFunction.ECOSTTRIPHDR_CREATE.service(), BAPIFunction.ECOSTTRIPHDR_CREATE_URI.service(), caReqHdrSoapObject);
				if (resultMap.get("errorCode") == StringUtils.EMPTY && resultMap.get("tripnumber") != StringUtils.EMPTY &&
						resultMap.get("message").split(" ").length == 1) {
					String message = resultMap.get("message");
					Long returnTripnumber = Long.parseLong(message);
					// update the claim number returned from SAP, and save the updated caReqHdr to database
					//caReqHdrEntity.setZclmno(caReqHdrEntity.getZclmno());
					//caReqHdr = caReqHdrCommandService.save(caReqHdrEntity, currentUser.isPresent() ? currentUser.get() : null);
					Optional<CaReqHdr> caReqHdr = caReqHdrCommandService.updateReinr(returnTripnumber, caReqHdrEntity, currentUser.isPresent() ? currentUser.get() : null);
					//caReqHdr = caReqHdrCommandService.save(caReqHdrEntity, currentUser.isPresent() ? currentUser.get() : null);
					if (!caReqHdr.isPresent()) {
						throw new CannotPersistException("SAP Result: Cannot update CaReqHdr (EmployeeCost Header) data. Please check your data!");
					}
					
					
					caReqHdrSoapObject = new CaReqHdrSoapObject(null, returnTripnumber, null);
					List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.ECOSTTRIPHDR_FETCH.service(),
							BAPIFunction.ECOSTTRIPHDR_FETCH_URI.service(), caReqHdrSoapObject);
					// synchronize CaReqHdr fields (in different thread) with the fetched fields from SAP
					if (list.get(0).get("zclmno") != null) {
					    caReqHdrEntity = new CaReqHdrMapper(list.get(0)).getCaReqHdr();
					    caReqHdrEntity.setBukrs(caReqHdr.get().getBukrs());
					    caReqHdrEntity.setProcess(caReqHdr.get().getProcess());
					    caReqHdrCommandService.threadUpdate(caReqHdrEntity);
					}
					
					
					for (CaReqDtl caReqDtl : listCaReqDtl) {
						caReqDtl.setReinr(returnTripnumber);
						Optional<CaReqDtl> caReqDtlUpdate = caReqDtlCommandService.save(caReqDtl, currentUser.isPresent() ? currentUser.get() : null);
						if (!caReqDtlUpdate.isPresent()) {
							throw new CannotPersistException("SAP Result: Cannot update CaReqDtl (EmployeeCost Receipts) data. Please check your data!");
						}
					}
					
					Optional<User> submitUser = userQueryService.findByPernr(caReqHdr.get().getPernr());
					if (submitUser.isPresent()) {
						User user = submitUser.get();
						String processed = "CREATED";
				        mailService.sendMail(user.getEmail(), "UPDATE ".
				    		concat(caReqHdrHCMSEntity.get().getEntityName()),
				    		"Dear <b>".concat(user.getEmployee().getSname()).concat("</b><br><br>Your Medical Claim Number <b>".
				    		concat(returnTripnumber.toString()).concat("</b> has been ").
				    		concat(processed).concat(" by your HR Admin or Superior.").concat("<br><br>Thank You.")));
					}
				    
				} else {
					response.setMessage(resultMap.get("errorMessage"));
					throw new CannotPersistException("Cannot post EmployeeCost Transaction to SAP! All data operation will be rollback to initial state.");
				}
				
				 ************************** 
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(CaReqHdrController.class).create(request, result)).withSelfRel());		
		return response;
	}

	*/


	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE, value = "/bulk")
	public APIResponseWrapper<?> createBulkWithItems(@RequestBody  CaTransactionRequestWrapper request[] //@Validated
	, BindingResult result)
	throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {

		ArrayData<CaTransactionResponseWrapper> bunchOfCa = new ArrayData<>();
		APIResponseWrapper<ArrayData<CaTransactionResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfCa);
		
		Double pettyCashLimit = 1000000.00;
		
		if (result.hasErrors()) {
		} else {
			
			Optional<User> currentUser = userQueryService.findByUsername(currentUser());
			if (!currentUser.isPresent()) {
				throw new AuthorizationException(
						"Insufficient create privileges. Please login as User!");						
			}
						
			Optional<HCMSEntity> caReqDtlEntity = hcmsEntityQueryService.findByEntityName(CaReqDtl.class.getSimpleName());
			if (!caReqDtlEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + CaReqDtl.class.getSimpleName() + "' !!!");
			}
			
			Optional<HCMSEntity> caReqHdrEntity = hcmsEntityQueryService.findByEntityName(CaReqHdr.class.getSimpleName());
			if (!caReqHdrEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + CaReqHdr.class.getSimpleName() + "' !!!");
			}
			
			try {
				int size = request.length;
				RequestObjectComparatorContainer<CaReqHdr, CaReqHdrRequestWrapper, String> newObjectContainerHdr; 				
				RequestObjectComparatorContainer<CaReqDtl, CaReqDtlRequestWrapper, String> newObjectContainerItm; 
				 
				List<CaTransactionResponseWrapper> records = new ArrayList<>();				
				
				for(int j = 0; j<size; j++){
				
					CaReqHdrRequestWrapper trxHdr = request[j].getTrxHdr();
					newObjectContainerHdr= caReqHdrRequestBuilderUtil.compareAndReturnUpdatedData(trxHdr, null);
					
					Optional<CaReqHdr> savedEntity = newObjectContainerHdr.getEntity();
					
					Optional<User> requestUser = Optional.empty();
					int currentProcess = WAITING_FOR_HEAD;

					if (savedEntity.isPresent()) {
						currentProcess = savedEntity.get().getProcess().intValue();
						requestUser = userQueryService.findByPernr(savedEntity.get().getPernr());
						if (!requestUser.isPresent()) {
							throw new AuthorizationException(
									"Invalid SN ".concat(savedEntity.get().getPernr().toString()).concat(" no User found!"));					
						}
						
						//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
						savedEntity.get().setPaymentStatus("N");
					    // Set Employee's Main Bank Detail
						Page<IT0009> page = it0009QueryService.findByPernrAndStatus(savedEntity.get().getPernr().longValue(),
								DocumentStatus.PUBLISHED.toString(), 1);
						if (page.hasContent() && page.getContent().get(0).getSubty().getId().getSubty().equals("0")) {
							IT0009 mainBank = page.getContent().get(0);
							savedEntity.get().setBankName(mainBank.getBankl() != null ?
									StringUtils.defaultString(mainBank.getBankl().getBanka()) : StringUtils.EMPTY);
							savedEntity.get().setBankAccount(StringUtils.defaultString(mainBank.getBankn()));
						}

						Optional<Role> headRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(AccessRole.HEAD_GROUP));
						Optional<Role> directorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(AccessRole.DIRECTOR_GROUP));
						
						// set default process to WAITING_FOR_HEAD (process=1)
						//savedEntity.get().setProcess(WAITING_FOR_HEAD);
						if (!savedEntity.get().getAdvanceCode().equals(CaAdvanceCode.TA.toString()) &&
								!savedEntity.get().getAdvanceCode().equals(CaAdvanceCode.RE.toString())) {
							if (savedEntity.get().getAmount().compareTo(pettyCashLimit) > 0) {
								savedEntity.get().setAdvanceCode(CaAdvanceCode.BT.toString());
							}
						}
						
						// request by Director, set approval to Administrator 
						if (requestUser.get().getRoles().contains(directorRole.get())) {
							if (savedEntity.get().getAdvanceCode().equals(CaAdvanceCode.TA.toString())) {
								savedEntity.get().setProcess(WAITING_FOR_GA);
							} else {
								savedEntity.get().setProcess(WAITING_FOR_ACCOUNTING);
							}
						} else
						// request by Head, set approval to Director 
						if (requestUser.get().getRoles().contains(headRole.get())) {
							// find Director user
							Optional<User> directorUser = userQueryService.findByPernrAndRelationRole(savedEntity.get().getPernr(), AccessRole.DIRECTOR_GROUP);
							if (directorUser.isPresent())
								savedEntity.get().setApproved2By(directorUser.get());
							savedEntity.get().setProcess(WAITING_FOR_DIRECTOR);
						} else {
							// find Head user
							Optional<User> headUser = userQueryService.findByPernrAndRelationRole(savedEntity.get().getPernr(), AccessRole.HEAD_GROUP);
							if (headUser.isPresent())
								savedEntity.get().setApproved1By(headUser.get());
						}
						
						//newObjectContainerHdr.getEntity().get().setCreatedBy(currentUser.get());
						//newObjectContainerHdr.getEntity().get().setCreatedAt(new Date());
						
						savedEntity = caReqHdrCommandService.save(newObjectContainerHdr.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						if (savedEntity.isPresent()) {
						    caReqHdrRequestBuilderUtil.saveAttachment(savedEntity.get(), trxHdr);
						}
					}		

					if (!savedEntity.isPresent()) {
						throw new CannotPersistException("Cannot create CaReqHdr data. Please check your data!");
					}

					StringBuilder key = new StringBuilder("req_no=" + savedEntity.get().getReqNo());
					key.append(",pernr=" + savedEntity.get().getPernr());
					key.append(",ac=" + savedEntity.get().getAdvanceCode());
					
					EventLog eventLog = new EventLog(caReqHdrEntity.get(), key.toString(), OperationType.CREATE, 
							(newObjectContainerHdr.getEventData().isPresent() ? newObjectContainerHdr.getEventData().get() : ""));
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());

					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}
					
					sendNotification(requestUser.get(), currentProcess, savedEntity.get(), response, eventLog, false);

					short itmno = 1; 
					
					CaReqDtl savedEntityItems[] = new CaReqDtl[request[j].getTrxItm().length];
					int itemSize = request[j].getTrxItm().length;
					
					for(int i = 0; i < itemSize; i++){
						
						request[j].getTrxItm()[i].setReqNo(savedEntity.get().getReqNo());
						request[j].getTrxItm()[i].setItemNo(itmno);
						newObjectContainerItm = caReqDtlRequestBuilderUtil.compareAndReturnUpdatedData(request[j].getTrxItm()[i], null);

						Optional<CaReqDtl> savedEntityItm = newObjectContainerItm.getEntity();

						if (newObjectContainerItm.getEntity().isPresent()) {
							//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
							savedEntityItm = caReqDtlCommandService.save(newObjectContainerItm.getEntity().get(),
									currentUser.isPresent() ? currentUser.get() : null);
						}		

						if (!savedEntityItm.isPresent()) {
							throw new CannotPersistException("Cannot create CaReqDtl data. Please check your data!");
						}

						StringBuilder keyItm = new StringBuilder("req_no=" + savedEntityItm.get().getId().getReqNo());
						keyItm.append(",item_no=" + savedEntityItm.get().getId().getItemNo());

						EventLog eventLogItm = new EventLog(caReqDtlEntity.get(), keyItm.toString(), OperationType.CREATE, 
								(newObjectContainerItm.getEventData().isPresent() ? newObjectContainerItm.getEventData().get() : ""));
						Optional<EventLog> savedEventLogItm = eventLogCommandService.save(eventLogItm, currentUser.get());

						if (!savedEventLogItm.isPresent()) {
							throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
						}
						
						savedEntityItems[i] = savedEntityItm.get();

						itmno++;
						/*
						// if not set, then set form_type as referred from item's sub form type
						if (savedEntity.get().getForm() == 0 && savedEntityItm.get().getSubt706b1() != null) {
							savedEntity.get().setForm(savedEntityItm.get().getSubt706b1().getSubForm());
						}
						*/
					}
					
					records.add(new CaTransactionResponseWrapper(savedEntity.get(), savedEntityItems));
					
					
					
					/* Post to MuleSoft */
					/*
					CaReqHdr savedCaReqHdr = savedEntity.get();
					ArrayList<CaReqDtl> listCaReqDtl = (ArrayList<CaReqDtl>) caReqDtlQueryService.findByTripnumber(savedCaReqHdr.getId().getReinr()).
							stream().collect(Collectors.toList());
					
					CaReqHdrSoapObject caReqHdrSoapObject = new CaReqHdrSoapObject(savedCaReqHdr);
					List<CaReqDtlSoapObject> caReqDtlSoapObjects = new ArrayList<CaReqDtlSoapObject>();
					
					for (int i=0; i<listCaReqDtl.size(); i++) {
						caReqDtlSoapObjects.add(new CaReqDtlSoapObject(listCaReqDtl.get(i)));
					}
					caReqHdrSoapObject.setTripReceipts(caReqDtlSoapObjects);

					HashMap<String, String> resultMap = (HashMap<String, String>) muleConsumerService.
							postToSAPSync(BAPIFunction.ECOSTTRIPHDR_CREATE.service(), BAPIFunction.ECOSTTRIPHDR_CREATE_URI.service(), caReqHdrSoapObject);
					if (resultMap.get("errorCode") == StringUtils.EMPTY && resultMap.get("tripnumber") != StringUtils.EMPTY &&
							resultMap.get("message").split(" ").length == 1) {
						String message = resultMap.get("message");
						Long returnTripnumber = Long.parseLong(message);
						// update the trip number returned from SAP, and save the updated caReqHdr to database
						Optional<CaReqHdr> caReqHdr = caReqHdrCommandService.updateReinr(returnTripnumber, savedCaReqHdr,
								currentUser.isPresent() ? currentUser.get() : null);
						if (!caReqHdr.isPresent()) {
							throw new CannotPersistException("SAP Result: Cannot update CaReqHdr (EmployeeCost Header) data. Please check your data!");
						}
						*/
						/*
						caReqHdrSoapObject = new CaReqHdrSoapObject(null, returnTripnumber, null);
						List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.ECOSTTRIPHDR_FETCH.service(),
								BAPIFunction.ECOSTTRIPHDR_FETCH_URI.service(), caReqHdrSoapObject);
						// synchronize CaReqHdr fields (in different thread) with the fetched fields from SAP
						if (list.get(0).get("reinr") != null) {
						    caReqHdrEntity = new CaReqHdrMapper(list.get(0)).getCaReqHdr();
						    caReqHdrEntity.setBukrs(caReqHdr.get().getBukrs());
						    caReqHdrEntity.setProcess(caReqHdr.get().getProcess());
						    caReqHdrCommandService.threadUpdate(caReqHdrEntity);
						}
						*/
					
					/*
						for (CaReqDtl caReqDtl : listCaReqDtl) {
							Optional<CaReqDtl> caReqDtlUpdate = caReqDtlCommandService.updateTripnumber(
									returnTripnumber, caReqDtl, currentUser.isPresent() ? currentUser.get() : null);
							if (!caReqDtlUpdate.isPresent()) {
								throw new CannotPersistException("SAP Result: Cannot update CaReqDtl (EmployeeCost Receipts) data. Please check your data!");
							}
						}
						
						Optional<User> submitUser = userQueryService.findByPernr(caReqHdr.get().getId().getPernr());
						if (submitUser.isPresent()) {
							User user = submitUser.get();
							String processed = "CREATED";
					        mailService.sendMail(user.getEmail(), "UPDATE ".
					    		concat(caReqHdrEntity.get().getEntityName()),
					    		"Dear <b>".concat(user.getEmployee().getSname()).concat("</b><br><br>Your Expense Trip Number <b>".
					    		concat(returnTripnumber.toString()).concat("</b> has been ").
					    		concat(processed).concat(" by your HR Admin or Superior.").concat("<br><br>Thank You.")));
						}
					    
					} else {
						response.setMessage(resultMap.get("errorMessage"));
						throw new CannotPersistException("Cannot post EmployeeCost Transaction to SAP! All data operation will be rollback to initial state.");
					}
					
					*/
					
					/* ************************** */
					
					
				}
				bunchOfCa.setItems(records.toArray(new CaTransactionResponseWrapper[records.size()]));
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(CaReqHdrController.class).createBulkWithItems(request, result)).withSelfRel());		
		return response;
	}

	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated CaReqHdrRequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			Double headOnlyApproveLimit = 10000000.00;
			
			CaReqHdrResponseWrapper responseWrapper = new CaReqHdrResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<HCMSEntity> caReqHdrHCMSEntity = hcmsEntityQueryService.findByEntityName(CaReqHdr.class.getSimpleName());
					if (!caReqHdrHCMSEntity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + CaReqHdr.class.getSimpleName() + "' !!!");
					}
					
					Optional<CaReqHdr> caReqHdr = caReqHdrQueryService.findById(Optional.ofNullable(request.getReqNo()));
					if (!caReqHdr.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
					
					String currentReason = caReqHdr.get().getReason();
					boolean workflowProcess = !caReqHdr.get().getProcess().equals(request.getProcess());
				
					RequestObjectComparatorContainer<CaReqHdr, CaReqHdrRequestWrapper, String> updatedContainer =
							caReqHdrRequestBuilderUtil.compareAndReturnUpdatedData(request, caReqHdr.get());
					
					int currentProcess = caReqHdr.get().getProcess().intValue();

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
					
					// if process = 1 (RELEASED by user)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							updatedContainer.getEntity().get().getProcess().intValue() == WAITING_FOR_HEAD) {
						//updatedContainer.getEntity().get().setCreatedBy(currentUser.get());
						//updatedContainer.getEntity().get().setCreatedAt(new Date());
						caReqHdr = caReqHdrCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						if (!caReqHdr.isPresent()) {
							throw new CannotPersistException("Cannot update CaReqHdr (Cash Advance Request Header) data. Please check your data!");
						}
					} else

					// if process = 2 (RELEASED by Head)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							updatedContainer.getEntity().get().getProcess().intValue() == WAITING_FOR_DIRECTOR) {
						
						Optional<Role> headRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(AccessRole.HEAD_GROUP));
						if (!currentUser.get().getRoles().contains(headRole.get())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Please login as 'HEAD'!");
						}
						// find Head user
						Optional<User> headUser = userQueryService.findByPernrAndRelationRole(
								updatedContainer.getEntity().get().getPernr(), AccessRole.HEAD_GROUP);
						if (!headUser.isPresent() || !currentUser.get().equals(headUser.get())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Can't approve request of employee whose not your sub-ordinate!");
						}
						
						updatedContainer.getEntity().get().setApproved1By(currentUser.get());
						updatedContainer.getEntity().get().setApproved1At(new Date());
						
						// amount <= 10,000,000 then skip to next approval
						if (updatedContainer.getEntity().get().getAmount().compareTo(headOnlyApproveLimit) <= 0) {
							updatedContainer.getEntity().get().setProcess(WAITING_FOR_GA);
							// Not Travel Advance, skip to process=5 (Waiting for Accounting)
							if (!CaAdvanceCode.TA.toString().equals(updatedContainer.getEntity().get().getAdvanceCode())) {
								updatedContainer.getEntity().get().setProcess(WAITING_FOR_ACCOUNTING);
							}
						} else {
							// find Director user
							Optional<User> directorUser = userQueryService.findByPernrAndRelationRole(
									updatedContainer.getEntity().get().getPernr(), AccessRole.DIRECTOR_GROUP);
							if (directorUser.isPresent()) {
								updatedContainer.getEntity().get().setApproved2By(directorUser.get());
							}
						}
												
						caReqHdr = caReqHdrCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						if (!caReqHdr.isPresent()) {
							throw new CannotPersistException("Cannot update CaReqHdr (Cash Advance Request Header) data. Please check your data!");
						}
						
					} else

					// if process = 3 (RELEASED by Director)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							updatedContainer.getEntity().get().getProcess().intValue() == WAITING_FOR_GA) {
						
						Optional<Role> directorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(AccessRole.DIRECTOR_GROUP));
						if (!currentUser.get().getRoles().contains(directorRole.get())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Please login as 'DIRECTOR'!");
						}
						// find Director user
						Optional<User> directorUser = userQueryService.findByPernrAndRelationRole(
								updatedContainer.getEntity().get().getPernr(), AccessRole.DIRECTOR_GROUP);
						if (!directorUser.isPresent() || !currentUser.get().equals(directorUser.get())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Can't approve request of employee whose not your sub-ordinate!");
						}
						
						updatedContainer.getEntity().get().setApproved2By(currentUser.get());
						updatedContainer.getEntity().get().setApproved2At(new Date());
						
						// Not Travel Advance, skip to process=5 (Waiting for Accounting)
						if (!CaAdvanceCode.TA.toString().equals(updatedContainer.getEntity().get().getAdvanceCode())) {
							updatedContainer.getEntity().get().setProcess(WAITING_FOR_ACCOUNTING);
						}
						
						caReqHdr = caReqHdrCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						if (!caReqHdr.isPresent()) {
							throw new CannotPersistException("Cannot update CaReqHdr (Cash Advance Request Header) data. Please check your data!");
						}
						
					} else
					
					// if process = 4 (RELEASED by GA Admin)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							updatedContainer.getEntity().get().getProcess().intValue() == WAITING_FOR_HR) {
						
						Optional<Role> gaRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(AccessRole.GA_GROUP));
						if (!currentUser.get().getRoles().contains(gaRole.get())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Please login as 'GA Admin'!");
						}
						
						if (!currentUser.get().getEmployee().getT500p().getBukrs().getBukrs().equals(
								updatedContainer.getEntity().get().getBukrs().getBukrs())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Can't approve request of other company's employee!");
						}
						
						updatedContainer.getEntity().get().setApproved3By(currentUser.get());
						updatedContainer.getEntity().get().setApproved3At(new Date());
						
						caReqHdr = caReqHdrCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						if (!caReqHdr.isPresent()) {
							throw new CannotPersistException("Cannot update CaReqHdr (Cash Advance Request Header) data. Please check your data!");
						}
						
					} else
					
					// if process = 5 (RELEASED by HR Admin)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							updatedContainer.getEntity().get().getProcess().intValue() == WAITING_FOR_ACCOUNTING) {
						
						Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(AccessRole.ADMIN_GROUP));
						if (!currentUser.get().getRoles().contains(adminRole.get())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Please login as 'HR Admin'!");
						}
						
						if (!currentUser.get().getEmployee().getT500p().getBukrs().getBukrs().equals(
								updatedContainer.getEntity().get().getBukrs().getBukrs())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Can't approve request of other company's employee!");
						}
						
						updatedContainer.getEntity().get().setApproved4By(currentUser.get());
						updatedContainer.getEntity().get().setApproved4At(new Date());
						
						caReqHdr = caReqHdrCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						if (!caReqHdr.isPresent()) {
							throw new CannotPersistException("Cannot update CaReqHdr (Cash Advance Request Header) data. Please check your data!");
						}
						
					} else
					
					// if process = 6 (RELEASED by Accounting)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							updatedContainer.getEntity().get().getProcess().intValue() == WAITING_FOR_TREASURY) {
						
						Optional<Role> accRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(AccessRole.ACCOUNTING_GROUP));
						if (!currentUser.get().getRoles().contains(accRole.get())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Please login as 'ACCOUNTING'!");
						}
						
						if (!currentUser.get().getEmployee().getT500p().getBukrs().getBukrs().equals(
								updatedContainer.getEntity().get().getBukrs().getBukrs())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Can't approve request of other company's employee!");
						}
						
						updatedContainer.getEntity().get().setApprovedAccountingBy(currentUser.get());
						updatedContainer.getEntity().get().setApprovedAccountingAt(new Date());
						
						caReqHdr = caReqHdrCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						if (!caReqHdr.isPresent()) {
							throw new CannotPersistException("Cannot update CaReqHdr (Cash Advance Request Header) data. Please check your data!");
						}
						
					} else
					
					// if process = 0 (REJECTED, REQUEST FOR EDIT)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							updatedContainer.getEntity().get().getProcess().intValue() == 0) {
						//updatedContainer.getEntity().get().setSeqnr(caReqHdr.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated
						caReqHdr = caReqHdrCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						if (!caReqHdr.isPresent()) {
							throw new CannotPersistException("Cannot update CaReqHdr (Cash Advance Request Header) data. Please check your data!");
						}
					}	
					
					StringBuilder key = new StringBuilder("req_no=" + caReqHdr.get().getReqNo());
					key.append(",pernr=" + caReqHdr.get().getPernr());
					key.append(",ac=" + caReqHdr.get().getAdvanceCode());
					
					EventLog eventLog = new EventLog(caReqHdrHCMSEntity.get(), key.toString(), OperationType.UPDATE, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					eventLog.setMailNotification(false);
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					
					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}
				
					responseWrapper = new CaReqHdrResponseWrapper(caReqHdr.get());
					response = new APIResponseWrapper<>(responseWrapper);
					
					String sapDocNumber = "";
					
					// if process = 7 (APPROVED by Treasury)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							(updatedContainer.getEntity().get().getProcess().intValue() == WORKFLOW_COMPLETED ||
							updatedContainer.getEntity().get().getProcess().intValue() == WORKFLOW_COMPLETED_PAID)) {
						
						Optional<Role> treasuryRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(AccessRole.TREASURY_GROUP));
						if (!currentUser.get().getRoles().contains(treasuryRole.get())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Please login as 'TREASURY'!");
						}
						
						if (!currentUser.get().getEmployee().getT500p().getBukrs().getBukrs().equals(
								updatedContainer.getEntity().get().getBukrs().getBukrs())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Can't approve request of other company's employee!");
						}
						
						//updatedContainer.getEntity().get().setApprovedTreasuryBy(currentUser.get());
						//updatedContainer.getEntity().get().setApprovedTreasuryAt(new Date());
						
						CaReqHdr caReqHdrEntity = caReqHdr.get();
						
						// sync / post to mulesoft only if last sync / post was 12 minutes past
						if (caReqHdr.get().getLastSync() == null ||
							(new Date().getTime() - caReqHdr.get().getLastSync().getTime()) / (60 * 1000) >= 12) {
							
							// set current time to lock sync / post to mulesoft
							Date lastSync = caReqHdrEntity.getLastSync() == null ? new Date(0) :
								new Date(caReqHdrEntity.getLastSync().getTime()-600000l);
							caReqHdrEntity.setLastSync(new Date());						
							
							Optional<User> createUser = Optional.ofNullable(updatedContainer.getEntity().get().getCreatedBy());
							
							/* Post to MuleSoft */
							
							//CaReqHdr caReqHdrEntity = caReqHdr.get();
							ArrayList<CaReqDtl> listCaReqDtl = (ArrayList<CaReqDtl>) caReqDtlQueryService.findByReqNo(caReqHdrEntity.getReqNo()).
									stream().collect(Collectors.toList());
							
							// /* TO BE DEVELOPED
							
							CaReqHdrSoapObject caReqHdrSoapObject = new CaReqHdrSoapObject(caReqHdrEntity);

							/*
							List<CaReqDtlSoapObject> caReqDtlSoapObjects = new ArrayList<CaReqDtlSoapObject>();
							
							for (int i=0; i<listCaReqDtl.size(); i++) {
								caReqDtlSoapObjects.add(new CaReqDtlSoapObject(listCaReqDtl.get(i)));
							}
							caReqHdrSoapObject.setTripReceipts(caReqDtlSoapObjects);
							
							// begin of summary per expense type
							List<CaReqDtlSoapObject> caReqDtlSoapObjectsSum = new ArrayList<CaReqDtlSoapObject>();
							for (CaReqDtlSoapObject caReqDtlSoapObject : caReqDtlSoapObjects) {
								boolean exist = false;
								for (CaReqDtlSoapObject caReqDtlSoapObjectSum : caReqDtlSoapObjectsSum) {
									if (caReqDtlSoapObject.getExpType().equals(caReqDtlSoapObjectSum.getExpType())) {
										exist = true;
										Double recAmount = Double.valueOf(caReqDtlSoapObject.getRecAmount());
										Double sumAmount = Double.valueOf(caReqDtlSoapObjectSum.getRecAmount());
										caReqDtlSoapObjectSum.setRecAmount(String.valueOf(sumAmount.intValue() + recAmount.intValue()));
										break;
									}
								}
								if (!exist) {
									caReqDtlSoapObjectsSum.add(caReqDtlSoapObject);
								}
							}
							caReqHdrSoapObject.setTripReceipts(caReqDtlSoapObjectsSum);
							// end of summary per expense type
							*/
							
							String muleService = BAPIFunction.CAREQHDR_PETTY_CASH_CREATE.service();
							String uri = BAPIFunction.CAREQHDR_PETTY_CASH_CREATE_URI.service();
							
							if (CaAdvanceCode.BT.toString().equals(caReqHdrEntity.getAdvanceCode())) {
								muleService = BAPIFunction.CAREQHDR_BANK_TRANSFER_CREATE.service();
								uri = BAPIFunction.CAREQHDR_BANK_TRANSFER_CREATE_URI.service();
							} else if (CaAdvanceCode.TA.toString().equals(caReqHdrEntity.getAdvanceCode())) {
								muleService = BAPIFunction.CAREQHDR_TRAVEL_CREATE.service();
								uri = BAPIFunction.CAREQHDR_TRAVEL_CREATE_URI.service();
							} else if (CaAdvanceCode.RE.toString().equals(caReqHdrEntity.getAdvanceCode())) {
								muleService = BAPIFunction.CAREQHDR_REIMBURSEMENT_CREATE.service();
								uri = BAPIFunction.CAREQHDR_REIMBURSEMENT_CREATE_URI.service();
							}
							
							if (updatedContainer.getEntity().get().getProcess().intValue() == WORKFLOW_COMPLETED_PAID) {
								muleService = BAPIFunction.CAREQHDR_PETTY_CASH_RECEIVE_CREATE.service();
								uri = BAPIFunction.CAREQHDR_PETTY_CASH_RECEIVE_CREATE_URI.service();
							}
							
							CaStlHdr caStlHdr = new CaStlHdr();
							caStlHdr.setReqNo(caReqHdr.get().getReqNo());
							caStlHdr.setBukrs(caReqHdr.get().getBukrs());
							caStlHdr.setPernr(caReqHdr.get().getPernr());
							
							List<CaStlDtl> listCaStlDtl = new ArrayList<CaStlDtl>();
							for (int i=0; i<listCaReqDtl.size(); i++) {
								CSKT cskt = new CSKT();
								cskt.setKokrs("1000");
								cskt.setKostl("992012");
								CaStlDtl caStlDtl = new CaStlDtl();
								caStlDtl.setOrderId(new CaOrder(7211001000l, null, cskt, null, null, null));
								caStlDtl.setPoolBudget(992012);
								caStlDtl.setAmount(listCaReqDtl.get(i).getAmount());
								listCaStlDtl.add(caStlDtl);
							}
							
							HashMap<String, String> resultMap = (CaAdvanceCode.RE.toString().equals(caReqHdrEntity.getAdvanceCode())) ?
									postMulesoft(muleService, uri, caStlHdr, listCaStlDtl) :
									(HashMap<String, String>) muleConsumerService.postToSAPSync(muleService, uri, caReqHdrSoapObject);
							
							if (resultMap.get("errorCode") == StringUtils.EMPTY && resultMap.get("belnr") != StringUtils.EMPTY &&
									resultMap.get("message").split(" ").length == 8) {
								
								caReqHdrEntity.setApprovedTreasuryBy(currentUser.get());
								caReqHdrEntity.setApprovedTreasuryAt(new Date());
								caReqHdrEntity.setLastSync(lastSync);
								
								String accountingDocno = resultMap.get("belnr");
								if (updatedContainer.getEntity().get().getProcess().intValue() == WORKFLOW_COMPLETED_PAID) {
									caReqHdrEntity.setDocno2(accountingDocno);
									caReqHdrEntity.setPaymentStatus2("P");
									caReqHdrEntity.setPaymentDate(new Date());
								} else {
									caReqHdrEntity.setPaymentStatus("N");
									caReqHdrEntity.setDocno(accountingDocno);
								}
								
								// save the updated caReqHdr to database
								caReqHdr = caReqHdrCommandService.save(caReqHdrEntity,
										currentUser.isPresent() ? currentUser.get() : null);
								if (!caReqHdr.isPresent()) {
									throw new CannotPersistException("Cannot update CaReqHdr (Cash Advance Request Header) data. Please check your data!");
								}
								
								responseWrapper = new CaReqHdrResponseWrapper(caReqHdrEntity);
								response = new APIResponseWrapper<>(responseWrapper);
								response.setMessage(resultMap.get("message"));
							    
							} else {
								caReqHdrEntity.setProcess(updatedContainer.getEntity().get().getProcess().intValue()-1);
								//caReqHdrEntity.setProcess(WAITING_FOR_TREASURY);
								caReqHdrEntity.setReason(currentReason);
								responseWrapper = new CaReqHdrResponseWrapper(caReqHdrEntity);
								response = new APIResponseWrapper<>(responseWrapper);
								//response.setMessage("Cannot post Medical Transaction to SAP " + resultMap.get("errorMessage") +
										//". Data has been saved for Admin to Update/Re-Post to SAP");
								response.setMessage("Error Message: " + StringUtils.defaultString(resultMap.get("errorMessage"),
										StringUtils.defaultString(resultMap.get("message"), "None")));
								System.out.println("MULESOFT FAILED on Cash Advance Update - ".concat(response.getMessage()));
								String statusMsg = response.getMessage().split(":")[1];
								int maxLength = statusMsg.length() > 25 ? 25 : statusMsg.length();
								//savedEventLog.get().setStatus((response.getMessage().split(":")[1]).substring(0, 25)); 
								savedEventLog.get().setStatus(statusMsg.substring(0, maxLength));
								//throw new CannotPersistException("Cannot post Medical Transaction to SAP! All data operation will be rollback to initial state.");
							}
							
							// END OF TO BE DEVOLEPED */
							
						} else {
							  caReqHdr.get().setProcess(WAITING_FOR_TREASURY);
							  caReqHdr.get().setReason(currentReason);
							  responseWrapper = new CaReqHdrResponseWrapper(caReqHdr.get());
							  response = new APIResponseWrapper<>(responseWrapper);
							  response.setMessage("Locked: Current transaction still in process HCMS to SAP. Please wait a few minutes then try again.");
							  savedEventLog.get().setStatus("Locked");
							  System.out.println("FAILED On Cash Advance Update - ".concat(response.getMessage()));
						}
					}
					
					Optional<User> submitUser = userQueryService.findByPernr(caReqHdr.get().getPernr());
					if (submitUser.isPresent()) {
						sendNotification(submitUser.get(), currentProcess, caReqHdr.get(), response, eventLog, workflowProcess);
					}
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(CaReqHdrController.class).edit(request, result)).withSelfRel());
		return response;
	}
	
	/*
	******************************************************************************************
	TO BE DEVELOPED - BEGIN HERE
	******************************************************************************************
	
	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE, value = "/bulk")
	public APIResponseWrapper<?> editBulkWithItems(@RequestBody  CaTransactionRequestWrapper request[] //@Validated
	, BindingResult result)
	throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {

		ArrayData<CaTransactionResponseWrapper> bunchOfEcost = new ArrayData<>();
		APIResponseWrapper<ArrayData<CaTransactionResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfEcost);
		
		if (result.hasErrors()) {
		} else {
						
			Optional<HCMSEntity> caReqDtlEntity = hcmsEntityQueryService.findByEntityName(CaReqDtl.class.getSimpleName());
			if (!caReqDtlEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + CaReqDtl.class.getSimpleName() + "' !!!");
			}
			
			Optional<HCMSEntity> caReqHdrEntity = hcmsEntityQueryService.findByEntityName(CaReqHdr.class.getSimpleName());
			if (!caReqHdrEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + CaReqHdr.class.getSimpleName() + "' !!!");
			}
			
			boolean headerDataChanged = false;
			
			edit: {
			try {
				int size = request.length;
				RequestObjectComparatorContainer<CaReqHdr, CaReqHdrRequestWrapper, String> newObjectContainerHdr; 				
				RequestObjectComparatorContainer<CaReqDtl, CaReqDtlRequestWrapper, String> newObjectContainerItm;
				RequestObjectComparatorContainer<CaReqDtl, CaReqDtlRequestWrapper, String> updatedContainerItm;
				 
				List<CaTransactionResponseWrapper> records = new ArrayList<>();
				
				for(int j = 0; j<size; j++){
					
					boolean isDataChanged = false;
					
					Optional<CaReqHdr> caReqHdr = caReqHdrQueryService.findOneByCompositeKey(
							request[j].getTrxHdr().getHdvrs(),
							request[j].getTrxHdr().getPernr(),
							request[j].getTrxHdr().getReinr());
					if (!caReqHdr.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						continue;
					}
					
					String currentProcess = caReqHdr.get().getProcess();
				
					RequestObjectComparatorContainer<CaReqHdr, CaReqHdrRequestWrapper, String> updatedContainer =
							caReqHdrRequestBuilderUtil.compareAndReturnUpdatedData(request[j].getTrxHdr(), caReqHdr.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						headerDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					CaReqHdrRequestWrapper TrxHdr = request[j].getTrxHdr();
					newObjectContainerHdr= caReqHdrRequestBuilderUtil.compareAndReturnUpdatedData(TrxHdr, null);						
					
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
					if (!currentUser.isPresent()) {
						throw new AuthorizationException(
								"Insufficient privileges. Please login!");						
					}
					
					Optional<CaReqHdr> savedEntity = newObjectContainerHdr.getEntity();

					if (newObjectContainerHdr.getEntity().isPresent()) {
						//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
						savedEntity = caReqHdrCommandService.save(newObjectContainerHdr.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}		

					if (!savedEntity.isPresent()) {
						throw new CannotPersistException("Cannot create CaReqDtl data. Please check your data!");
					}

					StringBuilder key = new StringBuilder("hdvrs=" + savedEntity.get().getId().getHdvrs());
					key.append(",pernr=" + savedEntity.get().getId().getPernr());
					key.append(",reinr=" + savedEntity.get().getId().getReinr());
					
					EventLog eventLog = new EventLog(caReqHdrEntity.get(), key.toString(), OperationType.CREATE, 
							(newObjectContainerHdr.getEventData().isPresent() ? newObjectContainerHdr.getEventData().get() : ""));
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());

					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}

					
					int itemSize = request[j].getTrxItm().length;	

					ArrayList<CaReqDtl> listCaReqDtl = (ArrayList<CaReqDtl>) caReqDtlQueryService.findByTripnumber(caReqHdr.get().getReinr()).
							stream().collect(Collectors.toList());
					
					if (listCaReqDtl.size() != itemSize) {
						isDataChanged = true;
					} else {
						for(int i = 0; i < itemSize; i++){
							Optional<CaReqDtl> caReqDtl = caReqDtlQueryService.findOneByCompositeKey(
									request[j].getTrxItm()[i].getMandt(),
									request[j].getTrxItm()[i].getPernr(),
									request[j].getTrxItm()[i].getReinr(),
									request[j].getTrxItm()[i].getPerio(),
									request[j].getTrxItm()[i].getReceiptno());
							if (!caReqDtl.isPresent()) {
								isDataChanged = true;
								break;
							} else {
								updatedContainerItm = caReqDtlRequestBuilderUtil.compareAndReturnUpdatedData(request[j].getTrxItm()[i], caReqDtl.get());
								if (updatedContainerItm.getRequestPayload().isPresent()) {
									isDataChanged = updatedContainerItm.getRequestPayload().get().isDataChanged();
									if (isDataChanged)
										break;
								}
							}
						}
					}

					if (isDataChanged || headerDataChanged) {
						if (updatedContainer.getEntity().isPresent()) {
							
							if (isDataChanged && !listCaReqDtl.isEmpty()) {
								caReqDtlCommandService.deleteList(listCaReqDtl);
							}
							
							if (headerDataChanged) {
								caReqHdr = caReqHdrCommandService.save(updatedContainer.getEntity().get(), currentUser.get());						
								if (!caReqHdr.isPresent()) {
									throw new CannotPersistException("Cannot update CaReqHdr data. Please check your data!");
								}
								StringBuilder key = new StringBuilder("hdvrs=" + caReqHdr.get().getId().getHdvrs());
								key.append(",pernr=" + caReqHdr.get().getId().getPernr());
								key.append(",reinr=" + caReqHdr.get().getId().getReinr());								
								EventLog eventLog = new EventLog(caReqHdrEntity.get(), key.toString(), OperationType.UPDATE, 
										(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
								Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
								if (!savedEventLog.isPresent()) {
									throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
								}
							}
							
						}
						//anyDataChanged = true;
					} else {
						if (j == (size-1)) {
							if (!headerDataChanged) {
							    response.setMessage("No data changed. No need to perform the update.");
							    break edit;
							}
						} else {
							continue;
						}
					}
					
					int itmno = 1;
					
					CaReqDtl savedEntityItems[] = new CaReqDtl[request[j].getTrxItm().length];
					
					
					for(int i = 0; i < itemSize; i++){
						
						OperationType operationType = OperationType.CREATE;
						
						Optional<CaReqDtl> caReqDtl = caReqDtlQueryService.findOneByCompositeKey(
								request[j].getTrxItm()[i].getMandt(),
								request[j].getTrxItm()[i].getPernr(),
								request[j].getTrxItm()[i].getReinr(),
								request[j].getTrxItm()[i].getPerio(),
								request[j].getTrxItm()[i].getReceiptno());
						if (!caReqDtl.isPresent()) {
							request[j].getTrxItm()[i].setReceiptno(String.valueOf(itmno));								
							newObjectContainerItm = caReqDtlRequestBuilderUtil.compareAndReturnUpdatedData(request[j].getTrxItm()[i], null);
						} else {
							operationType = OperationType.UPDATE;
							newObjectContainerItm = caReqDtlRequestBuilderUtil.compareAndReturnUpdatedData(request[j].getTrxItm()[i], caReqDtl.get());
						}
						caReqDtl = newObjectContainerItm.getEntity();
						
						//request[j].getTrxItm()[i].setReceiptno(String.valueOf(itmno));								
						//newObjectContainerItm = caReqDtlRequestBuilderUtil.compareAndReturnUpdatedData(request[j].getTrxItm()[i], null);

						//Optional<CaReqDtl> savedEntityItm = newObjectContainerItm.getEntity();
						Optional<CaReqDtl> savedEntityItm;

						//if (newObjectContainerItm.getEntity().isPresent()) {
							//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
							//savedEntityItm = caReqDtlCommandService.save(newObjectContainerItm.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
							savedEntityItm = caReqDtlCommandService.save(caReqDtl.get(), currentUser.isPresent() ? currentUser.get() : null);
						//}		

						if (!savedEntityItm.isPresent()) {
							throw new CannotPersistException("Cannot create CaReqDtl data. Please check your data!");
						}

						StringBuilder keyItm = new StringBuilder("mandt=" + savedEntityItm.get().getId().getMandt());
						keyItm.append(",prnr=" + savedEntityItm.get().getId().getPernr());
						keyItm.append(",reinr=" + savedEntityItm.get().getId().getReinr());
						keyItm.append(",perio=" + savedEntityItm.get().getId().getPerio());
						keyItm.append(",receiptno=" + savedEntityItm.get().getId().getReceiptno());

						EventLog eventLogItm = new EventLog(caReqDtlEntity.get(), keyItm.toString(), operationType, 
								(newObjectContainerItm.getEventData().isPresent() ? newObjectContainerItm.getEventData().get() : ""));
						Optional<EventLog> savedEventLogItm = eventLogCommandService.save(eventLogItm, currentUser.get());

						if (!savedEventLogItm.isPresent()) {
							throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
						}
						
						savedEntityItems[i] = savedEntityItm.get();

						itmno++;
						
						// if not set, then set form_type as referred from item's sub form type
						if (savedEntity.get().getForm() == 0 && savedEntityItm.get().getSubt706b1() != null) {
							savedEntity.get().setForm(savedEntityItm.get().getSubt706b1().getSubForm());
						}
						
					}
					
					records.add(new CaTransactionResponseWrapper(caReqHdr.get(), savedEntityItems));
					
				}
				bunchOfEcost.setItems(records.toArray(new CaTransactionResponseWrapper[records.size()]));
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
			}
		}
				
		response.add(linkTo(methodOn(CaReqHdrController.class).editBulkWithItems(request, result)).withSelfRel());		
		return response;
	}
	
	******************************************************************************************
	TO BE DEVELOPED - END HERE
	******************************************************************************************
	*/

	
	private void sendNotification(User user, int currentProcess, CaReqHdr caReqHdr, APIResponseWrapper<?> response,
			EventLog eventLog, boolean workflowProcess) {
		
		Optional<User> currentUser;
		try {
			currentUser = userQueryService.findByUsername(currentUser());
		} catch (Exception e) {
			currentUser = Optional.ofNullable(user);
			e.printStackTrace();
		}
		
		String processed = "RELEASED. Your Head or Director will review your submitted data prior to approval.";
		if (currentProcess == WAITING_FOR_HEAD) {
			if (caReqHdr.getProcess().intValue() == WAITING_FOR_GA)
				processed = "RELEASED and waiting for approval by GA.";
			if (caReqHdr.getProcess().intValue() == WAITING_FOR_ACCOUNTING)
				processed = "RELEASED and waiting for approval by Accounting.";
		} else if (currentProcess == WAITING_FOR_DIRECTOR) {
			processed = "APPROVED by your Head and waiting for next approval.";
		} else if (currentProcess == WAITING_FOR_GA) {
			processed = "APPROVED by your Director and waiting for next approval.";
		} else if (currentProcess == WAITING_FOR_HR) {
			processed = "APPROVED by GA Admin and waiting for next approval.";
		} else if (currentProcess == WAITING_FOR_ACCOUNTING) {
			processed = "APPROVED by HR Admin and waiting for next approval.";
		} else if (currentProcess == WAITING_FOR_TREASURY) {
			processed = "APPROVED by Accounting and waiting for next approval.";
		} else if (currentProcess == WORKFLOW_COMPLETED) {
			processed = "APPROVED by Treasury and Ready for Payment.";
			if (caReqHdr.getProcess().intValue() == WAITING_FOR_TREASURY) {
				processed = "FAILED to publish with following message:<br>".concat
						(StringUtils.defaultString(response.getMessage(), "No Message"));
			}
		} else if (currentProcess == WORKFLOW_COMPLETED_PAID) {
			processed = "APPROVED by Treasury and PAID.";
			if (caReqHdr.getProcess().intValue() == WORKFLOW_COMPLETED) {
				processed = "FAILED to paid with following message:<br>".concat
						(StringUtils.defaultString(response.getMessage(), "No Message"));
			}
		} else if (currentProcess == 0) {
			processed = workflowProcess ? "Requested for edit by ".concat(currentUser.get().getEmployee().getSname()).
					concat(" due to following reason :<br>").concat(caReqHdr.getReason())
					: eventLog.getOperationType().equals(OperationType.UPDATE) ?
							"Updated, waiting for your released and submitted required data to your Head or Director."
							: "Created, waiting for your released and submitted required data to your Head or Director.";
		} else if (currentProcess == WORKFLOW_CANCELED) {
			processed = "Cancelled by Treasury due to following reason :<br>".concat(caReqHdr.getReason());
		} else if (currentProcess == WORKFLOW_CANCELED_FAILED) {
			processed = "FAILED to cancel by Treasury with following message:<br>".concat
					(StringUtils.defaultString(response.getMessage(), "No Message"));
		}

		Map<String, String> composeMap = mailService.composeEmail(eventLog, user);
		if (StringUtils.isNotEmpty(composeMap.get("subject"))) {
			String mailBody = composeMap.get("body").replace(mailService.getMailCashAdvanceUpdate(), "has been ".concat(processed));
			mailService.sendMail(user.getEmail(), composeMap.get("subject"), mailBody);
			
			// notification email to Head
	        if (currentProcess == WAITING_FOR_HEAD && caReqHdr.getProcess().intValue() == WAITING_FOR_HEAD) {
	        	mailBody = mailBody.replace(processed, 
		    			"released, waiting for your review and approval.").
		    			replace("Your Request of ", "User Request of ");
	        	String body = mailBody.replace("Dear <b>".concat(user.getEmployee().getSname()),
	    				"Dear <b>".concat(caReqHdr.getApproved1By().getEmployee().getSname()));
	    		mailService.sendMail(caReqHdr.getApproved1By().getEmail(), composeMap.get("subject"), body);
	        }
			
			// notification email to Director
	        if (currentProcess == WAITING_FOR_DIRECTOR || caReqHdr.getProcess().intValue() == WAITING_FOR_DIRECTOR) {
	        	mailBody = mailBody.replace(processed, 
		    			"released, waiting for your review and approval.").
		    			replace("Your Request of ", "User Request of ");
	        	String body = mailBody.replace("Dear <b>".concat(user.getEmployee().getSname()),
	    				"Dear <b>".concat(caReqHdr.getApproved2By().getEmployee().getSname()));
	    		mailService.sendMail(caReqHdr.getApproved2By().getEmail(), composeMap.get("subject"), body);
	        }

			// notification email to all GA Admin
	        if (currentProcess == WAITING_FOR_GA || caReqHdr.getProcess().intValue() == WAITING_FOR_GA) {
			    List<User> gaUsers = new ArrayList<User>();
		    	mailBody = mailBody.replace(processed, 
		    			"released, waiting for your review and approval.").
		    			replace("Your Request of ", "User Request of ");
		    	
		    	Optional<Collection<User>> users = userQueryService.fetchByCompanyAndRole(
			    		user.getEmployee().getT500p().getBukrs().getBukrs(), AccessRole.GA_GROUP);
			    if (users.isPresent()) {
			    	gaUsers = users.get().stream().collect(Collectors.toList());
			    	
			    	for (User gaUser : gaUsers) {
			        	String body = mailBody.replace("Dear <b>".concat(user.getEmployee().getSname()),
			    				"Dear <b>".concat(gaUser.getEmployee().getSname()).concat(" (GA Admin)"));
			    		mailService.sendMail(gaUser.getEmail(), composeMap.get("subject"), body);
			    	}
			    }
	        }

			// notification email to all HR Admin
	        if (currentProcess == WAITING_FOR_HR) {
			    List<User> adminUsers = new ArrayList<User>();
		    	mailBody = mailBody.replace(processed, 
		    			"released, waiting for your review and approval.").
		    			replace("Your Request of ", "User Request of ");
		    	
		    	Optional<Collection<User>> users = userQueryService.fetchAdminByCompany(
			    		user.getEmployee().getT500p().getBukrs().getBukrs());
			    if (users.isPresent()) {
			    	adminUsers = users.get().stream().collect(Collectors.toList());
			    	
			    	for (User adminUser : adminUsers) {
			        	String body = mailBody.replace("Dear <b>".concat(user.getEmployee().getSname()),
			    				"Dear <b>".concat(adminUser.getEmployee().getSname()).concat(" (HR Admin)"));
			    		mailService.sendMail(adminUser.getEmail(), composeMap.get("subject"), body);
			    	}
			    }
	        }

			// notification email to all Accounting
	        if (currentProcess == WAITING_FOR_ACCOUNTING || caReqHdr.getProcess().intValue() == WAITING_FOR_ACCOUNTING) {
			    List<User> accUsers = new ArrayList<User>();
		    	mailBody = mailBody.replace(processed, 
		    			"released, waiting for your review and approval.").
		    			replace("Your Request of ", "User Request of ");
		    	
		    	Optional<Collection<User>> users = userQueryService.fetchByCompanyAndRole(
			    		user.getEmployee().getT500p().getBukrs().getBukrs(), AccessRole.ACCOUNTING_GROUP);
			    if (users.isPresent()) {
			    	accUsers = users.get().stream().collect(Collectors.toList());
			    	
			    	for (User accUser : accUsers) {
			        	String body = mailBody.replace("Dear <b>".concat(user.getEmployee().getSname()),
			    				"Dear <b>".concat(accUser.getEmployee().getSname()).concat(" (Accounting)"));
			    		mailService.sendMail(accUser.getEmail(), composeMap.get("subject"), body);
			    	}
			    }
	        }

			// notification email to all Treasury
	        if (currentProcess == WAITING_FOR_TREASURY) {
			    List<User> treasuryUsers = new ArrayList<User>();
		    	mailBody = mailBody.replace(processed, 
		    			"released, waiting for your review and approval.").
		    			replace("Your Request of ", "User Request of ");
		    	
		    	Optional<Collection<User>> users = userQueryService.fetchByCompanyAndRole(
			    		user.getEmployee().getT500p().getBukrs().getBukrs(), AccessRole.TREASURY_GROUP);
			    if (users.isPresent()) {
			    	treasuryUsers = users.get().stream().collect(Collectors.toList());
			    	
			    	for (User treasuryUser : treasuryUsers) {
			        	String body = mailBody.replace("Dear <b>".concat(user.getEmployee().getSname()),
			    				"Dear <b>".concat(treasuryUser.getEmployee().getSname()).concat(" (Treasury)"));
			    		mailService.sendMail(treasuryUser.getEmail(), composeMap.get("subject"), body);
			    	}
			    }
	        }
	        
	        // ***** read only (workflow) notification to all Treasury
	        List<User> treasuryUsers = new ArrayList<User>();
		    Optional<Collection<User>> users = userQueryService.fetchByCompanyAndRole(
		    		user.getEmployee().getT500p().getBukrs().getBukrs(), "ROLE_TREASURY");
		    if (users.isPresent()) {
		    	treasuryUsers = users.get().stream().collect(Collectors.toList());
		    	mailBody = mailBody.replace("released, waiting for your review and approval.", currentProcess < WAITING_FOR_TREASURY ?
		    			"approved by ".concat(currentUser.get().getEmployee().getSname()).concat(" and waiting for next approval.") :
		    				(caReqHdr.getProcess().intValue() == WAITING_FOR_TREASURY && currentProcess == WORKFLOW_COMPLETED) ?
		    						"approved but ".concat(processed) : currentProcess == WORKFLOW_COMPLETED ?
		    			    	"approved by ".concat(currentUser.get().getEmployee().getSname()).concat(" and PUBLISHED.") : processed).
		    			replace("Your Request of ", "User Request of ");
		    	for (User treasuryUser : treasuryUsers) {
		    		String body = mailBody.replace("Dear <b>".concat(user.getEmployee().getSname()),
		    				"Dear <b>".concat(treasuryUser.getEmployee().getSname()).concat(" (Treasury)"));
		    		mailService.sendMail(treasuryUser.getEmail(), composeMap.get("subject"), body);
		    	}
		    }
		    
		}
		
		/*
        if ((currentProcess.equals("4") && "1".equals(caReqHdr.getProcess())) ||
        		currentProcess.equals("6")) {
			throw new DataViolationException(processed.replace("<br>","\n"));
        }
        */
	}
	
	private HashMap<String, String> postMulesoft(String muleService, String uri, CaStlHdr caStlHdr, List<CaStlDtl> listCaStlDtl) {
		CaStlHdrSoapObject caStlHdrSoapObject = new CaStlHdrSoapObject(caStlHdr);		
		List<CaStlDtlSoapObject> caStlDtlSoapObjects = new ArrayList<CaStlDtlSoapObject>();
		
		for (int i=0; i<listCaStlDtl.size(); i++) {
			caStlDtlSoapObjects.add(new CaStlDtlSoapObject(listCaStlDtl.get(i)));
		}
		caStlHdrSoapObject.setListCaStlDtl(caStlDtlSoapObjects);
		
		HashMap<String, String> resultMap = (HashMap<String, String>) muleConsumerService.postToSAPSync(muleService, uri, caStlHdrSoapObject);
		
		return resultMap;
	}
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}
	
}