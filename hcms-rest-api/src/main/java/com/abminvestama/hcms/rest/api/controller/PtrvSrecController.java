package com.abminvestama.hcms.rest.api.controller;



import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.PtrvSrec;
import com.abminvestama.hcms.core.model.entity.PtrvSrec;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.PtrvSrecCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.PtrvSrecQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.PtrvSrecRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.PtrvSrecRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.PtrvSrecResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.PtrvSrecResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.PtrvSrecRequestBuilderUtil;


/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add createBulk method/td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/ptrv_srec")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class PtrvSrecController extends AbstractResource {
	
	private PtrvSrecCommandService ptrvSrecCommandService;
	private PtrvSrecQueryService ptrvSrecQueryService;
	private PtrvSrecRequestBuilderUtil ptrvSrecRequestBuilderUtil;
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	@Autowired
	void setPtrvSrecCommandService(PtrvSrecCommandService ptrvSrecCommandService) {
		this.ptrvSrecCommandService = ptrvSrecCommandService;
	}
	
	@Autowired
	void setPtrvSrecQueryService(PtrvSrecQueryService ptrvSrecQueryService) {
		this.ptrvSrecQueryService = ptrvSrecQueryService;
	}
	
	@Autowired
	void setPtrvSrecRequestBuilderUtil(PtrvSrecRequestBuilderUtil ptrvSrecRequestBuilderUtil) {
		this.ptrvSrecRequestBuilderUtil = ptrvSrecRequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	
	
	
	
	

	
	
	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE, value = "/single")
	public APIResponseWrapper<?> create(@RequestBody @Validated PtrvSrecRequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> ptrvSrecEntity = hcmsEntityQueryService.findByEntityName(PtrvSrec.class.getSimpleName());
			if (!ptrvSrecEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + PtrvSrec.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<PtrvSrec, PtrvSrecRequestWrapper, String> newObjectContainer 
					= ptrvSrecRequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				
				Optional<PtrvSrec> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
				//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = ptrvSrecCommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create PtrvSrec data. Please check your data!");
				}
				
				
				
				
				StringBuilder key = new StringBuilder("mandt=" + savedEntity.get().getId().getMandt());
				key.append(",pernr=" + savedEntity.get().getId().getPernr());
				key.append(",reinr=" + savedEntity.get().getId().getReinr());
				key.append(",perio=" + savedEntity.get().getId().getPerio());
				key.append(",receiptno=" + savedEntity.get().getId().getReceiptno());
				
				EventLog eventLog = new EventLog(ptrvSrecEntity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				
				PtrvSrecResponseWrapper responseWrapper = new PtrvSrecResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(PtrvSrecController.class).create(request, result)).withSelfRel());		
		return response;
	}


	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> createBulk(@RequestBody @Validated //MedTransactionRequestWrapper request[]
			PtrvSrecRequestWrapper request[], BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		
		
		

		ArrayData<PtrvSrecResponseWrapper> bunchOfPtrvSrec = new ArrayData<>();
		APIResponseWrapper<ArrayData<PtrvSrecResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfPtrvSrec);
		
		//APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
		} else {
						
			Optional<HCMSEntity> ptrvSrecEntity = hcmsEntityQueryService.findByEntityName(PtrvSrec.class.getSimpleName());
			if (!ptrvSrecEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + PtrvSrec.class.getSimpleName() + "' !!!");
			}
			
			try {
				int size = request.length;
				//PtrvSrecRequestWrapper requestPayload[] = new PtrvSrecRequestWrapper [size];
				
				RequestObjectComparatorContainer<PtrvSrec, PtrvSrecRequestWrapper, String> newObjectContainer; 
					//=	new RequestObjectComparatorContainer<PtrvSrec, PtrvSrecRequestWrapper, String> [size];
				 
				

				List<PtrvSrecResponseWrapper> records = new ArrayList<>();
				
				int receiptNo = 1;
				for(int i = 0; i < size; i++){
					
					//auto increment from SAP
					//itmno = lastItmno.substring(0, lastItmno.length()-5)+(Integer.parseInt(lastItmno.substring(lastItmno.length()-5,lastItmno.length()-1))+1);
					
					request[i].setReceiptno(String.valueOf(receiptNo));
					newObjectContainer= ptrvSrecRequestBuilderUtil.compareAndReturnUpdatedData(request[i], null);
					
										
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
					
					if (!currentUser.isPresent()) {
						throw new AuthorizationException(
								"Insufficient create privileges. Please login as Admin!");						
					}				
					
					Optional<PtrvSrec> savedEntity = newObjectContainer.getEntity();
					
					if (newObjectContainer.getEntity().isPresent()) {
					//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
						savedEntity = ptrvSrecCommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}		
					
					if (!savedEntity.isPresent()) {
						throw new CannotPersistException("Cannot create PtrvSrec data. Please check your data!");
					}
					
					StringBuilder key = new StringBuilder("mandt=" + savedEntity.get().getId().getMandt());
					key.append(",prnr=" + savedEntity.get().getId().getPernr());
					key.append(",reinr=" + savedEntity.get().getId().getReinr());
					key.append(",perio=" + savedEntity.get().getId().getPerio());
					key.append(",receiptno=" + savedEntity.get().getId().getReceiptno());
					
					EventLog eventLog = new EventLog(ptrvSrecEntity.get(), key.toString(), OperationType.CREATE, 
							(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					
					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}
					
					records.add(new PtrvSrecResponseWrapper(savedEntity.get()));
					
					receiptNo++;
				}
					

				bunchOfPtrvSrec.setItems(records.toArray(new PtrvSrecResponseWrapper[records.size()]));
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(PtrvSrecController.class).createBulk(request, result)).withSelfRel());		
		return response;
	}
	
	
	
	
	
	
	
	
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}
	
}