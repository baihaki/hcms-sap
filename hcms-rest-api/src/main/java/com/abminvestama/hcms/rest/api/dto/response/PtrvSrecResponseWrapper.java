package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.PtrvSrec;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add fields: startVolume, endVolume, sumVolume</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add fields: expTypeText, subExpTypeText, form</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add fields: morei, subExpType</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 * 
 */
@JsonInclude(Include.NON_NULL)
public class PtrvSrecResponseWrapper extends ResourceSupport {
	
	private String mandt;
	private Long pernr;
	private Long reinr;
	private Integer perio;
	private String receiptno;
	
	private String expType;
	private String expTypeText;
	private String recAmount;
	private String recCurr;
	private String recRate;
	private String locAmount;
	private String locCurr;
	private Date recDate;
	private String shorttxt;
	private String paperReceipt;
	
	private String morei;
	private String subExpType;
	private String subExpTypeText;
	private Short form;
	
	private String startVolume;
	private String endVolume;
	private String sumVolume;
	
	private PtrvSrecResponseWrapper() {}
	
	public PtrvSrecResponseWrapper(PtrvSrec ptrvSrec) {
		if (ptrvSrec == null) {
			new PtrvSrecResponseWrapper();
		} else {
			this.setMandt(ptrvSrec.getId().getMandt())
			.setPernr(ptrvSrec.getId().getPernr())			
			.setReinr(ptrvSrec.getId().getReinr())
			.setPerio(ptrvSrec.getId().getPerio())
			.setReceiptno(ptrvSrec.getId().getReceiptno())
			.setRecAmount(ptrvSrec.getRecAmount() != null ? ptrvSrec.getRecAmount().toString() : "0")
			.setRecCurr(StringUtils.defaultString(ptrvSrec.getRecCurr(), StringUtils.EMPTY))
			.setRecRate(ptrvSrec.getRecRate() != null ? ptrvSrec.getRecRate().toString() : StringUtils.EMPTY)
			.setLocAmount(ptrvSrec.getLocAmount() != null ? ptrvSrec.getLocAmount().toString() : StringUtils.EMPTY)
			.setLocCurr(StringUtils.defaultString(ptrvSrec.getLocCurr(), StringUtils.EMPTY))
			.setRecDate(ptrvSrec.getRecDate())
			.setShorttxt(StringUtils.defaultString(ptrvSrec.getShorttxt(), StringUtils.EMPTY))
			.setPaperReceipt(StringUtils.defaultString(ptrvSrec.getPaperReceipt(), StringUtils.EMPTY))
			//.setExpType(StringUtils.defaultString(ptrvSrec.getExpType(), StringUtils.EMPTY))
			.setMorei(ptrvSrec.getSubt706b1() != null ? StringUtils.defaultString(
					ptrvSrec.getSubt706b1().getId().getMorei(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setExpType(ptrvSrec.getSubt706b1() != null ? StringUtils.defaultString(
					ptrvSrec.getSubt706b1().getId().getSpkzl(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setExpTypeText(ptrvSrec.getSubt706b1() != null && ptrvSrec.getSubt706b1().getSht706b1() != null ? StringUtils.defaultString(
					ptrvSrec.getSubt706b1().getSht706b1().getSptxt(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setSubExpType(ptrvSrec.getSubt706b1() != null ? StringUtils.defaultString(
					ptrvSrec.getSubt706b1().getId().getSubSpkzl(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setSubExpTypeText(ptrvSrec.getSubt706b1() != null ? StringUtils.defaultString(
					ptrvSrec.getSubt706b1().getSubSptxt(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setForm(ptrvSrec.getSubt706b1() != null ? ptrvSrec.getSubt706b1().getSubForm() : 0)
			.setStartVolume(ptrvSrec.getStartVolume() != null ? ptrvSrec.getStartVolume().toString() : StringUtils.EMPTY)
			.setEndVolume(ptrvSrec.getEndVolume() != null ? ptrvSrec.getEndVolume().toString() : StringUtils.EMPTY)
			.setSumVolume(ptrvSrec.getSumVolume() != null ? ptrvSrec.getSumVolume().toString() : StringUtils.EMPTY);
		}
	}
	

	@JsonProperty("mandt")
	public String getMandt() {
		return mandt;
	}
	
	private PtrvSrecResponseWrapper setMandt(String mandt) {
		this.mandt = mandt;
		return this;
	}	
	
	@JsonProperty("pernr")
	public Long getPernr() {
		return pernr;
	}
	
	private PtrvSrecResponseWrapper setPernr(Long pernr) {
		this.pernr = pernr;
		return this;
	}	
	
	@JsonProperty("reinr")
	public Long getReinr() {
		return reinr;
	}
	
	private PtrvSrecResponseWrapper setReinr(Long reinr) {
		this.reinr = reinr;
		return this;
	}
	
	@JsonProperty("perio")
	public Integer getPerio() {
		return perio;
	}
	
	private PtrvSrecResponseWrapper setPerio(Integer perio) {
		this.perio = perio;
		return this;
	}	
	
	@JsonProperty("receiptno")
	public String getReceiptno() {
		return receiptno;
	}
	
	private PtrvSrecResponseWrapper setReceiptno(String receiptno) {
		this.receiptno = receiptno;
		return this;
	}	
	
	@JsonProperty("rec_date")
	public Date getRecDate() {
		return recDate;
	}
	
	private PtrvSrecResponseWrapper setRecDate(Date recDate) {
		this.recDate = recDate;
		return this;
	}
	
	@JsonProperty("exp_type")
	public String getExpType() {
		return expType;
	}
	
	private PtrvSrecResponseWrapper setExpType(String expType) {
		this.expType = expType;
		return this;
	}
	
	@JsonProperty("exp_type_text")
	public String getExpTypeText() {
		return expTypeText;
	}
	
	private PtrvSrecResponseWrapper setExpTypeText(String expTypeText) {
		this.expTypeText = expTypeText;
		return this;
	}	
	
	@JsonProperty("rec_amount")
	public String getRecAmount() {
		return recAmount;
	}
	
	private PtrvSrecResponseWrapper setRecAmount(String recAmount) {
		this.recAmount = recAmount;
		return this;
	}	
	
	@JsonProperty("rec_curr")
	public String getRecCurr() {
		return recCurr;
	}
	
	private PtrvSrecResponseWrapper setRecCurr(String recCurr) {
		this.recCurr = recCurr;
		return this;
	}
	
	@JsonProperty("rec_rate")
	public String getRecRate() {
		return recRate;
	}
	
	private PtrvSrecResponseWrapper setRecRate(String recRate) {
		this.recRate = recRate;
		return this;
	}	
	
	@JsonProperty("loc_amount")
	public String getLocAmount() {
		return locAmount;
	}
	
	private PtrvSrecResponseWrapper setLocAmount(String locAmount) {
		this.locAmount = locAmount;
		return this;
	}

	@JsonProperty("loc_curr")
	public String getLocCurr() {
		return locCurr;
	}
	
	private PtrvSrecResponseWrapper setLocCurr(String locCurr) {
		this.locCurr = locCurr;
		return this;
	}	
	
	@JsonProperty("shorttxt")
	public String getShorttxt() {
		return shorttxt;
	}
	
	private PtrvSrecResponseWrapper setShorttxt(String shorttxt) {
		this.shorttxt = shorttxt;
		return this;
	}	
	
	@JsonProperty("paper_receipt")
	public String getPaperReceipt() {
		return paperReceipt;
	}
	
	private PtrvSrecResponseWrapper setPaperReceipt(String paperReceipt) {
		this.paperReceipt = paperReceipt;
		return this;
	}

	@JsonProperty("morei")
	public String getMorei() {
		return morei;
	}
	
	private PtrvSrecResponseWrapper setMorei(String morei) {
		this.morei = morei;
		return this;
	}

	@JsonProperty("sub_exp_type")
	public String getSubExpType() {
		return subExpType;
	}
	
	private PtrvSrecResponseWrapper setSubExpType(String subExpType) {
		this.subExpType = subExpType;
		return this;
	}

	@JsonProperty("sub_exp_type_text")
	public String getSubExpTypeText() {
		return subExpTypeText;
	}
	
	private PtrvSrecResponseWrapper setSubExpTypeText(String subExpTypeText) {
		this.subExpTypeText = subExpTypeText;
		return this;
	}
	
	@JsonProperty("form")
	public Short getForm() {
		return form;
	}

	private PtrvSrecResponseWrapper setForm(Short form) {
		this.form = form;
		return this;
	}

	@JsonProperty("start_volume")
	public String getStartVolume() {
		return startVolume;
	}

	private PtrvSrecResponseWrapper setStartVolume(String startVolume) {
		this.startVolume = startVolume;
		return this;
	}

	@JsonProperty("end_volume")
	public String getEndVolume() {
		return endVolume;
	}

	private PtrvSrecResponseWrapper setEndVolume(String endVolume) {
		this.endVolume = endVolume;
		return this;
	}

	@JsonProperty("sum_volume")
	public String getSumVolume() {
		return sumVolume;
	}

	private PtrvSrecResponseWrapper setSumVolume(String sumVolume) {
		this.sumVolume = sumVolume;
		return this;
	}
	
}

