package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class IT0077RequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = -7089749882944022660L;
	
	private String molga;
	private String racky;
	
	@JsonProperty("country_grouping_code")
	public String getMolga() {
		return molga;
	}
	
	@JsonProperty("ethnic_origin_code")
	public String getRacky() {
		return racky;
	}
}