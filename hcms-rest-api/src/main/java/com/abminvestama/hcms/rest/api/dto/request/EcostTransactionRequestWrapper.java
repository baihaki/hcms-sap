package com.abminvestama.hcms.rest.api.dto.request;


import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
public class EcostTransactionRequestWrapper extends ITRequestWrapper {
	
	private static final long serialVersionUID = -3606156957371871172L;

	private PtrvHeadRequestWrapper trxHdr;

	private PtrvSrecRequestWrapper[] trxItm;
	
	@JsonProperty("trx_header")		
	public PtrvHeadRequestWrapper getTrxHdr() {
		return trxHdr;
	}
	
	@JsonProperty("trx_item")
	public PtrvSrecRequestWrapper[] getTrxItm() {
		return trxItm;
	}

}