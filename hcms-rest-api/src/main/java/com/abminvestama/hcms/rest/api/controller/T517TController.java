package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.entity.T517T;
import com.abminvestama.hcms.core.service.api.business.query.T517TQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@RestController
@RequestMapping("/api/v1/education_grade")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class T517TController extends AbstractResource {

	private T517TQueryService t517tQueryService;
	
	@Autowired
	void setT517TQueryService(T517TQueryService t517tQueryService) {
		this.t517tQueryService = t517tQueryService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/page/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<T517T>> fetchPage(@PathVariable int pageNumber, 
			@RequestParam(required = true, value = "username", defaultValue = "") String username,
			@RequestParam(required = true, value = "auth_token", defaultValue = "") String authToken) throws Exception {
		
		if (authToken == null || authToken.isEmpty()) {
			throw new Exception("No token provided");
		}
		
		Page<T517T> resultPage = t517tQueryService.fetchAllWithPaging(pageNumber);
		
		APIResponseWrapper<Page<T517T>> response = new APIResponseWrapper<>();
		response.setData(resultPage);
		Link self = linkTo(methodOn(T517TController.class).fetchPage(pageNumber, username, authToken)).withSelfRel();
		response.add(self);
		
		return response;
	}				
}