package com.abminvestama.hcms.rest.api.dto.response;


import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class MedQuota2ResponseWrapper extends ResourceSupport {

	private String quotamt;
	private String quoleft;
	private String kodequo;
	
	private MedQuota2ResponseWrapper() {}
	
	public MedQuota2ResponseWrapper(ZmedEmpquota zmed, ZmedQuotaused qused) {
		if (zmed == null) {
			new MedQuota2ResponseWrapper();
		} else {
			this
			.setKodequo(StringUtils.defaultString(zmed.getKodequo(), StringUtils.EMPTY))
			.setQuotamt(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY))	
			.setQuoleft(StringUtils.defaultString(!qused.equals(null) ? ""+(zmed.getQuotamt()-qused.getQuotamt()):zmed.getQuotamt().toString(),StringUtils.EMPTY));
		}
	}
	
	public MedQuota2ResponseWrapper(ZmedEmpquota zmed) {
		if (zmed == null) {
			new MedQuota2ResponseWrapper();
		} else {
			this
			.setKodequo(StringUtils.defaultString(zmed.getKodequo(), StringUtils.EMPTY))
			.setQuotamt(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY))	
			.setQuoleft(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY));
		}
	}
	
	public MedQuota2ResponseWrapper(ZmedEmpquotasm zmed, ZmedQuotaused qused) {
		if (zmed == null) {
			new MedQuota2ResponseWrapper();
		} else {
			this
			.setKodequo(StringUtils.defaultString(zmed.getKodequo(), StringUtils.EMPTY))
			.setQuotamt(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY))	
			.setQuoleft(StringUtils.defaultString(!qused.equals(null)? ""+(zmed.getQuotamt()-qused.getQuotamt()):zmed.getQuotamt().toString(),StringUtils.EMPTY));			
		}
	}
	
	public MedQuota2ResponseWrapper(ZmedEmpquotasm zmed) {
		if (zmed == null) {
			new MedQuota2ResponseWrapper();
		} else {
			this
			.setKodequo(StringUtils.defaultString(zmed.getKodequo(), StringUtils.EMPTY))
			.setQuotamt(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY))	
			.setQuoleft(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY));
		}
	}
	
	public MedQuota2ResponseWrapper(ZmedEmpquotastf zmed, ZmedQuotaused qused) {
		if (zmed == null) {
			new MedQuota2ResponseWrapper();
		} else {
			this
			.setKodequo(StringUtils.defaultString(zmed.getKodequo(), StringUtils.EMPTY))
			.setQuotamt(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY))	
			.setQuoleft(StringUtils.defaultString(!qused.equals(null) ? ""+(zmed.getQuotamt()-qused.getQuotamt()):zmed.getQuotamt().toString(),StringUtils.EMPTY));			
		}
	}
	
	
	public MedQuota2ResponseWrapper(ZmedEmpquotastf zmed) {
		if (zmed == null) {
			new MedQuota2ResponseWrapper();
		} else {
			this
			.setKodequo(StringUtils.defaultString(zmed.getKodequo(), StringUtils.EMPTY))
			.setQuotamt(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY))	
			.setQuoleft(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY));
		}
	}
	
	
	
	@JsonProperty("kodequo")
	public String getKodequo() {
		return kodequo;
	}
	
	private MedQuota2ResponseWrapper setKodequo(String kodequo) {
		this.kodequo = kodequo;
		return this;
	}
	
	@JsonProperty("quotamt")
	public String getQuotamt() {
		return quotamt;
	}
	
	private MedQuota2ResponseWrapper setQuotamt(String quotamt) {
		this.quotamt = quotamt;
		return this;
	}
	
	
	@JsonProperty("quota_left")
	public String getQuoleft() {
		return quoleft;
	}
	
	private MedQuota2ResponseWrapper setQuoleft(String quoleft) {
		this.quoleft = quoleft;
		return this;
	}
}
