package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0241;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Wrapper class for <strong>Tax Data Indonesia</strong> (i.e. IT0241 in SAP)
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add Constructor of Read from EventData -> IT0241ResponseWrapper(IT0241, String, CommonServiceFactory)</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add attachmentType, attachmentTypeText, attachmentPath field</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add documentStatus field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class IT0241ResponseWrapper extends ResourceSupport {

	private long pernr;
	private Date endda;
	private Date begda;
	
	private String taxId;
	private String marrd;
	private String spben;
	private String refno;
	private String depnd;
	//private Date rdate;
	private String rdate;
	private String documentStatus;
	
	private String attachmentType;
	private String attachmentTypeText;
	private String attachmentPath;
	
	private  IT0241ResponseWrapper() {}
	
	public IT0241ResponseWrapper(IT0241 it0241) {
		if (it0241 == null) {
			new IT0241ResponseWrapper();
		} else {
			this
				.setPernr(it0241.getId().getPernr())
				.setEndda(it0241.getId().getEndda()).setBegda(it0241.getId().getBegda())
				.setTaxId(StringUtils.defaultString(it0241.getTaxId(), null))
				.setMarrd(StringUtils.defaultString(it0241.getMarrd(), null))
				.setSpben(StringUtils.defaultString(it0241.getSpben(), null))
				.setRefno(StringUtils.defaultString(it0241.getRefno(), null))
				.setDepnd(StringUtils.defaultString(it0241.getDepnd(), null))
				.setRdate(it0241.getRdate() != null ? CommonDateFunction.convertDateToStringYMD(it0241.getRdate()) : StringUtils.EMPTY)
				.setAttachmentType(it0241.getAttachment() != null ? StringUtils.defaultString(it0241.getAttachment().getId().getSubty(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentTypeText(it0241.getAttachment() != null ? StringUtils.defaultString(it0241.getAttachment().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentPath(StringUtils.defaultString(it0241.getAttachmentPath(), StringUtils.EMPTY))
				.setDocumentStatus(it0241.getStatus());				
		}		
	}

	public IT0241ResponseWrapper(IT0241 currentData, String eventData, CommonServiceFactory serviceFactory) throws JSONException, Exception {
		this(currentData);
		IT0241ResponseWrapper itWrapper = (IT0241ResponseWrapper) new EventDataWrapper(eventData, this).getItWrapper();
		String[] ignoreProperties = EventDataWrapper.getNullPropertyNames(itWrapper);
		BeanUtils.copyProperties(itWrapper, this, ignoreProperties);
	}
	
	/**
	 * GET Employee SSN.
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	public IT0241ResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	public IT0241ResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}
	
	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	public IT0241ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * GET Tax ID.
	 * 
	 * @return
	 */
	@JsonProperty("tax_id")
	public String getTaxId() {
		return taxId;
	}
	
	public IT0241ResponseWrapper setTaxId(String taxId) {
		this.taxId = taxId;
		return this;
	}
	
	/**
	 * GET Married for Tax Purposes.
	 * 
	 * @return
	 */
	@JsonProperty("married_for_tax")
	public String getMarrd() {
		return marrd;
	}
	
	public IT0241ResponseWrapper setMarrd(String marrd) {
		this.marrd = marrd;
		return this;
	}
	
	/**
	 * GET Spouse Benefit.
	 * 
	 * @return
	 */
	@JsonProperty("spouse_benefit")
	public String getSpben() {
		return spben;
	}
	
	public IT0241ResponseWrapper setSpben(String spben) {
		this.spben = spben;
		return this;
	}
	
	/**
	 * GET Unemployment Reference Number.
	 * 
	 * @return
	 */
	@JsonProperty("unemployment_ref_no")
	public String getRefno() {
		return refno;
	}
	
	public IT0241ResponseWrapper setRefno(String refno) {
		this.refno = refno;
		return this;
	}
	
	/**
	 * GET Number of Dependents for Tax Purposes.
	 * 
	 * @return
	 */
	@JsonProperty("num_of_tax_dependents")
	public String getDepnd() {
		return depnd;
	}
	
	public IT0241ResponseWrapper setDepnd(String depnd) {
		this.depnd = depnd;
		return this;
	}
	
	/**
	 * GET NPWP Registration Date.
	 * 
	 * @return
	 */
	@JsonProperty("npwp_registration_date")
	public String getRdate() {
		return rdate;
	}
	
	public IT0241ResponseWrapper setRdate(String rdate) {
		this.rdate = rdate;
		return this;
	}
	
	/**
	 * GET Document Status.
	 * 
	 * @return
	 */
	@JsonProperty("document_status")
	public String getDocumentStatus() {
		return documentStatus;
	}
	
	public IT0241ResponseWrapper setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus.name();
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type")
	public String getAttachmentType() {
		return attachmentType;
	}
	
	public IT0241ResponseWrapper setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
		return this;
	}
	
	/**
	 * GET Attachment Type Text.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type_text")
	public String getAttachmentTypeText() {
		return attachmentTypeText;
	}
	
	public IT0241ResponseWrapper setAttachmentTypeText(String attachmentTypeText) {
		this.attachmentTypeText = attachmentTypeText;
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_path")
	public String getAttachmentPath() {
		return attachmentPath;
	}
	
	public IT0241ResponseWrapper setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
		return this;
	}
}