package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.FamilySubtype;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.Attachment;
import com.abminvestama.hcms.core.model.entity.IT0021;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.T005T;
import com.abminvestama.hcms.core.model.entity.T535N;
import com.abminvestama.hcms.core.model.entity.T535NKey;
import com.abminvestama.hcms.core.model.entity.T591S;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.business.query.AttachmentQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0021QueryService;
import com.abminvestama.hcms.core.service.api.business.query.T005TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T535NQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T591SQueryService;
import com.abminvestama.hcms.core.service.util.FileAttachmentService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0021RequestWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.4
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add fields: address, phone</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Adjust attachment fields on EventData Json</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData: Add attachment fields</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData to handle Create</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("it0021RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT0021RequestBuilderUtil {
	
	private T005TQueryService t005tQueryService;
	private T591SQueryService t591sQueryService;
	private T535NQueryService t535nQueryService;
	private IT0021QueryService it0021QueryService;
	private AttachmentQueryService attachmentQueryService;
	private FileAttachmentService fileAttachmentService;
	
	@Autowired
	void setT005TQueryService(T005TQueryService t005tQueryService) {
		this.t005tQueryService = t005tQueryService;
	}
	
	@Autowired
	void setT591SQueryService(T591SQueryService t591sQueryService) {
		this.t591sQueryService = t591sQueryService;
	}
	
	@Autowired
	void setT535NQueryService(T535NQueryService t535nQueryService) {
		this.t535nQueryService = t535nQueryService;
	}
	
	@Autowired
	void setIT0021QueryService(IT0021QueryService it0021QueryService) {
		this.it0021QueryService = it0021QueryService;
	}
	
	@Autowired
	void setAttachmentQueryService(AttachmentQueryService attachmentQueryService) {
		this.attachmentQueryService = attachmentQueryService;
	}
	
	@Autowired
	void setFileAttachmentService(FileAttachmentService fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}

	/**
	 * Compare IT0021 request payload with existing IT0021 in the database.
	 * Update existing IT0021 data with the latest data comes from the request payload. 
	 * 
	 * @param requestPayload request data 
	 * @param it0021DB current existing IT0021 in the database.
	 * @return updated IT0021 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0021, IT0021RequestWrapper, String> compareAndReturnUpdatedData(IT0021RequestWrapper requestPayload, IT0021 it0021DB) {
		
		RequestObjectComparatorContainer<IT0021, IT0021RequestWrapper, String> objectContainer = null;
		
		if (it0021DB == null) {
			it0021DB = new IT0021();
			String subty = (requestPayload.getDefinedSubty() != null && requestPayload.getDefinedSubty().equals(FamilySubtype.EMERGENCY_CONTACT.subtype())) ?
					requestPayload.getDefinedSubty() : requestPayload.getSubty();
			try {
				Optional<IT0021> existingIT0021 = it0021QueryService.findOneByCompositeKey(
						requestPayload.getPernr(),  subty, 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda()));
				
				if (existingIT0021.isPresent()) {
					throw new DataViolationException("Duplicate IT0021 Data for [ssn=" + requestPayload.getPernr() + ",end_date=" +
							requestPayload.getEndda() + ",begin_date=" + requestPayload.getBegda());
				}
				
				it0021DB.setId(
						new ITCompositeKeys(requestPayload.getPernr(), SAPInfoType.EMERGENCY_INFO.infoType(), requestPayload.getSubty(), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda())));
				it0021DB.setFgbdt(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getFgbdt()));
				it0021DB.setFasex(requestPayload.getFasex());
				T591SKey famsaKey = new T591SKey(SAPInfoType.EMERGENCY_INFO.infoType(), requestPayload.getFamsa());
				Optional<T591S> t591s = t591sQueryService.findById(Optional.ofNullable(famsaKey));
				it0021DB.setFamsa(t591s.isPresent() ? t591s.get() : null);
				T535NKey titleKey = new T535NKey(requestPayload.getFnmzu(), requestPayload.getTitle());
				Optional<T535N> t535n = t535nQueryService.findById(Optional.ofNullable(titleKey));
				it0021DB.setT535n(t535n.isPresent() ? t535n.get() : null);
				it0021DB.setFcnam(requestPayload.getFcnam());
				it0021DB.setFavor(requestPayload.getFavor());
				it0021DB.setFanam(requestPayload.getFanam());
				Optional<T005T> fgbld = t005tQueryService.findById(Optional.ofNullable(requestPayload.getFgbld()));
				it0021DB.setFgbld(fgbld.isPresent() ? fgbld.get() : null);
				Optional<T005T> fanat = t005tQueryService.findById(Optional.ofNullable(requestPayload.getFanat()));
				it0021DB.setFanat(fanat.isPresent() ? fgbld.get() : null);
				it0021DB.setFgbot(requestPayload.getFgbot());
				it0021DB.setFknzn(requestPayload.getFknzn());
				it0021DB.setSeqnr(1L);
				it0021DB.setStatus(DocumentStatus.INITIAL_DRAFT);
				it0021DB.setAddress(requestPayload.getAddress());
				it0021DB.setPhone(requestPayload.getPhone());
				
				it0021DB = saveAttachment(it0021DB, requestPayload);
				
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			
			objectContainer = new RequestObjectComparatorContainer<>(it0021DB, requestPayload);
			
		} else {
			if (StringUtils.isNotBlank(requestPayload.getFamsa())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getFamsa(), it0021DB.getFamsa() != null ? it0021DB.getFamsa().getId().getSubty() : "")) {
					try {
						T591SKey key = new T591SKey(SAPInfoType.EMERGENCY_INFO.infoType(), requestPayload.getFamsa());
						Optional<T591S> newT591S = t591sQueryService.findById(Optional.ofNullable(key));
						if (newT591S.isPresent()) {
							it0021DB.setFamsa(newT591S.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T591S not found...cancel the update process
					}
				}
			}
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getFgbdt()), it0021DB.getFgbdt())) {
				it0021DB.setFgbdt(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getFgbdt()));
				requestPayload.setDataChanged(true);
			}
			if (StringUtils.isNotBlank(requestPayload.getFgbld())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getFgbld(), it0021DB.getFgbld() != null ? it0021DB.getFgbld().getLand1() : "")) {
					try {
						Optional<T005T> newT005T = t005tQueryService.findById(Optional.ofNullable(requestPayload.getFgbld()));
						if (newT005T.isPresent()) {
							it0021DB.setFgbld(newT005T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						// T005T not found...cancel the update process
					}
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getFanat())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getFanat(), it0021DB.getFanat() != null ? it0021DB.getFanat().getLand1() : "")) {
					try {
						Optional<T005T> newT005T = t005tQueryService.findById(Optional.ofNullable(requestPayload.getFanat()));
						if (newT005T.isPresent()) {
							it0021DB.setFanat(newT005T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						// T005T not found...cancel the update process
					}
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getFasex())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getFasex(), it0021DB.getFasex())) {
					it0021DB.setFasex(requestPayload.getFasex());
					requestPayload.setDataChanged(true);
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getFavor())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getFavor(), it0021DB.getFavor())) {
					it0021DB.setFavor(requestPayload.getFavor());
					requestPayload.setDataChanged(true);
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getFanam())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getFanam(), it0021DB.getFanam())) {
					it0021DB.setFanam(requestPayload.getFanam());
					requestPayload.setDataChanged(true);
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getFgbot())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getFgbot(), it0021DB.getFgbot())) {
					it0021DB.setFgbot(requestPayload.getFgbot());
					requestPayload.setDataChanged(true);
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getFcnam())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getFcnam(), it0021DB.getFcnam())) {
					it0021DB.setFcnam(requestPayload.getFcnam());
					requestPayload.setDataChanged(true);
				}
			}
			if (CommonComparatorFunction.isDifferentNumberValues(Integer.valueOf(requestPayload.getFknzn()), it0021DB.getFknzn())) {
				it0021DB.setFknzn(requestPayload.getFknzn());
				requestPayload.setDataChanged(true);
			}
			if (StringUtils.isNotBlank(requestPayload.getFnmzu()) && StringUtils.isNotBlank(requestPayload.getTitle())) {
				try {
					T535NKey key = new T535NKey(requestPayload.getFnmzu(), requestPayload.getTitle());
					Optional<T535N> newT535N = t535nQueryService.findById(Optional.ofNullable(key));
					if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getFnmzu(), it0021DB.getT535n().getId().getArt())
						|| CommonComparatorFunction.isDifferentStringValues(requestPayload.getTitle(), it0021DB.getT535n().getId().getTitle())) {
						it0021DB.setT535n(newT535N.get());
						requestPayload.setDataChanged(true);
					}					
				} catch (Exception e) {
					// T535N not found...cancel update process
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getAddress())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAddress(), it0021DB.getAddress())) {
					it0021DB.setAddress(requestPayload.getAddress());
					requestPayload.setDataChanged(true);
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getPhone())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPhone(), it0021DB.getPhone())) {
					it0021DB.setPhone(requestPayload.getPhone());
					requestPayload.setDataChanged(true);
				}
			}
			
			it0021DB.setSeqnr(it0021DB.getSeqnr().longValue() + 1);
			try {
				it0021DB = saveAttachment(it0021DB, requestPayload);
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			it0021DB.setSeqnr(it0021DB.getSeqnr().longValue() - 1);
			
			objectContainer = new RequestObjectComparatorContainer<>(it0021DB, requestPayload);
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = "";
		try {
			jsonString = objectMapper.writeValueAsString(requestPayload);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}		
		
		//JSONObject json = new JSONObject(requestPayload);
		JSONObject json = new JSONObject(jsonString);
		// Adjust attachment fields on EventData Json
		json.remove("attachment1_type");
		json.remove("attachment1_base64");
		json.remove("attachment1_filename");
		if (it0021DB.getAttachment() != null)
			json.put("attachment1_type", it0021DB.getAttachment().getId().getSubty());
		if (it0021DB.getAttachmentPath() != null)
		    json.put("attachment1_path", it0021DB.getAttachmentPath());
		// ******************************************
		
		//if (it0021DB.getStatus() != null)
			//json.put("prev_document_status", it0021DB.getStatus().name());
		
		objectContainer.setEventData(json.toString());
		
		return objectContainer;
	}
	
	private IT0021 saveAttachment(IT0021 it0021DB, IT0021RequestWrapper requestPayload) throws Exception {
		
		T591SKey attachmentKey = new T591SKey(SAPInfoType.EMERGENCY_INFO.infoType(), requestPayload.getAttach1Type());
		Optional<Attachment> attachment = attachmentQueryService.findById(Optional.ofNullable(attachmentKey));
		it0021DB.setAttachment(attachment.isPresent() ? attachment.get() : null);
		String base64 = requestPayload.getAttach1Content();
		String filename = requestPayload.getAttach1Filename();
		if (StringUtils.isNotEmpty(base64) && StringUtils.isNotEmpty(filename)) {
			String ext = filename.split("\\.")[(filename.split("\\.")).length-1];
			String formatFilename = it0021DB.getId().toStringId().concat("_").concat(it0021DB.getSeqnr().toString()).concat(".").concat(ext);
		    String path = fileAttachmentService.save(formatFilename, base64);
		    it0021DB.setAttachmentPath(path);
		    requestPayload.setDataChanged(true);
		}
		
		return it0021DB;
		
	}
	
}