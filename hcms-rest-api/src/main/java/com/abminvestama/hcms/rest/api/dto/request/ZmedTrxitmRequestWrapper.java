package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ZmedTrxitmRequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = -2317050437190237970L;

	String itmno;
	private String bukrs;
	private String clmno;
	private String billdt;
	private String kodemed;
	private String kodecos;
	private String zcostyp;
	private String clmamt;
	private String rejamt;
	private String appamt;
	private String relat;
	private String umur;
	private String pasien;

	

	
	public void setItmno(String itmno) {
		this.itmno = itmno;
	}


	@JsonProperty("item_number")
	public String getItmno() {
		return itmno;
	}
	
	@JsonProperty("company_code")
	public String getBukrs() {
		return bukrs;
	}
	
	@JsonProperty("claim_number")
	public String getClmno() {
		return clmno;
	}
	
	
	@JsonProperty("date_created")
	public String getBilldt() {
		return billdt;
	}
	
	
	@JsonProperty("medical_type")
	public String getKodemed() {
		return kodemed;
	}
	
	
	@JsonProperty("cost_type")
	public String getKodecos() {
		return kodecos;
	}
		
	
	@JsonProperty("cost_types")
	public String getZcostyp() {
		return zcostyp;
	}
	
	
	@JsonProperty("claim_amount")
	public String getClmamt() {
		return clmamt;
	}
	
	
	@JsonProperty("rej_amount")
	public String getRejamt() {
		return rejamt;
	}
	
	
	@JsonProperty("app_amount")
	public String getAppamt() {
		return appamt;
	}
	
	
	@JsonProperty("relat")
	public String getRelat() {
		return relat;
	}
	
	
	@JsonProperty("umur")
	public String getUmur() {
		return umur;
	}
	
	@JsonProperty("pasien")
	public String getPasien() {
		return pasien;
	}
}