package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.exception.WorkflowNotFoundException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0022;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0022CommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0022QueryService;
import com.abminvestama.hcms.core.service.api.business.query.ITQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.helper.IT0022SoapObject;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0022RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ApprovalEventLogResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.DemographyCountResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.IT0022ResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.IT0022RequestBuilderUtil;

/**
 * 
 * @since 1.0.0
 * @version 1.0.4
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: findByCriteria</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: approve, reject, getUpdatedFields</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Modify edit method: update document only change the status</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add methods: countByEducationAndBukrs</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/education")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class IT0022Controller extends AbstractResource {
	
	private IT0022CommandService it0022CommandService;
	private IT0022QueryService it0022QueryService;
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	private Validator itValidator;
	private IT0022RequestBuilderUtil it0022RequestBuilderUtil;
	private UserQueryService userQueryService;

	private MuleConsumerService muleConsumerService;
	private CommonServiceFactory commonServiceFactory;
	private ApprovalController approvalController;
	private ITQueryService itQueryService;
	
	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	
	@Autowired
	void setIT0022CommandService(IT0022CommandService it0022CommandService) {
		this.it0022CommandService = it0022CommandService;
	}
	
	@Autowired
	void setIT0022QueryService(IT0022QueryService it0022QueryService) {
		this.it0022QueryService = it0022QueryService;
	}
	
	@Autowired
	@Qualifier("itValidator")	
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@Autowired
	@Qualifier("it0022RequestBuilderUtil")
	void setIT0022RequestBuilderUtil(IT0022RequestBuilderUtil it0022RequestBuilderUtil) {
		this.it0022RequestBuilderUtil = it0022RequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	@Autowired
	void setApprovalController(ApprovalController approvalController) {
		this.approvalController = approvalController;
	}
	
	@Autowired
	void setITQueryService(ITQueryService itQueryService) {
		this.itQueryService = itQueryService;
	}
	
	@SuppressWarnings("unchecked")
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_criteria", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<IT0022ResponseWrapper>> findByCriteria(
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "exclude_status", required = false, defaultValue="PUBLISHED,DRAFT_REJECTED,UPDATE_REJECTED") String excludeStatus,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {		

		Page<IT0022ResponseWrapper> responsePage = new PageImpl<IT0022ResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<IT0022ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<IT0022> resultPage = null;
		String bukrs = null;
		Long pernr = null;
		String[] excludes = excludeStatus.split(",");
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		
		if (StringUtils.isNotEmpty(ssn)) {
			pernr = Long.valueOf(ssn);
			if (!currentUser.get().getRoles().contains(adminRole.get()) &&
					!currentUser.get().getEmployee().getPernr().equals(pernr) ) {
				throw new AuthorizationException(
						"Insufficient Privileges. You can't fetch data of other Employee.");
			}
		} else {
			// no specified ssn, MUST BE ADMIN
			if (!currentUser.get().getRoles().contains(adminRole.get())) {
				throw new AuthorizationException(
						"Insufficient Privileges. Please login as 'ADMIN'!");
			}
			bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
		}
		
		try {
			resultPage = (Page<IT0022>) itQueryService.fetchDistinctByCriteriaWithPaging(pageNumber, bukrs, pernr, excludes,
					new IT0022(), currentUser.get().getEmployee());
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			
			if (resultPage.hasContent()) {

				List<IT0022ResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(it0022 -> {
					records.add(new IT0022ResponseWrapper(it0022));
				});
				responsePage = new PageImpl<IT0022ResponseWrapper>(records, it0022QueryService.createPageRequest(pageNumber),
						resultPage.getTotalElements());
				response.setData(responsePage);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0022Controller.class).findByCriteria(ssn, excludeStatus, pageNumber)).withSelfRel());
		
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.PUT, value = "/approve")
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> approve(
			@RequestHeader(required = true, value = "ssn") String pernr,
			@RequestHeader(required = true, value = "subtype") String subtype,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda)
					throws AuthorizationException, NumberFormatException, CannotPersistException,
					DataViolationException, EntityNotFoundException, WorkflowNotFoundException, Exception {
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", pernr);
			keyMap.put("infotype", SAPInfoType.EMPLOYEE_EDUCATION.infoType());
			keyMap.put("subtype", subtype);
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			keyMap.put("muleService", BAPIFunction.IT0022_CREATE.service());
			keyMap.put("muleUri", BAPIFunction.IT0022_CREATE_URI.service());
			
			String className = IT0022.class.getSimpleName();
			response = approvalController.approve(keyMap, className, it0022QueryService, it0022CommandService,
					IT0022SoapObject.class, it0022RequestBuilderUtil, new IT0022RequestWrapper());

		} catch (EntityNotFoundException e) {
			throw e;
		} catch (WorkflowNotFoundException e) {
			throw e;
		} catch (DataViolationException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0022Controller.class).approve(pernr, subtype, endda, begda)).withSelfRel());				
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.PUT, value = "/reject")
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> reject(
			@RequestHeader(required = true, value = "ssn") String pernr,
			@RequestHeader(required = true, value = "subtype") String subtype,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda) 
					throws AuthorizationException, NumberFormatException, CannotPersistException, 
					DataViolationException, EntityNotFoundException, Exception {
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", pernr);
			keyMap.put("infotype", SAPInfoType.EMPLOYEE_EDUCATION.infoType());
			keyMap.put("subtype", subtype);
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			
			String className = IT0022.class.getSimpleName();
			response = approvalController.reject(keyMap, className, it0022QueryService, it0022CommandService);
			
		} catch (AuthorizationException e) { 
			throw e;
		} catch (NumberFormatException e) { 
			throw e;
		} catch (CannotPersistException e) {
			throw e;
		} catch (DataViolationException e) {
			throw e;
		} catch (EntityNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0022Controller.class).reject(pernr, subtype, endda, begda)).withSelfRel());		
		return response;
	}
	
	@SuppressWarnings("unchecked")
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_updated_fields")
	public APIResponseWrapper<IT0022ResponseWrapper> getUpdatedFields(
			@RequestHeader(required = true, value = "ssn") String ssn,
			@RequestHeader(required = true, value = "subtype") String subtype,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda
			) throws DataViolationException, EntityNotFoundException, Exception {

		APIResponseWrapper<IT0022ResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", ssn);
			keyMap.put("infotype", SAPInfoType.EMPLOYEE_EDUCATION.infoType());
			keyMap.put("subtype", subtype);
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			
			String className = IT0022.class.getSimpleName();
			response = (APIResponseWrapper<IT0022ResponseWrapper>) approvalController.getUpdatedFields(
					keyMap, className, it0022QueryService, response, IT0022ResponseWrapper.class);
			
		} catch (DataViolationException e) { 
			throw e;
		} catch (EntityNotFoundException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0022Controller.class).getUpdatedFields(ssn, subtype, endda, begda)).withSelfRel());
		return response;
	}

	//@Secured({AccessRole.USER_GROUP})
	/*@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0022ResponseWrapper>> findOneByPernr(@PathVariable String pernr) throws Exception {
		ArrayData<IT0022ResponseWrapper> bunchOfIT0022 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0022ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0022);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0022> listOfIT0022 = it0022QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			
			if (listOfIT0022.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0022ResponseWrapper> records = new ArrayList<>();
				for (IT0022 it0022 : listOfIT0022) {
					records.add(new IT0022ResponseWrapper(it0022));
				}
				bunchOfIT0022.setItems(records.toArray(new IT0022ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0022Controller.class).findOneByPernr(pernr)).withSelfRel());
		return response;
	}	*/
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated IT0022RequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			IT0022ResponseWrapper responseWrapper = new IT0022ResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					
					Optional<HCMSEntity> it0022Entity = hcmsEntityQueryService.findByEntityName(IT0022.class.getSimpleName());
					if (!it0022Entity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + IT0022.class.getSimpleName() + "' !!!");
					}					
					
					
					
					Optional<IT0022> it0022 = it0022QueryService.findOneByCompositeKey(request.getPernr(), request.getSubty(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
					if (!it0022.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
					
					// since version 1.0.2
					IT0022 currentIT0022 = new IT0022();
					BeanUtils.copyProperties(it0022.get(), currentIT0022);
				
					RequestObjectComparatorContainer<IT0022, IT0022RequestWrapper, String> updatedContainer
					//= it0022RequestBuilderUtil.compareAndReturnUpdatedData(request, it0022.get());
					// since version 1.0.2
					= it0022RequestBuilderUtil.compareAndReturnUpdatedData(request, currentIT0022);

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
					if (updatedContainer.getEntity().isPresent()) {
						//updatedContainer.getEntity().get().setSeqnr(it0022.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
						//it0022 = it0022CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						// since version 1.0.2, update document only change the status 
						if (it0022.get().getStatus() == DocumentStatus.INITIAL_DRAFT) {
							it0022.get().setStatus(DocumentStatus.UPDATED_DRAFT);
							currentIT0022.setStatus(DocumentStatus.UPDATED_DRAFT);
						} else if (it0022.get().getStatus() == DocumentStatus.PUBLISHED) {
							it0022.get().setStatus(DocumentStatus.UPDATE_IN_PROGRESS);
							currentIT0022.setStatus(DocumentStatus.UPDATE_IN_PROGRESS);
						}
					}
				
					if (!it0022.isPresent()) {
						throw new CannotPersistException("Cannot update IT0022 (Educational Info) data. Please check your data!");
					}
				
					

					StringBuilder key = new StringBuilder("pernr=" + it0022.get().getId().getPernr());
					key.append(",infty=" + it0022.get().getId().getInfty());
					key.append(",subty=" + it0022.get().getId().getSubty());
					key.append(",endda=" + it0022.get().getId().getEndda());
					key.append(",begda=" + it0022.get().getId().getBegda());
					EventLog eventLog = new EventLog(it0022Entity.get(), key.toString(), OperationType.UPDATE, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					eventLog.setStatus(it0022.get().getStatus().toString());
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					
					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}

					//IT0022SoapObject it0022SoapObject = new IT0022SoapObject(savedEntity.get()); 
					//muleConsumerService.postToSAP(BAPIFunction.IT0022_CREATE.service(), BAPIFunction.IT0022_CREATE_URI.service(), it0022SoapObject);
					
					//responseWrapper = new IT0022ResponseWrapper(it0022.get());
					// since version 1.0.2
					responseWrapper = new IT0022ResponseWrapper(currentIT0022);
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(IT0022Controller.class).edit(request, result)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated IT0022RequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> it0022Entity = hcmsEntityQueryService.findByEntityName(IT0022.class.getSimpleName());
			if (!it0022Entity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + IT0022.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<IT0022, IT0022RequestWrapper, String> newObjectContainer 
					= it0022RequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				
				Optional<IT0022> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
					newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = it0022CommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create IT0022 (Bank Details) data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("pernr=" + savedEntity.get().getId().getPernr());
				key.append(",infty=" + savedEntity.get().getId().getInfty());
				key.append(",subty=" + savedEntity.get().getId().getSubty());
				key.append(",endda=" + CommonDateFunction.convertDateToStringYMD(savedEntity.get().getId().getEndda()));
				key.append(",begda=" + CommonDateFunction.convertDateToStringYMD(savedEntity.get().getId().getBegda()));
				EventLog eventLog = new EventLog(it0022Entity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				eventLog.setStatus(savedEntity.get().getStatus().toString());
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}

				//IT0022SoapObject it0022SoapObject = new IT0022SoapObject(savedEntity.get()); 
				//muleConsumerService.postToSAP(BAPIFunction.IT0022_CREATE.service(), BAPIFunction.IT0022_CREATE_URI.service() , it0022SoapObject);
				
				IT0022ResponseWrapper responseWrapper = new IT0022ResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(IT0022Controller.class).create(request, result)).withSelfRel());		
		return response;
	}
	

	
	
	
	//@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/status/{status}/page/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0022ResponseWrapper>> findByDocumentStatus(@PathVariable String status, @PathVariable int pageNumber) throws Exception {
		ArrayData<IT0022ResponseWrapper> bunchOfIT0022 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0022ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0022);
		
		try {
			Page<IT0022> listOfIT0022 = it0022QueryService.findByStatus(status, pageNumber);
			
			if (listOfIT0022==null || !listOfIT0022.hasContent()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0022ResponseWrapper> records = new ArrayList<>();
				
				listOfIT0022.forEach ( it0022 -> {
					records.add(new IT0022ResponseWrapper(it0022));
				});
				bunchOfIT0022.setItems(records.toArray(new IT0022ResponseWrapper[records.size()]));
			}
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0022Controller.class).findByDocumentStatus(status, pageNumber)).withSelfRel());
		
		return response;
	}

	
	
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0022ResponseWrapper>> findByPernr(@PathVariable String pernr, 
			@RequestParam(value = "status", required = false) String status, 
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		ArrayData<IT0022ResponseWrapper> bunchOfIT0022 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0022ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0022);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0022> listOfIT0022 = new ArrayList<>();
			
			if (StringUtils.isNotEmpty(status)) {
				Page<IT0022> data = it0022QueryService.findByPernrAndStatus(numOfPernr, status, pageNumber);
				if (data.hasContent()) {
					listOfIT0022 = data.getContent();
				}
			} else {
				listOfIT0022 = it0022QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			}
			
			if (listOfIT0022.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0022ResponseWrapper> records = new ArrayList<>();
				for (IT0022 it0022 : listOfIT0022) {
					records.add(new IT0022ResponseWrapper(it0022));
				}
				bunchOfIT0022.setItems(records.toArray(new IT0022ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0022Controller.class).findByPernr(pernr, status, pageNumber)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	@Secured({AccessRole.USER_GROUP, AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE,
			headers = "Accept=application/json", value = "/bukrs/{bukrs}")
	public APIResponseWrapper<DemographyCountResponseWrapper> countByEducationAndBukrs(
			@PathVariable String bukrs, 
			@RequestParam(value = "education", required = false) String slart ) throws Exception {
		
		APIResponseWrapper<DemographyCountResponseWrapper> response = new APIResponseWrapper<>();
		//response.setData(employeePositionResponses);
		
		try {
			//Long numOfPernr = Long.valueOf(pernr);
			
			Optional<User> currentUser = userQueryService.findByUsername(currentUser());
			
			if (!currentUser.isPresent()) {
				throw new UsernameNotFoundException("No User Logged-in!");
			}
			
			Long count = it0022QueryService.countByEduAndBukrs(slart, bukrs);
			
			if (count == null) {
				response.setMessage("No Data Found");
			} else {
				response.setData(new DemographyCountResponseWrapper( count));								
			}
		} catch (NumberFormatException e) { 
			throw e;
		} catch (UsernameNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0022Controller.class).countByEducationAndBukrs(bukrs,slart)).withSelfRel());		
		return response;
	}
	
	
	
	
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(itValidator);
	}			
}






