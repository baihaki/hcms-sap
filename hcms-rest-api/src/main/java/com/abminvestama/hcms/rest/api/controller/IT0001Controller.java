package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.model.constant.UserRole;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.PositionRelation;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.query.IT0001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.ITQueryService;
import com.abminvestama.hcms.core.service.api.business.query.PositionRelationQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.DemographyCountResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.EmployeePositionRelationResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.IT0002ResponseWrapper;

/**
 * 
 * @since 2.0.0
 * @version 2.0.3
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.4</td><td>Wijanarko</td><td>Edit method: findByBukrs, add ROLE_TREASURY</td></tr>
 *     <tr><td>2.0.3</td><td>Baihaki</td><td>Fix method: findPositionRelationByPernr, fetch array of employee's position relation</td></tr>
 *     <tr><td>2.0.2</td><td>Baihaki</td><td>Add methods: findByBukrs</td></tr>
 *     <tr><td>2.0.1</td><td>Anasuya</td><td>Add methods: countByPostAndBukrs, countByJobAndBukrs, countByEmpAndBukrs</td></tr>
 *     <tr><td>2.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 * Class that represents IT0001 Infotype in SAP. 
 * Moving forward, this class should be replaced by other class that represents 'Employee'.
 */
@RestController
@RequestMapping("/api/v2/it0001")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class IT0001Controller extends AbstractResource {

	private IT0001QueryService it0001QueryService;
	
	private PositionRelationQueryService positionRelationQueryService;
	private UserQueryService userQueryService;
	private CommonServiceFactory commonServiceFactory;
	private ITQueryService itQueryService;
	
	@Autowired
	void setIT0001QueryService(IT0001QueryService it0001QueryService) {
		this.it0001QueryService = it0001QueryService;
	}
	
	@Autowired
	void setPositionRelationQueryService(PositionRelationQueryService positionRelationQueryService) {
		this.positionRelationQueryService = positionRelationQueryService;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	@Autowired
	void setITQueryService(ITQueryService itQueryService) {
		this.itQueryService = itQueryService;
	}
	
	@SuppressWarnings("unchecked")
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP, AccessRole.TREASURY_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_bukrs", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<IT0001>> findByBukrs(
			@RequestParam(value = "bukrs", required = false) String bukrs,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {		

		Page<IT0001> responsePage = new PageImpl<IT0001>(new ArrayList<>());
		APIResponseWrapper<Page<IT0001>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<IT0001> resultPage = null;
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		Optional<Role> treasuryRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_TREASURY.roleIdent));

		String superiorPosition = null;
		
		if ("ALL".equals(bukrs)) {
			if (!currentUser.get().getRoles().contains(adminRole.get())
					&& !currentUser.get().getRoles().contains(treasuryRole.get())) {
				throw new AuthorizationException(
						"Insufficient Privileges. You can't fetch data of All Employee.");
			}
			bukrs = null;
		} else {
			// If NOT ADMIN (ROLE SUPERIOR ONLY) set superiorPosition parameter as additional filter
			if (!currentUser.get().getRoles().contains(adminRole.get())
					&& !currentUser.get().getRoles().contains(treasuryRole.get())) {
				superiorPosition = currentUser.get().getEmployee().getPosition().getId();
			}
			bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
		}
		
		try {
			resultPage = (Page<IT0001>) itQueryService.fetchDistinctByBukrsWithPaging(pageNumber, bukrs, superiorPosition);
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			
			if (resultPage.hasContent()) {
				/*
				List<IT0001ResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(it0001 -> {
					records.add(new IT0001ResponseWrapper(it0001));
				});
				responsePage = new PageImpl<IT0001ResponseWrapper>(records, it0001QueryService.createPageRequest(pageNumber),
						resultPage.getTotalElements());
				*/
				response.setData(resultPage);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0001Controller.class).findByBukrs(bukrs, pageNumber)).withSelfRel());
		
		return response;
	}
	
	@Secured({AccessRole.USER_GROUP, AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE,
			headers = "Accept=application/json", value = "/ssn/{pernr}")
	public APIResponseWrapper<ArrayData<EmployeePositionRelationResponseWrapper>> findPositionRelationByPernr(
			@PathVariable String pernr) throws Exception {
		
		ArrayData<EmployeePositionRelationResponseWrapper> employeePositionResponses = new ArrayData<>();
		
		APIResponseWrapper<ArrayData<EmployeePositionRelationResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(employeePositionResponses);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			Optional<User> currentUser = userQueryService.findByUsername(currentUser());
			
			if (!currentUser.isPresent()) {
				throw new UsernameNotFoundException("No User Logged-in!");
			}
			
			List<IT0001> listOfIT0001 = it0001QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			
			List<EmployeePositionRelationResponseWrapper> records = new ArrayList<>();
			
			if (listOfIT0001.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				IT0001 it0001 = listOfIT0001.get(0);
				
				List<PositionRelation> employeePositionRelations = positionRelationQueryService.findByFromPositionid(
						it0001.getPosition().getId()).stream().collect(Collectors.toList());
				
				if (!employeePositionRelations.isEmpty()) {
					/*
					List<IT0001> toIT0001List = it0001QueryService.findByPositionId(employeePositionRelations.get(0).getToPosition().getId())
							.stream().collect(Collectors.toList());
					for (IT0001 it0001Relation : toIT0001List) {
						records.add(new EmployeePositionRelationResponseWrapper(
								it0001, it0001Relation, employeePositionRelations.get(0).getPositionRelationType().getName()));
					}
					*/

					// fix employee's position relation, fetch array of employee's position relation
					for (PositionRelation positionRelation : employeePositionRelations) {
						List<IT0001> toIT0001List = it0001QueryService.findByPositionId(positionRelation.getToPosition().getId())
								.stream().collect(Collectors.toList());
						records.add(new EmployeePositionRelationResponseWrapper(
								it0001, toIT0001List.get(0), positionRelation.getPositionRelationType().getName()));
					}
					
					employeePositionResponses.setItems(records.toArray(new EmployeePositionRelationResponseWrapper[records.size()]));
				} else {
					response.setMessage("No Relation Found !");
				}								
			}
		} catch (NumberFormatException e) { 
			throw e;
		} catch (UsernameNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0001Controller.class).findPositionRelationByPernr(pernr)).withSelfRel());		
		return response;
	}
	
	
	
	
	
	
	
	
	
	
	
	@Secured({AccessRole.USER_GROUP, AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE,
			headers = "Accept=application/json", value = "/bukrs/{bukrs}")
	public APIResponseWrapper<DemographyCountResponseWrapper> countByPostAndBukrs(
			@PathVariable String bukrs, 
			@RequestParam(value = "post", required = false) String persa ) throws Exception {
		
		APIResponseWrapper<DemographyCountResponseWrapper> response = new APIResponseWrapper<>();
		//response.setData(employeePositionResponses);
		
		try {
			//Long numOfPernr = Long.valueOf(pernr);
			
			Optional<User> currentUser = userQueryService.findByUsername(currentUser());
			
			if (!currentUser.isPresent()) {
				throw new UsernameNotFoundException("No User Logged-in!");
			}
			
			Long count = it0001QueryService.countByPostAndBukrs(persa, bukrs);
			
			if (count == null) {
				response.setMessage("No Data Found");
			} else {
				response.setData(new DemographyCountResponseWrapper( count));								
			}
		} catch (NumberFormatException e) { 
			throw e;
		} catch (UsernameNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0001Controller.class).countByPostAndBukrs(bukrs,persa)).withSelfRel());		
		return response;
	}
	
	
	
	
	
	
	
	
	
	
	@Secured({AccessRole.USER_GROUP, AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE,
			headers = "Accept=application/json", value = "/bukrs/{bukrs}/job/{stell}")
	public APIResponseWrapper<DemographyCountResponseWrapper> countByJobAndBukrs(
			@PathVariable String bukrs, 
			@PathVariable String stell ) throws Exception {
		
		APIResponseWrapper<DemographyCountResponseWrapper> response = new APIResponseWrapper<>();
		//response.setData(employeePositionResponses);
		
		try {
			//Long numOfPernr = Long.valueOf(pernr);
			
			Optional<User> currentUser = userQueryService.findByUsername(currentUser());
			
			if (!currentUser.isPresent()) {
				throw new UsernameNotFoundException("No User Logged-in!");
			}
			
			Long count = it0001QueryService.countByJobAndBukrs(stell, bukrs);
			
			if (count == null) {
				response.setMessage("No Data Found");
			} else {
				response.setData(new DemographyCountResponseWrapper( count));								
			}
		} catch (NumberFormatException e) { 
			throw e;
		} catch (UsernameNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0001Controller.class).countByJobAndBukrs(bukrs,stell)).withSelfRel());		
		return response;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@Secured({AccessRole.USER_GROUP, AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE,
			headers = "Accept=application/json", value = "/bukrs/{bukrs}/emp/{persg}")
	public APIResponseWrapper<DemographyCountResponseWrapper> countByEmpAndBukrs(
			@PathVariable String bukrs, 
			@PathVariable String persg ) throws Exception {
		
		APIResponseWrapper<DemographyCountResponseWrapper> response = new APIResponseWrapper<>();
		//response.setData(employeePositionResponses);
		
		try {
			//Long numOfPernr = Long.valueOf(pernr);
			
			Optional<User> currentUser = userQueryService.findByUsername(currentUser());
			
			if (!currentUser.isPresent()) {
				throw new UsernameNotFoundException("No User Logged-in!");
			}
			
			Long count = it0001QueryService.countByEmpAndBukrs(persg, bukrs);
			
			if (count == null) {
				response.setMessage("No Data Found");
			} else {
				response.setData(new DemographyCountResponseWrapper( count));								
			}
		} catch (NumberFormatException e) { 
			throw e;
		} catch (UsernameNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0001Controller.class).countByEmpAndBukrs(bukrs,persg)).withSelfRel());		
		return response;
	}
	
	
	
	
	
}