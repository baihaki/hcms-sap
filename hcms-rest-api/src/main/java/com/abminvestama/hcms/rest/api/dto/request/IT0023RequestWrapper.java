package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 2.0.0
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.0</td><td>Baihaki</td><td>Add posName, posRoles field</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add attach1Type, attach1Content, attach1Filename field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public class IT0023RequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = 5810708859718546691L;
	
	private String arbgb;
	private String ort01;
	private String land1;
	private String branc;
	private long taete;
	private String ansvx;

	private String attach1Type;
	private String attach1Content;
	private String attach1Filename;
	
	private String posName;
	private String posRoles;
	
	@JsonProperty("employer")
	public String getArbgb() {
		return arbgb;
	}
	
	@JsonProperty("city")
	public String getOrt01() {
		return ort01;
	}
	
	@JsonProperty("country_key")
	public String getLand1() {
		return land1;
	}
	
	@JsonProperty("industry_code")
	public String getBranc() {
		return branc;
	}
	
	@JsonProperty("job_code")
	public long getTaete() {
		return taete;
	}
	
	@JsonProperty("work_contract")
	public String getAnsvx() {
		return ansvx;
	}
	
	@JsonProperty("attachment1_type")
	public String getAttach1Type() {
		return attach1Type;
	}
	
	@JsonProperty("attachment1_base64")
	public String getAttach1Content() {
		return attach1Content;
	}
	
	@JsonProperty("attachment1_filename")
	public String getAttach1Filename() {
		return attach1Filename;
	}
	
	@JsonProperty("position_name")
	public String getPosName() {
		return posName;
	}
	
	@JsonProperty("position_roles")
	public String getPosRoles() {
		return posRoles;
	}
}