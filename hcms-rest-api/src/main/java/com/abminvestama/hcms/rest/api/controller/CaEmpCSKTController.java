package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.entity.CaEmpCSKT;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.query.CaEmpCSKTQueryService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.CaEmpCSKTResponseWrapper;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v3/ca_emp_cskt")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CaEmpCSKTController extends AbstractResource {

	private EventLogCommandService eventLogCommandService;
	private HCMSEntityQueryService hcmsEntityQueryService;
	private UserQueryService userQueryService;
	private CommonServiceFactory commonServiceFactory;
	private CaEmpCSKTQueryService caEmpCSKTQueryService;

	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}

	@Autowired
	void setCaEmpCSKTQueryService(CaEmpCSKTQueryService caEmpCSKTQueryService) {
		this.caEmpCSKTQueryService = caEmpCSKTQueryService;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/findById/{empId}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<CaEmpCSKTResponseWrapper> findOneByEmpId(@PathVariable Long empId) throws Exception {
		
		APIResponseWrapper<CaEmpCSKTResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			Optional<CaEmpCSKT> caEmpCSKTObj = caEmpCSKTQueryService.findById(Optional.ofNullable(empId));
			
			if (!caEmpCSKTObj.isPresent()) {
				response.setMessage("No Data Found");
				response.setData(new CaEmpCSKTResponseWrapper(null));
			}
			else {
				response.setData(new CaEmpCSKTResponseWrapper(caEmpCSKTObj.get()));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(CaEmpCSKTController.class).findOneByEmpId(empId)).withSelfRel());
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{ssn}", headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<CaEmpCSKTResponseWrapper>> findCostCenterActiveByPernr(@PathVariable String ssn)
			throws Exception {

		ArrayData<CaEmpCSKTResponseWrapper> caEmpCSKTResponses = new ArrayData<>();
		APIResponseWrapper<ArrayData<CaEmpCSKTResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(caEmpCSKTResponses);

		try {
			Optional<User> currentUser = userQueryService.findByUsername(currentUser());

			if (!currentUser.isPresent()) {
				throw new UsernameNotFoundException("No User Logged-in!");
			}

			Long pernr = ssn != null && !ssn.trim().isEmpty() ? new Long(ssn) : 0L;
			String bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();

			List<CaEmpCSKT> listOfCaEmpCSKT;
			if (pernr == -1L) {
				// get All item, for ROLE TREASURY
				listOfCaEmpCSKT = caEmpCSKTQueryService.findAllCostCenterActiveByBukrs(bukrs).stream()
						.collect(Collectors.toList());
			} else {
				// get item by SSN
				listOfCaEmpCSKT = caEmpCSKTQueryService.findCostCenterActiveByPernr(new Long(pernr)).stream()
						.collect(Collectors.toList());
			}
			List<CaEmpCSKTResponseWrapper> records = new ArrayList<>();

			if (listOfCaEmpCSKT.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				for (CaEmpCSKT caEmpCSKT : listOfCaEmpCSKT) {
					records.add(new CaEmpCSKTResponseWrapper(caEmpCSKT));
				}
				caEmpCSKTResponses.setItems(records.toArray(new CaEmpCSKTResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(CaEmpCSKTController.class).findCostCenterActiveByPernr(ssn)).withSelfRel());
		return response;
	}
}
