package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.BNKA;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.IT2006;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.T005T;
import com.abminvestama.hcms.core.model.entity.T042Z;
import com.abminvestama.hcms.core.model.entity.T556B;
import com.abminvestama.hcms.core.model.entity.T591S;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.business.query.BNKAQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0009QueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT2006QueryService;
import com.abminvestama.hcms.core.service.api.business.query.T005TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T042ZQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T556BQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T591SQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0009RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.IT2006RequestWrapper;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Component("it2006RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT2006RequestBuilderUtil {


	private IT2006QueryService it2006QueryService;

	@Autowired
	void setIT2006QueryService(IT2006QueryService it2006QueryService) {
		this.it2006QueryService = it2006QueryService;
	}
	
	
	private T591SQueryService t591sQueryService;
	
	@Autowired
	void setT591SQueryService(T591SQueryService t591sQueryService) {
		this.t591sQueryService = t591sQueryService;
	}
	
	
	private T556BQueryService t556bQueryService;
	
	@Autowired
	void setT556BQueryService(T556BQueryService t556bQueryService) {
		this.t556bQueryService = t556bQueryService;
	}
	
	/**
	 * Compare IT2006 requested payload with an existing IT2006 in the database.
	 * Update the existing IT2006 data with the latest data comes from the requested payload. 
	 * 
	 * @param requestPayload requested data 
	 * @param it2006DB current existing IT2006 in the database.
	 * @return updated IT2006 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT2006, IT2006RequestWrapper, String> compareAndReturnUpdatedData(IT2006RequestWrapper requestPayload, IT2006 it2006DB) {
		
		RequestObjectComparatorContainer<IT2006, IT2006RequestWrapper, String> objectContainer = null; 
		
		
		if (it2006DB == null) {
			it2006DB = new IT2006();
			
			try {
				Optional<IT2006> existingIT2006 = it2006QueryService.findOneByCompositeKey(
						requestPayload.getPernr(), requestPayload.getSubty(), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda()));
				
				if (existingIT2006.isPresent()) {
					throw new DataViolationException("Duplicate IT2006 Data for [ssn=" + requestPayload.getPernr() + ",end_date=" +
							requestPayload.getEndda() + ",begin_date=" + requestPayload.getBegda());
				}
				
				it2006DB.setId(
						new ITCompositeKeys(requestPayload.getPernr(), "2006", requestPayload.getSubty(), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda())));
				
				
				it2006DB.setAnzhl(requestPayload.getAnzhl());
				it2006DB.setDeend(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDeend()));
				it2006DB.setDesta(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDesta()));
				it2006DB.setSeqnr(1L);
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			
			objectContainer = new RequestObjectComparatorContainer<>(it2006DB, requestPayload);
			
			
		} else {
			
			if (requestPayload.getKtart() != 0) {
				if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getKtart(), it2006DB.getKtart() != null ? it2006DB.getKtart().getKtart().intValue() : 0)) {
					try {
						Optional<T556B> newT556B = t556bQueryService.findById(Optional.ofNullable(requestPayload.getKtart()));
						if (newT556B.isPresent()) {
							it2006DB.setKtart(newT556B.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T517T not found... cancel the update process
					}
				}
			}
			
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getAnzhl(), it2006DB.getAnzhl() != null ? it2006DB.getAnzhl().doubleValue() : 0.0)) {
				it2006DB.setAnzhl(requestPayload.getAnzhl());
				requestPayload.setDataChanged(true);
			}
			
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getKverb(), it2006DB.getKverb() != null ? it2006DB.getKverb().doubleValue() : 0.0)) {
				it2006DB.setKverb(requestPayload.getKverb());
				requestPayload.setDataChanged(true);
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDesta())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDesta()), it2006DB.getDesta())) {
					it2006DB.setDesta(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDesta()));
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDeend())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDeend()), it2006DB.getDeend())) {
					it2006DB.setDeend(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDeend()));
					requestPayload.setDataChanged(true);
				}
			}

			objectContainer = new RequestObjectComparatorContainer<>(it2006DB, requestPayload);			
		}
		
		JSONObject json = new JSONObject(requestPayload);
		objectContainer.setEventData(json.toString());
		
		return objectContainer;
	}			
}