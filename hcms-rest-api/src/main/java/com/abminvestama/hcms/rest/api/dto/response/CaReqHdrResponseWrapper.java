package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.CaReqHdr;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class CaReqHdrResponseWrapper extends ResourceSupport {
	
	private String reqNo;
	private Long pernr;
	private Double amount;
	private String currency;
	private String description;
	private String attachment;
	private String bankName;
	private String bankAccount;
	private Integer process;
	private String notes;
	private String advanceCode;
	private String bukrs;
	private String createdByUsername;
	private String createdByFullname;
	private Date createdAt;
	private String updatedByUsername;
	private String updatedByFullname;
	private Date updatedAt;
	private String approved1ByUsername;
	private String approved1ByFullname;
	private Date approved1At;
	private String approved2ByUsername;
	private String approved2ByFullname;
	private Date approved2At;
	private String approved3ByUsername;
	private String approved3ByFullname;
	private Date approved3At;
	private String approved4ByUsername;
	private String approved4ByFullname;
	private Date approved4At;
	private String approvedAccountingByUsername;
	private String approvedAccountingByFullname;
	private Date approvedAccountingAt;
	private String approvedTreasuryByUsername;
	private String approvedTreasuryByFullname;
	private Date approvedTreasuryAt;
	private Date receivedAt;
	private String acctAp;
	private String reason;
	private String paymentStatus;
	private String paymentDate;
	private String docno;
	private String paymentStatus2;
	private String docno2;
	
	private CaReqHdrResponseWrapper() {}
	
	public CaReqHdrResponseWrapper(CaReqHdr caReqHdr) {
		if (caReqHdr == null) {
			new CaReqHdrResponseWrapper();
		} else {
			this
			.setReqNo(caReqHdr.getReqNo())
			.setPernr(caReqHdr.getPernr())			
			.setAmount(caReqHdr.getAmount() != null ? caReqHdr.getAmount() : 0)
			.setCurrency(caReqHdr.getCurrency() != null ? StringUtils.defaultString(caReqHdr.getCurrency(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setDescription(caReqHdr.getDescription() != null ? StringUtils.defaultString(caReqHdr.getDescription(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setAttachment(StringUtils.defaultString(caReqHdr.getAttachment(), StringUtils.EMPTY))
			.setBankName(StringUtils.defaultString(caReqHdr.getBankName(), StringUtils.EMPTY))
			.setBankAccount(StringUtils.defaultString(caReqHdr.getBankAccount(), StringUtils.EMPTY))
			.setProcess(caReqHdr.getProcess() != null ? caReqHdr.getProcess() : 0)
			.setNotes(StringUtils.defaultString(caReqHdr.getNotes(), StringUtils.EMPTY))
			.setAdvanceCode(StringUtils.defaultString(caReqHdr.getAdvanceCode(), StringUtils.EMPTY))
			.setBukrs(StringUtils.defaultString(caReqHdr.getBukrs().getBukrs(), StringUtils.EMPTY))
			.setCreatedByUsername(caReqHdr.getCreatedBy() != null ? caReqHdr.getCreatedBy().getUsername() : StringUtils.EMPTY)
			.setCreatedByFullname(caReqHdr.getCreatedBy() != null ? caReqHdr.getCreatedBy().getEmployee().getEname() : StringUtils.EMPTY)
			.setCreatedAt(caReqHdr.getCreatedAt())
			.setUpdatedByUsername(caReqHdr.getUpdatedBy() != null ? caReqHdr.getUpdatedBy().getUsername() : StringUtils.EMPTY)
			.setUpdatedByFullname(caReqHdr.getUpdatedBy() != null ? caReqHdr.getUpdatedBy().getEmployee().getEname() : StringUtils.EMPTY)
			.setUpdatedAt(caReqHdr.getUpdatedAt())
			.setApproved1ByUsername(caReqHdr.getApproved1At() != null ?
					(caReqHdr.getApproved1By() != null ? caReqHdr.getApproved1By().getUsername() : StringUtils.EMPTY) : StringUtils.EMPTY)
			.setApproved1ByFullname(caReqHdr.getApproved1At() != null ?
					(caReqHdr.getApproved1By() != null ? caReqHdr.getApproved1By().getEmployee().getEname() : StringUtils.EMPTY) : StringUtils.EMPTY)
			.setApproved1At(caReqHdr.getApproved1At())
			.setApproved2ByUsername(caReqHdr.getApproved2At() != null ?
					(caReqHdr.getApproved2By() != null ? caReqHdr.getApproved2By().getUsername() : StringUtils.EMPTY) : StringUtils.EMPTY)
			.setApproved2ByFullname(caReqHdr.getApproved2At() != null ?
					(caReqHdr.getApproved2By() != null ? caReqHdr.getApproved2By().getEmployee().getEname() : StringUtils.EMPTY) : StringUtils.EMPTY)
			.setApproved2At(caReqHdr.getApproved2At())
			.setApproved3ByUsername(caReqHdr.getApproved3By() != null ? caReqHdr.getApproved3By().getUsername() : StringUtils.EMPTY)
			.setApproved3ByFullname(caReqHdr.getApproved3By() != null ? caReqHdr.getApproved3By().getEmployee().getEname() : StringUtils.EMPTY)
			.setApproved3At(caReqHdr.getApproved3At())
			.setApproved4ByUsername(caReqHdr.getApproved4By() != null ? caReqHdr.getApproved4By().getUsername() : StringUtils.EMPTY)
			.setApproved4ByFullname(caReqHdr.getApproved4By() != null ? caReqHdr.getApproved4By().getEmployee().getEname() : StringUtils.EMPTY)
			.setApproved4At(caReqHdr.getApproved4At())
			.setApprovedAccountingByUsername(caReqHdr.getApprovedAccountingBy() != null ? caReqHdr.getApprovedAccountingBy().getUsername() : StringUtils.EMPTY)
			.setApprovedAccountingByFullname(caReqHdr.getApprovedAccountingBy() != null ? caReqHdr.getApprovedAccountingBy().getEmployee().getEname() : StringUtils.EMPTY)
			.setApprovedAccountingAt(caReqHdr.getApprovedAccountingAt())
			.setApprovedTreasuryByUsername(caReqHdr.getApprovedTreasuryBy() != null ? caReqHdr.getApprovedTreasuryBy().getUsername() : StringUtils.EMPTY)
			.setApprovedTreasuryByFullname(caReqHdr.getApprovedTreasuryBy() != null ? caReqHdr.getApprovedTreasuryBy().getEmployee().getEname() : StringUtils.EMPTY)
			.setApprovedTreasuryAt(caReqHdr.getApprovedTreasuryAt())
			.setReceivedAt(caReqHdr.getReceivedAt())
			.setAcctAp(StringUtils.defaultString(caReqHdr.getAcctAp(), StringUtils.EMPTY))
			.setReason(StringUtils.defaultString(caReqHdr.getReason(), StringUtils.EMPTY))
			.setPaymentStatus(StringUtils.defaultString(caReqHdr.getPaymentStatus(), StringUtils.EMPTY))
			.setPaymentDate(caReqHdr.getPaymentDate() != null ? CommonDateFunction.convertDateToStringYMD(caReqHdr.getPaymentDate()) : StringUtils.EMPTY)
			.setDocno(StringUtils.defaultString(caReqHdr.getDocno(), StringUtils.EMPTY))
			.setPaymentStatus2(StringUtils.defaultString(caReqHdr.getPaymentStatus2(), StringUtils.EMPTY))
			.setDocno2(StringUtils.defaultString(caReqHdr.getDocno2(), StringUtils.EMPTY));
		}
	}

	@JsonProperty("req_no")
	public String getReqNo() {
		return reqNo;
	}
	
	private CaReqHdrResponseWrapper setReqNo(String reqNo) {
		this.reqNo = reqNo;
		return this;
	}
	
	@JsonProperty("ssn")
	public Long getPernr() {
		return pernr;
	}
	
	private CaReqHdrResponseWrapper setPernr(Long pernr) {
		this.pernr = pernr;
		return this;
	}	
	
	@JsonProperty("amount")
	public Double getAmount() {
		return amount;
	}
	
	private CaReqHdrResponseWrapper setAmount(Double amount) {
		this.amount = amount;
		return this;
	}
	
	@JsonProperty("currency")
	public String getCurrency() {
		return currency;
	}
	
	private CaReqHdrResponseWrapper setCurrency(String currency) {
		this.currency = currency;
		return this;
	}
	
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}
	
	private CaReqHdrResponseWrapper setDescription(String description) {
		this.description = description;
		return this;
	}
	
	@JsonProperty("attachment")
	public String getAttachment() {
		return attachment;
	}
	
	private CaReqHdrResponseWrapper setAttachment(String attachment) {
		this.attachment = attachment;
		return this;
	}
	
	@JsonProperty("bank_name")
	public String getBankName() {
		return bankName;
	}
	
	private CaReqHdrResponseWrapper setBankName(String bankName) {
		this.bankName = bankName;
		return this;
	}
	
	@JsonProperty("bank_account")
	public String getBankAccount() {
		return bankAccount;
	}
	
	private CaReqHdrResponseWrapper setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
		return this;
	}
	
	@JsonProperty("process")
	public Integer getProcess() {
		return process;
	}
	
	private CaReqHdrResponseWrapper setProcess(Integer process) {
		this.process = process;
		return this;
	}
	
	@JsonProperty("notes")
	public String getNotes() {
		return notes;
	}
	
	private CaReqHdrResponseWrapper setNotes(String notes) {
		this.notes = notes;
		return this;
	}

	@JsonProperty("advance_code")
	public String getAdvanceCode() {
		return advanceCode;
	}
	
	private CaReqHdrResponseWrapper setAdvanceCode(String advanceCode) {
		this.advanceCode = advanceCode;
		return this;
	}
	
	@JsonProperty("bukrs")
	public String getBukrs() {
		return bukrs;
	}
	
	private CaReqHdrResponseWrapper setBukrs(String bukrs) {
		this.bukrs = bukrs;
		return this;
	}

	@JsonProperty("created_by_username")
	public String getCreatedByUsername() {
		return createdByUsername;
	}

	public CaReqHdrResponseWrapper setCreatedByUsername(String createdByUsername) {
		this.createdByUsername = createdByUsername;
		return this;
	}

	@JsonProperty("created_by_fullname")
	public String getCreatedByFullname() {
		return createdByFullname;
	}

	public CaReqHdrResponseWrapper setCreatedByFullname(String createdByFullname) {
		this.createdByFullname = createdByFullname;
		return this;
	}

	@JsonProperty("created_at")
	public Date getCreatedAt() {
		return createdAt;
	}

	public CaReqHdrResponseWrapper setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	@JsonProperty("updated_by_username")
	public String getUpdatedByUsername() {
		return updatedByUsername;
	}

	public CaReqHdrResponseWrapper setUpdatedByUsername(String updatedByUsername) {
		this.updatedByUsername = updatedByUsername;
		return this;
	}

	@JsonProperty("updated_by_fullname")
	public String getUpdatedByFullname() {
		return updatedByFullname;
	}

	public CaReqHdrResponseWrapper setUpdatedByFullname(String updatedByFullname) {
		this.updatedByFullname = updatedByFullname;
		return this;
	}

	@JsonProperty("updated_at")
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public CaReqHdrResponseWrapper setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
		return this;
	}

	@JsonProperty("approved1_by_username")
	public String getApproved1ByUsername() {
		return approved1ByUsername;
	}

	public CaReqHdrResponseWrapper setApproved1ByUsername(String approved1ByUsername) {
		this.approved1ByUsername = approved1ByUsername;
		return this;
	}

	@JsonProperty("approved1_by_fullname")
	public String getApproved1ByFullname() {
		return approved1ByFullname;
	}

	public CaReqHdrResponseWrapper setApproved1ByFullname(String approved1ByFullname) {
		this.approved1ByFullname = approved1ByFullname;
		return this;
	}

	@JsonProperty("approved1_at")
	public Date getApproved1At() {
		return approved1At;
	}

	public CaReqHdrResponseWrapper setApproved1At(Date approved1At) {
		this.approved1At = approved1At;
		return this;
	}

	@JsonProperty("approved2_by_username")
	public String getApproved2ByUsername() {
		return approved2ByUsername;
	}

	public CaReqHdrResponseWrapper setApproved2ByUsername(String approved2ByUsername) {
		this.approved2ByUsername = approved2ByUsername;
		return this;
	}

	@JsonProperty("approved2_by_fullname")
	public String getApproved2ByFullname() {
		return approved2ByFullname;
	}

	public CaReqHdrResponseWrapper setApproved2ByFullname(String approved2ByFullname) {
		this.approved2ByFullname = approved2ByFullname;
		return this;
	}

	@JsonProperty("approved2_at")
	public Date getApproved2At() {
		return approved2At;
	}

	public CaReqHdrResponseWrapper setApproved2At(Date approved2At) {
		this.approved2At = approved2At;
		return this;
	}

	@JsonProperty("approved3_by_username")
	public String getApproved3ByUsername() {
		return approved3ByUsername;
	}

	public CaReqHdrResponseWrapper setApproved3ByUsername(String approved3ByUsername) {
		this.approved3ByUsername = approved3ByUsername;
		return this;
	}

	@JsonProperty("approved3_by_fullname")
	public String getApproved3ByFullname() {
		return approved3ByFullname;
	}

	public CaReqHdrResponseWrapper setApproved3ByFullname(String approved3ByFullname) {
		this.approved3ByFullname = approved3ByFullname;
		return this;
	}

	@JsonProperty("approved3_at")
	public Date getApproved3At() {
		return approved3At;
	}

	public CaReqHdrResponseWrapper setApproved3At(Date approved3At) {
		this.approved3At = approved3At;
		return this;
	}

	@JsonProperty("approved4_by_username")
	public String getApproved4ByUsername() {
		return approved4ByUsername;
	}

	public CaReqHdrResponseWrapper setApproved4ByUsername(String approved4ByUsername) {
		this.approved4ByUsername = approved4ByUsername;
		return this;
	}

	@JsonProperty("approved4_by_fullname")
	public String getApproved4ByFullname() {
		return approved4ByFullname;
	}

	public CaReqHdrResponseWrapper setApproved4ByFullname(String approved4ByFullname) {
		this.approved4ByFullname = approved4ByFullname;
		return this;
	}

	@JsonProperty("approved4_at")
	public Date getApproved4At() {
		return approved4At;
	}

	public CaReqHdrResponseWrapper setApproved4At(Date approved4At) {
		this.approved4At = approved4At;
		return this;
	}

	@JsonProperty("approved_accounting_by_username")
	public String getApprovedAccountingByUsername() {
		return approvedAccountingByUsername;
	}

	public CaReqHdrResponseWrapper setApprovedAccountingByUsername(String approvedAccountingByUsername) {
		this.approvedAccountingByUsername = approvedAccountingByUsername;
		return this;
	}

	@JsonProperty("approved_accounting_by_fullname")
	public String getApprovedAccountingByFullname() {
		return approvedAccountingByFullname;
	}

	public CaReqHdrResponseWrapper setApprovedAccountingByFullname(String approvedAccountingByFullname) {
		this.approvedAccountingByFullname = approvedAccountingByFullname;
		return this;
	}

	@JsonProperty("approved_accounting_at")
	public Date getApprovedAccountingAt() {
		return approvedAccountingAt;
	}

	public CaReqHdrResponseWrapper setApprovedAccountingAt(Date approvedAccountingAt) {
		this.approvedAccountingAt = approvedAccountingAt;
		return this;
	}

	@JsonProperty("approved_treasury_by_username")
	public String getApprovedTreasuryByUsername() {
		return approvedTreasuryByUsername;
	}

	public CaReqHdrResponseWrapper setApprovedTreasuryByUsername(String approvedTreasuryByUsername) {
		this.approvedTreasuryByUsername = approvedTreasuryByUsername;
		return this;
	}

	@JsonProperty("approved_treasury_by_fullname")
	public String getApprovedTreasuryByFullname() {
		return approvedTreasuryByFullname;
	}

	public CaReqHdrResponseWrapper setApprovedTreasuryByFullname(String approvedTreasuryByFullname) {
		this.approvedTreasuryByFullname = approvedTreasuryByFullname;
		return this;
	}

	@JsonProperty("approved_treasury_at")
	public Date getApprovedTreasuryAt() {
		return approvedTreasuryAt;
	}

	public CaReqHdrResponseWrapper setApprovedTreasuryAt(Date approvedTreasuryAt) {
		this.approvedTreasuryAt = approvedTreasuryAt;
		return this;
	}

	@JsonProperty("received_at")
	public Date getReceivedAt() {
		return receivedAt;
	}

	public CaReqHdrResponseWrapper setReceivedAt(Date receivedAt) {
		this.receivedAt = receivedAt;
		return this;
	}
	
	@JsonProperty("acct_ap")
	public String getAcctAp() {
		return acctAp;
	}
	
	private CaReqHdrResponseWrapper setAcctAp(String acctAp) {
		this.acctAp = acctAp;
		return this;
	}
	
	@JsonProperty("reason")
	public String getReason() {
		return reason;
	}
	
	private CaReqHdrResponseWrapper setReason(String reason) {
		this.reason = reason;
		return this;
	}
	
	@JsonProperty("finance_payment_status")
	public String getPaymentStatus() {
		return paymentStatus;
	}

	private CaReqHdrResponseWrapper setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
		return this;
	}

	@JsonProperty("finance_payment_date")
	public String getPaymentDate() {
		return paymentDate;
	}

	private CaReqHdrResponseWrapper setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
		return this;
	}

	@JsonProperty("finance_document_number")
	public String getDocno() {
		return docno;
	}

	private CaReqHdrResponseWrapper setDocno(String docno) {
		this.docno = docno;
		return this;
	}

	@JsonProperty("finance_payment_status2")
	public String getPaymentStatus2() {
		return paymentStatus2;
	}

	private CaReqHdrResponseWrapper setPaymentStatus2(String paymentStatus2) {
		this.paymentStatus2 = paymentStatus2;
		return this;
	}

	@JsonProperty("finance_document_number2")
	public String getDocno2() {
		return docno2;
	}

	private CaReqHdrResponseWrapper setDocno2(String docno2) {
		this.docno2 = docno2;
		return this;
	}
	
}

