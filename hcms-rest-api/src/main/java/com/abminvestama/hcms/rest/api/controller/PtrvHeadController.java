package com.abminvestama.hcms.rest.api.controller;



import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.PositionRelation;
import com.abminvestama.hcms.core.model.entity.PtrvHead;
import com.abminvestama.hcms.core.model.entity.PtrvSrec;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.PtrvHeadCommandService;
import com.abminvestama.hcms.core.service.api.business.command.PtrvSrecCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0009QueryService;
import com.abminvestama.hcms.core.service.api.business.query.PtrvHeadQueryService;
import com.abminvestama.hcms.core.service.api.business.query.PtrvSrecQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.helper.PtrvHeadMapper;
import com.abminvestama.hcms.core.service.helper.PtrvHeadSoapObject;
import com.abminvestama.hcms.core.service.helper.PtrvSrecSoapObject;
import com.abminvestama.hcms.core.service.helper.ZmedTrxhdrMapper;
import com.abminvestama.hcms.core.service.helper.ZmedTrxhdrSoapObject;
import com.abminvestama.hcms.core.service.helper.ZmedTrxhdrsSoapObject;
import com.abminvestama.hcms.core.service.util.MailService;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.EcostTransactionRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.PtrvHeadRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.PtrvSrecRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.EcostTransactionResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.PtrvHeadResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ZmedTrxhdrResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.PtrvHeadRequestBuilderUtil;
import com.abminvestama.hcms.rest.api.helper.PtrvSrecRequestBuilderUtil;



/**
 * 
 * @since 1.0.0
 * @version 1.1.0
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.1.1</td><td>Baihaki</td><td>Modify createBulkWithItems: set createdAt and createdBy on user create (process=1)</td></tr>
 *     <tr><td>1.1.0</td><td>Baihaki</td><td>Add methods: findByDateWithItems</td></tr>
 *     <tr><td>1.0.11</td><td>Baihaki</td><td>Add notification email to all HR Admin & all superior</td></tr>
 *     <tr><td>1.0.10</td><td>Baihaki</td><td>Set Main Bank Detail on create, and on synchronize document status (Paid status)</td></tr>
 *     <tr><td>1.0.9</td><td>Baihaki</td><td>setPaymentStatus to default "N" on create</td></tr>
 *     <tr><td>1.0.8</td><td>Baihaki</td><td>Add parameter "finance_payment_status" on findByCriteria to filter by paymentStatus or not (optional)</td></tr>
 *     <tr><td>1.0.7</td><td>Baihaki</td><td>Add methods: updateDocumentStatus, synchronize Medical Document Status from SAP</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Add editBulkWithItems method</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add form parameter on findByCriteria</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: findOneByComposite</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: findByCriteria</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add edit method and Post to MuleSoft</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add createBulkWithItems method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/ptrv_head")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class PtrvHeadController extends AbstractResource {
	
	private PtrvHeadCommandService ptrvHeadCommandService;
	private PtrvHeadQueryService ptrvHeadQueryService;
	private PtrvHeadRequestBuilderUtil ptrvHeadRequestBuilderUtil;
	
	private PtrvSrecCommandService ptrvSrecCommandService;
	private PtrvSrecQueryService ptrvSrecQueryService;
	private PtrvSrecRequestBuilderUtil ptrvSrecRequestBuilderUtil;
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	
	private MuleConsumerService muleConsumerService;
	private MailService mailService;
	private CommonServiceFactory commonServiceFactory;
	private IT0009QueryService it0009QueryService;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	@Autowired
	void setPtrvHeadCommandService(PtrvHeadCommandService ptrvHeadCommandService) {
		this.ptrvHeadCommandService = ptrvHeadCommandService;
	}
	
	@Autowired
	void setPtrvHeadQueryService(PtrvHeadQueryService ptrvHeadQueryService) {
		this.ptrvHeadQueryService = ptrvHeadQueryService;
	}
	
	@Autowired
	void setPtrvHeadRequestBuilderUtil(PtrvHeadRequestBuilderUtil ptrvHeadRequestBuilderUtil) {
		this.ptrvHeadRequestBuilderUtil = ptrvHeadRequestBuilderUtil;
	}
	
	@Autowired
	void setPtrvSrecRequestBuilderUtil(PtrvSrecRequestBuilderUtil ptrvSrecRequestBuilderUtil) {
		this.ptrvSrecRequestBuilderUtil = ptrvSrecRequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	void setIT0009QueryService(IT0009QueryService it0009QueryService) {
		this.it0009QueryService = it0009QueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@Autowired
	void setPtrvSrecQueryService(PtrvSrecQueryService ptrvSrecQueryService) {
		this.ptrvSrecQueryService = ptrvSrecQueryService;
	}
	
	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}

	@Autowired
	void setPtrvSrecCommandService(PtrvSrecCommandService ptrvSrecCommandService) {
		this.ptrvSrecCommandService = ptrvSrecCommandService;
	}

	@Autowired
	void setMailService(MailService mailService) {
		this.mailService = mailService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	private void updateDocumentStatus(List<PtrvHead> listPtrvHead) {
		
		List<PtrvHead> listPtrvHeadForSync =  new ArrayList<PtrvHead>();
		List<PtrvHead> listPtrvHeadForCurrentSync =  new ArrayList<PtrvHead>();
		listPtrvHead.forEach(ptrvHead -> {
			// sync / update document status only if last sync was 12 minutes past
			if (ptrvHead.getLastSync() == null ||
					(new Date().getTime() - ptrvHead.getLastSync().getTime()) / (60 * 1000) % 60 >= 12)
			listPtrvHeadForSync.add(ptrvHead);
			listPtrvHeadForCurrentSync.add(ptrvHead);
		});
		if (!listPtrvHeadForSync.isEmpty()) {
			String bukrs = null;
			
			String year = CommonDateFunction.convertDateToStringY(listPtrvHeadForSync.get(0).getDates());
			List<PtrvHead> listPtrvHeadForNextSync =  new ArrayList<PtrvHead>();
			for (PtrvHead ptrvHead : listPtrvHeadForCurrentSync) {
				if (!year.equals(CommonDateFunction.convertDateToStringY(ptrvHead.getDates()))) {
					listPtrvHeadForNextSync.add(ptrvHead);
					listPtrvHeadForSync.remove(ptrvHead);
				}
			}
			// update document status of past year expense transaction
			if (listPtrvHeadForNextSync.size() > 0) {
				updateDocumentStatus(listPtrvHeadForNextSync);
			}
			
			try {
				Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
				bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			ZmedTrxhdrsSoapObject zmedTrxhdrsSoapObject = new ZmedTrxhdrsSoapObject(bukrs, year);
			
			List<ZmedTrxhdrSoapObject> listZmedTrxhdrSoapObject =  new ArrayList<ZmedTrxhdrSoapObject>();
			for (PtrvHead ptrvHead : listPtrvHeadForSync) {
				listZmedTrxhdrSoapObject.add(new ZmedTrxhdrSoapObject(
						ptrvHead.getBukrs().getBukrs(), ptrvHead.getReinr().toString(), ptrvHead.getPernr().toString()));
			}
			zmedTrxhdrsSoapObject.setListTrxHeader(listZmedTrxhdrSoapObject);
			
			List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.ZMEDDOCTRXHDR_FETCH_ABM.service(),
					BAPIFunction.ZMEDDOCTRXHDR_FETCH_ABM_URI.service(), zmedTrxhdrsSoapObject);
			
			for (int i = 0; i < list.size(); i++) {
				// synchronize PtrvHead Document Status fields (in different thread) with the fetched fields from SAP
				if (i > 0 && list.get(i).get("zclmno") != null) {
					PtrvHead ptrvHeadDB = listPtrvHeadForSync.get(i-1);
					PtrvHead ptrvHeadEntity = new PtrvHeadMapper(list.get(i)).getPtrvHead();
					for (PtrvHead item : listPtrvHeadForSync) {
						if (list.get(i).get("zclmno").equals(item.getReinr().toString())) {
							ptrvHeadDB =  item;
							break;
						}
					}
					ptrvHeadDB.setPaymentStatus(ptrvHeadEntity.getPaymentStatus());
					ptrvHeadDB.setPaymentDate(ptrvHeadEntity.getPaymentDate());
					ptrvHeadDB.setDocno(ptrvHeadEntity.getDocno());
					ptrvHeadDB.setLastSync(new Date());
					ptrvHeadDB.setPaymentStatus2(ptrvHeadEntity.getPaymentStatus2());
					ptrvHeadDB.setDocno2(ptrvHeadEntity.getDocno2());
					if (ptrvHeadEntity.getPaymentStatus().equals("P") || ptrvHeadEntity.getPaymentStatus2().equals("P")) {
						// Set Employee's Main Bank Detail
						Page<IT0009> page = it0009QueryService.findByPernrAndStatus(ptrvHeadDB.getId().getPernr().longValue(), DocumentStatus.PUBLISHED.toString(), 1);
						if (page.hasContent() && page.getContent().get(0).getSubty().getId().getSubty().equals("0")) {
							IT0009 mainBank = page.getContent().get(0);
							ptrvHeadDB.setBanka(mainBank.getBankl() != null ? StringUtils.defaultString(mainBank.getBankl().getBanka()) : StringUtils.EMPTY);
							ptrvHeadDB.setBankn(StringUtils.defaultString(mainBank.getBankn()));
						}
					}
				    //ptrvHeadCommandService.threadUpdate(ptrvHeadDB);
				    try {
						ptrvHeadCommandService.save(ptrvHeadDB, null);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		
	}

	/*
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_criteria", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<PtrvHeadResponseWrapper>> findByCriteria(
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "process", required = false) String process,
			@RequestParam(value = "finance_payment_status", required = false, defaultValue = "") String status,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {

		Page<PtrvHeadResponseWrapper> responsePage = new PageImpl<PtrvHeadResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<PtrvHeadResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<PtrvHead> resultPage = null;
		String bukrs = null;
		Long pernr = null;
		String[] processes = StringUtils.isEmpty(process) ? null : process.split(",");
		String notStatus = status;
		if (status.equalsIgnoreCase("N")) {
			notStatus = "P";
		} else if (status.equalsIgnoreCase("P")) {
			notStatus = "N";
		}
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		
		try {
			if (StringUtils.isNotEmpty(ssn)) {
				pernr = Long.valueOf(ssn);
				if (!currentUser.get().getRoles().contains(adminRole.get()) &&
						!currentUser.get().getEmployee().getPernr().equals(pernr) ) {
					throw new AuthorizationException(
							"Insufficient Privileges. You can't fetch data of other Employee.");
				}
				resultPage = (processes != null && processes.length > 1) ?
						ptrvHeadQueryService.findByPernrAndProcessesWithPaging(pernr.longValue(), processes, notStatus, pageNumber) :
							ptrvHeadQueryService.findByPernrAndProcessWithPaging(pernr.longValue(), process, notStatus, pageNumber);
			} else {
				// no specified ssn, MUST BE ADMIN
				if (!currentUser.get().getRoles().contains(adminRole.get())) {
					throw new AuthorizationException(
							"Insufficient Privileges. Please login as 'ADMIN'!");
				}
				bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
				resultPage = (processes != null && processes.length > 1) ?
						ptrvHeadQueryService.findByBukrsAndProcessesWithPaging(bukrs, processes, notStatus, pageNumber) :
							ptrvHeadQueryService.findByBukrsAndProcessWithPaging(bukrs, process, notStatus, pageNumber);
			}
			
			updateDocumentStatus(resultPage.getContent());
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			
			if (resultPage.hasContent()) {

				List<PtrvHeadResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(ptrvHead -> {
					records.add(new PtrvHeadResponseWrapper(ptrvHead));
				});
				responsePage = new PageImpl<PtrvHeadResponseWrapper>(records, ptrvHeadQueryService.createPageRequest(pageNumber),
						resultPage.getTotalElements());
				response.setData(responsePage);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(PtrvHeadController.class).findByCriteria(ssn, process, notStatus, pageNumber)).withSelfRel());
		
		return response;
	}
	*/
	
	@RequestMapping(method = RequestMethod.GET, value = "/reinr/{reinr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<EcostTransactionResponseWrapper> findOneByComposite(@PathVariable Long reinr,
			@RequestParam(value = "ssn", required = true) String ssn,
			@RequestParam(value = "hdvrs", required = false, defaultValue = "1") int hdvrs) throws Exception {
		
		APIResponseWrapper<EcostTransactionResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			Long pernr = Long.valueOf(ssn);
			Optional<PtrvHead> ptrvHeadObject = ptrvHeadQueryService.findOneByCompositeKey(hdvrs, pernr, reinr);
			
			if (!ptrvHeadObject.isPresent()) {
				response.setMessage("No Data Found");
				response.setData(new EcostTransactionResponseWrapper(null, null));
			} else {
				ArrayList<PtrvSrec> listPtrvSrec = (ArrayList<PtrvSrec>) ptrvSrecQueryService.findByTripnumber(ptrvHeadObject.get().getReinr()).
						stream().collect(Collectors.toList());
				PtrvSrec[] ptrvSrec = listPtrvSrec.isEmpty() ? new PtrvSrec[0] : new PtrvSrec[listPtrvSrec.size()];
				List<PtrvHead> listOfPtrvHead = new ArrayList<>();
				listOfPtrvHead.add(ptrvHeadObject.get());
				updateDocumentStatus(listOfPtrvHead);
				response.setData(new EcostTransactionResponseWrapper(ptrvHeadObject.get(), listPtrvSrec.toArray(ptrvSrec)));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(PtrvHeadController.class).findOneByComposite(reinr, ssn, hdvrs)).withSelfRel());
		return response;
	}

	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_criteria", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<PtrvHeadResponseWrapper>> findByCriteria(
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "process", required = false) String process,
			@RequestParam(value = "form", required = false, defaultValue = "0,1,2") String form,
			@RequestParam(value = "finance_payment_status", required = false, defaultValue = "") String status,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {

		Page<PtrvHeadResponseWrapper> responsePage = new PageImpl<PtrvHeadResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<PtrvHeadResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<PtrvHead> resultPage = null;
		String bukrs = null;
		Long pernr = null;
		String[] processes = StringUtils.isEmpty(process) ? null : process.split(",");
		String[] formsString = form.split(",");
		String notStatus = status;
		if (status.equalsIgnoreCase("N")) {
			notStatus = "P";
		} else if (status.equalsIgnoreCase("P")) {
			notStatus = "N";
		}
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		Optional<Role> superiorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_SUPERIOR"));
		
		try {
			short[] forms = new short[formsString.length];
			for (int i=0; i < formsString.length; i++) {
				forms[i] = Short.valueOf(formsString[i]).shortValue();
			}
			
			if (StringUtils.isNotEmpty(ssn)) {
				pernr = Long.valueOf(ssn);
				if (!currentUser.get().getRoles().contains(adminRole.get()) &&
						!currentUser.get().getEmployee().getPernr().equals(pernr) ) {
					throw new AuthorizationException(
							"Insufficient Privileges. You can't fetch data of other Employee.");
				}
				resultPage = (processes != null && processes.length > 1) ?
						ptrvHeadQueryService.findByPernrAndProcessesWithPaging(pernr.longValue(), processes, forms, notStatus, pageNumber) :
							ptrvHeadQueryService.findByPernrAndProcessWithPaging(pernr.longValue(), process, forms, notStatus, pageNumber);
			} else {
				// no specified ssn, MUST BE ADMIN / SUPERIOR 
				if (!currentUser.get().getRoles().contains(adminRole.get()) && !currentUser.get().getRoles().contains(superiorRole.get())) {
					throw new AuthorizationException(
							"Insufficient Privileges. Please login as 'ADMIN' or 'SUPERIOR'!");
				}
				// If NOT ADMIN (ROLE SUPERIOR ONLY) can only fetch Expenses which have been released by Admin (process=1 -> changed to process=2)
				if (!currentUser.get().getRoles().contains(adminRole.get()) && process != null && !process.equals("2")) {
					throw new AuthorizationException(
							"Insufficient Privileges. Please login as 'ADMIN'!");
				}
				String superiorPosition = null;
				// If NOT ADMIN (ROLE SUPERIOR ONLY) set superiorPosition parameter as additional filter
				if (!currentUser.get().getRoles().contains(adminRole.get()) && process != null && process.equals("2")) {
					superiorPosition = currentUser.get().getEmployee().getPosition().getId();
				}
				bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
				resultPage = (processes != null && processes.length > 1) ?
						ptrvHeadQueryService.findByBukrsAndProcessesWithPaging(bukrs, processes, forms, notStatus, pageNumber) :
							ptrvHeadQueryService.findByBukrsAndProcessWithPaging(bukrs, process, forms, notStatus, superiorPosition, pageNumber);
			}
			
			updateDocumentStatus(resultPage.getContent());
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			
			if (resultPage.hasContent()) {

				List<PtrvHeadResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(ptrvHead -> {
					records.add(new PtrvHeadResponseWrapper(ptrvHead));
				});
				responsePage = new PageImpl<PtrvHeadResponseWrapper>(records, ptrvHeadQueryService.createPageRequest(pageNumber),
						resultPage.getTotalElements());
				response.setData(responsePage);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(PtrvHeadController.class).findByCriteria(ssn, process, form, status, pageNumber)).withSelfRel());
		
		return response;
	}
	
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_date/{datv1}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<EcostTransactionResponseWrapper>> findByDateWithItems(
			@PathVariable String datv1,
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "process", required = false, defaultValue = "0,1,2,3") String process) throws Exception {

		ArrayData<EcostTransactionResponseWrapper> bunchOfEcost = new ArrayData<>();
		APIResponseWrapper<ArrayData<EcostTransactionResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfEcost);
		
		String bukrs = null;
		Long pernr = null;
		String[] processes = StringUtils.isEmpty(process) ? null : process.split(",");
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		Optional<Role> superiorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_SUPERIOR"));
		
		try {
			
			if (StringUtils.isNotEmpty(ssn)) {
				pernr = Long.valueOf(ssn);
				if (!currentUser.get().getRoles().contains(adminRole.get()) &&
						!currentUser.get().getEmployee().getPernr().equals(pernr) ) {
					throw new AuthorizationException(
							"Insufficient Privileges. You can't fetch data of other Employee.");
				}
				
			} else {
				// no specified ssn, MUST BE ADMIN / SUPERIOR 
				if (!currentUser.get().getRoles().contains(adminRole.get()) && !currentUser.get().getRoles().contains(superiorRole.get())) {
					throw new AuthorizationException(
							"Insufficient Privileges. Please login as 'ADMIN' or 'SUPERIOR'!");
				}
				String superiorPosition = null;
				// If NOT ADMIN (ROLE SUPERIOR ONLY) set superiorPosition parameter as additional filter
				if (!currentUser.get().getRoles().contains(adminRole.get()) && process != null && process.equals("2")) {
					superiorPosition = currentUser.get().getEmployee().getPosition().getId();
				}
				bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
			}
			
			List<PtrvHead> listOfPtrvHead = new ArrayList<>();
			Optional<Collection<PtrvHead>> collectionOfPtrvHead = ptrvHeadQueryService.findByPernrAndProcessesAndDatv1(pernr, processes,
					CommonDateFunction.convertDateRequestParameterIntoDate(datv1));
			if (collectionOfPtrvHead.isPresent() && collectionOfPtrvHead.get().size() > 0) {
				listOfPtrvHead = collectionOfPtrvHead.get().stream().collect(Collectors.toList());
				List<EcostTransactionResponseWrapper> records = new ArrayList<>();
				for (PtrvHead ptrvHead : listOfPtrvHead) {
					
					ArrayList<PtrvSrec> listPtrvSrec = (ArrayList<PtrvSrec>) ptrvSrecQueryService.findByTripnumber(ptrvHead.getReinr()).
							stream().collect(Collectors.toList());
					PtrvSrec[] ptrvSrec = listPtrvSrec.isEmpty() ? new PtrvSrec[0] : new PtrvSrec[listPtrvSrec.size()];
					records.add(new EcostTransactionResponseWrapper(ptrvHead, listPtrvSrec.toArray(ptrvSrec)));
				}
				bunchOfEcost.setItems(records.toArray(new EcostTransactionResponseWrapper[records.size()]));
			} else {
				response.setMessage("No Data Found");
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(PtrvHeadController.class).findByDateWithItems(datv1, ssn, process)).withSelfRel());
		
		return response;
	}
	
	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated PtrvHeadRequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> ptrvHeadHCMSEntity = hcmsEntityQueryService.findByEntityName(PtrvHead.class.getSimpleName());
			if (!ptrvHeadHCMSEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + PtrvHead.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<PtrvHead, PtrvHeadRequestWrapper, String> newObjectContainer 
					= ptrvHeadRequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				
				Optional<PtrvHead> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
				//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					newObjectContainer.getEntity().get().setPaymentStatus("N");
					savedEntity = ptrvHeadCommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create PtrvHead data. Please check your data!");
				}
				
				
				
				
				StringBuilder key = new StringBuilder("hdvrs=" + savedEntity.get().getId().getHdvrs());
				key.append(",pernr=" + savedEntity.get().getId().getPernr());
				key.append(",reinr=" + savedEntity.get().getId().getReinr());
				
				EventLog eventLog = new EventLog(ptrvHeadHCMSEntity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				
				
				PtrvHeadResponseWrapper responseWrapper = new PtrvHeadResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);
				
				/* Post to MuleSoft */
				/*
				PtrvHead ptrvHeadEntity = savedEntity.get();
				ArrayList<PtrvSrec> listPtrvSrec = (ArrayList<PtrvSrec>) ptrvSrecQueryService.findByTripnumber(ptrvHeadEntity.getId().getReinr()).
						stream().collect(Collectors.toList());
				
				PtrvHeadSoapObject ptrvHeadSoapObject = new PtrvHeadSoapObject(ptrvHeadEntity);
				List<PtrvSrecSoapObject> ptrvSrecSoapObjects = new ArrayList<PtrvSrecSoapObject>();
				
				for (int i=0; i<listPtrvSrec.size(); i++) {
					ptrvSrecSoapObjects.add(new PtrvSrecSoapObject(listPtrvSrec.get(i)));
				}
				ptrvHeadSoapObject.setTripReceipts(ptrvSrecSoapObjects);

				HashMap<String, String> resultMap = (HashMap<String, String>) muleConsumerService.
						postToSAPSync(BAPIFunction.ECOSTTRIPHDR_CREATE.service(), BAPIFunction.ECOSTTRIPHDR_CREATE_URI.service(), ptrvHeadSoapObject);
				if (resultMap.get("errorCode") == StringUtils.EMPTY && resultMap.get("tripnumber") != StringUtils.EMPTY &&
						resultMap.get("message").split(" ").length == 1) {
					String message = resultMap.get("message");
					Long returnTripnumber = Long.parseLong(message);
					// update the claim number returned from SAP, and save the updated ptrvHead to database
					//ptrvHeadEntity.setZclmno(ptrvHeadEntity.getZclmno());
					//ptrvHead = ptrvHeadCommandService.save(ptrvHeadEntity, currentUser.isPresent() ? currentUser.get() : null);
					Optional<PtrvHead> ptrvHead = ptrvHeadCommandService.updateReinr(returnTripnumber, ptrvHeadEntity, currentUser.isPresent() ? currentUser.get() : null);
					//ptrvHead = ptrvHeadCommandService.save(ptrvHeadEntity, currentUser.isPresent() ? currentUser.get() : null);
					if (!ptrvHead.isPresent()) {
						throw new CannotPersistException("SAP Result: Cannot update PtrvHead (EmployeeCost Header) data. Please check your data!");
					}
					*/
					/*
					ptrvHeadSoapObject = new PtrvHeadSoapObject(null, returnTripnumber, null);
					List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.ECOSTTRIPHDR_FETCH.service(),
							BAPIFunction.ECOSTTRIPHDR_FETCH_URI.service(), ptrvHeadSoapObject);
					// synchronize PtrvHead fields (in different thread) with the fetched fields from SAP
					if (list.get(0).get("zclmno") != null) {
					    ptrvHeadEntity = new PtrvHeadMapper(list.get(0)).getPtrvHead();
					    ptrvHeadEntity.setBukrs(ptrvHead.get().getBukrs());
					    ptrvHeadEntity.setProcess(ptrvHead.get().getProcess());
					    ptrvHeadCommandService.threadUpdate(ptrvHeadEntity);
					}
					*/
					/*
					for (PtrvSrec ptrvSrec : listPtrvSrec) {
						ptrvSrec.setReinr(returnTripnumber);
						Optional<PtrvSrec> ptrvSrecUpdate = ptrvSrecCommandService.save(ptrvSrec, currentUser.isPresent() ? currentUser.get() : null);
						if (!ptrvSrecUpdate.isPresent()) {
							throw new CannotPersistException("SAP Result: Cannot update PtrvSrec (EmployeeCost Receipts) data. Please check your data!");
						}
					}
					
					Optional<User> submitUser = userQueryService.findByPernr(ptrvHead.get().getPernr());
					if (submitUser.isPresent()) {
						User user = submitUser.get();
						String processed = "CREATED";
				        mailService.sendMail(user.getEmail(), "UPDATE ".
				    		concat(ptrvHeadHCMSEntity.get().getEntityName()),
				    		"Dear <b>".concat(user.getEmployee().getSname()).concat("</b><br><br>Your Medical Claim Number <b>".
				    		concat(returnTripnumber.toString()).concat("</b> has been ").
				    		concat(processed).concat(" by your HR Admin or Superior.").concat("<br><br>Thank You.")));
					}
				    
				} else {
					response.setMessage(resultMap.get("errorMessage"));
					throw new CannotPersistException("Cannot post EmployeeCost Transaction to SAP! All data operation will be rollback to initial state.");
				}
				*/
				/* ************************** */
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(PtrvHeadController.class).create(request, result)).withSelfRel());		
		return response;
	}

	


	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE, value = "/bulk")
	public APIResponseWrapper<?> createBulkWithItems(@RequestBody  EcostTransactionRequestWrapper request[] //@Validated
	, BindingResult result)
	throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {

		ArrayData<EcostTransactionResponseWrapper> bunchOfEcost = new ArrayData<>();
		APIResponseWrapper<ArrayData<EcostTransactionResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfEcost);
		
		if (result.hasErrors()) {
		} else {
						
			Optional<HCMSEntity> ptrvSrecEntity = hcmsEntityQueryService.findByEntityName(PtrvSrec.class.getSimpleName());
			if (!ptrvSrecEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + PtrvSrec.class.getSimpleName() + "' !!!");
			}
			
			Optional<HCMSEntity> ptrvHeadEntity = hcmsEntityQueryService.findByEntityName(PtrvHead.class.getSimpleName());
			if (!ptrvHeadEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + PtrvHead.class.getSimpleName() + "' !!!");
			}
			
			try {
				int size = request.length;
				RequestObjectComparatorContainer<PtrvHead, PtrvHeadRequestWrapper, String> newObjectContainerHdr; 				
				RequestObjectComparatorContainer<PtrvSrec, PtrvSrecRequestWrapper, String> newObjectContainerItm; 
				 
				List<EcostTransactionResponseWrapper> records = new ArrayList<>();				
				
				for(int j = 0; j<size; j++){
				
					PtrvHeadRequestWrapper TrxHdr = request[j].getTrxHdr();
					newObjectContainerHdr= ptrvHeadRequestBuilderUtil.compareAndReturnUpdatedData(TrxHdr, null);						
					
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
					if (!currentUser.isPresent()) {
						throw new AuthorizationException(
								"Insufficient create privileges. Please login as Admin!");						
					}	
					
					Optional<PtrvHead> savedEntity = newObjectContainerHdr.getEntity();

					if (savedEntity.isPresent()) {
						//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
						savedEntity.get().setPaymentStatus("N");
					    // Set Employee's Main Bank Detail
						Page<IT0009> page = it0009QueryService.findByPernrAndStatus(savedEntity.get().getId().getPernr().longValue(), DocumentStatus.PUBLISHED.toString(), 1);
						if (page.hasContent() && page.getContent().get(0).getSubty().getId().getSubty().equals("0")) {
							IT0009 mainBank = page.getContent().get(0);
							savedEntity.get().setBanka(mainBank.getBankl() != null ? StringUtils.defaultString(mainBank.getBankl().getBanka()) : StringUtils.EMPTY);
							savedEntity.get().setBankn(StringUtils.defaultString(mainBank.getBankn()));
						}
						// added on version 1.1.1
						newObjectContainerHdr.getEntity().get().setCreatedBy(currentUser.get());
						newObjectContainerHdr.getEntity().get().setCreatedAt(new Date());
						// end added on version 1.1.1
						savedEntity = ptrvHeadCommandService.save(newObjectContainerHdr.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}		

					if (!savedEntity.isPresent()) {
						throw new CannotPersistException("Cannot create PtrvSrec data. Please check your data!");
					}

					StringBuilder key = new StringBuilder("hdvrs=" + savedEntity.get().getId().getHdvrs());
					key.append(",pernr=" + savedEntity.get().getId().getPernr());
					key.append(",reinr=" + savedEntity.get().getId().getReinr());
					
					EventLog eventLog = new EventLog(ptrvHeadEntity.get(), key.toString(), OperationType.CREATE, 
							(newObjectContainerHdr.getEventData().isPresent() ? newObjectContainerHdr.getEventData().get() : ""));
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());

					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}

					int itmno = 1; 
					
					PtrvSrec savedEntityItems[] = new PtrvSrec[request[j].getTrxItm().length];
					int itemSize = request[j].getTrxItm().length;
					
					for(int i = 0; i < itemSize; i++){
						
						request[j].getTrxItm()[i].setReceiptno(String.valueOf(itmno));
						newObjectContainerItm = ptrvSrecRequestBuilderUtil.compareAndReturnUpdatedData(request[j].getTrxItm()[i], null);

						Optional<PtrvSrec> savedEntityItm = newObjectContainerItm.getEntity();

						if (newObjectContainerItm.getEntity().isPresent()) {
							//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
							savedEntityItm = ptrvSrecCommandService.save(newObjectContainerItm.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						}		

						if (!savedEntityItm.isPresent()) {
							throw new CannotPersistException("Cannot create PtrvSrec data. Please check your data!");
						}

						StringBuilder keyItm = new StringBuilder("mandt=" + savedEntityItm.get().getId().getMandt());
						keyItm.append(",prnr=" + savedEntityItm.get().getId().getPernr());
						keyItm.append(",reinr=" + savedEntityItm.get().getId().getReinr());
						keyItm.append(",perio=" + savedEntityItm.get().getId().getPerio());
						keyItm.append(",receiptno=" + savedEntityItm.get().getId().getReceiptno());

						EventLog eventLogItm = new EventLog(ptrvSrecEntity.get(), keyItm.toString(), OperationType.CREATE, 
								(newObjectContainerItm.getEventData().isPresent() ? newObjectContainerItm.getEventData().get() : ""));
						Optional<EventLog> savedEventLogItm = eventLogCommandService.save(eventLogItm, currentUser.get());

						if (!savedEventLogItm.isPresent()) {
							throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
						}
						
						savedEntityItems[i] = savedEntityItm.get();

						itmno++;
						/*
						// if not set, then set form_type as referred from item's sub form type
						if (savedEntity.get().getForm() == 0 && savedEntityItm.get().getSubt706b1() != null) {
							savedEntity.get().setForm(savedEntityItm.get().getSubt706b1().getSubForm());
						}
						*/
					}
					
					records.add(new EcostTransactionResponseWrapper(savedEntity.get(), savedEntityItems));
					
					
					
					/* Post to MuleSoft */
					/*
					PtrvHead savedPtrvHead = savedEntity.get();
					ArrayList<PtrvSrec> listPtrvSrec = (ArrayList<PtrvSrec>) ptrvSrecQueryService.findByTripnumber(savedPtrvHead.getId().getReinr()).
							stream().collect(Collectors.toList());
					
					PtrvHeadSoapObject ptrvHeadSoapObject = new PtrvHeadSoapObject(savedPtrvHead);
					List<PtrvSrecSoapObject> ptrvSrecSoapObjects = new ArrayList<PtrvSrecSoapObject>();
					
					for (int i=0; i<listPtrvSrec.size(); i++) {
						ptrvSrecSoapObjects.add(new PtrvSrecSoapObject(listPtrvSrec.get(i)));
					}
					ptrvHeadSoapObject.setTripReceipts(ptrvSrecSoapObjects);

					HashMap<String, String> resultMap = (HashMap<String, String>) muleConsumerService.
							postToSAPSync(BAPIFunction.ECOSTTRIPHDR_CREATE.service(), BAPIFunction.ECOSTTRIPHDR_CREATE_URI.service(), ptrvHeadSoapObject);
					if (resultMap.get("errorCode") == StringUtils.EMPTY && resultMap.get("tripnumber") != StringUtils.EMPTY &&
							resultMap.get("message").split(" ").length == 1) {
						String message = resultMap.get("message");
						Long returnTripnumber = Long.parseLong(message);
						// update the trip number returned from SAP, and save the updated ptrvHead to database
						Optional<PtrvHead> ptrvHead = ptrvHeadCommandService.updateReinr(returnTripnumber, savedPtrvHead,
								currentUser.isPresent() ? currentUser.get() : null);
						if (!ptrvHead.isPresent()) {
							throw new CannotPersistException("SAP Result: Cannot update PtrvHead (EmployeeCost Header) data. Please check your data!");
						}
						*/
						/*
						ptrvHeadSoapObject = new PtrvHeadSoapObject(null, returnTripnumber, null);
						List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.ECOSTTRIPHDR_FETCH.service(),
								BAPIFunction.ECOSTTRIPHDR_FETCH_URI.service(), ptrvHeadSoapObject);
						// synchronize PtrvHead fields (in different thread) with the fetched fields from SAP
						if (list.get(0).get("reinr") != null) {
						    ptrvHeadEntity = new PtrvHeadMapper(list.get(0)).getPtrvHead();
						    ptrvHeadEntity.setBukrs(ptrvHead.get().getBukrs());
						    ptrvHeadEntity.setProcess(ptrvHead.get().getProcess());
						    ptrvHeadCommandService.threadUpdate(ptrvHeadEntity);
						}
						*/
					
					/*
						for (PtrvSrec ptrvSrec : listPtrvSrec) {
							Optional<PtrvSrec> ptrvSrecUpdate = ptrvSrecCommandService.updateTripnumber(
									returnTripnumber, ptrvSrec, currentUser.isPresent() ? currentUser.get() : null);
							if (!ptrvSrecUpdate.isPresent()) {
								throw new CannotPersistException("SAP Result: Cannot update PtrvSrec (EmployeeCost Receipts) data. Please check your data!");
							}
						}
						
						Optional<User> submitUser = userQueryService.findByPernr(ptrvHead.get().getId().getPernr());
						if (submitUser.isPresent()) {
							User user = submitUser.get();
							String processed = "CREATED";
					        mailService.sendMail(user.getEmail(), "UPDATE ".
					    		concat(ptrvHeadEntity.get().getEntityName()),
					    		"Dear <b>".concat(user.getEmployee().getSname()).concat("</b><br><br>Your Expense Trip Number <b>".
					    		concat(returnTripnumber.toString()).concat("</b> has been ").
					    		concat(processed).concat(" by your HR Admin or Superior.").concat("<br><br>Thank You.")));
						}
					    
					} else {
						response.setMessage(resultMap.get("errorMessage"));
						throw new CannotPersistException("Cannot post EmployeeCost Transaction to SAP! All data operation will be rollback to initial state.");
					}
					
					*/
					
					/* ************************** */
					
					
				}
				bunchOfEcost.setItems(records.toArray(new EcostTransactionResponseWrapper[records.size()]));
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(PtrvHeadController.class).createBulkWithItems(request, result)).withSelfRel());		
		return response;
	}

	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated PtrvHeadRequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			PtrvHeadResponseWrapper responseWrapper = new PtrvHeadResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<HCMSEntity> ptrvHeadHCMSEntity = hcmsEntityQueryService.findByEntityName(PtrvHead.class.getSimpleName());
					if (!ptrvHeadHCMSEntity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + PtrvHead.class.getSimpleName() + "' !!!");
					}
					
					Optional<PtrvHead> ptrvHead = ptrvHeadQueryService.findOneByCompositeKey(
							request.getHdvrs(),
							request.getPernr(),
							request.getReinr());
					if (!ptrvHead.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
					
					String currentProcess = ptrvHead.get().getProcess();
					String currentReason = ptrvHead.get().getReason();
				
					RequestObjectComparatorContainer<PtrvHead, PtrvHeadRequestWrapper, String> updatedContainer =
							ptrvHeadRequestBuilderUtil.compareAndReturnUpdatedData(request, ptrvHead.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
					
					// if process = 1 (RELEASED by user)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							updatedContainer.getEntity().get().getProcess().equals("1")) {
						updatedContainer.getEntity().get().setCreatedBy(currentUser.get());
						updatedContainer.getEntity().get().setCreatedAt(new Date());
						ptrvHead = ptrvHeadCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						if (!ptrvHead.isPresent()) {
							throw new CannotPersistException("Cannot update PtrvHead (Employee Cost Transaction Header) data. Please check your data!");
						}
					}

					// if process = 2 (RELEASED by Superior -> changed to RELEASED by Admin)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							updatedContainer.getEntity().get().getProcess().equals("2")) {
						
						Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
						if (!currentUser.get().getRoles().contains(adminRole.get())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Please login as 'ADMIN'!");
						}
						
						updatedContainer.getEntity().get().setAdminApprovedBy(currentUser.get());
						updatedContainer.getEntity().get().setAdminApprovedAt(new Date());
						ptrvHead = ptrvHeadCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						if (!ptrvHead.isPresent()) {
							throw new CannotPersistException("Cannot update PtrvHead (Employee Cost Transaction Header) data. Please check your data!");
						}
						
					}
					
					// if process = 0 (REJECTED, REQUEST FOR EDIT)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							updatedContainer.getEntity().get().getProcess().equals("0")) {
						//updatedContainer.getEntity().get().setSeqnr(ptrvHead.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated
						ptrvHead = ptrvHeadCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						if (!ptrvHead.isPresent()) {
							throw new CannotPersistException("Cannot update PtrvHead (Employee Cost Transaction Header) data. Please check your data!");
						}
					}	
					
					StringBuilder key = new StringBuilder("hdvrs=" +  ptrvHead.get().getId().getHdvrs());
					key.append(",pernr=" + ptrvHead.get().getId().getPernr());
					key.append(",reinr=" +  ptrvHead.get().getId().getReinr());
					
					EventLog eventLog = new EventLog(ptrvHeadHCMSEntity.get(), key.toString(), OperationType.UPDATE, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					eventLog.setMailNotification(false);
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					
					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}
				
					responseWrapper = new PtrvHeadResponseWrapper(ptrvHead.get());
					response = new APIResponseWrapper<>(responseWrapper);
					
					String sapTripNumber = "";
					
					// if process = 3 (APPROVED by Admin -> changed to APPROVED by Superior)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							updatedContainer.getEntity().get().getProcess().equals("3")) {

					  PtrvHead ptrvHeadEntity = ptrvHead.get();

					  // sync / post to mulesoft only if last sync / post was 12 minutes past
					  if (ptrvHead.get().getLastSync() == null ||
							(new Date().getTime() - ptrvHead.get().getLastSync().getTime()) / (60 * 1000) >= 12) {
						
						// set current time to lock sync / post to mulesoft
						Date lastSync = ptrvHeadEntity.getLastSync() == null ? new Date(0) : new Date(ptrvHeadEntity.getLastSync().getTime()-600000l);
						ptrvHeadEntity.setLastSync(new Date());
						ptrvHead = ptrvHeadCommandService.updateReinr(ptrvHeadEntity.getId().getReinr(), ptrvHeadEntity,
								currentUser.isPresent() ? currentUser.get() : null);

						Optional<Role> superiorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_SUPERIOR"));
						if (!currentUser.get().getRoles().contains(superiorRole.get())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Please login as 'SUPERIOR'!");
						}
						Optional<User> submitUser = Optional.ofNullable(updatedContainer.getEntity().get().getCreatedBy());
						List<PositionRelation> listPositionRelation = 
								commonServiceFactory.getPositionRelationQueryService().findByFromPositionAndToPosition(
										submitUser.isPresent() ? submitUser.get().getEmployee().getPosition().getId() : StringUtils.EMPTY,
										currentUser.get().getEmployee().getPosition().getId()).
										stream().collect(Collectors.toList());
						if (listPositionRelation.isEmpty()) {
							throw new AuthorizationException(
									"Insufficient Privileges. You can't approve expense of other employee whose not your sub-ordinate!");
						}
						
						/* Post to MuleSoft */
						
						//PtrvHead ptrvHeadEntity = ptrvHead.get();
						ArrayList<PtrvSrec> listPtrvSrec = (ArrayList<PtrvSrec>) ptrvSrecQueryService.findByTripnumber(ptrvHeadEntity.getReinr()).
								stream().collect(Collectors.toList());
						
						PtrvHeadSoapObject ptrvHeadSoapObject = new PtrvHeadSoapObject(ptrvHeadEntity);
						List<PtrvSrecSoapObject> ptrvSrecSoapObjects = new ArrayList<PtrvSrecSoapObject>();
						
						for (int i=0; i<listPtrvSrec.size(); i++) {
							ptrvSrecSoapObjects.add(new PtrvSrecSoapObject(listPtrvSrec.get(i)));
						}
						ptrvHeadSoapObject.setTripReceipts(ptrvSrecSoapObjects);
						
						// begin of summary per expense type
						List<PtrvSrecSoapObject> ptrvSrecSoapObjectsSum = new ArrayList<PtrvSrecSoapObject>();
						for (PtrvSrecSoapObject ptrvSrecSoapObject : ptrvSrecSoapObjects) {
							boolean exist = false;
							for (PtrvSrecSoapObject ptrvSrecSoapObjectSum : ptrvSrecSoapObjectsSum) {
								if (ptrvSrecSoapObject.getExpType().equals(ptrvSrecSoapObjectSum.getExpType())) {
									exist = true;
									Double recAmount = Double.valueOf(ptrvSrecSoapObject.getRecAmount());
									Double sumAmount = Double.valueOf(ptrvSrecSoapObjectSum.getRecAmount());
									ptrvSrecSoapObjectSum.setRecAmount(String.valueOf(sumAmount.intValue() + recAmount.intValue()));
									break;
								}
							}
							if (!exist) {
								ptrvSrecSoapObjectsSum.add(ptrvSrecSoapObject);
							}
						}
						ptrvHeadSoapObject.setTripReceipts(ptrvSrecSoapObjectsSum);
						// end of summary per expense type

						HashMap<String, String> resultMap = (HashMap<String, String>) muleConsumerService.
								postToSAPSync(BAPIFunction.ECOSTTRIPHDR_CREATE.service(), BAPIFunction.ECOSTTRIPHDR_CREATE_URI.service(), ptrvHeadSoapObject);
						
						if (resultMap.get("errorCode") == StringUtils.EMPTY && resultMap.get("tripnumber") != StringUtils.EMPTY &&
								resultMap.get("message").split(" ").length == 1 && Long.parseLong(resultMap.get("message")) != 0 ) {
							
							ptrvHeadEntity.setSuperiorApprovedBy(currentUser.get());
							ptrvHeadEntity.setSuperiorApprovedAt(new Date());
							ptrvHeadEntity.setLastSync(lastSync);
							
							String message = resultMap.get("message");
							Long returnTripnumber = Long.parseLong(message);
							// update the trip number returned from SAP, and save the updated ptrvHead to database
							ptrvHead = ptrvHeadCommandService.updateReinr(returnTripnumber, ptrvHeadEntity,
									currentUser.isPresent() ? currentUser.get() : null);
							if (!ptrvHead.isPresent()) {
								throw new CannotPersistException("SAP Result: Cannot update PtrvHead (EmployeeCost Header) data. Please check your data!");
							}
							System.out.println("ptrvHeadEntity===".concat(((Object)ptrvHeadEntity).toString()).concat(" - reinr=").concat(ptrvHeadEntity.getReinr().toString()));
							System.out.println("ptrvHead===".concat(((Object)ptrvHead.get()).toString()).concat(" - reinr=").concat(ptrvHead.get().getReinr().toString()));
							ptrvHeadCommandService.threadUpdate(ptrvHead.get(), eventLogCommandService, eventLog, currentUser.get());
							sapTripNumber = returnTripnumber.toString();
							
							/*
							ptrvHeadSoapObject = new PtrvHeadSoapObject(null, returnClaimNo, null);
							List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.ECOSTTRIPHDR_FETCH.service(),
									BAPIFunction.ECOSTTRIPHDR_FETCH_URI.service(), ptrvHeadSoapObject);
							// synchronize PtrvHead fields (in different thread) with the fetched fields from SAP
							if (list.get(0).get("reinr") != null) {
							    ptrvHeadEntity = new PtrvHeadMapper(list.get(0)).getPtrvHead();
							    ptrvHeadEntity.setBukrs(ptrvHead.get().getBukrs());
							    ptrvHeadEntity.setProcess(ptrvHead.get().getProcess());
							    ptrvHeadEntity.setPaymentStatus("N");
							    ptrvHeadCommandService.threadUpdate(ptrvHeadEntity);
							}
							*/
							
							for (PtrvSrec ptrvSrec : listPtrvSrec) {
								Optional<PtrvSrec> ptrvSrecUpdate = ptrvSrecCommandService.updateTripnumber(
										returnTripnumber, ptrvSrec, currentUser.isPresent() ? currentUser.get() : null);
								if (!ptrvSrecUpdate.isPresent()) {
									throw new CannotPersistException("SAP Result: Cannot update PtrvSrec (EmployeeCost Receipts) data. Please check your data!");
								}
							}
							
							responseWrapper = new PtrvHeadResponseWrapper(ptrvHeadEntity);
							response = new APIResponseWrapper<>(responseWrapper);
							response.setMessage(message);
						    
						} else {
							ptrvHeadEntity.setProcess(currentProcess);
							ptrvHeadEntity.setReason(currentReason);
							responseWrapper = new PtrvHeadResponseWrapper(ptrvHeadEntity);
							response = new APIResponseWrapper<>(responseWrapper);
							//response.setMessage("Cannot post Medical Transaction to SAP " + resultMap.get("errorMessage") +
									//". Data has been saved for Admin to Update/Re-Post to SAP");
							response.setMessage("Error Message: " + StringUtils.defaultString(resultMap.get("errorMessage"),
									StringUtils.defaultString(resultMap.get("msgMessage"), "None")));
							System.out.println("MULESOFT FAILED on Employee Cost Update - ".concat(response.getMessage()));
							String statusMsg = response.getMessage().split(":")[1];
							int maxLength = statusMsg.length() > 25 ? 25 : statusMsg.length();
							//savedEventLog.get().setStatus((response.getMessage().split(":")[1]).substring(0, 25)); 
							savedEventLog.get().setStatus(statusMsg.substring(0, maxLength));
							//throw new CannotPersistException("Cannot post Medical Transaction to SAP! All data operation will be rollback to initial state.");
						}
						
						/* ************************** */
						
					  } else {
						  ptrvHead.get().setProcess(currentProcess);
						  ptrvHead.get().setReason(currentReason);
						  responseWrapper = new PtrvHeadResponseWrapper(ptrvHead.get());
						  response = new APIResponseWrapper<>(responseWrapper);
						  response.setMessage("Locked: Current transaction still in process to SAP.");
						  savedEventLog.get().setStatus("Locked");
						  System.out.println("MULESOFT FAILED On Employee Cost Update - ".concat(response.getMessage()));
					  }
					
					}
					
					Optional<User> submitUser = userQueryService.findByPernr(ptrvHead.get().getPernr());
					if (submitUser.isPresent()) {
						User user = submitUser.get();/*
						String processed = "RELEASED. Your Admin and Superior will review your submitted data prior to approval.";
						if (ptrvHead.get().getProcess().equals("2")) {
							processed = "APPROVED by your Admin.";
							if (currentProcess.equals("2")) {
								processed = "CANCELED with following message:<br>".concat(response.getMessage()) ;
							}
						} else if (ptrvHead.get().getProcess().equals("3")) {
							processed = "APPROVED by your Superior.";
						} else if (ptrvHead.get().getProcess().equals("4")) {
							processed = "REVIEWED by your ".concat(currentProcess.equals("1") ? "Admin" : "Superior").concat(
									" and he/she request you to change your expense detail with following reason:<br>").concat(
									ptrvHead.get().getReason());
						}
				        mailService.sendMail(user.getEmail(), "UPDATE ".
				    		concat(ptrvHeadHCMSEntity.get().getEntityName()),
				    		"Dear <b>".concat(user.getEmployee().getSname()).concat("</b><br><br>Your Expense Trip Number <b>".
				    		concat(ptrvHead.get().getReinr().toString()).concat("</b> has been ").
				    		concat(processed).concat("<br><br>Thank You.")));*/
						String processed = "<br><br>has been released, waiting for data review and approval from HR Admin or Your Superior.";
						if (ptrvHead.get().getProcess().equals("2")) {
							processed = "<br><br>has been approved by HR Admin, waiting for approval from Your Superior.";
							if (currentProcess.equals("2")) {
								processed = "<br><br>has been approved by Your Superior but it was failed to publish with following message:<br>".
										concat(response.getMessage()) ;
							}
						} else if (ptrvHead.get().getProcess().equals("3")) {
							processed = "<br>SAP Expense Claim Number = ".concat(sapTripNumber)
									.concat("<br><br>has been approved by Your Superior, waiting for payment from Finance.");
						} else if (ptrvHead.get().getProcess().equals("0")) {
							processed = "<br><br>has been rejected by ".concat(currentProcess.equals("1") ? "HR Admin" : "Your Superior").concat(
									" due to following reason :<br>").concat(
									ptrvHead.get().getReason());
						}
						Map<String, String> composeMap = mailService.composeEmail(eventLog, user);
						if (StringUtils.isNotEmpty(composeMap.get("subject"))) {
							String mailBody = composeMap.get("body").replace("<br><br>".concat(mailService.getMailExpenseUpdate()), processed);
							if (ptrvHead.get().getProcess().equals("3")) {
								// replace the old trip number to the generated SAP trip number in the url 
								mailBody = mailBody.replace("reinr/".concat(ptrvHead.get().getId().getReinr().toString()), "reinr/".concat(sapTripNumber));
							}
						    mailService.sendMail(user.getEmail(), composeMap.get("subject"), mailBody);
						    
						    // notification email to all HR Admin
						    if (ptrvHead.get().getProcess().equals("1")) {
							    List<User> adminUsers = new ArrayList<User>();
							    Optional<Collection<User>> users = userQueryService.fetchAdminByCompany(
							    		user.getEmployee().getT500p().getBukrs().getBukrs());
							    if (users.isPresent()) {
							    	adminUsers = users.get().stream().collect(Collectors.toList());
							    	mailBody = mailBody.replace(processed, 
							    			"<br><br>has been released, waiting for your review and approval.").
							    			replace("Dear <b>".concat(user.getEmployee().getSname()), "Dear <b>HR Admin").
							    			replace("Your expense claim below", "User expense claim below");
							    	for (User adminUser : adminUsers) {
							    		mailService.sendMail(adminUser.getEmail(), composeMap.get("subject"), mailBody);
							    	}
							    }
							// notification email to all Superior, only if it was currentProcess=1 (released by user)
						    } else if (ptrvHead.get().getProcess().equals("2")) {
							    List<User> superiorUsers = new ArrayList<User>();
						    	mailBody = mailBody.replace(processed, "1".equals(currentProcess) ?
						    			"<br><br>has been approved by HR Admin, waiting for your approval." :
						    			"2".equals(currentProcess) ?
						    			"<br><br>has been approved but it was failed to publish with following message:<br>".
											concat(response.getMessage()) : processed).
						    			replace("Your expense claim below", "User expense claim below");
						    	
						    	// fetch all user's Superior
						    	List<PositionRelation> employeePositionRelations = commonServiceFactory.getPositionRelationQueryService()
						    			.findByFromPositionid(submitUser.get().getEmployee().getPosition().getId())
						    			.stream().collect(Collectors.toList());									
								if (!employeePositionRelations.isEmpty()) {
									for (PositionRelation positionRelation : employeePositionRelations) {
										List<IT0001> toIT0001List = commonServiceFactory.getIT0001QueryService()
												.findByPositionId(positionRelation.getToPosition().getId())
												.stream().collect(Collectors.toList());
										Optional<User> superior = userQueryService.findByPernr(
												toIT0001List.isEmpty() ? 0 : toIT0001List.get(0).getPernr());
										if (superior.isPresent()) {
											superiorUsers.add(superior.get());
										}
									}
								}
						    	
						    	for (User superiorUser : superiorUsers) {
						    		String body = mailBody.replace("Dear <b>".concat(user.getEmployee().getSname()),
						    				"Dear <b>".concat(superiorUser.getEmployee().getSname()));
						    		mailService.sendMail(superiorUser.getEmail(), composeMap.get("subject"), body);
						    	}
						    }
						}
					}
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(PtrvHeadController.class).edit(request, result)).withSelfRel());
		return response;
	}

	


	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE, value = "/bulk")
	public APIResponseWrapper<?> editBulkWithItems(@RequestBody  EcostTransactionRequestWrapper request[] //@Validated
	, BindingResult result)
	throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {

		ArrayData<EcostTransactionResponseWrapper> bunchOfEcost = new ArrayData<>();
		APIResponseWrapper<ArrayData<EcostTransactionResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfEcost);
		
		if (result.hasErrors()) {
		} else {
						
			Optional<HCMSEntity> ptrvSrecEntity = hcmsEntityQueryService.findByEntityName(PtrvSrec.class.getSimpleName());
			if (!ptrvSrecEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + PtrvSrec.class.getSimpleName() + "' !!!");
			}
			
			Optional<HCMSEntity> ptrvHeadEntity = hcmsEntityQueryService.findByEntityName(PtrvHead.class.getSimpleName());
			if (!ptrvHeadEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + PtrvHead.class.getSimpleName() + "' !!!");
			}
			
			boolean headerDataChanged = false;
			
			edit: {
			try {
				int size = request.length;
				RequestObjectComparatorContainer<PtrvHead, PtrvHeadRequestWrapper, String> newObjectContainerHdr; 				
				RequestObjectComparatorContainer<PtrvSrec, PtrvSrecRequestWrapper, String> newObjectContainerItm;
				RequestObjectComparatorContainer<PtrvSrec, PtrvSrecRequestWrapper, String> updatedContainerItm;
				 
				List<EcostTransactionResponseWrapper> records = new ArrayList<>();
				
				for(int j = 0; j<size; j++){
					
					boolean isDataChanged = false;
					
					Optional<PtrvHead> ptrvHead = ptrvHeadQueryService.findOneByCompositeKey(
							request[j].getTrxHdr().getHdvrs(),
							request[j].getTrxHdr().getPernr(),
							request[j].getTrxHdr().getReinr());
					if (!ptrvHead.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						continue;
					}
					
					String currentProcess = ptrvHead.get().getProcess();
				
					RequestObjectComparatorContainer<PtrvHead, PtrvHeadRequestWrapper, String> updatedContainer =
							ptrvHeadRequestBuilderUtil.compareAndReturnUpdatedData(request[j].getTrxHdr(), ptrvHead.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						headerDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				/*
					PtrvHeadRequestWrapper TrxHdr = request[j].getTrxHdr();
					newObjectContainerHdr= ptrvHeadRequestBuilderUtil.compareAndReturnUpdatedData(TrxHdr, null);						
				*/	
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
					if (!currentUser.isPresent()) {
						throw new AuthorizationException(
								"Insufficient privileges. Please login!");						
					}
/*					
					Optional<PtrvHead> savedEntity = newObjectContainerHdr.getEntity();

					if (newObjectContainerHdr.getEntity().isPresent()) {
						//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
						savedEntity = ptrvHeadCommandService.save(newObjectContainerHdr.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}		

					if (!savedEntity.isPresent()) {
						throw new CannotPersistException("Cannot create PtrvSrec data. Please check your data!");
					}

					StringBuilder key = new StringBuilder("hdvrs=" + savedEntity.get().getId().getHdvrs());
					key.append(",pernr=" + savedEntity.get().getId().getPernr());
					key.append(",reinr=" + savedEntity.get().getId().getReinr());
					
					EventLog eventLog = new EventLog(ptrvHeadEntity.get(), key.toString(), OperationType.CREATE, 
							(newObjectContainerHdr.getEventData().isPresent() ? newObjectContainerHdr.getEventData().get() : ""));
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());

					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}
*/
					
					int itemSize = request[j].getTrxItm().length;	

					ArrayList<PtrvSrec> listPtrvSrec = (ArrayList<PtrvSrec>) ptrvSrecQueryService.findByTripnumber(ptrvHead.get().getReinr()).
							stream().collect(Collectors.toList());
					
					if (listPtrvSrec.size() != itemSize) {
						isDataChanged = true;
					} else {
						for(int i = 0; i < itemSize; i++){
							Optional<PtrvSrec> ptrvSrec = ptrvSrecQueryService.findOneByCompositeKey(
									request[j].getTrxItm()[i].getMandt(),
									request[j].getTrxItm()[i].getPernr(),
									request[j].getTrxItm()[i].getReinr(),
									request[j].getTrxItm()[i].getPerio(),
									request[j].getTrxItm()[i].getReceiptno());
							if (!ptrvSrec.isPresent()) {
								isDataChanged = true;
								break;
							} else {
								updatedContainerItm = ptrvSrecRequestBuilderUtil.compareAndReturnUpdatedData(request[j].getTrxItm()[i], ptrvSrec.get());
								if (updatedContainerItm.getRequestPayload().isPresent()) {
									isDataChanged = updatedContainerItm.getRequestPayload().get().isDataChanged();
									if (isDataChanged)
										break;
								}
							}
						}
					}

					if (isDataChanged || headerDataChanged) {
						if (updatedContainer.getEntity().isPresent()) {
							
							if (isDataChanged && !listPtrvSrec.isEmpty()) {
								ptrvSrecCommandService.deleteList(listPtrvSrec);
							}
							
							if (headerDataChanged) {
								ptrvHead = ptrvHeadCommandService.save(updatedContainer.getEntity().get(), currentUser.get());						
								if (!ptrvHead.isPresent()) {
									throw new CannotPersistException("Cannot update PtrvHead data. Please check your data!");
								}
								StringBuilder key = new StringBuilder("hdvrs=" + ptrvHead.get().getId().getHdvrs());
								key.append(",pernr=" + ptrvHead.get().getId().getPernr());
								key.append(",reinr=" + ptrvHead.get().getId().getReinr());								
								EventLog eventLog = new EventLog(ptrvHeadEntity.get(), key.toString(), OperationType.UPDATE, 
										(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
								Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
								if (!savedEventLog.isPresent()) {
									throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
								}
							}
							
						}
						//anyDataChanged = true;
					} else {
						if (j == (size-1)) {
							if (!headerDataChanged) {
							    response.setMessage("No data changed. No need to perform the update.");
							    break edit;
							}
						} else {
							continue;
						}
					}
					
					int itmno = 1;
					
					PtrvSrec savedEntityItems[] = new PtrvSrec[request[j].getTrxItm().length];
					
					
					for(int i = 0; i < itemSize; i++){
						
						OperationType operationType = OperationType.CREATE;
						
						Optional<PtrvSrec> ptrvSrec = ptrvSrecQueryService.findOneByCompositeKey(
								request[j].getTrxItm()[i].getMandt(),
								request[j].getTrxItm()[i].getPernr(),
								request[j].getTrxItm()[i].getReinr(),
								request[j].getTrxItm()[i].getPerio(),
								request[j].getTrxItm()[i].getReceiptno());
						if (!ptrvSrec.isPresent()) {
							request[j].getTrxItm()[i].setReceiptno(String.valueOf(itmno));								
							newObjectContainerItm = ptrvSrecRequestBuilderUtil.compareAndReturnUpdatedData(request[j].getTrxItm()[i], null);
						} else {
							operationType = OperationType.UPDATE;
							newObjectContainerItm = ptrvSrecRequestBuilderUtil.compareAndReturnUpdatedData(request[j].getTrxItm()[i], ptrvSrec.get());
						}
						ptrvSrec = newObjectContainerItm.getEntity();
						
						//request[j].getTrxItm()[i].setReceiptno(String.valueOf(itmno));								
						//newObjectContainerItm = ptrvSrecRequestBuilderUtil.compareAndReturnUpdatedData(request[j].getTrxItm()[i], null);

						//Optional<PtrvSrec> savedEntityItm = newObjectContainerItm.getEntity();
						Optional<PtrvSrec> savedEntityItm;

						//if (newObjectContainerItm.getEntity().isPresent()) {
							//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
							//savedEntityItm = ptrvSrecCommandService.save(newObjectContainerItm.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
							savedEntityItm = ptrvSrecCommandService.save(ptrvSrec.get(), currentUser.isPresent() ? currentUser.get() : null);
						//}		

						if (!savedEntityItm.isPresent()) {
							throw new CannotPersistException("Cannot create PtrvSrec data. Please check your data!");
						}

						StringBuilder keyItm = new StringBuilder("mandt=" + savedEntityItm.get().getId().getMandt());
						keyItm.append(",prnr=" + savedEntityItm.get().getId().getPernr());
						keyItm.append(",reinr=" + savedEntityItm.get().getId().getReinr());
						keyItm.append(",perio=" + savedEntityItm.get().getId().getPerio());
						keyItm.append(",receiptno=" + savedEntityItm.get().getId().getReceiptno());

						EventLog eventLogItm = new EventLog(ptrvSrecEntity.get(), keyItm.toString(), operationType, 
								(newObjectContainerItm.getEventData().isPresent() ? newObjectContainerItm.getEventData().get() : ""));
						Optional<EventLog> savedEventLogItm = eventLogCommandService.save(eventLogItm, currentUser.get());

						if (!savedEventLogItm.isPresent()) {
							throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
						}
						
						savedEntityItems[i] = savedEntityItm.get();

						itmno++;
						/*
						// if not set, then set form_type as referred from item's sub form type
						if (savedEntity.get().getForm() == 0 && savedEntityItm.get().getSubt706b1() != null) {
							savedEntity.get().setForm(savedEntityItm.get().getSubt706b1().getSubForm());
						}
						*/
					}
					
					records.add(new EcostTransactionResponseWrapper(ptrvHead.get(), savedEntityItems));
					
				}
				bunchOfEcost.setItems(records.toArray(new EcostTransactionResponseWrapper[records.size()]));
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
			}
		}
				
		response.add(linkTo(methodOn(PtrvHeadController.class).editBulkWithItems(request, result)).withSelfRel());		
		return response;
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}
	
}