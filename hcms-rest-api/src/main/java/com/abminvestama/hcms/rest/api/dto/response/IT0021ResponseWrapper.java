package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.IT0021;
import com.abminvestama.hcms.core.model.entity.T005T;
import com.abminvestama.hcms.core.model.entity.T591S;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Wrapper class for <strong>Emergency Info</strong> (i.e. IT0021 in SAP).
 * 
 * @since 1.0.0
 * @version 1.0.8
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.8</td><td>Baihaki</td><td>Add fields: address, phone</td></tr>
 *     <tr><td>1.0.7</td><td>Baihaki</td><td>Add fasexText field</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Fix getUpdatedFields bug: Update the current referred fields i.e. text fields</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add famsaText field</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add Constructor of Read from EventData -> IT0021ResponseWrapper(IT0021, String, CommonServiceFactory)</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add attachmentType, attachmentTypeText, attachmentPath field</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add fgbldText and fanatText field</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add documentStatus field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 * 
 */
@JsonInclude(Include.NON_NULL)
public class IT0021ResponseWrapper extends ResourceSupport {

	private long pernr;
	private String subty;
	private String subtyText;
	private Date endda;
	private Date begda;
	
	private String famsa;
	private String famsaText;
	//private Date fgbdt;
	private String fgbdt;
	private String fgbld;
	private String fgbldText;
	private String fanat;
	private String fanatText;
	private String fasex;
	private String fasexText;
	private String favor;
	private String fanam;
	private String fgbot;
	private String fcnam;
	private int fknzn;
	private String fnmzu;
	private String title;
	private String documentStatus;
	
	private String attachmentType;
	private String attachmentTypeText;
	private String attachmentPath;
	
	private String address;
	private String phone;
	
	private  IT0021ResponseWrapper() {}
	
	public IT0021ResponseWrapper(IT0021 it0021) {
		if (it0021 == null) {
			new IT0021ResponseWrapper();
		} else {
			this
				.setPernr(it0021.getId().getPernr())
				.setSubty(it0021.getSubty() != null ? 
						StringUtils.defaultString(it0021.getSubty().getId() != null ? it0021.getSubty().getId().getSubty() :  StringUtils.EMPTY, StringUtils.EMPTY) 
						: it0021.getId().getSubty())
				.setSubtyText(it0021.getSubty() != null ? StringUtils.defaultString(it0021.getSubty().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setEndda(it0021.getId().getEndda()).setBegda(it0021.getId().getBegda())
				.setFamsa(it0021.getFamsa() != null ? (it0021.getFamsa().getId() != null ? it0021.getFamsa().getId().getSubty() : StringUtils.EMPTY) 
						 : StringUtils.EMPTY)
				.setFamsaText(it0021.getFamsa() != null ? StringUtils.defaultString(it0021.getFamsa().getStext(), 
						it0021.getFamsa().getId() != null ? it0021.getFamsa().getId().getSubty() : StringUtils.EMPTY) : StringUtils.EMPTY)
				.setFgbdt(it0021.getFgbdt() != null ? CommonDateFunction.convertDateToStringYMD(it0021.getFgbdt()) : StringUtils.EMPTY)
				.setFgbld(it0021.getFgbld() != null ? StringUtils.defaultString(it0021.getFgbld().getLand1(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setFgbldText(it0021.getFgbld() != null ? StringUtils.defaultString(it0021.getFgbld().getLandx(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setFanat(it0021.getFanat() != null ? StringUtils.defaultString(it0021.getFanat().getLand1(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setFanatText(it0021.getFanat() != null ? StringUtils.defaultString(it0021.getFanat().getLandx(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setFasex(StringUtils.defaultString(it0021.getFasex(), StringUtils.EMPTY))
				.setFasexText(it0021.getFasex() != null ? (it0021.getFasex().equals("1") ? "Male" :
					(it0021.getFasex().equals("2") ? "Female" : StringUtils.EMPTY)) : StringUtils.EMPTY)
				.setFavor(StringUtils.defaultString(it0021.getFavor(), StringUtils.EMPTY))
				.setFanam(StringUtils.defaultString(it0021.getFanam(), StringUtils.EMPTY))
				.setFgbot(StringUtils.defaultString(it0021.getFgbot(), StringUtils.EMPTY))
				.setFcnam(StringUtils.defaultString(it0021.getFcnam(), StringUtils.EMPTY))
				.setFknzn(it0021.getFknzn() != null ? it0021.getFknzn().intValue() : 0)
				.setFnmzu(it0021.getT535n() != null ? StringUtils.defaultString(it0021.getT535n().getId().getArt(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setTitle(it0021.getT535n() != null ? StringUtils.defaultString(it0021.getT535n().getId().getTitle(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentType(it0021.getAttachment() != null ? StringUtils.defaultString(it0021.getAttachment().getId().getSubty(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentTypeText(it0021.getAttachment() != null ? StringUtils.defaultString(it0021.getAttachment().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentPath(StringUtils.defaultString(it0021.getAttachmentPath(), StringUtils.EMPTY))
				.setAddress(StringUtils.defaultString(it0021.getAddress(), StringUtils.EMPTY))
				.setPhone(StringUtils.defaultString(it0021.getPhone(), StringUtils.EMPTY))
				.setDocumentStatus(it0021.getStatus());
		}
	}

	public IT0021ResponseWrapper(IT0021 currentData, String eventData, CommonServiceFactory serviceFactory) throws JSONException, Exception {
		this(currentData);
		IT0021ResponseWrapper itWrapper = (IT0021ResponseWrapper) new EventDataWrapper(eventData, this).getItWrapper();
		String[] ignoreProperties = EventDataWrapper.getNullPropertyNames(itWrapper);
		BeanUtils.copyProperties(itWrapper, this, ignoreProperties);
		
		// Update the current referred fields i.e. text fields
		T591SKey famsaKey = new T591SKey(SAPInfoType.EMERGENCY_INFO.infoType(), famsa);
		Optional<T591S> famsaObject = serviceFactory.getT591SQueryService().findById(Optional.ofNullable(famsaKey));
		famsaText = famsaObject.isPresent() ? famsaObject.get().getStext() : StringUtils.EMPTY;
		Optional<T005T> fgbldObject = serviceFactory.getT005TQueryService().findById(Optional.ofNullable(fgbld));
		fgbldText = fgbldObject.isPresent() ? fgbldObject.get().getLandx() : StringUtils.EMPTY;
		Optional<T005T> fanatObject = serviceFactory.getT005TQueryService().findById(Optional.ofNullable(fanat));
		fanatText = fanatObject.isPresent() ? fanatObject.get().getLandx() : StringUtils.EMPTY;
		fasexText = fasex != null ? (fasex.equals("1") ? "Male" :
			(fasex.equals("2") ? "Female" : StringUtils.EMPTY)) : StringUtils.EMPTY;
	}
	
	/**
	 * GET Employee SSN.
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	public IT0021ResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	@JsonProperty("subtype")
	public String getSubty() {
		return subty;
	}
	
	public IT0021ResponseWrapper setSubty(String subty) {
		this.subty = subty;
		return this;
	}
	
	/**
	 * GET Subtype Name/Text.
	 * 
	 * @return
	 */
	@JsonProperty("subtype_text")
	public String getSubtyText() {
		return subtyText;
	}
	
	public IT0021ResponseWrapper setSubtyText(String subtyText) {
		this.subtyText = subtyText;
		return this;
	}
	
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	public IT0021ResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}
	
	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	public IT0021ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * GET Family Member.
	 * 
	 * @return
	 */
	@JsonProperty("family_member")
	public String getFamsa() {
		return famsa;
	}
	
	public IT0021ResponseWrapper setFamsa(String famsa) {
		this.famsa = famsa;
		return this;
	}
	
	/**
	 * GET Family Member.
	 * 
	 * @return
	 */
	@JsonProperty("family_member_text")
	public String getFamsaText() {
		return famsaText;
	}
	
	public IT0021ResponseWrapper setFamsaText(String famsaText) {
		this.famsaText = famsaText;
		return this;
	}
	
	/**
	 * GET Date of Birth.
	 * 
	 * @return
	 */
	@JsonProperty("date_of_birth")
	public String getFgbdt() {
		return fgbdt;
	}
	
	public IT0021ResponseWrapper setFgbdt(String fgbdt) {
		this.fgbdt = fgbdt;
		return this;
	}
	
	/**
	 * GET Country of Birth.
	 * 
	 * @return
	 */
	@JsonProperty("country_of_birth")
	public String getFgbld() {
		return fgbld;
	}
	
	public IT0021ResponseWrapper setFgbld(String fgbld) {
		this.fgbld = fgbld;
		return this;
	}
	
	/**
	 * GET Country of Birth.
	 * 
	 * @return
	 */
	@JsonProperty("country_of_birth_text")
	public String getFgbldText() {
		return fgbldText;
	}
	
	public IT0021ResponseWrapper setFgbldText(String fgbldText) {
		this.fgbldText = fgbldText;
		return this;
	}
	
	/**
	 * GET Nationality.
	 * 
	 * @return
	 */
	@JsonProperty("nationality")
	public String getFanat() {
		return fanat;
	}
	
	public IT0021ResponseWrapper setFanat(String fanat) {
		this.fanat = fanat;
		return this;
	}
	
	/**
	 * GET Nationality.
	 * 
	 * @return
	 */
	@JsonProperty("nationality_text")
	public String getFanatText() {
		return fanatText;
	}
	
	public IT0021ResponseWrapper setFanatText(String fanatText) {
		this.fanatText = fanatText;
		return this;
	}
	
	/**
	 * GET Gender.
	 * 
	 * @return
	 */
	@JsonProperty("gender")
	public String getFasex() {
		return fasex;
	}
	
	public IT0021ResponseWrapper setFasex(String fasex) {
		this.fasex = fasex;
		return this;
	}
	
	/**
	 * GET Gender.
	 * 
	 * @return
	 */
	@JsonProperty("gender_text")
	public String getFasexText() {
		return fasexText;
	}
	
	public IT0021ResponseWrapper setFasexText(String fasexText) {
		this.fasexText = fasexText;
		return this;
	}
	
	/**
	 * GET First Name.
	 * 
	 * @return
	 */
	@JsonProperty("first_name")
	public String getFavor() {
		return favor;
	}
	
	public IT0021ResponseWrapper setFavor(String favor) {
		this.favor = favor;
		return this;
	}
	
	/**
	 * GET Last Name.
	 * 
	 * @return
	 */
	@JsonProperty("last_name")
	public String getFanam() {
		return fanam;
	}
	
	public IT0021ResponseWrapper setFanam(String fanam) {
		this.fanam = fanam;
		return this;
	}
	
	/**
	 * GET Birth Place.
	 * 
	 * @return
	 */
	@JsonProperty("birth_place")
	public String getFgbot() {
		return fgbot;
	}
	
	public IT0021ResponseWrapper setFgbot(String fgbot) {
		this.fgbot = fgbot;
		return this;
	}
	
	/**
	 * GET Full Name.
	 * 
	 * @return
	 */
	@JsonProperty("full_name")
	public String getFcnam() {
		return fcnam;
	}
	
	public IT0021ResponseWrapper setFcnam(String fcnam) {
		this.fcnam = fcnam;
		return this;
	}
	
	/**
	 * GET Ind.Name
	 * 
	 * @return
	 */
	@JsonProperty("ind_name")
	public int getFknzn() {
		return fknzn;
	}
	
	public IT0021ResponseWrapper setFknzn(int fknzn) {
		this.fknzn = fknzn;
		return this;
	}
	
	/**
	 * GET Other Title.
	 * 
	 * @return
	 */
	@JsonProperty("other_title")
	public String getFnmzu() {
		return fnmzu;
	}
	
	public IT0021ResponseWrapper setFnmzu(String fnmzu) {
		this.fnmzu = fnmzu;
		return this;
	}
	
	/**
	 * GET Title.
	 * 
	 * @return
	 */
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}
	
	public IT0021ResponseWrapper setTitle(String title) {
		this.title = title;
		return this;
	}
	
	/**
	 * GET Document Status.
	 * 
	 * @return
	 */
	@JsonProperty("document_status")
	public String getDocumentStatus() {
		return documentStatus;
	}
	
	public IT0021ResponseWrapper setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus.name();
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type")
	public String getAttachmentType() {
		return attachmentType;
	}
	
	public IT0021ResponseWrapper setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
		return this;
	}
	
	/**
	 * GET Attachment Type Text.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type_text")
	public String getAttachmentTypeText() {
		return attachmentTypeText;
	}
	
	public IT0021ResponseWrapper setAttachmentTypeText(String attachmentTypeText) {
		this.attachmentTypeText = attachmentTypeText;
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_path")
	public String getAttachmentPath() {
		return attachmentPath;
	}
	
	public IT0021ResponseWrapper setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
		return this;
	}

	/**
	 * GET Emergency Contact Address.
	 * 
	 * @return
	 */
	@JsonProperty("address")
	public String getAddress() {
		return address;
	}
	
	public IT0021ResponseWrapper setAddress(String address) {
		this.address = address;
		return this;
	}

	/**
	 * GET Emergency Contact Phone Number.
	 * 
	 * @return
	 */
	@JsonProperty("phone")
	public String getPhone() {
		return phone;
	}
	
	public IT0021ResponseWrapper setPhone(String phone) {
		this.phone = phone;
		return this;
	}
}