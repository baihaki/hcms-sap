package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.Attachment;
import com.abminvestama.hcms.core.model.entity.IT0185;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.T005T;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.model.entity.T5R06;
import com.abminvestama.hcms.core.service.api.business.query.AttachmentQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T5R06QueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0185QueryService;
import com.abminvestama.hcms.core.service.api.business.query.T005TQueryService;
import com.abminvestama.hcms.core.service.util.FileAttachmentService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0185RequestWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.4
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Adjust attachment fields on EventData Json</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData: Add attachment fields</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Validation: ictype must be equals to subty (Create and Edit)</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData to handle Create</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("it0185RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT0185RequestBuilderUtil {
	
	private T5R06QueryService t5r06QueryService;
	private T005TQueryService t005tQueryService;
	private IT0185QueryService it0185QueryService;
	private AttachmentQueryService attachmentQueryService;
	private FileAttachmentService fileAttachmentService;
	
	@Autowired
	void setT5R06QueryService(T5R06QueryService t5r06QueryService) {
		this.t5r06QueryService = t5r06QueryService;
	}
	
	@Autowired
	void setT005TQueryService(T005TQueryService t005tQueryService) {
		this.t005tQueryService = t005tQueryService;
	}
	
	@Autowired
	void setIT0185QueryService(IT0185QueryService it0185QueryService) {
		this.it0185QueryService = it0185QueryService;
	}
	
	@Autowired
	void setAttachmentQueryService(AttachmentQueryService attachmentQueryService) {
		this.attachmentQueryService = attachmentQueryService;
	}
	
	@Autowired
	void setFileAttachmentService(FileAttachmentService fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}

	/**
	 * Compare IT0185 request payload with existing IT0185 in the database.
	 * Update existing IT0185 data with the latest data comes from the request payload. 
	 * 
	 * @param requestPayload request data 
	 * @param it0185DB current existing IT0185 in the database.
	 * @return updated IT0185 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0185, IT0185RequestWrapper, String> compareAndReturnUpdatedData(IT0185RequestWrapper requestPayload, IT0185 it0185DB) {
		
		RequestObjectComparatorContainer<IT0185, IT0185RequestWrapper, String> objectContainer = null;
		
		if (it0185DB == null) {
			it0185DB = new IT0185();
			try {
				Optional<IT0185> existingIT0185 = it0185QueryService.findOneByCompositeKey(
						requestPayload.getPernr(),  requestPayload.getSubty(), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda()));
				
				if (existingIT0185.isPresent()) {
					throw new DataViolationException("Duplicate IT0185 Data for [ssn=" + requestPayload.getPernr() + ",end_date=" +
							requestPayload.getEndda() + ",begin_date=" + requestPayload.getBegda());
				}
				
				it0185DB.setId(
						new ITCompositeKeys(requestPayload.getPernr(), SAPInfoType.PERSONAL_ID.infoType(), requestPayload.getSubty(), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda())));
				it0185DB.setAuth1(requestPayload.getAuth1());
				//Optional<T5R06> newT5R06 = t5r06QueryService.findById(Optional.ofNullable(requestPayload.getIctyp()));
				// ictype must be equals to subty, ignore any ictype value from requestPayload
				Optional<T5R06> newT5R06 = t5r06QueryService.findById(Optional.ofNullable(requestPayload.getSubty()));
				it0185DB.setIctyp(newT5R06.isPresent() ? newT5R06.get() : null);
				it0185DB.setIcnum(requestPayload.getIcnum());
				it0185DB.setFpdat(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getFpdat()));
				it0185DB.setExpid(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getExpid()));
				it0185DB.setIsspl(requestPayload.getIsspl());
				Optional<T005T> iscot = t005tQueryService.findById(Optional.ofNullable(requestPayload.getIscot()));
				it0185DB.setIscot(iscot.isPresent() ? iscot.get() : null);
				it0185DB.setSeqnr(1L);
				it0185DB.setStatus(DocumentStatus.INITIAL_DRAFT);
				
				it0185DB = saveAttachment(it0185DB, requestPayload);
				
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			
			objectContainer = new RequestObjectComparatorContainer<>(it0185DB, requestPayload);
			
		} else {
			if (requestPayload.getIctyp() != null) {
				//if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getIctyp(), it0185DB.getIctyp() != null ? it0185DB.getIctyp().getIctyp() : "")) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getSubty(), it0185DB.getIctyp() != null ? it0185DB.getIctyp().getIctyp() : "")) {
				// ictype must be equals to subty, ignore any ictype value from requestPayload
					try {
						//Optional<T5R06> newT5R06 = t5r06QueryService.findById(Optional.ofNullable(requestPayload.getIctyp()));
						Optional<T5R06> newT5R06 = t5r06QueryService.findById(Optional.ofNullable(requestPayload.getSubty()));
						if (newT5R06.isPresent()) {
							it0185DB.setIctyp(newT5R06.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						// T5R06 not found...cancel the update process
					}
				}
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getIcnum(), it0185DB.getIcnum())) {
				it0185DB.setIcnum(StringUtils.defaultString(requestPayload.getIcnum(), ""));
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAuth1(), it0185DB.getAuth1())) {
				it0185DB.setAuth1(StringUtils.defaultString(requestPayload.getAuth1(), ""));
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getFpdat()), it0185DB.getFpdat())) {
				it0185DB.setFpdat(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getFpdat()));
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getExpid()), it0185DB.getExpid())) {
				it0185DB.setExpid(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getExpid()));
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getIsspl(), it0185DB.getIsspl())) {
				it0185DB.setIsspl(requestPayload.getIsspl());
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getIscot(), it0185DB.getIscot() != null ? it0185DB.getIscot().getLand1() : "")) {
				try {
					Optional<T005T> newT005T = t005tQueryService.findById(Optional.ofNullable(requestPayload.getIscot()));
					if (newT005T.isPresent()) {
						it0185DB.setIscot(newT005T.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					// T005T not found... cancel the update process
				}
			}
			
			it0185DB.setSeqnr(it0185DB.getSeqnr().longValue() + 1);
			try {
				it0185DB = saveAttachment(it0185DB, requestPayload);
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			it0185DB.setSeqnr(it0185DB.getSeqnr().longValue() - 1);
			
			objectContainer = new RequestObjectComparatorContainer<>(it0185DB, requestPayload);
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = "";
		try {
			jsonString = objectMapper.writeValueAsString(requestPayload);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}		
		
		//JSONObject json = new JSONObject(requestPayload);
		JSONObject json = new JSONObject(jsonString);
		// Adjust attachment fields on EventData Json
		json.remove("attachment1_type");
		json.remove("attachment1_base64");
		json.remove("attachment1_filename");
		if (it0185DB.getAttachment() != null)
			json.put("attachment1_type", it0185DB.getAttachment().getId().getSubty());
		if (it0185DB.getAttachmentPath() != null)
		    json.put("attachment1_path", it0185DB.getAttachmentPath());
		// ******************************************
		
		//if (it0185DB.getStatus() != null)
			//json.put("prev_document_status", it0185DB.getStatus().name());
		
		objectContainer.setEventData(json.toString());
		
		return objectContainer;
	}
	
	private IT0185 saveAttachment(IT0185 it0185DB, IT0185RequestWrapper requestPayload) throws Exception {
		
		T591SKey attachmentKey = new T591SKey(SAPInfoType.PERSONAL_ID.infoType(), requestPayload.getAttach1Type());
		Optional<Attachment> attachment = attachmentQueryService.findById(Optional.ofNullable(attachmentKey));
		it0185DB.setAttachment(attachment.isPresent() ? attachment.get() : null);
		String base64 = requestPayload.getAttach1Content();
		String filename = requestPayload.getAttach1Filename();
		if (StringUtils.isNotEmpty(base64) && StringUtils.isNotEmpty(filename)) {
			String ext = filename.split("\\.")[(filename.split("\\.")).length-1];
			String formatFilename = it0185DB.getId().toStringId().concat("_").concat(it0185DB.getSeqnr().toString()).concat(".").concat(ext);
		    String path = fileAttachmentService.save(formatFilename, base64);
		    it0185DB.setAttachmentPath(path);
		    requestPayload.setDataChanged(true);
		}
		
		return it0185DB;
		
	}

}