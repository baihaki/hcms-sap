package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ZmedHeaderPostRequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = -2317050437190237970L;
	
	private String objType;
	private String objKey;
	private String objSys;
	private String busAct;
	private String username;
	private String headerTxt;
	private String compCode;
	private String docDate;
	private String pstngDate;
	private String transDate;
	private String fiscYear;
	private String fisPeriod;
	private String docType;
	private String refDocNo;
	private String accDocNo;
	private String objKeyR;
	private String reasonRev;
	private String compoAcc;
	private String refDocNoLong;
	private String accPrinciple;
	private String negPosting;
	private String objKeyInv;
	private String billCategory;
	private String vatdate;
	private String logsys;
	
	@JsonProperty("obj_type")
	public String getObjType() {
		return objType;
	}
	
	@JsonProperty("obj_key")
	public String getObjKey() {
		return objKey;
	}
	
	@JsonProperty("obj_sys")
	public String getObjSys() {
		return objSys;
	}
	
	@JsonProperty("bus_act")
	public String getBusAct() {
		return busAct;
	}
	
	@JsonProperty("username")
	public String getUsername() {
		return username;
	}
	
	@JsonProperty("header_text")
	public String getHeaderTxt() {
		return headerTxt;
	}
	
	@JsonProperty("comp_code")
	public String getCompCode() {
		return compCode;
	}
	
	@JsonProperty("doc_date")
	public String getDocDate() {
		return docDate;
	}
	
	@JsonProperty("pstng_date")
	public String getPstngDate() {
		return pstngDate;
	}
	
	@JsonProperty("trans_date")
	public String getTransDate() {
		return transDate;
	}
	
	@JsonProperty("fisc_year")
	public String getFiscYear() {
		return fiscYear;
	}
	
	@JsonProperty("fis_period")
	public String getFisPeriod() {
		return fisPeriod;
	}
	
	@JsonProperty("doc_type")
	public String getDocType() {
		return docType;
	}
	
	@JsonProperty("ref_doc_no")
	public String getRefDocNo() {
		return refDocNo;
	}
	
	@JsonProperty("acc_doc_no")
	public String getAccDocNo() {
		return accDocNo;
	}
	
	@JsonProperty("obj_key_r")
	public String getObjKeyR() {
		return objKeyR;
	}
	
	@JsonProperty("reason_rev")
	public String getReasonRev() {
		return reasonRev;
	}
	
	@JsonProperty("compo_acc")
	public String getCompoAcc() {
		return compoAcc;
	}
	
	@JsonProperty("ref_doc_no_long")
	public String getRefDocNoLong() {
		return refDocNoLong;
	}
	
	@JsonProperty("acc_principle")
	public String getAccPrinciple() {
		return accPrinciple;
	}
	
	@JsonProperty("neg_posting")
	public String getNegPosting() {
		return negPosting;
	}
	
	@JsonProperty("obj_key_inv")
	public String getObjKeyInv() {
		return objKeyInv;
	}
	
	@JsonProperty("bill_category")
	public String getBillCategory() {
		return billCategory;
	}
	
	@JsonProperty("vatdate")
	public String getVatdate() {
		return vatdate;
	}
	
	@JsonProperty("logsys")
	public String getLogsys() {
		return logsys;
	}
	
	
}
