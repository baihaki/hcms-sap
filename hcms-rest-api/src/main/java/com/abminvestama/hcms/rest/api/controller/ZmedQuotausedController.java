package com.abminvestama.hcms.rest.api.controller;



import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;
import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedQuotausedCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedQuotausedQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedQuotausedRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.ZmedQuotausedRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.ZmedQuotausedResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ZmedQuotausedResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.ZmedQuotausedRequestBuilderUtil;




@RestController
@RequestMapping("/api/v1/medical_quota_used")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ZmedQuotausedController extends AbstractResource {
	
	private ZmedQuotausedCommandService zmedQuotausedCommandService;
	private ZmedQuotausedQueryService zmedQuotausedQueryService;
	private ZmedQuotausedRequestBuilderUtil zmedQuotausedRequestBuilderUtil;
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	@Autowired
	void setZmedQuotausedCommandService(ZmedQuotausedCommandService zmedQuotausedCommandService) {
		this.zmedQuotausedCommandService = zmedQuotausedCommandService;
	}
	
	@Autowired
	void setZmedQuotausedQueryService(ZmedQuotausedQueryService zmedQuotausedQueryService) {
		this.zmedQuotausedQueryService = zmedQuotausedQueryService;
	}
	
	@Autowired
	void setZmedQuotausedRequestBuilderUtil(ZmedQuotausedRequestBuilderUtil zmedQuotausedRequestBuilderUtil) {
		this.zmedQuotausedRequestBuilderUtil = zmedQuotausedRequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	
	
	
	
	

	
	
	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated ZmedQuotausedRequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> zmedQuotausedEntity = hcmsEntityQueryService.findByEntityName(ZmedQuotaused.class.getSimpleName());
			if (!zmedQuotausedEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + ZmedQuotaused.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<ZmedQuotaused, ZmedQuotausedRequestWrapper, String> newObjectContainer 
					= zmedQuotausedRequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				
				Optional<ZmedQuotaused> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
				//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = zmedQuotausedCommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create ZmedQuotaused data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("gjahr=" + savedEntity.get().getId().getGjahr());
				key.append(",bukrs=" + savedEntity.get().getId().getBukrs());
				key.append(",pernr=" + savedEntity.get().getId().getPernr());
				key.append(",kodequo=" + savedEntity.get().getId().getKodequo());
				
				EventLog eventLog = new EventLog(zmedQuotausedEntity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				
				ZmedQuotausedResponseWrapper responseWrapper = new ZmedQuotausedResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(ZmedQuotausedController.class).create(request, result)).withSelfRel());		
		return response;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated ZmedQuotausedRequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			ZmedQuotausedResponseWrapper responseWrapper = new ZmedQuotausedResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<ZmedQuotaused> zmedQuotaused = zmedQuotausedQueryService.findOneByCompositeKey(
							request.getGjahr(), request.getBukrs(),
							request.getPernr(), request.getKodequo());
					if (!zmedQuotaused.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					RequestObjectComparatorContainer<ZmedQuotaused, ZmedQuotausedRequestWrapper, String> updatedContainer = zmedQuotausedRequestBuilderUtil.compareAndReturnUpdatedData(request, zmedQuotaused.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
				//	Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				//
				//	if (updatedContainer.getEntity().isPresent()) {
				//		updatedContainer.getEntity().get().setSeqnr(zmedQuotaused.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
				//		zmedQuotaused = zmedQuotausedCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				//	}
				
				//	if (!zmedQuotaused.isPresent()) {
				//		throw new CannotPersistException("Cannot update ZmedQuotaused (Personal Information) data. Please check your data!");
				//	}
				
					responseWrapper = new ZmedQuotausedResponseWrapper(zmedQuotaused.get());
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(ZmedQuotausedController.class).edit(request, result)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}
	
	
}
