package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.IT0002;
import com.abminvestama.hcms.core.model.entity.IT0021;
import com.abminvestama.hcms.core.model.entity.IT0184;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0002QueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0021QueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0184QueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.MedPatientResponseWrapper;

@RestController
@RequestMapping("/api/v1/medical_Patient")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class MedPatientController extends AbstractResource {

	private IT0001QueryService it0001QueryService;
	private IT0021QueryService it0021QueryService;
	private IT0002QueryService it0002QueryService;
	private IT0184QueryService it0184QueryService;
	
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	
	
	@Autowired
	void setIT0001QueryService(IT0001QueryService it0001QueryService) {
		this.it0001QueryService = it0001QueryService;
	}
	
	@Autowired
	void setIT0002QueryService(IT0002QueryService it0002QueryService) {
		this.it0002QueryService = it0002QueryService;
	}
	
	@Autowired
	void setIT0021QueryService(IT0021QueryService it0021QueryService) {
		this.it0021QueryService = it0021QueryService;
	}
	
	@Autowired
	void setIT0184QueryService(IT0184QueryService it0184QueryService) {
		this.it0184QueryService = it0184QueryService;
	}
	
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	
	
	
	
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<MedPatientResponseWrapper>>
	/*APIResponseWrapper<?>*/ findByPernr(@PathVariable String pernr) throws Exception {
		
		

		ArrayData<MedPatientResponseWrapper> bunchOfZmed = new ArrayData<>();
		APIResponseWrapper<ArrayData<MedPatientResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfZmed);
		
		/*
		 * Getting data from it0001 by matching pernr
		 */				
		String gender = "";
		String mstatus = "";

		try {
			long numOfPernr = Long.parseLong(pernr);//

			List<IT0001> listOfIT0001 = it0001QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());

			if (listOfIT0001.isEmpty()) {
				response.setMessage("No Data Found in it0001 (Master Employee Data)");
			} else {
				int length = listOfIT0001.size();
				IT0001 it0001 = null;
				for (int i = 0; i < length; i++) {
					it0001 = listOfIT0001.get(i);
					if(!CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31"), it0001.getId().getEndda())) {
						//got correct record
						break;
					}
				}
				
				/*
				 * Getting data from it0002
				 */
				try {
					List<IT0002> listOfIT0002 = it0002QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());

					if (listOfIT0002.isEmpty()) {
						response.setMessage("No Data Found in it0002 (Personal Data)");
					} else {
						int length2 = listOfIT0002.size();
						IT0002 it0002;
						for (int i = 0; i < length2; i++) {
							it0002 = listOfIT0002.get(i);
							if(!CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31"), it0002.getId().getEndda())) {
								//got correct record
								gender = it0002.getGesch()!= null ? it0002.getGesch() : "";
								mstatus = it0002.getFamst().getFamst()!= null ? it0002.getFamst().getFamst() : "";
								if(!(gender.equals(null)||mstatus.equals(null)))
									break;
							}
						}
						
						
						
						if(mstatus.equals("0")){ //unmarried
							List<MedPatientResponseWrapper> records = new ArrayList<>();
							records.add(new MedPatientResponseWrapper(it0001));
							bunchOfZmed.setItems(records.toArray(new MedPatientResponseWrapper[records.size()]));
						}else{
							if(gender.equals("1")){//male
								
								
								try {
									List<IT0021> listOfIT0021 = it0021QueryService.findMedPatientByPernr(numOfPernr).stream().collect(Collectors.toList());

									List<MedPatientResponseWrapper> records = new ArrayList<>();
									records.add(new MedPatientResponseWrapper(it0001));

									if (listOfIT0021.isEmpty()) {
										//response.setMessage("No Data Found in it0021");
									} else {
										//List<MedPatientResponseWrapper> records = new ArrayList<>();
										//records.add(new MedPatientResponseWrapper(it0001));
										for (IT0021 zmed : listOfIT0021) {
											//if(Zmed.getFamsa().getId().getSubty().equals("2") ||
													//Zmed.getFamsa().getId().getSubty().equals("1"))
												records.add(new MedPatientResponseWrapper(zmed));
										}
										//bunchOfZmed.setItems(records.toArray(new MedPatientResponseWrapper[records.size()]));							
									}
									bunchOfZmed.setItems(records.toArray(new MedPatientResponseWrapper[records.size()]));
								} catch (NumberFormatException e) { 
									throw e;
								} catch (UsernameNotFoundException e) {
									throw e;
								} catch (Exception e) {
									throw e;
								}							
								
								
							}else{//female

								List<MedPatientResponseWrapper> records = new ArrayList<>();
								records.add(new MedPatientResponseWrapper(it0001));
								
								try {
									List<IT0184> listOfIT0184 = it0184QueryService.findByPernrAndSubty(numOfPernr, "03").stream().collect(Collectors.toList());

									if (listOfIT0184.isEmpty()) {
										//response.setMessage("No Data Found in it0184");
									} else {
										String subty = "";
										
										int length184 = listOfIT0184.size();
										IT0184 it0184;
										for (int i = 0; i < length184; i++) {
											it0184 = listOfIT0184.get(i);
											if(!CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31"), it0184.getId().getEndda())) {
												//got correct record
												subty = it0184.getSubty().getId().getSubty();
												break;
											}
										}
										
										if(subty.equals("03")){ //breadwinner
											
											try {
												List<IT0021> listOfIT0021 = it0021QueryService.findMedPatientByPernr(numOfPernr).stream().collect(Collectors.toList());

												if (listOfIT0021.isEmpty()) {
													//response.setMessage("No Data Found in it0021");
												} else {
													//List<MedPatientResponseWrapper> records = new ArrayList<>();
													//records.add(new MedPatientResponseWrapper(it0001));
													for (IT0021 Zmed : listOfIT0021) {
														if(Zmed.getFamsa().getId().getSubty().equals("2"))
															records.add(new MedPatientResponseWrapper(Zmed));
													}
													//bunchOfZmed.setItems(records.toArray(new MedPatientResponseWrapper[records.size()]));							
												}
											} catch (NumberFormatException e) { 
												throw e;
											} catch (UsernameNotFoundException e) {
												throw e;
											} catch (Exception e) {
												throw e;
											}											
											
										}else{
											//List<MedPatientResponseWrapper> records = new ArrayList<>();
											//records.add(new MedPatientResponseWrapper(it0001));
											
										}
									}
								} catch (NumberFormatException e) { 
									throw e;
								} catch (UsernameNotFoundException e) {
									throw e;
								} catch (Exception e) {
									throw e;
								}

								bunchOfZmed.setItems(records.toArray(new MedPatientResponseWrapper[records.size()]));
								
							}
							
							
						}
						
					}
				} catch (NumberFormatException e) { 
					throw e;
				} catch (UsernameNotFoundException e) {
					throw e;
				} catch (Exception e) {
					throw e;
				}
				
				
			}
		} catch (NumberFormatException e) { 
			throw e;
		} catch (UsernameNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(MedPatientController.class).findByPernr(pernr)).withSelfRel());
		return response;
	}
	
	
		
		

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}

}
