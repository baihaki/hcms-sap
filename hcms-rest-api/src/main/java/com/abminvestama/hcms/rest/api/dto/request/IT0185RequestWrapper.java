package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add attach1Type, attach1Content, attach1Filename field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public class IT0185RequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = -5234365762723526620L;
	
	private String ictyp;
	private String icnum;
	private String auth1;
	private String fpdat;
	private String expid;
	private String isspl;
	private String iscot;

	private String attach1Type;
	private String attach1Content;
	private String attach1Filename;
	
	@JsonProperty("ic_type")
	public String getIctyp() {
		return ictyp;
	}
	
	@JsonProperty("ic_number")
	public String getIcnum() {
		return icnum;
	}
	
	@JsonProperty("issuing_authority")
	public String getAuth1() {
		return auth1;
	}
	
	@JsonProperty("issuing_date")
	public String getFpdat() {
		return fpdat;
	}
	
	@JsonProperty("expiry_date")
	public String getExpid() {
		return expid;
	}
	
	@JsonProperty("place_of_issue")
	public String getIsspl() {
		return isspl;
	}
	
	@JsonProperty("country_of_issue")
	public String getIscot() {
		return iscot;
	}
	
	@JsonProperty("attachment1_type")
	public String getAttach1Type() {
		return attach1Type;
	}
	
	@JsonProperty("attachment1_base64")
	public String getAttach1Content() {
		return attach1Content;
	}
	
	@JsonProperty("attachment1_filename")
	public String getAttach1Filename() {
		return attach1Filename;
	}
}