package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0006;
import com.abminvestama.hcms.core.model.entity.T005T;
import com.abminvestama.hcms.core.model.entity.T005U;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * Wrapper class for <strong>Address</strong> (i.e. IT0006 in SAP)
 * 
 * @since 1.0.0
 * @version 1.0.5
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Fix getUpdatedFields bug: Update the current referred fields i.e. text fields</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add fields: land1, land1Text, state, stateText</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add Constructor of Read from EventData -> IT0006ResponseWrapper(IT0006, String, CommonServiceFactory)</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add attachmentType, attachmentTypeText, attachmentPath field</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add documentStatus field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class IT0006ResponseWrapper extends ResourceSupport {

	private long pernr;
	
	private String subty;
	
	private String subtyText;
	
	private Date endda;
	
	private Date begda;
	
	private Date aedtm;
	
	private String uname;
	
	private String anssa;
	
	private String name2;
	
	private String stras;
	
	private String ort01;
	
	private String ort02;
	
	private String pstlz;
	
	private String telnr;
	
	private double entkm;
	
	private String locat;
	
	private double entk2;
	
	private String com01;
	
	private String num01;
	
	private String land1;
	private String land1Text;
	private String state;
	private String stateText;
	
	private String documentStatus;
	
	private String attachmentType;
	private String attachmentTypeText;
	private String attachmentPath;
	
	private  IT0006ResponseWrapper() {}
	
	public IT0006ResponseWrapper(IT0006 it0006) {
		if (it0006 == null) {
			new IT0006ResponseWrapper();
		} else {
			this
				.setPernr(it0006.getId().getPernr())
				.setSubty(it0006.getSubty() != null ? (it0006.getSubty().getId() != null ? it0006.getSubty().getId().getSubty() : StringUtils.EMPTY) : it0006.getId().getSubty())
				.setSubtyText(it0006.getSubty() != null ? StringUtils.defaultString(it0006.getSubty().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setEndda(it0006.getId().getEndda()).setBegda(it0006.getId().getBegda())
				.setAedtm(it0006.getAedtm())
				.setUname(it0006.getUname())
				.setAnssa(it0006.getAnssa())
				.setName2(it0006.getName2())
				.setStras(it0006.getStras())
				.setOrt01(it0006.getOrt01())
				.setOrt02(it0006.getOrt02())
				.setPstlz(it0006.getPstlz())
				.setTelnr(it0006.getTelnr())
				.setEntkm(it0006.getEntkm() != null ? it0006.getEntkm() : 0.0)
				.setLocat(it0006.getLocat())
				.setEntk2(it0006.getEntk2() != null ? it0006.getEntk2() : 0.0)
				.setCom01(it0006.getCom01())
				.setNum01(it0006.getNum01())
				.setLand1(it0006.getT005t() != null ? it0006.getT005t().getLand1() : StringUtils.EMPTY)
				.setLand1Text(it0006.getT005t() != null ? it0006.getT005t().getLandx() : StringUtils.EMPTY)
				.setState(it0006.getT005u() != null ? it0006.getT005u().getBland() : StringUtils.EMPTY)
				.setStateText(it0006.getT005u() != null ? it0006.getT005u().getBezei() : StringUtils.EMPTY)
				.setAttachmentType(it0006.getAttachment() != null ? StringUtils.defaultString(it0006.getAttachment().getId().getSubty(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentTypeText(it0006.getAttachment() != null ? StringUtils.defaultString(it0006.getAttachment().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentPath(StringUtils.defaultString(it0006.getAttachmentPath(), StringUtils.EMPTY))
				.setDocumentStatus(it0006.getStatus());
			
		}
	}

	public IT0006ResponseWrapper(IT0006 currentData, String eventData, CommonServiceFactory serviceFactory) throws JSONException, Exception {
		this(currentData);
		IT0006ResponseWrapper itWrapper = (IT0006ResponseWrapper) new EventDataWrapper(eventData, this).getItWrapper();
		String[] ignoreProperties = EventDataWrapper.getNullPropertyNames(itWrapper);
		BeanUtils.copyProperties(itWrapper, this, ignoreProperties);
		
		// Update the current referred fields i.e. text fields
		Optional<T005T> land1Object = serviceFactory.getT005TQueryService().findById(Optional.ofNullable(land1));
		land1Text = land1Object.isPresent() ? land1Object.get().getLandx() : StringUtils.EMPTY;
		Optional<T005U> stateObject = serviceFactory.getT005UQueryService().findById(Optional.ofNullable(state));
		stateText = stateObject.isPresent() ? stateObject.get().getBezei() : StringUtils.EMPTY;
	}
	
	/**
	 * Employee SSN (PERNR)
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	public IT0006ResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	/**
	 * Address Subtype.
	 * 
	 * @return
	 */
	@JsonProperty("subtype")
	public String getSubty() {
		return subty;
	}
	
	public IT0006ResponseWrapper setSubty(String subty) {
		this.subty = subty;
		return this;
	}
	
	/**
	 * Address Subtype Text.
	 * 
	 * @return
	 */
	@JsonProperty("subtype_text")
	public String getSubtyText() {
		return subtyText;
	}
	
	public IT0006ResponseWrapper setSubtyText(String subtyText) {
		this.subtyText = subtyText;
		return this;
	}
	
	/**
	 * End Date.
	 * 
	 * @return
	 */
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	public IT0006ResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}
	
	/**
	 * Begin Date.
	 * 
	 * @return
	 */
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	public IT0006ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * Date of Record Changed.
	 * 
	 * @return
	 */
	@JsonProperty("changed_on")
	public Date getAedtm() {
		return aedtm;
	}
	
	public IT0006ResponseWrapper setAedtm(Date aedtm) {
		this.aedtm = aedtm;
		return this;
	}
	
	/**
	 * Last User that changed the Record.
	 * 
	 * @return
	 */
	@JsonProperty("changed_by")
	public String getUname() {
		return uname;
	}
	
	public IT0006ResponseWrapper setUname(String uname) {
		this.uname = uname;
		return this;
	}
	
	/**
	 * Address Type.
	 * 
	 * @return
	 */
	@JsonProperty("address_type")
	public String getAnssa() {
		return anssa;
	}
	
	public IT0006ResponseWrapper setAnssa(String anssa) {
		this.anssa = anssa;
		return this;
	}
	
	@JsonProperty("care_of")
	public String getName2() {
		return name2;
	}
	
	public IT0006ResponseWrapper setName2(String name2) {
		this.name2 = name2;
		return this;
	}
	
	/**
	 * Street and House No.
	 * 
	 * @return
	 */
	@JsonProperty("street_house_no")
	public String getStras() {
		return stras;
	}
	
	public IT0006ResponseWrapper setStras(String stras) {
		this.stras = stras;
		return this;
	}
	
	/**
	 * City.
	 * 
	 * @return
	 */
	@JsonProperty("city")
	public String getOrt01() {
		return ort01;
	}
	
	public IT0006ResponseWrapper setOrt01(String ort01) {
		this.ort01 = ort01;
		return this;
	}

	/**
	 * District.
	 * 
	 * @return
	 */
	@JsonProperty("district")
	public String getOrt02() {
		return ort02;
	}
	
	public IT0006ResponseWrapper setOrt02(String ort02) {
		this.ort02 = ort02;
		return this;
	}
	
	/**
	 * Postal Code.
	 * 
	 * @return
	 */
	@JsonProperty("postal_code")
	public String getPstlz() {
		return pstlz;
	}
	
	public IT0006ResponseWrapper setPstlz(String pstlz) {
		this.pstlz = pstlz;
		return this;
	}
	
	/**
	 * Phone No.
	 * 
	 * @return
	 */
	@JsonProperty("phone_no")
	public String getTelnr() {
		return telnr;
	}
	
	public IT0006ResponseWrapper setTelnr(String telnr) {
		this.telnr = telnr;
		return this;
	}
	
	/**
	 * Distance in KM.
	 * 
	 * @return
	 */
	@JsonProperty("distance_km")
	public double getEntkm() {
		return entkm;
	}
	
	public IT0006ResponseWrapper setEntkm(double entkm) {
		this.entkm = entkm;
		return this;
	}
	
	/**
	 * Second Address.
	 * 
	 * @return
	 */
	@JsonProperty("second_address")
	public String getLocat() {
		return locat;
	}
	
	public IT0006ResponseWrapper setLocat(String locat) {
		this.locat = locat;
		return this;
	}
	
	/**
	 * Distaince in KM2.
	 * 
	 * @return
	 */
	@JsonProperty("distance_km2")
	public double getEntk2() {
		return entk2;
	}
	
	public IT0006ResponseWrapper setEntk2(double entk2) {
		this.entk2 = entk2;
		return this;
	}
	
	/**
	 * Communication Type.
	 * 
	 * @return
	 */
	@JsonProperty("communication_type")
	public String getCom01() {
		return com01;
	}
	
	public IT0006ResponseWrapper setCom01(String com01) {
		this.com01 = com01;
		return this;
	}
	
	/**
	 * Communication Type Number/Text.
	 * 
	 * @return
	 */
	@JsonProperty("number")
	public String getNum01() {
		return num01;
	}
	
	public IT0006ResponseWrapper setNum01(String num01) {
		this.num01 = num01;
		return this;
	}
	
	/**
	 * Country Key.
	 * 
	 * @return
	 */
	@JsonProperty("country")
	public String getLand1() {
		return land1;
	}
	
	public IT0006ResponseWrapper setLand1(String land1) {
		this.land1 = land1;
		return this;
	}
	
	/**
	 * Country Key Text.
	 * 
	 * @return
	 */
	@JsonProperty("country_text")
	public String getLand1Text() {
		return land1Text;
	}
	
	public IT0006ResponseWrapper setLand1Text(String land1Text) {
		this.land1Text = land1Text;
		return this;
	}
	
	/**
	 * Region.
	 * 
	 * @return
	 */
	@JsonProperty("province")
	public String getState() {
		return state;
	}
	
	public IT0006ResponseWrapper setState(String state) {
		this.state = state;
		return this;
	}
	
	/**
	 * Region.
	 * 
	 * @return
	 */
	@JsonProperty("province_text")
	public String getStateText() {
		return stateText;
	}
	
	public IT0006ResponseWrapper setStateText(String stateText) {
		this.stateText = stateText;
		return this;
	}
	

	/**
	 * GET Document Status.
	 * 
	 * @return
	 */
	@JsonProperty("document_status")
	public String getDocumentStatus() {
		return documentStatus;
	}
	
	public IT0006ResponseWrapper setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus.name();
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type")
	public String getAttachmentType() {
		return attachmentType;
	}
	
	public IT0006ResponseWrapper setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
		return this;
	}
	
	/**
	 * GET Attachment Type Text.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type_text")
	public String getAttachmentTypeText() {
		return attachmentTypeText;
	}
	
	public IT0006ResponseWrapper setAttachmentTypeText(String attachmentTypeText) {
		this.attachmentTypeText = attachmentTypeText;
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_path")
	public String getAttachmentPath() {
		return attachmentPath;
	}
	
	public IT0006ResponseWrapper setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
		return this;
	}
}