package com.abminvestama.hcms.rest.api.dto.response;

import java.text.SimpleDateFormat;

import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.TrxApprovalProcess;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class TrxApprovalProcessResponseWrapper extends ResourceSupport {
	private String trxId;
	private String cfgEntityDesc;
	private String entityObjectId;
	private Integer seq;
	private Integer process;
	private String roleName;
	private String approvalAt;
	private String approvalByName;
	private String approvalByPernr;
	private String approvalAction;
	private String approvalNote;
	
	private TrxApprovalProcessResponseWrapper() { }
	
	public TrxApprovalProcessResponseWrapper(TrxApprovalProcess trxApprovalProcess) {
		if (trxApprovalProcess == null) {
			new TrxApprovalProcessResponseWrapper();
		}
		else {
			this
				.setTrxId(trxApprovalProcess.getId())
				.setCfgEntityDesc(trxApprovalProcess.getCfgEntityId() != null ? trxApprovalProcess.getCfgEntityId().getEntityDesc() : "")
				.setEntityObjectId(trxApprovalProcess.getEntityObjectId())
				.setSeq(trxApprovalProcess.getSeq())
				.setProcess(trxApprovalProcess.getProcess())
				.setRoleName(trxApprovalProcess.getRoleId() != null ? trxApprovalProcess.getRoleId().getName() : "")
				.setApprovalAt(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(trxApprovalProcess.getApprovalAt()))
				.setApprovalByPernr(trxApprovalProcess.getApprovalBy() != null ? trxApprovalProcess.getApprovalBy().getEmployee().getPernr() + "" : "")
				.setApprovalByName(trxApprovalProcess.getApprovalBy() != null ? trxApprovalProcess.getApprovalBy().getEmployee().getEname() : "")
				.setApprovalAction(trxApprovalProcess.getApprovalAction() != null ? trxApprovalProcess.getApprovalAction() : "")
				.setApprovalNote(trxApprovalProcess.getApprovalNote() != null ? trxApprovalProcess.getApprovalNote() : "")
				;
		}
	}

	public String getTrxId() {
		return trxId;
	}

	public TrxApprovalProcessResponseWrapper setTrxId(String trxId) {
		this.trxId = trxId;
		return this;
	}

	public String getCfgEntityDesc() {
		return cfgEntityDesc;
	}

	public TrxApprovalProcessResponseWrapper setCfgEntityDesc(String cfgEntityDesc) {
		this.cfgEntityDesc = cfgEntityDesc;
		return this;
	}

	public String getEntityObjectId() {
		return entityObjectId;
	}

	public TrxApprovalProcessResponseWrapper setEntityObjectId(String entityObjectId) {
		this.entityObjectId = entityObjectId;
		return this;
	}

	public Integer getSeq() {
		return seq;
	}

	public TrxApprovalProcessResponseWrapper setSeq(Integer seq) {
		this.seq = seq;
		return this;
	}

	public Integer getProcess() {
		return process;
	}

	public TrxApprovalProcessResponseWrapper setProcess(Integer process) {
		this.process = process;
		return this;
	}

	public String getRoleName() {
		return roleName;
	}

	public TrxApprovalProcessResponseWrapper setRoleName(String roleName) {
		this.roleName = roleName;
		return this;
	}

	public String getApprovalAt() {
		return approvalAt;
	}

	public TrxApprovalProcessResponseWrapper setApprovalAt(String approvalAt) {
		this.approvalAt = approvalAt;
		return this;
	}

	public String getApprovalByName() {
		return approvalByName;
	}

	public TrxApprovalProcessResponseWrapper setApprovalByName(String approvalByName) {
		this.approvalByName = approvalByName;
		return this;
	}

	public String getApprovalByPernr() {
		return approvalByPernr;
	}

	public TrxApprovalProcessResponseWrapper setApprovalByPernr(String approvalByPernr) {
		this.approvalByPernr = approvalByPernr;
		return this;
	}

	public String getApprovalAction() {
		return approvalAction;
	}

	public TrxApprovalProcessResponseWrapper setApprovalAction(String approvalAction) {
		this.approvalAction = approvalAction;
		return this;
	}

	public String getApprovalNote() {
		return approvalNote;
	}

	public TrxApprovalProcessResponseWrapper setApprovalNote(String approvalNote) {
		this.approvalNote = approvalNote;
		return this;
	}

}
