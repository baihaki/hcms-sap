package com.abminvestama.hcms.rest.api.dto.request;

import java.util.Date;

import com.abminvestama.hcms.core.model.entity.User;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public class CaStlDtlRequestWrapper extends ZmedRequestWrapper {

	private static final long serialVersionUID = -6055114023225637095L;

	private String detailId;
	private Integer itemNo;
	private String reqNo;
	private Date stlDate;
	private String description;
	private String amount;
	private String currency;
	private Long orderId;
	private Date createdAt;
	private User createdBy;
	private Date updatedAt;
	private User updatedBy;
	private Integer poolBudget;

	@JsonProperty("detailid")
	public String getDetailId() {
		return detailId;
	}

	@JsonProperty("itemno")
	public Integer getItemNo() {
		return itemNo;
	}

	public void setReqNo(String reqNo) {
		this.reqNo = reqNo;
	}

	@JsonProperty("reqno")
	public String getReqNo() {
		return reqNo;
	}

	public void setStlDate(Date stlDate) {
		this.stlDate = stlDate;
	}

	@JsonProperty("stldate")
	public Date getStlDate() {
		return stlDate;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("amount")
	public String getAmount() {
		return amount;
	}

	@JsonProperty("currency")
	public String getCurrency() {
		return currency;
	}

	@JsonProperty("orderid")
	public Long getOrderId() {
		return orderId;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	@JsonProperty("createdat")
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	@JsonProperty("createdby")
	public User getCreatedBy() {
		return createdBy;
	}

	@JsonProperty("updatedat")
	public Date getUpdatedAt() {
		return updatedAt;
	}

	@JsonProperty("updatedby")
	public User getUpdatedBy() {
		return updatedBy;
	}

	@JsonProperty("poolbudget")
	public Integer getPoolBudget() {
		return poolBudget;
	}

}
