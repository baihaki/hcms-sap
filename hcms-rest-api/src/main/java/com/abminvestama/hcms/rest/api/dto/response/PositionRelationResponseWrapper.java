package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.PositionRelation;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.rest.api.model.constant.HCMSResourceIdentifier;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@JsonInclude(Include.NON_NULL)
public class PositionRelationResponseWrapper extends ResourceSupport {

	public static final String RESOURCE = HCMSResourceIdentifier.POSITION_RELATION.label();
	
	private String uuid;
	private String positionRelationTypeId;
	private String positionRelationTypeName;
	private String fromPositionId;
	private String fromPositionCode;
	private String fromPositionName;
	private String toPositionId;
	private String toPositionCode;
	private String toPositionName;
	private Date validFrom;
	private Date validThru;
	private AuthorResponseWrapper createdBy;
	private AuthorResponseWrapper updatedBy;
	
	public PositionRelationResponseWrapper(PositionRelation positionRelation) {
		this.uuid = positionRelation.getId();
		this.positionRelationTypeId = positionRelation.getPositionRelationType().getId();
		this.positionRelationTypeName = positionRelation.getPositionRelationType().getName();
		this.fromPositionId = positionRelation.getFromPosition().getId();
		this.fromPositionCode = positionRelation.getFromPosition().getCode();
		this.fromPositionName = positionRelation.getFromPosition().getName();
		this.toPositionId = positionRelation.getToPosition().getId();
		this.toPositionCode = positionRelation.getToPosition().getCode();
		this.toPositionName = positionRelation.getToPosition().getName();
		this.validFrom = positionRelation.getValidFrom();
		this.validThru = positionRelation.getValidThru();
	}
	
	@JsonProperty("id")
	public String getUuid() {
		return uuid;
	}
	
	@JsonProperty("position_relation_type_id")
	public String getPositionRelationTypeId() {
		return positionRelationTypeId;
	}
	
	@JsonProperty("position_relation_type_name")
	public String getPositionRelationTypeName() {
		return positionRelationTypeName;
	}
	
	@JsonProperty("from_position_id")
	public String getFromPositionId() {
		return fromPositionId;
	}
	
	@JsonProperty("from_position_code")
	public String getFromPositionCode() {
		return fromPositionCode;
	}
	
	@JsonProperty("from_position_name")
	public String getFromPositionName() {
		return fromPositionName;
	}
	
	@JsonProperty("to_position_id")
	public String getToPositionId() {
		return toPositionId;
	}
	
	@JsonProperty("to_position_code")
	public String getToPositionCode() {
		return toPositionCode;
	}
	
	@JsonProperty("to_position_name")
	public String getToPositionName() {
		return toPositionName;
	}
	
	@JsonProperty("valid_from")
	public Date getValidFrom() {
		return validFrom;
	}
	
	@JsonProperty("valid_thru")
	public Date getValidThru() {
		return validThru;
	}
	
	@JsonProperty("created_by")
	public AuthorResponseWrapper getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(AuthorResponseWrapper createdBy) {
		this.createdBy = createdBy;
	}
	
	public void setCreatedBy(User user, Link userLink) {
		AuthorResponseWrapper createdBy = new AuthorResponseWrapper(user, userLink);
		this.createdBy = createdBy;
	}
	
	@JsonProperty("updated_by")
	public AuthorResponseWrapper getUpdatedBy() {
		return updatedBy;
	}
	
	public void setUpdatedBy(AuthorResponseWrapper updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public void setUpdatedBy(User user, Link userLink) {
		AuthorResponseWrapper updatedBy = new AuthorResponseWrapper(user, userLink);
		this.updatedBy = updatedBy;
	}	
}