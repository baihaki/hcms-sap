package com.abminvestama.hcms.rest.api.helper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.IT2001;
import com.abminvestama.hcms.core.model.entity.IT2006;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.T554T;
import com.abminvestama.hcms.core.service.api.business.query.IT2001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT2006QueryService;
import com.abminvestama.hcms.core.service.api.business.query.T554TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T591SQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT2001RequestWrapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add method: getAbsenceQuotas</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add field: address, phone, mobile, notes, reason, infotype, process</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add field: start, end</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("it2001RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT2001RequestBuilderUtil {


	private IT2001QueryService it2001QueryService;

	@Autowired
	void setIT2001QueryService(IT2001QueryService it2001QueryService) {
		this.it2001QueryService = it2001QueryService;
	}
	

	private IT2006QueryService it2006QueryService;	

	@Autowired
	void setIT2006QueryService(IT2006QueryService it2006QueryService) {
		this.it2006QueryService = it2006QueryService;
	}
	
	private T591SQueryService t591sQueryService;
	
	@Autowired
	void setT591SQueryService(T591SQueryService t591sQueryService) {
		this.t591sQueryService = t591sQueryService;
	}
	
	
	private T554TQueryService t554tQueryService;
	
	@Autowired
	void setT554TQueryService(T554TQueryService t554tQueryService) {
		this.t554tQueryService = t554tQueryService;
	}

	/**
	 * Compare IT2001 request payload with existing IT2001 in the database.
	 * Update existing IT2001 data with the latest data comes from the request payload. 
	 * 
	 * @param requestPayload request data 
	 * @param it2001DB current existing IT2001 in the database.
	 * @return updated IT2001 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT2001, IT2001RequestWrapper, String> compareAndReturnUpdatedData(IT2001RequestWrapper requestPayload, IT2001 it2001DB) {

		RequestObjectComparatorContainer<IT2001, IT2001RequestWrapper, String> objectContainer = null; 
		
		if (it2001DB == null) {
			it2001DB = new IT2001();
			

			try {

				Optional<IT2001> existingIT2001 = it2001QueryService.findOneByCompositeKeyWithInfotype(
						requestPayload.getPernr(), requestPayload.getInfotype(), requestPayload.getSubty(), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda()));
				
				if (existingIT2001.isPresent()) {
					throw new DataViolationException("Duplicate IT2001 Data for [ssn=" + requestPayload.getPernr() +
							",infotype=" + requestPayload.getInfotype() + ",subtype=" + requestPayload.getSubty() +
							",end_date=" + requestPayload.getEndda() + ",begin_date=" + requestPayload.getBegda());
				}
				
				
				
				if (SAPInfoType.ABSENCES.infoType().equals(requestPayload.getInfotype()) &&
						getAbsenceQuotas(requestPayload) <= requestPayload.getAbwtg()) {
					throw new DataViolationException("Number of days of leave exceeds absence quota");
				}
				
				if (SAPInfoType.ABSENCES.infoType().equals(requestPayload.getInfotype()) ||
						SAPInfoType.ATTENDANCES.infoType().equals(requestPayload.getInfotype()) )
				{
					it2001DB.setId(
							new ITCompositeKeys(requestPayload.getPernr(), requestPayload.getInfotype() , requestPayload.getSubty(), 
									CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
									CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda())));

					Optional<T554T> t554t = t554tQueryService.findById(Optional.ofNullable(requestPayload.getAwart()));
					it2001DB.setAwart(t554t.isPresent() ? t554t.get() : null);
					it2001DB.setAbwtg(requestPayload.getAbwtg());
					it2001DB.setStdaz(requestPayload.getStdaz() > 0 ? requestPayload.getStdaz() : null);
					it2001DB.setKaltg(requestPayload.getKaltg());
					it2001DB.setSeqnr(1L);
					DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
					it2001DB.setStart(StringUtils.isNotEmpty(requestPayload.getStart()) ?
							formatter.parse(requestPayload.getStart()) : null);
					it2001DB.setEnd(StringUtils.isNotEmpty(requestPayload.getEnd()) ?
							formatter.parse(requestPayload.getEnd()) : null);
					it2001DB.setAddress(requestPayload.getAddress());
					it2001DB.setPhone(requestPayload.getPhone());
					it2001DB.setMobile(requestPayload.getMobile());
					it2001DB.setNotes(requestPayload.getNotes());
					it2001DB.setReason(requestPayload.getReason());
					it2001DB.setProcess(requestPayload.getProcess());
				} else {
					throw new DataViolationException("Invalid infotype");
				}
			} catch (Exception ignored) {
				it2001DB.setReason(ignored.getMessage());
				System.err.println(ignored);
			}
			
			objectContainer = new RequestObjectComparatorContainer<>(it2001DB, requestPayload);
			
			
			
			
		} else {
			if (requestPayload.getAwart() != null) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAwart(), it2001DB.getAwart().getAwart())) {
					try {
						Optional<T554T> newT554T = t554tQueryService.findById(Optional.ofNullable(requestPayload.getAwart()));
						if (newT554T.isPresent()) {
							it2001DB.setAwart(newT554T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						// t554t not found... ignore change
					}
				}
			}
			
			if (requestPayload.getAbwtg() > 0 && requestPayload.getAbwtg() != it2001DB.getAbwtg().doubleValue()) {
				it2001DB.setAbwtg(requestPayload.getAbwtg());
				requestPayload.setDataChanged(true);
			}
			
			if (requestPayload.getStdaz() > 0 && requestPayload.getStdaz() != it2001DB.getStdaz().doubleValue()) {
				it2001DB.setStdaz(requestPayload.getStdaz());
				requestPayload.setDataChanged(true);
			}
			
			if (requestPayload.getKaltg() > 0 && requestPayload.getKaltg() != it2001DB.getKaltg().doubleValue()) {
				it2001DB.setKaltg(requestPayload.getKaltg());
				requestPayload.setDataChanged(true);
			}
			
			DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
			
			if (StringUtils.isNotEmpty(requestPayload.getStart())) {
				if (it2001DB.getStart() == null) {
					try {
						it2001DB.setStart(formatter.parse(requestPayload.getStart()));
						requestPayload.setDataChanged(true);
					} catch (ParseException e) {
					}
				} else if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getStart(), it2001DB.getStart().toString())) {
					try {
						it2001DB.setStart(formatter.parse(requestPayload.getStart()));
						requestPayload.setDataChanged(true);
					} catch (ParseException e) {
					}
				}
			}
			
			if (StringUtils.isNotEmpty(requestPayload.getEnd())) {
				if (it2001DB.getEnd() == null) {
					try {
						it2001DB.setEnd(formatter.parse(requestPayload.getEnd()));
						requestPayload.setDataChanged(true);
					} catch (ParseException e) {
					}
				} else if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getEnd(), it2001DB.getEnd().toString())) {
					try {
						it2001DB.setEnd(formatter.parse(requestPayload.getEnd()));
						requestPayload.setDataChanged(true);
					} catch (ParseException e) {
					}
				}
			}

			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAddress(), it2001DB.getAddress())) {
				it2001DB.setAddress(StringUtils.defaultString(requestPayload.getAddress()));
				requestPayload.setDataChanged(true);
			}

			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPhone(), it2001DB.getPhone())) {
				it2001DB.setPhone(StringUtils.defaultString(requestPayload.getPhone()));
				requestPayload.setDataChanged(true);
			}

			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getMobile(), it2001DB.getMobile())) {
				it2001DB.setMobile(StringUtils.defaultString(requestPayload.getMobile()));
				requestPayload.setDataChanged(true);
			}

			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getNotes(), it2001DB.getNotes())) {
				it2001DB.setNotes(StringUtils.defaultString(requestPayload.getNotes()));
				requestPayload.setDataChanged(true);
			}

			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getReason(), it2001DB.getReason())) {
				it2001DB.setReason(StringUtils.defaultString(requestPayload.getReason()));
				requestPayload.setDataChanged(true);
			}
			
			if (StringUtils.isNotBlank(requestPayload.getProcess())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getProcess(), it2001DB.getProcess())) {
					it2001DB.setProcess(requestPayload.getProcess());
					requestPayload.setDataChanged(true);
				}
			}
			
			objectContainer = new RequestObjectComparatorContainer<>(it2001DB, requestPayload);			
		}
		
		JSONObject json = new JSONObject(requestPayload);
		objectContainer.setEventData(json.toString());
		
		return objectContainer;
	}
	
	private double getAbsenceQuotas(IT2001RequestWrapper requestPayload) {
		Date begda = CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda());
		Date endda = CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda());
		Calendar calBegda = Calendar.getInstance();
		calBegda.setTime(begda);
		String quotaBegda = calBegda.get(Calendar.YEAR) + "-01-01";
		String quotaEndda = calBegda.get(Calendar.YEAR) + "-12-31";
		
		double totalQuota = 365.00;

		Optional<IT2006> existingIT2006 = it2006QueryService.findOneByCompositeKey(
				requestPayload.getPernr(), requestPayload.getSubty(), 
				CommonDateFunction.convertDateRequestParameterIntoDate(quotaEndda), 
				CommonDateFunction.convertDateRequestParameterIntoDate(quotaBegda));
		if (existingIT2006.isPresent() && existingIT2006.get().getKverb() != null) {
			totalQuota = existingIT2006.get().getKverb().doubleValue();
		}
		
		Calendar calEndda = Calendar.getInstance();
		calEndda.setTime(endda);
		if (calEndda.get(Calendar.YEAR) > calBegda.get(Calendar.YEAR)) {
			quotaBegda = calEndda.get(Calendar.YEAR) + "-01-01";
			quotaEndda = calEndda.get(Calendar.YEAR) + "-12-31";
			existingIT2006 = it2006QueryService.findOneByCompositeKey(
					requestPayload.getPernr(), requestPayload.getSubty(), 
					CommonDateFunction.convertDateRequestParameterIntoDate(quotaEndda), 
					CommonDateFunction.convertDateRequestParameterIntoDate(quotaBegda));
			if (existingIT2006.isPresent() && existingIT2006.get().getKverb() != null) {
				totalQuota = totalQuota + existingIT2006.get().getKverb().doubleValue();
			}
		}
		
		return totalQuota;
	}
}