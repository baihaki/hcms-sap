package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.Position;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.rest.api.model.constant.HCMSResourceIdentifier;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@JsonInclude(Include.NON_NULL)
public class PositionResponseWrapper extends ResourceSupport {

	public static final String RESOURCE = HCMSResourceIdentifier.POSITION.label();
	
	private String uuid;
	private String code;
	private String name;
	private String description;
	private Date createdAt;
	private AuthorResponseWrapper createdBy;
	private Date updatedAt;
	private AuthorResponseWrapper updatedBy;
	
	public PositionResponseWrapper(Position position) {
		this.uuid = position.getId();
		this.code = position.getCode();
		this.name = position.getName();
		this.description = position.getDescription();
		this.createdAt = position.getCreatedAt();
		this.updatedAt = position.getUpdatedAt();
	}
	
	@JsonProperty("id")
	public String getUuid() {
		return uuid;
	}
	
	@JsonProperty("position_code")
	public String getCode() {
		return code;
	}
	
	@JsonProperty("position_name")
	public String getName() {
		return name;
	}
	
	@JsonProperty("position_description")
	public String getDescription() {
		return description;
	}
	
	@JsonProperty("created_at")
	public Date getCreatedAt() {
		return createdAt;
	}
	
	@JsonProperty("created_by")
	public AuthorResponseWrapper getCreatedBy() {
		return createdBy;
	}
	
	public PositionResponseWrapper setCreatedBy(Link link) {
		AuthorResponseWrapper createdBy = new AuthorResponseWrapper(link);
		this.createdBy = createdBy;
		return this;
	}
	
	public PositionResponseWrapper setCreatedBy(User user, Link link) {
		AuthorResponseWrapper createdBy = new AuthorResponseWrapper(user, link);
		this.createdBy = createdBy;
		return this;
	}
	
	@JsonProperty("updated_at")
	public Date getUpdatedAt() {
		return updatedAt;
	}
	
	@JsonProperty("updated_by")
	public AuthorResponseWrapper getUpdatedBy() {
		return updatedBy;
	}
	
	public PositionResponseWrapper setUpdatedBy(Link link) {
		AuthorResponseWrapper updatedBy = new AuthorResponseWrapper(link);
		this.updatedBy = updatedBy;
		return this;
	}
	
	public PositionResponseWrapper setUpdatedBy(User user, Link link) {
		AuthorResponseWrapper updatedBy = new AuthorResponseWrapper(user, link);
		this.updatedBy = updatedBy;
		return this;
	}
}