package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class IT0105RequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = 8730722420343459773L;
	
	private String usrty;
	private String usridLong;
	
	@JsonProperty("communication_type")
	public String getUsrty() {
		return usrty;
	}
	
	@JsonProperty("long_id")
	public String getUsridLong() {
		return usridLong;
	}
}