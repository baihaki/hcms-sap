package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.Date;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.CaStlDtl;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.CaStlDtlCommandService;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.query.CaStlDtlQueryService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.CaStlDtlRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.CaStlDtlResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.CaStlDtlRequestBuilderUtil;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v3/ca_stldtl")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CaStlDtlController extends AbstractResource {
	private EventLogCommandService eventLogCommandService;
	private HCMSEntityQueryService hcmsEntityQueryService;
	private UserQueryService userQueryService;
	private CommonServiceFactory commonServiceFactory;
	private CaStlDtlRequestBuilderUtil caStlDtlRequestBuilderUtil;
	private CaStlDtlCommandService caStlDtlCommandService;
	private CaStlDtlQueryService caStlDtlQueryService;
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}

	@Autowired
	void setCaStlDtlRequestBuilderUtil(CaStlDtlRequestBuilderUtil caStlDtlRequestBuilderUtil) {
		this.caStlDtlRequestBuilderUtil = caStlDtlRequestBuilderUtil;
	}

	@Autowired
	void setCaStlDtlCommandService(CaStlDtlCommandService caStlDtlCommandService) {
		this.caStlDtlCommandService = caStlDtlCommandService;
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated CaStlDtlRequestWrapper request, BindingResult result)
				throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(
					new ArrayData<>(ExceptionResponseWrapper.getErrors(result.getFieldErrors(), HttpMethod.PUT)),
					"Validation error!");
		}
		else {
			// entity model for SAP classes
			/**
			Optional<HCMSEntity> caStlDtlHCMSEntity = hcmsEntityQueryService.findByEntityName(CaStlDtl.class.getSimpleName());
			if (!caStlDtlHCMSEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + CaStlDtl.class.getSimpleName() + "' !!!");
			}
			*/
			
			try {
				// GET SETTLEMENT HEADER

				RequestObjectComparatorContainer<CaStlDtl, CaStlDtlRequestWrapper, String> newObjectContainer = caStlDtlRequestBuilderUtil
						.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(AccessRole.ADMIN_GROUP));
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login first!");
				}
				
				/*
				// employee can't create claim for other employee, except Admin
				if (currentUser.get().getEmployee().getPernr() != request.getPernr() &&
						!currentUser.get().getRoles().contains(adminRole.get())) {
					throw new AuthorizationException(
							"Insufficient Privileges. Please login as 'ADMIN'!");
				}
				*/		
				
				Optional<CaStlDtl> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
					Date thisDate = new Date();
					
					newObjectContainer.getEntity().get().setCreatedAt(thisDate);
					newObjectContainer.getEntity().get().setCreatedBy(currentUser.get());
					newObjectContainer.getEntity().get().setUpdatedAt(thisDate);
					newObjectContainer.getEntity().get().setUpdatedBy(currentUser.get());
					
					savedEntity = caStlDtlCommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create CaStlDtl data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("id=" + savedEntity.get().getDetailId());
				key.append(",reqno=" + savedEntity.get().getReqNo());
				key.append(",itemno=" + savedEntity.get().getItemNo());
				
				/**
				EventLog eventLog = new EventLog(caStlDtlHCMSEntity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				// Don't send email notification email for temporary settlement number
				eventLog.setMailNotification(false);
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				 */

				CaStlDtlResponseWrapper responseWrapper = new CaStlDtlResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);
				
				// Post to MuleSoft - Start
								
				// Post to MuleSoft - End	
				
			} catch (DataViolationException e) {
				System.out.println(e.getMessage());
				response.setMessage(e.getMessage());
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(CaStlDtlController.class).create(request, result)).withSelfRel());		
		return response;
	}
}
