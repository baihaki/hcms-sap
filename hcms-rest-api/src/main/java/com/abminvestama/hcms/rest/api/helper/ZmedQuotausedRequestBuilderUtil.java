package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyQuotaused;
import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;
import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;
import com.abminvestama.hcms.core.service.api.business.query.ZmedQuotausedQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedQuotausedRequestWrapper;

@Component("ZmedQuotausedRequestBuilderUtil")
@Transactional(readOnly = true)
public class ZmedQuotausedRequestBuilderUtil {

	
	
	private T001QueryService t001QueryService;
	private ZmedQuotausedQueryService zmedQuotausedQueryService;
	
	@Autowired
	void setZmedQuotausedQueryService(ZmedQuotausedQueryService zmedQuotausedQueryService) {
		this.zmedQuotausedQueryService = zmedQuotausedQueryService;
	}
	
	@Autowired
	void setT001QueryService(T001QueryService t001QueryService) {
		this.t001QueryService = t001QueryService;
	}
	
	public RequestObjectComparatorContainer<ZmedQuotaused, ZmedQuotausedRequestWrapper, String> compareAndReturnUpdatedData(ZmedQuotausedRequestWrapper requestPayload, ZmedQuotaused zmedQuotausedDB) {
		
		RequestObjectComparatorContainer<ZmedQuotaused, ZmedQuotausedRequestWrapper, String> objectContainer = null; 
		
	if (zmedQuotausedDB == null) {
		zmedQuotausedDB = new ZmedQuotaused();
		
		try {
			Optional<ZmedQuotaused> existingZmedQuotaused = zmedQuotausedQueryService.findOneByCompositeKey(
					requestPayload.getGjahr(), requestPayload.getBukrs(), 
					requestPayload.getPernr(), requestPayload.getKodequo());
			
			if (existingZmedQuotaused.isPresent()) {
				throw new DataViolationException("Duplicate ZmedQuotaused Data for [ssn=" + requestPayload.getPernr());
			}
			zmedQuotausedDB.setId(
					new ZmedCompositeKeyQuotaused(requestPayload.getGjahr(), requestPayload.getBukrs(), 
							requestPayload.getPernr(), requestPayload.getKodequo()));
			
			zmedQuotausedDB.setGjahr(requestPayload.getGjahr());
			zmedQuotausedDB.setPernr(requestPayload.getPernr());
			zmedQuotausedDB.setQuotamt(Double.parseDouble(requestPayload.getQuotamt()));
			zmedQuotausedDB.setKodequo(requestPayload.getKodequo());
			Optional<T001> bnka = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));				
			zmedQuotausedDB.setBukrs(bnka.isPresent() ? bnka.get() : null);
			//zmedQuotausedDB.setBukrs(requestPayload.getBukrs());
			
		} catch (Exception ignored) {
			System.err.println(ignored);
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedQuotausedDB, requestPayload);
		
	} else {
		
		if (StringUtils.isNotBlank(""+requestPayload.getGjahr())) {
			if (CommonComparatorFunction.isDifferentStringValues(""+requestPayload.getGjahr(), zmedQuotausedDB.getGjahr().toString())) {
				zmedQuotausedDB.setGjahr(requestPayload.getGjahr());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getKodequo())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getKodequo(), zmedQuotausedDB.getKodequo())) {
				zmedQuotausedDB.setKodequo(requestPayload.getKodequo());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedQuotausedDB.getBukrs() != null ? zmedQuotausedDB.getBukrs().getBukrs() : StringUtils.EMPTY)) {
				try {
					Optional<T001> newT001 = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));
					if (newT001.isPresent()) {
						zmedQuotausedDB.setBukrs(newT001.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					//T535N not found... cancel the update process
				}
			}
		}
		
		
		/*if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedQuotausedDB.getBukrs())) {
				zmedQuotausedDB.setBukrs(requestPayload.getBukrs());
				requestPayload.setDataChanged(true);
			}
		}*/

			
		
		/*if (StringUtils.isNotBlank(requestPayload.getPernr())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPernr(), zmedQuotausedDB.getPernr().toString())) {
				zmedQuotausedDB.setPernr(Double.parseDouble(requestPayload.getPernr()));
				requestPayload.setDataChanged(true);
			}
		}*/
		
		if (StringUtils.isNotBlank(requestPayload.getQuotamt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getQuotamt(),zmedQuotausedDB.getQuotamt().toString())) {
				zmedQuotausedDB.setQuotamt(Double.parseDouble(requestPayload.getQuotamt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		
		
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedQuotausedDB, requestPayload);	
				
	}
	
	JSONObject json = new JSONObject(requestPayload);
	objectContainer.setEventData(json.toString());
	
	return objectContainer;
}
}
