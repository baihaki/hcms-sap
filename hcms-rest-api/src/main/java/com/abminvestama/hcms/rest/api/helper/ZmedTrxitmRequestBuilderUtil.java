package com.abminvestama.hcms.rest.api.helper;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyCostype;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyTrxitm;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;
import com.abminvestama.hcms.core.service.api.business.query.ZmedCostypeQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedTrxitmQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedTrxitmRequestWrapper;


/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add zmedCostypeQueryService to fetch Cost Type Desc and set to zcostyp field</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("ZmedTrxitmRequestBuilderUtil")
@Transactional(readOnly = true)
public class ZmedTrxitmRequestBuilderUtil {

	
	private T001QueryService t001QueryService;
	private ZmedTrxitmQueryService zmedTrxitmQueryService;
	private ZmedCostypeQueryService zmedCostypeQueryService;
	
	@Autowired
	void setZmedTrxitmQueryService(ZmedTrxitmQueryService zmedTrxitmQueryService) {
		this.zmedTrxitmQueryService = zmedTrxitmQueryService;
	}
	
	@Autowired
	void setT001QueryService(T001QueryService t001QueryService) {
		this.t001QueryService = t001QueryService;
	}
	
	@Autowired
	void setZmedCostypeQueryService(ZmedCostypeQueryService zmedCostypeQueryService) {
		this.zmedCostypeQueryService = zmedCostypeQueryService;
	}
	
	public RequestObjectComparatorContainer<ZmedTrxitm, ZmedTrxitmRequestWrapper, String> compareAndReturnUpdatedData(ZmedTrxitmRequestWrapper requestPayload, ZmedTrxitm zmedTrxitmDB) {
		
		RequestObjectComparatorContainer<ZmedTrxitm, ZmedTrxitmRequestWrapper, String> objectContainer = null; 
		
	if (zmedTrxitmDB == null) {
		zmedTrxitmDB = new ZmedTrxitm();
		
		try {
			//Optional<ZmedTrxitm> existingZmedTrxitm = zmedTrxitmQueryService.findOneByCompositeKey(
			//		requestPayload.getItmno());
			
			//if (existingZmedTrxitm.isPresent()) {
			//	throw new DataViolationException("Duplicate ZmedTrxitm Data for [ssn=" + requestPayload.getItmno());
			//}
			zmedTrxitmDB.setId(
					new ZmedCompositeKeyTrxitm(requestPayload.getItmno(), requestPayload.getClmno()));
			
			if(!(requestPayload.getBilldt()==null))
				zmedTrxitmDB.setBilldt(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBilldt()));
			zmedTrxitmDB.setItmno(StringUtils.defaultString(requestPayload.getItmno(), StringUtils.EMPTY));//
			zmedTrxitmDB.setClmno(StringUtils.defaultString(requestPayload.getClmno(), StringUtils.EMPTY));////
			zmedTrxitmDB.setClmamt(requestPayload.getClmamt() != null ? Double.parseDouble(requestPayload.getClmamt()):0.0);
			zmedTrxitmDB.setRejamt(requestPayload.getRejamt() != null ? Double.parseDouble(requestPayload.getRejamt()):0.0);
			zmedTrxitmDB.setAppamt(requestPayload.getAppamt() != null ? Double.parseDouble(requestPayload.getAppamt()):0.0);
			zmedTrxitmDB.setRelat(StringUtils.defaultString(requestPayload.getRelat(), StringUtils.EMPTY));
			zmedTrxitmDB.setUmur(StringUtils.defaultString(requestPayload.getUmur(), StringUtils.EMPTY));
			zmedTrxitmDB.setPasien(StringUtils.defaultString(requestPayload.getPasien(), StringUtils.EMPTY));//
			zmedTrxitmDB.setKodemed(StringUtils.defaultString(requestPayload.getKodemed(), StringUtils.EMPTY));//
			zmedTrxitmDB.setKodecos(StringUtils.defaultString(requestPayload.getKodecos(), StringUtils.EMPTY));//			
			Optional<T001> bnka = t001QueryService.findByBukrs(requestPayload.getBukrs());//				
			zmedTrxitmDB.setBukrs(bnka.isPresent() ? bnka.get() : null);
			//zmedTrxitmDB.setBukrs(requestPayload.getBukrs());
			
			String costDesc = StringUtils.defaultString(requestPayload.getZcostyp(), StringUtils.EMPTY);
			Collection<ZmedCostype> listZmedCostype = zmedCostypeQueryService.findByKodemed(zmedTrxitmDB.getBukrs().getBukrs(), zmedTrxitmDB.getKodemed(), 
					CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31"));
			if (!listZmedCostype.isEmpty()) {
				List<ZmedCostype> list =listZmedCostype.stream().collect(Collectors.toList());
				for (ZmedCostype zmedCostype : list) {
					if (zmedCostype.getKodecos().equals(zmedTrxitmDB.getKodecos())) {
						costDesc = zmedCostype.getDescr();
						break;
					}
				}
			}
			zmedTrxitmDB.setZcostyp(costDesc);
			
		} catch (Exception ignored) {
			System.err.println(ignored);
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedTrxitmDB, requestPayload);
		
	} else {
		
		if (StringUtils.isNotBlank(requestPayload.getItmno())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getItmno(), zmedTrxitmDB.getItmno())) {
				zmedTrxitmDB.setItmno(requestPayload.getItmno());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getClmno())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getClmno(), zmedTrxitmDB.getClmno())) {
				zmedTrxitmDB.setClmno(requestPayload.getClmno());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getZcostyp())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getZcostyp(), zmedTrxitmDB.getZcostyp())) {
				zmedTrxitmDB.setZcostyp(requestPayload.getZcostyp());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getPasien())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPasien(), zmedTrxitmDB.getPasien())) {
				zmedTrxitmDB.setPasien(requestPayload.getPasien());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getUmur())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getUmur(), zmedTrxitmDB.getUmur())) {
				zmedTrxitmDB.setUmur(requestPayload.getUmur());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getRelat())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getRelat(), zmedTrxitmDB.getRelat())) {
				zmedTrxitmDB.setRelat(requestPayload.getRelat());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getClmamt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getClmamt(), zmedTrxitmDB.getClmamt().toString())) {
				zmedTrxitmDB.setClmamt(Double.parseDouble(requestPayload.getClmamt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getRejamt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getRejamt(), zmedTrxitmDB.getRejamt().toString())) {
				zmedTrxitmDB.setRejamt(Double.parseDouble(requestPayload.getRejamt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getAppamt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAppamt(), zmedTrxitmDB.getAppamt().toString())) {
				zmedTrxitmDB.setAppamt(Double.parseDouble(requestPayload.getAppamt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getKodemed())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getKodemed(), zmedTrxitmDB.getKodemed())) {
				zmedTrxitmDB.setKodemed(requestPayload.getKodemed());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedTrxitmDB.getBukrs() != null ? zmedTrxitmDB.getBukrs().getBukrs() : StringUtils.EMPTY)) {
				try {
					Optional<T001> newT001 = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));
					if (newT001.isPresent()) {
						zmedTrxitmDB.setBukrs(newT001.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					//T535N not found... cancel the update process
				}
			}
		}
		
		
		
		/*if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedTrxitmDB.getBukrs())) {
				zmedTrxitmDB.setBukrs(requestPayload.getBukrs());
				requestPayload.setDataChanged(true);
			}
		}*/

			
		
		if (StringUtils.isNotBlank(requestPayload.getKodecos())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getKodecos(), zmedTrxitmDB.getKodecos())) {
				zmedTrxitmDB.setKodecos(requestPayload.getKodecos());
				requestPayload.setDataChanged(true);
			}
		}
		
		
		
		if (StringUtils.isNotBlank(requestPayload.getBilldt())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBilldt()), (zmedTrxitmDB.getBilldt())) ){
				zmedTrxitmDB.setBilldt(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBilldt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedTrxitmDB, requestPayload);	
				
	}
	
	JSONObject json = new JSONObject(requestPayload);
	objectContainer.setEventData(json.toString());
	
	return objectContainer;
}
}
