package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyTrxhdr;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;
import com.abminvestama.hcms.core.service.api.business.query.ZmedTrxhdrQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedTrxhdrRequestWrapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add field: reason</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add field: process</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 * 
 */
@Component("ZmedTrxhdrRequestBuilderUtil")
@Transactional(readOnly = true)
public class ZmedTrxhdrRequestBuilderUtil {

	
	private T001QueryService t001QueryService;
	private ZmedTrxhdrQueryService zmedTrxhdrQueryService;
	
	@Autowired
	void setZmedTrxhdrQueryService(ZmedTrxhdrQueryService zmedTrxhdrQueryService) {
		this.zmedTrxhdrQueryService = zmedTrxhdrQueryService;
	}
	
	@Autowired
	void setT001QueryService(T001QueryService t001QueryService) {
		this.t001QueryService = t001QueryService;
	}
	
	public RequestObjectComparatorContainer<ZmedTrxhdr, ZmedTrxhdrRequestWrapper, String> compareAndReturnUpdatedData
			(ZmedTrxhdrRequestWrapper requestPayload, ZmedTrxhdr zmedTrxhdrDB) throws DataViolationException {
		
		RequestObjectComparatorContainer<ZmedTrxhdr, ZmedTrxhdrRequestWrapper, String> objectContainer = null; 
		
	if (zmedTrxhdrDB == null) {
		zmedTrxhdrDB = new ZmedTrxhdr();
		
		try {
			Optional<ZmedTrxhdr> existingZmedTrxhdr = zmedTrxhdrQueryService.findOneByCompositeKey(
					requestPayload.getBukrs(), requestPayload.getZclmno());
			
			if (existingZmedTrxhdr.isPresent()) {
				throw new DataViolationException("Duplicate ZmedTrxhdr Data for [ssn=" + requestPayload.getZclmno());
			}
			
			zmedTrxhdrDB.setId(
					new ZmedCompositeKeyTrxhdr(requestPayload.getBukrs(), requestPayload.getZclmno()));
			
			zmedTrxhdrDB.setPernr(requestPayload.getPernr());
			zmedTrxhdrDB.setZclmno(StringUtils.defaultString(requestPayload.getZclmno(), StringUtils.EMPTY));//
			zmedTrxhdrDB.setOrgtx(StringUtils.defaultString(requestPayload.getOrgtx(), StringUtils.EMPTY));//
			zmedTrxhdrDB.setZdivision(StringUtils.defaultString(requestPayload.getZdivision(), StringUtils.EMPTY));//
			zmedTrxhdrDB.setEname(StringUtils.defaultString(requestPayload.getEname(), StringUtils.EMPTY));//
			zmedTrxhdrDB.setClmst(StringUtils.defaultString(requestPayload.getClmst(), StringUtils.EMPTY));//
			zmedTrxhdrDB.setClmby(StringUtils.defaultString(requestPayload.getClmby(), StringUtils.EMPTY));//
			zmedTrxhdrDB.setAprby(StringUtils.defaultString(requestPayload.getAprby(), StringUtils.EMPTY));//
			zmedTrxhdrDB.setPayby(StringUtils.defaultString(requestPayload.getPayby(), StringUtils.EMPTY));//
			zmedTrxhdrDB.setRevby(StringUtils.defaultString(requestPayload.getRevby(), StringUtils.EMPTY));//
			zmedTrxhdrDB.setPaymethod(StringUtils.defaultString(requestPayload.getPaymethod(), StringUtils.EMPTY));//
			zmedTrxhdrDB.setBelnr(StringUtils.defaultString(requestPayload.getBelnr(), StringUtils.EMPTY));//
			zmedTrxhdrDB.setJamin(StringUtils.defaultString(requestPayload.getJamin(), StringUtils.EMPTY));//
			if (requestPayload.getKodemed() != null && !requestPayload.getKodemed().equalsIgnoreCase("MHOSP")) 
				zmedTrxhdrDB.setJamin(StringUtils.EMPTY); //Selain Medical Type = HOSPITALIZATION, set Vendor Number = EMPTY.
			zmedTrxhdrDB.setNotex(StringUtils.defaultString(requestPayload.getNotex(), StringUtils.EMPTY));//
			zmedTrxhdrDB.setPagu1(requestPayload.getPagu1() != null ? Double.parseDouble(requestPayload.getPagu1()): 0.0);
			zmedTrxhdrDB.setPagu2(requestPayload.getPagu2() != null ? Double.parseDouble(requestPayload.getPagu2()): 0.0);
			zmedTrxhdrDB.setPagudent(requestPayload.getPagudent() != null ? Double.parseDouble(requestPayload.getPagudent()): 0.0);
			zmedTrxhdrDB.setPagu2byemp(requestPayload.getPagu2byemp() != null ? Double.parseDouble(requestPayload.getPagu2byemp()): 0.0);
			zmedTrxhdrDB.setTotamt(requestPayload.getTotamt() != null ? Double.parseDouble(requestPayload.getTotamt()): 0.0);
			zmedTrxhdrDB.setMedtydesc(StringUtils.defaultString(requestPayload.getMedtydesc(), StringUtils.EMPTY));//
			zmedTrxhdrDB.setPenyakit(StringUtils.defaultString(requestPayload.getPenyakit(), StringUtils.EMPTY));//
			zmedTrxhdrDB.setClmamt(requestPayload.getClmamt() != null ? Double.parseDouble(requestPayload.getClmamt()): 0.0);//
			zmedTrxhdrDB.setRejamt(requestPayload.getRejamt() != null ? Double.parseDouble(requestPayload.getRejamt()): 0.0);//
			zmedTrxhdrDB.setAppamt(requestPayload.getAppamt() != null ? Double.parseDouble(requestPayload.getAppamt()): 0.0);//
			zmedTrxhdrDB.setPasien(StringUtils.defaultString(requestPayload.getPasien(), StringUtils.EMPTY));//
			zmedTrxhdrDB.setKodemed(StringUtils.defaultString(requestPayload.getKodemed(), StringUtils.EMPTY));//
			Optional<T001> bnka = t001QueryService.findByBukrs(requestPayload.getBukrs());//				
			zmedTrxhdrDB.setBukrs(bnka.isPresent() ? bnka.get() : null);
			//zmedTrxhdrDB.setBukrs(requestPayload.getBukrs());
			if (StringUtils.isNotEmpty(requestPayload.getBilldt()))
				zmedTrxhdrDB.setBilldt(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBilldt()));
			if (StringUtils.isNotEmpty(requestPayload.getTglmasuk()))
				zmedTrxhdrDB.setTglmasuk(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getTglmasuk()));
			if (StringUtils.isNotEmpty(requestPayload.getTglkeluar()))
				zmedTrxhdrDB.setTglkeluar(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getTglkeluar()));
			if (StringUtils.isNotEmpty(requestPayload.getClmdt()))
				zmedTrxhdrDB.setClmdt(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getClmdt()));
			if (StringUtils.isNotEmpty(requestPayload.getAprdt()))
				zmedTrxhdrDB.setAprdt(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getAprdt()));
			if (StringUtils.isNotEmpty(requestPayload.getPaydt()))
				zmedTrxhdrDB.setPaydt(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getPaydt()));
			if (StringUtils.isNotEmpty(requestPayload.getRevdt()))
				zmedTrxhdrDB.setRevdt(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getRevdt()));
			zmedTrxhdrDB.setProcess(requestPayload.getProcess());
			zmedTrxhdrDB.setReason(requestPayload.getReason());
			
			
		} catch (Exception ignored) {
			System.err.println(ignored);
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedTrxhdrDB, requestPayload);
		
	} else {
		
		if (StringUtils.isNotBlank(requestPayload.getOrgtx())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getOrgtx(), zmedTrxhdrDB.getOrgtx())) {
				zmedTrxhdrDB.setOrgtx(requestPayload.getOrgtx());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getZclmno())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getZclmno(), zmedTrxhdrDB.getZclmno())) {
				zmedTrxhdrDB.setZclmno(requestPayload.getZclmno());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getZdivision())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getZdivision(), zmedTrxhdrDB.getZdivision())) {
				zmedTrxhdrDB.setZdivision(requestPayload.getZdivision());
				requestPayload.setDataChanged(true);
			}
		}
				
		if (StringUtils.isNotBlank(requestPayload.getPaymethod())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPaymethod(), zmedTrxhdrDB.getPaymethod())) {
				zmedTrxhdrDB.setPaymethod(requestPayload.getPaymethod());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getEname())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getEname(), zmedTrxhdrDB.getEname())) {
				zmedTrxhdrDB.setEname(requestPayload.getEname());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getClmst())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getClmst(), zmedTrxhdrDB.getClmst())) {
				zmedTrxhdrDB.setClmst(requestPayload.getClmst());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getClmby())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getClmby(), zmedTrxhdrDB.getClmby())) {
				zmedTrxhdrDB.setClmby(requestPayload.getClmby());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getAprby())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAprby(), zmedTrxhdrDB.getAprby())) {
				zmedTrxhdrDB.setAprby(requestPayload.getAprby());
				requestPayload.setDataChanged(true);
			}
		}
				
		if (StringUtils.isNotBlank(requestPayload.getPayby())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPayby(), zmedTrxhdrDB.getPayby())) {
				zmedTrxhdrDB.setPayby(requestPayload.getPayby());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getRevby())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getRevby(), zmedTrxhdrDB.getRevby())) {
				zmedTrxhdrDB.setRevby(requestPayload.getRevby());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getBelnr())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBelnr(), zmedTrxhdrDB.getBelnr())) {
				zmedTrxhdrDB.setBelnr(requestPayload.getBelnr());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getJamin())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getJamin(), zmedTrxhdrDB.getJamin())) {
				zmedTrxhdrDB.setJamin(requestPayload.getJamin());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getNotex())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getNotex(), zmedTrxhdrDB.getNotex())) {
				zmedTrxhdrDB.setNotex(requestPayload.getNotex());
				requestPayload.setDataChanged(true);
			}
		}
				
		if (StringUtils.isNotBlank(requestPayload.getPagu1())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPagu1(), zmedTrxhdrDB.getPagu1().toString())) {
				zmedTrxhdrDB.setPagu1(Double.parseDouble(requestPayload.getPagu1()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getPagu2())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPagu2(), zmedTrxhdrDB.getPagu2().toString())) {
				zmedTrxhdrDB.setPagu2(Double.parseDouble(requestPayload.getPagu2()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getPagudent())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPagudent(), zmedTrxhdrDB.getPagudent().toString())) {
				zmedTrxhdrDB.setPagudent(Double.parseDouble(requestPayload.getPagudent()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getPagu2byemp())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPagu2byemp(), zmedTrxhdrDB.getPagu2byemp().toString())) {
				zmedTrxhdrDB.setPagu2byemp(Double.parseDouble(requestPayload.getPagu2byemp()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getTotamt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getTotamt(), zmedTrxhdrDB.getTotamt().toString())) {
				zmedTrxhdrDB.setTotamt(Double.parseDouble(requestPayload.getTotamt()));
				requestPayload.setDataChanged(true);
			}
		}

		
		
		
		
		
		if (StringUtils.isNotBlank(requestPayload.getPasien())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPasien(), zmedTrxhdrDB.getPasien())) {
				zmedTrxhdrDB.setPasien(requestPayload.getPasien());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getClmamt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getClmamt(), zmedTrxhdrDB.getClmamt().toString())) {
				zmedTrxhdrDB.setClmamt(Double.parseDouble(requestPayload.getClmamt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getRejamt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getRejamt(), zmedTrxhdrDB.getRejamt().toString())) {
				zmedTrxhdrDB.setRejamt(Double.parseDouble(requestPayload.getRejamt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getAppamt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAppamt(), zmedTrxhdrDB.getAppamt().toString())) {
				zmedTrxhdrDB.setAppamt(Double.parseDouble(requestPayload.getAppamt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getKodemed())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getKodemed(), zmedTrxhdrDB.getKodemed())) {
				zmedTrxhdrDB.setKodemed(requestPayload.getKodemed());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedTrxhdrDB.getBukrs() != null ? zmedTrxhdrDB.getBukrs().getBukrs() : StringUtils.EMPTY)) {
				try {
					Optional<T001> newT001 = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));
					if (newT001.isPresent()) {
						zmedTrxhdrDB.setBukrs(newT001.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					//T535N not found... cancel the update process
				}
			}
		}
		
		
		
		/*if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedTrxhdrDB.getBukrs())) {
				zmedTrxhdrDB.setBukrs(requestPayload.getBukrs());
				requestPayload.setDataChanged(true);
			}
		}*/

			
		
		if (StringUtils.isNotBlank(requestPayload.getPenyakit())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPenyakit(), zmedTrxhdrDB.getPenyakit())) {
				zmedTrxhdrDB.setPenyakit(requestPayload.getPenyakit());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getMedtydesc())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getMedtydesc(), zmedTrxhdrDB.getMedtydesc())) {
				zmedTrxhdrDB.setMedtydesc(requestPayload.getMedtydesc());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getBilldt())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBilldt()), (zmedTrxhdrDB.getBilldt())) ){
				zmedTrxhdrDB.setBilldt(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBilldt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getTglmasuk())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getTglmasuk()), (zmedTrxhdrDB.getTglmasuk())) ){
				zmedTrxhdrDB.setTglmasuk(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getTglmasuk()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getTglkeluar())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getTglkeluar()), (zmedTrxhdrDB.getTglkeluar())) ){
				zmedTrxhdrDB.setTglkeluar(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getTglkeluar()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getClmdt())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getClmdt()), (zmedTrxhdrDB.getClmdt())) ){
				zmedTrxhdrDB.setClmdt(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getClmdt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getAprdt())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getAprdt()), (zmedTrxhdrDB.getAprdt())) ){
				zmedTrxhdrDB.setAprdt(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getAprdt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getPaydt())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getPaydt()), (zmedTrxhdrDB.getPaydt())) ){
				zmedTrxhdrDB.setPaydt(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getPaydt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getRevdt())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getRevdt()), (zmedTrxhdrDB.getRevdt())) ){
				zmedTrxhdrDB.setRevdt(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getRevdt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getProcess())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getProcess(), zmedTrxhdrDB.getProcess())) {
				zmedTrxhdrDB.setProcess(requestPayload.getProcess());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getReason())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getReason(), zmedTrxhdrDB.getReason())) {
				zmedTrxhdrDB.setReason(requestPayload.getReason());
				requestPayload.setDataChanged(true);
			}
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedTrxhdrDB, requestPayload);	
				
	}
	
	JSONObject json = new JSONObject(requestPayload);
	objectContainer.setEventData(json.toString());
	
	return objectContainer;
}
}