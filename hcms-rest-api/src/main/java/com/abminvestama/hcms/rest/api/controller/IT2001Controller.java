package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.UserRole;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.IT2001;
import com.abminvestama.hcms.core.model.entity.IT2006;
import com.abminvestama.hcms.core.model.entity.Phdat;
import com.abminvestama.hcms.core.model.entity.PositionRelation;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT2001CommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT2006CommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT2001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT2006QueryService;
import com.abminvestama.hcms.core.service.api.business.query.PhdatQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.helper.IT2006Mapper;
import com.abminvestama.hcms.core.service.helper.TimeAbsenceAttendanceSoapObject;
import com.abminvestama.hcms.core.service.util.MailService;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT2001RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.IT2001ResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.IT2001RequestBuilderUtil;

/**
 * 
 * @since 1.0.0
 * @version 1.0.6
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Add calculateDateHours to calculate Calendar Days, Absence Days, Absence Hours</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add it2006CommandService and it2006QueryService to save/update IT2006 after create/update IT2001</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: findOneByCompositeKey</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add parameters to findByCriteria: infty, subty, begin, end</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add methods: findByCriteria</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add edit method and Post to MuleSoft</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/absences")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class IT2001Controller extends AbstractResource {

	private IT2001CommandService it2001CommandService;
	private IT2001QueryService it2001QueryService;
	private IT2006CommandService it2006CommandService;
	private IT2006QueryService it2006QueryService;	

	
	private HCMSEntityQueryService hcmsEntityQueryService;
	private EventLogCommandService eventLogCommandService;
	
	private IT2001RequestBuilderUtil it2001RequestBuilderUtil;
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	private MailService mailService;
	private CommonServiceFactory commonServiceFactory;
	private MuleConsumerService muleConsumerService;
	private PhdatQueryService phdatQueryService; 
	

	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	@Autowired
	void setIT2001CommandService(IT2001CommandService it2001CommandService) {
		this.it2001CommandService = it2001CommandService;
	}
	
	@Autowired
	void setIT2001QueryService(IT2001QueryService it2001QueryService) {
		this.it2001QueryService = it2001QueryService;
	}
	
	@Autowired
	void setIT2006CommandService(IT2006CommandService it2006CommandService) {
		this.it2006CommandService = it2006CommandService;
	}
	
	@Autowired
	void setIT2006QueryService(IT2006QueryService it2006QueryService) {
		this.it2006QueryService = it2006QueryService;
	}
	
	@Autowired
	void setIT2001RequestBuilderUtil(IT2001RequestBuilderUtil it2001RequestBuilderUtil) {
		this.it2001RequestBuilderUtil = it2001RequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}
	
	@Autowired
	void setPhdatQueryService(PhdatQueryService phdatQueryService) {
		this.phdatQueryService = phdatQueryService;
	}

	@Autowired
	void setMailService(MailService mailService) {
		this.mailService = mailService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/get_one",
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({UserRole.AccessRole.USER_GROUP})
	public APIResponseWrapper<IT2001ResponseWrapper> findOneByCompositeKey(
			@RequestParam(value = "ssn", required = true) String ssn,
			@RequestParam(value = "infty", required = true) String infty,
			@RequestParam(value = "subty", required = true) String subty,
			@RequestParam(value = "endda", required = true) String endda,
			@RequestParam(value = "begda", required = true) String begda) throws Exception {
		IT2001ResponseWrapper it2001Response = null;
		APIResponseWrapper<IT2001ResponseWrapper> response = new APIResponseWrapper<>(it2001Response);
		try {
			Function<IT2001, IT2001ResponseWrapper> it2001ResponseWrapperFunction
				= (IT2001 it2001) -> new IT2001ResponseWrapper(it2001);
				
			Long pernr = Long.valueOf(ssn);
			
			Optional<IT2001> it2001Object = it2001QueryService.findOneByCompositeKeyWithInfotype(pernr, infty, subty,
					CommonDateFunction.convertDateRequestParameterIntoDate(endda), CommonDateFunction.convertDateRequestParameterIntoDate(begda));
			if (it2001Object.isPresent()) {
				it2001Response = it2001ResponseWrapperFunction.apply(it2001Object.get());
				response = new APIResponseWrapper<>(it2001Response);
			} else {
				response.setMessage("No Data Found");
			}
		} catch (Exception e) {
			throw e;
		}
		response.add(linkTo(methodOn(IT2001Controller.class).findOneByCompositeKey(ssn, infty, subty, endda, begda)).withSelfRel());
		
		return response;
	}

	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_criteria", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<IT2001ResponseWrapper>> findByCriteria(
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "process", required = false) String process,
			@RequestParam(value = "infty", required = false) String infty,
			@RequestParam(value = "subty", required = false) String subty,
			@RequestParam(value = "begin", required = false) String periodBegin,
			@RequestParam(value = "end", required = false) String periodEnd,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {

		Page<IT2001ResponseWrapper> responsePage = new PageImpl<IT2001ResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<IT2001ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<IT2001> resultPage = null;
		String bukrs = null;
		Long pernr = null;
		String[] processes = StringUtils.isEmpty(process) ? null : process.split(",");
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		Optional<Role> superiorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_SUPERIOR"));
		
		try {
			
			if (StringUtils.isNotEmpty(ssn)) {
				pernr = Long.valueOf(ssn);
				
				if (!currentUser.get().getEmployee().getPernr().equals(pernr) && 
						!( currentUser.get().getRoles().contains(adminRole.get()) || currentUser.get().getRoles().contains(superiorRole.get()) ) ) {
					throw new AuthorizationException(
							"Insufficient Privileges. You can't fetch data of other Employee.");
				}
				
				// query by superior
				if (!currentUser.get().getEmployee().getPernr().equals(pernr) && currentUser.get().getRoles().contains(superiorRole.get()) ) {
					boolean authorized = false;
					
					if (currentUser.get().getRoles().contains(adminRole.get())) {
						authorized = true;
					} else {
						List<PositionRelation> listPositionRelation = commonServiceFactory.getPositionRelationQueryService().findByToPositionId(
								currentUser.get().getEmployee().getPosition().getId()).stream().collect(Collectors.toList());
						Optional<User> submitUser = userQueryService.findByPernr(pernr);
						if (submitUser.isPresent()) {
							for (PositionRelation positionRelation : listPositionRelation) {
								if (positionRelation.getFromPosition().getId().equals(submitUser.get().getEmployee().getPosition().getId())) {
									authorized = true;
									break;
								}
							}
						}
					}

					if (!authorized) {
						throw new AuthorizationException(
								"Insufficient Privileges. You can't fetch data of other Employee.");
					}
				}
				
				/*
				resultPage = (processes != null && processes.length > 1) ?
						it2001QueryService.findByPernrAndProcessesWithPaging(pernr.longValue(), processes, pageNumber) :
							it2001QueryService.findByPernrAndProcessWithPaging(pernr.longValue(), process, pageNumber);
				*/

				resultPage = (processes != null && processes.length > 1) ?
						it2001QueryService.findByPernrAndProcessesWithPaging(pernr.longValue(), processes, pageNumber, infty, subty, periodBegin, periodEnd) :
							it2001QueryService.findByPernrAndProcessWithPaging(pernr.longValue(), process, pageNumber, infty, subty, periodBegin, periodEnd);
				
			} else {
				// no specified ssn, MUST BE ADMIN / SUPERIOR 
				if (!currentUser.get().getRoles().contains(adminRole.get()) && !currentUser.get().getRoles().contains(superiorRole.get())) {
					throw new AuthorizationException(
							"Insufficient Privileges. Please login as 'ADMIN' or 'SUPERIOR'!");
				}
				/*
				// If NOT ADMIN (ROLE SUPERIOR ONLY) can only fetch Expenses which have been released by user (process=1) or has been approved by all superior (process=4)
				if (!currentUser.get().getRoles().contains(adminRole.get()) && process != null && !process.equals("1,4")) {
					throw new AuthorizationException(
							"Insufficient Privileges. Please login as 'ADMIN'!");
				}
				*/
				String superiorPosition = null;
				// If NOT ADMIN (ROLE SUPERIOR ONLY) set superiorPosition parameter as additional filter
				//if (!currentUser.get().getRoles().contains(adminRole.get()) && process != null && process.equals("1,4")) {
				if (!currentUser.get().getRoles().contains(adminRole.get())) {
					superiorPosition = currentUser.get().getEmployee().getPosition().getId();
				}
				bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
				if (superiorPosition == null) {
					resultPage = (processes != null && processes.length > 1) ?
							it2001QueryService.findByBukrsAndProcessesWithPaging(bukrs, processes, pageNumber) :
								it2001QueryService.findByBukrsAndProcessWithPaging(bukrs, process, superiorPosition, pageNumber);
				} else {
					resultPage = it2001QueryService.findByBukrsAndProcessWithPaging(bukrs, process, superiorPosition, pageNumber);
				}
			}
			
			//updateDocumentStatus(resultPage.getContent());
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			
			if (resultPage.hasContent()) {

				List<IT2001ResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(it2001 -> {
					records.add(new IT2001ResponseWrapper(it2001));
				});
				responsePage = new PageImpl<IT2001ResponseWrapper>(records, it2001QueryService.createPageRequest(pageNumber),
						resultPage.getTotalElements());
				response.setData(responsePage);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT2001Controller.class).findByCriteria(ssn, process, infty, subty, periodBegin, periodEnd, pageNumber)).withSelfRel());
		
		return response;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT2001ResponseWrapper>> findOneByPernr(@PathVariable String pernr) throws Exception {
		ArrayData<IT2001ResponseWrapper> bunchOfIT2001 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT2001ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT2001);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT2001> listOfIT2001 = it2001QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			
			if (listOfIT2001.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<IT2001ResponseWrapper> records = new ArrayList<>();
				for (IT2001 it2001 : listOfIT2001) {
					records.add(new IT2001ResponseWrapper(it2001));
				}
				bunchOfIT2001.setItems(records.toArray(new IT2001ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT2001Controller.class).findOneByPernr(pernr)).withSelfRel());
		return response;
	}		
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated IT2001RequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			IT2001ResponseWrapper responseWrapper = new IT2001ResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<HCMSEntity> it2001HCMSEntity = hcmsEntityQueryService.findByEntityName(IT2001.class.getSimpleName());
					if (!it2001HCMSEntity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + IT2001.class.getSimpleName() + "' !!!");
					}
					
					Optional<IT2001> it2001 = it2001QueryService.findOneByCompositeKeyWithInfotype(request.getPernr(),
							request.getInfotype(), request.getSubty(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
					if (!it2001.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
					
					boolean workflowProcess = !it2001.get().getProcess().equals(request.getProcess());
					//String currentProcess = it2001.get().getProcess();
				
					RequestObjectComparatorContainer<IT2001, IT2001RequestWrapper, String> updatedContainer 
						= it2001RequestBuilderUtil.compareAndReturnUpdatedData(request, it2001.get());
					
					String currentProcess = updatedContainer.getEntity().get().getProcess();

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
					
					if (updatedContainer.getEntity().isPresent()) {
						calculateDateHours(updatedContainer.getEntity().get());
					}
					
					// if process = 1 (RELEASED by user)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							updatedContainer.getEntity().get().getProcess().equals("1")) {
						updatedContainer.getEntity().get().setCreatedBy(currentUser.get());
						updatedContainer.getEntity().get().setCreatedAt(new Date());
						it2001 = it2001CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						if (!it2001.isPresent()) {
							throw new CannotPersistException("Cannot update IT2001/IT2002 (Leave) data. Please check your data!");
						}
					}

					// if process = 2 or 3 (APPROVED by Superior1 or Superior2)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							(updatedContainer.getEntity().get().getProcess().equals("2") || updatedContainer.getEntity().get().getProcess().equals("3")) ) {
						Optional<Role> superiorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_SUPERIOR"));
						if (!currentUser.get().getRoles().contains(superiorRole.get())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Please login as 'SUPERIOR'!");
						}
						Optional<User> submitUser = Optional.ofNullable(updatedContainer.getEntity().get().getCreatedBy());
						if (submitUser.isPresent() && submitUser.get().getEmployee().getPernr().intValue()
								!= updatedContainer.getEntity().get().getPernr().intValue()) {
							// transaction was created by Superior or HR Admin, find User by pernr
							submitUser = userQueryService.findByPernr(updatedContainer.getEntity().get().getPernr());
						}
						List<PositionRelation> listPositionRelation = 
								commonServiceFactory.getPositionRelationQueryService().findByFromPositionAndToPosition(
										submitUser.isPresent() ? submitUser.get().getEmployee().getPosition().getId() : StringUtils.EMPTY,
										currentUser.get().getEmployee().getPosition().getId()).
										stream().collect(Collectors.toList());
						if (listPositionRelation.isEmpty()) {
							throw new AuthorizationException(
									"Insufficient Privileges. You can't approve request of other employee whose not your sub-ordinate!");
						}
						
						listPositionRelation = commonServiceFactory.getPositionRelationQueryService().findByFromPositionid(
								submitUser.isPresent() ? submitUser.get().getEmployee().getPosition().getId() : StringUtils.EMPTY)
								.stream().collect(Collectors.toList());
						int approveLevel = 1;
						if (updatedContainer.getEntity().get().getSuperiorApprovedBy() != null && listPositionRelation.size() > approveLevel) {
							if (updatedContainer.getEntity().get().getSuperiorApprovedBy().equals(currentUser.get()))
								throw new AuthorizationException("You can't approve the same request again!");
							approveLevel++;
						}
						if (updatedContainer.getEntity().get().getSuperior2ApprovedBy() != null && listPositionRelation.size() > approveLevel) {
							if (updatedContainer.getEntity().get().getSuperior2ApprovedBy().equals(currentUser.get()))
								throw new AuthorizationException("You can't approve the same request again!");
							approveLevel++;
						}
						if (updatedContainer.getEntity().get().getSuperior3ApprovedBy() != null && listPositionRelation.size() > approveLevel) {
							if (updatedContainer.getEntity().get().getSuperior3ApprovedBy().equals(currentUser.get()))
								throw new AuthorizationException("You can't approve the same request again!");
							approveLevel++;
						}
						if (updatedContainer.getEntity().get().getApproved4By() != null && listPositionRelation.size() > approveLevel) {
							if (updatedContainer.getEntity().get().getApproved4By().equals(currentUser.get()))
								throw new AuthorizationException("You can't approve the same request again!");
							approveLevel++;
						}
						/*
						if (updatedContainer.getEntity().get().getProcess().equals("3") && updatedContainer.getEntity().get().getSuperiorApprovedBy() != null &&
								updatedContainer.getEntity().get().getSuperiorApprovedBy().equals(currentUser.get()) ) {
							throw new AuthorizationException("You can't approve the same request again!");
						}
						*/
						switch (approveLevel) {
						    case 1:
								updatedContainer.getEntity().get().setSuperiorApprovedBy(currentUser.get());
								updatedContainer.getEntity().get().setSuperiorApprovedAt(new Date());
								break;
						    case 2:
								updatedContainer.getEntity().get().setSuperior2ApprovedBy(currentUser.get());
								updatedContainer.getEntity().get().setSuperior2ApprovedAt(new Date());
								break;
						    case 3:
								updatedContainer.getEntity().get().setSuperior3ApprovedBy(currentUser.get());
								updatedContainer.getEntity().get().setSuperior3ApprovedAt(new Date());
								break;
						    case 4:
								updatedContainer.getEntity().get().setApproved4By(currentUser.get());
								updatedContainer.getEntity().get().setApproved4At(new Date());
								break;
						    case 5:
								updatedContainer.getEntity().get().setApproved5By(currentUser.get());
								updatedContainer.getEntity().get().setApproved5At(new Date());
								break;
						    default:
						}
						
						if (listPositionRelation.size() > approveLevel) {
							// workflow still in progress, set back process=1
							updatedContainer.getEntity().get().setProcess("1");
							it2001 = it2001CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
							if (!it2001.isPresent()) {
								throw new CannotPersistException("Cannot update IT2001/IT2002 (Leave) data. Please check your data!");
							}
						} else {
							// workflow is done, set process=4
							updatedContainer.getEntity().get().setProcess("4");
							currentProcess = updatedContainer.getEntity().get().getProcess();
						}
					}
				
					/// REQUIRED ???
					responseWrapper = new IT2001ResponseWrapper(it2001.get());
					response = new APIResponseWrapper<>(responseWrapper);
					/// REQUIRED ???
					
					// if process = 5 (CANCELED by ADMIN)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							updatedContainer.getEntity().get().getProcess().equals("5")) {
						
						Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
						if (!currentUser.get().getRoles().contains(adminRole.get())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Please login as 'ADMIN'!");
						}
						
						TimeAbsenceAttendanceSoapObject timeAbsenceAttendanceSoapObject = new TimeAbsenceAttendanceSoapObject(it2001.get());
												
						List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.TIME_ABSATD_CREATE.service(),
								BAPIFunction.TIME_ABSATD_DELETE_URI.service(), timeAbsenceAttendanceSoapObject);
						HashMap<String, String> resultMap = (HashMap<String, String>) list.get(0);
						
						for (int i = 0; i < list.size(); i++) {
							// synchronize IT2006 fields with the fetched fields from SAP
							if (i > 0 && list.get(i).get("pernr") != null) {
								IT2006 it2006Entity = (new IT2006Mapper(list.get(i), commonServiceFactory)).getIT2006();
								IT2006 it2006DB = it2006Entity;
								Optional<IT2006> it2006 = it2006QueryService.findOneByCompositeKey(it2006Entity.getId().getPernr(),
										it2006Entity.getId().getSubty(), it2006Entity.getId().getEndda(), it2006Entity.getId().getBegda());
								if (it2006.isPresent()) {
									it2006DB = it2006.get();
									BeanUtils.copyProperties(it2006Entity, it2006DB, "id");
								}
								it2006CommandService.save(it2006DB, currentUser.get());
							}
						}
						
						if (resultMap.get("errorCode") == StringUtils.EMPTY && resultMap.get("errorMessage") == StringUtils.EMPTY) {
							String message = resultMap.get("message");
							it2001 = it2001CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
							if (!it2001.isPresent()) {
								throw new CannotPersistException("Cannot update IT2001/IT2002 (Leave) data. Please check your data!");
							}
							responseWrapper = new IT2001ResponseWrapper(updatedContainer.getEntity().get());
							response = new APIResponseWrapper<>(responseWrapper);
							response.setMessage(message);
						} else {
							currentProcess = "6";
							responseWrapper = new IT2001ResponseWrapper(it2001.get());
							response = new APIResponseWrapper<>(responseWrapper);
							response.setMessage("Error: " + resultMap.get("errorMessage") +".");
						}
					}
					
					// if process = 0 (REJECTED / Request for edit)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							updatedContainer.getEntity().get().getProcess().equals("0")) {
						it2001 = it2001CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						if (!it2001.isPresent()) {
							throw new CannotPersistException("Cannot update IT2001/IT2002 (Leave) data. Please check your data!");
						}
					}
				
					/*
					if (updatedContainer.getEntity().isPresent()) {
						updatedContainer.getEntity().get().setSeqnr(it2001.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
						it2001 = it2001CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}
					
				
					if (!it2001.isPresent()) {
						throw new CannotPersistException("Cannot update IT2001 (Absences) data. Please check your data!");
					}
					*/

					// if process = 4 (APPROVED by Admin / Last Superior)
					if (updatedContainer.getEntity().isPresent() && updatedContainer.getEntity().get().getProcess() != null &&
							updatedContainer.getEntity().get().getProcess().equals("4")) {
						/*
						Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
						if (!currentUser.get().getRoles().contains(adminRole.get())) {
							throw new AuthorizationException(
									"Insufficient Privileges. Please login as 'ADMIN'!");
						}
						*/
						/* Post to MuleSoft */
						
						IT2001 it2001Entity = it2001.get();
						
						// Start of split transaction on Public Holidays ****************************
						
						/*
						List<IT2001> splitIT2001 = new ArrayList<>();
						List<Phdat> listHoliday = phdatQueryService.findByDateAndType(it2001Entity.getId().getBegda(),
								it2001Entity.getId().getEndda(), "").stream().collect(Collectors.toList());
						if (!listHoliday.isEmpty()) {
							Date startDate = it2001Entity.getId().getBegda();
							Date endDate = it2001Entity.getId().getEndda();
							for (Phdat phdat : listHoliday) {
								Calendar cal = Calendar.getInstance();
								cal.setTime(phdat.getId().getErdat());
								int day = cal.get(Calendar.DAY_OF_WEEK);
								// increase offDay, only for Public Holiday which is not Weekend
								//if (day > Calendar.SUNDAY && day < Calendar.SATURDAY) {
									cal.add(Calendar.DATE, -1);
								//}
								endDate = cal.getTime();
								IT2001 newIT2001 = new IT2001(new ITCompositeKeys(it2001Entity.getId().getPernr(),
										it2001Entity.getId().getInfty(), it2001Entity.getId().getSubty(), endDate, startDate));
								splitIT2001.add(newIT2001);
								cal.setTime(phdat.getId().getErdat());
								cal.add(Calendar.DATE, 1);
								startDate = cal.getTime();
							}
							endDate = it2001Entity.getId().getEndda();
							if (!endDate.before(startDate)) {
								IT2001 newIT2001 = new IT2001(new ITCompositeKeys(it2001Entity.getId().getPernr(),
										it2001Entity.getId().getInfty(), it2001Entity.getId().getSubty(), endDate, startDate));
								splitIT2001.add(newIT2001);
							}
							for (IT2001 newIT2001 : splitIT2001) {
								if (!newIT2001.getId().getEndda().before(newIT2001.getId().getBegda())) {
									BeanUtils.copyProperties(it2001Entity, newIT2001, "id");
									int calDays = 0;
									int offDay = 0;
									Calendar cal = Calendar.getInstance();
									cal.setTime(newIT2001.getId().getBegda());
									cal.add(Calendar.DATE, -1);
									while (cal.getTime().before(newIT2001.getId().getEndda())) {
										calDays++;
										cal.add(Calendar.DATE, 1);
										int day = cal.get(Calendar.DAY_OF_WEEK);
										// increase offDay, for Weekend
										if (day < Calendar.MONDAY || day > Calendar.FRIDAY) {
											offDay++;
										}
									}
									newIT2001.setKaltg(Double.valueOf(calDays));
									newIT2001.setAbwtg(Double.valueOf(calDays-offDay));
									if ((calDays-offDay) > 1) {
										newIT2001.setStdaz(newIT2001.getAbwtg() * 8);
									}
									Optional<IT2001> saveIT2001 = it2001CommandService.save(newIT2001, currentUser.isPresent() ? currentUser.get() : null);
									if (!saveIT2001.isPresent()) {
										throw new CannotPersistException("Cannot update IT2001/IT2002 (Absences/Attendances) data. Please check your data!");
									}
								}
							}
						}
						*/
						
						// End of split transaction on Public Holidays ****************************
						
						TimeAbsenceAttendanceSoapObject timeAbsenceAttendanceSoapObject = new TimeAbsenceAttendanceSoapObject(it2001Entity);
						
						//HashMap<String, String> resultMap = (HashMap<String, String>) muleConsumerService.
								//postToSAPSync(BAPIFunction.TIME_ABSATD_CREATE.service(), BAPIFunction.TIME_ABSATD_CREATE_URI.service(), timeAbsenceAttendanceSoapObject);
												
						List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.TIME_ABSATD_CREATE.service(),
								BAPIFunction.TIME_ABSATD_CREATE_URI.service(), timeAbsenceAttendanceSoapObject);
						HashMap<String, String> resultMap = (HashMap<String, String>) list.get(0);
						
						for (int i = 0; i < list.size(); i++) {
							// synchronize IT2006 fields with the fetched fields from SAP
							if (i > 0 && list.get(i).get("pernr") != null) {
								IT2006 it2006Entity = (new IT2006Mapper(list.get(i), commonServiceFactory)).getIT2006();
								IT2006 it2006DB = it2006Entity;
								Optional<IT2006> it2006 = it2006QueryService.findOneByCompositeKey(it2006Entity.getId().getPernr(),
										it2006Entity.getId().getSubty(), it2006Entity.getId().getEndda(), it2006Entity.getId().getBegda());
								if (it2006.isPresent()) {
									it2006DB = it2006.get();
									BeanUtils.copyProperties(it2006Entity, it2006DB, "id");
								}
								it2006CommandService.save(it2006DB, currentUser.get());
							}
						}
						

						//muleConsumerService.postToSAPAsync(BAPIFunction.TIME_ABSATD_CREATE.service(), BAPIFunction.TIME_ABSATD_CREATE_URI.service(),
								//timeAbsenceAttendanceSoapObject, it2001CommandService, it2001Entity);
						
						if (resultMap.get("errorCode") == StringUtils.EMPTY && resultMap.get("errorMessage") == StringUtils.EMPTY) {
							
							String message = resultMap.get("message");
							it2001 = it2001CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
							if (!it2001.isPresent()) {
								throw new CannotPersistException("Cannot update IT2001/IT2002 (Leave) data. Please check your data!");
							}
							responseWrapper = new IT2001ResponseWrapper(updatedContainer.getEntity().get());
							response = new APIResponseWrapper<>(responseWrapper);
							response.setMessage(message);
						} else {
							it2001Entity.setProcess("1");
							responseWrapper = new IT2001ResponseWrapper(it2001Entity);
							response = new APIResponseWrapper<>(responseWrapper);
							response.setMessage("Error: " + resultMap.get("errorMessage") +".");
							System.out.println("MULESOFT FAILED on Leave/Attendance Update - ".concat(response.getMessage()));
						}
						
						/* ************************** */
						
					}
					
					StringBuilder key = new StringBuilder("pernr=" + it2001.get().getId().getPernr());
					key.append(",infty=" + it2001.get().getId().getInfty());
					key.append(",subty=" + it2001.get().getId().getSubty());
					key.append(",endda=" + CommonDateFunction.convertDateToStringYMD(it2001.get().getId().getEndda()));
					key.append(",begda=" + CommonDateFunction.convertDateToStringYMD(it2001.get().getId().getBegda()));
					EventLog eventLog = new EventLog(it2001HCMSEntity.get(), key.toString(), OperationType.UPDATE, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					eventLog.setStatus("process=".concat(updatedContainer.getEntity().get().getProcess()));
					// workflow update, don't send email notification
					if (workflowProcess) {
						eventLog.setMailNotification(false);
					}
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					
					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}
					
					Optional<User> submitUser = userQueryService.findByPernr(it2001.get().getPernr());
					if (submitUser.isPresent()) {
						
						sendNotification(submitUser.get(), currentProcess, it2001.get(), response, eventLog, workflowProcess);
						
						/*
						User user = submitUser.get();
						String processed = "RELEASED. Your Superior and Admin will review your submitted data prior to approval.";
						if (currentProcess.equals("2")) {
							processed = "APPROVED by your Superior and waiting for next approval.";
						} else if (currentProcess.equals("4")) {
							processed = "APPROVED by your Superior and PUBLISHED.";
							if ("1".equals(it2001.get().getProcess())) {
								processed = "FAILED to publish with following message:<br>".concat
										(StringUtils.defaultString(response.getMessage(), "No Message"));
							}
						} else if (currentProcess.equals("0")) {
							processed = workflowProcess ? "Requested for edit by Your Superior due to following reason :<br>".concat(it2001.get().getReason())
									: "Updated, waiting for your released and submitted required data to HR Admin or Your Superior.";
						} else if (currentProcess.equals("5")) {
							processed = "Cancelled by HR Admin due to following reason :<br>".concat(it2001.get().getReason());
						} else if (currentProcess.equals("6")) {
							processed = "FAILED to cancel by HR Admin with following message:<br>".concat
									(StringUtils.defaultString(response.getMessage(), "No Message"));
						}

						Map<String, String> composeMap = mailService.composeEmail(eventLog, user);
						if (StringUtils.isNotEmpty(composeMap.get("subject"))) {
							String mailBody = composeMap.get("body").replace(mailService.getMailMasterDataUpdate(), "has been ".concat(processed)).
									replace("Your data update request", "Your request for ".
											concat(it2001.get().getId().getInfty().equals("2001") ? "Absence " : "Attendance ").
								    		concat(StringUtils.defaultString(it2001.get().getSubty().getStext(), StringUtils.EMPTY)));
							mailService.sendMail(user.getEmail(), composeMap.get("subject"), mailBody);

							// notification email to all Superior
					        if (currentProcess.equals("1")) {
							    List<User> superiorUsers = new ArrayList<User>();
						    	mailBody = mailBody.replace(processed, 
						    			"released, waiting for your review and approval.").
						    			replace("Your request for ", "User request for ");
						    	
						    	// fetch all user's Superior
						    	List<PositionRelation> employeePositionRelations = commonServiceFactory.getPositionRelationQueryService()
						    			.findByFromPositionid(user.getEmployee().getPosition().getId())
						    			.stream().collect(Collectors.toList());									
								if (!employeePositionRelations.isEmpty()) {
									for (PositionRelation positionRelation : employeePositionRelations) {
										List<IT0001> toIT0001List = commonServiceFactory.getIT0001QueryService()
												.findByPositionId(positionRelation.getToPosition().getId())
												.stream().collect(Collectors.toList());
										Optional<User> superior = userQueryService.findByPernr(
												toIT0001List.isEmpty() ? 0 : toIT0001List.get(0).getPernr());
										if (superior.isPresent()) {
											superiorUsers.add(superior.get());
										}
									}
								}
						    	
						    	for (User superiorUser : superiorUsers) {
						    		String body = mailBody.replace("Dear <b>".concat(user.getEmployee().getSname()),
						    				"Dear <b>".concat(superiorUser.getEmployee().getSname()));
						    		mailService.sendMail(superiorUser.getEmail(), composeMap.get("subject"), body);
						    	}
						    // notification email to all HR Admin
					        } else if (currentProcess.equals("2") || currentProcess.equals("4")) {
							    List<User> adminUsers = new ArrayList<User>();
							    Optional<Collection<User>> users = userQueryService.fetchAdminByCompany(
							    		user.getEmployee().getT500p().getBukrs().getBukrs());
							    if (users.isPresent()) {
							    	adminUsers = users.get().stream().collect(Collectors.toList());
							    	mailBody = mailBody.replace(processed, currentProcess.equals("2") ?
							    			"approved by ".concat(currentUser.get().getEmployee().getSname()).concat(" and waiting for next approval.") :
							    				"1".equals(it2001.get().getProcess()) ? "approved but ".concat(processed) : 
							    			    	"approved by ".concat(currentUser.get().getEmployee().getSname()).concat(" and PUBLISHED.")).
							    			replace("Dear <b>".concat(user.getEmployee().getSname()), "Dear <b>HR Admin").
							    			replace("Your request for ", "User request for ");
							    	for (User adminUser : adminUsers) {
							    		mailService.sendMail(adminUser.getEmail(), composeMap.get("subject"), mailBody);
							    	}
							    }
							}					        
						}
						
				        if ((currentProcess.equals("4") && "1".equals(it2001.get().getProcess())) ||
				        		currentProcess.equals("6")) {
							throw new DataViolationException(processed.replace("<br>","\n"));
				        }
						*/
					}
				
				} catch (DataViolationException e) { 
					throw e;
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(IT2001Controller.class).edit(request, result)).withSelfRel());
		return response;
	}	
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated IT2001RequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> it2001Entity = hcmsEntityQueryService.findByEntityName(IT2001.class.getSimpleName());
			if (!it2001Entity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + IT2001.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<IT2001, IT2001RequestWrapper, String> newObjectContainer 
					= it2001RequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				
				Optional<IT2001> savedEntity = newObjectContainer.getEntity();
				
				if (!savedEntity.isPresent() || savedEntity.get().getId() == null) {
					throw new DataViolationException(savedEntity.isPresent() ?
							savedEntity.get().getReason() : "Cannot Create IT2001/IT2002 (Leave) data. Please check your data!");
				}
				
				calculateDateHours(savedEntity.get());
				
				// process = 1 (by user him-self or by Superior)
				
				// if process = 1 (RELEASED by user)
				if (savedEntity.isPresent() && savedEntity.get().getProcess() != null &&
						savedEntity.get().getProcess().equals("1")) {
					savedEntity.get().setCreatedBy(currentUser.get());
					savedEntity.get().setCreatedAt(new Date());
				}
				
				Optional<User> submitUser = currentUser;
				if (submitUser.isPresent() && submitUser.get().getEmployee().getPernr().intValue()
						!= savedEntity.get().getId().getPernr().intValue()) {
					// transaction was created by Superior or HR Admin, find User by pernr
					submitUser = userQueryService.findByPernr(savedEntity.get().getId().getPernr());
					savedEntity.get().setProcess("2");
					
					// if it was created by HR Admin, set process=1 (waiting for approval by superior)
					List<PositionRelation> listPositionRelation = 
							commonServiceFactory.getPositionRelationQueryService().findByFromPositionAndToPosition(
									submitUser.isPresent() ? submitUser.get().getEmployee().getPosition().getId() : StringUtils.EMPTY,
									currentUser.get().getEmployee().getPosition().getId()).
									stream().collect(Collectors.toList());
					if (listPositionRelation.isEmpty()) {
						savedEntity.get().setProcess("1");
					}
				}
				
				savedEntity.get().setBukrs(submitUser.get().getEmployee().getT500p().getBukrs());
				String currentProcess = savedEntity.get().getProcess();

				// if process = 2 (RELEASED and APPROVED by Superior)
				if (savedEntity.isPresent() && savedEntity.get().getProcess() != null &&
						(savedEntity.get().getProcess().equals("2")) ) {
					Optional<Role> superiorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_SUPERIOR"));
					if (!currentUser.get().getRoles().contains(superiorRole.get())) {
						throw new AuthorizationException(
								"Insufficient Privileges. Please login as 'SUPERIOR'!");
					}
					
					List<PositionRelation> listPositionRelation = 
							commonServiceFactory.getPositionRelationQueryService().findByFromPositionAndToPosition(
									submitUser.isPresent() ? submitUser.get().getEmployee().getPosition().getId() : StringUtils.EMPTY,
									currentUser.get().getEmployee().getPosition().getId()).
									stream().collect(Collectors.toList());
					if (listPositionRelation.isEmpty()) {
						throw new AuthorizationException(
								"Insufficient Privileges. You can't create and approve transaction of other employee whose not your sub-ordinate!");
					}
					
					listPositionRelation = commonServiceFactory.getPositionRelationQueryService().findByFromPositionid(
							submitUser.isPresent() ? submitUser.get().getEmployee().getPosition().getId() : StringUtils.EMPTY)
							.stream().collect(Collectors.toList());
					int approveLevel = 1;
					savedEntity.get().setSuperiorApprovedBy(currentUser.get());
					savedEntity.get().setSuperiorApprovedAt(new Date());
					
					if (listPositionRelation.size() > approveLevel) {
						// workflow still in progress, set back process=1
						savedEntity.get().setProcess("1");
						savedEntity = it2001CommandService.save(savedEntity.get(), currentUser.isPresent() ? currentUser.get() : null);
						if (!savedEntity.isPresent()) {
							throw new CannotPersistException("Cannot Create IT2001/IT2002 (Leave) data. Please check your data!");
						}
					} else {
						// workflow is done, set process=4
						savedEntity.get().setProcess("4");
						currentProcess = savedEntity.get().getProcess();
					}
				}
				
				IT2001ResponseWrapper responseWrapper = null;

				// if process = 4 (APPROVED by Admin / Last Superior)
				if (savedEntity.isPresent() && savedEntity.get().getProcess() != null &&
						savedEntity.get().getProcess().equals("4")) {
					
					/* Post to MuleSoft */
					
					TimeAbsenceAttendanceSoapObject timeAbsenceAttendanceSoapObject = new TimeAbsenceAttendanceSoapObject(savedEntity.get());
											
					List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.TIME_ABSATD_CREATE.service(),
							BAPIFunction.TIME_ABSATD_CREATE_URI.service(), timeAbsenceAttendanceSoapObject);
					HashMap<String, String> resultMap = (HashMap<String, String>) list.get(0);
					
					for (int i = 0; i < list.size(); i++) {
						// synchronize IT2006 fields with the fetched fields from SAP
						if (i > 0 && list.get(i).get("pernr") != null) {
							IT2006 it2006Entity = (new IT2006Mapper(list.get(i), commonServiceFactory)).getIT2006();
							IT2006 it2006DB = it2006Entity;
							Optional<IT2006> it2006 = it2006QueryService.findOneByCompositeKey(it2006Entity.getId().getPernr(),
									it2006Entity.getId().getSubty(), it2006Entity.getId().getEndda(), it2006Entity.getId().getBegda());
							if (it2006.isPresent()) {
								it2006DB = it2006.get();
								BeanUtils.copyProperties(it2006Entity, it2006DB, "id");
							}
							it2006CommandService.save(it2006DB, currentUser.get());
						}
					}
					
					if (resultMap.get("errorCode") == StringUtils.EMPTY && resultMap.get("errorMessage") == StringUtils.EMPTY) {
						String message = resultMap.get("message");
						responseWrapper = new IT2001ResponseWrapper(savedEntity.get());
						response = new APIResponseWrapper<>(responseWrapper);
						response.setMessage(message);
					} else {
						savedEntity.get().setProcess("1");
						responseWrapper = new IT2001ResponseWrapper(savedEntity.get());
						response = new APIResponseWrapper<>(responseWrapper);
						response.setMessage("Error: " + resultMap.get("errorMessage") +".");
					}
					savedEntity.get().setBukrs(submitUser.get().getEmployee().getT500p().getBukrs());
					savedEntity = it2001CommandService.save(savedEntity.get(), currentUser.isPresent() ? currentUser.get() : null);
					if (!savedEntity.isPresent()) {
						throw new CannotPersistException("Cannot create IT2001/IT2002 (Leave) data. Please check your data!");
					}
					
					/* ************************** */
					
				}
				
				// End of --- if process = 1 (RELEASED by user)
				

				// if process = 0 or 1 (CREATED or RELEASED by User)
				if (savedEntity.isPresent() && savedEntity.get().getProcess() != null &&
						(savedEntity.get().getProcess().equals("0") || savedEntity.get().getProcess().equals("1")) ) {
					if (newObjectContainer.getEntity().isPresent()) {
						newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
						savedEntity = it2001CommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}
				}
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create IT2001/IT2002 (Leave) data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("pernr=" + savedEntity.get().getId().getPernr());
				key.append(",infty=" + savedEntity.get().getId().getInfty());
				key.append(",subty=" + savedEntity.get().getId().getSubty());
				key.append(",endda=" + CommonDateFunction.convertDateToStringYMD(savedEntity.get().getId().getEndda()));
				key.append(",begda=" + CommonDateFunction.convertDateToStringYMD(savedEntity.get().getId().getBegda()));
				EventLog eventLog = new EventLog(it2001Entity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				eventLog.setMailNotification(false);
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				
				if (submitUser.isPresent()) {
					sendNotification(submitUser.get(), currentProcess, savedEntity.get(), response, eventLog, false);
				}
				
				responseWrapper = new IT2001ResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) {
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(IT2001Controller.class).create(request, result)).withSelfRel());		
		return response;
	}
	
	private void sendNotification(User user, String currentProcess, IT2001 it2001, APIResponseWrapper<?> response,
			EventLog eventLog, boolean workflowProcess) {
		
		Optional<User> currentUser;
		try {
			currentUser = userQueryService.findByUsername(currentUser());
		} catch (Exception e) {
			currentUser = Optional.ofNullable(user);
			e.printStackTrace();
		}
		
		String processed = "RELEASED. Your Superior and Admin will review your submitted data prior to approval.";
		if (currentProcess.equals("2")) {
			processed = "APPROVED by your Superior and waiting for next approval.";
		} else if (currentProcess.equals("4")) {
			processed = "APPROVED by your Superior and PUBLISHED.";
			if ("1".equals(it2001.getProcess())) {
				processed = "FAILED to publish with following message:<br>".concat
						(StringUtils.defaultString(response.getMessage(), "No Message"));
			}
		} else if (currentProcess.equals("0")) {
			processed = workflowProcess ? "Requested for edit by Your Superior due to following reason :<br>".concat(it2001.getReason())
					: eventLog.getOperationType().equals(OperationType.UPDATE) ?
							"Updated, waiting for your released and submitted required data to HR Admin or Your Superior."
							: "Created, waiting for your released and submitted required data to HR Admin or Your Superior.";
		} else if (currentProcess.equals("5")) {
			processed = "Cancelled by HR Admin due to following reason :<br>".concat(it2001.getReason());
		} else if (currentProcess.equals("6")) {
			processed = "FAILED to cancel by HR Admin with following message:<br>".concat
					(StringUtils.defaultString(response.getMessage(), "No Message"));
		}

		Map<String, String> composeMap = mailService.composeEmail(eventLog, user);
		if (StringUtils.isNotEmpty(composeMap.get("subject"))) {
			String mailBody = composeMap.get("body").replace(mailService.getMailMasterDataUpdate(), "has been ".concat(processed)).
					replace(eventLog.getOperationType().equals(OperationType.UPDATE) ? "Your data update request"
							: "Your data request", "Your Leave request for ".
							concat(it2001.getId().getInfty().equals("2001") ? "Absence " : "Attendance ").
				    		concat(it2001.getSubty() != null ? StringUtils.defaultString(it2001.getSubty().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY));
			mailService.sendMail(user.getEmail(), composeMap.get("subject"), mailBody);

			// notification email to all Superior
	        if (currentProcess.equals("1") || (currentProcess.equals("2") && eventLog.getOperationType().equals(OperationType.CREATE))) {
			    List<User> superiorUsers = new ArrayList<User>();
		    	mailBody = mailBody.replace(processed, 
		    			"released, waiting for your review and approval.").
		    			replace("Your Leave request for ", "User Leave request for ");
		    	
		    	// fetch all user's Superior
		    	List<PositionRelation> employeePositionRelations = commonServiceFactory.getPositionRelationQueryService()
		    			.findByFromPositionid(user.getEmployee().getPosition().getId())
		    			.stream().collect(Collectors.toList());									
				if (!employeePositionRelations.isEmpty()) {
					for (PositionRelation positionRelation : employeePositionRelations) {
						List<IT0001> toIT0001List = commonServiceFactory.getIT0001QueryService()
								.findByPositionId(positionRelation.getToPosition().getId())
								.stream().collect(Collectors.toList());
						Optional<User> superior = userQueryService.findByPernr(
								toIT0001List.isEmpty() ? 0 : toIT0001List.get(0).getPernr());
						if (superior.isPresent()) {
							if ( !(currentProcess.equals("2") && superior.get().equals(currentUser.get())) )
								superiorUsers.add(superior.get());
						}
					}
				}
		    	
		    	for (User superiorUser : superiorUsers) {
		    		String body = mailBody.replace("Dear <b>".concat(user.getEmployee().getSname()),
		    				"Dear <b>".concat(superiorUser.getEmployee().getSname()));
		    		mailService.sendMail(superiorUser.getEmail(), composeMap.get("subject"), body);
		    	}
		    // notification email to all HR Admin
	        } else if (currentProcess.equals("2") || currentProcess.equals("4")) {
			    List<User> adminUsers = new ArrayList<User>();
			    Optional<Collection<User>> users = userQueryService.fetchAdminByCompany(
			    		user.getEmployee().getT500p().getBukrs().getBukrs());
			    if (users.isPresent()) {
			    	adminUsers = users.get().stream().collect(Collectors.toList());
			    	mailBody = mailBody.replace(processed, currentProcess.equals("2") ?
			    			"approved by ".concat(currentUser.get().getEmployee().getSname()).concat(" and waiting for next approval.") :
			    				"1".equals(it2001.getProcess()) ? "approved but ".concat(processed) : 
			    			    	"approved by ".concat(currentUser.get().getEmployee().getSname()).concat(" and PUBLISHED.")).
			    			replace("Dear <b>".concat(user.getEmployee().getSname()), "Dear <b>HR Admin").
			    			replace("Your Leave request for ", "User Leave request for ");
			    	for (User adminUser : adminUsers) {
			    		mailService.sendMail(adminUser.getEmail(), composeMap.get("subject"), mailBody);
			    	}
			    }
			}					        
		}
		
        if ((currentProcess.equals("4") && "1".equals(it2001.getProcess())) ||
        		currentProcess.equals("6")) {
			throw new DataViolationException(processed.replace("<br>","\n"));
        }
        
	}
	
	private void calculateDateHours(IT2001 it2001Entity) {
		int calDays = 0;
		int offDay = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTime(it2001Entity.getId().getBegda());
		cal.add(Calendar.DATE, -1);
		while (cal.getTime().before(it2001Entity.getId().getEndda())) {
			calDays++;
			cal.add(Calendar.DATE, 1);
			int day = cal.get(Calendar.DAY_OF_WEEK);
			// increase offDay, for Weekend
			if (day < Calendar.MONDAY || day > Calendar.FRIDAY) {
				offDay++;
			}
		}
		List<Phdat> listHoliday = phdatQueryService.findByDateAndType(it2001Entity.getId().getBegda(),
				it2001Entity.getId().getEndda(), "").stream().collect(Collectors.toList());
		if (!listHoliday.isEmpty()) {
			for (Phdat phdat : listHoliday) {
				cal = Calendar.getInstance();
				cal.setTime(phdat.getId().getErdat());
				int day = cal.get(Calendar.DAY_OF_WEEK);
				// increase offDay, only for Public Holiday which is not Weekend
				if (day > Calendar.SUNDAY && day < Calendar.SATURDAY) {
					offDay++;
				}
			}
		}
		it2001Entity.setKaltg(Double.valueOf(calDays));
		it2001Entity.setAbwtg(Double.valueOf(calDays-offDay));
		if (it2001Entity.getStdaz() == null) {
			it2001Entity.setStdaz(it2001Entity.getAbwtg() * 8);
		}
	}
	
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(itValidator);
	}	
}