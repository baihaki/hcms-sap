package com.abminvestama.hcms.rest.api.controller;



import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;
import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;
import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedTrxitmCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedTrxitmQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedTrxitmRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.ZmedTrxitmRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.ZmedTrxitmResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ZmedTrxitmResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ZmedTrxitmResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.ZmedTrxitmRequestBuilderUtil;




@RestController
@RequestMapping("/api/v1/medical_trxitm")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ZmedTrxitmController extends AbstractResource {
	
	private ZmedTrxitmCommandService zmedTrxitmCommandService;
	private ZmedTrxitmQueryService zmedTrxitmQueryService;
	private ZmedTrxitmRequestBuilderUtil zmedTrxitmRequestBuilderUtil;
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	@Autowired
	void setZmedTrxitmCommandService(ZmedTrxitmCommandService zmedTrxitmCommandService) {
		this.zmedTrxitmCommandService = zmedTrxitmCommandService;
	}
	
	@Autowired
	void setZmedTrxitmQueryService(ZmedTrxitmQueryService zmedTrxitmQueryService) {
		this.zmedTrxitmQueryService = zmedTrxitmQueryService;
	}
	
	@Autowired
	void setZmedTrxitmRequestBuilderUtil(ZmedTrxitmRequestBuilderUtil zmedTrxitmRequestBuilderUtil) {
		this.zmedTrxitmRequestBuilderUtil = zmedTrxitmRequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	
	
	
	
	
	
	
	

	@RequestMapping(method = RequestMethod.GET, value = "/clmno/{clmno}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<ZmedTrxitmResponseWrapper>> findByPernr(@PathVariable String clmno, 
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		
		
		ArrayData<ZmedTrxitmResponseWrapper> bunchOfZmedTrxitm = new ArrayData<>();
		APIResponseWrapper<ArrayData<ZmedTrxitmResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfZmedTrxitm);
		
		try {
			List<ZmedTrxitm> listOfZmedTrxitm = new ArrayList<>();
			
			//if (StringUtils.isNotEmpty(datbi)) {
				
				/*if(StringUtils.isNotEmpty(kodemed)){
					listOfZmedTrxitm = zmedTrxitmQueryService.findByKodemed(bukrs, kodemed, 
						CommonDateFunction.convertDateRequestParameterIntoDate(datbi)).stream().collect(Collectors.toList());
			
				} else */{
					listOfZmedTrxitm = zmedTrxitmQueryService.findByClmno(clmno).stream().collect(Collectors.toList());
				
				}
			//}
			
			if (listOfZmedTrxitm.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<ZmedTrxitmResponseWrapper> records = new ArrayList<>();
				for (ZmedTrxitm ZmedTrxitm : listOfZmedTrxitm) {
					records.add(new ZmedTrxitmResponseWrapper(ZmedTrxitm));
				}
				bunchOfZmedTrxitm.setItems(records.toArray(new ZmedTrxitmResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(ZmedTrxitmController.class).findByPernr(clmno, pageNumber)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	
	

	
	
	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated ZmedTrxitmRequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> zmedTrxitmEntity = hcmsEntityQueryService.findByEntityName(ZmedTrxitm.class.getSimpleName());
			if (!zmedTrxitmEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + ZmedTrxitm.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				

				String itmno = "";
				String lastItmno = zmedTrxitmQueryService.findLastId();
				itmno = lastItmno.substring(0, lastItmno.length()-1)+(Integer.parseInt(""+lastItmno.charAt(lastItmno.length()-1))+1);

				request.setItmno(itmno);
				
				RequestObjectComparatorContainer<ZmedTrxitm, ZmedTrxitmRequestWrapper, String> newObjectContainer 
					= zmedTrxitmRequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				
				Optional<ZmedTrxitm> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
				//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = zmedTrxitmCommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create ZmedTrxitm data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("itmno=" + savedEntity.get().getId().getItmno());
				
				EventLog eventLog = new EventLog(zmedTrxitmEntity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				
				ZmedTrxitmResponseWrapper responseWrapper = new ZmedTrxitmResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(ZmedTrxitmController.class).create(request, result)).withSelfRel());		
		return response;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated ZmedTrxitmRequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			ZmedTrxitmResponseWrapper responseWrapper = new ZmedTrxitmResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<ZmedTrxitm> zmedTrxitm = zmedTrxitmQueryService.findOneByCompositeKey(
							request.getItmno(), request.getClmno());
					if (!zmedTrxitm.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					RequestObjectComparatorContainer<ZmedTrxitm, ZmedTrxitmRequestWrapper, String> updatedContainer = zmedTrxitmRequestBuilderUtil.compareAndReturnUpdatedData(request, zmedTrxitm.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
				//	Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				//
				//	if (updatedContainer.getEntity().isPresent()) {
				//		updatedContainer.getEntity().get().setSeqnr(zmedTrxitm.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
				//		zmedTrxitm = zmedTrxitmCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				//	}
				
				//	if (!zmedTrxitm.isPresent()) {
				//		throw new CannotPersistException("Cannot update ZmedTrxitm (Personal Information) data. Please check your data!");
				//	}
				
					responseWrapper = new ZmedTrxitmResponseWrapper(zmedTrxitm.get());
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(ZmedTrxitmController.class).edit(request, result)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}
	
	
}
