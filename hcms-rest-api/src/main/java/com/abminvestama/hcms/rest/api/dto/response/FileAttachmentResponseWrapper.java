package com.abminvestama.hcms.rest.api.dto.response;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 * 
 * Wrapper class for <strong>File Attachment</strong>
 *
 */
@JsonInclude(Include.NON_NULL)
public class FileAttachmentResponseWrapper extends ResourceSupport {
	
	private String attachmentPath;
	private String attachmentContent;

	
	public FileAttachmentResponseWrapper() {
	}
	
	public FileAttachmentResponseWrapper(String attachmentPath, String attachmentContent) {
		if (StringUtils.isEmpty(attachmentPath) && StringUtils.isEmpty(attachmentContent)) {
			new FileAttachmentResponseWrapper();
		} else {
			this
				.setAttachmentPath(attachmentPath)
				.setAttachmentContent(attachmentContent);
		}
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment_path")
	public String getAttachmentPath() {
		return attachmentPath;
	}
	
	private FileAttachmentResponseWrapper setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
		return this;
	}

	/**
	 * GET Attachment Content (base64 encoded string).
	 * 
	 * @return
	 */
	@JsonProperty("attachment_base64")
	public String getAttachmentContent() {
		return attachmentContent;
	}
	
	private FileAttachmentResponseWrapper setAttachmentContent(String attachmentContent) {
		this.attachmentContent = attachmentContent;
		return this;
	}
}