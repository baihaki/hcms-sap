package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.PtrvSrecKey;
import com.abminvestama.hcms.core.model.entity.SubT706B1;
import com.abminvestama.hcms.core.model.entity.SubT706B1Key;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyCostype;
import com.abminvestama.hcms.core.model.entity.PtrvSrec;
import com.abminvestama.hcms.core.service.api.business.query.PtrvSrecQueryService;
import com.abminvestama.hcms.core.service.api.business.query.SubT706B1QueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.PtrvSrecRequestWrapper;


/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add fields: startVolume, endVolume, sumVolume</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add fields: morei, subExpType</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 * 
 */
@Component("PtrvSrecRequestBuilderUtil")
@Transactional(readOnly = true)
public class PtrvSrecRequestBuilderUtil {
	

	private PtrvSrecQueryService ptrvSrecQueryService;
	private SubT706B1QueryService subt706b1QueryService;
	
	@Autowired
	void setPtrvSrecQueryService(PtrvSrecQueryService ptrvSrecQueryService) {
		this.ptrvSrecQueryService = ptrvSrecQueryService;
	}
	
	@Autowired
	void setSubT706B1QueryService(SubT706B1QueryService subt706b1QueryService) {
		this.subt706b1QueryService = subt706b1QueryService;
	}
	
	
	public RequestObjectComparatorContainer<PtrvSrec, PtrvSrecRequestWrapper, String> compareAndReturnUpdatedData(PtrvSrecRequestWrapper requestPayload, PtrvSrec ptrvSrecDB) {
		
		RequestObjectComparatorContainer<PtrvSrec, PtrvSrecRequestWrapper, String> objectContainer = null; 
		
	if (ptrvSrecDB == null) {
		ptrvSrecDB = new PtrvSrec();
		
		try {
			//------------------------------------------------
			//Integrate with SAP and get reinr
			Optional<PtrvSrec> existingPtrvSrec = ptrvSrecQueryService.findOneByCompositeKey(
					StringUtils.defaultString(requestPayload.getMandt(), ""), requestPayload.getPernr(), requestPayload.getReinr(),
					requestPayload.getPerio(), requestPayload.getReceiptno());
			
			if (existingPtrvSrec.isPresent()) {
				throw new DataViolationException("Duplicate PtrvSrec Data for [ssn=" + requestPayload.getPerio());
			}
			ptrvSrecDB.setId(
					new PtrvSrecKey(StringUtils.defaultString(requestPayload.getMandt(), ""), requestPayload.getPernr(), 
							requestPayload.getReinr(), requestPayload.getPerio(), requestPayload.getReceiptno()));
			
			//------------------------------------------------
			
			//ptrvSrecDB.setExpType(requestPayload.getExpType());
			ptrvSrecDB.setRecAmount(requestPayload.getRecAmount());
			ptrvSrecDB.setRecCurr(requestPayload.getRecCurr());
			ptrvSrecDB.setRecRate(requestPayload.getRecRate());
			ptrvSrecDB.setLocAmount(requestPayload.getLocAmount());			
			ptrvSrecDB.setLocCurr(requestPayload.getLocCurr());
			ptrvSrecDB.setShorttxt(requestPayload.getShorttxt());
			ptrvSrecDB.setPaperReceipt(requestPayload.getPaperReceipt());			
			ptrvSrecDB.setRecDate(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getRecDate()));
			SubT706B1Key subt706b1Key = new SubT706B1Key(requestPayload.getMorei(), requestPayload.getExpType(), requestPayload.getSubExpType());
			Optional<SubT706B1> subT706B1 = subt706b1QueryService.findById(Optional.ofNullable(subt706b1Key));
			if (subT706B1.isPresent()) {
				ptrvSrecDB.setSubt706b1(subT706B1.get());
			}
			ptrvSrecDB.setStartVolume(requestPayload.getStartVolume());
			ptrvSrecDB.setEndVolume(requestPayload.getEndVolume());
			ptrvSrecDB.setSumVolume(requestPayload.getSumVolume());
			
			
		} catch (Exception ignored) {
			System.err.println(ignored);
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(ptrvSrecDB, requestPayload);
		
	} else {
		
		if (StringUtils.isNotBlank(requestPayload.getExpType())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getExpType(), ptrvSrecDB.getSubt706b1().getSht706b1().getSpklz())) {
				SubT706B1Key subt706b1Key = new SubT706B1Key(requestPayload.getMorei(), requestPayload.getExpType(), requestPayload.getSubExpType());
				try {
					Optional<SubT706B1> subT706B1 = subt706b1QueryService.findById(Optional.ofNullable(subt706b1Key));
					if (subT706B1.isPresent()) {
						ptrvSrecDB.setSubt706b1(subT706B1.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
				}
			}
		}
		
		if (requestPayload.getRecAmount() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getRecAmount(), ptrvSrecDB.getRecAmount())) {
				ptrvSrecDB.setRecAmount(requestPayload.getRecAmount());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getRecCurr())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getRecCurr(), ptrvSrecDB.getRecCurr())) {
				ptrvSrecDB.setRecCurr(requestPayload.getRecCurr());
				requestPayload.setDataChanged(true);
			}
		}			
		
		if (requestPayload.getRecRate() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getRecRate(), ptrvSrecDB.getRecRate())) {
				ptrvSrecDB.setRecRate(requestPayload.getRecRate());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (requestPayload.getLocAmount() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getLocAmount(), ptrvSrecDB.getLocAmount())) {
				ptrvSrecDB.setLocAmount(requestPayload.getLocAmount());
				requestPayload.setDataChanged(true);
			}
		}

		if (StringUtils.isNotBlank(requestPayload.getLocCurr())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getLocCurr(), ptrvSrecDB.getLocCurr())) {
				ptrvSrecDB.setLocCurr(requestPayload.getLocCurr());
				requestPayload.setDataChanged(true);
			}
		}

		if (StringUtils.isNotBlank(requestPayload.getShorttxt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getShorttxt(), ptrvSrecDB.getShorttxt())) {
				ptrvSrecDB.setShorttxt(requestPayload.getShorttxt());
				requestPayload.setDataChanged(true);
			}
		}

		if (StringUtils.isNotBlank(requestPayload.getPaperReceipt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPaperReceipt(), ptrvSrecDB.getPaperReceipt())) {
				ptrvSrecDB.setPaperReceipt(requestPayload.getPaperReceipt());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getRecDate())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getRecDate()), ptrvSrecDB.getRecDate()) ){
				ptrvSrecDB.setRecDate(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getRecDate()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (requestPayload.getStartVolume() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getStartVolume(), ptrvSrecDB.getStartVolume())) {
				ptrvSrecDB.setStartVolume(requestPayload.getStartVolume());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (requestPayload.getEndVolume() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getEndVolume(), ptrvSrecDB.getEndVolume())) {
				ptrvSrecDB.setEndVolume(requestPayload.getEndVolume());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (requestPayload.getSumVolume() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getSumVolume(), ptrvSrecDB.getSumVolume())) {
				ptrvSrecDB.setSumVolume(requestPayload.getSumVolume());
				requestPayload.setDataChanged(true);
			}
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(ptrvSrecDB, requestPayload);	
				
	}
	
	JSONObject json = new JSONObject(requestPayload);
	objectContainer.setEventData(json.toString());
	
	return objectContainer;
}
}
