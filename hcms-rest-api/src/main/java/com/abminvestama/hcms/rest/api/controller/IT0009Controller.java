package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.exception.WorkflowNotFoundException;
import com.abminvestama.hcms.core.model.constant.ApprovalAction;
import com.abminvestama.hcms.core.model.constant.ApprovalType;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.EntityCategory;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.constant.UserRole;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.constant.WorkflowType;
import com.abminvestama.hcms.core.model.entity.ApprovalCategory;
import com.abminvestama.hcms.core.model.entity.ApprovalEventLog;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.Workflow;
import com.abminvestama.hcms.core.model.entity.WorkflowStep;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.IT0009CommandService;
import com.abminvestama.hcms.core.service.api.business.query.IT0009QueryService;
import com.abminvestama.hcms.core.service.api.business.query.ITQueryService;
import com.abminvestama.hcms.core.service.helper.IT0009SoapObject;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0009RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ApprovalEventLogResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.IT0009ResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.IT0009RequestBuilderUtil;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 * 
 * Document WorkflowType Variations :
 * 1. (create)  —> INITIAL_DRAFT —> (approved) —> PUBLISHED
 * 2. (create)  —> INITIAL_DRAFT —> (rejected) —> DRAFT_REJECTED —> (updated)  —> UPDATED_DRAFT  —> (approved) —> PUBLISHED
 * 3. (create)  —> INITIAL_DRAFT —> (updated)  —> UPDATED_DRAFT  —> (approved) —> PUBLISHED
 * 4. (create)  —> INITIAL_DRAFT —> (updated)  —> UPDATED_DRAFT  —> (rejected) —> DRAFT_REJECTED —> (updated)  —> UPDATED_DRAFT —> (approved) —> PUBLISHED
 * 5. PUBLISHED —> (updated) —> UPDATE_IN_PROGRESS —> (approved) —> PUBLISHED
 * 6. PUBLISHED —> (updated) —> UPDATE_IN_PROGRESS —> (rejected) —> UPDATE_REJECTED —> (updated) —> UPDATE_IN_PROGRESS —> (approved) —> PUBLISHED
 *
 * TODO: MUST REFACTOR LATER !!!
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: findByCriteria</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Modify methods (implement reflection on ApprovalController): approve, reject, getUpdatedFields</td></tr>
 *     <tr><td>1.0.1</td><td>Yauri</td><td>Add methods: approve, reject, getUpdatedFields</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/bank")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = { CannotPersistException.class, Exception.class })
public class IT0009Controller extends AbstractResource {

	private IT0009CommandService it0009CommandService;
	private IT0009QueryService it0009QueryService;
	private IT0009RequestBuilderUtil it0009RequestBuilderUtil;
	
	private CommonServiceFactory commonServiceFactory;
	
	private Validator itValidator;
	
	private MuleConsumerService muleConsumerService;
	private ApprovalController approvalController;
	private ITQueryService itQueryService;
	
	@Autowired
	void setIT0009CommandService(IT0009CommandService it0009CommandService) {
		this.it0009CommandService = it0009CommandService;
	}
	
	@Autowired
	void setIT0009QueryService(IT0009QueryService it0009QueryService) {
		this.it0009QueryService = it0009QueryService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	@Autowired
	void setIT0009RequestBuilderUtil(IT0009RequestBuilderUtil it0009RequestBuilderUtil) {
		this.it0009RequestBuilderUtil = it0009RequestBuilderUtil;
	}
	
	@Autowired
	@Qualifier("itValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}
	
	@Autowired
	void setApprovalController(ApprovalController approvalController) {
		this.approvalController = approvalController;
	}
	
	@Autowired
	void setITQueryService(ITQueryService itQueryService) {
		this.itQueryService = itQueryService;
	}
	
	@SuppressWarnings("unchecked")
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_criteria", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<IT0009ResponseWrapper>> findByCriteria(
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "exclude_status", required = false, defaultValue="PUBLISHED,DRAFT_REJECTED,UPDATE_REJECTED") String excludeStatus,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {

		Page<IT0009ResponseWrapper> responsePage = new PageImpl<IT0009ResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<IT0009ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<IT0009> resultPage = null;
		String bukrs = null;
		Long pernr = null;
		String[] excludes = excludeStatus.split(",");
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		
		if (StringUtils.isNotEmpty(ssn)) {
			pernr = Long.valueOf(ssn);
			if (!currentUser.get().getRoles().contains(adminRole.get()) &&
					!currentUser.get().getEmployee().getPernr().equals(pernr) ) {
				throw new AuthorizationException(
						"Insufficient Privileges. You can't fetch data of other Employee.");
			}
		} else {
			// no specified ssn, MUST BE ADMIN
			if (!currentUser.get().getRoles().contains(adminRole.get())) {
				throw new AuthorizationException(
						"Insufficient Privileges. Please login as 'ADMIN'!");
			}
			bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
		}
		
		try {
			resultPage = (Page<IT0009>) itQueryService.fetchDistinctByCriteriaWithPaging(pageNumber, bukrs, pernr, excludes,
					new IT0009(), currentUser.get().getEmployee());
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			
			if (resultPage.hasContent()) {

				List<IT0009ResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(it0009 -> {
					records.add(new IT0009ResponseWrapper(it0009));
				});
				responsePage = new PageImpl<IT0009ResponseWrapper>(records, it0009QueryService.createPageRequest(pageNumber),
						resultPage.getTotalElements());
				response.setData(responsePage);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0009Controller.class).findByCriteria(ssn, excludeStatus, pageNumber)).withSelfRel());
		
		return response;
	}
	
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/status/{status}/page/{pageNumber}", 
	produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0009ResponseWrapper>> findByDocumentStatus(@PathVariable String status, 
			@PathVariable int pageNumber) throws Exception {
		ArrayData<IT0009ResponseWrapper> bunchOfIT0009 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0009ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0009);
		
		try {
			Page<IT0009> it0009Records = it0009QueryService.findByStatus(status, pageNumber);
			
			if (it0009Records == null || !it0009Records.hasContent()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0009ResponseWrapper> records = new ArrayList<>();
				
				it0009Records.forEach(it0009 -> {
					records.add(new IT0009ResponseWrapper(it0009));
				});
				
				bunchOfIT0009.setItems(records.toArray(new IT0009ResponseWrapper[records.size()]));
			}
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0009Controller.class).findByDocumentStatus(status, pageNumber)).withSelfRel());
		
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.PUT, value = "/approveOri")
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> approveOri(@RequestHeader(required = true, value = "ssn") String pernr,
			@RequestHeader(required = true, value = "infotype") String infty,
			@RequestHeader(required = true, value = "subtype") String subtype,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda)
					throws AuthorizationException, NumberFormatException, CannotPersistException,
					DataViolationException, EntityNotFoundException, WorkflowNotFoundException, Exception {
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());

			long ssn = Long.valueOf(pernr);
			
			Optional<HCMSEntity> it0009Entity = commonServiceFactory.getHCMSEntityQueryService().findByEntityName(IT0009.class.getSimpleName());
			
			Optional<IT0009> it0009 = it0009QueryService.findOneByCompositeKey(Long.valueOf(pernr), subtype, 
					CommonDateFunction.convertDateRequestParameterIntoDate(endda), 
					CommonDateFunction.convertDateRequestParameterIntoDate(begda));
							
			//StringBuffer insufficientMsg = new StringBuffer();
			
			// Check the operation type (Create or Update) from the document_status, with one from the following condition
			// 1. if document_status is INITIAL_DRAFT, then the operation_type is CREATE
			// 2. if document_status either UPDATED_DRAFT or UPDATE_IN_PROGRESS, then the operation type is UPDATE
			
			StringBuffer objectId = new StringBuffer("pernr=").append(ssn);
			objectId.append(",infty=0009,subty=").append(subtype).append(",endda=").append(endda);
			objectId.append(",begda=").append(begda);
			
			// process approval
			commonServiceFactory.getIT0009CommandService().save(
					this.processApproval(new IT0009Controller.WorkflowStepApproval(
					it0009.get().getStatus(), it0009Entity.get(), it0009.get(), objectId.toString(), currentUser.get())), 
					currentUser.get());
			
			ApprovalEventLog approvalEventLog = new ApprovalEventLog(it0009Entity.get(), objectId.toString(), 
					currentUser.get().getEmployee().getPernr(), currentUser.get().getEmployee().getPosition(), ApprovalAction.APPROVED);
			commonServiceFactory.getApprovalEventLogCommandService().save(approvalEventLog, currentUser.get());
			// Record (Append) to trx_event_log
			StringBuffer eventData = new StringBuffer("{\"dataChanged\": false, \"prev_document_status\":\"");
			eventData.append(DocumentStatus.INITIAL_DRAFT);
			eventData.append("\", \"document_status\":\"");
			eventData.append(DocumentStatus.PUBLISHED);
			eventData.append("\"}");
			EventLog eventLog = new EventLog(it0009Entity.get(), objectId.toString(), OperationType.APPROVAL, eventData.toString());
			commonServiceFactory.getEventLogCommandService().save(eventLog, currentUser.get());
			
			response.setData(new ApprovalEventLogResponseWrapper(approvalEventLog));			
			
		} catch (EntityNotFoundException e) {
			throw e;
		} catch (WorkflowNotFoundException e) {
			throw e;
		} catch (DataViolationException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0009Controller.class).approveOri(pernr, infty, subtype, endda, begda)).withSelfRel());				
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.PUT, value = "/approve")
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> approve(
			@RequestHeader(required = true, value = "ssn") String pernr,
			@RequestHeader(required = true, value = "subtype") String subtype,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda)
					throws AuthorizationException, NumberFormatException, CannotPersistException,
					DataViolationException, EntityNotFoundException, WorkflowNotFoundException, Exception {
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", pernr);
			keyMap.put("infotype", SAPInfoType.BANK_DETAILS.infoType());
			keyMap.put("subtype", subtype);
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			keyMap.put("muleService", BAPIFunction.IT0009_CREATE.service());
			keyMap.put("muleUri", BAPIFunction.IT0009_CREATE_URI.service());
			
			String className = IT0009.class.getSimpleName();
			response = approvalController.approve(keyMap, className, it0009QueryService, it0009CommandService,
					IT0009SoapObject.class, it0009RequestBuilderUtil, new IT0009RequestWrapper());

		} catch (EntityNotFoundException e) {
			throw e;
		} catch (WorkflowNotFoundException e) {
			throw e;
		} catch (DataViolationException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0009Controller.class).approve(pernr, subtype, endda, begda)).withSelfRel());				
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.PUT, value = "/rejectOri")
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> rejectOri(@RequestHeader(required = true, value = "ssn") String pernr,
			@RequestHeader(required = true, value = "subtype") String subtype,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda) 
					throws AuthorizationException, NumberFormatException, CannotPersistException, 
					DataViolationException, EntityNotFoundException, Exception {
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
			
			if (!currentUser.isPresent()) {
				throw new AuthorizationException(
						"Insufficient privileges. Please login either as 'ADMIN' or 'SUPERIOR'!");						
			}
			
			long ssn = Long.valueOf(pernr);			
			
			if (currentUser.get().getEmployee().getPernr() == ssn) {
				throw new AuthorizationException("You can't reject your own data!");
			}						
			
			Optional<HCMSEntity> it0009Entity = commonServiceFactory.getHCMSEntityQueryService().findByEntityName(IT0009.class.getSimpleName());
			if (!it0009Entity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + IT0009.class.getSimpleName() + "' !!!");
			}						
						
			Optional<IT0009> it0009 = it0009QueryService.findOneByCompositeKey(ssn, subtype, 
					CommonDateFunction.convertDateRequestParameterIntoDate(endda), 
					CommonDateFunction.convertDateRequestParameterIntoDate(begda));
			if (!it0009.isPresent()) {
				throw new EntityNotFoundException("Data can't be found!");
			}		
			
			if (it0009.get().getStatus() == DocumentStatus.PUBLISHED) {
				throw new DataViolationException("You cannot reject 'PUBLISHED' document!");
			}		
			
			if (it0009.get().getStatus() == DocumentStatus.DRAFT_REJECTED || it0009.get().getStatus() == DocumentStatus.UPDATE_REJECTED) {
				throw new DataViolationException("Document had already been rejected.");
			}
			
			// Do a 'reject' logic to either INITIAL_DRAFT or UPDATED_DRAFT or UPDATE_IN_PROGRESS document (merely change the 'document_status')
			if (it0009.get().getStatus() == DocumentStatus.INITIAL_DRAFT || it0009.get().getStatus() == DocumentStatus.UPDATED_DRAFT) {
				it0009.get().setStatus(DocumentStatus.DRAFT_REJECTED);
			} else if (it0009.get().getStatus() == DocumentStatus.UPDATE_IN_PROGRESS) {
				it0009.get().setStatus(DocumentStatus.UPDATE_REJECTED);
			}
			
			StringBuilder key = new StringBuilder("pernr=" + pernr);
			key.append(",infty=0009");
			key.append(",subty=").append(subtype);
			key.append(",endda=").append(endda);
			key.append(",begda=").append(begda);
			
			Collection<EventLog> eventLogRecords = commonServiceFactory.getEventLogQueryService().findByEntityIdAndObjectId(it0009Entity.get().getId(), key.toString());
			
			EventLog lastEventLog = eventLogRecords.isEmpty() ? null : eventLogRecords.stream().collect(Collectors.toList()).get(0);
			
			EventLog eventLog = new EventLog(it0009Entity.get(), key.toString(), OperationType.DOCUMENT_REJECT,
					(!StringUtils.isEmpty(lastEventLog.getEventData()) ? lastEventLog.getEventData() : ""));
			
			Optional<EventLog> savedEventLog = commonServiceFactory.getEventLogCommandService().save(eventLog, currentUser.get());
			
			if (!savedEventLog.isPresent()) {
				throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
			}				
			
			ApprovalEventLog approvalEventLog = new ApprovalEventLog(it0009Entity.get(), key.toString(), currentUser.get().getEmployee().getPernr(), 
					currentUser.get().getEmployee().getPosition(), ApprovalAction.REJECTED);
			
			Optional<ApprovalEventLog> savedApprovalEventLog = commonServiceFactory.getApprovalEventLogCommandService().save(approvalEventLog, currentUser.get());
			
			if (!savedApprovalEventLog.isPresent()) {
				throw new CannotPersistException("Cannot reject document! Operation failed.");
			}
			
			ApprovalEventLogResponseWrapper approvalEventLogResponse = new ApprovalEventLogResponseWrapper(approvalEventLog);
			
			response.setData(approvalEventLogResponse);
			
		} catch (AuthorizationException e) { 
			throw e;
		} catch (NumberFormatException e) { 
			throw e;
		} catch (CannotPersistException e) {
			throw e;
		} catch (DataViolationException e) {
			throw e;
		} catch (EntityNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0009Controller.class).rejectOri(pernr, subtype, endda, begda)).withSelfRel());		
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.PUT, value = "/reject")
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> reject(
			@RequestHeader(required = true, value = "ssn") String pernr,
			@RequestHeader(required = true, value = "subtype") String subtype,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda) 
					throws AuthorizationException, NumberFormatException, CannotPersistException, 
					DataViolationException, EntityNotFoundException, Exception {
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", pernr);
			keyMap.put("infotype", SAPInfoType.BANK_DETAILS.infoType());
			keyMap.put("subtype", subtype);
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			
			String className = IT0009.class.getSimpleName();
			response = approvalController.reject(keyMap, className, it0009QueryService, it0009CommandService);
			
		} catch (AuthorizationException e) { 
			throw e;
		} catch (NumberFormatException e) { 
			throw e;
		} catch (CannotPersistException e) {
			throw e;
		} catch (DataViolationException e) {
			throw e;
		} catch (EntityNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0009Controller.class).reject(pernr, subtype, endda, begda)).withSelfRel());		
		return response;
	}
	
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_updated_fieldsOri")
	public APIResponseWrapper<IT0009ResponseWrapper> getUpdatedFieldsOri(
			@RequestHeader(required = true, value = "ssn") long ssn,
			@RequestHeader(required = true, value = "subtype") String subtype,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda
			) throws DataViolationException, EntityNotFoundException, Exception {

		APIResponseWrapper<IT0009ResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Optional<IT0009> it0009 = it0009QueryService.findOneByCompositeKey(ssn, subtype, 
					CommonDateFunction.convertDateRequestParameterIntoDate(endda), 
					CommonDateFunction.convertDateRequestParameterIntoDate(begda));
			if (!it0009.isPresent()) {
				throw new EntityNotFoundException("Data can't be found!");
			}
			
			if (it0009.get().getStatus() != DocumentStatus.UPDATE_IN_PROGRESS) {
				throw new DataViolationException("No Data Changes! Data is up-to-date.");
			}
			
			Optional<HCMSEntity> it0009Entity = commonServiceFactory.getHCMSEntityQueryService().findByEntityName(IT0009.class.getSimpleName());
			if (!it0009Entity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + IT0009.class.getSimpleName() + "' !!!");
			}			
			
			StringBuffer objectId = new StringBuffer("pernr=").append(ssn);
			objectId.append(",infty=0009,subty=").append(subtype).append(",endda=").append(endda);
			objectId.append(",begda=").append(begda);
			
			Collection<EventLog> eventLogRecords = commonServiceFactory.getEventLogQueryService().findByEntityIdAndObjectId(
					it0009Entity.get().getId(), objectId.toString());
			
			EventLog eventLog = eventLogRecords.isEmpty() ? null : eventLogRecords.stream().collect(Collectors.toList()).get(0);
			
			response.setData(new IT0009ResponseWrapper(it0009.get(), eventLog.getEventData(), commonServiceFactory));
			
		} catch (DataViolationException e) { 
			throw e;
		} catch (EntityNotFoundException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0009Controller.class).getUpdatedFieldsOri(ssn, subtype, endda, begda)).withSelfRel());
		return response;
	}
	
	@SuppressWarnings("unchecked")
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_updated_fields")
	public APIResponseWrapper<IT0009ResponseWrapper> getUpdatedFields(
			@RequestHeader(required = true, value = "ssn") String ssn,
			@RequestHeader(required = true, value = "subtype") String subtype,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda
			) throws DataViolationException, EntityNotFoundException, Exception {

		APIResponseWrapper<IT0009ResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", ssn);
			keyMap.put("infotype", SAPInfoType.BANK_DETAILS.infoType());
			keyMap.put("subtype", subtype);
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			
			String className = IT0009.class.getSimpleName();
			response = (APIResponseWrapper<IT0009ResponseWrapper>) approvalController.getUpdatedFields(
					keyMap, className, it0009QueryService, response, IT0009ResponseWrapper.class);
			
		} catch (DataViolationException e) { 
			throw e;
		} catch (EntityNotFoundException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0009Controller.class).getUpdatedFields(ssn, subtype, endda, begda)).withSelfRel());
		return response;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0009ResponseWrapper>> findByPernr(@PathVariable String pernr, 
			@RequestParam(value = "status", required = false) String status, 
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws NumberFormatException, Exception {
		ArrayData<IT0009ResponseWrapper> bunchOfIT0009 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0009ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0009);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0009> listOfIT0009 = new ArrayList<>();
			
			if (StringUtils.isNotEmpty(status)) {
				Page<IT0009> data = it0009QueryService.findByPernrAndStatus(numOfPernr, status, pageNumber);
				if (data.hasContent()) {
					listOfIT0009 = data.getContent();
				}
			} else {
				listOfIT0009 = it0009QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			}
			
			if (listOfIT0009.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0009ResponseWrapper> records = new ArrayList<>();
				for (IT0009 it0009 : listOfIT0009) {
					records.add(new IT0009ResponseWrapper(it0009));
				}
				bunchOfIT0009.setItems(records.toArray(new IT0009ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0009Controller.class).findByPernr(pernr, status, pageNumber)).withSelfRel());
		return response;
	}	
	
	@Secured({AccessRole.USER_GROUP, AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated IT0009RequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
			
			IT0009ResponseWrapper responseWrapper = new IT0009ResponseWrapper(null);
			response = new APIResponseWrapper<>(responseWrapper);
			
			Optional<IT0009> existingIT0009 = it0009QueryService.findOneByCompositeKey(
					request.getPernr(), request.getSubty(), 
					CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()), 
					CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
			
			if (existingIT0009.isPresent()) {
				throw new DataViolationException("Duplicate IT0009 Data for [ssn=" + request.getPernr() + ",end_date=" +
						request.getEndda() + ",begin_date=" + request.getBegda() + "]");
			}
						
			Optional<HCMSEntity> it0009Entity = commonServiceFactory.getHCMSEntityQueryService().findByEntityName(IT0009.class.getSimpleName());
			if (!it0009Entity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + IT0009.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<IT0009, IT0009RequestWrapper, String> newObjectContainer 
					= it0009RequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient privileges. Please login!");						
				}
				
				Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
				if (currentUser.get().getEmployee().getPernr().longValue() != request.getPernr() && 
						!currentUser.get().getRoles().contains(adminRole.get())) {
					throw new AuthorizationException(
							"You don't have privilege to create/update other employee data. Only Admin can do that!");
				}
				
				Optional<IT0009> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
					newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = it0009CommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create IT0009 (Bank Details) data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("pernr=" + savedEntity.get().getId().getPernr());
				key.append(",infty=" + savedEntity.get().getId().getInfty());
				key.append(",subty=" + savedEntity.get().getId().getSubty());
				key.append(",endda=" + CommonDateFunction.convertDateToStringYMD(savedEntity.get().getId().getEndda()));
				key.append(",begda=" + CommonDateFunction.convertDateToStringYMD(savedEntity.get().getId().getBegda()));
				EventLog eventLog = new EventLog(it0009Entity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				eventLog.setStatus(savedEntity.get().getStatus().toString());
				Optional<EventLog> savedEventLog = commonServiceFactory.getEventLogCommandService().save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				
				//IT0009SoapObject it0009SoapObject = new IT0009SoapObject(savedEntity.get()); 
				//muleConsumerService.postToSAP(BAPIFunction.IT0009_CREATE.service(), "bankdetail", it0009SoapObject);
				
				//IT0009ResponseWrapper responseWrapper = new IT0009ResponseWrapper(savedEntity.get());
				responseWrapper = new IT0009ResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);
				
			} catch (DataViolationException e) { 
				throw e;
				//System.err.println(e);
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
				//System.err.println(e);
			} catch (Exception e) {
				throw e;
			}
		}
		response.add(linkTo(methodOn(IT0009Controller.class).create(request, result)).withSelfRel());		
		return response;
	}
	
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated IT0009RequestWrapper request, BindingResult result) 
			throws NullPointerException, AuthorizationException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			
			boolean isDataChanged = false;
			
			IT0009ResponseWrapper responseWrapper = new IT0009ResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<HCMSEntity> it0009Entity = commonServiceFactory.getHCMSEntityQueryService().findByEntityName(IT0009.class.getSimpleName());
					if (!it0009Entity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + IT0009.class.getSimpleName() + "' !!!");
					}					
					
					Optional<IT0009> it0009 = it0009QueryService.findOneByCompositeKey(request.getPernr(), request.getSubty(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
					if (!it0009.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
					
					IT0009 currentIT0009 = it0009RequestBuilderUtil.copy(it0009.get());
									
					RequestObjectComparatorContainer<IT0009, IT0009RequestWrapper, String> updatedContainer 
						//= it0009RequestBuilderUtil.compareAndReturnUpdatedData(request, it0009.get());
						= it0009RequestBuilderUtil.compareAndReturnUpdatedData(request, currentIT0009);

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
					
					if (!currentUser.isPresent()) {
						throw new AuthorizationException(
								"Insufficient privileges. Please login!");						
					}
					
					Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
					if (currentUser.get().getEmployee().getPernr().longValue() != request.getPernr() && 
							!currentUser.get().getRoles().contains(adminRole.get())) {
						throw new AuthorizationException(
								"You don't have privilege to create/update other employee data. Only Admin can do that!");
					}
				
					if (updatedContainer.getEntity().isPresent()) {
						//updatedContainer.getEntity().get().setSeqnr(it0009.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
						//it0009 = it0009CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null); // since version 2, update document only change the status 
						if (it0009.get().getStatus() == DocumentStatus.INITIAL_DRAFT) {
							it0009.get().setStatus(DocumentStatus.UPDATED_DRAFT);
							currentIT0009.setStatus(DocumentStatus.UPDATED_DRAFT);
						} else if (it0009.get().getStatus() == DocumentStatus.PUBLISHED) { 
							it0009.get().setStatus(DocumentStatus.UPDATE_IN_PROGRESS);
							currentIT0009.setStatus(DocumentStatus.UPDATE_IN_PROGRESS);
						}						
					}
				
					if (!it0009.isPresent()) {
						throw new CannotPersistException("Cannot update IT0009 (Bank Details) data. Please check your data!");
					}
					
					StringBuilder key = new StringBuilder("pernr=" + it0009.get().getId().getPernr());
					key.append(",infty=" + it0009.get().getId().getInfty());
					key.append(",subty=" + it0009.get().getId().getSubty());
					key.append(",endda=" + it0009.get().getId().getEndda());
					key.append(",begda=" + it0009.get().getId().getBegda());
					EventLog eventLog = new EventLog(it0009Entity.get(), key.toString(), OperationType.UPDATE, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					eventLog.setStatus(it0009.get().getStatus().toString());
					Optional<EventLog> savedEventLog = commonServiceFactory.getEventLogCommandService().save(eventLog, currentUser.get());
					
					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}					
				
					//responseWrapper = new IT0009ResponseWrapper(it0009.get());
					responseWrapper = new IT0009ResponseWrapper(currentIT0009);
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (EntityNotFoundException enf) { 
					throw enf;
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (AuthorizationException ae) { 
					throw ae;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(IT0009Controller.class).edit(request, result)).withSelfRel());
		return response;
	}	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(itValidator);
	}
	
	private class WorkflowStepApproval {
		
		DocumentStatus documentStatus;
		HCMSEntity entity;
		IT0009 it0009;
		String objectId;
		User currentUser;
		
		WorkflowStepApproval(DocumentStatus documentStatus, HCMSEntity entity, IT0009 it0009, String objectId, User currentUser) {
			this.documentStatus = documentStatus;
			this.entity = entity;
			this.it0009 = it0009;
			this.objectId = objectId;
			this.currentUser = currentUser;
		}
		
	}
	
	private IT0009 processApproval(WorkflowStepApproval workflowStepApproval) throws Exception {
		Optional<Workflow> workflow = Optional.empty();
		Optional<ApprovalCategory> approvalCategory = Optional.empty();
		Collection<ApprovalEventLog> approvalEventLogs = Collections.emptyList();
		Optional<Collection<WorkflowStep>> workflowSteps = Optional.empty();
		
		StringBuffer insufficientMsg = new StringBuffer();
		
		//switch (it0009.get().getStatus()) {
		switch (workflowStepApproval.documentStatus) {
			case INITIAL_DRAFT:
				workflow = commonServiceFactory.getWorkflowQueryService().findByName(WorkflowType.CREATE_HR_MASTER.name());
				if (!workflow.isPresent()) {
					throw new WorkflowNotFoundException("Cannot find workflow for this kind of approval!");
				}
				approvalCategory = commonServiceFactory.getApprovalCategoryQueryService().findByWorkflowAndEntityCategory(workflow.get(), EntityCategory.HR_MASTER_DATA);
				if (!approvalCategory.isPresent()) {
					throw new EntityNotFoundException("Cannot find approval category!");
				}
				
				/*
				approvalEventLogs = commonServiceFactory.getApprovalEventLogQueryService()
						//.findByEntityIdAndObjectIdAndApprovalAction(it0009Entity.get().getId(),
						.findByEntityIdAndObjectIdAndApprovalAction(workflowStepApproval.entity.getId(),
						//objectId.toString(), ApprovalAction.APPROVED);
						workflowStepApproval.objectId, ApprovalAction.APPROVED);
				*/
				
				workflowSteps = commonServiceFactory.getWorkflowStepQueryService()
						.findByWorkflowAndSeq(workflow.get(), (approvalEventLogs.isEmpty() ? 1 : approvalEventLogs.size() + 1));

				if (!workflowSteps.isPresent()) {
					throw new WorkflowNotFoundException("Cannot find workflow for this operation! Please define the workflow first!");
				}
				
				WorkflowStep workflowStep = workflowSteps.get().stream().collect(Collectors.toList()).get(0);
				
				//if (insufficientMsg.length() > 0 && (!workflowStep.getPosition().getId().equalsIgnoreCase(currentUser.get().getEmployee().getPosition().getId()))) {
				if (insufficientMsg.length() > 0 && (!workflowStep.getPosition().getId().equalsIgnoreCase(workflowStepApproval.currentUser.getEmployee().getPosition().getId()))) {
					throw new DataViolationException("Only 'SUPERIOR' or 'ADMIN' can do the approval!");
				}
				
				if (workflowStep.getApprovalType() == ApprovalType.ONE_SUPERIOR) {
					workflowStepApproval.it0009.setStatus(DocumentStatus.PUBLISHED);
					//commonServiceFactory.getIT0009CommandService().save(workflowStepApproval.it0009, workflowStepApproval.currentUser);
				} else { // need approval from all superior 
				}
				
				break;
			case UPDATED_DRAFT:
			case UPDATE_IN_PROGRESS:
				workflow = commonServiceFactory.getWorkflowQueryService().findByName(WorkflowType.UPDATE_HR_MASTER.name());
				if (!workflow.isPresent()) {
					throw new WorkflowNotFoundException("Cannot find workflow for this kind of approval!");
				}
				approvalCategory = commonServiceFactory.getApprovalCategoryQueryService().findByWorkflowAndEntityCategory(workflow.get(), EntityCategory.HR_MASTER_DATA);
				if (!approvalCategory.isPresent()) {
					throw new EntityNotFoundException("Cannot find approval category!");
				}
				
				/*
				approvalEventLogs = commonServiceFactory.getApprovalEventLogQueryService()
						.findByEntityIdAndObjectIdAndApprovalAction(workflowStepApproval.entity.getId(), 
						workflowStepApproval.objectId, ApprovalAction.APPROVED);
				*/

				workflowSteps = commonServiceFactory.getWorkflowStepQueryService()
						.findByWorkflowAndSeq(workflow.get(), (approvalEventLogs.isEmpty() ? 1 : approvalEventLogs.size() + 1));
				
				break;
				
			default: throw new DataViolationException("Invalid Operation. Can't do approval for this 'PUBLISHED' document!");
		}		
		
		
		return workflowStepApproval.it0009;
	}
}
