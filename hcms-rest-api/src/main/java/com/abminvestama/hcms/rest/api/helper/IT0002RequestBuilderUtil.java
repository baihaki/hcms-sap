package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.Attachment;
import com.abminvestama.hcms.core.model.entity.IT0002;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.T002T;
import com.abminvestama.hcms.core.model.entity.T005T;
import com.abminvestama.hcms.core.model.entity.T502T;
import com.abminvestama.hcms.core.model.entity.T516T;
import com.abminvestama.hcms.core.model.entity.T522G;
import com.abminvestama.hcms.core.model.entity.T535N;
import com.abminvestama.hcms.core.model.entity.T535NKey;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.business.query.AttachmentQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0002QueryService;
import com.abminvestama.hcms.core.service.api.business.query.T002TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T005TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T502TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T516TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T522GQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T535NQueryService;
import com.abminvestama.hcms.core.service.util.FileAttachmentService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0002RequestWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.4
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Fix: if Anred (Form-of-Address) is null, set Anred value based on Gender and Marrital Status</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Adjust attachment fields on EventData Json</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData: Add attachment fields</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Modify compareAndReturnUpdatedData to handle Create</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("it0002RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT0002RequestBuilderUtil {
	
	private T535NQueryService t535nQueryService;
	private T522GQueryService t522gQueryService;
	private T005TQueryService t005tQueryService;
	private T002TQueryService t002tQueryService;
	private T516TQueryService t516tQueryService;
	private T502TQueryService t502tQueryService;
	private IT0002QueryService it0002QueryService;
	private AttachmentQueryService attachmentQueryService;
	private FileAttachmentService fileAttachmentService;
	
	@Autowired
	void setIT0002QueryService(IT0002QueryService it0002QueryService) {
		this.it0002QueryService = it0002QueryService;
	}
	
	@Autowired
	void setT535NQueryService(T535NQueryService t535nQueryService) {
		this.t535nQueryService = t535nQueryService;
	}
	
	@Autowired
	void setT522GQueryService(T522GQueryService t522gQueryService) {
		this.t522gQueryService = t522gQueryService;
	}
	
	@Autowired
	void setT005TQueryService(T005TQueryService t005tQueryService) {
		this.t005tQueryService = t005tQueryService;
	}
	
	@Autowired
	void setT002TQueryService(T002TQueryService t002tQueryService) {
		this.t002tQueryService = t002tQueryService;
	}
	
	@Autowired
	void setT516TQueryService(T516TQueryService t516tQueryService) {
		this.t516tQueryService = t516tQueryService;
	}
	
	@Autowired
	void setT502TQueryService(T502TQueryService t502tQueryService) {
		this.t502tQueryService = t502tQueryService;
	}
	
	@Autowired
	void setAttachmentQueryService(AttachmentQueryService attachmentQueryService) {
		this.attachmentQueryService = attachmentQueryService;
	}
	
	@Autowired
	void setFileAttachmentService(FileAttachmentService fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}

	/**
	 * Compare IT0002 requested payload with an existing IT0002 in the database.
	 * Update the existing IT0002 data with the latest data comes from the requested payload. 
	 * 
	 * @param requestPayload requested data 
	 * @param it0002DB current existing IT0002 in the database.
	 * @return updated IT0002 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0002, IT0002RequestWrapper, String> compareAndReturnUpdatedData(IT0002RequestWrapper requestPayload, IT0002 it0002DB) {
		
			RequestObjectComparatorContainer<IT0002, IT0002RequestWrapper, String> objectContainer = null; 
			
		if (it0002DB == null) {
			it0002DB = new IT0002();
			
			try {
				Optional<IT0002> existingIT0002 = it0002QueryService.findOneByCompositeKey(
						requestPayload.getPernr(),
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda()));
				
				if (existingIT0002.isPresent()) {
					throw new DataViolationException("Duplicate IT0002 Data for [ssn=" + requestPayload.getPernr() + ",end_date=" +
							requestPayload.getEndda() + ",begin_date=" + requestPayload.getBegda());
				}
				
				it0002DB.setId(	new ITCompositeKeysNoSubtype(requestPayload.getPernr(), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda())));
				
				
				it0002DB.setCname(requestPayload.getCname());
				it0002DB.setGbort(requestPayload.getGbort());
				it0002DB.setGesch(requestPayload.getGesch());
				it0002DB.setKnznm(requestPayload.getKnznm());
				it0002DB.setNachn(requestPayload.getNachn());
				it0002DB.setRufnm(requestPayload.getRufnm());
				it0002DB.setSeqnr(1L);
			    it0002DB.setGbdat(!StringUtils.isEmpty(requestPayload.getGbdat()) ? CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getGbdat()) : null);
			    it0002DB.setFamdt(!StringUtils.isEmpty(requestPayload.getFamdt()) ? CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getFamdt()) : null);
				
				Optional<T522G> bnka = t522gQueryService.findById(Optional.ofNullable(requestPayload.getAnred()));				
				it0002DB.setAnred(bnka.isPresent() ? bnka.get() : null);
				Optional<T502T> bnka1 = t502tQueryService.findById(Optional.ofNullable(requestPayload.getFamst()));				
				it0002DB.setFamst(bnka1.isPresent() ? bnka1.get() : null);

				//Optional<T502T> bnk = t502tQueryService.findById(Optional.ofNullable(requestPayload.getFamdt()));				
				//it0002DB.setFamdt(bnk.isPresent() ? CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getFamdt()) : null);
				
				
				Optional<T005T> bnka2 = t005tQueryService.findById(Optional.ofNullable(requestPayload.getGblnd()));				
				it0002DB.setGblnd(bnka2.isPresent() ? bnka2.get() : null);
				Optional<T516T> bnka3 = t516tQueryService.findById(Optional.ofNullable(requestPayload.getKonfe()));				
				it0002DB.setKonfe(bnka3.isPresent() ? bnka3.get() : null);
				//Optional<T535N> bnka4 = t535nQueryService.findById(Optional.ofNullable(requestPayload.getNamzu()));				
				//it0002DB.setNamzu(bnka4.isPresent() ? bnka4.get() : null);
				
				Optional<T005T> bnka5 = t005tQueryService.findById(Optional.ofNullable(requestPayload.getNatio()));				
				it0002DB.setNatio(bnka5.isPresent() ? bnka5.get() : null);
				
				
				Optional<T002T> bnka6 = t002tQueryService.findById(Optional.ofNullable(requestPayload.getSprsl()));				
				it0002DB.setSprsl(bnka6.isPresent() ? bnka6.get() : null);
				//Optional<T535N> bnka7 = t535nQueryService.findById(Optional.ofNullable(requestPayload.getTitle2()));				
				//it0002DB.setTitl2(bnka7.isPresent() ? bnka7.get() : null);
				Optional<T535N> title = t535nQueryService.findById(Optional.ofNullable(new T535NKey(requestPayload.getArt(), requestPayload.getTitle())));				
				it0002DB.setTitle(title.isPresent() ? title.get() : null);
				Optional<T535N> title2 = t535nQueryService.findById(Optional.ofNullable(new T535NKey(requestPayload.getArt2(), requestPayload.getTitle2())));				
				it0002DB.setTitl2(title2.isPresent() ? title2.get() : null);

				it0002DB.setStatus(DocumentStatus.INITIAL_DRAFT);
				
				it0002DB = saveAttachment(it0002DB, requestPayload);
				
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			
			objectContainer = new RequestObjectComparatorContainer<>(it0002DB, requestPayload);
			
		} else {
			
			if (StringUtils.isNotBlank(requestPayload.getNachn())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getNachn(), it0002DB.getNachn())) {
					it0002DB.setNachn(requestPayload.getNachn());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getCname())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getCname(), it0002DB.getCname())) {
					it0002DB.setCname(requestPayload.getCname());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getArt()) && StringUtils.isNotBlank(requestPayload.getTitle())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getArt(), it0002DB.getTitle() != null ? it0002DB.getTitle().getId().getArt() : StringUtils.EMPTY)
						&&
					CommonComparatorFunction.isDifferentStringValues(requestPayload.getTitle(),  it0002DB.getTitle() != null ? it0002DB.getTitle().getId().getTitle() : StringUtils.EMPTY)) {
					try {
						Optional<T535N> newT535N = t535nQueryService.findById(Optional.ofNullable(new T535NKey(requestPayload.getArt(), requestPayload.getTitle())));
						if (newT535N.isPresent()) {
							it0002DB.setTitle(newT535N.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T535N not found... cancel the update process
					}
				}
			}

			if (StringUtils.isNotBlank(requestPayload.getArt2()) && StringUtils.isNotBlank(requestPayload.getTitle2())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getArt2(), it0002DB.getTitl2() != null ? it0002DB.getTitl2().getId().getArt() : StringUtils.EMPTY)
						&&
					CommonComparatorFunction.isDifferentStringValues(requestPayload.getTitle2(),  it0002DB.getTitl2() != null ? it0002DB.getTitl2().getId().getTitle() : StringUtils.EMPTY)) {
					try {
						Optional<T535N> newT535N = t535nQueryService.findById(Optional.ofNullable(new T535NKey(requestPayload.getArt2(), requestPayload.getTitle2())));
						if (newT535N.isPresent()) {
							it0002DB.setTitl2(newT535N.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T535N not found... cancel the update process
					}
				}
			}			
			
			if (StringUtils.isNotBlank(requestPayload.getArt3()) && StringUtils.isNotBlank(requestPayload.getTitle3())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getArt3(), it0002DB.getNamzu() != null ? it0002DB.getNamzu().getId().getArt() : StringUtils.EMPTY)
						&&
					CommonComparatorFunction.isDifferentStringValues(requestPayload.getTitle3(),  it0002DB.getNamzu() != null ? it0002DB.getNamzu().getId().getTitle() : StringUtils.EMPTY)) {
					try {
						Optional<T535N> newT535N = t535nQueryService.findById(Optional.ofNullable(new T535NKey(requestPayload.getArt3(), requestPayload.getTitle3())));
						if (newT535N.isPresent()) {
							it0002DB.setNamzu(newT535N.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T535N not found... cancel the update process
					}
				}
			}			
			
			if (StringUtils.isNotBlank(requestPayload.getRufnm())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getRufnm(), it0002DB.getRufnm())) {
					it0002DB.setRufnm(requestPayload.getRufnm());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getKnznm())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getKnznm(), it0002DB.getKnznm())) {
					it0002DB.setKnznm(requestPayload.getKnznm());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getAnred())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAnred(), it0002DB.getAnred().getAnred())) {
					try {
						Optional<T522G> newT522G = t522gQueryService.findById(Optional.ofNullable(requestPayload.getAnred()));
						if (newT522G.isPresent()) {
							it0002DB.setAnred(newT522G.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T522G not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getGesch())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getGesch(), it0002DB.getGesch())) {
					it0002DB.setGesch(requestPayload.getGesch());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getGbdat())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getGbdat()), it0002DB.getGbdat())) {
					it0002DB.setGbdat(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getGbdat()));
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getGblnd())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getGblnd(), it0002DB.getGblnd() != null ? it0002DB.getGblnd().getLand1() : StringUtils.EMPTY)) {
					try {
						Optional<T005T> newT005T = t005tQueryService.findById(Optional.ofNullable(requestPayload.getGblnd()));
						if (newT005T.isPresent()) {
							it0002DB.setGblnd(newT005T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T005T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getGbort())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getGbort(), it0002DB.getGbort())) {
					it0002DB.setGbort(requestPayload.getGbort());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getSprsl())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getSprsl(), it0002DB.getSprsl() != null ? it0002DB.getSprsl().getSprsl() : StringUtils.EMPTY)) {
					try {
						Optional<T002T> newT002T = t002tQueryService.findById(Optional.ofNullable(requestPayload.getSprsl()));
						if (newT002T.isPresent()) {
							it0002DB.setSprsl(newT002T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T002T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getKonfe())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getKonfe(), it0002DB.getKonfe() != null ? it0002DB.getKonfe().getKonfe() : StringUtils.EMPTY)) {
					try {
						Optional<T516T> newT516T = t516tQueryService.findById(Optional.ofNullable(requestPayload.getKonfe()));
						if (newT516T.isPresent()) {
							it0002DB.setKonfe(newT516T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T516T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getFamst())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getFamst(), it0002DB.getFamst() != null ? it0002DB.getFamst().getFamst() : StringUtils.EMPTY)) {
					try {
						Optional<T502T> newT502T = t502tQueryService.findById(Optional.ofNullable(requestPayload.getFamst()));
						if (newT502T.isPresent()) {
							it0002DB.setFamst(newT502T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T502T not found.. cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getFamdt())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getFamdt()), it0002DB.getFamdt())) {
					it0002DB.setFamdt(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getFamdt()));
					requestPayload.setDataChanged(true);
				}
			}
			
			if (requestPayload.getAnzkd() != 0) {
				if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getAnzkd(), it0002DB.getAnzkd() != null ? it0002DB.getAnzkd().byteValue() : 0)) {
					it0002DB.setAnzkd(requestPayload.getAnzkd());
					requestPayload.setDataChanged(true);
				}
			}
			
			it0002DB.setSeqnr(it0002DB.getSeqnr().longValue() + 1);
			try {
				it0002DB = saveAttachment(it0002DB, requestPayload);
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			it0002DB.setSeqnr(it0002DB.getSeqnr().longValue() - 1);
			
			objectContainer = new RequestObjectComparatorContainer<>(it0002DB, requestPayload);			
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = "";
		try {
			jsonString = objectMapper.writeValueAsString(requestPayload);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}		
		
		//JSONObject json = new JSONObject(requestPayload);
		JSONObject json = new JSONObject(jsonString);
		// Adjust attachment fields on EventData Json
		json.remove("attachment1_type");
		json.remove("attachment1_base64");
		json.remove("attachment1_filename");
		json.remove("subtype");
		if (it0002DB.getAttachment() != null)
			json.put("attachment1_type", it0002DB.getAttachment().getId().getSubty());
		if (it0002DB.getAttachmentPath() != null)
		    json.put("attachment1_path", it0002DB.getAttachmentPath());
		// ******************************************
		
		//if (it0002DB.getStatus() != null)
			//json.put("prev_document_status", it0002DB.getStatus().name());
		
		objectContainer.setEventData(json.toString());
		
		// if Anred (Form-of-Address) is null, set Anred value based on Gender and Marrital Status
		if (objectContainer.getEntity().isPresent()) {
			IT0002 it0002 = objectContainer.getEntity().get();
			if (it0002.getAnred() == null) {
				String anred = StringUtils.defaultString(it0002.getGesch(), "1");
				if ("2".equals(anred)) {
					if (it0002.getFamst() == null || it0002.getFamst().getFamst().equals("0"))
						anred = "3";
				}
				Optional<T522G> t522g;
				try {
					t522g = t522gQueryService.findById(Optional.ofNullable(anred));
					if (t522g.isPresent()) {
						it0002.setAnred(t522g.get());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		return objectContainer;
	}
	
	private IT0002 saveAttachment(IT0002 it0002DB, IT0002RequestWrapper requestPayload) throws Exception {
		
		T591SKey attachmentKey = new T591SKey(SAPInfoType.PERSONAL_DATA.infoType(), requestPayload.getAttach1Type());
		Optional<Attachment> attachment = attachmentQueryService.findById(Optional.ofNullable(attachmentKey));
		it0002DB.setAttachment(attachment.isPresent() ? attachment.get() : null);
		String base64 = requestPayload.getAttach1Content();
		String filename = requestPayload.getAttach1Filename();
		if (StringUtils.isNotEmpty(base64) && StringUtils.isNotEmpty(filename)) {
			String ext = filename.split("\\.")[(filename.split("\\.")).length-1];
			String formatFilename = it0002DB.getId().toStringId(SAPInfoType.PERSONAL_DATA.infoType()).
					concat("_").concat(it0002DB.getSeqnr().toString()).concat(".").concat(ext);
		    String path = fileAttachmentService.save(formatFilename, base64);
		    it0002DB.setAttachmentPath(path);
		    requestPayload.setDataChanged(true);
		}
		
		return it0002DB;
		
	}
}