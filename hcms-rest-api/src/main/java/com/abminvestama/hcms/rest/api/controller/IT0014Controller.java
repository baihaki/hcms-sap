package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0014;
import com.abminvestama.hcms.core.model.entity.IT0014;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0014CommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0014QueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.helper.IT0014SoapObject;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0014RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.IT0014ResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.IT0014RequestBuilderUtil;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@RestController
@RequestMapping("/api/v1/recurring_payments")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class IT0014Controller extends AbstractResource {

	private IT0014CommandService it0014CommandService;
	private IT0014QueryService it0014QueryService;
	

	private HCMSEntityQueryService hcmsEntityQueryService;
	private EventLogCommandService eventLogCommandService;
	
	private UserQueryService userQueryService;
	
	private IT0014RequestBuilderUtil it0014RequestBuilderUtil;
	
	private Validator itValidator;
	

	private MuleConsumerService muleConsumerService;
	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	
	
	
	@Autowired
	void setIT0014CommandService(IT0014CommandService it0014CommandService) {
		this.it0014CommandService = it0014CommandService;
	}
	
	@Autowired
	void setIT0014QueryService(IT0014QueryService it0014QueryService) {
		this.it0014QueryService = it0014QueryService;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	void setIT0014RequestBuilderUtil(IT0014RequestBuilderUtil it0014RequestBuilderUtil) {
		this.it0014RequestBuilderUtil = it0014RequestBuilderUtil;
	}
	
	@Autowired
	@Qualifier("itValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0014ResponseWrapper>> findOneByPernr(@PathVariable String pernr) throws Exception {
		ArrayData<IT0014ResponseWrapper> bunchOfIT0014 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0014ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0014);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0014> listOfIT0014 = it0014QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			
			if (listOfIT0014.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0014ResponseWrapper> records = new ArrayList<>();
				for (IT0014 it0014 : listOfIT0014) {
					records.add(new IT0014ResponseWrapper(it0014));
				}
				bunchOfIT0014.setItems(records.toArray(new IT0014ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0014Controller.class).findOneByPernr(pernr)).withSelfRel());
		return response;
	}	
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated IT0014RequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			IT0014ResponseWrapper responseWrapper = new IT0014ResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<HCMSEntity> it0014Entity = hcmsEntityQueryService.findByEntityName(IT0014.class.getSimpleName());
					if (!it0014Entity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + IT0014.class.getSimpleName() + "' !!!");
					}	
					
					
					
					Optional<IT0014> it0014 = it0014QueryService.findOneByCompositeKey(request.getPernr(), request.getSubty(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
					if (!it0014.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					RequestObjectComparatorContainer<IT0014, IT0014RequestWrapper, String> updatedContainer = it0014RequestBuilderUtil.compareAndReturnUpdatedData(request, it0014.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
					if (updatedContainer.getEntity().isPresent()) {
						updatedContainer.getEntity().get().setSeqnr(it0014.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
						it0014 = it0014CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}
				
					if (!it0014.isPresent()) {
						throw new CannotPersistException("Cannot update IT0014 (Recurring Payment Deductions) data. Please check your data!");
					}
					
					
					StringBuilder key = new StringBuilder("pernr=" + it0014.get().getId().getPernr());
					key.append(",infty=" + it0014.get().getId().getInfty());
					key.append(",subty=" + it0014.get().getId().getSubty());
					key.append(",endda=" + it0014.get().getId().getEndda());
					key.append(",begda=" + it0014.get().getId().getBegda());
					EventLog eventLog = new EventLog(it0014Entity.get(), key.toString(), OperationType.UPDATE, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					

					//IT0014SoapObject it0014SoapObject = new IT0014SoapObject(updatedContainer.get()); 
					//muleConsumerService.postToSAP(BAPIFunction.IT0014_CREATE.service(), "recurringPayments", it0014SoapObject);
					
				
					responseWrapper = new IT0014ResponseWrapper(it0014.get());
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(IT0014Controller.class).edit(request, result)).withSelfRel());
		return response;
	}		
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(itValidator);
	}	
}