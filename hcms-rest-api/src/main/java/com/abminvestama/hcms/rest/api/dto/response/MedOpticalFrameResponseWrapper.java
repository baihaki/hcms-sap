package com.abminvestama.hcms.rest.api.dto.response;


import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class MedOpticalFrameResponseWrapper extends ResourceSupport {

	private String quotamt;
	
	private MedOpticalFrameResponseWrapper() {}
	
	public MedOpticalFrameResponseWrapper(ZmedEmpquota zmed) {
		if (zmed == null) {
			new MedOpticalFrameResponseWrapper();
		} else {
			this
			.setQuotamt(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY));			
		}
	}
	
	public MedOpticalFrameResponseWrapper(ZmedEmpquotasm zmed) {
		if (zmed == null) {
			new MedOpticalFrameResponseWrapper();
		} else {
			this
			.setQuotamt(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY));			
		}
	}
	
	public MedOpticalFrameResponseWrapper(ZmedEmpquotastf zmed) {
		if (zmed == null) {
			new MedOpticalFrameResponseWrapper();
		} else {
			this
			.setQuotamt(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY));			
		}
	}
	
	
	@JsonProperty("quotamt")
	public String getQuotamt() {
		return quotamt;
	}
	
	private MedOpticalFrameResponseWrapper setQuotamt(String quotamt) {
		this.quotamt = quotamt;
		return this;
	}
}
