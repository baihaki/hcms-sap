package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0041;
import com.abminvestama.hcms.core.model.entity.T548T;
import com.abminvestama.hcms.core.service.api.business.query.T548TQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0041RequestWrapper;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Component("it0041RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT0041RequestBuilderUtil {

	private T548TQueryService t548tQueryService;
	
	@Autowired
	void setT548TQueryService(T548TQueryService t548tQueryService) {
		this.t548tQueryService = t548tQueryService;
	}
	
	/**
	 * Compare IT0041 requested payload with an existing IT0041 in the database.
	 * Update the existing IT0041 data with the latest data comes from the requested payload. 
	 * 
	 * @param requestPayload requested data 
	 * @param it0041DB current existing IT0041 in the database.
	 * @return updated IT0041 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0041, IT0041RequestWrapper, String> compareAndReturnUpdatedData(IT0041RequestWrapper requestPayload, IT0041 it0041DB) {
		if (it0041DB == null) {
			it0041DB = new IT0041();
		} else {
			
			if (StringUtils.isNotBlank(requestPayload.getDar01())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDar01(), it0041DB.getDar01() != null ? it0041DB.getDar01().getDatar() : StringUtils.EMPTY)) {
					try {
						Optional<T548T> newT548T = t548tQueryService.findById(Optional.ofNullable(requestPayload.getDar01().trim()));
						if (newT548T.isPresent()) {
							it0041DB.setDar01(newT548T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T548T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDat01())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat01()), it0041DB.getDat01())) {
					it0041DB.setDat01(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat01()));
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDar02())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDar02(), it0041DB.getDar02() != null ? it0041DB.getDar02().getDatar() : StringUtils.EMPTY)) {
					try {
						Optional<T548T> newT548T = t548tQueryService.findById(Optional.ofNullable(requestPayload.getDar02().trim()));
						if (newT548T.isPresent()) {
							it0041DB.setDar02(newT548T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T548T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDat02())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat02()), it0041DB.getDat02())) {
					it0041DB.setDat02(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat02()));
					requestPayload.setDataChanged(true);
				}
			}

			if (StringUtils.isNotBlank(requestPayload.getDar03())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDar01(), it0041DB.getDar03() != null ? it0041DB.getDar03().getDatar() : StringUtils.EMPTY)) {
					try {
						Optional<T548T> newT548T = t548tQueryService.findById(Optional.ofNullable(requestPayload.getDar03().trim()));
						if (newT548T.isPresent()) {
							it0041DB.setDar03(newT548T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T548T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDat03())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat03()), it0041DB.getDat03())) {
					it0041DB.setDat03(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat03()));
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDar04())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDar04(), it0041DB.getDar04() != null ? it0041DB.getDar04().getDatar() : StringUtils.EMPTY)) {
					try {
						Optional<T548T> newT548T = t548tQueryService.findById(Optional.ofNullable(requestPayload.getDar04().trim()));
						if (newT548T.isPresent()) {
							it0041DB.setDar04(newT548T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T548T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDat03())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat04()), it0041DB.getDat04())) {
					it0041DB.setDat04(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat04()));
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDar05())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDar05(), it0041DB.getDar05() != null ? it0041DB.getDar05().getDatar() : StringUtils.EMPTY)) {
					try {
						Optional<T548T> newT548T = t548tQueryService.findById(Optional.ofNullable(requestPayload.getDar05().trim()));
						if (newT548T.isPresent()) {
							it0041DB.setDar05(newT548T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T548T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDat05())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat05()), it0041DB.getDat05())) {
					it0041DB.setDat05(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat05()));
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDar06())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDar06(), it0041DB.getDar06() != null ? it0041DB.getDar06().getDatar() : StringUtils.EMPTY)) {
					try {
						Optional<T548T> newT548T = t548tQueryService.findById(Optional.ofNullable(requestPayload.getDar06().trim()));
						if (newT548T.isPresent()) {
							it0041DB.setDar06(newT548T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T548T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDat06())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat06()), it0041DB.getDat06())) {
					it0041DB.setDat06(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat06()));
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDar07())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDar07(), it0041DB.getDar07() != null ? it0041DB.getDar07().getDatar() : StringUtils.EMPTY)) {
					try {
						Optional<T548T> newT548T = t548tQueryService.findById(Optional.ofNullable(requestPayload.getDar07().trim()));
						if (newT548T.isPresent()) {
							it0041DB.setDar07(newT548T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T548T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDat07())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat07()), it0041DB.getDat07())) {
					it0041DB.setDat07(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat07()));
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDar08())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDar08(), it0041DB.getDar08() != null ? it0041DB.getDar08().getDatar() : StringUtils.EMPTY)) {
					try {
						Optional<T548T> newT548T = t548tQueryService.findById(Optional.ofNullable(requestPayload.getDar08().trim()));
						if (newT548T.isPresent()) {
							it0041DB.setDar08(newT548T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T548T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDat08())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat08()), it0041DB.getDat08())) {
					it0041DB.setDat08(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat08()));
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDar09())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDar09(), it0041DB.getDar09() != null ? it0041DB.getDar09().getDatar() : StringUtils.EMPTY)) {
					try {
						Optional<T548T> newT548T = t548tQueryService.findById(Optional.ofNullable(requestPayload.getDar09().trim()));
						if (newT548T.isPresent()) {
							it0041DB.setDar09(newT548T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T548T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDat09())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat09()), it0041DB.getDat09())) {
					it0041DB.setDat09(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat09()));
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDar10())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDar10(), it0041DB.getDar10() != null ? it0041DB.getDar10().getDatar() : StringUtils.EMPTY)) {
					try {
						Optional<T548T> newT548T = t548tQueryService.findById(Optional.ofNullable(requestPayload.getDar10().trim()));
						if (newT548T.isPresent()) {
							it0041DB.setDar10(newT548T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T548T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDat10())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat10()), it0041DB.getDat10())) {
					it0041DB.setDat10(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat10()));
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDar11())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDar11(), it0041DB.getDar11() != null ? it0041DB.getDar11().getDatar() : StringUtils.EMPTY)) {
					try {
						Optional<T548T> newT548T = t548tQueryService.findById(Optional.ofNullable(requestPayload.getDar11().trim()));
						if (newT548T.isPresent()) {
							it0041DB.setDar11(newT548T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T548T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDat11())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat11()), it0041DB.getDat11())) {
					it0041DB.setDat11(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat11()));
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDar12())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDar12(), it0041DB.getDar12() != null ? it0041DB.getDar12().getDatar() : StringUtils.EMPTY)) {
					try {
						Optional<T548T> newT548T = t548tQueryService.findById(Optional.ofNullable(requestPayload.getDar12().trim()));
						if (newT548T.isPresent()) {
							it0041DB.setDar12(newT548T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T548T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDat12())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat12()), it0041DB.getDat12())) {
					it0041DB.setDat12(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDat12()));
					requestPayload.setDataChanged(true);
				}
			}			
			
		}
		
		return new RequestObjectComparatorContainer<>(it0041DB, requestPayload);
	}								
}