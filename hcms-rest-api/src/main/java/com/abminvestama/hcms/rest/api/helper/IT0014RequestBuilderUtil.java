package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.core.model.entity.IT0014;
import com.abminvestama.hcms.core.model.entity.T512T;
import com.abminvestama.hcms.core.service.api.business.query.T512TQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0014RequestWrapper;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Component("it0014RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT0014RequestBuilderUtil {
	
	private T512TQueryService t512tQueryService;
	
	@Autowired
	void setT512TQueryService(T512TQueryService t512tQueryService) {
		this.t512tQueryService = t512tQueryService;
	}

	/**
	 * Compare IT0014 request payload with existing IT0014 in the database.
	 * Update existing IT0014 data with the latest data comes from the request payload. 
	 * 
	 * @param requestPayload request data 
	 * @param it0014DB current existing IT0014 in the database.
	 * @return updated IT0014 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0014, IT0014RequestWrapper, String> compareAndReturnUpdatedData(IT0014RequestWrapper requestPayload, IT0014 it0014DB) {
		if (it0014DB == null) {
			it0014DB = new IT0014();
		} else {
			if (requestPayload.getLgart() != null) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getLgart(), it0014DB.getLgart().getLgart())) {
					try {
						Optional<T512T> newT512T = t512tQueryService.findById(Optional.ofNullable(requestPayload.getLgart()));
						if (newT512T.isPresent()) {
							it0014DB.setLgart(newT512T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						// t512t not found... ignore change
					}
				}
			}
			if (requestPayload.getOpken() != null) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getOpken(), it0014DB.getOpken())) {
					it0014DB.setOpken(requestPayload.getOpken());
					requestPayload.setDataChanged(true);
				}
			}
			if (requestPayload.getBetrg() != (it0014DB.getBetrg() != null ? it0014DB.getBetrg().doubleValue() : 0.0)) {
				it0014DB.setBetrg(requestPayload.getBetrg());
				requestPayload.setDataChanged(true);
			}			
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getWaers(), it0014DB.getWaers())) {
				it0014DB.setWaers(StringUtils.defaultString(requestPayload.getWaers()));
				requestPayload.setDataChanged(true);
			}
		}
		
		return new RequestObjectComparatorContainer<>(it0014DB, requestPayload);
	}				
}