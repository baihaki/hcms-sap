package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class IT0014RequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = 2475738723683829103L;
	
	private String lgart;
	private String opken;
	private double betrg;
	private String waers;
	
	/**
	 * GET Wage Type Code.
	 * 
	 * @return
	 */
	@JsonProperty("wage_type")
	public String getLgart() {
		return lgart;
	}
	
	/**
	 * GET Operation Indicator.
	 * 
	 * @return
	 */
	@JsonProperty("operation_indicator")
	public String getOpken() {
		return opken;
	}
	
	/**
	 * GET Amount.
	 * 
	 * @return
	 */
	@JsonProperty("amount")
	public double getBetrg() {
		return betrg;
	}
	
	/**
	 * GET Currency.
	 * 
	 * @return
	 */
	@JsonProperty("currency")
	public String getWaers() {
		return waers;
	}
}