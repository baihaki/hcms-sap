package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.exception.WorkflowNotFoundException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0241;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0241CommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0241QueryService;
import com.abminvestama.hcms.core.service.api.business.query.ITQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.helper.IT0241SoapObject;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0241RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ApprovalEventLogResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.IT0241ResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.IT0241RequestBuilderUtil;

/**
 * 
 * @since 1.0.0
 * @version 1.0.6
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Revert to 1.0.2 (remove approval on edit)</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add methods: findByCriteria</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: approve, reject, getUpdatedFields</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Modify edit method: update document only change the status</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Modify findOneByPernr into findByPernr method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add create, findByDocumentStatus method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/tax_info")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class IT0241Controller extends AbstractResource {

	private IT0241CommandService it0241CommandService;
	private IT0241QueryService it0241QueryService;
	
	private IT0241RequestBuilderUtil it0241RequestBuilderUtil;
	
	private Validator itValidator;
	
	private UserQueryService userQueryService;
	
	private HCMSEntityQueryService hcmsEntityQueryService;
	private EventLogCommandService eventLogCommandService;

	private MuleConsumerService muleConsumerService;
	private CommonServiceFactory commonServiceFactory;
	private ApprovalController approvalController;
	private ITQueryService itQueryService;
	
	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}
	
	@Autowired
	void setIT0241CommandService(IT0241CommandService it0241CommandService) {
		this.it0241CommandService = it0241CommandService;
	}
	
	@Autowired
	void setIT0241QueryService(IT0241QueryService it0241QueryService) {
		this.it0241QueryService = it0241QueryService;
	}
	
	@Autowired
	void setIT0241RequestBuilderUtil(IT0241RequestBuilderUtil it0241RequestBuilderUtil) {
		this.it0241RequestBuilderUtil = it0241RequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	@Autowired
	void setApprovalController(ApprovalController approvalController) {
		this.approvalController = approvalController;
	}
	
	@Autowired
	void setITQueryService(ITQueryService itQueryService) {
		this.itQueryService = itQueryService;
	}
	
	@SuppressWarnings("unchecked")
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_criteria", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<IT0241ResponseWrapper>> findByCriteria(
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "exclude_status", required = false, defaultValue="PUBLISHED,DRAFT_REJECTED,UPDATE_REJECTED") String excludeStatus,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {		

		Page<IT0241ResponseWrapper> responsePage = new PageImpl<IT0241ResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<IT0241ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<IT0241> resultPage = null;
		String bukrs = null;
		Long pernr = null;
		String[] excludes = excludeStatus.split(",");
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		
		if (StringUtils.isNotEmpty(ssn)) {
			pernr = Long.valueOf(ssn);
			if (!currentUser.get().getRoles().contains(adminRole.get()) &&
					!currentUser.get().getEmployee().getPernr().equals(pernr) ) {
				throw new AuthorizationException(
						"Insufficient Privileges. You can't fetch data of other Employee.");
			}
		} else {
			// no specified ssn, MUST BE ADMIN
			if (!currentUser.get().getRoles().contains(adminRole.get())) {
				throw new AuthorizationException(
						"Insufficient Privileges. Please login as 'ADMIN'!");
			}
			bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
		}
		
		try {
			resultPage = (Page<IT0241>) itQueryService.fetchDistinctByCriteriaWithPaging(pageNumber, bukrs, pernr, excludes,
					new IT0241(), currentUser.get().getEmployee());
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			
			if (resultPage.hasContent()) {

				List<IT0241ResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(it0241 -> {
					records.add(new IT0241ResponseWrapper(it0241));
				});
				responsePage = new PageImpl<IT0241ResponseWrapper>(records, it0241QueryService.createPageRequest(pageNumber),
						resultPage.getTotalElements());
				response.setData(responsePage);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0241Controller.class).findByCriteria(ssn, excludeStatus, pageNumber)).withSelfRel());
		
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.PUT, value = "/approve")
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> approve(
			@RequestHeader(required = true, value = "ssn") String pernr,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda)
					throws AuthorizationException, NumberFormatException, CannotPersistException,
					DataViolationException, EntityNotFoundException, WorkflowNotFoundException, Exception {
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", pernr);
			keyMap.put("infotype", SAPInfoType.TAX_DATA.infoType());
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			keyMap.put("muleService", BAPIFunction.IT0241_CREATE.service());
			keyMap.put("muleUri", BAPIFunction.IT0241_CREATE_URI.service());
			
			String className = IT0241.class.getSimpleName();
			response = approvalController.approve(keyMap, className, it0241QueryService, it0241CommandService,
					IT0241SoapObject.class, it0241RequestBuilderUtil, new IT0241RequestWrapper());

		} catch (EntityNotFoundException e) {
			throw e;
		} catch (WorkflowNotFoundException e) {
			throw e;
		} catch (DataViolationException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0241Controller.class).approve(pernr, endda, begda)).withSelfRel());				
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.PUT, value = "/reject")
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> reject(
			@RequestHeader(required = true, value = "ssn") String pernr,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda) 
					throws AuthorizationException, NumberFormatException, CannotPersistException, 
					DataViolationException, EntityNotFoundException, Exception {
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", pernr);
			keyMap.put("infotype", SAPInfoType.TAX_DATA.infoType());
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			
			String className = IT0241.class.getSimpleName();
			response = approvalController.reject(keyMap, className, it0241QueryService, it0241CommandService);
			
		} catch (AuthorizationException e) { 
			throw e;
		} catch (NumberFormatException e) { 
			throw e;
		} catch (CannotPersistException e) {
			throw e;
		} catch (DataViolationException e) {
			throw e;
		} catch (EntityNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0241Controller.class).reject(pernr, endda, begda)).withSelfRel());		
		return response;
	}
	
	@SuppressWarnings("unchecked")
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_updated_fields")
	public APIResponseWrapper<IT0241ResponseWrapper> getUpdatedFields(
			@RequestHeader(required = true, value = "ssn") String ssn,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda
			) throws DataViolationException, EntityNotFoundException, Exception {

		APIResponseWrapper<IT0241ResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", ssn);
			keyMap.put("infotype", SAPInfoType.TAX_DATA.infoType());
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			
			String className = IT0241.class.getSimpleName();
			response = (APIResponseWrapper<IT0241ResponseWrapper>) approvalController.getUpdatedFields(
					keyMap, className, it0241QueryService, response, IT0241ResponseWrapper.class);
			
		} catch (DataViolationException e) { 
			throw e;
		} catch (EntityNotFoundException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0241Controller.class).getUpdatedFields(ssn, endda, begda)).withSelfRel());
		return response;
	}
	
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/status/{status}/page/{pageNumber}", 
	produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0241ResponseWrapper>> findByDocumentStatus(@PathVariable String status, 
			@PathVariable int pageNumber) throws Exception {
		ArrayData<IT0241ResponseWrapper> bunchOfIT0241 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0241ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0241);
		
		try {
			Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
			
			Page<IT0241> it0241Records = it0241QueryService.findByStatus(status, pageNumber, currentUser.get().getEmployee());
			
			if (it0241Records == null || !it0241Records.hasContent()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0241ResponseWrapper> records = new ArrayList<>();
				
				it0241Records.forEach(it0241 -> {
					records.add(new IT0241ResponseWrapper(it0241));
				});
				
				bunchOfIT0241.setItems(records.toArray(new IT0241ResponseWrapper[records.size()]));
			}
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0241Controller.class).findByDocumentStatus(status, pageNumber)).withSelfRel());
		
		return response;
	}
		
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0241ResponseWrapper>> findByPernr(@PathVariable String pernr, 
			@RequestParam(value = "status", required = false) String status, 
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		ArrayData<IT0241ResponseWrapper> bunchOfIT0241 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0241ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0241);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0241> listOfIT0241 = new ArrayList<>();
			
			if (StringUtils.isNotEmpty(status)) {
				Page<IT0241> data = it0241QueryService.findByPernrAndStatus(numOfPernr, status, pageNumber);
				if (data.hasContent()) {
					listOfIT0241 = data.getContent();
				}
			} else {
				listOfIT0241 = it0241QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			}
			
			if (listOfIT0241.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0241ResponseWrapper> records = new ArrayList<>();
				for (IT0241 it0241 : listOfIT0241) {
					records.add(new IT0241ResponseWrapper(it0241));
				}
				bunchOfIT0241.setItems(records.toArray(new IT0241ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0241Controller.class).findByPernr(pernr, status, pageNumber)).withSelfRel());
		return response;
	}
	/*
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0241ResponseWrapper>> findOneByPernr(@PathVariable String pernr) throws Exception {
		ArrayData<IT0241ResponseWrapper> bunchOfIT0241 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0241ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0241);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0241> listOfIT0241 = it0241QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			
			if (listOfIT0241.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				
				List<IT0241ResponseWrapper> records = new ArrayList<>();
				for (IT0241 it0241 : listOfIT0241) {
					records.add(new IT0241ResponseWrapper(it0241));
				}				
				bunchOfIT0241.setItems(records.toArray(new IT0241ResponseWrapper[records.size()]));				
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0241Controller.class).findOneByPernr(pernr)).withSelfRel());
		return response;
	}		
	*/
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated IT0241RequestWrapper request, BindingResult result)
			throws CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> it0241Entity = hcmsEntityQueryService.findByEntityName(IT0241.class.getSimpleName());
			if (!it0241Entity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + IT0241.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<IT0241, IT0241RequestWrapper, String> newObjectContainer 
					= it0241RequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				/*
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				*/
				
				Optional<IT0241> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
					newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = it0241CommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create IT0241 (Tax Info) data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("pernr=" + savedEntity.get().getId().getPernr());
				key.append(",endda=" + CommonDateFunction.convertDateToStringYMD(savedEntity.get().getId().getEndda()));
				key.append(",begda=" + CommonDateFunction.convertDateToStringYMD(savedEntity.get().getId().getBegda()));
				EventLog eventLog = new EventLog(it0241Entity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}

				//IT0241SoapObject it0241SoapObject = new IT0241SoapObject(savedEntity.get()); 
				//muleConsumerService.postToSAP(BAPIFunction.IT0241_CREATE.service(), BAPIFunction.IT0241_CREATE_URI.service(), it0241SoapObject);
				
				IT0241ResponseWrapper responseWrapper = new IT0241ResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;	
			} /* catch (AuthorizationException e) { 
				throw e;
			}*/ catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(IT0241Controller.class).create(request, result)).withSelfRel());		
		return response;
	}

	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated IT0241RequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			IT0241ResponseWrapper responseWrapper = new IT0241ResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<HCMSEntity> it0241Entity = hcmsEntityQueryService.findByEntityName(IT0241.class.getSimpleName());
					if (!it0241Entity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + IT0241.class.getSimpleName() + "' !!!");
					}
					
					Optional<IT0241> it0241 = it0241QueryService.findOneByCompositeKey(request.getPernr(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
					if (!it0241.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					/*
					// since version 1.0.3
					IT0241 currentIT0241 = new IT0241();
					BeanUtils.copyProperties(it0241.get(), currentIT0241);
					*/
				
					RequestObjectComparatorContainer<IT0241, IT0241RequestWrapper, String> updatedContainer 
						= it0241RequestBuilderUtil.compareAndReturnUpdatedData(request, it0241.get());
				        /*
					    // since version 1.0.3
				        = it0241RequestBuilderUtil.compareAndReturnUpdatedData(request, currentIT0241);
				        */

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
					if (updatedContainer.getEntity().isPresent()) {
						updatedContainer.getEntity().get().setSeqnr(it0241.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
						it0241 = it0241CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						/*
						// since version 1.0.3, update document only change the status 
						if (it0241.get().getStatus() == DocumentStatus.INITIAL_DRAFT) {
							it0241.get().setStatus(DocumentStatus.UPDATED_DRAFT);
							currentIT0241.setStatus(DocumentStatus.UPDATED_DRAFT);
						} else if (it0241.get().getStatus() == DocumentStatus.PUBLISHED) { 
							it0241.get().setStatus(DocumentStatus.UPDATE_IN_PROGRESS);
							currentIT0241.setStatus(DocumentStatus.UPDATE_IN_PROGRESS);
						}
						*/
					}
				
					if (!it0241.isPresent()) {
						throw new CannotPersistException("Cannot update IT0241 (Tax Data Indonesia) data. Please check your data!");
					}
					
					StringBuilder key = new StringBuilder("pernr=" + it0241.get().getId().getPernr());
					key.append(",endda=" + it0241.get().getId().getEndda());
					key.append(",begda=" + it0241.get().getId().getBegda());
					EventLog eventLog = new EventLog(it0241Entity.get(), key.toString(), OperationType.UPDATE, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					
					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}

					//IT0009SoapObject it0009SoapObject = new IT0009SoapObject(savedEntity.get()); 
					//muleConsumerService.postToSAP(BAPIFunction.IT0009_CREATE.service(), "taxInfo", it0009SoapObject);
					
					responseWrapper = new IT0241ResponseWrapper(it0241.get());
					/*
					// since version 1.0.3
					responseWrapper = new IT0241ResponseWrapper(currentIT0241);
					*/
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(IT0241Controller.class).edit(request, result)).withSelfRel());
		return response;
	}		

	/*
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> editWithWorkflow(@RequestBody @Validated IT0241RequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			IT0241ResponseWrapper responseWrapper = new IT0241ResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<HCMSEntity> it0241Entity = hcmsEntityQueryService.findByEntityName(IT0241.class.getSimpleName());
					if (!it0241Entity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + IT0241.class.getSimpleName() + "' !!!");
					}
					
					Optional<IT0241> it0241 = it0241QueryService.findOneByCompositeKey(request.getPernr(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
					if (!it0241.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					// since version 1.0.3
					IT0241 currentIT0241 = new IT0241();
					BeanUtils.copyProperties(it0241.get(), currentIT0241);
				
					RequestObjectComparatorContainer<IT0241, IT0241RequestWrapper, String> updatedContainer 
						//= it0241RequestBuilderUtil.compareAndReturnUpdatedData(request, it0241.get());
				        // since version 1.0.3
				        = it0241RequestBuilderUtil.compareAndReturnUpdatedData(request, currentIT0241);

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
					if (updatedContainer.getEntity().isPresent()) {
						//updatedContainer.getEntity().get().setSeqnr(it0241.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
						//it0241 = it0241CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						// since version 1.0.3, update document only change the status 
						if (it0241.get().getStatus() == DocumentStatus.INITIAL_DRAFT) {
							it0241.get().setStatus(DocumentStatus.UPDATED_DRAFT);
							currentIT0241.setStatus(DocumentStatus.UPDATED_DRAFT);
						} else if (it0241.get().getStatus() == DocumentStatus.PUBLISHED) { 
							it0241.get().setStatus(DocumentStatus.UPDATE_IN_PROGRESS);
							currentIT0241.setStatus(DocumentStatus.UPDATE_IN_PROGRESS);
						}
					}
				
					if (!it0241.isPresent()) {
						throw new CannotPersistException("Cannot update IT0241 (Tax Data Indonesia) data. Please check your data!");
					}
					
					StringBuilder key = new StringBuilder("pernr=" + it0241.get().getId().getPernr());
					key.append(",endda=" + it0241.get().getId().getEndda());
					key.append(",begda=" + it0241.get().getId().getBegda());
					EventLog eventLog = new EventLog(it0241Entity.get(), key.toString(), OperationType.UPDATE, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					
					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}

					//IT0009SoapObject it0009SoapObject = new IT0009SoapObject(savedEntity.get()); 
					//muleConsumerService.postToSAP(BAPIFunction.IT0009_CREATE.service(), "taxInfo", it0009SoapObject);
					
					//responseWrapper = new IT0241ResponseWrapper(it0241.get());
					// since version 1.0.3
					responseWrapper = new IT0241ResponseWrapper(currentIT0241);
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(IT0241Controller.class).edit(request, result)).withSelfRel());
		return response;
	}		
	*/
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(itValidator);
	}		
}