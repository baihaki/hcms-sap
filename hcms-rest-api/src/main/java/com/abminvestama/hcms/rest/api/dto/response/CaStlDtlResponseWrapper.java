package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.CaStlDtl;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class CaStlDtlResponseWrapper extends ResourceSupport {
	private String detailId;
	private Integer itemNo;
	private String reqNo;
	private Date stlDate;
	private String description;
	private Double amount;
	private String currency;
	private Long orderId;
	private String orderText;
	private String kostl;
	private String kltxt;
	private Long glAcctId;
	private String glAcctText;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	private Integer poolBudget;

	private CaStlDtlResponseWrapper() {	}

	public CaStlDtlResponseWrapper(CaStlDtl caStlDtl) {
		if (caStlDtl == null) {
			new CaStlDtlResponseWrapper();
		} else {
			this
			.setDetailId(caStlDtl.getDetailId())
			.setItemNo(caStlDtl.getItemNo())
			.setReqNo(caStlDtl.getReqNo())
			.setStlDate(caStlDtl.getStlDate())
			.setDescription(caStlDtl.getDescription())
			.setAmount(caStlDtl.getAmount())
			.setCurrency(caStlDtl.getCurrency())
			.setOrderId(caStlDtl.getOrderId() != null ? caStlDtl.getOrderId().getId() : 0L)
			.setOrderText(caStlDtl.getOrderId() != null ? caStlDtl.getOrderId().getDescription() : "")
			.setKostl(caStlDtl.getOrderId() != null && caStlDtl.getOrderId().getCskt() != null ? caStlDtl.getOrderId().getCskt().getKostl() : "")
			.setKltxt(caStlDtl.getOrderId() != null && caStlDtl.getOrderId().getCskt() != null ? caStlDtl.getOrderId().getCskt().getKltxt() : "")
			.setGlAcctId(caStlDtl.getOrderId() != null && caStlDtl.getOrderId().getGlAccount() != null ? caStlDtl.getOrderId().getGlAccount().getId() : 0L)
			.setGlAcctText(caStlDtl.getOrderId() != null && caStlDtl.getOrderId().getGlAccount() != null ? caStlDtl.getOrderId().getGlAccount().getGlAcctText() : "")
			.setCreatedAt(caStlDtl.getCreatedAt())
			.setCreatedBy(caStlDtl.getCreatedBy().getId())
			.setUpdatedAt(caStlDtl.getUpdatedAt())
			.setUpdatedBy(caStlDtl.getUpdatedBy().getEmployee().getEname())
			.setPoolBudget(caStlDtl.getPoolBudget());
		}
	}

	@JsonProperty("detail_id")
	public String getDetailId() {
		return detailId;
	}

	public CaStlDtlResponseWrapper setDetailId(String detailId) {
		this.detailId = detailId;
		return this;
	}

	@JsonProperty("itemno")
	public Integer getItemNo() {
		return itemNo;
	}

	public CaStlDtlResponseWrapper setItemNo(Integer itemNo) {
		this.itemNo = itemNo;
		return this;
	}

	@JsonProperty("req_no")
	public String getReqNo() {
		return reqNo;
	}

	public CaStlDtlResponseWrapper setReqNo(String reqNo) {
		this.reqNo = reqNo;
		return this;
	}

	@JsonProperty("stl_date")
	public Date getStlDate() {
		return stlDate;
	}

	public CaStlDtlResponseWrapper setStlDate(Date stlDate) {
		this.stlDate = stlDate;
		return this;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public CaStlDtlResponseWrapper setDescription(String description) {
		this.description = description;
		return this;
	}

	@JsonProperty("amount")
	public Double getAmount() {
		return amount;
	}

	public CaStlDtlResponseWrapper setAmount(Double amount) {
		this.amount = amount;
		return this;
	}

	@JsonProperty("currency")
	public String getCurrency() {
		return currency;
	}

	public CaStlDtlResponseWrapper setCurrency(String currency) {
		this.currency = currency;
		return this;
	}

	@JsonProperty("orderid")
	public Long getOrderId() {
		return orderId;
	}

	public CaStlDtlResponseWrapper setOrderId(Long orderId) {
		this.orderId = orderId;
		return this;
	}

	@JsonProperty("order_text")
	public String getOrderText() {
		return orderText;
	}

	public CaStlDtlResponseWrapper setOrderText(String orderText) {
		this.orderText = orderText;
		return this;
	}

	@JsonProperty("kltxt")
	public String getKltxt() {
		return kltxt;
	}

	public CaStlDtlResponseWrapper setKltxt(String kltxt) {
		this.kltxt = kltxt;
		return this;
	}

	@JsonProperty("kostl")
	public String getKostl() {
		return kostl;
	}

	public CaStlDtlResponseWrapper setKostl(String kostl) {
		this.kostl = kostl;
		return this;
	}

	@JsonProperty("gl_acct_id")
	public Long getGlAcctId() {
		return glAcctId;
	}

	public CaStlDtlResponseWrapper setGlAcctId(Long glAcctId) {
		this.glAcctId = glAcctId;
		return this;
	}

	@JsonProperty("gl_acct_text")
	public String getGlAcctText() {
		return glAcctText;
	}

	public CaStlDtlResponseWrapper setGlAcctText(String glAcctText) {
		this.glAcctText = glAcctText;
		return this;
	}

	@JsonProperty("createdat")
	public Date getCreatedAt() {
		return createdAt;
	}

	public CaStlDtlResponseWrapper setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	@JsonProperty("createdby")
	public String getCreatedBy() {
		return createdBy;
	}

	public CaStlDtlResponseWrapper setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
		return this;
	}

	@JsonProperty("updatedat")
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public CaStlDtlResponseWrapper setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
		return this;
	}

	@JsonProperty("updatedby")
	public String getUpdatedBy() {
		return updatedBy;
	}

	public CaStlDtlResponseWrapper setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
		return this;
	}

	@JsonProperty("poolbudget")
	public Integer getPoolBudget() {
		return poolBudget;
	}

	public CaStlDtlResponseWrapper setPoolBudget(Integer poolBudget) {
		this.poolBudget = poolBudget;
		return this;
	}

}
