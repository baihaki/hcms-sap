package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyHeaderPost;
import com.abminvestama.hcms.core.model.entity.ZmedHeaderPost;
import com.abminvestama.hcms.core.model.entity.ZmedHeaderPost;
import com.abminvestama.hcms.core.service.api.business.query.ZmedHeaderPostQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedHeaderPostRequestWrapper;



@Component("ZmedHeaderPostRequestBuilderUtil")
@Transactional(readOnly = true)
public class ZmedHeaderPostRequestBuilderUtil {
	
	
	private T001QueryService t001QueryService;
	private ZmedHeaderPostQueryService zmedHeaderPostQueryService;
	
	@Autowired
	void setZmedHeaderPostQueryService(ZmedHeaderPostQueryService zmedHeaderPostQueryService) {
		this.zmedHeaderPostQueryService = zmedHeaderPostQueryService;
	}
	
	@Autowired
	void setT001QueryService(T001QueryService t001QueryService) {
		this.t001QueryService = t001QueryService;
	}
	
	public RequestObjectComparatorContainer<ZmedHeaderPost, ZmedHeaderPostRequestWrapper, String> compareAndReturnUpdatedData(ZmedHeaderPostRequestWrapper requestPayload, ZmedHeaderPost zmedHeaderPostDB) {
		
		RequestObjectComparatorContainer<ZmedHeaderPost, ZmedHeaderPostRequestWrapper, String> objectContainer = null; 
		
	if (zmedHeaderPostDB == null) {
		zmedHeaderPostDB = new ZmedHeaderPost();
		
		try {
			Optional<ZmedHeaderPost> existingZmedHeaderPost = zmedHeaderPostQueryService.findOneByCompositeKey(
					requestPayload.getObjType(), requestPayload.getObjKey(), requestPayload.getObjSys());
			
			if (existingZmedHeaderPost.isPresent()) {
				throw new DataViolationException("Duplicate ZmedHeaderPost Data for [ssn=" + requestPayload.getObjKey());
			}
			zmedHeaderPostDB.setId(
					new ZmedCompositeKeyHeaderPost(requestPayload.getObjType(), requestPayload.getObjKey(),
							requestPayload.getObjSys()));
			
			
			zmedHeaderPostDB.setVatdate(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getVatdate()));
			zmedHeaderPostDB.setTransDate(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getTransDate()));
			zmedHeaderPostDB.setPstngDate(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getPstngDate()));
			zmedHeaderPostDB.setDocDate(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDocDate()));
			zmedHeaderPostDB.setObjType(requestPayload.getObjType());
			zmedHeaderPostDB.setObjKey(requestPayload.getObjKey());//
			zmedHeaderPostDB.setObjSys(requestPayload.getObjSys());//
			zmedHeaderPostDB.setBusAct(requestPayload.getBusAct());//
			zmedHeaderPostDB.setUsername(requestPayload.getUsername());//
			zmedHeaderPostDB.setHeaderTxt(requestPayload.getHeaderTxt());//
			zmedHeaderPostDB.setFiscYear(Byte.parseByte(requestPayload.getFiscYear()));//
			zmedHeaderPostDB.setFisPeriod(Byte.parseByte(requestPayload.getFisPeriod()));//
			zmedHeaderPostDB.setDocType(requestPayload.getDocType());//
			zmedHeaderPostDB.setRefDocNo(requestPayload.getRefDocNo());//
			zmedHeaderPostDB.setAcDocNo(requestPayload.getAccDocNo());//
			zmedHeaderPostDB.setObjKeyR(requestPayload.getObjKeyR());//
			zmedHeaderPostDB.setReasonRev(requestPayload.getReasonRev());//
			zmedHeaderPostDB.setCompoAcc(requestPayload.getCompoAcc());//
			zmedHeaderPostDB.setRefDocNoLong(requestPayload.getRefDocNoLong());
			zmedHeaderPostDB.setAccPrinciple(requestPayload.getAccPrinciple());
			zmedHeaderPostDB.setNegPosting(requestPayload.getNegPosting());
			zmedHeaderPostDB.setObjKeyInv(requestPayload.getObjKeyInv());
			zmedHeaderPostDB.setBillCategory(requestPayload.getBillCategory());
			zmedHeaderPostDB.setLogsys(requestPayload.getLogsys());//
			Optional<T001> bnka = t001QueryService.findById(Optional.ofNullable(requestPayload.getCompCode()));//				
			zmedHeaderPostDB.setCompCode(bnka.isPresent() ? bnka.get() : null);
			//zmedHeaderPostDB.setCompCode(requestPayload.getCompCode());
			
		} catch (Exception ignored) {
			System.err.println(ignored);
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedHeaderPostDB, requestPayload);
		
	} else {
		
		if (StringUtils.isNotBlank(requestPayload.getObjType())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getObjType(), zmedHeaderPostDB.getObjType())) {
				zmedHeaderPostDB.setObjType(requestPayload.getObjType());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getObjKey())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getObjKey(), zmedHeaderPostDB.getObjKey())) {
				zmedHeaderPostDB.setObjKey(requestPayload.getObjKey());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getObjSys())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getObjSys(), zmedHeaderPostDB.getObjSys())) {
				zmedHeaderPostDB.setObjSys(requestPayload.getObjSys());
				requestPayload.setDataChanged(true);
			}
		}
				
		if (StringUtils.isNotBlank(requestPayload.getBusAct())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBusAct(), zmedHeaderPostDB.getBusAct())) {
				zmedHeaderPostDB.setBusAct(requestPayload.getBusAct());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getUsername())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getUsername(), zmedHeaderPostDB.getUsername())) {
				zmedHeaderPostDB.setUsername(requestPayload.getUsername());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getHeaderTxt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getHeaderTxt(), zmedHeaderPostDB.getHeaderTxt())) {
				zmedHeaderPostDB.setHeaderTxt(requestPayload.getHeaderTxt());
				requestPayload.setDataChanged(true);
			}
		}
		
						
		if (StringUtils.isNotBlank(requestPayload.getDocType())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDocType(), zmedHeaderPostDB.getDocType())) {
				zmedHeaderPostDB.setDocType(requestPayload.getDocType());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getRefDocNo())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getRefDocNo(), zmedHeaderPostDB.getRefDocNo())) {
				zmedHeaderPostDB.setRefDocNo(requestPayload.getRefDocNo());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getAccDocNo())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAccDocNo(), zmedHeaderPostDB.getAcDocNo())) {
				zmedHeaderPostDB.setAcDocNo(requestPayload.getAccDocNo());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getObjKeyR())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getObjKeyR(), zmedHeaderPostDB.getObjKeyR())) {
				zmedHeaderPostDB.setObjKeyR(requestPayload.getObjKeyR());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getReasonRev())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getReasonRev(), zmedHeaderPostDB.getReasonRev())) {
				zmedHeaderPostDB.setReasonRev(requestPayload.getReasonRev());
				requestPayload.setDataChanged(true);
			}
		}
				
		if (StringUtils.isNotBlank(requestPayload.getCompoAcc())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getCompoAcc(), zmedHeaderPostDB.getCompoAcc())) {
				zmedHeaderPostDB.setCompoAcc(requestPayload.getCompoAcc());
				requestPayload.setDataChanged(true);
			}
		}
			
		if (StringUtils.isNotBlank(requestPayload.getRefDocNoLong())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getRefDocNoLong(), zmedHeaderPostDB.getRefDocNoLong())) {
				zmedHeaderPostDB.setRefDocNoLong(requestPayload.getRefDocNoLong());
				requestPayload.setDataChanged(true);
			}
		}

		
		if (StringUtils.isNotBlank(requestPayload.getAccPrinciple())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAccPrinciple(), zmedHeaderPostDB.getAccPrinciple())) {
				zmedHeaderPostDB.setAccPrinciple(requestPayload.getAccPrinciple());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getNegPosting())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getNegPosting(), zmedHeaderPostDB.getNegPosting())) {
				zmedHeaderPostDB.setNegPosting(requestPayload.getNegPosting());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getObjKeyInv())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getObjKeyInv(), zmedHeaderPostDB.getObjKeyInv())) {
				zmedHeaderPostDB.setObjKeyInv(requestPayload.getObjKeyInv());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getBillCategory())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBillCategory(), zmedHeaderPostDB.getBillCategory())) {
				zmedHeaderPostDB.setBillCategory(requestPayload.getBillCategory());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getLogsys())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getLogsys(), zmedHeaderPostDB.getLogsys())) {
				zmedHeaderPostDB.setLogsys(requestPayload.getLogsys());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getCompCode())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getCompCode(), zmedHeaderPostDB.getCompCode() != null ? zmedHeaderPostDB.getCompCode().getBukrs() : StringUtils.EMPTY)) {
				try {
					Optional<T001> newT001 = t001QueryService.findById(Optional.ofNullable(requestPayload.getCompCode()));
					if (newT001.isPresent()) {
						zmedHeaderPostDB.setCompCode(newT001.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					//T535N not found... cancel the update process
				}
			}
		}
		
		
		/*if (StringUtils.isNotBlank(requestPayload.getCompCode())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getCompCode(), zmedHeaderPostDB.getCompCode())) {
				zmedHeaderPostDB.setCompCode(requestPayload.getCompCode());
				requestPayload.setDataChanged(true);
			}
		}*/

			
		
		if (StringUtils.isNotBlank(requestPayload.getFiscYear())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getFiscYear(), zmedHeaderPostDB.getFiscYear().toString())) {
				zmedHeaderPostDB.setFiscYear(Byte.parseByte(requestPayload.getFiscYear()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getFisPeriod())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getFisPeriod(), zmedHeaderPostDB.getFisPeriod().toString())) {
				zmedHeaderPostDB.setFisPeriod(Byte.parseByte(requestPayload.getFisPeriod()));
				requestPayload.setDataChanged(true);
			}
		}
		
		
		if (StringUtils.isNotBlank(requestPayload.getVatdate())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getVatdate()), (zmedHeaderPostDB.getVatdate())) ){
				zmedHeaderPostDB.setVatdate(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getVatdate()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getTransDate())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getTransDate()), (zmedHeaderPostDB.getTransDate())) ){
				zmedHeaderPostDB.setTransDate(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getTransDate()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getPstngDate())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getPstngDate()), (zmedHeaderPostDB.getPstngDate())) ){
				zmedHeaderPostDB.setPstngDate(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getPstngDate()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getDocDate())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDocDate()), (zmedHeaderPostDB.getDocDate())) ){
				zmedHeaderPostDB.setDocDate(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDocDate()));
				requestPayload.setDataChanged(true);
			}
		}
		
		
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedHeaderPostDB, requestPayload);	
				
	}
	
	JSONObject json = new JSONObject(requestPayload);
	objectContainer.setEventData(json.toString());
	
	return objectContainer;
	}
	}
	
