package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.Attachment;
import com.abminvestama.hcms.core.model.entity.IT0022;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.T005T;
import com.abminvestama.hcms.core.model.entity.T517T;
import com.abminvestama.hcms.core.model.entity.T517X;
import com.abminvestama.hcms.core.model.entity.T518B;
import com.abminvestama.hcms.core.model.entity.T519T;
import com.abminvestama.hcms.core.model.entity.T538T;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.business.query.AttachmentQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0022QueryService;
import com.abminvestama.hcms.core.service.api.business.query.T005TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T517TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T517XQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T518BQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T519TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T538TQueryService;
import com.abminvestama.hcms.core.service.util.FileAttachmentService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0022RequestWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Adjust attachment fields on EventData Json</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData: Add attachment fields</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Modify compareAndReturnUpdatedData to handle Create</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("it0022RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT0022RequestBuilderUtil {
	
	private T517TQueryService t517tQueryService;
	private T005TQueryService t005tQueryService;
	private T518BQueryService t518bQueryService;
	private T519TQueryService t519tQueryService;
	private T538TQueryService t538tQueryService;
	private T517XQueryService t517xQueryService;
	private IT0022QueryService it0022QueryService;
	private AttachmentQueryService attachmentQueryService;
	private FileAttachmentService fileAttachmentService;
	
	@Autowired
	void setT517TQueryService(T517TQueryService t517tQueryService) {
		this.t517tQueryService = t517tQueryService;
	}
	
	@Autowired
	void setT005TQueryService(T005TQueryService t005tQueryService) {
		this.t005tQueryService = t005tQueryService;
	}
	
	@Autowired
	void setT518BQueryService(T518BQueryService t518bQueryService) {
		this.t518bQueryService = t518bQueryService;
	}
	
	@Autowired
	void setT519TQueryService(T519TQueryService t519tQueryService) {
		this.t519tQueryService = t519tQueryService;
	}
	
	@Autowired
	void setT538TQueryService(T538TQueryService t538tQueryService) {
		this.t538tQueryService = t538tQueryService;
	}
	
	@Autowired
	void setT517XQueryService(T517XQueryService t517xQueryService) {
		this.t517xQueryService = t517xQueryService;
	}
	
	@Autowired
	void setIT0022QueryService(IT0022QueryService it0022QueryService) {
		this.it0022QueryService = it0022QueryService;
	}
	
	@Autowired
	void setAttachmentQueryService(AttachmentQueryService attachmentQueryService) {
		this.attachmentQueryService = attachmentQueryService;
	}
	
	@Autowired
	void setFileAttachmentService(FileAttachmentService fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}

	/**
	 * Compare IT0022 requested payload with an existing IT0022 in the database.
	 * Update the existing IT0022 data with the latest data comes from the requested payload. 
	 * 
	 * @param requestPayload requested data 
	 * @param it0022DB current existing IT0022 in the database.
	 * @return updated IT0022 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0022, IT0022RequestWrapper, String> compareAndReturnUpdatedData(IT0022RequestWrapper requestPayload, IT0022 it0022DB) {
		
		RequestObjectComparatorContainer<IT0022, IT0022RequestWrapper, String> objectContainer = null; 
		if (it0022DB == null) {
			it0022DB = new IT0022();
			try {
				Optional<IT0022> existingIT0022 = it0022QueryService.findOneByCompositeKey(
						requestPayload.getPernr(), requestPayload.getSubty(), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda()));
				
				if (existingIT0022.isPresent()) {
					throw new DataViolationException("Duplicate IT0022 Data for [ssn=" + requestPayload.getPernr() + ",end_date=" +
							requestPayload.getEndda() + ",begin_date=" + requestPayload.getBegda());
				}
				
				it0022DB.setId(
						new ITCompositeKeys(requestPayload.getPernr(), "0022", requestPayload.getSubty(), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda())));
				it0022DB.setAnzkl(requestPayload.getAnzkl());
				Optional<T517T> bnka = t517tQueryService.findById(Optional.ofNullable(requestPayload.getSlart()));
				it0022DB.setSlart(bnka.isPresent() ? bnka.get() : null);
				it0022DB.setInsti(requestPayload.getInsti());
				Optional<T005T> t005t = t005tQueryService.findById(Optional.ofNullable(requestPayload.getSland()));
				it0022DB.setSland(t005t.isPresent() ? t005t.get() : null);
				Optional<T518B> t591s = t518bQueryService.findById(Optional.ofNullable(requestPayload.getAusbi()));
				it0022DB.setAusbi(t591s.isPresent() ? t591s.get() : null);
				it0022DB.setSeqnr(1L);
				Optional<T519T> t042z = t519tQueryService.findById(Optional.ofNullable(requestPayload.getSlabs()));
				it0022DB.setSlabs(t042z.isPresent() ? t042z.get() : null);
				Optional<T538T> t538t = t538tQueryService.findById(Optional.ofNullable(requestPayload.getAnzeh()));
				it0022DB.setAnzeh(t538t.isPresent() ? t538t.get() : null);
				Optional<T517X> t517x = t517xQueryService.findById(Optional.ofNullable(requestPayload.getSltp2()));
				it0022DB.setSltp2(t517x.isPresent() ? t517x.get() : null);
				Optional<T517X> t517x2 = t517xQueryService.findById(Optional.ofNullable(requestPayload.getSltp1()));
				it0022DB.setSltp1(t517x2.isPresent() ? t517x2.get() : null);
				it0022DB.setEmark(requestPayload.getEmark());
				it0022DB.setStatus(DocumentStatus.INITIAL_DRAFT);
				
				it0022DB = saveAttachment(it0022DB, requestPayload);
				
			} catch (Exception ignored) {
				System.err.println(ignored);
			}

			objectContainer = new RequestObjectComparatorContainer<>(it0022DB, requestPayload);
		} else {
			
			if (StringUtils.isNotBlank(requestPayload.getSlart())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getSlart(), it0022DB.getSlart() != null ? it0022DB.getSlart().getSlart() : StringUtils.EMPTY)) {
					try {
						Optional<T517T> newT517T = t517tQueryService.findById(Optional.ofNullable(requestPayload.getSlart()));
						if (newT517T.isPresent()) {
							it0022DB.setSlart(newT517T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T517T not found... cancel the update process
					}
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getInsti())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getInsti(), it0022DB.getInsti())) {
					it0022DB.setInsti(requestPayload.getInsti());
					requestPayload.setDataChanged(true);
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getEmark())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getEmark(), it0022DB.getEmark())) {
					it0022DB.setEmark(requestPayload.getEmark());
					requestPayload.setDataChanged(true);
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getSland())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getSland(), it0022DB.getSland() != null ? it0022DB.getSland().getLand1() : StringUtils.EMPTY)) {
					try {
						Optional<T005T> newT005T = t005tQueryService.findById(Optional.ofNullable(requestPayload.getSland().trim()));
						if (newT005T.isPresent()) {
							it0022DB.setSland(newT005T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T005T not found... cancel the update process
					}
				}
			}
			if (requestPayload.getAusbi() != 0) {
				if (CommonComparatorFunction.isDifferentNumberValues(Long.valueOf(requestPayload.getAusbi()), it0022DB.getAusbi() != null ? it0022DB.getAusbi().getAusbi().longValue() : 0)) {
					try {
						Optional<T518B> newT518B = t518bQueryService.findById(Optional.ofNullable(requestPayload.getAusbi()));
						if (newT518B.isPresent()) {
							it0022DB.setAusbi(newT518B.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T518B not found... cancel the update process
					}
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getSlabs())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getSlabs(), it0022DB.getSlabs() != null ? it0022DB.getSlabs().getSlabs() : StringUtils.EMPTY)) {
					try {
						Optional<T519T> newT519T = t519tQueryService.findById(Optional.ofNullable(requestPayload.getSlabs()));
						if (newT519T.isPresent()) {
							it0022DB.setSlabs(newT519T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T519T not found... cancel the update process
					}
				}
			}
			if (requestPayload.getAnzkl() != 0.0) {
				if (CommonComparatorFunction.isDifferentNumberValues(Double.valueOf(requestPayload.getAnzkl()), it0022DB.getAnzkl().doubleValue())) {
					it0022DB.setAnzkl(requestPayload.getAnzkl());
					requestPayload.setDataChanged(true);
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getAnzeh())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAnzeh(), it0022DB.getAnzeh() != null ? it0022DB.getAnzeh().getZeinh() : StringUtils.EMPTY)) {
					try {
						Optional<T538T> newT538T = t538tQueryService.findById(Optional.ofNullable(requestPayload.getAnzeh()));
						if (newT538T.isPresent()) {
							it0022DB.setAnzeh(newT538T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T538T not found... cancel the update process
					}
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getSltp1())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getSltp1(), it0022DB.getSltp1() != null ? it0022DB.getSltp1().getFaart() : StringUtils.EMPTY)) {
					try {
						Optional<T517X> newT517X = t517xQueryService.findById(Optional.ofNullable(requestPayload.getSltp1()));
						if (newT517X.isPresent()) {
							it0022DB.setSltp1(newT517X.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T517X not found... cancel the update process
					}
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getSltp2())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getSltp2(), it0022DB.getSltp2() != null ? it0022DB.getSltp2().getFaart() : StringUtils.EMPTY)) {
					try {
						Optional<T517X> newT517X = t517xQueryService.findById(Optional.ofNullable(requestPayload.getSltp2()));
						if (newT517X.isPresent()) {
							it0022DB.setSltp2(newT517X.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T517X not found... cancel the update process
					}
				}
			}
			
			it0022DB.setSeqnr(it0022DB.getSeqnr().longValue() + 1);
			try {
				it0022DB = saveAttachment(it0022DB, requestPayload);
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			it0022DB.setSeqnr(it0022DB.getSeqnr().longValue() - 1);
			
			objectContainer = new RequestObjectComparatorContainer<>(it0022DB, requestPayload);
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = "";
		try {
			jsonString = objectMapper.writeValueAsString(requestPayload);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}		
		
		//JSONObject json = new JSONObject(requestPayload);
		JSONObject json = new JSONObject(jsonString);
		// Adjust attachment fields on EventData Json
		json.remove("attachment1_type");
		json.remove("attachment1_base64");
		json.remove("attachment1_filename");
		if (it0022DB.getAttachment() != null)
			json.put("attachment1_type", it0022DB.getAttachment().getId().getSubty());
		if (it0022DB.getAttachmentPath() != null)
		    json.put("attachment1_path", it0022DB.getAttachmentPath());
		// ******************************************
		
		//if (it0022DB.getStatus() != null)
			//json.put("prev_document_status", it0022DB.getStatus().name());
		
		objectContainer.setEventData(json.toString());
		
		return objectContainer;
	}
	
	private IT0022 saveAttachment(IT0022 it0022DB, IT0022RequestWrapper requestPayload) throws Exception {
		
		T591SKey attachmentKey = new T591SKey(SAPInfoType.EMPLOYEE_EDUCATION.infoType(), requestPayload.getAttach1Type());
		Optional<Attachment> attachment = attachmentQueryService.findById(Optional.ofNullable(attachmentKey));
		it0022DB.setAttachment(attachment.isPresent() ? attachment.get() : null);
		String base64 = requestPayload.getAttach1Content();
		String filename = requestPayload.getAttach1Filename();
		if (StringUtils.isNotEmpty(base64) && StringUtils.isNotEmpty(filename)) {
			String ext = filename.split("\\.")[(filename.split("\\.")).length-1];
			String formatFilename = it0022DB.getId().toStringId().concat("_").concat(it0022DB.getSeqnr().toString()).concat(".").concat(ext);
		    String path = fileAttachmentService.save(formatFilename, base64);
		    it0022DB.setAttachmentPath(path);
		    requestPayload.setDataChanged(true);
		}
		
		return it0022DB;
		
	}
}