package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.entity.CaDeptsCostCenter;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.query.CaDeptsCostCenterQueryService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.CaDeptsCostCenterResponseWrapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/ca_depts_cost_center")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CaDeptsCostCenterController extends AbstractResource {

	private EventLogCommandService eventLogCommandService;
	private HCMSEntityQueryService hcmsEntityQueryService;
	private UserQueryService userQueryService;
	private CommonServiceFactory commonServiceFactory;
	private CaDeptsCostCenterQueryService caDeptsCostCenterQueryService;

	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}

	@Autowired
	public void setCaDeptsCostCenterQueryService(CaDeptsCostCenterQueryService caDeptsCostCenterQueryService) {
		this.caDeptsCostCenterQueryService = caDeptsCostCenterQueryService;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/findById/{deptCostCenterId}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<CaDeptsCostCenterResponseWrapper> findOneByDeptCostCenterId(@PathVariable Long deptCostCenterId)
			throws Exception {
		
		APIResponseWrapper<CaDeptsCostCenterResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			Optional<CaDeptsCostCenter> caDeptsCostCenterObj = caDeptsCostCenterQueryService.findById(Optional.ofNullable(deptCostCenterId));
			
			if (!caDeptsCostCenterObj.isPresent()) {
				response.setMessage("No Data Found");
				response.setData(new CaDeptsCostCenterResponseWrapper(null));
			}
			else {
				response.setData(new CaDeptsCostCenterResponseWrapper(caDeptsCostCenterObj.get()));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(CaDeptsCostCenterController.class).findOneByDeptCostCenterId(deptCostCenterId)).withSelfRel());
		return response;
	}	
}
