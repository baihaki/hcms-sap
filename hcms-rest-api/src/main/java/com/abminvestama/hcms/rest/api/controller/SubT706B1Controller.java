package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.entity.SubT706B1;
import com.abminvestama.hcms.core.service.api.business.query.SubT706B1QueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.SubT706B1ResponseWrapper;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@RestController
@RequestMapping("/api/v1/sub_exp_type")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class SubT706B1Controller extends AbstractResource {

	private SubT706B1QueryService subt706b1QueryService;
	
	@Autowired
	void setSubT706B1QueryService(SubT706B1QueryService subt706b1QueryService) {
		this.subt706b1QueryService = subt706b1QueryService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/morei/{morei}",
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<SubT706B1ResponseWrapper>> findByMoreiNE(@PathVariable String morei,
			@RequestParam(value = "form", required = false) Short form) throws Exception {
		
		ArrayData<SubT706B1ResponseWrapper> list = new ArrayData<>();
		APIResponseWrapper<ArrayData<SubT706B1ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(list);
		
		String spkzlPrefix = "NE%";
		
		try {
			List<SubT706B1> listOfSubT706B1 = subt706b1QueryService.findByMoreiAndSpkzlPrefixAndForm(morei, spkzlPrefix, form).
					stream().collect(Collectors.toList());

			if (listOfSubT706B1.isEmpty()) {
				response.setMessage("No Data Found in subt706b1");
			} else {
				List<SubT706B1ResponseWrapper> records = new ArrayList<>();
				//records.add(new MedPatientResponseWrapper(it0001));
				for (SubT706B1 subT706B1 : listOfSubT706B1) {
					records.add(new SubT706B1ResponseWrapper(subT706B1));
				}
				list.setItems(records.toArray(new SubT706B1ResponseWrapper[records.size()]));							
			}
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(SubT706B1Controller.class).findByMoreiNE(morei, form)).withSelfRel());
		return response;
	}	

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}

}
