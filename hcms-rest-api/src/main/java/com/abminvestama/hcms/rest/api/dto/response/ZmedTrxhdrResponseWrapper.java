package com.abminvestama.hcms.rest.api.dto.response;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.5
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add fields: banka, bankn</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add fields: reason</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add fields: paymentStatus2, docno2</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add fields: paymentStatus, paymentDate, docno</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add fields: process</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class ZmedTrxhdrResponseWrapper extends ResourceSupport {

	private String bukrs;
	private String zclmno;
	private String pernr;
	private String orgtx;
	private String zdivision;
	private String ename;
	private String clmst;
	private String rejamt;
	private String appamt;
	private String clmamt;
	private String pagu1;
	private String pagu2;

	private String pagudent;
	private String pagu2byemp;
	private String totamt;
	private String pasien;
	private String penyakit;
	private String tglmasuk;
	private String tglkeluar;
	private String clmdt;
	private String clmby;

	private String aprdt;
	private String aprby;
	private String paydt;
	private String payby;
	private String paymethod;
	private String belnr;
	private String jamin;
	private String notex;
	private String billdt;

	private String kodemed;
	private String medtydesc;
	private String revdt;
	private String revby;
	
	private String process;
	private String paymentStatus;
	private String paymentDate;
	private String docno;

	private String paymentStatus2;
	private String docno2;
	
	private String reason;
	private String banka;
	private String bankn;
	

	private ZmedTrxhdrResponseWrapper() {}
	
	public ZmedTrxhdrResponseWrapper(ZmedTrxhdr zmedTrxhdr) {
		if (zmedTrxhdr == null) {
			new ZmedTrxhdrResponseWrapper();
		} else {
			this
			//.setBukrs(zmedTrxhdr.getBukrs() != null ? StringUtils.defaultString(zmedTrxhdr.getBukrs().getBukrs(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setBilldt(zmedTrxhdr.getBilldt()!= null?StringUtils.defaultString(zmedTrxhdr.getBilldt().toString(), StringUtils.EMPTY):StringUtils.EMPTY)
			.setClmdt(zmedTrxhdr.getClmdt()!= null?StringUtils.defaultString(zmedTrxhdr.getClmdt().toString(), StringUtils.EMPTY):StringUtils.EMPTY)
			.setAprdt(zmedTrxhdr.getAprdt()!= null?StringUtils.defaultString(zmedTrxhdr.getAprdt().toString(), StringUtils.EMPTY):StringUtils.EMPTY)
			.setPaydt(zmedTrxhdr.getPaydt()!= null?StringUtils.defaultString(zmedTrxhdr.getPaydt().toString(), StringUtils.EMPTY):StringUtils.EMPTY)
			.setRevdt(zmedTrxhdr.getRevdt()!= null?StringUtils.defaultString(zmedTrxhdr.getRevdt().toString(), StringUtils.EMPTY):StringUtils.EMPTY)
			.setTglmasuk(zmedTrxhdr.getTglmasuk()!= null?StringUtils.defaultString(zmedTrxhdr.getTglmasuk().toString(), StringUtils.EMPTY):StringUtils.EMPTY)
			.setTglkeluar(zmedTrxhdr.getTglkeluar()!= null?StringUtils.defaultString(zmedTrxhdr.getTglkeluar().toString(), StringUtils.EMPTY):StringUtils.EMPTY)
			.setBukrs(StringUtils.defaultString(zmedTrxhdr.getBukrs().getBukrs(), StringUtils.EMPTY))
			.setMedtydesc(StringUtils.defaultString(zmedTrxhdr.getMedtydesc(), StringUtils.EMPTY))
			.setRevby(StringUtils.defaultString(zmedTrxhdr.getRevby(), StringUtils.EMPTY))
			.setZclmno(StringUtils.defaultString(zmedTrxhdr.getZclmno(), StringUtils.EMPTY))
			.setPernr(StringUtils.defaultString(zmedTrxhdr.getPernr().toString(), StringUtils.EMPTY))
			.setKodemed(StringUtils.defaultString(zmedTrxhdr.getKodemed(), StringUtils.EMPTY))
			.setOrgtx(StringUtils.defaultString(zmedTrxhdr.getOrgtx(), StringUtils.EMPTY))
			.setZdivision(StringUtils.defaultString(zmedTrxhdr.getZdivision(), StringUtils.EMPTY))
			.setClmamt(zmedTrxhdr.getClmamt() != null ? String.format("%.2f", zmedTrxhdr.getClmamt().doubleValue()) : StringUtils.EMPTY)
			//.setRejamt(StringUtils.defaultString(zmedTrxhdr.getRejamt().toString(), StringUtils.EMPTY))
			.setRejamt(zmedTrxhdr.getRejamt() != null ? String.format("%.2f", zmedTrxhdr.getRejamt().doubleValue()) : StringUtils.EMPTY)
			//.setAppamt(StringUtils.defaultString(zmedTrxhdr.getAppamt().toString(), StringUtils.EMPTY))
			.setAppamt(zmedTrxhdr.getAppamt() != null ? String.format("%.2f", zmedTrxhdr.getAppamt().doubleValue()) : StringUtils.EMPTY)
			.setEname(StringUtils.defaultString(zmedTrxhdr.getEname(), StringUtils.EMPTY))
			.setClmst(StringUtils.defaultString(zmedTrxhdr.getClmst(), StringUtils.EMPTY))
			.setPasien(StringUtils.defaultString(zmedTrxhdr.getPasien(), StringUtils.EMPTY))
			//.setPagu1(StringUtils.defaultString(zmedTrxhdr.getPagu1().toString(), StringUtils.EMPTY))
			.setPagu1(zmedTrxhdr.getPagu1() != null ? String.format("%.2f", zmedTrxhdr.getPagu1().doubleValue()) : StringUtils.EMPTY)
			//.setPagu2(StringUtils.defaultString(zmedTrxhdr.getPagu2().toString(), StringUtils.EMPTY))
			.setPagu2(zmedTrxhdr.getPagu2() != null ? String.format("%.2f", zmedTrxhdr.getPagu2().doubleValue()) : StringUtils.EMPTY)
			//.setPagudent(StringUtils.defaultString(zmedTrxhdr.getPagudent().toString(), StringUtils.EMPTY))
			.setPagudent(zmedTrxhdr.getPagudent() != null ? String.format("%.2f", zmedTrxhdr.getPagudent().doubleValue()) : StringUtils.EMPTY)
			//.setPagu2byemp(StringUtils.defaultString(zmedTrxhdr.getPagu2byemp().toString(), StringUtils.EMPTY))
			.setPagu2byemp(zmedTrxhdr.getPagu2byemp() != null ? String.format("%.2f", zmedTrxhdr.getPagu2byemp().doubleValue()) : StringUtils.EMPTY)
			//.setTotamt(StringUtils.defaultString(zmedTrxhdr.getTotamt().toString(), StringUtils.EMPTY))
			.setTotamt(zmedTrxhdr.getTotamt() != null ? String.format("%.2f", zmedTrxhdr.getTotamt().doubleValue()) : StringUtils.EMPTY)
			.setNotex(StringUtils.defaultString(zmedTrxhdr.getNotex(), StringUtils.EMPTY))
			.setPenyakit(StringUtils.defaultString(zmedTrxhdr.getPenyakit(), StringUtils.EMPTY))
			.setPaymethod(StringUtils.defaultString(zmedTrxhdr.getPaymethod(), StringUtils.EMPTY))
			.setAprby(StringUtils.defaultString(zmedTrxhdr.getAprby(), StringUtils.EMPTY))
			.setClmby(StringUtils.defaultString(zmedTrxhdr.getClmby(), StringUtils.EMPTY))
			.setPayby(StringUtils.defaultString(zmedTrxhdr.getPayby(), StringUtils.EMPTY))
			.setBelnr(StringUtils.defaultString(zmedTrxhdr.getBelnr(), StringUtils.EMPTY))
			.setJamin(StringUtils.defaultString(zmedTrxhdr.getJamin(), StringUtils.EMPTY))
			.setProcess(StringUtils.defaultString(zmedTrxhdr.getProcess(), StringUtils.EMPTY))
			.setPaymentStatus(StringUtils.defaultString(zmedTrxhdr.getPaymentStatus(), StringUtils.EMPTY))
			.setPaymentDate(zmedTrxhdr.getPaymentDate() != null ? CommonDateFunction.convertDateToStringYMD(zmedTrxhdr.getPaymentDate()) : StringUtils.EMPTY)
			.setDocno(StringUtils.defaultString(zmedTrxhdr.getDocno(), StringUtils.EMPTY))
			.setPaymentStatus2(StringUtils.defaultString(zmedTrxhdr.getPaymentStatus2(), StringUtils.EMPTY))
			.setDocno2(StringUtils.defaultString(zmedTrxhdr.getDocno2(), StringUtils.EMPTY))
			.setReason(StringUtils.defaultString(zmedTrxhdr.getReason(), StringUtils.EMPTY))
			.setBanka(StringUtils.defaultString(zmedTrxhdr.getBanka(), StringUtils.EMPTY))
			.setBankn(StringUtils.defaultString(zmedTrxhdr.getBankn(), StringUtils.EMPTY));	
		}
	}
	


	
	
	@JsonProperty("company_code")
	public String getBukrs() {
		return bukrs;
	}
	
	private ZmedTrxhdrResponseWrapper setBukrs(String bukrs) {
		this.bukrs = bukrs;
		return this;
	}
	


	
	
	@JsonProperty("zclmno")
	public String getZclmno() {
		return zclmno;
	}
	
	private ZmedTrxhdrResponseWrapper setZclmno(String zclmno) {
		this.zclmno = zclmno;
		return this;
	}
	


	
	
	@JsonProperty("pernr")
	public String getPernr() {
		return pernr;
	}
	
	private ZmedTrxhdrResponseWrapper setPernr(String pernr) {
		this.pernr = pernr;
		return this;
	}
	
	
	@JsonProperty("orgtx")
	public String getOrgtx() {
		return orgtx;
	}
	
	private ZmedTrxhdrResponseWrapper setOrgtx(String orgtx) {
		this.orgtx = orgtx;
		return this;
	}
	
	
	
	@JsonProperty("zdivision")
	public String getZdivision() {
		return zdivision;
	}
	
	private ZmedTrxhdrResponseWrapper setZdivision(String zdivision) {
		this.zdivision = zdivision;
		return this;
	}
	
	
	
	@JsonProperty("ename")
	public String getEname() {
		return ename;
	}
	
	private ZmedTrxhdrResponseWrapper setEname(String ename) {
		this.ename = ename;
		return this;
	}
	
	

	@JsonProperty("clmst")
	public String getClmst() {
		return clmst;
	}
	
	private ZmedTrxhdrResponseWrapper setClmst(String clmst) {
		this.clmst = clmst;
		return this;
	}
	
	
	
	@JsonProperty("kodemed")
	public String getKodemed() {
		return kodemed;
	}
	
	private ZmedTrxhdrResponseWrapper setKodemed(String kodemed) {
		this.kodemed = kodemed;
		return this;
	}
	
	
	
	@JsonProperty("medtydesc")
	public String getMedtydesc() {
		return medtydesc;
	}
	
	private ZmedTrxhdrResponseWrapper setMedtydesc(String medtydesc) {
		this.medtydesc = medtydesc;
		return this;
	}
	


	@JsonProperty("revby")
	public String getRevby() {
		return revby;
	}
	
	private ZmedTrxhdrResponseWrapper setRevby(String revby) {
		this.revby = revby;
		return this;
	}
	
	
	@JsonProperty("revdt")
	public String getRevdt() {
		return revdt;
	}
	
	private ZmedTrxhdrResponseWrapper setRevdt(String revdt) {
		this.revdt = revdt;
		return this;
	}
	
	
	
	@JsonProperty("rejamt")
	public String getRejamt() {
		return rejamt;
	}
	
	private ZmedTrxhdrResponseWrapper setRejamt(String rejamt) {
		this.rejamt = rejamt;
		return this;
	}
	


	@JsonProperty("appamt")
	public String getAppamt() {
		return appamt;
	}
	
	private ZmedTrxhdrResponseWrapper setAppamt(String appamt) {
		this.appamt = appamt;
		return this;
	}
	
	
	
	
	@JsonProperty("bill_date")
	public String getBilldt() {
		return billdt;
	}
	
	private ZmedTrxhdrResponseWrapper setBilldt(String billdt) {
		this.billdt = billdt;
		return this;
	}
	
	
	
	
	
	@JsonProperty("pagu1")
	public String getPagu1() {
		return pagu1;
	}
	
	private ZmedTrxhdrResponseWrapper setPagu1(String pagu1) {
		this.pagu1 = pagu1;
		return this;
	}
	


	@JsonProperty("pagu2")
	public String getPagu2() {
		return pagu2;
	}
	
	private ZmedTrxhdrResponseWrapper setPagu2(String pagu2) {
		this.pagu2 = pagu2;
		return this;
	}
	
	
	@JsonProperty("clmamt")
	public String getClmamt() {
		return clmamt;
	}
	
	private ZmedTrxhdrResponseWrapper setClmamt(String clmamt) {
		this.clmamt = clmamt;
		return this;
	}
	
	
	
	@JsonProperty("pagudent")
	public String getPagudent() {
		return pagudent;
	}
	
	private ZmedTrxhdrResponseWrapper setPagudent(String pagudent) {
		this.pagudent = pagudent;
		return this;
	}
	


	@JsonProperty("pagu2byemp")
	public String getPagu2byemp() {
		return pagu2byemp;
	}
	
	private ZmedTrxhdrResponseWrapper setPagu2byemp(String pagu2byemp) {
		this.pagu2byemp = pagu2byemp;
		return this;
	}
	
	
	
	
	
	
	
	


	@JsonProperty("totamt")
	public String getTotamt() {
		return totamt;
	}
	
	private ZmedTrxhdrResponseWrapper setTotamt(String totamt) {
		this.totamt = totamt;
		return this;
	}
	
	
	@JsonProperty("pasien")
	public String getPasien() {
		return pasien;
	}
	
	private ZmedTrxhdrResponseWrapper setPasien(String pasien) {
		this.pasien = pasien;
		return this;
	}
	
	
	
	@JsonProperty("penyakit")
	public String getPenyakit() {
		return penyakit;
	}
	
	private ZmedTrxhdrResponseWrapper setPenyakit(String penyakit) {
		this.penyakit = penyakit;
		return this;
	}
	


	@JsonProperty("notex")
	public String getNotex() {
		return notex;
	}
	
	private ZmedTrxhdrResponseWrapper setNotex(String notex) {
		this.notex = notex;
		return this;
	}
	
	@JsonProperty("tglmasuk")
	public String getTglmasuk() {
		return tglmasuk;
	}
	
	private ZmedTrxhdrResponseWrapper setTglmasuk(String tglmasuk) {
		this.tglmasuk = tglmasuk;
		return this;
	}
	
	@JsonProperty("tglkeluar")
	public String getTglkeluar() {
		return tglkeluar;
	}
	
	private ZmedTrxhdrResponseWrapper setTglkeluar(String tglkeluar) {
		this.tglkeluar = tglkeluar;
		return this;
	}
	
	@JsonProperty("clmdt")
	public String getClmdt() {
		return clmdt;
	}
	
	private ZmedTrxhdrResponseWrapper setClmdt(String clmdt) {
		this.clmdt = clmdt;
		return this;
	}
	
	@JsonProperty("aprdt")
	public String getAprdt() {
		return aprdt;
	}
	
	private ZmedTrxhdrResponseWrapper setAprdt(String aprdt) {
		this.aprdt = aprdt;
		return this;
	}
	
	@JsonProperty("paydt")
	public String getPaydt() {
		return paydt;
	}
	
	private ZmedTrxhdrResponseWrapper setPaydt(String paydt) {
		this.paydt = paydt;
		return this;
	}
	
	@JsonProperty("clmby")
	public String getClmby() {
		return clmby;
	}
	
	private ZmedTrxhdrResponseWrapper setClmby(String clmby) {
		this.clmby = clmby;
		return this;
	}
	
	
	
	@JsonProperty("aprby")
	public String getAprby() {
		return aprby;
	}
	
	private ZmedTrxhdrResponseWrapper setAprby(String aprby) {
		this.aprby = aprby;
		return this;
	}
	
	
	
	@JsonProperty("payby")
	public String getPayby() {
		return payby;
	}
	
	private ZmedTrxhdrResponseWrapper setPayby(String payby) {
		this.payby = payby;
		return this;
	}
	
	@JsonProperty("paymethod")
	public String getPaymethod() {
		return paymethod;
	}
	
	private ZmedTrxhdrResponseWrapper setPaymethod(String paymethod) {
		this.paymethod = paymethod;
		return this;
	}
	
	
	
	@JsonProperty("belnr")
	public String getBelnr() {
		return belnr;
	}
	
	private ZmedTrxhdrResponseWrapper setBelnr(String belnr) {
		this.belnr = belnr;
		return this;
	}
	
	
	
	@JsonProperty("jamin")
	public String getJamin() {
		return jamin;
	}
	
	private ZmedTrxhdrResponseWrapper setJamin(String jamin) {
		this.jamin = jamin;
		return this;
	}
	
	@JsonProperty("process")
	public String getProcess() {
		return process;
	}
	
	private ZmedTrxhdrResponseWrapper setProcess(String process) {
		this.process = process;
		return this;
	}

	@JsonProperty("finance_payment_status")
	public String getPaymentStatus() {
		return paymentStatus;
	}

	private ZmedTrxhdrResponseWrapper setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
		return this;
	}

	@JsonProperty("finance_payment_date")
	public String getPaymentDate() {
		return paymentDate;
	}

	private ZmedTrxhdrResponseWrapper setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
		return this;
	}

	@JsonProperty("finance_document_number")
	public String getDocno() {
		return docno;
	}

	private ZmedTrxhdrResponseWrapper setDocno(String docno) {
		this.docno = docno;
		return this;
	}

	@JsonProperty("finance_payment_status2")
	public String getPaymentStatus2() {
		return paymentStatus2;
	}

	private ZmedTrxhdrResponseWrapper setPaymentStatus2(String paymentStatus2) {
		this.paymentStatus2 = paymentStatus2;
		return this;
	}

	@JsonProperty("finance_document_number2")
	public String getDocno2() {
		return docno2;
	}

	private ZmedTrxhdrResponseWrapper setDocno2(String docno2) {
		this.docno2 = docno2;
		return this;
	}

	@JsonProperty("reason")
	public String getReason() {
		return reason;
	}

	private ZmedTrxhdrResponseWrapper setReason(String reason) {
		this.reason = reason;
		return this;
	}

	@JsonProperty("bank_name")
	public String getBanka() {
		return banka;
	}

	public ZmedTrxhdrResponseWrapper setBanka(String banka) {
		this.banka = banka;
		return this;
	}

	@JsonProperty("bank_account")
	public String getBankn() {
		return bankn;
	}

	public ZmedTrxhdrResponseWrapper setBankn(String bankn) {
		this.bankn = bankn;
		return this;
	}

}