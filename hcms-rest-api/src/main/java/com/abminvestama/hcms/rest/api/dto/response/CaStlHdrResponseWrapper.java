package com.abminvestama.hcms.rest.api.dto.response;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.constant.CaAdvanceCode;
import com.abminvestama.hcms.core.model.constant.SettlementCASStatus;
import com.abminvestama.hcms.core.model.constant.SettlementProcess;
import com.abminvestama.hcms.core.model.entity.CaStlDtl;
import com.abminvestama.hcms.core.model.entity.CaStlHdr;
import com.abminvestama.hcms.core.model.entity.TrxApprovalProcess;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class CaStlHdrResponseWrapper extends ResourceSupport {
	private String reqNo;
	private String tcaNo;
	private String docNo;
	private String docNo2;
	private Long pernr;
	private Double totalAmount;
	private Double diffAmount;
	private String currency;
	private String casStatus;
	private String casStatusTxt;
	private String createdAt;
	private String createdByFullname;
	private String updatedAt;
	private String updatedByFullname;
	private Integer process;
	private String status;
	private String notes;
	private String bukrs;
	private Double refundAmount;
	private Double claimAmount;
	private String lastSync;
	private String sapMessage;
	private String reason;
	private String advanceCode;
	private String advanceCodeTxt;
	private String attachment1Filename;
	
	private ArrayData<CaStlDtlResponseWrapper> detail;
	private ArrayData<TrxApprovalProcessResponseWrapper> approvalDetail;

	private CaStlHdrResponseWrapper() {}
	
	public CaStlHdrResponseWrapper(CaStlHdr stlHdr) {
		if (stlHdr == null) {
			new CaStlHdrResponseWrapper();
		}
		else {
			// SETTLEMENT DETAILS
			ArrayData<CaStlDtlResponseWrapper> bunchOfCaStlDtl = new ArrayData<>();
			APIResponseWrapper<ArrayData<CaStlDtlResponseWrapper>> response = new APIResponseWrapper<>();
			response.setData(bunchOfCaStlDtl);
			List<CaStlDtlResponseWrapper> records = new ArrayList<>();
			if (stlHdr.getStlDetail() != null && stlHdr.getStlDetail().size()>0) {
				List<CaStlDtl> stlDtl = stlHdr.getStlDetail();
				for (int i = 0; i < stlDtl.size(); i++) {
					records.add(new CaStlDtlResponseWrapper(stlDtl.get(i)));
				}
			}
			bunchOfCaStlDtl.setItems(records.toArray(new CaStlDtlResponseWrapper[records.size()]));
			
			// APPROVAL DETAILS
			ArrayData<TrxApprovalProcessResponseWrapper> bunchOfApprovalDtl = new ArrayData<>();
			APIResponseWrapper<ArrayData<TrxApprovalProcessResponseWrapper>> approvalResponse = new APIResponseWrapper<>();
			approvalResponse.setData(bunchOfApprovalDtl);
			List<TrxApprovalProcessResponseWrapper> approvalRecords = new ArrayList<>();
			if (stlHdr.getApprovalDetail() != null && stlHdr.getApprovalDetail().size()>0) {
				List<TrxApprovalProcess> item = stlHdr.getApprovalDetail();
				for (int i = 0; i < item.size(); i++) {
					approvalRecords.add(new TrxApprovalProcessResponseWrapper(item.get(i)));
				}
			}
			bunchOfApprovalDtl.setItems(approvalRecords.toArray(new TrxApprovalProcessResponseWrapper[approvalRecords.size()]));
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			String settlementStatus = ""; 
			for (SettlementProcess item : SettlementProcess.values()) {
				if (stlHdr.getProcess().intValue() == item.val) {
					settlementStatus = item.name().replace("_", " ");
					break;
				}
			}
			
			// SETTLEMENT HEADER
			this
			.setReqNo(stlHdr.getReqNo() != null ? stlHdr.getReqNo() : StringUtils.EMPTY)
			.setTcaNo(stlHdr.getTcaNo() != null ? stlHdr.getTcaNo().getReqNo() : StringUtils.EMPTY)
			.setDocNo(stlHdr.getDocNo() != null ? stlHdr.getDocNo() : StringUtils.EMPTY)
			.setDocNo2(stlHdr.getDocNo2() != null ? stlHdr.getDocNo2() : StringUtils.EMPTY)
			.setPernr(stlHdr.getPernr() != null ? stlHdr.getPernr() : null)
			.setTotalAmount(stlHdr.getTotalAmount() != null ? stlHdr.getTotalAmount() : 0)
			.setDiffAmount(stlHdr.getDiffAmount() != null ? stlHdr.getDiffAmount() : 0)
			.setCurrency(StringUtils.defaultString(stlHdr.getCurrency(), StringUtils.EMPTY))
			.setCasStatus(StringUtils.defaultString(stlHdr.getCasStatus(), StringUtils.EMPTY))
			.setCasStatusTxt(SettlementCASStatus.valueOf(stlHdr.getCasStatus()).description)
			.setCreatedAt(stlHdr.getCreatedAt() != null ? df.format(stlHdr.getCreatedAt()) : StringUtils.EMPTY)
			.setCreatedByFullname(stlHdr.getCreatedBy() != null ? stlHdr.getCreatedBy().getEmployee().getEname() : StringUtils.EMPTY)
			.setUpdatedAt(stlHdr.getUpdatedAt() != null ? df.format(stlHdr.getUpdatedAt()) : StringUtils.EMPTY )
			.setUpdatedByFullname(stlHdr.getUpdatedBy() != null ? stlHdr.getUpdatedBy().getEmployee().getEname() : StringUtils.EMPTY)
			.setProcess(stlHdr.getProcess())
			.setStatus(settlementStatus)
			.setNotes(stlHdr.getNotes() != null ? stlHdr.getNotes() : StringUtils.EMPTY)
			.setBukrs(stlHdr.getBukrs() != null ? stlHdr.getBukrs().getBukrs() : StringUtils.EMPTY)
			.setRefundAmount(stlHdr.getRefundAmount() != null ? stlHdr.getRefundAmount() : 0)
			.setClaimAmount(stlHdr.getClaimAmount() != null ? stlHdr.getClaimAmount() : 0)
			.setLastSync(stlHdr.getLastSync() != null ? df.format(stlHdr.getLastSync()) : StringUtils.EMPTY)
			.setSapMessage(stlHdr.getNotes() != null ? stlHdr.getNotes() : StringUtils.EMPTY)
			.setReason(stlHdr.getReason() != null ? stlHdr.getReason() : StringUtils.EMPTY)
			.setAdvanceCode(stlHdr.getTcaNo() != null ? stlHdr.getTcaNo().getAdvanceCode() : "")
			.setAdvanceCodeTxt(stlHdr.getTcaNo() != null ? CaAdvanceCode.valueOf(stlHdr.getTcaNo().getAdvanceCode()).description : "")
			.setAttachment1Filename(StringUtils.defaultString(stlHdr.getAttachment(), StringUtils.EMPTY))
			
			.setDetail(bunchOfCaStlDtl) // SETTLEMENT DETAILS
			.setApprovalDetail(bunchOfApprovalDtl) // APPROVAL DETAILS
			;
		}
	}

	@JsonProperty("req_no")
	public String getReqNo() {
		return reqNo;
	}

	public CaStlHdrResponseWrapper setReqNo(String reqNo) {
		this.reqNo = reqNo;
		return this;
	}

	@JsonProperty("tca_no")
	public String getTcaNo() {
		return tcaNo;
	}

	public CaStlHdrResponseWrapper setTcaNo(String tcaNo) {
		this.tcaNo = tcaNo;
		return this;
	}

	@JsonProperty("doc_no")
	public String getDocNo() {
		return docNo;
	}

	public CaStlHdrResponseWrapper setDocNo(String docNo) {
		this.docNo = docNo;
		return this;
	}

	@JsonProperty("doc_no2")
	public String getDocNo2() {
		return docNo2;
	}

	public CaStlHdrResponseWrapper setDocNo2(String docNo2) {
		this.docNo2 = docNo2;
		return this;
	}

	@JsonProperty("pernr")
	public Long getPernr() {
		return pernr;
	}

	public CaStlHdrResponseWrapper setPernr(Long pernr) {
		this.pernr = pernr;
		return this;
	}

	@JsonProperty("total_amount")
	public Double getTotalAmount() {
		return totalAmount;
	}

	public CaStlHdrResponseWrapper setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
		return this;
	}

	@JsonProperty("diff_amount")
	public Double getDiffAmount() {
		return diffAmount;
	}

	public CaStlHdrResponseWrapper setDiffAmount(Double diffAmount) {
		this.diffAmount = diffAmount;
		return this;
	}

	@JsonProperty("currency")
	public String getCurrency() {
		return currency;
	}

	public CaStlHdrResponseWrapper setCurrency(String currency) {
		this.currency = currency;
		return this;
	}

	@JsonProperty("cas_status")
	public String getCasStatus() {
		return casStatus;
	}

	public CaStlHdrResponseWrapper setCasStatus(String casStatus) {
		this.casStatus = casStatus;
		return this;
	}

	@JsonProperty("cas_status_txt")
	public String getCasStatusTxt() {
		return casStatusTxt;
	}

	public CaStlHdrResponseWrapper setCasStatusTxt(String casStatusTxt) {
		this.casStatusTxt = casStatusTxt;
		return this;
	}

	@JsonProperty("created_at")
	public String getCreatedAt() {
		return createdAt;
	}

	public CaStlHdrResponseWrapper setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	@JsonProperty("created_by_fullname")
	public String getCreatedByFullname() {
		return createdByFullname;
	}

	public CaStlHdrResponseWrapper setCreatedByFullname(String createdByFullname) {
		this.createdByFullname = createdByFullname;
		return this;
	}

	@JsonProperty("updated_at")
	public String getUpdatedAt() {
		return updatedAt;
	}

	public CaStlHdrResponseWrapper setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
		return this;
	}

	@JsonProperty("updated_by_fullname")
	public String getUpdatedByFullname() {
		return updatedByFullname;
	}

	public CaStlHdrResponseWrapper setUpdatedByFullname(String updatedByFullname) {
		this.updatedByFullname = updatedByFullname;
		return this;
	}

	@JsonProperty("process")
	public Integer getProcess() {
		return process;
	}

	public CaStlHdrResponseWrapper setProcess(Integer process) {
		this.process = process;
		return this;
	}

	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	public CaStlHdrResponseWrapper setStatus(String status) {
		this.status = status;
		return this;
	}

	@JsonProperty("notes")
	public String getNotes() {
		return notes;
	}

	public CaStlHdrResponseWrapper setNotes(String notes) {
		this.notes = notes;
		return this;
	}

	@JsonProperty("bukrs")
	public String getBukrs() {
		return bukrs;
	}

	public CaStlHdrResponseWrapper setBukrs(String bukrs) {
		this.bukrs = bukrs;
		return this;
	}

	@JsonProperty("refund_amount")
	public Double getRefundAmount() {
		return refundAmount;
	}

	public CaStlHdrResponseWrapper setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
		return this;
	}

	@JsonProperty("claim_amount")
	public Double getClaimAmount() {
		return claimAmount;
	}

	public CaStlHdrResponseWrapper setClaimAmount(Double claimAmount) {
		this.claimAmount = claimAmount;
		return this;
	}

	@JsonProperty("last_sync")
	public String getLastSync() {
		return lastSync;
	}

	public CaStlHdrResponseWrapper setLastSync(String lastSync) {
		this.lastSync = lastSync;
		return this;
	}

	@JsonProperty("sap_message")
	public String getSapMessage() {
		return sapMessage;
	}

	public CaStlHdrResponseWrapper setSapMessage(String sapMessage) {
		this.sapMessage = sapMessage;
		return this;
	}

	@JsonProperty("reason")
	public String getReason() {
		return reason;
	}

	public CaStlHdrResponseWrapper setReason(String reason) {
		this.reason = reason;
		return this;
	}

	@JsonProperty("advancecode")
	public String getAdvanceCode() {
		return advanceCode;
	}

	public CaStlHdrResponseWrapper setAdvanceCode(String advanceCode) {
		this.advanceCode = advanceCode;
		return this;
	}

	@JsonProperty("attachment1_filename")
	public String getAttachment1Filename() {
		return attachment1Filename;
	}

	public CaStlHdrResponseWrapper setAttachment1Filename(String attachment1Filename) {
		this.attachment1Filename = attachment1Filename;
		return this;
	}

	@JsonProperty("advancecodetxt")
	public String getAdvanceCodeTxt() {
		return advanceCodeTxt;
	}

	public CaStlHdrResponseWrapper setAdvanceCodeTxt(String advanceCodeTxt) {
		this.advanceCodeTxt = advanceCodeTxt;
		return this;
	}

	public ArrayData<CaStlDtlResponseWrapper> getDetail() {
		return detail;
	}

	public CaStlHdrResponseWrapper setDetail(ArrayData<CaStlDtlResponseWrapper> detail) {
		this.detail = detail;
		return this;
	}

	public ArrayData<TrxApprovalProcessResponseWrapper> getApprovalDetail() {
		return approvalDetail;
	}

	public CaStlHdrResponseWrapper setApprovalDetail(ArrayData<TrxApprovalProcessResponseWrapper> approvalDetail) {
		this.approvalDetail = approvalDetail;
		return this;
	}

}
