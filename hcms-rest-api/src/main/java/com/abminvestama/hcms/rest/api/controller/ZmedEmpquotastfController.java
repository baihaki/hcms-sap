package com.abminvestama.hcms.rest.api.controller;



import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedEmpquotastfCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotastfQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedEmpquotastfRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.ZmedEmpquotastfRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.ZmedEmpquotastfResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ZmedEmpquotastfResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.ZmedEmpquotastfRequestBuilderUtil;




@RestController
@RequestMapping("/api/v1/medical_empquotastf")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ZmedEmpquotastfController extends AbstractResource {
	
	private ZmedEmpquotastfCommandService zmedEmpquotastfCommandService;
	private ZmedEmpquotastfQueryService zmedEmpquotastfQueryService;
	private ZmedEmpquotastfRequestBuilderUtil zmedEmpquotastfRequestBuilderUtil;
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	@Autowired
	void setZmedEmpquotastfCommandService(ZmedEmpquotastfCommandService zmedEmpquotastfCommandService) {
		this.zmedEmpquotastfCommandService = zmedEmpquotastfCommandService;
	}
	
	@Autowired
	void setZmedEmpquotastfQueryService(ZmedEmpquotastfQueryService zmedEmpquotastfQueryService) {
		this.zmedEmpquotastfQueryService = zmedEmpquotastfQueryService;
	}
	
	@Autowired
	void setZmedEmpquotastfRequestBuilderUtil(ZmedEmpquotastfRequestBuilderUtil zmedEmpquotastfRequestBuilderUtil) {
		this.zmedEmpquotastfRequestBuilderUtil = zmedEmpquotastfRequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	
	
	

	
	
	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated ZmedEmpquotastfRequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> zmedEmpquotastfEntity = hcmsEntityQueryService.findByEntityName(ZmedEmpquotastf.class.getSimpleName());
			if (!zmedEmpquotastfEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + ZmedEmpquotastf.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<ZmedEmpquotastf, ZmedEmpquotastfRequestWrapper, String> newObjectContainer 
					= zmedEmpquotastfRequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				
				Optional<ZmedEmpquotastf> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
				//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = zmedEmpquotastfCommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create ZmedEmpquotastf data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("stell=" + savedEntity.get().getId().getStell());
				key.append(",bukrs=" + savedEntity.get().getId().getBukrs());
				key.append(",persg=" + savedEntity.get().getId().getPersg());
				key.append(",persk=" + savedEntity.get().getId().getPersk());
				key.append(",kodequo=" + savedEntity.get().getId().getKodequo());
				key.append(",datab=" + savedEntity.get().getId().getDatab());
				key.append(",datbi=" + savedEntity.get().getId().getDatbi());

				EventLog eventLog = new EventLog(zmedEmpquotastfEntity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				
				ZmedEmpquotastfResponseWrapper responseWrapper = new ZmedEmpquotastfResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(ZmedEmpquotastfController.class).create(request, result)).withSelfRel());		
		return response;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated ZmedEmpquotastfRequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			ZmedEmpquotastfResponseWrapper responseWrapper = new ZmedEmpquotastfResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<ZmedEmpquotastf> zmedEmpquotastf = zmedEmpquotastfQueryService.findOneByCompositeKey(
							request.getStell(), request.getBukrs(),
							request.getPersg(), request.getPersk(), request.getKodequo(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getDatab()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getDatbi()));
					if (!zmedEmpquotastf.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					RequestObjectComparatorContainer<ZmedEmpquotastf, ZmedEmpquotastfRequestWrapper, String> updatedContainer = zmedEmpquotastfRequestBuilderUtil.compareAndReturnUpdatedData(request, zmedEmpquotastf.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
				//	Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				//
				//	if (updatedContainer.getEntity().isPresent()) {
				//		updatedContainer.getEntity().get().setSeqnr(zmedEmpquotastf.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
				//		zmedEmpquotastf = zmedEmpquotastfCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				//	}
				
				//	if (!zmedEmpquotastf.isPresent()) {
				//		throw new CannotPersistException("Cannot update ZmedEmpquotastf (Personal Information) data. Please check your data!");
				//	}
				
					responseWrapper = new ZmedEmpquotastfResponseWrapper(zmedEmpquotastf.get());
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(ZmedEmpquotastfController.class).edit(request, result)).withSelfRel());
		return response;
	}	
	
		
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}
	
	
}
