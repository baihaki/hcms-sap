package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class WorkflowRequestWrapper {

	private String name;
	private String description;
	
	WorkflowRequestWrapper() {}
	
	public WorkflowRequestWrapper(String name) {
		this.name = name;
	}
	
	@JsonProperty("workflow_name")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@JsonProperty("workflow_description")
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
}