package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add attach1Type, attach1Content, attach1Filename field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public class IT0009RequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = 2539645779455671060L;

	private double betrg;
	private String waers;
	private double anzhl;
	private String bnksa;
	private String zlsch;
	private String emftx;
	private String bkplz;
	private String bkort;
	private String banks;
	private String bankl;
	private String bankn;
	private String zweck;

	private String attach1Type;
	private String attach1Content;
	private String attach1Filename;
	
	@JsonProperty("standard_value")
	public double getBetrg() {
		return betrg;
	}
	
	@JsonProperty("payment_currency")
	public String getWaers() {
		return waers;
	}
	
	@JsonProperty("standard_percentage")
	public double getAnzhl() {
		return anzhl;
	}
	
	@JsonProperty("bank_details_type")
	public String getBnksa() {
		return bnksa;
	}
	
	@JsonProperty("payment_method")
	public String getZlsch() {
		return zlsch;
	}
	
	@JsonProperty("payee")
	public String getEmftx() {
		return emftx;
	}
	
	@JsonProperty("postal_code")
	public String getBkplz() {
		return bkplz;
	}
	
	@JsonProperty("city")
	public String getBkort() {
		return bkort;
	}
	
	@JsonProperty("bank_country")
	public String getBanks() {
		return banks;
	}
	
	@JsonProperty("bank_key")
	public String getBankl() {
		return bankl;
	}
	
	@JsonProperty("bank_account")
	public String getBankn() {
		return bankn;
	}
	
	@JsonProperty("purpose")
	public String getZweck() {
		return zweck;
	}
	
	@JsonProperty("attachment1_type")
	public String getAttach1Type() {
		return attach1Type;
	}
	
	@JsonProperty("attachment1_base64")
	public String getAttach1Content() {
		return attach1Content;
	}
	
	@JsonProperty("attachment1_filename")
	public String getAttach1Filename() {
		return attach1Filename;
	}
}