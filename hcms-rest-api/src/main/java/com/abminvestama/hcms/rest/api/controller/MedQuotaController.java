package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyQuotaused;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedEmpquotaCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedEmpquotasmCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedEmpquotastfCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedQuotausedCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotaQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotasmQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotastfQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedQuotausedQueryService;
import com.abminvestama.hcms.core.service.helper.ZmedQuotausedMapper;
import com.abminvestama.hcms.core.service.helper.ZmedQuotausedSoapObject;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.request.MedQuota2RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.MedQuota1ResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.MedQuota2ResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ZmedMedtypeResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.ZmedEmpquotaRequestBuilderUtil;
import com.abminvestama.hcms.rest.api.helper.ZmedEmpquotasmRequestBuilderUtil;
import com.abminvestama.hcms.rest.api.helper.ZmedEmpquotastfRequestBuilderUtil;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add Fetch from MuleSoft on findQuotamt method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/medical_quotas")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class MedQuotaController extends AbstractResource {


	private IT0001QueryService it0001QueryService;
	private ZmedQuotausedQueryService zmedQuotausedQueryService;
	
	private ZmedEmpquotaCommandService zmedEmpquotaCommandService;
	private ZmedEmpquotaQueryService zmedEmpquotaQueryService;
	private ZmedEmpquotaRequestBuilderUtil zmedEmpquotaRequestBuilderUtil;
	

	private ZmedEmpquotasmCommandService zmedEmpquotasmCommandService;
	private ZmedEmpquotasmQueryService zmedEmpquotasmQueryService;
	private ZmedEmpquotasmRequestBuilderUtil zmedEmpquotasmRequestBuilderUtil;
	

	private ZmedEmpquotastfCommandService zmedEmpquotastfCommandService;
	private ZmedEmpquotastfQueryService zmedEmpquotastfQueryService;
	private ZmedEmpquotastfRequestBuilderUtil zmedEmpquotastfRequestBuilderUtil;
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	private UserQueryService userQueryService;
	private Validator itValidator;
	
	private ZmedQuotausedCommandService zmedQuotausedCommandService;
	private MuleConsumerService muleConsumerService;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	
	
	@Autowired
	void setIT0001QueryService(IT0001QueryService it0001QueryService) {
		this.it0001QueryService = it0001QueryService;
	}
	
	
	
	

	@Autowired
	void setZmedQuotausedQueryService(ZmedQuotausedQueryService zmedQuotausedQueryService) {
		this.zmedQuotausedQueryService = zmedQuotausedQueryService;
	}
	
	
	
	
	
	@Autowired
	void setZmedEmpquotaCommandService(ZmedEmpquotaCommandService zmedEmpquotaCommandService) {
		this.zmedEmpquotaCommandService = zmedEmpquotaCommandService;
	}
	
	@Autowired
	void setZmedEmpquotaQueryService(ZmedEmpquotaQueryService zmedEmpquotaQueryService) {
		this.zmedEmpquotaQueryService = zmedEmpquotaQueryService;
	}
	
	@Autowired
	void setZmedEmpquotaRequestBuilderUtil(ZmedEmpquotaRequestBuilderUtil zmedEmpquotaRequestBuilderUtil) {
		this.zmedEmpquotaRequestBuilderUtil = zmedEmpquotaRequestBuilderUtil;
	}
	
	
	
	
	@Autowired
	void setZmedEmpquotasmCommandService(ZmedEmpquotasmCommandService zmedEmpquotasmCommandService) {
		this.zmedEmpquotasmCommandService = zmedEmpquotasmCommandService;
	}
	
	@Autowired
	void setZmedEmpquotasmQueryService(ZmedEmpquotasmQueryService zmedEmpquotasmQueryService) {
		this.zmedEmpquotasmQueryService = zmedEmpquotasmQueryService;
	}
	
	@Autowired
	void setZmedEmpquotasmRequestBuilderUtil(ZmedEmpquotasmRequestBuilderUtil zmedEmpquotasmRequestBuilderUtil) {
		this.zmedEmpquotasmRequestBuilderUtil = zmedEmpquotasmRequestBuilderUtil;
	}
	
	
	
	
	@Autowired
	void setZmedEmpquotastfCommandService(ZmedEmpquotastfCommandService zmedEmpquotastfCommandService) {
		this.zmedEmpquotastfCommandService = zmedEmpquotastfCommandService;
	}
	
	@Autowired
	void setZmedEmpquotastfQueryService(ZmedEmpquotastfQueryService zmedEmpquotastfQueryService) {
		this.zmedEmpquotastfQueryService = zmedEmpquotastfQueryService;
	}
	
	@Autowired
	void setZmedEmpquotastfRequestBuilderUtil(ZmedEmpquotastfRequestBuilderUtil zmedEmpquotastfRequestBuilderUtil) {
		this.zmedEmpquotastfRequestBuilderUtil = zmedEmpquotastfRequestBuilderUtil;
	}
	
	
	
	
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@Autowired
	void setZmedQuotausedCommandService(ZmedQuotausedCommandService zmedQuotausedCommandService) {
		this.zmedQuotausedCommandService = zmedQuotausedCommandService;
	}
	
	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}
	
	
	
	
	
	
	
	//secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
		//@SuppressWarnings("deprecation")
		@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, //POST -- GET changed
			consumes = MediaType.APPLICATION_JSON_VALUE)
		public APIResponseWrapper<?> findQuotamt(@RequestBody @Validated MedQuota2RequestWrapper request, BindingResult result)
				throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
			
			
			//APIResponseWrapper<?> response = null;
			ArrayData<MedQuota2ResponseWrapper> bunchOfZmed = new ArrayData<>();
			APIResponseWrapper<ArrayData<MedQuota2ResponseWrapper>> response = new APIResponseWrapper<>();
			response.setData(bunchOfZmed);
	
			//response = new APIResponseWrapper<>();

			List<MedQuota2ResponseWrapper> records = new ArrayList<>();
			
			try {

				/*
				 * Getting data from it0001 by matching pernr
				 */				
				String bukrs = "";
				String persg = "";
				String persk = "";
				long stell = 0;
				
				try {
					Long numOfPernr = request.getPernr();//
					
					List<IT0001> listOfIT0001 = it0001QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
					
					if (listOfIT0001.isEmpty()) {
						response.setMessage("No Data Found in it0001");
					} else {
						int length = listOfIT0001.size();
						
						for (int i = 0; i < length; i++) {
							IT0001 it0001 = listOfIT0001.get(i);
							//if(!CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31"), it0001.getId().getEndda())) {
							if(it0001.getId().getEndda().compareTo(CommonDateFunction.convertDateRequestParameterIntoDate(request.getDatbi())) >= 0) {
								bukrs = it0001.getT500p().getBukrs().getBukrs();
								persg = it0001.getT501t().getPersg();
								persk = it0001.getT503k().getMandt();
								stell = it0001.getT513s().getStell();
								break;
							}
						}
					}
				} catch (NumberFormatException e) { 
					throw e;
				} catch (UsernameNotFoundException e) {
					throw e;
				} catch (Exception e) {
					throw e;
				}
				
				/*
				 * 
				 * reading zmed_empquotasm
				 */
				try {
					String datab = request.getDatab();//
					String datbi = request.getDatbi();//
					Long pernr = request.getPernr();//
					Integer year = Integer.parseInt(datab.substring(0, 4));////
					
					
					
					List<ZmedEmpquotasm> listOfZmedsm = new ArrayList<>();
					listOfZmedsm = zmedEmpquotasmQueryService.findQuotas(pernr, bukrs, persg, persk,  
							Date.valueOf(datab), 
							Date.valueOf(datbi));

					List<ZmedQuotaused> listOfZmedQuota = new ArrayList<>();
					//listOfZmedQuota = zmedQuotausedQueryService.findQuotas(year, bukrs, pernr);
					

					// ************ Begin of Fetch from MuleSoft
					
					ZmedQuotaused zmedQuotaused = new ZmedQuotaused(new ZmedCompositeKeyQuotaused(
							year, null, pernr, null));
					ZmedQuotausedSoapObject zmedQuotausedSoapObject = new ZmedQuotausedSoapObject(zmedQuotaused);
					List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.ZMEDQUOTAUSED_FETCH.service(),
							BAPIFunction.ZMEDQUOTAUSED_FETCH_URI.service(), zmedQuotausedSoapObject);
					// synchronize ZmedQuotaused fields (in different thread) with the fetched fields from SAP
					List<ZmedQuotaused> listAllZmedQuotaused = new ArrayList<ZmedQuotaused>();
					if (Integer.parseInt(list.get(0).get("rowCount") != null ? list.get(0).get("rowCount") : "0") > 0) { 
					    for (Map<String, String> map : list) {
					    	// BAPI always return null on bukrs field, hence set the bukrs as fetched from it0001 table
					    	map.put("bukrs", bukrs);
							ZmedQuotaused zmedQuotausedItem = new ZmedQuotausedMapper(map).getZmedQuotaused();
							if (zmedQuotausedItem.getId().getGjahr().equals(year) && zmedQuotausedItem.getId().getBukrs().equals(bukrs)
									&& zmedQuotausedItem.getId().getPernr().equals(pernr)) {
								listOfZmedQuota.add(zmedQuotausedItem);
							}
							listAllZmedQuotaused.add(zmedQuotausedItem);
						}
					    zmedQuotausedCommandService.threadUpdate(listAllZmedQuotaused);
					} else {
						// no result from SAP (any failure), then load from zmed_quotaused table
						listOfZmedQuota = zmedQuotausedQueryService.findQuotas(year, bukrs, pernr);
					}
					
					// ************ End of Fetch from MuleSoft
					
					
					

					if (!listOfZmedsm.isEmpty() /* && !listOfZmedQuota.isEmpty()*/) {
						
						//List<MedQuota2ResponseWrapper> records = new ArrayList<>();
						for (ZmedEmpquotasm Zmed : listOfZmedsm) {
							
							if(!listOfZmedQuota.isEmpty()){
								int found = 0;
								for (ZmedQuotaused Zmedq : listOfZmedQuota) {
									if(Zmedq.getId().getKodequo().equals(Zmed.getKodequo())){
										found = 1;
										records.add(new MedQuota2ResponseWrapper(Zmed, Zmedq));
										break;
									}
								}
								if(found == 0)
									records.add(new MedQuota2ResponseWrapper(Zmed));
							} else{
								records.add(new MedQuota2ResponseWrapper(Zmed));
							}							
						}
						//bunchOfZmed.setItems(records.toArray(new MedQuota2ResponseWrapper[records.size()]));						
					}
					else{
					
					/*
					 * 
					 * reading zmed_empquotastf
					 */
					List<ZmedEmpquotastf> listOfZmedstf = new ArrayList<>();
					listOfZmedstf = zmedEmpquotastfQueryService.findQuotas(stell, bukrs, persg, persk, 
							Date.valueOf(datab), 
							Date.valueOf(datbi));

					if (!listOfZmedstf.isEmpty() /* && !listOfZmedQuota.isEmpty()*/) {

						//List<MedQuota2ResponseWrapper> records = new ArrayList<>();
						for (ZmedEmpquotastf Zmed : listOfZmedstf) {

							if(!listOfZmedQuota.isEmpty()){
								int found = 0;
								for (ZmedQuotaused Zmedq : listOfZmedQuota) {
									if(Zmedq.getId().getKodequo().equals(Zmed.getKodequo())){
										found = 1;
										records.add(new MedQuota2ResponseWrapper(Zmed, Zmedq));
										break;
									}
								}
								if(found == 0)
									records.add(new MedQuota2ResponseWrapper(Zmed));
							} else{
								records.add(new MedQuota2ResponseWrapper(Zmed));
							}							
						}
						//bunchOfZmed.setItems(records.toArray(new MedQuota2ResponseWrapper[records.size()]));
					}
					else {
					

					/*
					 * 
					 * reading zmed_empquota
					 */
					List<ZmedEmpquota> listOfZmed = new ArrayList<>();
					listOfZmed = zmedEmpquotaQueryService.findQuotas(bukrs, persg, persk, 
							Date.valueOf(datab), 
							Date.valueOf(datbi));

					if (!listOfZmed.isEmpty() /* && !listOfZmedQuota.isEmpty()*/) {

						//List<MedQuota2ResponseWrapper> records = new ArrayList<>();
						for (ZmedEmpquota Zmed : listOfZmed) {

							if(!listOfZmedQuota.isEmpty()){
								int found = 0;
								for (ZmedQuotaused Zmedq : listOfZmedQuota) {
									if(Zmedq.getId().getKodequo().equals(Zmed.getKodequo())){
										found = 1;
										records.add(new MedQuota2ResponseWrapper(Zmed, Zmedq));
										break;
									}
								}
								if(found == 0)
									records.add(new MedQuota2ResponseWrapper(Zmed));
							} else{
								records.add(new MedQuota2ResponseWrapper(Zmed));
							}							
						}
						//bunchOfZmed.setItems(records.toArray(new MedQuota2ResponseWrapper[records.size()]));
					}
					}
					}

					bunchOfZmed.setItems(records.toArray(new MedQuota2ResponseWrapper[records.size()]));
					
				} catch (NumberFormatException e) { 
					throw e;
				} catch (UsernameNotFoundException e) {
					throw e;
				} catch (Exception e) {
					throw e;
				}	
								
			response.add(linkTo(methodOn(MedQuotaController.class).findQuotamt(request, result)).withSelfRel());		
			
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		//response.add(linkTo(methodOn(MedQuotaController.class).findQuotamt(request, result)).withSelfRel());
		return response;
	}
	
	

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}

}
