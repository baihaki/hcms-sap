package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.IT0184;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.service.api.business.query.IT0184QueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0184RequestWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData to handle Create, and Edit (Blood Type)</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("it0184RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT0184RequestBuilderUtil {

	private IT0184QueryService it0184QueryService;
	
	@Autowired
	void setIT0184QueryService(IT0184QueryService it0184QueryService) {
		this.it0184QueryService = it0184QueryService;
	}

	/**
	 * Compare IT0184 request payload with existing IT0184 in the database.
	 * Update existing IT0184 data with the latest data comes from the request payload. 
	 * 
	 * @param requestPayload request data 
	 * @param it0184DB current existing IT0184 in the database.
	 * @return updated IT0184 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0184, IT0184RequestWrapper, String> compareAndReturnUpdatedData(IT0184RequestWrapper requestPayload, IT0184 it0184DB) {
		
		RequestObjectComparatorContainer<IT0184, IT0184RequestWrapper, String> objectContainer = null;
		
		if (it0184DB == null) {
			it0184DB = new IT0184();
			try {
				Optional<IT0184> existingIT0184 = it0184QueryService.findOneByCompositeKey(
						requestPayload.getPernr(),  requestPayload.getSubty(), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda()));
				
				if (existingIT0184.isPresent()) {
					throw new DataViolationException("Duplicate IT0184 Data for [ssn=" + requestPayload.getPernr() + ",end_date=" +
							requestPayload.getEndda() + ",begin_date=" + requestPayload.getBegda());
				}
				
				it0184DB.setId(
						new ITCompositeKeys(requestPayload.getPernr(), SAPInfoType.RESUME_TEXTS.infoType(), requestPayload.getSubty(), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda())));
				it0184DB.setItxex(requestPayload.getItxex());
				it0184DB.setText1(requestPayload.getText1());
				it0184DB.setSeqnr(1L);
				it0184DB.setStatus(DocumentStatus.INITIAL_DRAFT);
				
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			
			objectContainer = new RequestObjectComparatorContainer<>(it0184DB, requestPayload);
			
		} else {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getItxex(), StringUtils.defaultString(it0184DB.getItxex(), StringUtils.EMPTY))) {
				it0184DB.setItxex(requestPayload.getItxex());
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getText1(), StringUtils.defaultString(it0184DB.getText1(), StringUtils.EMPTY))) {
				it0184DB.setText1(requestPayload.getText1());
				requestPayload.setDataChanged(true);
			}
			
			objectContainer = new RequestObjectComparatorContainer<>(it0184DB, requestPayload);
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = "";
		try {
			jsonString = objectMapper.writeValueAsString(requestPayload);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}		
		/*
		JSONObject json = new JSONObject(requestPayload);
		if (it0184DB.getStatus() != null)
			json.put("prev_document_status", it0184DB.getStatus().name());
		objectContainer.setEventData(json.toString());
		*/
		objectContainer.setEventData(jsonString);
		
		return objectContainer;
	}				
}