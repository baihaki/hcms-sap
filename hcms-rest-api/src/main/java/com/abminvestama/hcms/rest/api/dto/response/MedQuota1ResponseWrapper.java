package com.abminvestama.hcms.rest.api.dto.response;


import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class MedQuota1ResponseWrapper extends ResourceSupport {

	private String quotamt;
	private String quoleft;
	
	private MedQuota1ResponseWrapper() {}
	
	public MedQuota1ResponseWrapper(ZmedEmpquota zmed, ZmedQuotaused qused) {
		if (zmed == null) {
			new MedQuota1ResponseWrapper();
		} else {
			this
			.setQuotamt(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY))	
			.setQuoleft(StringUtils.defaultString(!qused.equals(null) ? ""+(zmed.getQuotamt()-qused.getQuotamt()):zmed.getQuotamt().toString(),StringUtils.EMPTY));
						
		}
	}
	
	public MedQuota1ResponseWrapper(ZmedEmpquota zmed) {
		if (zmed == null) {
			new MedQuota1ResponseWrapper();
		} else {
			this
			.setQuotamt(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY))	
			.setQuoleft(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY));
		}
	}
	
	public MedQuota1ResponseWrapper(ZmedEmpquotasm zmed, ZmedQuotaused qused) {
		if (zmed == null) {
			new MedQuota1ResponseWrapper();
		} else {
			this
			.setQuotamt(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY))	
			.setQuoleft(StringUtils.defaultString(!qused.equals(null) ? ""+(zmed.getQuotamt()-qused.getQuotamt()):zmed.getQuotamt().toString(),StringUtils.EMPTY));
						
		}
	}
	
	public MedQuota1ResponseWrapper(ZmedEmpquotasm zmed) {
		if (zmed == null) {
			new MedQuota1ResponseWrapper();
		} else {
			this
			.setQuotamt(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY))	
			.setQuoleft(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY));
		}
	}
	
	public MedQuota1ResponseWrapper(ZmedEmpquotastf zmed, ZmedQuotaused qused) {
		if (zmed == null) {
			new MedQuota1ResponseWrapper();
		} else {
			this
			.setQuotamt(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY))	
			.setQuoleft(StringUtils.defaultString(!qused.equals(null) ? ""+(zmed.getQuotamt()-qused.getQuotamt()):zmed.getQuotamt().toString(),StringUtils.EMPTY));
						
		}
	}
	

	public MedQuota1ResponseWrapper(ZmedEmpquotastf zmed) {
		if (zmed == null) {
			new MedQuota1ResponseWrapper();
		} else {
			this
			.setQuotamt(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY))	
			.setQuoleft(StringUtils.defaultString(zmed.getQuotamt().toString(), StringUtils.EMPTY));
		}
	}
	
	@JsonProperty("quotamt")
	public String getQuotamt() {
		return quotamt;
	}
	
	private MedQuota1ResponseWrapper setQuotamt(String quotamt) {
		this.quotamt = quotamt;
		return this;
	}
	

	@JsonProperty("quota_left")
	public String getQuoleft() {
		return quoleft;
	}
	
	private MedQuota1ResponseWrapper setQuoleft(String quoleft) {
		this.quoleft = quoleft;
		return this;
	}
}
