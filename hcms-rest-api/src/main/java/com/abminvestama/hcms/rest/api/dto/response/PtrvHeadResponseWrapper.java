package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.PtrvHead;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.11
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.11</td><td>Baihaki</td><td>Add fields: totalAmount, pulseRejectAmount, fuelRejectAmount</td></tr>
 *     <tr><td>1.0.10</td><td>Baihaki</td><td>Add fields: banka, bankn</td></tr>
 *     <tr><td>1.0.9</td><td>Baihaki</td><td>Add fields: paymentStatus2, docno2</td></tr>
 *     <tr><td>1.0.8</td><td>Baihaki</td><td>Add fields: paymentStatus, paymentDate, docno</td></tr>
 *     <tr><td>1.0.7</td><td>Baihaki</td><td>Add fields: reason</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Add fields: lastPrevMonth, lastThisMonth</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add fields: adminApprovedByUsername, adminApprovedByFullname, adminApprovedAt</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add fields: superiorApprovedByUsername, superiorApprovedByFullname, superiorApprovedAt</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add fields: createdByUsername, createdByFullname, createdAt</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add fields: bukrs, process, form</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add field: kunde</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class PtrvHeadResponseWrapper extends ResourceSupport {
	
	private Integer hdvrs;
	private Long pernr;
	private Long reinr;
	private String molga;
	private String morei;
	private String schem;
	private String zland;
	private Date datv1;
	private Date uhrv1;
	private Date datb1;
	private Date uhrb1;
	private Date dath1;
	private Date uhrh1;
	private Date datr1;
	private Date uhrr1;
	private Date endrg;
	private Date dates;
	private Date times;
	private String uname;
	private String repid;
	private Long dantn;
	private Long fintn;
	private String request;
	private String travelPlan;
	private String expenses;
	private Date stTrgtg;
	private Date stTrgall;
	private Date datReduc1;
	private Date datReduc2;
	private Date datv1Dienst;
	private Date uhrv1Dienst;
	private Date datb1Dienst;
	private Date uhrb1Dienst;
	private Long abordnung;
	private Date exchangeDate;
	private String zort1;
	private String kunde;
	  
	private String bukrs;
	private String process;
	private Short form;
	  
	private String createdByUsername;
	private String createdByFullname;
	private Date createdAt;
	private String superiorApprovedByUsername;
	private String superiorApprovedByFullname;
	private Date superiorApprovedAt;
	private String adminApprovedByUsername;
	private String adminApprovedByFullname;
	private Date adminApprovedAt;
	private String lastPrevMonth;
	private String lastThisMonth;
	
	private String reason;
	
	private String paymentStatus;
	private String paymentDate;
	private String docno;

	private String paymentStatus2;
	private String docno2;
	private String banka;
	private String bankn;
	
	private String totalAmount;
	private String pulseRejectAmount;
	private String fuelRejectAmount;
	
	private PtrvHeadResponseWrapper() {}
	
	public PtrvHeadResponseWrapper(PtrvHead ptrvHead) {
		if (ptrvHead == null) {
			new PtrvHeadResponseWrapper();
		} else {
			JSONObject json = new JSONObject();
			if (ptrvHead.getRejectAmounts() != null) {
				try {
				    json = new JSONObject(ptrvHead.getRejectAmounts());
				} catch (Exception e) {
					// do nothing
				}
			}
			this
			.setHdvrs(ptrvHead.getId().getHdvrs())
			.setPernr(ptrvHead.getId().getPernr())			
			.setReinr(ptrvHead.getReinr() != null ? ptrvHead.getReinr() : ptrvHead.getId().getReinr())
			.setMolga(ptrvHead.getMolga() != null ? StringUtils.defaultString(ptrvHead.getMolga().getMolga(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setMorei(ptrvHead.getMorei() != null ? StringUtils.defaultString(ptrvHead.getMorei().getMorei(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setSchem(StringUtils.defaultString(ptrvHead.getSchem(), StringUtils.EMPTY))
			.setZland(StringUtils.defaultString(ptrvHead.getZland(), StringUtils.EMPTY))
			.setUname(StringUtils.defaultString(ptrvHead.getUname(), StringUtils.EMPTY))
			.setRepid(StringUtils.defaultString(ptrvHead.getRepid(), StringUtils.EMPTY))
			.setDantn(ptrvHead.getDantn())
			.setExchangeDate(ptrvHead.getExchangeDate())
			.setDatv1(ptrvHead.getDatv1())
			.setUhrv1(ptrvHead.getUhrv1())
			.setDatb1(ptrvHead.getDatb1())
			.setUhrb1(ptrvHead.getUhrb1())
			.setDath1(ptrvHead.getDath1())
			.setUhrh1(ptrvHead.getUhrh1()) 
			.setDatr1(ptrvHead.getDatr1())
			.setUhrr1(ptrvHead.getUhrr1())
			.setEndrg(ptrvHead.getEndrg())
			.setDates(ptrvHead.getDates())
			.setTimes(ptrvHead.getTimes())
			.setStTrgtg(ptrvHead.getStTrgtg())
			.setStTrgall(ptrvHead.getStTrgall())
			.setDatReduc1(ptrvHead.getDatReduc1())
			.setDatReduc2(ptrvHead.getDatReduc2())
			.setDatv1Dienst(ptrvHead.getDatv1Dienst())
			.setUhrv1Dienst(ptrvHead.getUrhv1Dienst())
			.setDatb1Dienst(ptrvHead.getDatb1Dienst())			
			.setUhrb1Dienst(ptrvHead.getUhrb1Dienst())
			.setFintn(ptrvHead.getFintn())
			.setRequest(StringUtils.defaultString(ptrvHead.getRequest(), StringUtils.EMPTY))
			.setTravelPlan(StringUtils.defaultString(ptrvHead.getTravelPlan(), StringUtils.EMPTY))
			.setExpenses(StringUtils.defaultString(ptrvHead.getExpenses(), StringUtils.EMPTY))
			.setZort1(StringUtils.defaultString(ptrvHead.getZort1(), StringUtils.EMPTY))
			.setKunde(StringUtils.defaultString(ptrvHead.getKunde(), StringUtils.EMPTY))
			.setAbordnung(ptrvHead.getAbordnung())
			.setBukrs(StringUtils.defaultString(ptrvHead.getBukrs().getBukrs(), StringUtils.EMPTY))
			.setProcess(StringUtils.defaultString(ptrvHead.getProcess(), StringUtils.EMPTY))
			.setForm(ptrvHead.getForm())
			.setCreatedByUsername(ptrvHead.getCreatedBy() != null ? ptrvHead.getCreatedBy().getUsername() : StringUtils.EMPTY)
			.setCreatedByFullname(ptrvHead.getCreatedBy() != null ? ptrvHead.getCreatedBy().getEmployee().getEname() : StringUtils.EMPTY)
			.setCreatedAt(ptrvHead.getCreatedAt())
			.setSuperiorApprovedByUsername(ptrvHead.getSuperiorApprovedBy() != null ? ptrvHead.getSuperiorApprovedBy().getUsername() : StringUtils.EMPTY)
			.setSuperiorApprovedByFullname(ptrvHead.getSuperiorApprovedBy() != null ? ptrvHead.getSuperiorApprovedBy().getEmployee().getEname() : StringUtils.EMPTY)
			.setSuperiorApprovedAt(ptrvHead.getSuperiorApprovedAt())
			.setAdminApprovedByUsername(ptrvHead.getAdminApprovedBy() != null ? ptrvHead.getAdminApprovedBy().getUsername() : StringUtils.EMPTY)
			.setAdminApprovedByFullname(ptrvHead.getAdminApprovedBy() != null ? ptrvHead.getAdminApprovedBy().getEmployee().getEname() : StringUtils.EMPTY)
			.setAdminApprovedAt(ptrvHead.getAdminApprovedAt())
			.setLastPrevMonth(ptrvHead.getLastPrevMonth() != null ? ptrvHead.getLastPrevMonth().toString() : StringUtils.EMPTY)
			.setLastThisMonth(ptrvHead.getLastThisMonth() != null ? ptrvHead.getLastThisMonth().toString() : StringUtils.EMPTY)
			.setReason(StringUtils.defaultString(ptrvHead.getReason(), StringUtils.EMPTY))
			.setPaymentStatus(StringUtils.defaultString(ptrvHead.getPaymentStatus(), StringUtils.EMPTY))
			.setPaymentDate(ptrvHead.getPaymentDate() != null ? CommonDateFunction.convertDateToStringYMD(ptrvHead.getPaymentDate()) : StringUtils.EMPTY)
			.setDocno(StringUtils.defaultString(ptrvHead.getDocno(), StringUtils.EMPTY))
			.setPaymentStatus2(StringUtils.defaultString(ptrvHead.getPaymentStatus2(), StringUtils.EMPTY))
			.setDocno2(StringUtils.defaultString(ptrvHead.getDocno2(), StringUtils.EMPTY))
			.setBanka(StringUtils.defaultString(ptrvHead.getBanka(), StringUtils.EMPTY))
			.setBankn(StringUtils.defaultString(ptrvHead.getBankn(), StringUtils.EMPTY))
			.setTotalAmount(ptrvHead.getTotalAmount() != null ? ptrvHead.getTotalAmount().toString() : StringUtils.EMPTY)
			.setPulseRejectAmount(String.valueOf(json.optDouble("CP01", 0)))
			.setFuelRejectAmount(String.valueOf(json.optDouble("GA01", 0)));
		}
	}
	


	

	@JsonProperty("hdvrs")
	public Integer getHdvrs() {
		return hdvrs;
	}
	
	private PtrvHeadResponseWrapper setHdvrs(Integer hdvrs) {
		this.hdvrs = hdvrs;
		return this;
	}
	


	
	
	@JsonProperty("ssn")
	public Long getPernr() {
		return pernr;
	}
	
	private PtrvHeadResponseWrapper setPernr(Long pernr) {
		this.pernr = pernr;
		return this;
	}
	


	
	
	@JsonProperty("reinr")
	public Long getReinr() {
		return reinr;
	}
	
	private PtrvHeadResponseWrapper setReinr(Long reinr) {
		this.reinr = reinr;
		return this;
	}
	
	
	@JsonProperty("molga")
	public String getMolga() {
		return molga;
	}
	
	private PtrvHeadResponseWrapper setMolga(String molga) {
		this.molga = molga;
		return this;
	}
	


	
	
	@JsonProperty("morei")
	public String getMorei() {
		return morei;
	}
	
	private PtrvHeadResponseWrapper setMorei(String morei) {
		this.morei = morei;
		return this;
	}
	


		
	
	@JsonProperty("schem")
	public String getSchem() {
		return schem;
	}
	
	private PtrvHeadResponseWrapper setSchem(String schem) {
		this.schem = schem;
		return this;
	}
	


	
	
	@JsonProperty("zland")
	public String getZland() {
		return zland;
	}
	
	private PtrvHeadResponseWrapper setZland(String zland) {
		this.zland = zland;
		return this;
	}
	


	
	
	@JsonProperty("uname")
	public String getUname() {
		return uname;
	}
	
	private PtrvHeadResponseWrapper setUname(String uname) {
		this.uname = uname;
		return this;
	}
	
	
	@JsonProperty("repid")
	public String getRepid() {
		return repid;
	}
	
	private PtrvHeadResponseWrapper setRepid(String repid) {
		this.repid = repid;
		return this;
	}
	


	
	
	@JsonProperty("dantn")
	public Long getDantn() {
		return dantn;
	}
	
	private PtrvHeadResponseWrapper setDantn(Long dantn) {
		this.dantn = dantn;
		return this;
	}
	

	@JsonProperty("fintn")
	public Long getFintn() {
		return fintn;
	}
	
	private PtrvHeadResponseWrapper setFintn(Long fintn) {
		this.fintn = fintn;
		return this;
	}
	


	
	
	@JsonProperty("request")
	public String getRequest() {
		return request;
	}
	
	private PtrvHeadResponseWrapper setRequest(String request) {
		this.request = request;
		return this;
	}
	


	
	
	@JsonProperty("travel_plan")
	public String getTravelPlan() {
		return travelPlan;
	}
	
	private PtrvHeadResponseWrapper setTravelPlan(String travelPlan) {
		this.travelPlan = travelPlan;
		return this;
	}
	

	
	

	@JsonProperty("expenses")
	public String getExpenses() {
		return expenses;
	}
	
	private PtrvHeadResponseWrapper setExpenses(String expenses) {
		this.expenses = expenses;
		return this;
	}
	


	
	
	@JsonProperty("zort1")
	public String getZort1() {
		return zort1;
	}
	
	private PtrvHeadResponseWrapper setZort1(String zort1) {
		this.zort1 = zort1;
		return this;
	}
	


	
	
	@JsonProperty("abordnung")
	public Long getAbordnung() {
		return abordnung;
	}
	
	private PtrvHeadResponseWrapper setAbordnung(Long abordnung) {
		this.abordnung = abordnung;
		return this;
	}
	

	
	
	

	
	@JsonProperty("exchange_date")
	public Date getExchangeDate() {
		return exchangeDate;
	}
	
	private PtrvHeadResponseWrapper setExchangeDate(Date exchangeDate) {
		this.exchangeDate = exchangeDate;
		return this;
	}
	
	
	
	@JsonProperty("datv1")
	public Date getDatv1() {
		return datv1;
	}
	
	private PtrvHeadResponseWrapper setDatv1(Date datv1) {
		this.datv1 = datv1;
		return this;
	}
	
	
	
	@JsonProperty("datb1")
	public Date getDatb1() {
		return datb1;
	}
	
	private PtrvHeadResponseWrapper setDatb1(Date datb1) {
		this.datb1 = datb1;
		return this;
	}
	
	
	
	@JsonProperty("uhrv1")
	public Date getUhrv1() {
		return uhrv1;
	}
	
	private PtrvHeadResponseWrapper setUhrv1(Date uhrv1) {
		this.uhrv1 = uhrv1;
		return this;
	}
	
	
	
	@JsonProperty("dath1")
	public Date getDath1() {
		return dath1;
	}
	
	private PtrvHeadResponseWrapper setDath1(Date dath1) {
		this.dath1 = dath1;
		return this;
	}
	
	
	
	@JsonProperty("uhrb1")
	public Date getUhrb1() {
		return uhrb1;
	}
	
	private PtrvHeadResponseWrapper setUhrb1(Date uhrb1) {
		this.uhrb1 = uhrb1;
		return this;
	}
	
	
	
	@JsonProperty("uhrh1")
	public Date getUhrh1() {
		return uhrh1;
	}
	
	private PtrvHeadResponseWrapper setUhrh1(Date uhrh1) {
		this.uhrh1 = uhrh1;
		return this;
	}
	
	
	
	@JsonProperty("datr1")
	public Date getDatr1() {
		return datr1;
	}
	
	private PtrvHeadResponseWrapper setDatr1(Date datr1) {
		this.datr1 = datr1;
		return this;
	}
	
	
	
	@JsonProperty("uhrr1")
	public Date getUhrr1() {
		return uhrr1;
	}
	
	private PtrvHeadResponseWrapper setUhrr1(Date uhrr1) {
		this.uhrr1 = uhrr1;
		return this;
	}
	
	
	
	@JsonProperty("endrg")
	public Date getEndrg() {
		return endrg;
	}
	
	private PtrvHeadResponseWrapper setEndrg(Date endrg) {
		this.endrg = endrg;
		return this;
	}
	
	
	
	@JsonProperty("times")
	public Date getTimes() {
		return times;
	}
	
	private PtrvHeadResponseWrapper setTimes(Date times) {
		this.times = times;
		return this;
	}
	
	

	@JsonProperty("dates")
	public Date getDates() {
		return dates;
	}
	
	private PtrvHeadResponseWrapper setDates(Date dates) {
		this.dates = dates;
		return this;
	}
	

	@JsonProperty("st_trgtg")
	public Date getStTrgtg() {
		return stTrgtg;
	}
	
	private PtrvHeadResponseWrapper setStTrgtg(Date stTrgtg) {
		this.stTrgtg = stTrgtg;
		return this;
	}
	
	
	

	@JsonProperty("st_trgall")
	public Date getStTrgall() {
		return stTrgall;
	}
	
	private PtrvHeadResponseWrapper setStTrgall(Date stTrgall) {
		this.stTrgall = stTrgall;
		return this;
	}
	
	
	@JsonProperty("dat_reduc1")
	public Date getDatReduc1() {
		return datReduc1;
	}
	
	private PtrvHeadResponseWrapper setDatReduc1(Date datReduc1) {
		this.datReduc1 = datReduc1;
		return this;
	}
	
	
	
	@JsonProperty("dat_reduc2")
	public Date getDatReduc2() {
		return datReduc2;
	}
	
	private PtrvHeadResponseWrapper setDatReduc2(Date datReduc2) {
		this.datReduc2 = datReduc2;
		return this;
	}
	
	
	@JsonProperty("datv1_dienst")
	public Date getDatv1Dienst() {
		return datv1Dienst;
	}
	
	private PtrvHeadResponseWrapper setDatv1Dienst(Date datv1Dienst) {
		this.datv1Dienst = datv1Dienst;
		return this;
	}
	
	
	
	@JsonProperty("datb1_dienst")
	public Date getDatb1Dienst() {
		return datb1Dienst;
	}
	
	private PtrvHeadResponseWrapper setDatb1Dienst(Date datb1Dienst) {
		this.datb1Dienst = datb1Dienst;
		return this;
	}
	
	
	@JsonProperty("uhrv1_dienst")
	public Date getUhrv1Dienst() {
		return uhrv1Dienst;
	}
	
	private PtrvHeadResponseWrapper setUhrv1Dienst(Date uhrv1Dienst) {
		this.uhrv1Dienst = uhrv1Dienst;
		return this;
	}
	
	
	
	@JsonProperty("uhrb1_dienst")
	public Date getUhrb1Dienst() {
		return uhrb1Dienst;
	}
	
	private PtrvHeadResponseWrapper setUhrb1Dienst(Date uhrb1Dienst) {
		this.uhrb1Dienst = uhrb1Dienst;
		return this;
	}
	
	@JsonProperty("kunde")
	public String getKunde() {
		return kunde;
	}
	
	private PtrvHeadResponseWrapper setKunde(String kunde) {
		this.kunde = kunde;
		return this;
	}
	
	@JsonProperty("bukrs")
	public String getBukrs() {
		return bukrs;
	}
	
	private PtrvHeadResponseWrapper setBukrs(String bukrs) {
		this.bukrs = bukrs;
		return this;
	}
	
	@JsonProperty("process")
	public String getProcess() {
		return process;
	}
	
	private PtrvHeadResponseWrapper setProcess(String process) {
		this.process = process;
		return this;
	}
	
	@JsonProperty("form")
	public Short getForm() {
		return form;
	}
	
	private PtrvHeadResponseWrapper setForm(Short form) {
		this.form = form;
		return this;
	}

	@JsonProperty("created_by_username")
	public String getCreatedByUsername() {
		return createdByUsername;
	}

	public PtrvHeadResponseWrapper setCreatedByUsername(String createdByUsername) {
		this.createdByUsername = createdByUsername;
		return this;
	}

	@JsonProperty("created_by_fullname")
	public String getCreatedByFullname() {
		return createdByFullname;
	}

	public PtrvHeadResponseWrapper setCreatedByFullname(String createdByFullname) {
		this.createdByFullname = createdByFullname;
		return this;
	}

	@JsonProperty("created_at")
	public Date getCreatedAt() {
		return createdAt;
	}

	public PtrvHeadResponseWrapper setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	@JsonProperty("superior_approved_by_username")
	public String getSuperiorApprovedByUsername() {
		return superiorApprovedByUsername;
	}

	public PtrvHeadResponseWrapper setSuperiorApprovedByUsername(String superiorApprovedByUsername) {
		this.superiorApprovedByUsername = superiorApprovedByUsername;
		return this;
	}

	@JsonProperty("superior_approved_by_fullname")
	public String getSuperiorApprovedByFullname() {
		return superiorApprovedByFullname;
	}

	public PtrvHeadResponseWrapper setSuperiorApprovedByFullname(String superiorApprovedByFullname) {
		this.superiorApprovedByFullname = superiorApprovedByFullname;
		return this;
	}

	@JsonProperty("superior_approved_at")
	public Date getSuperiorApprovedAt() {
		return superiorApprovedAt;
	}

	public PtrvHeadResponseWrapper setSuperiorApprovedAt(Date superiorApprovedAt) {
		this.superiorApprovedAt = superiorApprovedAt;
		return this;
	}

	@JsonProperty("admin_approved_by_username")
	public String getAdminApprovedByUsername() {
		return adminApprovedByUsername;
	}

	public PtrvHeadResponseWrapper setAdminApprovedByUsername(String adminApprovedByUsername) {
		this.adminApprovedByUsername = adminApprovedByUsername;
		return this;
	}

	@JsonProperty("admin_approved_by_fullname")
	public String getAdminApprovedByFullname() {
		return adminApprovedByFullname;
	}

	public PtrvHeadResponseWrapper setAdminApprovedByFullname(String adminApprovedByFullname) {
		this.adminApprovedByFullname = adminApprovedByFullname;
		return this;
	}

	@JsonProperty("admin_approved_at")
	public Date getAdminApprovedAt() {
		return adminApprovedAt;
	}

	public PtrvHeadResponseWrapper setAdminApprovedAt(Date adminApprovedAt) {
		this.adminApprovedAt = adminApprovedAt;
		return this;
	}

	@JsonProperty("last_previous_month")
	public String getLastPrevMonth() {
		return lastPrevMonth;
	}

	public PtrvHeadResponseWrapper setLastPrevMonth(String lastPrevMonth) {
		this.lastPrevMonth = lastPrevMonth;
		return this;
	}

	@JsonProperty("last_this_month")
	public String getLastThisMonth() {
		return lastThisMonth;
	}

	public PtrvHeadResponseWrapper setLastThisMonth(String lastThisMonth) {
		this.lastThisMonth = lastThisMonth;
		return this;
	}

	@JsonProperty("reason")
	public String getReason() {
		return reason;
	}

	public PtrvHeadResponseWrapper setReason(String reason) {
		this.reason = reason;
		return this;
	}

	@JsonProperty("finance_payment_status")
	public String getPaymentStatus() {
		return paymentStatus;
	}

	private PtrvHeadResponseWrapper setPaymentStatus(String paymentStatus) {
		/*if (process.equals("3")) {
			paymentStatus = "P";
		} else {
			paymentStatus = "N";
		}*/
		this.paymentStatus = paymentStatus;
		return this;
	}

	@JsonProperty("finance_payment_date")
	public String getPaymentDate() {
		return paymentDate;
	}

	private PtrvHeadResponseWrapper setPaymentDate(String paymentDate) {
		/*if (process.equals("3")) {
			paymentDate = CommonDateFunction.convertDateToStringYMD(new Date());
		}*/
		this.paymentDate = paymentDate;
		return this;
	}

	@JsonProperty("finance_document_number")
	public String getDocno() {
		return docno;
	}

	private PtrvHeadResponseWrapper setDocno(String docno) {
		/*if (process.equals("1") || process.equals("2")) {
			docno = "N".concat(reinr.toString());
		} else if (process.equals("3")) {
			docno = "P".concat(reinr.toString());
		}*/
		this.docno = docno;
		return this;
	}

	@JsonProperty("finance_payment_status2")
	public String getPaymentStatus2() {
		return paymentStatus2;
	}

	private PtrvHeadResponseWrapper setPaymentStatus2(String paymentStatus2) {
		/*if (process.equals("1") || process.equals("2")) {
			paymentStatus2 = "N";
		} else if (process.equals("3")) {
			paymentStatus2 = "P";
		}*/
		this.paymentStatus2 = paymentStatus2;
		return this;
	}

	@JsonProperty("finance_document_number2")
	public String getDocno2() {
		return docno2;
	}

	private PtrvHeadResponseWrapper setDocno2(String docno2) {
		/*if (process.equals("1") || process.equals("2")) {
			docno2 = "NN".concat(reinr.toString());
		} else if (process.equals("3")) {
			docno2 = "PP".concat(reinr.toString());
		}*/
		this.docno2 = docno2;
		return this;
	}

	@JsonProperty("bank_name")
	public String getBanka() {
		return banka;
	}

	public PtrvHeadResponseWrapper setBanka(String banka) {
		this.banka = banka;
		return this;
	}

	@JsonProperty("bank_account")
	public String getBankn() {
		return bankn;
	}

	public PtrvHeadResponseWrapper setBankn(String bankn) {
		this.bankn = bankn;
		return this;
	}

	@JsonProperty("total_amount")
	public String getTotalAmount() {
		return totalAmount;
	}

	public PtrvHeadResponseWrapper setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
		return this;
	}

	@JsonProperty("voucher_reject_amount")
	public String getPulseRejectAmount() {
		return pulseRejectAmount;
	}

	public PtrvHeadResponseWrapper setPulseRejectAmount(String pulseRejectAmount) {
		this.pulseRejectAmount = pulseRejectAmount;
		return this;
	}

	@JsonProperty("fuel_reject_amount")
	public String getFuelRejectAmount() {
		return fuelRejectAmount;
	}

	public PtrvHeadResponseWrapper setFuelRejectAmount(String fuelRejectAmount) {
		this.fuelRejectAmount = fuelRejectAmount;
		return this;
	}
	
}

