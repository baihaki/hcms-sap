package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.exception.WorkflowNotFoundException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0184;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0184CommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0184QueryService;
import com.abminvestama.hcms.core.service.api.business.query.ITQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.helper.IT0184SoapObject;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0184RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ApprovalEventLogResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.IT0184ResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.IT0184RequestBuilderUtil;

/**
 * 
 * @since 1.0.0
 * @version 1.0.6
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Rename findOneByPernrAndSubty to findByPernrAndSubty, add status parameter</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add methods: findByCriteria</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: approve, reject, getUpdatedFields</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Fix findOneByPernrAndSubty response data</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Modify findByPernr method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add create, findByDocumentStatus method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/resume_text")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class IT0184Controller extends AbstractResource {

	private IT0184CommandService it0184CommandService;
	private IT0184QueryService it0184QueryService;
	
	private IT0184RequestBuilderUtil it0184RequestBuilderUtil;
	private UserQueryService userQueryService;
	private Validator itValidator;
	
	private HCMSEntityQueryService hcmsEntityQueryService;
	private EventLogCommandService eventLogCommandService;

	private MuleConsumerService muleConsumerService;
	private CommonServiceFactory commonServiceFactory;
	private ApprovalController approvalController;
	private ITQueryService itQueryService;

	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}
	
	@Autowired
	void setIT0184CommandService(IT0184CommandService it0184CommandService) {
		this.it0184CommandService = it0184CommandService;
	}
	
	@Autowired
	void setIT0184QueryService(IT0184QueryService it0184QueryService) {
		this.it0184QueryService = it0184QueryService;
	}
	
	@Autowired
	void setIT0184RequestBuilderUtil(IT0184RequestBuilderUtil it0184RequestBuilderUtil) {
		this.it0184RequestBuilderUtil = it0184RequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	@Autowired
	void setApprovalController(ApprovalController approvalController) {
		this.approvalController = approvalController;
	}
	
	@Autowired
	void setITQueryService(ITQueryService itQueryService) {
		this.itQueryService = itQueryService;
	}
	
	@SuppressWarnings("unchecked")
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_criteria", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<IT0184ResponseWrapper>> findByCriteria(
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "exclude_status", required = false, defaultValue="PUBLISHED,DRAFT_REJECTED,UPDATE_REJECTED") String excludeStatus,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {		

		Page<IT0184ResponseWrapper> responsePage = new PageImpl<IT0184ResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<IT0184ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<IT0184> resultPage = null;
		String bukrs = null;
		Long pernr = null;
		String[] excludes = excludeStatus.split(",");
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		
		if (StringUtils.isNotEmpty(ssn)) {
			pernr = Long.valueOf(ssn);
			if (!currentUser.get().getRoles().contains(adminRole.get()) &&
					!currentUser.get().getEmployee().getPernr().equals(pernr) ) {
				throw new AuthorizationException(
						"Insufficient Privileges. You can't fetch data of other Employee.");
			}
		} else {
			// no specified ssn, MUST BE ADMIN
			if (!currentUser.get().getRoles().contains(adminRole.get())) {
				throw new AuthorizationException(
						"Insufficient Privileges. Please login as 'ADMIN'!");
			}
			bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
		}
		
		try {
			resultPage = (Page<IT0184>) itQueryService.fetchDistinctByCriteriaWithPaging(pageNumber, bukrs, pernr, excludes,
					new IT0184(), currentUser.get().getEmployee());
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			
			if (resultPage.hasContent()) {

				List<IT0184ResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(it0184 -> {
					records.add(new IT0184ResponseWrapper(it0184));
				});
				responsePage = new PageImpl<IT0184ResponseWrapper>(records, it0184QueryService.createPageRequest(pageNumber),
						resultPage.getTotalElements());
				response.setData(responsePage);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0184Controller.class).findByCriteria(ssn, excludeStatus, pageNumber)).withSelfRel());
		
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.PUT, value = "/approve")
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> approve(
			@RequestHeader(required = true, value = "ssn") String pernr,
			@RequestHeader(required = true, value = "subtype") String subtype,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda)
					throws AuthorizationException, NumberFormatException, CannotPersistException,
					DataViolationException, EntityNotFoundException, WorkflowNotFoundException, Exception {
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", pernr);
			keyMap.put("infotype", SAPInfoType.RESUME_TEXTS.infoType());
			keyMap.put("subtype", subtype);
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			keyMap.put("muleService", BAPIFunction.IT0184_CREATE.service());
			keyMap.put("muleUri", BAPIFunction.IT0184_CREATE_URI.service());
			
			String className = IT0184.class.getSimpleName();
			response = approvalController.approve(keyMap, className, it0184QueryService, it0184CommandService,
					IT0184SoapObject.class, it0184RequestBuilderUtil, new IT0184RequestWrapper());

		} catch (EntityNotFoundException e) {
			throw e;
		} catch (WorkflowNotFoundException e) {
			throw e;
		} catch (DataViolationException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0184Controller.class).approve(pernr, subtype, endda, begda)).withSelfRel());				
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.PUT, value = "/reject")
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> reject(
			@RequestHeader(required = true, value = "ssn") String pernr,
			@RequestHeader(required = true, value = "subtype") String subtype,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda) 
					throws AuthorizationException, NumberFormatException, CannotPersistException, 
					DataViolationException, EntityNotFoundException, Exception {
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", pernr);
			keyMap.put("infotype", SAPInfoType.RESUME_TEXTS.infoType());
			keyMap.put("subtype", subtype);
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			
			String className = IT0184.class.getSimpleName();
			response = approvalController.reject(keyMap, className, it0184QueryService, it0184CommandService);
			
		} catch (AuthorizationException e) { 
			throw e;
		} catch (NumberFormatException e) { 
			throw e;
		} catch (CannotPersistException e) {
			throw e;
		} catch (DataViolationException e) {
			throw e;
		} catch (EntityNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0184Controller.class).reject(pernr, subtype, endda, begda)).withSelfRel());		
		return response;
	}
	
	@SuppressWarnings("unchecked")
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_updated_fields")
	public APIResponseWrapper<IT0184ResponseWrapper> getUpdatedFields(
			@RequestHeader(required = true, value = "ssn") String ssn,
			@RequestHeader(required = true, value = "subtype") String subtype,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda
			) throws DataViolationException, EntityNotFoundException, Exception {

		APIResponseWrapper<IT0184ResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", ssn);
			keyMap.put("infotype", SAPInfoType.RESUME_TEXTS.infoType());
			keyMap.put("subtype", subtype);
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			
			String className = IT0184.class.getSimpleName();
			response = (APIResponseWrapper<IT0184ResponseWrapper>) approvalController.getUpdatedFields(
					keyMap, className, it0184QueryService, response, IT0184ResponseWrapper.class);
			
		} catch (DataViolationException e) { 
			throw e;
		} catch (EntityNotFoundException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0184Controller.class).getUpdatedFields(ssn, subtype, endda, begda)).withSelfRel());
		return response;
	}
	
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/status/{status}/page/{pageNumber}", 
	produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0184ResponseWrapper>> findByDocumentStatus(@PathVariable String status, 
			@PathVariable int pageNumber) throws Exception {
		ArrayData<IT0184ResponseWrapper> bunchOfIT0184 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0184ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0184);
		
		try {
			Page<IT0184> it0184Records = it0184QueryService.findByStatus(status, pageNumber);
			
			if (it0184Records == null || !it0184Records.hasContent()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0184ResponseWrapper> records = new ArrayList<>();
				
				it0184Records.forEach(it0184 -> {
					records.add(new IT0184ResponseWrapper(it0184));
				});
				
				bunchOfIT0184.setItems(records.toArray(new IT0184ResponseWrapper[records.size()]));
			}
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0184Controller.class).findByDocumentStatus(status, pageNumber)).withSelfRel());
		
		return response;
	}
		
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0184ResponseWrapper>> findByPernr(@PathVariable String pernr, 
			@RequestParam(value = "status", required = false) String status, 
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		ArrayData<IT0184ResponseWrapper> bunchOfIT0184 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0184ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0184);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0184> listOfIT0184 = new ArrayList<>();
			
			if (StringUtils.isNotEmpty(status)) {
				Page<IT0184> data = it0184QueryService.findByPernrAndStatus(numOfPernr, status, pageNumber);
				if (data.hasContent()) {
					listOfIT0184 = data.getContent();
				}
			} else {
				listOfIT0184 = it0184QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			}
			
			if (listOfIT0184.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0184ResponseWrapper> records = new ArrayList<>();
				for (IT0184 it0184 : listOfIT0184) {
					records.add(new IT0184ResponseWrapper(it0184));
				}
				bunchOfIT0184.setItems(records.toArray(new IT0184ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0184Controller.class).findByPernr(pernr, status, pageNumber)).withSelfRel());
		return response;
	}
	
	/*
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0184ResponseWrapper>> findByPernr(@PathVariable String pernr) throws Exception {
		ArrayData<IT0184ResponseWrapper> bunchOfIT0184 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0184ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0184);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0184> listOfIT0184 = it0184QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			
			if (listOfIT0184.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0184ResponseWrapper> records = new ArrayList<>();
				for (IT0184 it0184 : listOfIT0184) {
					records.add(new IT0184ResponseWrapper(it0184));
				}
				bunchOfIT0184.setItems(records.toArray(new IT0184ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0184Controller.class).findByPernr(pernr)).withSelfRel());
		return response;
	}		
	*/
	
	/*
	@RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<IT0184ResponseWrapper> findByPernrAndSubty(
			@RequestParam("ssn") String pernr,
			@RequestParam(value = "subtype", required = false, defaultValue = "01") String subty, 
			@RequestParam(value = "status", required = false) String status) throws Exception {
		
		IT0184ResponseWrapper it0184Response = null;
		APIResponseWrapper<IT0184ResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0184> listOfIT0184 = it0184QueryService.findByPernrAndSubty(numOfPernr, subty).stream().collect(Collectors.toList());
			
			if (listOfIT0184.isEmpty()) {
				//it0184Response = new IT0184ResponseWrapper(null);
				response.setMessage("No Data Found");
			} else {
				IT0184 it0184 = listOfIT0184.get(0);
				it0184Response = new IT0184ResponseWrapper(it0184);
			}
		} catch (NumberFormatException nfe) { 
			throw nfe;
		} catch (Exception e) {
			throw e;
		}
		
		response.setData(it0184Response);
		response.add(linkTo(methodOn(IT0184Controller.class).findByPernrAndSubty(pernr, subty, status)).withSelfRel());
		return response;
	}	
	*/
	
	@RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0184ResponseWrapper>> findByPernrAndSubty(
			@RequestParam("ssn") String pernr,
			@RequestParam(value = "subtype", required = false, defaultValue = "01") String subty, 
			@RequestParam(value = "status", required = false) String status) throws Exception {
		
		ArrayData<IT0184ResponseWrapper> bunchOfIT0184 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0184ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0184);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0184> listOfIT0184 = new ArrayList<>();
			
			if (StringUtils.isNotEmpty(status)) {
				listOfIT0184 = it0184QueryService.findByPernrAndSubtyAndStatus(numOfPernr, subty, status).stream().collect(Collectors.toList());
			} else {
				listOfIT0184 = it0184QueryService.findByPernrAndSubty(numOfPernr, subty).stream().collect(Collectors.toList());
			}
			
			if (listOfIT0184.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0184ResponseWrapper> records = new ArrayList<>();
				for (IT0184 it0184 : listOfIT0184) {
					records.add(new IT0184ResponseWrapper(it0184));
				}
				bunchOfIT0184.setItems(records.toArray(new IT0184ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0184Controller.class).findByPernrAndSubty(pernr, subty, status)).withSelfRel());
		return response;
	}
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated IT0184RequestWrapper request, BindingResult result)
			throws CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> it0184Entity = hcmsEntityQueryService.findByEntityName(IT0184.class.getSimpleName());
			if (!it0184Entity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + IT0184.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<IT0184, IT0184RequestWrapper, String> newObjectContainer 
					= it0184RequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				/*
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				*/
				
				Optional<IT0184> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
					newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = it0184CommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create IT0184 (Resume Texts) data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("pernr=" + savedEntity.get().getId().getPernr());
				key.append(",infty=" + savedEntity.get().getId().getInfty());
				key.append(",subty=" + savedEntity.get().getId().getSubty());
				key.append(",endda=" + CommonDateFunction.convertDateToStringYMD(savedEntity.get().getId().getEndda()));
				key.append(",begda=" + CommonDateFunction.convertDateToStringYMD(savedEntity.get().getId().getBegda()));
				EventLog eventLog = new EventLog(it0184Entity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				eventLog.setStatus(savedEntity.get().getStatus().toString());
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}

				//IT0184SoapObject it0184SoapObject = new IT0184SoapObject(savedEntity.get()); 
				//muleConsumerService.postToSAP(BAPIFunction.IT0184_CREATE.service(), BAPIFunction.IT0184_CREATE_URI.service(), it0184SoapObject);
				
				IT0184ResponseWrapper responseWrapper = new IT0184ResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;	
			} /* catch (AuthorizationException e) { 
				throw e;
			}*/ catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(IT0184Controller.class).create(request, result)).withSelfRel());		
		return response;
	}
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated IT0184RequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			IT0184ResponseWrapper responseWrapper = new IT0184ResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<HCMSEntity> it0184Entity = hcmsEntityQueryService.findByEntityName(IT0184.class.getSimpleName());
					if (!it0184Entity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + IT0184.class.getSimpleName() + "' !!!");
					}
					
					Optional<IT0184> it0184 = it0184QueryService.findOneByCompositeKey(request.getPernr(), request.getSubty(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
					IT0184 currentIT0184 = null;
					if (!it0184.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						// force create new Resume Text
						//break edit;
					} else {
						currentIT0184 = new IT0184();
						BeanUtils.copyProperties(it0184.get(), currentIT0184);
					}
				
					RequestObjectComparatorContainer<IT0184, IT0184RequestWrapper, String> updatedContainer =
							// it0184RequestBuilderUtil.compareAndReturnUpdatedData(request, it0184.isPresent() ? it0184.get() : null);
							it0184RequestBuilderUtil.compareAndReturnUpdatedData(request, currentIT0184);

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged && it0184.isPresent()) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
					OperationType opType = OperationType.UPDATE;
				
					if (updatedContainer.getEntity().isPresent()) {
						/*
						updatedContainer.getEntity().get().setSeqnr(
								it0184.isPresent() ? it0184.get().getSeqnr().longValue() + 1 : 1); // increment "seqnr" everytime record being updated, or initial 1 for new record (force create)						
						it0184 = it0184CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						*/
						// since version 2, update document only change the status
						if (it0184.isPresent()) {
							if (it0184.get().getStatus() == DocumentStatus.INITIAL_DRAFT) {
								it0184.get().setStatus(DocumentStatus.UPDATED_DRAFT);
								currentIT0184.setStatus(DocumentStatus.UPDATED_DRAFT);
							} else if (it0184.get().getStatus() == DocumentStatus.PUBLISHED) { 
								it0184.get().setStatus(DocumentStatus.UPDATE_IN_PROGRESS);
								currentIT0184.setStatus(DocumentStatus.UPDATE_IN_PROGRESS);
							}
						} else {
							// force create
							it0184 = it0184CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
							currentIT0184 = it0184.isPresent() ? it0184.get() : currentIT0184;
							opType = OperationType.CREATE;
						}
					}
				
					if (!it0184.isPresent()) {
						throw new CannotPersistException("Cannot update IT0184 (Resume Texts) data. Please check your data!");
					}
					
					StringBuilder key = new StringBuilder("pernr=" + it0184.get().getId().getPernr());
					key.append(",infty=" + it0184.get().getId().getInfty());
					key.append(",subty=" + it0184.get().getId().getSubty());
					key.append(",endda=" + CommonDateFunction.convertDateToStringYMD(it0184.get().getId().getEndda()));
					key.append(",begda=" + CommonDateFunction.convertDateToStringYMD(it0184.get().getId().getBegda()));
					EventLog eventLog = new EventLog(it0184Entity.get(), key.toString(), opType, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					eventLog.setStatus(it0184.get().getStatus().toString());
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					
					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}

					//IT0184SoapObject it0184SoapObject = new IT0184SoapObject(savedEntity.get()); 
					//muleConsumerService.postToSAP(BAPIFunction.IT0184_CREATE.service(), "resumeText", it0184SoapObject);
					
					//responseWrapper = new IT0184ResponseWrapper(it0184.get());
					responseWrapper = new IT0184ResponseWrapper(currentIT0184);
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(IT0184Controller.class).edit(request, result)).withSelfRel());
		return response;
	}		
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(itValidator);
	}	
}