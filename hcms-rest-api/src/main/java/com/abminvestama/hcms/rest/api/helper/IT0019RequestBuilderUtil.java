package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.Attachment;
import com.abminvestama.hcms.core.model.entity.IT0019;
import com.abminvestama.hcms.core.model.entity.T531S;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.business.query.AttachmentQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T531SQueryService;
import com.abminvestama.hcms.core.service.util.FileAttachmentService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0019RequestWrapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData: Add attachment fields</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData: Add fields text1, text2, text3</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("it0019RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT0019RequestBuilderUtil {

	private T531SQueryService t531sQueryService;
	private AttachmentQueryService attachmentQueryService;
	private FileAttachmentService fileAttachmentService;
	
	@Autowired
	void setT531SQueryService(T531SQueryService t531sQueryService) {
		this.t531sQueryService = t531sQueryService;
	}
	
	@Autowired
	void setAttachmentQueryService(AttachmentQueryService attachmentQueryService) {
		this.attachmentQueryService = attachmentQueryService;
	}
	
	@Autowired
	void setFileAttachmentService(FileAttachmentService fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}
	
	/**
	 * Compare IT0019 request payload with existing IT0019 in the database.
	 * Update existing IT0019 data with the latest data comes from the request payload. 
	 * 
	 * @param requestPayload request data 
	 * @param it0019DB current existing IT0019 in the database.
	 * @return updated IT0019 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0019, IT0019RequestWrapper, String> compareAndReturnUpdatedData(IT0019RequestWrapper requestPayload, IT0019 it0019DB) {
		
		RequestObjectComparatorContainer<IT0019, IT0019RequestWrapper, String> objectContainer = null;
		
		if (it0019DB == null) {
			it0019DB = new IT0019();
		} else {
			if (StringUtils.isNotBlank(requestPayload.getItxex())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getItxex(), it0019DB.getItxex())) {
					it0019DB.setItxex(requestPayload.getItxex());
					requestPayload.setDataChanged(true);
				}
			}

			if (StringUtils.isNotBlank(requestPayload.getTmart())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getTmart(), StringUtils.defaultString(it0019DB.getTmart().getTmart(), StringUtils.EMPTY))) {
					try {
						Optional<T531S> newT531S = t531sQueryService.findById(Optional.ofNullable(requestPayload.getTmart()));
						if (newT531S.isPresent()) {
							it0019DB.setTmart(newT531S.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						// t531s not found... ignore change.
					}
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getTermn())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getTermn()), it0019DB.getTermn())) {
					it0019DB.setTermn(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getTermn()));
					requestPayload.setDataChanged(true);
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getMndat())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getMndat()), it0019DB.getMndat())) {
					it0019DB.setMndat(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getMndat()));
					requestPayload.setDataChanged(true);
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getBvmrk())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBvmrk(), it0019DB.getBvmrk())) {
					it0019DB.setBvmrk(requestPayload.getBvmrk());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (requestPayload.getTmjhr() != (it0019DB.getTmjhr() != null ? it0019DB.getTmjhr().intValue() : 0)) {
				it0019DB.setTmjhr(requestPayload.getTmjhr());
				requestPayload.setDataChanged(true);
			}
			
			if (requestPayload.getTmmon() != (it0019DB.getTmmon() != null ? it0019DB.getTmmon().intValue() : 0)) {
				it0019DB.setTmmon(requestPayload.getTmmon());
				requestPayload.setDataChanged(true);
			}
			if (requestPayload.getTmtag() != (it0019DB.getTmtag() != null ? it0019DB.getTmtag().intValue() : 0)) {
				it0019DB.setTmtag(requestPayload.getTmtag());
				requestPayload.setDataChanged(true);
			}
			
			if (requestPayload.getMnjhr() != (it0019DB.getMnjhr() != null ? it0019DB.getMnjhr().intValue() : 0)) {
				it0019DB.setMnjhr(requestPayload.getMnjhr());
				requestPayload.setDataChanged(true);
			}
			if (requestPayload.getMnmon() != (it0019DB.getMnmon() != null ? it0019DB.getMnmon().intValue() : 0)) {
				it0019DB.setMnmon(requestPayload.getMnmon());
				requestPayload.setDataChanged(true);
			}
			if (requestPayload.getMntag() != (it0019DB.getMntag() != null ? it0019DB.getMntag().intValue() : 0)) {
				it0019DB.setMntag(requestPayload.getMntag());
				requestPayload.setDataChanged(true);
			}

			if (StringUtils.isNotBlank(requestPayload.getText1())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getText1(), it0019DB.getText1())) {
					it0019DB.setText1(requestPayload.getText1());
					requestPayload.setDataChanged(true);
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getText2())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getText2(), it0019DB.getText2())) {
					it0019DB.setText2(requestPayload.getText2());
					requestPayload.setDataChanged(true);
				}
			}
			if (StringUtils.isNotBlank(requestPayload.getText3())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getText3(), it0019DB.getText3())) {
					it0019DB.setText3(requestPayload.getText3());
					requestPayload.setDataChanged(true);
				}
			}
			
			it0019DB.setSeqnr(it0019DB.getSeqnr().longValue() + 1);
			try {
				it0019DB = saveAttachment(it0019DB, requestPayload);
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			it0019DB.setSeqnr(it0019DB.getSeqnr().longValue() - 1);
			
			objectContainer = new RequestObjectComparatorContainer<>(it0019DB, requestPayload);
		}
		
		JSONObject json = new JSONObject(requestPayload);
		// Adjust attachment fields on EventData Json
		json.remove("attach1Type");
		json.remove("attach1Content");
		json.remove("attach1Filename");
		if (it0019DB.getAttachment() != null)
			json.put("attach1_subty", it0019DB.getAttachment().getId().getSubty());
		if (it0019DB.getAttachmentPath() != null)
		    json.put("attach1_path", it0019DB.getAttachmentPath());
		// ******************************************
		objectContainer.setEventData(json.toString());
		
		return objectContainer;
	}
	
	private IT0019 saveAttachment(IT0019 it0019DB, IT0019RequestWrapper requestPayload) throws Exception {
		
		T591SKey attachmentKey = new T591SKey(SAPInfoType.TASK_MONITORING.infoType(), requestPayload.getAttach1Type());
		Optional<Attachment> attachment = attachmentQueryService.findById(Optional.ofNullable(attachmentKey));
		it0019DB.setAttachment(attachment.isPresent() ? attachment.get() : null);
		String base64 = requestPayload.getAttach1Content();
		String filename = requestPayload.getAttach1Filename();
		if (StringUtils.isNotEmpty(base64) && StringUtils.isNotEmpty(filename)) {
			String ext = filename.split("\\.")[(filename.split("\\.")).length-1];
			String formatFilename = it0019DB.getId().toStringId().concat("_").concat(it0019DB.getSeqnr().toString()).concat(".").concat(ext);
		    String path = fileAttachmentService.save(formatFilename, base64);
		    it0019DB.setAttachmentPath(path);
		    requestPayload.setDataChanged(true);
		}
		
		return it0019DB;
		
	}
}