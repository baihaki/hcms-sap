package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyEmpquotastf;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotastfQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedEmpquotastfRequestWrapper;

@Component("ZmedEmpquotastfRequestBuilderUtil")
@Transactional(readOnly = true)
public class ZmedEmpquotastfRequestBuilderUtil {
	
	


	private T001QueryService t001QueryService;
	private ZmedEmpquotastfQueryService zmedEmpquotastfQueryService;
	
	@Autowired
	void setZmedEmpquotastfQueryService(ZmedEmpquotastfQueryService zmedEmpquotastfQueryService) {
		this.zmedEmpquotastfQueryService = zmedEmpquotastfQueryService;
	}
	
	@Autowired
	void setT001QueryService(T001QueryService t001QueryService) {
		this.t001QueryService = t001QueryService;
	}
	
	public RequestObjectComparatorContainer<ZmedEmpquotastf, ZmedEmpquotastfRequestWrapper, String> compareAndReturnUpdatedData(ZmedEmpquotastfRequestWrapper requestPayload, ZmedEmpquotastf zmedEmpquotastfDB) {
		
		RequestObjectComparatorContainer<ZmedEmpquotastf, ZmedEmpquotastfRequestWrapper, String> objectContainer = null; 
		
	if (zmedEmpquotastfDB == null) {
		zmedEmpquotastfDB = new ZmedEmpquotastf();
		
		try {
			Optional<ZmedEmpquotastf> existingZmedEmpquotastf = zmedEmpquotastfQueryService.findOneByCompositeKey(
					requestPayload.getStell(), requestPayload.getBukrs(),
					requestPayload.getPersg(), requestPayload.getPersk(),
					requestPayload.getKodequo(),
					CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
					CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
			
			if (existingZmedEmpquotastf.isPresent()) {
				throw new DataViolationException("Duplicate ZmedEmpquotastf Data for [ssn=" + requestPayload.getKodequo());
			}
			zmedEmpquotastfDB.setId(
					new ZmedCompositeKeyEmpquotastf(requestPayload.getStell(), requestPayload.getBukrs(),
							requestPayload.getPersg(), requestPayload.getPersk(),
							requestPayload.getKodequo(),
							CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
							CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi())));
			
			zmedEmpquotastfDB.setDatab(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()));
			zmedEmpquotastfDB.setDatbi(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
			zmedEmpquotastfDB.setStell(requestPayload.getStell());
			zmedEmpquotastfDB.setKodequo(requestPayload.getKodequo());
			zmedEmpquotastfDB.setPersg(requestPayload.getPersg());
			zmedEmpquotastfDB.setPersk(requestPayload.getPersk());
			zmedEmpquotastfDB.setQuotamt(Double.parseDouble(requestPayload.getQuotamt()));			
			Optional<T001> bnka = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));				
			zmedEmpquotastfDB.setBukrs(bnka.isPresent() ? bnka.get() : null);
			//zmedEmpquotastfDB.setBukrs(requestPayload.getBukrs());
			
		} catch (Exception ignored) {
			System.err.println(ignored);
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedEmpquotastfDB, requestPayload);
		
	} else {
		
		if (StringUtils.isNotBlank(requestPayload.getPersg())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPersg(), zmedEmpquotastfDB.getPersg())) {
				zmedEmpquotastfDB.setPersg(requestPayload.getPersg());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getPersk())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPersk(), zmedEmpquotastfDB.getPersk())) {
				zmedEmpquotastfDB.setPersk(requestPayload.getPersk());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(""+requestPayload.getStell())) {
			if (CommonComparatorFunction.isDifferentStringValues(""+requestPayload.getStell(), zmedEmpquotastfDB.getStell().toString())) {
				zmedEmpquotastfDB.setStell(requestPayload.getStell());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getKodequo())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getKodequo(), zmedEmpquotastfDB.getKodequo())) {
				zmedEmpquotastfDB.setKodequo(requestPayload.getKodequo());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedEmpquotastfDB.getBukrs() != null ? zmedEmpquotastfDB.getBukrs().getBukrs() : StringUtils.EMPTY)) {
				try {
					Optional<T001> newT001 = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));
					if (newT001.isPresent()) {
						zmedEmpquotastfDB.setBukrs(newT001.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					//T535N not found... cancel the update process
				}
			}
		}

		/*if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedEmpquotastfDB.getBukrs())) {
				zmedEmpquotastfDB.setBukrs(requestPayload.getBukrs());
				requestPayload.setDataChanged(true);
			}
		}*/	
		
		
		if (StringUtils.isNotBlank(requestPayload.getQuotamt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getQuotamt(), zmedEmpquotastfDB.getQuotamt().toString())) {
				zmedEmpquotastfDB.setQuotamt(Double.parseDouble(requestPayload.getQuotamt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getDatab())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), (zmedEmpquotastfDB.getDatab()))) {
				zmedEmpquotastfDB.setDatab(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()));
				requestPayload.setDataChanged(true);
			}
		}
		
		
		if (StringUtils.isNotBlank(requestPayload.getDatbi())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()), (zmedEmpquotastfDB.getDatbi())) ){
				zmedEmpquotastfDB.setDatbi(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
				requestPayload.setDataChanged(true);
			}
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedEmpquotastfDB, requestPayload);	
				
	}
	
	JSONObject json = new JSONObject(requestPayload);
	objectContainer.setEventData(json.toString());
	
	return objectContainer;
}
}