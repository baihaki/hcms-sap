package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyMedtype;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.abminvestama.hcms.core.service.api.business.query.ZmedMedtypeQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedMedtypeRequestWrapper;


@Component("ZmedMedtypeRequestBuilderUtil")
@Transactional(readOnly = true)
public class ZmedMedtypeRequestBuilderUtil {

	
	private T001QueryService t001QueryService;
	private ZmedMedtypeQueryService zmedMedtypeQueryService;
	
	@Autowired
	void setZmedMedtypeQueryService(ZmedMedtypeQueryService zmedMedtypeQueryService) {
		this.zmedMedtypeQueryService = zmedMedtypeQueryService;
	}
	
	@Autowired
	void setT001QueryService(T001QueryService t001QueryService) {
		this.t001QueryService = t001QueryService;
	}
	
	public RequestObjectComparatorContainer<ZmedMedtype, ZmedMedtypeRequestWrapper, String> compareAndReturnUpdatedData(ZmedMedtypeRequestWrapper requestPayload, ZmedMedtype zmedMedtypeDB) {
		
		RequestObjectComparatorContainer<ZmedMedtype, ZmedMedtypeRequestWrapper, String> objectContainer = null; 
		
	if (zmedMedtypeDB == null) {
		zmedMedtypeDB = new ZmedMedtype();
		
		try {
			Optional<ZmedMedtype> existingZmedMedtype = zmedMedtypeQueryService.findOneByCompositeKey(
					requestPayload.getBukrs(), requestPayload.getKodemed(), 
					CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
					CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
			
			if (existingZmedMedtype.isPresent()) {
				throw new DataViolationException("Duplicate ZmedMedtype Data for [ssn=" + requestPayload.getKodemed());
			}
			zmedMedtypeDB.setId(
					new ZmedCompositeKeyMedtype(requestPayload.getBukrs(), requestPayload.getKodemed(),
							CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
							CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi())));
			
			zmedMedtypeDB.setDatab(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()));
			zmedMedtypeDB.setDatbi(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
			zmedMedtypeDB.setDescr(requestPayload.getDescr());
			zmedMedtypeDB.setKodemed(requestPayload.getKodemed());		
			Optional<T001> bnka = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));				
			zmedMedtypeDB.setBukrs(bnka.isPresent() ? bnka.get() : null);
			//zmedMedtypeDB.setBukrs(requestPayload.getBukrs());
			
		} catch (Exception ignored) {
			System.err.println(ignored);
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedMedtypeDB, requestPayload);
		
	} else {
		
		if (StringUtils.isNotBlank(requestPayload.getDescr())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDescr(), zmedMedtypeDB.getDescr())) {
				zmedMedtypeDB.setDescr(requestPayload.getDescr());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getKodemed())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getKodemed(), zmedMedtypeDB.getKodemed())) {
				zmedMedtypeDB.setKodemed(requestPayload.getKodemed());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedMedtypeDB.getBukrs() != null ? zmedMedtypeDB.getBukrs().getBukrs() : StringUtils.EMPTY)) {
				try {
					Optional<T001> newT001 = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));
					if (newT001.isPresent()) {
						zmedMedtypeDB.setBukrs(newT001.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					//T535N not found... cancel the update process
				}
			}
		}
		
		
		/*if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedMedtypeDB.getBukrs())) {
				zmedMedtypeDB.setBukrs(requestPayload.getBukrs());
				requestPayload.setDataChanged(true);
			}
		}*/

			
		
				
		if (StringUtils.isNotBlank(requestPayload.getDatab())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), (zmedMedtypeDB.getDatab()))) {
				zmedMedtypeDB.setDatab(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()));
				requestPayload.setDataChanged(true);
			}
		}
		
		
		if (StringUtils.isNotBlank(requestPayload.getDatbi())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()), (zmedMedtypeDB.getDatbi())) ){
				zmedMedtypeDB.setDatbi(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
				requestPayload.setDataChanged(true);
			}
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedMedtypeDB, requestPayload);	
				
	}
	
	JSONObject json = new JSONObject(requestPayload);
	objectContainer.setEventData(json.toString());
	
	return objectContainer;
}
}