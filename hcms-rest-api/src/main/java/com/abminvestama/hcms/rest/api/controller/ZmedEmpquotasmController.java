package com.abminvestama.hcms.rest.api.controller;



import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedEmpquotasmCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotasmQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedEmpquotasmRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.ZmedEmpquotasmRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.ZmedEmpquotasmResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ZmedEmpquotasmResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.ZmedEmpquotasmRequestBuilderUtil;




@RestController
@RequestMapping("/api/v1/medical_empquotasm")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ZmedEmpquotasmController extends AbstractResource {
	
	private ZmedEmpquotasmCommandService zmedEmpquotasmCommandService;
	private ZmedEmpquotasmQueryService zmedEmpquotasmQueryService;
	private ZmedEmpquotasmRequestBuilderUtil zmedEmpquotasmRequestBuilderUtil;
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	@Autowired
	void setZmedEmpquotasmCommandService(ZmedEmpquotasmCommandService zmedEmpquotasmCommandService) {
		this.zmedEmpquotasmCommandService = zmedEmpquotasmCommandService;
	}
	
	@Autowired
	void setZmedEmpquotasmQueryService(ZmedEmpquotasmQueryService zmedEmpquotasmQueryService) {
		this.zmedEmpquotasmQueryService = zmedEmpquotasmQueryService;
	}
	
	@Autowired
	void setZmedEmpquotasmRequestBuilderUtil(ZmedEmpquotasmRequestBuilderUtil zmedEmpquotasmRequestBuilderUtil) {
		this.zmedEmpquotasmRequestBuilderUtil = zmedEmpquotasmRequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
/*	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ZmedEmpquotasmResponseWrapper> findOneByPernr(@PathVariable String pernr) throws Exception {
		ZmedEmpquotasmResponseWrapper zmedEmpquotasmResponse = null;
		APIResponseWrapper<ZmedEmpquotasmResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<ZmedEmpquotasm> listOfZmedEmpquotasm = zmedEmpquotasmQueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			
			if (listOfZmedEmpquotasm.isEmpty()) {
				zmedEmpquotasmResponse = new ZmedEmpquotasmResponseWrapper(null);
				response.setMessage("No Data Found");
			} else {
				ZmedEmpquotasm zmedEmpquotasm = listOfZmedEmpquotasm.get(0);
				zmedEmpquotasmResponse = new ZmedEmpquotasmResponseWrapper(zmedEmpquotasm);
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.setData(zmedEmpquotasmResponse);
		response.add(linkTo(methodOn(ZmedEmpquotasmController.class).findOneByPernr(pernr)).withSelfRel());
		return response;
	}		
	*/
	
	
	
	
	
	
	
	
	
	
	

	
	
	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated ZmedEmpquotasmRequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> zmedEmpquotasmEntity = hcmsEntityQueryService.findByEntityName(ZmedEmpquotasm.class.getSimpleName());
			if (!zmedEmpquotasmEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + ZmedEmpquotasm.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<ZmedEmpquotasm, ZmedEmpquotasmRequestWrapper, String> newObjectContainer 
					= zmedEmpquotasmRequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				
				Optional<ZmedEmpquotasm> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
				//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = zmedEmpquotasmCommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create ZmedEmpquotasm data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("pernr=" + savedEntity.get().getId().getPernr());
				key.append(",bukrs=" + savedEntity.get().getId().getBukrs());
				key.append(",persg=" + savedEntity.get().getId().getPersg());
				key.append(",persk=" + savedEntity.get().getId().getPersk());
				key.append(",fatxt=" + savedEntity.get().getId().getFatxt());
				key.append(",kodequo=" + savedEntity.get().getId().getKodequo());
				key.append(",datab=" + savedEntity.get().getId().getDatab());
				key.append(",datbi=" + savedEntity.get().getId().getDatbi());

				EventLog eventLog = new EventLog(zmedEmpquotasmEntity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				
				ZmedEmpquotasmResponseWrapper responseWrapper = new ZmedEmpquotasmResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(ZmedEmpquotasmController.class).create(request, result)).withSelfRel());		
		return response;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated ZmedEmpquotasmRequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			ZmedEmpquotasmResponseWrapper responseWrapper = new ZmedEmpquotasmResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<ZmedEmpquotasm> zmedEmpquotasm = zmedEmpquotasmQueryService.findOneByCompositeKey(
							request.getPernr(), request.getBukrs(),
							request.getPersg(), request.getPersk(),
							request.getFatxt(), request.getKodequo(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getDatab()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getDatbi()));
					if (!zmedEmpquotasm.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					RequestObjectComparatorContainer<ZmedEmpquotasm, ZmedEmpquotasmRequestWrapper, String> updatedContainer = zmedEmpquotasmRequestBuilderUtil.compareAndReturnUpdatedData(request, zmedEmpquotasm.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
				//	Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				//
				//	if (updatedContainer.getEntity().isPresent()) {
				//		updatedContainer.getEntity().get().setSeqnr(zmedEmpquotasm.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
				//		zmedEmpquotasm = zmedEmpquotasmCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				//	}
				
				//	if (!zmedEmpquotasm.isPresent()) {
				//		throw new CannotPersistException("Cannot update ZmedEmpquotasm (Personal Information) data. Please check your data!");
				//	}
				
					responseWrapper = new ZmedEmpquotasmResponseWrapper(zmedEmpquotasm.get());
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(ZmedEmpquotasmController.class).edit(request, result)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}
	
	
}
