package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.WeakHashMap;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.CaAdvanceCode;
import com.abminvestama.hcms.core.model.constant.CashAdvanceConstant;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.SettlementCASStatus;
import com.abminvestama.hcms.core.model.constant.SettlementProcess;
import com.abminvestama.hcms.core.model.constant.UserRole;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.CaEmpCSKT;
import com.abminvestama.hcms.core.model.entity.CaReqHdr;
import com.abminvestama.hcms.core.model.entity.CaStlDtl;
import com.abminvestama.hcms.core.model.entity.CaStlHdr;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.TrxApprovalProcess;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.CaReqHdrCommandService;
import com.abminvestama.hcms.core.service.api.business.command.CaStlDtlCommandService;
import com.abminvestama.hcms.core.service.api.business.command.CaStlHdrCommandService;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.TrxApprovalProcessCommandService;
import com.abminvestama.hcms.core.service.api.business.query.CSKTQueryService;
import com.abminvestama.hcms.core.service.api.business.query.CaEmpCSKTQueryService;
import com.abminvestama.hcms.core.service.api.business.query.CaStlDtlQueryService;
import com.abminvestama.hcms.core.service.api.business.query.CaStlHdrQueryService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.TrxApprovalProcessQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.helper.CaHdrDocSoapObject;
import com.abminvestama.hcms.core.service.helper.CaReqHdrMapper;
import com.abminvestama.hcms.core.service.helper.CaReqHdrSoapObject;
import com.abminvestama.hcms.core.service.helper.CaStlDtlSoapObject;
import com.abminvestama.hcms.core.service.helper.CaStlHdrSoapObject;
import com.abminvestama.hcms.core.service.util.MailService;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.CaStlDtlRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.CaStlHdrRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.CaStlHdrResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.CaReqHdrRequestBuilderUtil;
import com.abminvestama.hcms.rest.api.helper.CaStlDtlRequestBuilderUtil;
import com.abminvestama.hcms.rest.api.helper.CaStlHdrRequestBuilderUtil;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v3/ca_stlhdr")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CaStlHdrController extends AbstractResource {
	
	@Autowired
	private Environment envProps;
	
	private MuleConsumerService muleConsumerService;
	private MailService mailService;
	private EventLogCommandService eventLogCommandService;
	private HCMSEntityQueryService hcmsEntityQueryService;
	private UserQueryService userQueryService;
	private CommonServiceFactory commonServiceFactory;
	private CaStlHdrRequestBuilderUtil caStlHdrRequestBuilderUtil;
	private CaStlDtlRequestBuilderUtil caStlDtlRequestBuilderUtil;
	private CSKTQueryService csktQueryService;
	private CaEmpCSKTQueryService caEmpCSKTQueryService;
	private TrxApprovalProcessQueryService trxApprovalProcessQueryService;
	private TrxApprovalProcessCommandService trxApprovalProcessCommandService;
	
	private CaReqHdrCommandService caReqHdrCommandService;
	private CaReqHdrRequestBuilderUtil caReqHdrRequestBuilderUtil;
	
	private CaStlHdrCommandService caStlHdrCommandService;
	private CaStlHdrQueryService caStlHdrQueryService;
	private CaStlDtlQueryService caStlDtlQueryService;
	private CaStlDtlCommandService caStlDtlCommandService;

	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}

	@Autowired
	void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	@Autowired
	void setCaStlHdrRequestBuilderUtil(CaStlHdrRequestBuilderUtil caStlHdrRequestBuilderUtil) {
		this.caStlHdrRequestBuilderUtil = caStlHdrRequestBuilderUtil;
	}

	@Autowired
	void setCaStlDtlRequestBuilderUtil(CaStlDtlRequestBuilderUtil caStlDtlRequestBuilderUtil) {
		this.caStlDtlRequestBuilderUtil = caStlDtlRequestBuilderUtil;
	}

	@Autowired
	void setCSKTQueryService(CSKTQueryService csktQueryService) {
		this.csktQueryService = csktQueryService;
	}

	@Autowired
	void setCaEmpCSKTQueryService(CaEmpCSKTQueryService caEmpCSKTQueryService) {
		this.caEmpCSKTQueryService = caEmpCSKTQueryService;
	}

	@Autowired
	void setTrxApprovalProcessQueryService(TrxApprovalProcessQueryService trxApprovalProcessQueryService) {
		this.trxApprovalProcessQueryService = trxApprovalProcessQueryService;
	}

	@Autowired
	void setTrxApprovalProcessCommandService(TrxApprovalProcessCommandService trxApprovalProcessCommandService) {
		this.trxApprovalProcessCommandService = trxApprovalProcessCommandService;
	}

	@Autowired
	void setCaReqHdrCommandService(CaReqHdrCommandService caReqHdrCommandService) {
		this.caReqHdrCommandService = caReqHdrCommandService;
	}

	@Autowired
	void setCaReqHdrRequestBuilderUtil(CaReqHdrRequestBuilderUtil caReqHdrRequestBuilderUtil) {
		this.caReqHdrRequestBuilderUtil = caReqHdrRequestBuilderUtil;
	}

	@Autowired
	void setCaStlHdrCommandService(CaStlHdrCommandService caStlHdrCommandService) {
		this.caStlHdrCommandService = caStlHdrCommandService;
	}

	@Autowired
	void setCaStlHdrQueryService(CaStlHdrQueryService caStlHdrQueryService) {
		this.caStlHdrQueryService = caStlHdrQueryService;
	}

	@Autowired
	void setCaStlDtlQueryService(CaStlDtlQueryService caStlDtlQueryService) {
		this.caStlDtlQueryService = caStlDtlQueryService;
	}

	@Autowired
	void setCaStlDtlCommandService(CaStlDtlCommandService caStlDtlCommandService) {
		this.caStlDtlCommandService = caStlDtlCommandService;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/reqno/{reqno}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<CaStlHdrResponseWrapper>> findOneByReqno(@PathVariable String reqno, 
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		
		ArrayData<CaStlHdrResponseWrapper> bunchOfCaStlHdr = new ArrayData<>();
		APIResponseWrapper<ArrayData<CaStlHdrResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfCaStlHdr);
		List<CaStlHdr> listOfCaStlHdr = new ArrayList<>();
		
		// GET SINGLE HEADER SETTLEMENT
		Optional<CaStlHdr> caStlHdr = caStlHdrQueryService.findByReqno(reqno);
		
		goto1:
			if (!caStlHdr.isPresent()) {
				response.setMessage("No Data Found");
			}
			else {
				WeakHashMap<String, String> valid = caStlHdrRequestBuilderUtil.validationProcessByCurrentUserRole(currentUser(), caStlHdr);
				if (valid.get("IS_VALID").equals("FALSE")) {
					response.setMessage(valid.get("RESPONSE_MESSAGE"));
					break goto1;
				}
				
				// GET DETAILS SETTLEMENT
				Collection<CaStlDtl> caStlDtl = caStlDtlQueryService.fetchByReqno(reqno);
				caStlHdr.get().setStlDetail(caStlDtl.stream().collect(Collectors.toList()));
				listOfCaStlHdr.add(caStlHdr.get());
				
				// GET DETAILS APPROVAL
				Collection<TrxApprovalProcess> approvalDtl = trxApprovalProcessQueryService.fetchByEntityObjectId(caStlHdr.get().getReqNo(), "seq ASC");
				caStlHdr.get().setApprovalDetail(approvalDtl.stream().collect(Collectors.toList()));
				
				if (listOfCaStlHdr.isEmpty()) {
					response.setMessage("No Data Found");
				}
				else {
					List<CaStlHdrResponseWrapper> records = new ArrayList<>();
					for (CaStlHdr stlHdr : listOfCaStlHdr) {
						records.add(new CaStlHdrResponseWrapper(stlHdr));
					}
					bunchOfCaStlHdr.setItems(records.toArray(new CaStlHdrResponseWrapper[records.size()]));
				}
			}
		
		response.add(linkTo(methodOn(CaStlHdrController.class).findOneByReqno(reqno, pageNumber)).withSelfRel());
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/tcano/{tcano}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<CaStlHdrResponseWrapper>> findOneByTcano(@PathVariable String tcano, 
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		
		ArrayData<CaStlHdrResponseWrapper> bunchOfCaStlHdr = new ArrayData<>();
		APIResponseWrapper<ArrayData<CaStlHdrResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfCaStlHdr);
		List<CaStlHdr> listOfCaStlHdr = new ArrayList<>();
		
		// GET SINGLE HEADER SETTLEMENT
		Optional<CaStlHdr> caStlHdr = caStlHdrQueryService.findByTcano(tcano);
		
		goto1:
			if (!caStlHdr.isPresent()) {
				response.setMessage("No Data Found");
			}
			else {
				WeakHashMap<String, String> valid = caStlHdrRequestBuilderUtil.validationProcessByCurrentUserRole(currentUser(), caStlHdr);
				if (valid.get("IS_VALID").equals("FALSE")) {
					response.setMessage(valid.get("RESPONSE_MESSAGE"));
					break goto1;
				}
				
				// GET DETAILS SETTLEMENT
				Collection<CaStlDtl> caStlDtl = caStlDtlQueryService.fetchByReqno(caStlHdr.get().getReqNo());
				caStlHdr.get().setStlDetail(caStlDtl.stream().collect(Collectors.toList()));
				
				// GET DETAILS APPROVAL
				Collection<TrxApprovalProcess> approvalDtl = trxApprovalProcessQueryService.fetchByEntityObjectId(caStlHdr.get().getReqNo(), "seq ASC");
				caStlHdr.get().setApprovalDetail(approvalDtl.stream().collect(Collectors.toList()));
				
				listOfCaStlHdr.add(caStlHdr.get());
				if (listOfCaStlHdr.isEmpty()) {
					response.setMessage("No Data Found");
				}
				else {
					// SYNC DOC NO. SAP
					updateDocumentStatus(listOfCaStlHdr);
					
					List<CaStlHdrResponseWrapper> records = new ArrayList<>();
					for (CaStlHdr stlHdr : listOfCaStlHdr) {
						records.add(new CaStlHdrResponseWrapper(stlHdr));
					}
					bunchOfCaStlHdr.setItems(records.toArray(new CaStlHdrResponseWrapper[records.size()]));
				}
			}
		response.add(linkTo(methodOn(CaStlHdrController.class).findOneByTcano(tcano, pageNumber)).withSelfRel());
		return response;
	}

	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_criteria", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<CaStlHdrResponseWrapper>> fetchByCriteria(
			@RequestParam(required = false, value = "ssn", defaultValue = "") String ssn,
			@RequestParam(required = false, value = "process", defaultValue = "") String process,
			@RequestParam(required = false, value = "advance_code", defaultValue = "BL,PC,BT,RE") String advanceCode,
			@RequestParam(required = false, value = "finance_payment_status", defaultValue = "") String status,
			@RequestParam(required = false, value = "page", defaultValue = "1") int pageNumber) throws Exception {
		
		Page<CaStlHdrResponseWrapper> responsePage = new PageImpl<CaStlHdrResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<CaStlHdrResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<CaStlHdr> resultPage = null;
		String bukrs = null;
		boolean isSubOrdinate = false;
		Long[] pernrs = null;
		Long pernr = null;
		Integer intProcess = null;
		
		Optional<User> currentUser = userQueryService.findByUsername(currentUser());
		Optional<Role> hrAdminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_ADMIN.roleIdent));
		Optional<Role> headRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_HEAD.roleIdent));
		Optional<Role> directorRole =commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_DIRECTOR.roleIdent));
		Optional<Role> gaRole =commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_GA.roleIdent));
		Optional<Role> poolingRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_POOLING.roleIdent));
		Optional<Role> accountingRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_ACCOUNTING.roleIdent));
		Optional<Role> treasuryRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_TREASURY.roleIdent));
		
		try {
			String positionId = currentUser.get().getEmployee().getPosition().getId();
			String[] advanceCodes = advanceCode.split(",");
			String[] processesString = StringUtils.isEmpty(process) ? null : process.split(",");
			Integer[] processes = StringUtils.isEmpty(process) ? null : new Integer[processesString.length];
			
			if (processesString != null && processesString.length > 1 ) {
				for (int i=0; i < processesString.length; i++) {
					processes[i] = Integer.valueOf(processesString[i]).intValue();
				}
			}
			else {
				intProcess = new Integer(process);
			}
			
			Optional<User> employee = null;
			if (StringUtils.isNotEmpty(ssn)) {
				employee = userQueryService.findByPernr(new Long(ssn));
				if (!employee.isPresent()) {
					throw new Exception(
							"Invalid SN ".concat(employee.get().getEmployee().getPernr().toString()).concat(" no User found!"));
				}
				// PERNR = SSN, default PERNR = null
				else
					pernr = currentUser.get().getEmployee().getPernr();
			}
			
			// Get SELF EMPLOYEE 
			if(employee != null && currentUser.get().equals(employee.get())) {
				if (!process.isEmpty()) {
					resultPage = caStlHdrQueryService.findAllByCriteria(pernr, intProcess, pageNumber);
				}
				else if(!ssn.isEmpty()) {
					resultPage = caStlHdrQueryService.findByPernr(pernr, pageNumber);
				}
			}
			// ROLE TREASURY
			if (currentUser.get().getRoles().contains(treasuryRole.get())) {
				if (StringUtils.isNotEmpty(ssn)) {
					if (processes != null && processes.length > 1) {
						resultPage = caStlHdrQueryService.findAllByCriteriaWithProcesses(new Long(ssn), processes, pageNumber);
					}
					else if (StringUtils.isNotEmpty(process)) {
						resultPage = caStlHdrQueryService.findAllByCriteria(new Long(ssn), new Integer(process), pageNumber);
					}
					else {
						resultPage = null;
					}
				}
				else if (processes != null && processes.length > 1) {
					// get ALL Settlement by process
					resultPage = caStlHdrQueryService.findAllByCriteriaWithProcesses(null, processes, pageNumber);
				}
				else {
					resultPage = caStlHdrQueryService.findAllByCriteria(null, new Integer(process), pageNumber);
				}
			}
			// NOT ROLE TREASURY
			else {
				if (!currentUser.get().getRoles().contains(treasuryRole.get())
						&& !currentUser.get().getRoles().contains(headRole.get())
						&& !currentUser.get().getRoles().contains(directorRole.get())
						&& !currentUser.get().getRoles().contains(gaRole.get())
						&& !currentUser.get().getRoles().contains(hrAdminRole.get())
						&& !currentUser.get().getRoles().contains(poolingRole.get())
						&& !currentUser.get().getRoles().contains(accountingRole.get())
						&& !currentUser.get().getEmployee().getPernr().equals(pernr)) {
					throw new Exception(
							"Insufficient Privileges. You can't fetch data of other Employee.");
				}
				
				// ROLE HEAD
				if (currentUser.get().getRoles().contains(headRole.get())
						&& SettlementProcess.WAITING_HEAD_RELEASE.val == intProcess.intValue()) {
					// get All PERNR of Sub-Ordinates
					List<Long> listOrPernr = userQueryService.fetchPernrByToPositionId(positionId);
					if (!listOrPernr.isEmpty()) {
						pernrs = new Long[listOrPernr.size()];
						for (int idx=0; idx<listOrPernr.size(); idx++) {
							pernrs[idx] = new Long(""+listOrPernr.get(idx));
							// filter SSN, must be member of SubOrdinates, default isSubOrdinate = false
							if (pernr != null && pernr.equals(pernrs[idx])) isSubOrdinate = true; 
						}
					}
					/*
					else {
						throw new Exception(
								"Insufficient Privileges. You can't fetch data of other Employee.");
					}
					// validation filter SSN
					if (pernr != null && isSubOrdinate)
						throw new Exception(
								"Insufficient Privileges. You can't fetch data of other Employee.");
					*/
					
					// Fetch Sub-Ordinates, filter by SSN(Single Employee) & process
					if (!ssn.isEmpty() && SettlementProcess.WAITING_HEAD_RELEASE.val == intProcess.intValue()) {
						resultPage = caStlHdrQueryService.findAllByCriteria(pernr, intProcess, pageNumber);
					}
					// Fetch All Sub-Ordinates, nothing filter SSN & process
					else if (processes != null && processes.length > 1) {
						resultPage = caStlHdrQueryService.findAllByCriteriaWithPernrs(pernrs, null, pageNumber);
					}
					// Fetch Sub-Ordinates, filter process only
					else if (pernrs != null) {
						resultPage = caStlHdrQueryService.findAllByCriteriaWithPernrs(pernrs, intProcess, pageNumber);
					}
					else {
						//No Data Found
					}
				}
				// ROLE DIRECTOR
				else if (currentUser.get().getRoles().contains(directorRole.get())
						&& SettlementProcess.WAITING_DIRECTOR_RELEASE.val == intProcess.intValue()) {
					// get All PERNR of Sub-Ordinates
					List<Long> listOrPernr = userQueryService.fetchPernrByToPositionId(positionId);
					if (!listOrPernr.isEmpty()) {
						pernrs = new Long[listOrPernr.size()];
						for (int idx=0; idx<listOrPernr.size(); idx++) {
							pernrs[idx] = new Long(""+listOrPernr.get(idx));
							// filter SSN, must be member of SubOrdinates, default isSubOrdinate = false
							if (pernr != null && pernr.equals(pernrs[idx])) isSubOrdinate = true; 
						}
					}
					/*
					else {
						throw new Exception(
								"Insufficient Privileges. You can't fetch data of other Employee.");
					}
					// validation filter SSN
					if (pernr != null && isSubOrdinate)
						throw new Exception(
								"Insufficient Privileges. You can't fetch data of other Employee.");
					*/
					
					// Fetch Sub-Ordinates, filter by SSN(Single Employee) & process
					if (!ssn.isEmpty() && SettlementProcess.WAITING_DIRECTOR_RELEASE.val == intProcess.intValue()) {
						resultPage = caStlHdrQueryService.findAllByCriteria(pernr, intProcess, pageNumber);
					}
					// Fetch All Sub-Ordinates, nothing filter SSN & process
					else if (processes != null && processes.length > 1) {
						resultPage = caStlHdrQueryService.findAllByCriteriaWithPernrs(pernrs, null, pageNumber);
					}
					// Fetch Sub-Ordinates, filter process only
					else if (pernrs != null) {
						resultPage = caStlHdrQueryService.findAllByCriteriaWithPernrs(pernrs, intProcess, pageNumber);
					}
					else {
						//No Data Found
					}
				}
				// ROLE GA ADMIN
				else if (currentUser.get().getRoles().contains(gaRole.get())
						&& SettlementProcess.WAITING_GA_RELEASE.val == intProcess.intValue()) {
					// Fetch by SSN & process
					if (!ssn.isEmpty() && SettlementProcess.WAITING_GA_RELEASE.val == intProcess.intValue()) {
						resultPage = caStlHdrQueryService.findSubOrdinatesByCriteria(pernr, intProcess, positionId, pageNumber);
					}
					// Fetch All PERNR by Process
					else if (!process.isEmpty() && SettlementProcess.WAITING_GA_RELEASE.val == intProcess.intValue()) {
						resultPage = caStlHdrQueryService.findAllByCriteria(null, intProcess, pageNumber);
					}
					// Fetch Self
					else if (currentUser.get().getEmployee().getPernr().equals(new Long(ssn))) {
						resultPage = caStlHdrQueryService.findByPernr(new Long(ssn), pageNumber);
					}
				}
				// ROLE HR ADMIN
				else if (currentUser.get().getRoles().contains(hrAdminRole.get())
						&& SettlementProcess.WAITING_HR_RELEASE.val == intProcess.intValue()) {
					// Fetch by SSN & process
					if (!ssn.isEmpty() && SettlementProcess.WAITING_HR_RELEASE.val == intProcess.intValue()) {
						resultPage = caStlHdrQueryService.findSubOrdinatesByCriteria(pernr, intProcess, positionId, pageNumber);
					}
					// Fetch All PERNR by Process
					else if (!process.isEmpty() && SettlementProcess.WAITING_HR_RELEASE.val == intProcess.intValue()) {
						resultPage = caStlHdrQueryService.findAllByCriteria(null, intProcess, pageNumber);
					}
				}
				// ROLE POOLING
				else if (currentUser.get().getRoles().contains(poolingRole.get())
						&& SettlementProcess.WAITING_POOLING_RELEASE.val == intProcess.intValue()) {
					// Fetch by SSN & process
					if (!ssn.isEmpty() && SettlementProcess.WAITING_POOLING_RELEASE.val == intProcess.intValue()) {
						resultPage = caStlHdrQueryService.findSubOrdinatesByCriteria(pernr, intProcess, positionId, pageNumber);
					}
					// Fetch All PERNR by Process
					else if (!process.isEmpty() && SettlementProcess.WAITING_POOLING_RELEASE.val == intProcess.intValue()) {
						resultPage = caStlHdrQueryService.findAllByCriteria(null, intProcess, pageNumber);
					}
				}
				// ROLE ACCOUNTING
				else if (currentUser.get().getRoles().contains(accountingRole.get())
						&& SettlementProcess.WAITING_ACCOUNTING_RELEASE.val == intProcess.intValue()) {
					// Fetch by SSN & process
					if (!ssn.isEmpty() && SettlementProcess.WAITING_ACCOUNTING_RELEASE.val == intProcess.intValue()) {
						resultPage = caStlHdrQueryService.findSubOrdinatesByCriteria(pernr, intProcess, positionId, pageNumber);
					}
					// Fetch All PERNR by Process
					else if (!process.isEmpty() && SettlementProcess.WAITING_ACCOUNTING_RELEASE.val == intProcess.intValue()) {
						resultPage = caStlHdrQueryService.findAllByCriteria(null, intProcess, pageNumber);
					}
				}
			}
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			else {
				List<CaStlHdrResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(caStlHdr -> {
					// GET SETTLEMENT DETAILS
					Collection<CaStlDtl> caStlDtl = caStlDtlQueryService.fetchByReqno(caStlHdr.getReqNo());
					caStlHdr.setStlDetail(caStlDtl.stream().collect(Collectors.toList()));
					
					records.add(new CaStlHdrResponseWrapper(caStlHdr));
				});
				responsePage = new PageImpl<CaStlHdrResponseWrapper>(records, caStlHdrQueryService.createPageRequest(pageNumber),
						resultPage.getTotalElements());
				response.setData(responsePage);
			}
		}
		catch (Exception e) {
			throw e;
		}
		
		Link self = linkTo(methodOn(CaStlHdrController.class).fetchByCriteria(ssn, process, advanceCode, status, pageNumber)).withSelfRel();
		response.add(self);
		
		return response;
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated CaStlHdrRequestWrapper request, BindingResult result)
				throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		
		APIResponseWrapper<?> response = null;
		Date thisDate = new Date();
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(
					new ArrayData<>(ExceptionResponseWrapper.getErrors(result.getFieldErrors(), HttpMethod.PUT)),
					"Validation error!");
		}
		else {
			//=============================================================
			// GET ALL MASTER ROLE ON DATABASE
			//=============================================================
			//EMPLOYEE REQUESTER
			Optional<User> emp = userQueryService.findByPernr(request.getPernr());
//			Optional<Role> hrAdminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_ADMIN.roleIdent));
			Optional<Role> headRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_HEAD.roleIdent));
//			Optional<Role> directorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_DIRECTOR.roleIdent));
//			Optional<Role> gaRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_GA.roleIdent));
//			Optional<Role> accountingRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_ACCOUNTING.roleIdent));
//			Optional<Role> treasuryRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_TREASURY.roleIdent));
			Optional<User> currentUser = userQueryService.findByUsername(currentUser());
			
			if (!currentUser.isPresent()) {
				throw new AuthorizationException(
						"Insufficient create privileges. Please login first!");
			}
			
			// entity model for SAP classes
			Optional<HCMSEntity> caStlHdrEntity = hcmsEntityQueryService.findByEntityName(CaStlHdr.class.getSimpleName());
			if (!caStlHdrEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + CaStlHdr.class.getSimpleName() + "' !!!");
			}
			
			Optional<CaStlHdr> caStlHdr = caStlHdrQueryService.findByTcano(request.getTcaNo());
			// PETTY CASH, BANK TRANSFER & TRAVEL ADVACE
			if (caStlHdr.isPresent()) {
				if (caStlHdr.get().getProcess().intValue() == SettlementProcess.SETTLED.val) {
					throw new EntityNotFoundException("Advance Request No. [" + request.getTcaNo() + "] is locked! <br>"
							+ "Settlement No.[" + caStlHdr.get().getReqNo() + "] has been settled");
				}
				// skip validation, exceptions for REJECTED status
				else if (caStlHdr.get().getProcess().intValue() != SettlementProcess.REJECTED.val) {
					throw new EntityNotFoundException("Advance Request No. [" + request.getTcaNo() + "] is locked! <br>"
							+ "Please check settlement no. [" + caStlHdr.get().getReqNo() + "] status");
				}
			}
			
			try {
				RequestObjectComparatorContainer<CaStlHdr, CaStlHdrRequestWrapper, String> newObjectContainer = caStlHdrRequestBuilderUtil
						.compareAndReturnUpdatedData(request, null);
				
				if (caStlHdr.isPresent() && SettlementProcess.REJECTED.val == caStlHdr.get().getProcess().intValue()) {
					newObjectContainer.getEntity().get().setReqNo(caStlHdr.get().getReqNo());
					newObjectContainer.getEntity().get().setReason(caStlHdr.get().getReason());
					
					ArrayList<CaStlDtl> listOfCaStlDtl = (ArrayList<CaStlDtl>) caStlDtlQueryService
							.fetchByReqno(newObjectContainer.getEntity().get().getReqNo()).stream().collect(Collectors.toList());
					
					//delete all detail
					if (!listOfCaStlDtl.isEmpty() && listOfCaStlDtl.size() > 0)
						caStlDtlCommandService.deleteList(listOfCaStlDtl);
				}
				
				Optional<CaStlHdr> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
					// employee can't create settlement for other employee
					Long ssn = newObjectContainer.getEntity().get().getTcaNo().getPernr();
					if (!currentUser.get().getEmployee().getPernr().equals(ssn)) {
						throw new AuthorizationException(
								"Insufficient Privileges. Please login as 'SN: " + ssn + "'!");
					}
					
					if (emp.get().getRoles().contains(headRole.get())) {
						// Requester Role Head, REVIEWER to DIRECTOR 
						newObjectContainer.getEntity().get().setProcess(SettlementProcess.WAITING_DIRECTOR_RELEASE.val);
					}
					else {
						// Requester Role Non Head
						
						// Requester have superior of ROLE HEAD
						newObjectContainer.getEntity().get().setProcess(SettlementProcess.WAITING_HEAD_RELEASE.val);
						
						/*
						Optional<User> empSpv = userQueryService.findByPernrAndRelationRole(emp.get().getEmployee().getPernr(), UserRole.ROLE_SUPERIOR.roleIdent);
						if (empSpv.isPresent() && empSpv.get().getRoles().contains(directorRole.get())) {
							// Requester don't have superior of ROLE HEAD(direct to DIRECTOR)
							newObjectContainer.getEntity().get().setProcess(SettlementProcess.WAITING_DIRECTOR_RELEASE.val);
						}
						else {
							// Requester have superior of ROLE HEAD
							newObjectContainer.getEntity().get().setProcess(SettlementProcess.WAITING_HEAD_RELEASE.val);
						}
						*/
					}
					
					newObjectContainer.getEntity().get().setCreatedAt(thisDate);
					newObjectContainer.getEntity().get().setCreatedBy(currentUser.get());
					
					Double totalStlAmount = newObjectContainer.getEntity().get().getTotalAmount();
					Double totalTcaAmount = newObjectContainer.getEntity().get().getTcaNo().getAmount();
					Double totalDiff = totalStlAmount - totalTcaAmount;
					if (totalDiff > 0) {
						// UNDER PAY
						newObjectContainer.getEntity().get().setCasStatus(SettlementCASStatus.UP.name());
						
						newObjectContainer.getEntity().get().setDiffAmount(totalDiff);
						newObjectContainer.getEntity().get().setRefundAmount(0.0);
						newObjectContainer.getEntity().get().setClaimAmount(totalTcaAmount);
					}
					else if(totalDiff < 0) {
						// OVER PAY
						newObjectContainer.getEntity().get().setCasStatus(SettlementCASStatus.OP.name());
						
						newObjectContainer.getEntity().get().setDiffAmount(Math.abs(totalDiff));
						newObjectContainer.getEntity().get().setRefundAmount(Math.abs(totalDiff));
						newObjectContainer.getEntity().get().setClaimAmount(totalTcaAmount);
					}
					else {
						// BALANCE
						newObjectContainer.getEntity().get().setCasStatus(SettlementCASStatus.BL.name());
						
						newObjectContainer.getEntity().get().setDiffAmount(0.0);
						newObjectContainer.getEntity().get().setRefundAmount(0.0);
						newObjectContainer.getEntity().get().setClaimAmount(totalTcaAmount);
					}
					// set cash advance request 
					newObjectContainer.getEntity().get().getTcaNo().setCreatedAt(thisDate);
					newObjectContainer.getEntity().get().getTcaNo().setCreatedBy(currentUser.get());
					
					savedEntity = caStlHdrCommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					
					if (savedEntity.isPresent()) {
						for (CaStlDtlRequestWrapper stlDetail : request.getStlDetail()) {
							// Set Primary Key of Settlement Header
							stlDetail.setReqNo(savedEntity.get().getReqNo());
							stlDetail.setStlDate(thisDate);
							
							RequestObjectComparatorContainer<CaStlDtl, CaStlDtlRequestWrapper, String> newObjectDtlContainer = caStlDtlRequestBuilderUtil
									.compareAndReturnUpdatedData(stlDetail, null);
							newObjectDtlContainer.getEntity().get().setCreatedAt(thisDate);
							newObjectDtlContainer.getEntity().get().setCreatedBy(currentUser.get());
							newObjectDtlContainer.getEntity().get().setUpdatedAt(thisDate);
							newObjectDtlContainer.getEntity().get().setUpdatedBy(currentUser.get());
							
							Optional<CaStlDtl> savedDetailEntity = caStlDtlCommandService.save(newObjectDtlContainer.getEntity().get(),
									currentUser.isPresent() ? currentUser.get() : null);
						} 
						caStlHdrRequestBuilderUtil.saveAttachment(savedEntity.get(), request);
						
						// SEND MAIL
						sendMail(savedEntity.get());
					}
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create settlement header data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("reqno=" + savedEntity.get().getReqNo());
				EventLog eventLog = new EventLog(caStlHdrEntity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				// Don't send email notification email for temporary settlement number
				eventLog.setMailNotification(false);
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}

				CaStlHdrResponseWrapper responseWrapper = new CaStlHdrResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(CaStlHdrController.class).create(request, result)).withSelfRel());		
		return response;
	}

	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated CaStlHdrRequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		String message = "";
		String belnr = "";
		TrxApprovalProcess trxApprovalProcess = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), CashAdvanceConstant.MESSAGE_INFO + " Validation error!");
		}
		else {
			boolean isDataChanged = false;
			
			CaStlHdrResponseWrapper responseWrapper = new CaStlHdrResponseWrapper(null);
			response = new APIResponseWrapper<>(responseWrapper);
			
			try {
				Optional<HCMSEntity> stlHdrEntity = hcmsEntityQueryService.findByEntityName(CaStlHdr.class.getSimpleName());
				if (!stlHdrEntity.isPresent()) {
					message = CashAdvanceConstant.MESSAGE_INFO + " Cannot find Entity '" + CaStlHdr.class.getSimpleName() + "' !!!";
					throw new Exception(message);
				}

				Optional<CaStlHdr> caStlHdr = caStlHdrQueryService.findByReqno(request.getReqNo());
				if (!caStlHdr.isPresent()) {
					message = CashAdvanceConstant.MESSAGE_INFO + " Cannot update data. No Data Found!";
					throw new Exception(message);
				}

				// GET DETAILS SETTLEMENT
				Collection<CaStlDtl> caStlDtl = caStlDtlQueryService.fetchByReqno(caStlHdr.get().getReqNo());
				caStlHdr.get().setStlDetail(caStlDtl.stream().collect(Collectors.toList()));

				// GET DETAILS APPROVAL
				Collection<TrxApprovalProcess> approvalDtl = trxApprovalProcessQueryService.fetchByEntityObjectId(caStlHdr.get().getReqNo(), "seq DESC");
				caStlHdr.get().setApprovalDetail(approvalDtl.stream().collect(Collectors.toList()));

				//CURRENT USER
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());

				RequestObjectComparatorContainer<CaStlHdr, CaStlHdrRequestWrapper, String> updatedContainer = caStlHdrRequestBuilderUtil
						.compareAndReturnUpdatedData(request, caStlHdr.get());
				//DATA IS EXIST
				if (updatedContainer.getRequestPayload().isPresent()) {
					isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
				}
			
				if (!isDataChanged) {
					message = CashAdvanceConstant.MESSAGE_INFO + " No data changed. No need to perform the update.";
					throw new Exception(message);
				}

				if (updatedContainer.getEntity().isPresent()) {
					Date thisDate = new Date();
					String approvalStatus = "";
					String approvalProcessNote = "";
					//String advanceCode = updatedContainer.getEntity().get().getTcaNo().getAdvanceCode();
					int countNumberOfApprovedPooling = 0;
					int currentProcess = updatedContainer.getEntity().get().getProcess().intValue();
					int nextProcess = currentProcess;

					//validation SELF APPROVED
					if (currentUser.get().getEmployee().getPernr().equals(updatedContainer.getEntity().get().getTcaNo().getPernr())) {
						message = CashAdvanceConstant.MESSAGE_INFO + " Unauthorized to self approved!";
						throw new Exception(message);
					}

					// ==========================================================================
					// POOLING APPROVAL
					// ==========================================================================
					// default Employee CSKT(Cost Center)
					List<CaEmpCSKT> caEmpCSKTs = caEmpCSKTQueryService
							.findCostCenterActiveByPernr(updatedContainer.getEntity().get().getTcaNo().getPernr()).stream().collect(Collectors.toList());
					CaEmpCSKT requesterCSKTDefault = null;
					if (caEmpCSKTs != null && caEmpCSKTs.size() > 0) {
						requesterCSKTDefault = caEmpCSKTs.get(0); // DEFAULT Cost Center Of REQUESTER
					}

					// find pooling budget
					List<CaEmpCSKT> listOfCSKTPooling = new ArrayList<CaEmpCSKT>();
					for (CaStlDtl settlementDtl : updatedContainer.getEntity().get().getStlDetail()) {
						// settlement detail is pooling budget
						if (settlementDtl.getPoolBudget() == 1) {
							// the Cost Center is Pooling Root
							if (settlementDtl.getOrderId().getCskt().getLevelCode().equals(new Integer(1))) {
								// searching PERNR of POOLING APPROVAL by COST_CENTER + POOLING_CODE(TOP_DOWN_TYPE)
								Collection<CaEmpCSKT> bunchOfCSKTTopDown = caEmpCSKTQueryService
										.findEmpCSKTByKostlPoolAndPoolingCode(requesterCSKTDefault.getCskt().getKostl(),
												CashAdvanceConstant.EMP_CSKT_POOLING_CODE_TOP_DOWN);
								if (bunchOfCSKTTopDown.size() > 0) {
									listOfCSKTPooling.add(bunchOfCSKTTopDown.stream().collect(Collectors.toList()).get(0));
								}
							}
							// the Cost Center is Non Pooling Root
							else {
								// searching PERNR of POOLING APPROVAL by COST_CENTER + POOLING_CODE(BOTTOM_UP_TYPE)
								Collection<CaEmpCSKT> bunchOfCSKTButtomUp = caEmpCSKTQueryService
										.findEmpCSKTByKostlPoolAndPoolingCode(settlementDtl.getOrderId().getCskt().getKostl(),
												CashAdvanceConstant.EMP_CSKT_POOLING_CODE_BOTTOM_UP);
								if (bunchOfCSKTButtomUp.size() > 0) {
									listOfCSKTPooling.add(bunchOfCSKTButtomUp.stream().collect(Collectors.toList()).get(0));
								}
								else {
									// searching PERNR of POOLING APPROVAL by COST_CENTER + POOLING_CODE(TOP_DOWN_TYPE)
									bunchOfCSKTButtomUp = caEmpCSKTQueryService
											.findEmpCSKTByKostlPoolAndPoolingCode(settlementDtl.getOrderId().getCskt().getKostl(),
													CashAdvanceConstant.EMP_CSKT_POOLING_CODE_TOP_DOWN);
									if (bunchOfCSKTButtomUp.size() > 0) {
										listOfCSKTPooling.add(bunchOfCSKTButtomUp.stream().collect(Collectors.toList()).get(0));
									}
								}
							}
						}
					}
					
					// validation ALL APPROVAL PROCESS
					if (request.getTrxCode().equals(CashAdvanceConstant.TRANSACTION_CODE_SUBMITED) && !approvalDtl.isEmpty()) {
						List<TrxApprovalProcess> listOfApproval = approvalDtl.stream().collect(Collectors.toList());
						for (TrxApprovalProcess item : listOfApproval) {
							if (item.getProcess().intValue() >= SettlementProcess.WAITING_HEAD_RELEASE.val) {
								//validation DUPLICATE APPROVAL
								if (currentProcess == SettlementProcess.WAITING_HEAD_RELEASE.val
										&& UserRole.ROLE_HEAD.roleIdent.equals(item.getRoleId().getName())
										&& item.getApprovalBy().equals(currentUser.get())) {
									message = CashAdvanceConstant.MESSAGE_INFO 
											+ "SN: " + currentUser.get().getEmployee().getPernr() + ", " 
											+ "Duplicate " + CashAdvanceConstant.APPROVAL_BY_HEAD + " approval!";
									throw new Exception(message);
								}
								else if (currentProcess == SettlementProcess.WAITING_DIRECTOR_RELEASE.val
										&& UserRole.ROLE_DIRECTOR.roleIdent.equals(item.getRoleId().getName()) 
										&& item.getApprovalBy().equals(currentUser.get())) {
									message = CashAdvanceConstant.MESSAGE_INFO 
											+ "SN: " + currentUser.get().getEmployee().getPernr() + ", " 
											+ "Duplicate " + CashAdvanceConstant.APPROVAL_BY_DIRECTOR + " approval!";
									throw new Exception(message);
								}
								else if (currentProcess == SettlementProcess.WAITING_GA_RELEASE.val
										&& UserRole.ROLE_GA.roleIdent.equals(item.getRoleId().getName())
										&& item.getApprovalBy().equals(currentUser.get())) {
									message = CashAdvanceConstant.MESSAGE_INFO 
											+ "SN: " + currentUser.get().getEmployee().getPernr() + ", " 
											+ "Duplicate " + CashAdvanceConstant.APPROVAL_BY_GA + " approval!";
									throw new Exception(message);
								}
								else if (currentProcess == SettlementProcess.WAITING_HR_RELEASE.val
										&& UserRole.ROLE_ADMIN.roleIdent.equals(item.getRoleId().getName()) 
										&& item.getApprovalBy().equals(currentUser.get())) {
									message = CashAdvanceConstant.MESSAGE_INFO 
											+ "SN: " + currentUser.get().getEmployee().getPernr() + ", " 
											+ "Duplicate " + CashAdvanceConstant.APPROVAL_BY_HR + " approval!";
									throw new Exception(message);
								}
								else if (currentProcess == SettlementProcess.WAITING_ACCOUNTING_RELEASE.val
										&& UserRole.ROLE_ACCOUNTING.roleIdent.equals(item.getRoleId().getName())
										&& item.getApprovalBy().equals(currentUser.get())
										&& item.getApprovalAction().equals(CashAdvanceConstant.TRANSACTION_APPROVED)) {
									message = CashAdvanceConstant.MESSAGE_INFO 
											+ "SN: " + currentUser.get().getEmployee().getPernr() + ", " 
											+ "Duplicate " + CashAdvanceConstant.APPROVAL_BY_ACCOUNTING + " approval!";
									throw new Exception(message);
								}
								else if (currentProcess == SettlementProcess.WAITING_POOLING_RELEASE.val
										&& UserRole.ROLE_POOLING.roleIdent.equals(item.getRoleId().getName())
										&& item.getApprovalNote().contains("kostl=") && listOfCSKTPooling.size() > 0) {
									// validation DUPLICATE APPROVAL POOLING by KOSTL & PERNR APPROVAL
									for (CaEmpCSKT csktPooling : listOfCSKTPooling) {
//										Optional<User> approvalPoolingUser = userQueryService.findByPernr(csktPooling.getPernr());
										if (item.getApprovalNote().contains("kostl=" + csktPooling.getKostlPool())
												&& item.getApprovalNote().contains("pernr=" + currentUser.get().getEmployee().getPernr())) {
											message = CashAdvanceConstant.MESSAGE_INFO 
													+ "SN: " + currentUser.get().getEmployee().getPernr() + " AND "
													+ "Cost center: " + csktPooling.getKostlPool() + ", " 
													+ "Duplicate " + CashAdvanceConstant.APPROVAL_BY_POOLING + " approval!";
											throw new Exception(message);
										}
										else {
											countNumberOfApprovedPooling++;
										}
									} 
								}
							} // if (item.getProcess().intValue() >= SettlementProcess.WAITING_HEAD_RELEASE.val) {
							else {
								break; // REJECTED, nothing validation DUPLICATE APPROVAL
							}
						} // for (TrxApprovalProcess item : listOfApproval) {
						
					} // if (request.getTrxCode().equals(CashAdvanceConstant.TRANSACTION_CODE_SUBMITED) && !approvalDtl.isEmpty()) {
					
					// ========================================================
					// 	SET TRANSACTION APPROVAL PROCESS (HISTORICAL APPROVAL)
					// ========================================================
					trxApprovalProcess = new TrxApprovalProcess();
					trxApprovalProcess.setProcess(updatedContainer.getEntity().get().getProcess());
					
					// APPROVED
					if (request.getTrxCode().equals(CashAdvanceConstant.TRANSACTION_CODE_SUBMITED)) {
						approvalStatus = CashAdvanceConstant.TRANSACTION_APPROVED;
						// VALIDATION Role Approval
						WeakHashMap<String, Object> validationAppoval = caStlHdrRequestBuilderUtil.validationAppoval(
								currentProcess, currentUser, updatedContainer.getEntity().get(), listOfCSKTPooling, countNumberOfApprovedPooling);

						approvalProcessNote = (String) validationAppoval.get("APPROVAL_PROCESS_NOTE");
						trxApprovalProcess.setRoleId((Role) validationAppoval.get("TRX_APPROVAL_PROCESS"));
						// next process
						nextProcess = (Integer) validationAppoval.get("PROCESS");
					}
					// REJECTED
					else if (request.getTrxCode().equals(CashAdvanceConstant.TRANSACTION_CODE_REJECTED)) {
						approvalStatus = CashAdvanceConstant.TRANSACTION_REJECTED;
						nextProcess = SettlementProcess.REJECTED.val;
					}
					updatedContainer.getEntity().get().setProcess(nextProcess);
					updatedContainer.getEntity().get().setUpdatedAt(thisDate);
					updatedContainer.getEntity().get().setUpdatedBy(currentUser.get());
					
					// ========================================================================
					//	APPROVAL NOTE
					// ========================================================================
					String approvalNote = CashAdvanceConstant.SETTLEMENT_APPROVAL_NOTE_TEMPLATE;
					approvalNote = approvalNote.replace("APPROVAL_CREATED_AT", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(thisDate));
					approvalNote = approvalNote.replace("APPROVAL_NAME", currentUser.get().getEmployee().getEname());
					approvalNote = approvalNote.replace("STATUS_APPROVAL", approvalStatus);
					approvalNote = approvalNote.replace("APPROVAL_MSG", request.getReason());
					// Old approval note
					if (StringUtils.isNotEmpty(updatedContainer.getEntity().get().getReason())) {
						approvalNote += updatedContainer.getEntity().get().getReason();
					}
					updatedContainer.getEntity().get().setReason(approvalNote);
					
					trxApprovalProcess.setCfgEntityId(stlHdrEntity.get());
					trxApprovalProcess.setEntityObjectId(updatedContainer.getEntity().get().getReqNo());
					trxApprovalProcess.setSeq(approvalDtl.size());
					trxApprovalProcess.setApprovalAt(thisDate);
					trxApprovalProcess.setApprovalBy(currentUser.get());
					trxApprovalProcess.setApprovalAction(approvalStatus);
					trxApprovalProcess.setApprovalNote(approvalProcessNote);
					
					// posting to SAP
					if (currentProcess == SettlementProcess.WAITING_TREASURY_RELEASE.val
							|| currentProcess == SettlementProcess.READY_TO_SETTLE.val) {

						String muleService = null;
						String uri = null;
						CaStlHdr caStlHdrEntity = caStlHdr.get();
						CaReqHdr caReqHdrEntity = caStlHdr.get().getTcaNo();

						// SETTLEMENT BAPI
						// input BAPI Import Parameters - I_BUKRS, I_BUDAT, I_XBLNR, I_SN
						CaStlHdrSoapObject caStlHdrSoapObject = new CaStlHdrSoapObject(caStlHdrEntity);
						List<CaStlDtlSoapObject> caStlDtlSoapObjects = new ArrayList<CaStlDtlSoapObject>();
						ArrayList<CaStlDtl> listCaStlDtl = (ArrayList<CaStlDtl>) caStlDtlQueryService
								.fetchByReqno(caStlHdrEntity.getReqNo()).stream().collect(Collectors.toList());

						// ADVANCE REQUEST BAPI
						// input BAPI Import Parameters - I_BUKRS, I_BUDAT, I_HKONT, I_XBLNR, I_SN, I_WRBTR, I_TEXT
						CaReqHdrSoapObject caReqSoapObject = new CaReqHdrSoapObject(caReqHdrEntity);
						caStlHdrEntity.setLastSync(new Date());
						
						// PETTY CASH
						if (CaAdvanceCode.PC.name().equals(caStlHdrEntity.getTcaNo().getAdvanceCode())) {
							// BALANCE
							if (SettlementCASStatus.BL.name().equals(caStlHdrEntity.getCasStatus())) {
								if (currentProcess == SettlementProcess.READY_TO_SETTLE.val) {
									// BAPI => ZFI_SETTL_ADVANCE_AQ
									muleService = BAPIFunction.CASTLHDR_EQUAL_ADVANCE_CREATE.service();
									uri = BAPIFunction.CASTLHDR_EQUAL_ADVANCE_CREATE_URI.service();
								}
							}
							// OVERPAY
							else if (SettlementCASStatus.OP.name().equals(caStlHdrEntity.getCasStatus())) {
								if (currentProcess == SettlementProcess.WAITING_TREASURY_RELEASE.val) {
									// BAPI => ZFI_SETTL_ADVANCE_LT
									muleService = BAPIFunction.CASTLHDR_LT_CREATE.service();
									uri = BAPIFunction.CASTLHDR_LT_CREATE_URI.service();
									
									caStlHdrSoapObject.setAmount(String.valueOf(caStlHdrEntity.getDiffAmount().intValue())+"");
								}
								else if (currentProcess == SettlementProcess.READY_TO_SETTLE.val) {
									// BAPI => ZFI_PAY_EMP_TO_PC
									muleService = BAPIFunction.CA_PAY_EMP_TO_PC.service();
									uri = BAPIFunction.CA_PAY_EMP_TO_PC_URI.service();
									
									caReqHdrEntity.setAmount(caStlHdrEntity.getDiffAmount());
									caReqHdrEntity.setDescription(caStlHdrEntity.getNotes());
									caReqSoapObject = new CaReqHdrSoapObject(caReqHdrEntity);
								}
							}
							// UNDERPAY
							else if (SettlementCASStatus.UP.name().equals(caStlHdrEntity.getCasStatus())) {
								if (currentProcess == SettlementProcess.WAITING_TREASURY_RELEASE.val) {
//									if (caStlHdrEntity.getTotalAmount() > CashAdvanceConstant.PETTY_CASH_LIMIT) {
//										// BAPI => ZFI_SETTL_ADVANCE_LT
//										muleService = BAPIFunction.CASTLHDR_LT_CREATE.service();
//										uri = BAPIFunction.CASTLHDR_LT_CREATE_URI.service();
//									}
//									else {
										// BAPI => ZFI_SETTL_ADVANCE_GT
										muleService = BAPIFunction.CASTLHDR_GT_CREATE.service();
										uri = BAPIFunction.CASTLHDR_GT_CREATE_URI.service();
//									}
									
									caStlHdrSoapObject.setAmount(String.valueOf(caStlHdrEntity.getDiffAmount().intValue())+"");
									
								}
								else if (currentProcess == SettlementProcess.READY_TO_SETTLE.val) {
									if (caStlHdrEntity.getTotalAmount() > CashAdvanceConstant.PETTY_CASH_LIMIT) {
										// BAPI => ZFI_PAY_EMP_TO_PC
										muleService = BAPIFunction.CA_PAY_EMP_TO_PC.service();
										uri = BAPIFunction.CA_PAY_EMP_TO_PC_URI.service();

										caReqHdrEntity.setAmount(caStlHdrEntity.getDiffAmount());
										caReqSoapObject = new CaReqHdrSoapObject(caReqHdrEntity);
									}
									else {
										// BAPI => ZFI_PAY_ADVANCE_PC
										muleService = BAPIFunction.CAREQHDR_PETTY_CASH_RECEIVE_CREATE.service();
										uri = BAPIFunction.CAREQHDR_PETTY_CASH_RECEIVE_CREATE_URI.service();

										caReqHdrEntity.setAmount(caStlHdrEntity.getDiffAmount());
										caReqHdrEntity.setDescription(caStlHdrEntity.getNotes());
										caReqSoapObject = new CaReqHdrSoapObject(caReqHdrEntity);
									}
								}
							}
							
							// input BAPI Tables - I_EXPENSE (HKONT, KOSTL, WRBTR)
							for (int i=0; i<listCaStlDtl.size(); i++) {
								caStlDtlSoapObjects.add(new CaStlDtlSoapObject(listCaStlDtl.get(i)));
							}
							caStlHdrSoapObject.setListCaStlDtl(caStlDtlSoapObjects);
						}
						// BANK TRANSFER
						else if (CaAdvanceCode.BT.name().equals(caStlHdrEntity.getTcaNo().getAdvanceCode())) {
							// BALANCE
							if (SettlementCASStatus.BL.name().equals(caStlHdrEntity.getCasStatus())) {
								if (currentProcess == SettlementProcess.READY_TO_SETTLE.val) {
									// BAPI => ZFI_SETTL_ADVANCE_AQ
									muleService = BAPIFunction.CASTLHDR_EQUAL_ADVANCE_CREATE.service();
									uri = BAPIFunction.CASTLHDR_EQUAL_ADVANCE_CREATE_URI.service();
								}
							}
							// OVERPAY
							else if (SettlementCASStatus.OP.name().equals(caStlHdrEntity.getCasStatus())) {
								if (currentProcess == SettlementProcess.WAITING_TREASURY_RELEASE.val) {
									// BAPI => ZFI_SETTL_ADVANCE_LT
									muleService = BAPIFunction.CASTLHDR_LT_CREATE.service();
									uri = BAPIFunction.CASTLHDR_LT_CREATE_URI.service();
									
									caStlHdrSoapObject.setAmount(String.valueOf(caStlHdrEntity.getDiffAmount().intValue())+"");
								}
								else if (currentProcess == SettlementProcess.READY_TO_SETTLE.val) {
									// BAPI => ZFI_CLEARSYNC
								}
							}
							// UNDERPAY
							else if (SettlementCASStatus.UP.name().equals(caStlHdrEntity.getCasStatus())) {
								if (currentProcess == SettlementProcess.WAITING_TREASURY_RELEASE.val) {
									// BAPI => ZFI_SETTL_ADVANCE_GT
									muleService = BAPIFunction.CASTLHDR_GT_CREATE.service();
									uri = BAPIFunction.CASTLHDR_GT_CREATE_URI.service();
									
									caStlHdrSoapObject.setAmount(String.valueOf(caStlHdrEntity.getDiffAmount().intValue())+"");
								}
								else if (currentProcess == SettlementProcess.READY_TO_SETTLE.val) {
									// BAPI => ZFI_CLEARSYNC
								}
							}
							
							// input BAPI Tables - I_EXPENSE (HKONT, KOSTL, WRBTR)
							for (int i=0; i<listCaStlDtl.size(); i++) {
								caStlDtlSoapObjects.add(new CaStlDtlSoapObject(listCaStlDtl.get(i)));
							}
							caStlHdrSoapObject.setListCaStlDtl(caStlDtlSoapObjects);
						}
						// TRAVEL ADVANCE
						else if (CaAdvanceCode.TA.name().equals(caStlHdrEntity.getTcaNo().getAdvanceCode())) {
							
						}
						// REIMBURSEMENT
						else if (CaAdvanceCode.RE.name().equals(caStlHdrEntity.getTcaNo().getAdvanceCode())) {
							if (currentProcess == SettlementProcess.READY_TO_SETTLE.val) {
								// BAPI => ZFI_REIMBURSEMENT_PC
								muleService = BAPIFunction.CAREQHDR_REIMBURSEMENT_CREATE.service();
								uri =BAPIFunction.CAREQHDR_REIMBURSEMENT_CREATE_URI.service();
								
								// input BAPI Tables - I_EXPENSE (HKONT, KOSTL, WRBTR)
								for (int i=0; i<listCaStlDtl.size(); i++) {
									caStlDtlSoapObjects.add(new CaStlDtlSoapObject(listCaStlDtl.get(i)));
								}
								caStlHdrSoapObject.setListCaStlDtl(caStlDtlSoapObjects);
							}
						}
						
						HashMap<String, String> resultMap = null;
						if (uri != null) {
							// ADVANCE TO SAP
							if (CaAdvanceCode.PC.name().equals(caStlHdrEntity.getTcaNo().getAdvanceCode())
									&& currentProcess == SettlementProcess.READY_TO_SETTLE.val
									&& !SettlementCASStatus.BL.name().equals(caStlHdrEntity.getCasStatus())) {
								// PETTY CASH (READY TO SETTLE & NOT BALANCE)
								resultMap = (HashMap<String, String>) muleConsumerService.postToSAPSync(muleService, uri, caReqSoapObject);
							}
							// SETTLEMENT TO SAP
							else {
								// WAITING_TREASURY_RELEASE, PETTY CASH BALANCE, REIMBURSEMENT
								resultMap = (HashMap<String, String>) muleConsumerService.postToSAPSync(muleService, uri, caStlHdrSoapObject);
							}
							
							if (resultMap == null) {
								message = CashAdvanceConstant.MESSAGE_SAP + " SAP result is null, please contact administrator!";
								throw new Exception(message);
							}
							belnr = resultMap.get("belnr");
							if (belnr != StringUtils.EMPTY) {
								if (belnr.equals("FAULT")) {
									message = CashAdvanceConstant.MESSAGE_INFO + " Settlement is settled [Acct.Doc: " + belnr + "]";
									// Roll Back previous status
									updatedContainer.getEntity().get().setProcess(trxApprovalProcess.getProcess());
									updatedContainer.getEntity().get().setSapMessage(message);
									trxApprovalProcess = null;
									throw new CannotPersistException(message);
								}
								else {
									if (currentProcess == SettlementProcess.WAITING_TREASURY_RELEASE.val) {
										updatedContainer.getEntity().get().setDocNo(belnr);
										updatedContainer.getEntity().get().setPaymentStatus(CashAdvanceConstant.PAYMENT_STATUS_IS_PAID);
										updatedContainer.getEntity().get().setPaymentStatus2(CashAdvanceConstant.PAYMENT_STATUS_IS_WAITING);
									}
									else if (currentProcess == SettlementProcess.READY_TO_SETTLE.val) {
										updatedContainer.getEntity().get().setPaymentDate(thisDate);
										updatedContainer.getEntity().get().setDocNo2(belnr);
										updatedContainer.getEntity().get().setPaymentStatus2(CashAdvanceConstant.PAYMENT_STATUS_IS_PAID);
									}
								}
							}
							else if (resultMap.get("message") != StringUtils.EMPTY
									&& belnr == StringUtils.EMPTY) {
								message = CashAdvanceConstant.MESSAGE_SAP + " " + resultMap.get("message");
								// Roll Back previous status
								updatedContainer.getEntity().get().setProcess(trxApprovalProcess.getProcess());
								updatedContainer.getEntity().get().setSapMessage(message);
								trxApprovalProcess = null;
								throw new CannotPersistException(message);
							}
							else {
								message = CashAdvanceConstant.MESSAGE_SAP + " SAP result fatal error, please contact administrator!";
								// Roll Back previous status
								updatedContainer.getEntity().get().setProcess(trxApprovalProcess.getProcess());
								updatedContainer.getEntity().get().setSapMessage(message);
								trxApprovalProcess = null;
								throw new CannotPersistException(message);
							}
						}
						else {
							message = CashAdvanceConstant.MESSAGE_INFO + " URL RFC SAP not available!";
							// Roll Back previous status
							updatedContainer.getEntity().get().setProcess(trxApprovalProcess.getProcess());
							updatedContainer.getEntity().get().setSapMessage(message);
							trxApprovalProcess = null;
							throw new CannotPersistException(message);
						}
						
						// UPDATE TO DATABASE(After getting DOC NO. SAP)
						if (belnr != null && !belnr.isEmpty() && !belnr.equals("FAULT")) {
							User savedByCurrentUser = currentUser.isPresent() ? currentUser.get() : null;
							caStlHdr = caStlHdrCommandService.save(updatedContainer.getEntity().get(), savedByCurrentUser);
							if (!caStlHdr.isPresent()) {
								message = CashAdvanceConstant.MESSAGE_INFO + " Cannot update Settlement Header is fail. Please check your data!";
								throw new Exception(message);
							}
							
						}
					}
					// Not posting to SAP
					else {
						// UPDATE TO DATABASE
						User savedByCurrentUser = currentUser.isPresent() ? currentUser.get() : null;
						caStlHdr = caStlHdrCommandService.save(updatedContainer.getEntity().get(), savedByCurrentUser);
					}
				} // if (updatedContainer.getEntity().isPresent()) {
			
				// SAVE TO TRANSACTION APPROVAL PROCESS
				if (caStlHdr.isPresent() && trxApprovalProcess != null) {
					if (caStlHdr.get().getProcess() == SettlementProcess.REJECTED.val) {
						trxApprovalProcessCommandService.deleteList(approvalDtl.stream().collect(Collectors.toList()));
					}
 					trxApprovalProcessCommandService.save(trxApprovalProcess,
							currentUser.isPresent() ? currentUser.get() : null);
				}
				
				// SEND MAIL
				if (caStlHdr.isPresent()) {
					sendMail(updatedContainer.getEntity().get());
				}
				
				// SAVE TO EVENT LOG
				caStlHdrSaveToEventLog(caStlHdr, OperationType.UPDATE, updatedContainer, currentUser);
			
				responseWrapper = new CaStlHdrResponseWrapper(caStlHdr.get());
				response = new APIResponseWrapper<>(responseWrapper);
			}
			catch (NullPointerException nfe) {
				if (!nfe.getMessage().contains(CashAdvanceConstant.MESSAGE_INFO) 
						&& !nfe.getMessage().contains(CashAdvanceConstant.MESSAGE_SAP)) {
					message = CashAdvanceConstant.MESSAGE_ERROR + nfe.getMessage();
				}
				else {
					message = nfe.getMessage();
				}
				//response.setMessage(message);
				//throw nfe;
			}
			catch (CannotPersistException e) {
				if (!e.getMessage().contains(CashAdvanceConstant.MESSAGE_INFO) 
						&& !e.getMessage().contains(CashAdvanceConstant.MESSAGE_SAP)) {
					message = CashAdvanceConstant.MESSAGE_ERROR + e.getMessage();
				}
				else {
					message = e.getMessage();
				}
				//throw e;
			}
			catch (DataViolationException e) { 
				if (!e.getMessage().contains(CashAdvanceConstant.MESSAGE_INFO) 
						&& !e.getMessage().contains(CashAdvanceConstant.MESSAGE_SAP)) {
					message = CashAdvanceConstant.MESSAGE_ERROR + e.getMessage();
				}
				else {
					message = e.getMessage();
				}
				//throw e;
			}
			catch (Exception e) {
				if (!e.getMessage().contains(CashAdvanceConstant.MESSAGE_INFO) 
						&& !e.getMessage().contains(CashAdvanceConstant.MESSAGE_SAP)) {
					message = CashAdvanceConstant.MESSAGE_ERROR + e.getMessage();
				}
				else {
					message = e.getMessage();
				}
				//response.setMessage(message);
				//throw e;
			}
		} // else - if (result.hasErrors()) {
		
		if (message != null && !message.isEmpty())
			response.setMessage(message);
		response.add(linkTo(methodOn(CaStlHdrController.class).edit(request, result)).withSelfRel());
		return response;
	}

	/**
	 * SAVE TO EVENT LOG
	 * 
	 * @param CaStlHdr
	 * @param operationType
	 * @param currentUser
	 */
	private void caStlHdrSaveToEventLog(Optional<CaStlHdr> caStlHdr, OperationType operationType,
			RequestObjectComparatorContainer<CaStlHdr, CaStlHdrRequestWrapper, String> objContainer,
			Optional<User> currentUser) {
		StringBuilder key = new StringBuilder("reqno=" + caStlHdr.get().getReqNo());
		Optional<HCMSEntity> stlHdrEntity = hcmsEntityQueryService.findByEntityName(CaStlHdr.class.getSimpleName());
		
		if (!stlHdrEntity.isPresent()) 
			throw new EntityNotFoundException("Cannot find Entity '" + CaStlHdr.class.getSimpleName() + "' !!!");
		
		EventLog eventLog = new EventLog(stlHdrEntity.get(), key.toString(), operationType, objContainer.getEventData().get());
		eventLog.setStatus("process=" + caStlHdr.get().getProcess() + ",");
		Optional<EventLog> updateEventLog;
		try {
			updateEventLog = eventLogCommandService.save(eventLog, currentUser.get());
			if (!updateEventLog.isPresent())
				throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
		} catch (ConstraintViolationException | CannotPersistException | NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void sendMail(CaStlHdr caStlHdr) {
		// TO EMPLOYEE
		submitMail(mailTemplate(UserRole.ROLE_EMPLOYEE.roleIdent, caStlHdr));
		// TO APROVAL
		submitMail(mailTemplate(null, caStlHdr));
	}

	private WeakHashMap<String, String> mailTemplate(String role, CaStlHdr caStlHdr) {
		String MAIL_TO = "";
		String MAIL_SUBJECT = "";
		String MAIL_BODY = "";
		String DEAR_NAME = "";
		String APPROVAL_BY = "";
		Optional<User> emp;
		WeakHashMap<String, String> result;
		Integer process = caStlHdr.getProcess();
		
		if (process == SettlementProcess.WAITING_HEAD_RELEASE.val) {
			APPROVAL_BY = CashAdvanceConstant.APPROVAL_BY_HEAD;
			
			Optional<User> approvalUser = userQueryService.findByPernrAndRelationRole(caStlHdr.getTcaNo().getPernr(),
					UserRole.ROLE_HEAD.roleIdent);
			if (approvalUser.isPresent()) {
				DEAR_NAME 	= approvalUser.get().getEmployee().getEname();
				MAIL_TO 	= approvalUser.get().getEmail();
			}
		}
		else if (process == SettlementProcess.WAITING_DIRECTOR_RELEASE.val) {
			APPROVAL_BY = CashAdvanceConstant.APPROVAL_BY_DIRECTOR;
			
			Optional<User> approvalUser = userQueryService.findByPernrAndRelationRole(caStlHdr.getTcaNo().getPernr(),
					UserRole.ROLE_DIRECTOR.roleIdent);
			if (approvalUser.isPresent()) {
				DEAR_NAME 	= approvalUser.get().getEmployee().getEname();
				MAIL_TO = approvalUser.get().getEmail();
			}
		}
		else if (process == SettlementProcess.WAITING_GA_RELEASE.val) {
			APPROVAL_BY = CashAdvanceConstant.APPROVAL_BY_GA;
			
			Optional<User> approvalUser = userQueryService.findByPernrAndRelationRole(caStlHdr.getTcaNo().getPernr(),
					UserRole.ROLE_GA.roleIdent);
			if (approvalUser.isPresent()) {
				DEAR_NAME 	= approvalUser.get().getEmployee().getEname();
				MAIL_TO = approvalUser.get().getEmail();
			}
		}
		else if (process == SettlementProcess.WAITING_HR_RELEASE.val) {
			APPROVAL_BY = CashAdvanceConstant.APPROVAL_BY_HR;
			
			Optional<User> approvalUser = userQueryService.findByPernrAndRelationRole(caStlHdr.getTcaNo().getPernr(),
					UserRole.ROLE_ADMIN.roleIdent);
			if (approvalUser.isPresent()) {
				DEAR_NAME 	= approvalUser.get().getEmployee().getEname();
				MAIL_TO = approvalUser.get().getEmail();
			}
		}
		else if (process == SettlementProcess.WAITING_ACCOUNTING_RELEASE.val) {
			APPROVAL_BY = CashAdvanceConstant.APPROVAL_BY_ACCOUNTING;
			
			Optional<User> approvalUser = userQueryService.findByPernrAndRelationRole(caStlHdr.getTcaNo().getPernr(),
					UserRole.ROLE_ACCOUNTING.roleIdent);
			if (approvalUser.isPresent()) {
				DEAR_NAME 	= approvalUser.get().getEmployee().getEname();
				MAIL_TO = approvalUser.get().getEmail();
			}
		}
		else if (process == SettlementProcess.WAITING_TREASURY_RELEASE.val) {
			APPROVAL_BY = CashAdvanceConstant.APPROVAL_BY_TREASURY;
			
			Optional<User> approvalUser = userQueryService.findByPernrAndRelationRole(caStlHdr.getTcaNo().getPernr(),
					UserRole.ROLE_TREASURY.roleIdent);
			if (approvalUser.isPresent()) {
				DEAR_NAME 	= approvalUser.get().getEmployee().getEname();
				MAIL_TO = approvalUser.get().getEmail();
			}
		}
			
		if (role != null && role.equals(UserRole.ROLE_EMPLOYEE.name())) {
			DEAR_NAME 		= caStlHdr.getCreatedBy().getEmployee().getEname();
			MAIL_TO 		= caStlHdr.getCreatedBy().getEmail();
			MAIL_SUBJECT	= "Cash Advance Settlement - [SETTLEMENT_ID]";
			MAIL_BODY		= 
					"Dear Mr/Ms <b>[DEAR_NAME]</b><br>" +
					"<br>" +
					"Your request for Cash Advance Settlement below :<br>" +
					"<br>" +
					"Name&emsp; = [EMPLOYEE_NAME]&nbsp;[SSN]<br>" +
					"Total&emsp; = [SETTLEMENT_TOTAL]<br>" +
					"<br>" +
					"Has been released, your [APPROVAL_BY] will review your submitted data prior to approval.<br>" +
					"To view this request please click this <a href='[MAIL_HREF]'>Show Request</a>.";
		}
		else {
			MAIL_SUBJECT	= "Cash Advance Settlement - [SETTLEMENT_ID]";
			MAIL_BODY		= 
					"Dear Mr/Ms <b>[DEAR_NAME]</b><br>" +
					"<br>" +
					"User request for Cash Advance Settlement below :<br>" +
					"<br>" +
					"Name&emsp; = [EMPLOYEE_NAME]&nbsp;[SSN]<br>" +
					"Total&emsp; = [SETTLEMENT_TOTAL]<br>" +
					"<br>" +
					"has been released, waiting for your review and approval.<br>" +
					"To view this request please click this <a href='[MAIL_HREF]'>Show Request</a>";
		}
		
		result = new WeakHashMap<String, String>();
		emp = userQueryService.findByPernr(caStlHdr.getTcaNo().getPernr());
		
		
		MAIL_SUBJECT = MAIL_SUBJECT.replace("[SETTLEMENT_ID]", caStlHdr.getReqNo());
		MAIL_BODY = MAIL_BODY.replace("[DEAR_NAME]", DEAR_NAME);
		MAIL_BODY = MAIL_BODY.replace("[EMPLOYEE_NAME]", emp.get().getEmployee().getEname());
		MAIL_BODY = MAIL_BODY.replace("[SSN]", "[" + emp.get().getEmployee().getPernr() + "]");
		MAIL_BODY = MAIL_BODY.replace("[SETTLEMENT_TOTAL]", caStlHdr.getCurrency() + " " + String.format ("%,.2f", caStlHdr.getTotalAmount()));
		MAIL_BODY = MAIL_BODY.replace("[APPROVAL_BY]", APPROVAL_BY);
		MAIL_BODY = MAIL_BODY.replace("[MAIL_HREF]", envProps.getProperty("mail.notification.url").concat("ca_settlement/summary?reqno=").concat(caStlHdr.getReqNo()));
		
		result.put(CashAdvanceConstant.MAIL_TO, MAIL_TO);
		result.put(CashAdvanceConstant.MAIL_SUBJECT, MAIL_SUBJECT);
		result.put(CashAdvanceConstant.MAIL_BODY, MAIL_BODY);
		
		return result;
	}

	private void submitMail(WeakHashMap<String, String> template) {
		mailService.sendMail(
				template.get(CashAdvanceConstant.MAIL_TO),
				template.get(CashAdvanceConstant.MAIL_SUBJECT) + " : ", 
				template.get(CashAdvanceConstant.MAIL_BODY)
		);
	}

	private int updateDocumentStatus(List<CaStlHdr> listCaStlHdr) {
		if (listCaStlHdr.isEmpty()) return 0;
		
		CaReqHdr advReqHdr;
		List<CaReqHdr> listCaReqHdr = new ArrayList<CaReqHdr>();
		List<CaStlHdr> listCaStlHdrForCurrentSync = new ArrayList<CaStlHdr>();
		for (CaStlHdr caStlHdr : listCaStlHdr) {
			if (CaAdvanceCode.BT.name().equals(caStlHdr.getTcaNo().getAdvanceCode())
					&& SettlementProcess.READY_TO_SETTLE.val == caStlHdr.getProcess()
					&& CashAdvanceConstant.PAYMENT_STATUS_IS_WAITING.equals(caStlHdr.getPaymentStatus2())) {
				
				listCaStlHdrForCurrentSync.add(caStlHdr);
				
				// Ready to sync Doc No. SAP
				advReqHdr = caStlHdr.getTcaNo();
				advReqHdr.setLastSync(caStlHdr.getLastSync());
				advReqHdr.setDescription(caStlHdr.getNotes());
				
				listCaReqHdr.add(advReqHdr);
			}
		}
		
		List<CaReqHdr> listCaReqHdrForSync =  new ArrayList<CaReqHdr>();
		listCaReqHdr.forEach(caReqHdr -> {
			// sync / update document status only if last sync was 12 minutes past
			if (caReqHdr.getLastSync() == null
					|| (int)(new Date().getTime() - caReqHdr.getLastSync().getTime()) / (60 * 1000) >= 12)
				listCaReqHdrForSync.add(caReqHdr);
		});
		if (!listCaReqHdrForSync.isEmpty()) {
			
			CaHdrDocSoapObject caHdrDocSoapObject = new CaHdrDocSoapObject();
			
			List<CaReqHdrSoapObject> listCaReqHdrSoapObject =  new ArrayList<CaReqHdrSoapObject>();
			for (CaReqHdr caReqHdr : listCaReqHdrForSync) {
				listCaReqHdrSoapObject.add(new CaReqHdrSoapObject(caReqHdr.getBukrs().getBukrs(), 
						CommonDateFunction.convertDateToStringY(caReqHdr.getApprovedTreasuryAt()), caReqHdr.getReqNo()));
			}
			caHdrDocSoapObject.setListCaReqHdr(listCaReqHdrSoapObject);
			
			List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.CA_DOCUMENT_FETCH.service(),
					BAPIFunction.CA_DOCUMENT_FETCH_URI.service(), caHdrDocSoapObject);
			
			for (Map<String, String> map : list) {
				// synchronize CaReqHdr Document Status fields (in different thread) with the fetched fields from SAP
				if (map.get("reqno") != null
						&& StringUtils.isNotEmpty(map.get("augbl"))) {
					for (CaStlHdr caStlHdr : listCaStlHdrForCurrentSync) {
						if (caStlHdr.getTcaNo().getReqNo().equals(map.get("reqno"))) {
							caStlHdr.setDocNo2(map.get("augbl"));
							caStlHdr.setLastSync(new Date());
							caStlHdr.setPaymentStatus2(CashAdvanceConstant.PAYMENT_STATUS_IS_PAID);
							try {
								caStlHdrCommandService.save(caStlHdr, null);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
			return 1;
		}
		return 0;
	}
}
