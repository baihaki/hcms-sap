package com.abminvestama.hcms.rest.api.dto.response;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.Date;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.WorkflowStep;
import com.abminvestama.hcms.rest.api.controller.WorkflowStepController;
import com.abminvestama.hcms.rest.api.model.constant.HCMSResourceIdentifier;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@JsonInclude(Include.NON_NULL)
public class WorkflowStepResponseWrapper extends ResourceSupport {

	public static final String RESOURCE = HCMSResourceIdentifier.WORKFLOW_STEPS.label();
	
	private String uuid;
	private WorkflowResponseWrapper workflow;
	private int seq;
	private String description;
	private PositionResponseWrapper position;
	private String sequenceType;
	private String approvalType;
	private Date createdAt;
	private AuthorResponseWrapper createdBy;
	private Date updatedAt;
	private AuthorResponseWrapper updatedBy;
	private Link link;
	
	public WorkflowStepResponseWrapper(WorkflowStep workflowStep) {
		this.uuid = workflowStep.getId();
		this.workflow = new WorkflowResponseWrapper(workflowStep.getWorkflow());
		this.seq = workflowStep.getSeq();
		this.description = workflowStep.getDescription();
		this.position = new PositionResponseWrapper(workflowStep.getPosition());
		this.sequenceType = workflowStep.getSequenceType().name();
		this.approvalType = workflowStep.getApprovalType().name();
		this.createdAt = workflowStep.getCreatedAt();
		this.updatedAt = workflowStep.getUpdatedAt();
		try {
			this.link = linkTo(methodOn(WorkflowStepController.class).fetchById(workflowStep.getId()))
					.withRel(RESOURCE);
		} catch (Exception e) {}
	}
	
	@JsonProperty("id")
	public String getUuid() {
		return uuid;
	}
	
	@JsonProperty("workflow")
	public WorkflowResponseWrapper getWorkflow() {
		return workflow;
	}
	
	@JsonProperty("sequence_no")
	public int getSeq() {
		return seq;
	}
	
	@JsonProperty("sequence_type")
	public String getSequenceType() {
		return sequenceType;
	}
	
	@JsonProperty("workflow_step_description")
	public String getDescription() {
		return description;
	}
	
	@JsonProperty("position")
	public PositionResponseWrapper getPosition() {
		return position;
	}
	
	@JsonProperty("approval_type")
	public String getApprovalType() {
		return approvalType;
	}
	
	@JsonProperty("created_at")
	public Date getCreatedAt() {
		return createdAt;
	}
	
	@JsonProperty("created_by")
	public AuthorResponseWrapper getCreatedBy() {
		return createdBy;
	}
	
	public WorkflowStepResponseWrapper setCreatedBy(Link link) {
		AuthorResponseWrapper createdBy = new AuthorResponseWrapper(link);
		this.createdBy = createdBy;
		return this;
	}
	
	public WorkflowStepResponseWrapper setCreatedBy(User user, Link link) {
		AuthorResponseWrapper createdBy = new AuthorResponseWrapper(user, link);
		this.createdBy = createdBy;
		return this;
	}
	
	@JsonProperty("updated_at")
	public Date getUpdatedAt() {
		return updatedAt;
	}
	
	@JsonProperty("updated_by")
	public AuthorResponseWrapper getUpdatedBy() {
		return updatedBy;
	}
	
	public WorkflowStepResponseWrapper setUpdatedBy(Link link) {
		AuthorResponseWrapper updatedBy = new AuthorResponseWrapper(link);
		this.updatedBy = updatedBy;
		return this;
	}
	
	public WorkflowStepResponseWrapper setUpdatedBy(User user, Link link) {
		AuthorResponseWrapper updatedBy = new AuthorResponseWrapper(user, link);
		this.updatedBy = updatedBy;
		return this;
	}
	
	@JsonProperty("href")
	public Link getLink() {
		return link;
	}
}