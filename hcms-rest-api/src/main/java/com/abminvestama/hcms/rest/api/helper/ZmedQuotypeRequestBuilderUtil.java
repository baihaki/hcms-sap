package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyQuotype;
import com.abminvestama.hcms.core.model.entity.ZmedQuotype;
import com.abminvestama.hcms.core.model.entity.ZmedQuotype;
import com.abminvestama.hcms.core.service.api.business.query.ZmedQuotypeQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedQuotypeRequestWrapper;

@Component("ZmedQuotypeRequestBuilderUtil")
@Transactional(readOnly = true)
public class ZmedQuotypeRequestBuilderUtil {

	
	
	private T001QueryService t001QueryService;
	private ZmedQuotypeQueryService zmedQuotypeQueryService;
	
	@Autowired
	void setZmedQuotypeQueryService(ZmedQuotypeQueryService zmedQuotypeQueryService) {
		this.zmedQuotypeQueryService = zmedQuotypeQueryService;
	}
	
	@Autowired
	void setT001QueryService(T001QueryService t001QueryService) {
		this.t001QueryService = t001QueryService;
	}
	
	public RequestObjectComparatorContainer<ZmedQuotype, ZmedQuotypeRequestWrapper, String> compareAndReturnUpdatedData(ZmedQuotypeRequestWrapper requestPayload, ZmedQuotype zmedQuotypeDB) {
		
		RequestObjectComparatorContainer<ZmedQuotype, ZmedQuotypeRequestWrapper, String> objectContainer = null; 
		
	if (zmedQuotypeDB == null) {
		zmedQuotypeDB = new ZmedQuotype();
		
		try {
			Optional<ZmedQuotype> existingZmedQuotype = zmedQuotypeQueryService.findOneByCompositeKey(
					requestPayload.getBukrs(), requestPayload.getKodequo(),
					CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
					CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
			
			if (existingZmedQuotype.isPresent()) {
				throw new DataViolationException("Duplicate ZmedQuotype Data for [ssn=" + requestPayload.getKodequo());
			}
			zmedQuotypeDB.setId(
					new ZmedCompositeKeyQuotype(requestPayload.getBukrs(), requestPayload.getKodequo(),
							CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
							CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi())));
			
			
			zmedQuotypeDB.setDatab(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()));
			zmedQuotypeDB.setDatbi(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
			zmedQuotypeDB.setDescr(requestPayload.getDescr());
			zmedQuotypeDB.setKodequo(requestPayload.getKodequo());
			zmedQuotypeDB.setPersen(Double.parseDouble(requestPayload.getPersen()));			
			Optional<T001> bnka = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));				
			zmedQuotypeDB.setBukrs(bnka.isPresent() ? bnka.get() : null);
			//zmedQuotypeDB.setBukrs(requestPayload.getBukrs());
			
		} catch (Exception ignored) {
			System.err.println(ignored);
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedQuotypeDB, requestPayload);
		
	} else {
		
		if (StringUtils.isNotBlank(requestPayload.getDescr())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDescr(), zmedQuotypeDB.getDescr())) {
				zmedQuotypeDB.setDescr(requestPayload.getDescr());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getKodequo())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getKodequo(), zmedQuotypeDB.getKodequo())) {
				zmedQuotypeDB.setKodequo(requestPayload.getKodequo());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedQuotypeDB.getBukrs() != null ? zmedQuotypeDB.getBukrs().getBukrs() : StringUtils.EMPTY)) {
				try {
					Optional<T001> newT001 = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));
					if (newT001.isPresent()) {
						zmedQuotypeDB.setBukrs(newT001.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					//T535N not found... cancel the update process
				}
			}
		}
		
		
		
		/*if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedQuotypeDB.getBukrs())) {
				zmedQuotypeDB.setBukrs(requestPayload.getBukrs());
				requestPayload.setDataChanged(true);
			}
		}*/

			
		
		if (StringUtils.isNotBlank(requestPayload.getPersen())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPersen(), zmedQuotypeDB.getPersen().toString())) {
				zmedQuotypeDB.setPersen(Double.parseDouble(requestPayload.getPersen()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getDatab())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), (zmedQuotypeDB.getDatab()))) {
				zmedQuotypeDB.setDatab(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()));
				requestPayload.setDataChanged(true);
			}
		}
		
		
		if (StringUtils.isNotBlank(requestPayload.getDatbi())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()), (zmedQuotypeDB.getDatbi())) ){
				zmedQuotypeDB.setDatbi(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
				requestPayload.setDataChanged(true);
			}
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedQuotypeDB, requestPayload);	
				
	}
	
	JSONObject json = new JSONObject(requestPayload);
	objectContainer.setEventData(json.toString());
	
	return objectContainer;
}
}
