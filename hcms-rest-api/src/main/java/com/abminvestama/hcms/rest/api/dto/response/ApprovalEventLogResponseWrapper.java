package com.abminvestama.hcms.rest.api.dto.response;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.ApprovalEventLog;
import com.abminvestama.hcms.core.model.entity.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@JsonInclude(Include.NON_NULL)
public class ApprovalEventLogResponseWrapper extends ResourceSupport {

	private String uuid;
	private String entityId;
	private String entityObjectId;
	private long performedBySSN;
	private String performedByPositionId;
	private AuthorResponseWrapper createdBy;
	private String approvalAction;
	
	protected ApprovalEventLogResponseWrapper() {}
	
	public ApprovalEventLogResponseWrapper(ApprovalEventLog approvalEventLog) {
		this.uuid = approvalEventLog.getId();
		this.entityId = approvalEventLog.getHcmsEntity().getId();
		this.entityObjectId = approvalEventLog.getEntityObjectId();
		this.performedBySSN = approvalEventLog.getPerformedBySSN();
		this.performedByPositionId = approvalEventLog.getPerformedByPosition().getId();
		this.approvalAction = approvalEventLog.getApprovalAction().name();
	}
	
	@JsonProperty("id")
	public String getUuid() {
		return uuid;
	}
	
	@JsonProperty("entity_id")
	public String getEntityId() {
		return entityId;
	}
	
	@JsonProperty("entity_object_id")
	public String getEntityObjectId() {
		return entityObjectId;
	}
	
	@JsonProperty("performed_by_ssn")
	public long getPerformedBySSN() {
		return performedBySSN;
	}
	
	@JsonProperty("performed_by_position_id")
	public String getPerformedByPositionId() {
		return performedByPositionId;
	}
	
	@JsonProperty("created_by")
	public AuthorResponseWrapper getCreatedBy() {
		return createdBy;
	}
	
	@JsonProperty("action_taken")
	public String getApprovalAction() {
		return approvalAction;
	}
	
	public ApprovalEventLogResponseWrapper setCreatedBy(Link userLink) {
		AuthorResponseWrapper createdBy = new AuthorResponseWrapper(userLink);
		this.createdBy = createdBy;
		return this;
	}
	
	public ApprovalEventLogResponseWrapper setCreatedBy(User user, Link userLink) {
		AuthorResponseWrapper createdBy = new AuthorResponseWrapper(user, userLink);
		this.createdBy = createdBy;
		return this;
	}	
}