package com.abminvestama.hcms.rest.api.dto.response;

import com.abminvestama.hcms.core.model.entity.CaEmpCSKT;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class CaEmpCSKTResponseWrapper {

	private Long empId;
	private Long pernr;
	private String kokrs;
	private String kostl; // kostl default 
	private String kltxt;
	private Integer kostlLevel;
	private String kostlPool;
	private String poolingCode;

	private CaEmpCSKTResponseWrapper() {}
	
	public CaEmpCSKTResponseWrapper(CaEmpCSKT caEmpCSKT) {
		if (caEmpCSKT == null) {
			new CaEmpCSKTResponseWrapper();
		}
		else {
			this
			.setEmpId(caEmpCSKT.getEmpId())
			.setPernr(caEmpCSKT.getPernr() != null ? caEmpCSKT.getPernr() : 0L)
			.setKokrs(caEmpCSKT.getCskt() != null ? defaultString(caEmpCSKT.getCskt().getKokrs()) : "")
			.setKostl(caEmpCSKT.getCskt() != null ? defaultString(caEmpCSKT.getCskt().getKostl()) : "")
			.setKltxt(caEmpCSKT.getCskt() != null ? defaultString(caEmpCSKT.getCskt().getKltxt()) : "")
			.setKostlLevel(caEmpCSKT.getKostlLevel())
			.setKostlPool(defaultString(caEmpCSKT.getKostlPool()))
			.setPoolingCode(defaultString(caEmpCSKT.getPoolingCode()))
			;
		}
	}

	@JsonProperty("empid")
	public Long getEmpId() {
		return empId;
	}

	public CaEmpCSKTResponseWrapper setEmpId(Long empId) {
		this.empId = empId;
		return this;
	}

	@JsonProperty("pernr")
	public Long getPernr() {
		return pernr;
	}

	public CaEmpCSKTResponseWrapper setPernr(Long pernr) {
		this.pernr = pernr;
		return this;
	}

	@JsonProperty("kokrs")
	public String getKokrs() {
		return kokrs;
	}

	public CaEmpCSKTResponseWrapper setKokrs(String kokrs) {
		this.kokrs = kokrs;
		return this;
	}

	@JsonProperty("kostl")
	public String getKostl() {
		return kostl;
	}

	public CaEmpCSKTResponseWrapper setKostl(String kostl) {
		this.kostl = kostl;
		return this;
	}

	@JsonProperty("kltxt")
	public String getKltxt() {
		return kltxt;
	}

	public CaEmpCSKTResponseWrapper setKltxt(String kltxt) {
		this.kltxt = kltxt;
		return this;
	}

	@JsonProperty("kostl_level")
	public Integer getKostlLevel() {
		return kostlLevel;
	}

	public CaEmpCSKTResponseWrapper setKostlLevel(Integer kostlLevel) {
		this.kostlLevel = kostlLevel;
		return this;
	}

	@JsonProperty("kostl_pool")
	public String getKostlPool() {
		return kostlPool;
	}

	public CaEmpCSKTResponseWrapper setKostlPool(String kostlPool) {
		this.kostlPool = kostlPool;
		return this;
	}

	@JsonProperty("kostl_code")
	public String getPoolingCode() {
		return poolingCode;
	}

	public CaEmpCSKTResponseWrapper setPoolingCode(String poolingCode) {
		this.poolingCode = poolingCode;
		return this;
	}

	private String defaultString(String str) {
		return defaultString(str, "");
	}

	private String defaultString(String str, String defaultValue) {
		return str != null ? str : defaultValue;
	}
}
