package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add attach1Type, attach1Content, attach1Filename field</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add fields: text1, text2, text3</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public class IT0019RequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = 6699362883990615202L;
	
	private String itxex;
	private String tmart;
	private String termn;
	private String mndat;
	private String bvmrk;
	private int tmjhr;
	private int tmmon;
	private int tmtag;
	private int mnjhr;
	private int mnmon;
	private int mntag;

	private String text1;
	private String text2;
	private String text3;

	private String attach1Type;
	private String attach1Content;
	private String attach1Filename;
	
	@JsonProperty("text_exists")
	public String getItxex() {
		return itxex;
	}
	
	@JsonProperty("task_type")
	public String getTmart() {
		return tmart;
	}
	
	@JsonProperty("task_date")
	public String getTermn() {
		return termn;
	}
	
	@JsonProperty("reminder_date")
	public String getMndat() {
		return mndat;
	}
	
	@JsonProperty("processing_indicator")
	public String getBvmrk() {
		return bvmrk;
	}
	
	@JsonProperty("year_of_date")
	public int getTmjhr() {
		return tmjhr;
	}
	
	@JsonProperty("month_of_date")
	public int getTmmon() {
		return tmmon;
	}
	
	@JsonProperty("day_of_date")
	public int getTmtag() {
		return tmtag;
	}
	
	@JsonProperty("year_of_reminder")
	public int getMnjhr() {
		return mnjhr;
	}
	
	@JsonProperty("month_of_reminder")
	public int getMnmon() {
		return mnmon;
	}
	
	@JsonProperty("day_of_reminder")
	public int getMntag() {
		return mntag;
	}

	@JsonProperty("comment1")
	public String getText1() {
		return text1;
	}

	@JsonProperty("comment2")
	public String getText2() {
		return text2;
	}

	@JsonProperty("comment3")
	public String getText3() {
		return text3;
	}

	@JsonProperty("attachment1_type")
	public String getAttach1Type() {
		return attach1Type;
	}
	
	@JsonProperty("attachment1_base64")
	public String getAttach1Content() {
		return attach1Content;
	}
	
	@JsonProperty("attachment1_filename")
	public String getAttach1Filename() {
		return attach1Filename;
	}
}