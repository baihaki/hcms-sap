package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.IT0105;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.T591S;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.business.query.IT0105QueryService;
import com.abminvestama.hcms.core.service.api.business.query.T591SQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0105RequestWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData: Add EventData Json</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Modify compareAndReturnUpdatedData to handle Create</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("it0105RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT0105RequestBuilderUtil {
	
	private T591SQueryService t591sQueryService;
	private IT0105QueryService it0105QueryService;
	
	
	@Autowired
	void setT591SQueryService(T591SQueryService t591sQueryService) {
		this.t591sQueryService = t591sQueryService;
	}
	

	@Autowired
	void setIT0105QueryService(IT0105QueryService it0105QueryService) {
		this.it0105QueryService = it0105QueryService;
	}

	/**
	 * Compare IT0105 requested payload with an existing IT0105 in the database.
	 * Update the existing IT0105 data with the latest data comes from the requested payload. 
	 * 
	 * @param requestPayload requested data 
	 * @param it0105DB current existing IT0105 in the database.
	 * @return updated IT0105 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0105, IT0105RequestWrapper, String> compareAndReturnUpdatedData(IT0105RequestWrapper requestPayload, IT0105 it0105DB) {
		
		RequestObjectComparatorContainer<IT0105, IT0105RequestWrapper, String> objectContainer = null; 
		
		if (it0105DB == null) {
			it0105DB = new IT0105();
			
			try {
				Optional<IT0105> existingIT0105 = it0105QueryService.findOneByCompositeKey(
						requestPayload.getPernr(), requestPayload.getSubty(), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda()));
				
				if (existingIT0105.isPresent()) {
					throw new DataViolationException("Duplicate IT0105 Data for [ssn=" + requestPayload.getPernr() + ",end_date=" +
							requestPayload.getEndda() + ",begin_date=" + requestPayload.getBegda());
				}
				
				it0105DB.setId(
						new ITCompositeKeys(requestPayload.getPernr(), "0105", requestPayload.getSubty(), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda())));
				it0105DB.setSeqnr(1L);
				//it0105DB.setUname(requestPayload.get);

				T591SKey bnksaKey = new T591SKey("0105", requestPayload.getUsrty());
				Optional<T591S> t591s = t591sQueryService.findById(Optional.ofNullable(bnksaKey));
				it0105DB.setUsrty(t591s.isPresent() ? t591s.get() : null);
				
				//Optional<T591S> bnka = t591sQueryService.findById(Optional.ofNullable(requestPayload.getUsrty()));
				//it0105DB.setUsrty(bnka.isPresent() ? bnka.get() : null);
				it0105DB.setUsridLong(requestPayload.getUsridLong());
				it0105DB.setStatus(DocumentStatus.INITIAL_DRAFT);
				
				
				
			} catch (Exception ignored) {
				System.err.println(ignored);
			}

			objectContainer = new RequestObjectComparatorContainer<>(it0105DB, requestPayload);
			
			
			
		} else {
			
			if (StringUtils.isNotBlank(requestPayload.getUsrty())) {
				
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getUsrty(), it0105DB.getUsrty() != null ? it0105DB.getUsrty().getId().getSubty() : StringUtils.EMPTY)) {
					try {
						T591SKey key = new T591SKey(SAPInfoType.COMMUNICATION_TYPE.infoType(), requestPayload.getUsrty());
						Optional<T591S> newT591S = t591sQueryService.findById(Optional.ofNullable(key));
						if (newT591S.isPresent()) {
							it0105DB.setUsrty(newT591S.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T591S not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getUsridLong())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getUsridLong(), it0105DB.getUsridLong())) {
					it0105DB.setUsridLong(requestPayload.getUsridLong());
					requestPayload.setDataChanged(true);
				}
			}
			objectContainer = new RequestObjectComparatorContainer<>(it0105DB, requestPayload);
		}
		
		// Add EventData Json		
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = "";
		try {
			jsonString = objectMapper.writeValueAsString(requestPayload);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		JSONObject json = new JSONObject(jsonString);
		
		//if (it0105DB.getStatus() != null)
			//json.put("prev_document_status", it0105DB.getStatus().name());
		
		objectContainer.setEventData(json.toString());
		
		return objectContainer;
	}						
}