package com.abminvestama.hcms.rest.api.dto.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 /**
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public class CaStlHdrRequestWrapper extends ITRequestWrapper {
	
	private static final long serialVersionUID = -5520966773552326796L;
	
	private String reqNo;
	private String tcaNo;
	private String docNo;
	private String totalAmount;
	private String diffAmount;
	private String currency;
	private String casStatus;
	private String createdAt;
	private String createdBy;
	private String updatedAt;
	private String updatedBy;
	private String approved1By;
	private String approved1At;
	private String approved2By;
	private String approved2At;
	private String approved3By;
	private String approved3At;
	private String approved4By;
	private String approved4At;
	private String approvedPoolingDeptBy;
	private String approvedPoolingDeptAt;
	private String approvedAccountingBy;
	private String approvedAccountingAt;
	private String approvedTreasuryBy;
	private String approvedTreasuryAt;
	private String process;
	private String notes;
	private String bukrs;
	private String refundAmount;
	private String claimAmount;
	private String lastSync;
	private String sapMessage;
	private String reason;
	private String trxCode;
	private String advanceCode;
	
	private String attach1Content;
	private String attach1Filename;
	
	private List<CaStlDtlRequestWrapper> stlDetail;

	@JsonProperty("reqno")
	public String getReqNo() {
		return reqNo;
	}

	@JsonProperty("tcano")
	public String getTcaNo() {
		return tcaNo;
	}

	@JsonProperty("docno")
	public String getDocNo() {
		return docNo;
	}

	@JsonProperty("totalamount")
	public String getTotalAmount() {
		return totalAmount;
	}

	@JsonProperty("diffamount")
	public String getDiffAmount() {
		return diffAmount;
	}

	@JsonProperty("currency")
	public String getCurrency() {
		return currency;
	}

	@JsonProperty("casstatus")
	public String getCasStatus() {
		return casStatus;
	}

	@JsonProperty("createat")
	public String getCreatedAt() {
		return createdAt;
	}

	@JsonProperty("createdby")
	public String getCreatedBy() {
		return createdBy;
	}

	@JsonProperty("updatedat")
	public String getUpdatedAt() {
		return updatedAt;
	}

	@JsonProperty("updatedby")
	public String getUpdatedBy() {
		return updatedBy;
	}

	@JsonProperty("approved1by")
	public String getApproved1By() {
		return approved1By;
	}

	@JsonProperty("approved1at")
	public String getApproved1At() {
		return approved1At;
	}

	@JsonProperty("approved2by")
	public String getApproved2By() {
		return approved2By;
	}

	@JsonProperty("approved2at")
	public String getApproved2At() {
		return approved2At;
	}

	@JsonProperty("approved3by")
	public String getApproved3By() {
		return approved3By;
	}

	@JsonProperty("approved3at")
	public String getApproved3At() {
		return approved3At;
	}

	@JsonProperty("approved4by")
	public String getApproved4By() {
		return approved4By;
	}

	@JsonProperty("approved4at")
	public String getApproved4At() {
		return approved4At;
	}

	@JsonProperty("approvedpooldeptby")
	public String getApprovedPoolingDeptBy() {
		return approvedPoolingDeptBy;
	}

	@JsonProperty("approvedpooldeptat")
	public String getApprovedPoolingDeptAt() {
		return approvedPoolingDeptAt;
	}

	@JsonProperty("approvedaccountingby")
	public String getApprovedAccountingBy() {
		return approvedAccountingBy;
	}

	@JsonProperty("approvedaccountingat")
	public String getApprovedAccountingAt() {
		return approvedAccountingAt;
	}

	@JsonProperty("approvedtreasuryby")
	public String getApprovedTreasuryBy() {
		return approvedTreasuryBy;
	}

	@JsonProperty("approvedtreasuryat")
	public String getApprovedTreasuryAt() {
		return approvedTreasuryAt;
	}

	@JsonProperty("process")
	public String getProcess() {
		return process;
	}

	@JsonProperty("notes")
	public String getNotes() {
		return notes;
	}

	@JsonProperty("bukrs")
	public String getBukrs() {
		return bukrs;
	}

	@JsonProperty("refundamount")
	public String getRefundAmount() {
		return refundAmount;
	}

	@JsonProperty("claimamount")
	public String getClaimAmount() {
		return claimAmount;
	}

	@JsonProperty("lastsync")
	public String getLastSync() {
		return lastSync;
	}

	@JsonProperty("sapmessage")
	public String getSapMessage() {
		return sapMessage;
	}

	@JsonProperty("reason")
	public String getReason() {
		return reason;
	}

	@JsonProperty("trxcode")
	public String getTrxCode() {
		return trxCode;
	}

	@JsonProperty("advance_code")
	public String getAdvanceCode() {
		return advanceCode;
	}

	@JsonProperty("attachment1_base64")
	public String getAttach1Content() {
		return attach1Content;
	}

	@JsonProperty("attachment1_filename")
	public String getAttach1Filename() {
		return attach1Filename;
	}

	@JsonProperty("stldetail")
	public List<CaStlDtlRequestWrapper> getStlDetail() {
		return stlDetail;
	}

}
