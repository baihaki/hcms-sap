package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyEmpquotastf;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotaQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotasmQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotastfQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedEmpquotaRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.ZmedEmpquotasmRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.ZmedEmpquotastfRequestWrapper;

@Component("MedOpticalLensRequestBuilderUtil")
@Transactional(readOnly = true)
public class MedOpticalLensRequestBuilderUtil {

	private ZmedEmpquotaQueryService zmedEmpquotaQueryService;
	
	@Autowired
	void setZmedEmpquotaQueryService(ZmedEmpquotaQueryService zmedEmpquotaQueryService) {
		this.zmedEmpquotaQueryService = zmedEmpquotaQueryService;
	}
	
	private ZmedEmpquotasmQueryService zmedEmpquotasmQueryService;
	
	@Autowired
	void setZmedEmpquotasmQueryService(ZmedEmpquotasmQueryService zmedEmpquotasmQueryService) {
		this.zmedEmpquotasmQueryService = zmedEmpquotasmQueryService;
	}
	
	private ZmedEmpquotastfQueryService zmedEmpquotastfQueryService;
	
	@Autowired
	void setZmedEmpquotastfQueryService(ZmedEmpquotastfQueryService zmedEmpquotastfQueryService) {
		this.zmedEmpquotastfQueryService = zmedEmpquotastfQueryService;
	}
	
	
	
	
	public RequestObjectComparatorContainer<ZmedEmpquota, ZmedEmpquotaRequestWrapper, String> compareAndReturnUpdatedData(ZmedEmpquotaRequestWrapper requestPayload, ZmedEmpquota zmedEmpquotaDB) {
		
		RequestObjectComparatorContainer<ZmedEmpquota, ZmedEmpquotaRequestWrapper, String> objectContainer = null; 
		
		if (zmedEmpquotaDB == null) {
			zmedEmpquotaDB = new ZmedEmpquota();

			try {

				Optional<ZmedEmpquota> existingZmedEmpquota = zmedEmpquotaQueryService.findOneByCompositeKey(
						requestPayload.getBukrs(), requestPayload.getPersg(), requestPayload.getPersk(),
						requestPayload.getFatxt(), "QLENS",
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));

				if (existingZmedEmpquota.isPresent()) {
					throw new DataViolationException("Duplicate ZmedEmpquota Data for [ssn=" + requestPayload.getKodequo());
				}
				zmedEmpquotaDB.setId(
						new ZmedCompositeKeyEmpquota(requestPayload.getBukrs(), requestPayload.getPersg(), requestPayload.getPersk(),
								requestPayload.getFatxt(), "QLENS",
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi())));

				zmedEmpquotaDB.setQuotamt(Double.parseDouble(requestPayload.getQuotamt()));			

			} catch (Exception ignored) {
				System.err.println(ignored);
			}

			objectContainer = new RequestObjectComparatorContainer<>(zmedEmpquotaDB, requestPayload);

		} else {

			if (StringUtils.isNotBlank(requestPayload.getQuotamt())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getQuotamt(), zmedEmpquotaDB.getQuotamt().toString())) {
					zmedEmpquotaDB.setQuotamt(Double.parseDouble(requestPayload.getQuotamt()));
					requestPayload.setDataChanged(true);
				}
			}

			objectContainer = new RequestObjectComparatorContainer<>(zmedEmpquotaDB, requestPayload);	
		}

		JSONObject json = new JSONObject(requestPayload);
		objectContainer.setEventData(json.toString());

		return objectContainer;
	}
	
	
	
	
	
	
	
	
	
	public RequestObjectComparatorContainer<ZmedEmpquotasm, ZmedEmpquotasmRequestWrapper, String> compareAndReturnUpdatedData(ZmedEmpquotasmRequestWrapper requestPayload, ZmedEmpquotasm zmedEmpquotasmDB) {
		
		RequestObjectComparatorContainer<ZmedEmpquotasm, ZmedEmpquotasmRequestWrapper, String> objectContainer = null; 
		
		if (zmedEmpquotasmDB == null) {
			zmedEmpquotasmDB = new ZmedEmpquotasm();

			try {

				Optional<ZmedEmpquotasm> existingZmedEmpquotasm = zmedEmpquotasmQueryService.findOneByCompositeKey(
						requestPayload.getPernr(), requestPayload.getBukrs(), 
						requestPayload.getPersg(), requestPayload.getPersk(),
						requestPayload.getFatxt(), "QLENS",
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));

				if (existingZmedEmpquotasm.isPresent()) {
					throw new DataViolationException("Duplicate ZmedEmpquotasm Data for [ssn=" + requestPayload.getKodequo());
				}
				zmedEmpquotasmDB.setId(
						new ZmedCompositeKeyEmpquotasm(requestPayload.getPernr(), requestPayload.getBukrs(), 
								requestPayload.getPersg(), requestPayload.getPersk(),
								requestPayload.getFatxt(), "QLENS",
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi())));

				zmedEmpquotasmDB.setQuotamt(Double.parseDouble(requestPayload.getQuotamt()));			

			} catch (Exception ignored) {
				System.err.println(ignored);
			}

			objectContainer = new RequestObjectComparatorContainer<>(zmedEmpquotasmDB, requestPayload);

		} else {

			if (StringUtils.isNotBlank(requestPayload.getQuotamt())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getQuotamt(), zmedEmpquotasmDB.getQuotamt().toString())) {
					zmedEmpquotasmDB.setQuotamt(Double.parseDouble(requestPayload.getQuotamt()));
					requestPayload.setDataChanged(true);
				}
			}

			objectContainer = new RequestObjectComparatorContainer<>(zmedEmpquotasmDB, requestPayload);	
		}

		JSONObject json = new JSONObject(requestPayload);
		objectContainer.setEventData(json.toString());

		return objectContainer;
	}
	
	
	
	
	
	
	
	
	
	public RequestObjectComparatorContainer<ZmedEmpquotastf, ZmedEmpquotastfRequestWrapper, String> compareAndReturnUpdatedData(ZmedEmpquotastfRequestWrapper requestPayload, ZmedEmpquotastf zmedEmpquotastfDB) {
		
		RequestObjectComparatorContainer<ZmedEmpquotastf, ZmedEmpquotastfRequestWrapper, String> objectContainer = null; 
		
		if (zmedEmpquotastfDB == null) {
			zmedEmpquotastfDB = new ZmedEmpquotastf();

			try {

				Optional<ZmedEmpquotastf> existingZmedEmpquotastf = zmedEmpquotastfQueryService.findOneByCompositeKey(
						requestPayload.getStell(), requestPayload.getBukrs(), 
						requestPayload.getPersg(), requestPayload.getPersk(),
						"QLENS",
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));

				if (existingZmedEmpquotastf.isPresent()) {
					throw new DataViolationException("Duplicate ZmedEmpquotastf Data for [ssn=" + requestPayload.getKodequo());
				}
				zmedEmpquotastfDB.setId(
						new ZmedCompositeKeyEmpquotastf(requestPayload.getStell(), 
								requestPayload.getBukrs(), requestPayload.getPersg(), 
								requestPayload.getPersk(), "QLENS",
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi())));

				zmedEmpquotastfDB.setQuotamt(Double.parseDouble(requestPayload.getQuotamt()));			

			} catch (Exception ignored) {
				System.err.println(ignored);
			}

			objectContainer = new RequestObjectComparatorContainer<>(zmedEmpquotastfDB, requestPayload);

		} else {

			if (StringUtils.isNotBlank(requestPayload.getQuotamt())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getQuotamt(), zmedEmpquotastfDB.getQuotamt().toString())) {
					zmedEmpquotastfDB.setQuotamt(Double.parseDouble(requestPayload.getQuotamt()));
					requestPayload.setDataChanged(true);
				}
			}

			objectContainer = new RequestObjectComparatorContainer<>(zmedEmpquotastfDB, requestPayload);	
		}

		JSONObject json = new JSONObject(requestPayload);
		objectContainer.setEventData(json.toString());

		return objectContainer;
	}
}
