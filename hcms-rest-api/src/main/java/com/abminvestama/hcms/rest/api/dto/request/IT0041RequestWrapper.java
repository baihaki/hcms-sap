package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class IT0041RequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = 7434687444210266561L;
	
	private String dar01;
	private String dat01;
	
	private String dar02;
	private String dat02;
	
	private String dar03;
	private String dat03;
	
	private String dar04;
	private String dat04;
	
	private String dar05;
	private String dat05;
	
	private String dar06;
	private String dat06;
	
	private String dar07;
	private String dat07;
	
	private String dar08;
	private String dat08;
	
	private String dar09;
	private String dat09;
	
	private String dar10;
	private String dat10;
	
	private String dar11;
	private String dat11;
	
	private String dar12;
	private String dat12;
	
	@JsonProperty("date_type1")
	public String getDar01() {
		return dar01;
	}
	
	@JsonProperty("date_type1_date")
	public String getDat01() {
		return dat01;
	}
	
	@JsonProperty("date_type2")
	public String getDar02() {
		return dar02;
	}
	
	@JsonProperty("date_type2_date")
	public String getDat02() {
		return dat02;
	}
	
	@JsonProperty("date_type3")
	public String getDar03() {
		return dar03;
	}
	
	@JsonProperty("date_type3_date")
	public String getDat03() {
		return dat03;
	}
	
	@JsonProperty("date_type4")
	public String getDar04() {
		return dar04;
	}
	
	@JsonProperty("date_type4_date")
	public String getDat04() {
		return dat04;
	}
	
	@JsonProperty("date_type5")
	public String getDar05() {
		return dar05;
	}
	
	@JsonProperty("date_type5_date")
	public String getDat05() {
		return dat05;
	}
	
	@JsonProperty("date_type6")
	public String getDar06() {
		return dar06;
	}
	
	@JsonProperty("date_type6_date")
	public String getDat06() {
		return dat06;
	}
	
	@JsonProperty("date_type7")
	public String getDar07() {
		return dar07;
	}
	
	@JsonProperty("date_type7_date")
	public String getDat07() {
		return dat07;
	}
	
	@JsonProperty("date_type8")
	public String getDar08() {
		return dar08;
	}
	
	@JsonProperty("date_type8_date")
	public String getDat08() {
		return dat08;
	}
	@JsonProperty("date_type9")
	public String getDar09() {
		return dar09;
	}
	
	@JsonProperty("date_type9_date")
	public String getDat09() {
		return dat09;
	}
	@JsonProperty("date_type10")
	public String getDar10() {
		return dar10;
	}
	
	@JsonProperty("date_type10_date")
	public String getDat10() {
		return dat10;
	}
	@JsonProperty("date_type11")
	public String getDar11() {
		return dar11;
	}
	
	@JsonProperty("date_type11_date")
	public String getDat11() {
		return dat11;
	}
	@JsonProperty("date_type12")
	public String getDar12() {
		return dar12;
	}
	
	@JsonProperty("date_type12_date")
	public String getDat12() {
		return dat12;
	}	
}