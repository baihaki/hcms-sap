package com.abminvestama.hcms.rest.api.dto.response;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;
import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class MedTransactionResponseWrapper  extends ResourceSupport {

	private ArrayData<ZmedTrxitmResponseWrapper> Zmed;
	private ArrayData<ZmedTrxhdrResponseWrapper> Zmedhdr;


	private MedTransactionResponseWrapper() {}
	
	
	/**
	 * @param zmedTrxhdr
	 * @param zmedTrxitm
	 */
	public MedTransactionResponseWrapper(ZmedTrxhdr zmedTrxhdr, ZmedTrxitm zmedTrxitm[]) {
		if (zmedTrxitm == null || zmedTrxhdr == null) {
			new MedTransactionResponseWrapper();
		} else {
			
			
//----------------------------------------
			ArrayData<ZmedTrxitmResponseWrapper> bunchOfZmed = new ArrayData<>();
			APIResponseWrapper<ArrayData<ZmedTrxitmResponseWrapper>> response = new APIResponseWrapper<>();
			response.setData(bunchOfZmed);
			
			List<ZmedTrxitmResponseWrapper> records = new ArrayList<>();
			
			for(int i = 0; i < zmedTrxitm.length; i++){
				records.add(new ZmedTrxitmResponseWrapper(zmedTrxitm[i]));
			}
			bunchOfZmed.setItems(records.toArray(new ZmedTrxitmResponseWrapper[records.size()]));
			
//----------------------------------------			
			
			ArrayData<ZmedTrxhdrResponseWrapper> bunchOfZmedhdr = new ArrayData<>();
			APIResponseWrapper<ArrayData<ZmedTrxhdrResponseWrapper>> responsehdr = new APIResponseWrapper<>();
			responsehdr.setData(bunchOfZmedhdr);
			
			List<ZmedTrxhdrResponseWrapper> recordshdr = new ArrayList<>();
			
			//for(int i = 0; i < zmedTrxitm.length; i++){
				recordshdr.add(new ZmedTrxhdrResponseWrapper(zmedTrxhdr));
			//}
			bunchOfZmedhdr.setItems(recordshdr.toArray(new ZmedTrxhdrResponseWrapper[recordshdr.size()]));
			
			
//-----------------------------------------			
			
			this.setHeader(bunchOfZmedhdr)
				.setItems(bunchOfZmed);
		}
	}
	


	
	
	@JsonProperty("transaction_header")
	public ArrayData<ZmedTrxhdrResponseWrapper> getHeader() {
		return Zmedhdr;
	}
	
	private MedTransactionResponseWrapper setHeader(ArrayData<ZmedTrxhdrResponseWrapper> bunchOfZmedhdr) {
		this.Zmedhdr = bunchOfZmedhdr;
		return this;
	}
	


	
	
	@JsonProperty("transaction_items")
	public ArrayData<ZmedTrxitmResponseWrapper> getItems() {
		return Zmed;
	}
	
	private MedTransactionResponseWrapper setItems(ArrayData<ZmedTrxitmResponseWrapper> bunchOfZmed) {
		this.Zmed = bunchOfZmed;
		return this;
	}
	

}

