package com.abminvestama.hcms.rest.api.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.IT0009ResponseWrapper;

/**
 * 
 * @author yauritux@gmail.com
 * 
 *
 */
@RestController
@RequestMapping("/api/v2/sap_bank_details")
public class BankDetailController extends AbstractResource {

	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> postToSAP() throws Exception {
		try {
			APIResponseWrapper<?> response = null;
			
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			
			// Send SOAP Message to SOAP Server
			//String url = "http://10.32.162.66:8098/bankdetail?wsdl";
			//String url = "http://10.32.162.66:8098/bankdetail";
			String url = "http://localhost:8098/bankdetail";
			SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(), url);
			
			// Process the SOAP Response
			Source source = printSOAPResponse(soapResponse);
			//return new APIResponseWrapper<>(source.getSystemId());
			
			
			SOAPBody body = soapResponse.getSOAPPart().getEnvelope().getBody();
			NodeList nodes = body.getFirstChild().getFirstChild().getChildNodes();
			Map<String, Node> nodeMap = new HashMap<String, Node>();
			for (int i=0; i < nodes.getLength(); i ++) {
				nodeMap.put(nodes.item(i).getLocalName(), nodes.item(i));
			}
			
			IT0009 it0009 = new IT0009(new ITCompositeKeys(Long.parseLong(nodeMap.get("employeenumber").getTextContent()), "0009", "0", new Date(), new Date()));
			it0009.setUname(nodeMap.get("message").getTextContent());
			it0009.setBankn(nodeMap.get("statustext").getTextContent());
			it0009.setStatus(DocumentStatus.PUBLISHED);
			
			
			IT0009ResponseWrapper responseWrapper = new IT0009ResponseWrapper(it0009);
			
			response = new APIResponseWrapper<>(responseWrapper);
			return response;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}		
	}
	
	private SOAPMessage createSOAPRequest() throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		
		String serverURI = "http://bankdetail.hcms.com/";
		
		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration("ban", serverURI);
		
		/* 
		Constructed SOAP Request Message:
	<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ban="http://bankdetail.hcms.com/">
	  <soapenv:Header/>
	  <soapenv:Body>
	     <ban:BAPI_BANKDETAIL_CREATE>
	        <import>
	           <employeenumber>00001979</employeenumber>
	           <!--Optional:-->
	           <validitybegin>20170801</validitybegin>
	           <!--Optional:-->
	           <validityend>99991231</validityend>
	           <!--Optional:-->
	           <banktype>0</banktype>
	           <!--Optional:-->
	           <paymentmethod>T</paymentmethod>
	           <!--Optional:-->
	           <payee>GOGON</payee>
	           <!--Optional:-->
	           <payeestreet></payeestreet>
	           <!--Optional:-->
	           <payeepostalcodecity>15160</payeepostalcodecity>
	           <!--Optional:-->
	           <payeecity>Kutoarjo</payeecity>
	           <!--Optional:-->
	           <payeeregion></payeeregion>
	           <!--Optional:-->
	           <bankcountry>ID</bankcountry>
	           <!--Optional:-->
	           <bankkey>MAN0012701</bankkey>
	           <!--Optional:-->
	           <accountno>1212121211</accountno>
	           <!--Optional:-->
	           <bankreference></bankreference>
	           <!--Optional:-->
	           <standardvalue></standardvalue>
	           <!--Optional:-->
	           <currency>IDR</currency>
	           <!--Optional:-->
	           <standardpercent></standardpercent>
	           <!--Optional:-->
	           <timeunit></timeunit>
	           <!--Optional:-->
	           <checkdigit></checkdigit>
	           <!--Optional:-->
	           <controlkey></controlkey>
	           <!--Optional:-->
	           <purpose>Untuk beli ikan lele yg nge-hits</purpose>
	           <!--Optional:-->
	           <debit></debit>
	           <!--Optional:-->
	           <nocommit></nocommit>
	           <!--Optional:-->
	           <standardvaluecurr></standardvaluecurr>
	           <!--Optional:-->
	           <iban></iban>
	        </import>
	     </ban:BAPI_BANKDETAIL_CREATE>
	  </soapenv:Body>
	</soapenv:Envelope>		
		*/
		/*
		// SOAP Body for other bank
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("BAPI_BANKDETAIL_CREATE", "ban");
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("import");
		SOAPElement soapBodyElem11 = soapBodyElem1.addChildElement("employeenumber");
		soapBodyElem11.addTextNode("21555");
		SOAPElement soapBodyElem12 = soapBodyElem1.addChildElement("validitybegin");
		soapBodyElem12.addTextNode("20160201");
		SOAPElement soapBodyElem14 = soapBodyElem1.addChildElement("validityend");
		soapBodyElem14.addTextNode("99991231");
		SOAPElement soapBodyElem13 = soapBodyElem1.addChildElement("banktype");
		soapBodyElem13.addTextNode("1");
		SOAPElement soapBodyElem15 = soapBodyElem1.addChildElement("paymentmethod");
		soapBodyElem15.addTextNode("t");
		SOAPElement soapBodyElem16 = soapBodyElem1.addChildElement("payee");
		soapBodyElem16.addTextNode("BAIHAKI");
		SOAPElement soapBodyElem17 = soapBodyElem1.addChildElement("payeepostalcodecity");
		soapBodyElem17.addTextNode("12000");
		SOAPElement soapBodyElem18 = soapBodyElem1.addChildElement("payeecity");
		soapBodyElem18.addTextNode("Jakarta");
		SOAPElement soapBodyElem19 = soapBodyElem1.addChildElement("bankcountry");
		soapBodyElem19.addTextNode("ID");
		SOAPElement soapBodyElem190 = soapBodyElem1.addChildElement("bankkey");
		soapBodyElem190.addTextNode("BNI0000014");
		SOAPElement soapBodyElem191 = soapBodyElem1.addChildElement("accountno");
		soapBodyElem191.addTextNode("123456789");
		SOAPElement soapBodyElem192 = soapBodyElem1.addChildElement("standardvalue");
		soapBodyElem192.addTextNode("2500");
		SOAPElement soapBodyElem193 = soapBodyElem1.addChildElement("currency");
		soapBodyElem193.addTextNode("IDR");
		SOAPElement soapBodyElem194 = soapBodyElem1.addChildElement("purpose");
		soapBodyElem194.addTextNode("Cicilan KPR");
		*/
		

		// SOAP Body for main bank
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement("BAPI_BANKDETAIL_CREATE", "ban");
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("import");
		SOAPElement soapBodyElem11 = soapBodyElem1.addChildElement("employeenumber");
		soapBodyElem11.addTextNode("21555");
		SOAPElement soapBodyElem12 = soapBodyElem1.addChildElement("validitybegin");
		soapBodyElem12.addTextNode("20160201");
		SOAPElement soapBodyElem14 = soapBodyElem1.addChildElement("validityend");
		soapBodyElem14.addTextNode("99991231");
		SOAPElement soapBodyElem13 = soapBodyElem1.addChildElement("banktype");
		soapBodyElem13.addTextNode("0");
		SOAPElement soapBodyElem15 = soapBodyElem1.addChildElement("paymentmethod");
		soapBodyElem15.addTextNode("t");
		SOAPElement soapBodyElem16 = soapBodyElem1.addChildElement("payee");
		soapBodyElem16.addTextNode("BAIHAKI");
		SOAPElement soapBodyElem17 = soapBodyElem1.addChildElement("payeepostalcodecity");
		soapBodyElem17.addTextNode("12000");
		SOAPElement soapBodyElem18 = soapBodyElem1.addChildElement("payeecity");
		soapBodyElem18.addTextNode("Jakarta");
		SOAPElement soapBodyElem19 = soapBodyElem1.addChildElement("bankcountry");
		soapBodyElem19.addTextNode("ID");
		SOAPElement soapBodyElem190 = soapBodyElem1.addChildElement("bankkey");
		soapBodyElem190.addTextNode("BNI0000015");
		SOAPElement soapBodyElem191 = soapBodyElem1.addChildElement("accountno");
		soapBodyElem191.addTextNode("1234567890");
		SOAPElement soapBodyElem192 = soapBodyElem1.addChildElement("standardvalue");
		soapBodyElem192.addTextNode("0");
		SOAPElement soapBodyElem193 = soapBodyElem1.addChildElement("currency");
		soapBodyElem193.addTextNode("IDR");
		SOAPElement soapBodyElem194 = soapBodyElem1.addChildElement("purpose");
		soapBodyElem194.addTextNode("Gaji Utama");
		
		MimeHeaders headers = soapMessage.getMimeHeaders();
		//headers.addHeader("SOAPAction", serverURI + "bankdetail");
		//headers.addHeader("SOAPAction", serverURI);
		headers.addHeader("SOAPAction", "");
		
		
		soapMessage.saveChanges();
		
		// Print the request message
		System.out.println("Request SOAP Messages = ");
		soapMessage.writeTo(System.out);
		System.out.println();
		
		return soapMessage;
	}
	
	private Source printSOAPResponse(SOAPMessage soapResponse) throws Exception {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Source sourceContent = soapResponse.getSOAPPart().getContent();
		System.out.print("\nResponse SOAP Message = ");
		StreamResult result = new StreamResult(System.out);
		transformer.transform(sourceContent, result);
		return sourceContent;
	}
}