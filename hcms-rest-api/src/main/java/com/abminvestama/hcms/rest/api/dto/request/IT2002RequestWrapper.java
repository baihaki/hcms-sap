package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class IT2002RequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = 346240575088638885L;
	
	private String awart;
	private double abwtg;
	private double stdaz;
	private double kaltg;
	
	@JsonProperty("attendance_type")
	public String getAwart() {
		return awart;
	}
	
	@JsonProperty("attendance_days")
	public double getAbwtg() {
		return abwtg;
	}
	
	@JsonProperty("attendance_hours")
	public double getStdaz() {
		return stdaz;
	}
	
	@JsonProperty("calendar_days")
	public double getKaltg() {
		return kaltg;
	}	
}