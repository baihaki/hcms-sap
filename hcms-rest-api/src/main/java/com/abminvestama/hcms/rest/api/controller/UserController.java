package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.SecurityConfig;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.constant.UserRole;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.IT0002;
import com.abminvestama.hcms.core.model.entity.IT0014;
import com.abminvestama.hcms.core.model.entity.IT0019;
import com.abminvestama.hcms.core.model.entity.IT0040;
import com.abminvestama.hcms.core.model.entity.IT0184;
import com.abminvestama.hcms.core.model.entity.IT0241;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.T002T;
import com.abminvestama.hcms.core.model.entity.T502T;
import com.abminvestama.hcms.core.model.entity.T512T;
import com.abminvestama.hcms.core.model.entity.T538T;
import com.abminvestama.hcms.core.model.entity.T591S;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.IT0001CommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0002CommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0009CommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0014CommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0019CommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0021CommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0040CommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0041CommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0184CommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0241CommandService;
import com.abminvestama.hcms.core.service.api.business.command.UserCommandService;
import com.abminvestama.hcms.core.service.api.business.query.IT0001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.RoleQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.helper.IT0001Mapper;
import com.abminvestama.hcms.core.service.helper.IT0019Mapper;
import com.abminvestama.hcms.core.service.helper.MasterDataSoapObject;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.request.UserRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.AuthorResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.UserResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;

/**
 * 
 * User's RESTful API
 * 
 * @since 1.0.0
 * @version 1.0.9
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)<br>wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.9</td><td>Baihaki</td><td>Fix create User: set lgart on save IT0014 data</td></tr>
 *     <tr><td>1.0.8</td><td>Wijanarko</td><td>Add getUpdateUserAuthentication method</td></tr>
 *     <tr><td>1.0.7</td><td>Baihaki</td><td>Add findByEmployeeName method</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Create New User: create new objects on IT0021, IT0009, IT0041</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add getUserProfileByPernr method</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Create New User: create new object on IT0019, IT0040, IT0014</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Create New User: setPhotoLink, create new object on IT0241 + IT0184</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Create New User: Fetch from SAP (BAPIFunction.IT0001_FETCH) and create new object on IT0001 + IT0002</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add getEmploymentHistoryByUsername method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 *
 */
@RestController
@RequestMapping("/api/v1/user")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class UserController extends AbstractResource {

	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
	
	private SecurityConfig securityConfig; //Add by wijaanarko
	
	private UserCommandService userCommandService;
	private UserQueryService userQueryService;
	private RoleQueryService roleQueryService;
	private Validator userValidator;
	
	private IT0001QueryService it0001QueryService;

	private IT0001CommandService it0001CommandService;
	private IT0002CommandService it0002CommandService;
	private IT0241CommandService it0241CommandService;
	private IT0184CommandService it0184CommandService;
	private IT0019CommandService it0019CommandService;
	private IT0040CommandService it0040CommandService;
	private IT0014CommandService it0014CommandService;
	private IT0021CommandService it0021CommandService;
	private IT0009CommandService it0009CommandService;
	private IT0041CommandService it0041CommandService;
	private MuleConsumerService muleConsumerService;
	private CommonServiceFactory commonServiceFactory;

	
	@Autowired
	public void setSecurityConfig(SecurityConfig securityConfig) {
		this.securityConfig = securityConfig;
	}

	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	void setUserCommandService(UserCommandService userCommandService) {
		this.userCommandService = userCommandService;
	}
	
	@Autowired
	void setRoleQueryService(RoleQueryService roleQueryService) {
		this.roleQueryService = roleQueryService;
	}
	
	@Autowired
	@Qualifier("userValidator")
	public void setITValidator(Validator userValidator) {
		this.userValidator = userValidator;
	}	
	
	@Autowired
	public void setIT0001QueryService(IT0001QueryService it0001QueryService) {
		this.it0001QueryService = it0001QueryService;
	}
	
	@Autowired
	public void setIT0001CommandService(IT0001CommandService it0001CommandService) {
		this.it0001CommandService = it0001CommandService;
	}
	
	@Autowired
	public void setIT0002CommandService(IT0002CommandService it0002CommandService) {
		this.it0002CommandService = it0002CommandService;
	}
	
	@Autowired
	void setIT0241CommandService(IT0241CommandService it0241CommandService) {
		this.it0241CommandService = it0241CommandService;
	}
	
	@Autowired
	void setIT0184CommandService(IT0184CommandService it0184CommandService) {
		this.it0184CommandService = it0184CommandService;
	}
	
	@Autowired
	void setIT0019CommandService(IT0019CommandService it0019CommandService) {
		this.it0019CommandService = it0019CommandService;
	}
	
	@Autowired
	void setIT0040CommandService(IT0040CommandService it0040CommandService) {
		this.it0040CommandService = it0040CommandService;
	}
	
	@Autowired
	void setIT0014CommandService(IT0014CommandService it0014CommandService) {
		this.it0014CommandService = it0014CommandService;
	}

	@Autowired
	public void setIT0021CommandService(IT0021CommandService it0021CommandService) {
		this.it0021CommandService = it0021CommandService;
	}

	@Autowired
	public void setIT0009CommandService(IT0009CommandService it0009CommandService) {
		this.it0009CommandService = it0009CommandService;
	}
	
	@Autowired
	public void setIT0041CommandService(IT0041CommandService it0041CommandService) {
		this.it0041CommandService = it0041CommandService;
	}

	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	@Autowired
    Environment env;
	
	@RequestMapping(method = RequestMethod.GET, value = "/{username:.+}",
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({UserRole.AccessRole.USER_GROUP, UserRole.AccessRole.SUPERIOR_GROUP, UserRole.AccessRole.ADMIN_GROUP})
	public APIResponseWrapper<UserResponseWrapper> getUserProfileByUsername(@PathVariable String username) throws Exception {
		UserResponseWrapper userResponse = null;
		try {
			Function<User, UserResponseWrapper> userResponseWrapperFunction 
				= (User user) -> new UserResponseWrapper(user);
			
			userResponse = userResponseWrapperFunction.apply(userQueryService.findByUsername(
					Optional.ofNullable(username)).get());
		} catch (Exception e) {
			LOG.error(e.getMessage());
			throw e;
		}
		
		APIResponseWrapper<UserResponseWrapper> response = new APIResponseWrapper<>(userResponse);
		response.add(linkTo(methodOn(UserController.class).getUserProfileByUsername(username)).withSelfRel());
		
		return response;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/employment_history/{username:.+}",
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({UserRole.AccessRole.USER_GROUP, UserRole.AccessRole.SUPERIOR_GROUP, UserRole.AccessRole.ADMIN_GROUP})
	public APIResponseWrapper<ArrayData<UserResponseWrapper>> getEmploymentHistoryByUsername(@PathVariable String username) throws Exception {
		ArrayData<UserResponseWrapper> bunchOfUser = new ArrayData<>();
		APIResponseWrapper<ArrayData<UserResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfUser);
		try {
				
			Optional<User> existUser = userQueryService.findByUsername(Optional.ofNullable(username));
			if (existUser.isPresent()) {
				User viewUser = new User(null, null, null, null);
				BeanUtils.copyProperties(existUser.get(), viewUser);
				Function<User, UserResponseWrapper> userResponseWrapperFunction = (User user) -> new UserResponseWrapper(user);
				List<UserResponseWrapper> listOfUserResponse = new ArrayList<UserResponseWrapper>();
				List<IT0001> listOfIT0001 = it0001QueryService.findByPernr(existUser.get().getEmployee().getPernr()).
						stream().collect(Collectors.toList());
				listOfIT0001.forEach(it0001 -> {
					viewUser.setEmployee(it0001);
					UserResponseWrapper userResponse = userResponseWrapperFunction.apply(viewUser);
					listOfUserResponse.add(userResponse);
				});
				bunchOfUser.setItems(listOfUserResponse.toArray(new UserResponseWrapper[0]));
			} else {
				response.setMessage("No Data Found");
			}
			
		} catch (Exception e) {
			LOG.error(e.getMessage());
			throw e;
		}
		
		response.add(linkTo(methodOn(UserController.class).getEmploymentHistoryByUsername(username)).withSelfRel());
		
		return response;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/id/{id}",
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({UserRole.AccessRole.USER_GROUP, UserRole.AccessRole.SUPERIOR_GROUP, UserRole.AccessRole.ADMIN_GROUP})
	public APIResponseWrapper<UserResponseWrapper> getUserProfileById(@PathVariable String id) throws Exception {
		UserResponseWrapper userResponse = null;
		try {
			Function<User, UserResponseWrapper> userResponseWrapperFunction
				= (User user) -> new UserResponseWrapper(user);
			
			userResponse = userResponseWrapperFunction.apply(userQueryService.findById(Optional.of(id)).get());
		} catch (Exception e) {
			LOG.error(e.getMessage());
			throw e;
		}
		
		APIResponseWrapper<UserResponseWrapper> response = new APIResponseWrapper<>(userResponse);
		response.add(linkTo(methodOn(UserController.class).getUserProfileById(id)).withSelfRel());
		
		return response;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{ssn}",
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({UserRole.AccessRole.USER_GROUP, UserRole.AccessRole.SUPERIOR_GROUP, UserRole.AccessRole.ADMIN_GROUP})
	public APIResponseWrapper<UserResponseWrapper> getUserProfileByPernr(@PathVariable String ssn) throws Exception {
		UserResponseWrapper userResponse = null;
		APIResponseWrapper<UserResponseWrapper> response = new APIResponseWrapper<>(userResponse);
		try {
			Function<User, UserResponseWrapper> userResponseWrapperFunction
				= (User user) -> new UserResponseWrapper(user);
				
			Long pernr = Long.valueOf(ssn);
			
			Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
			
			Optional<User> userObject = userQueryService.findByPernr(pernr);
			if (userObject.isPresent()) {
				
				// if the company of the requested user is different to admin's (requestor) company (AND NOT SUPERADMIN) then set error message
				if (!userObject.get().getEmployee().getT500p().getBukrs().getBukrs().equals(
						currentUser.get().getEmployee().getT500p().getBukrs().getBukrs()) &&
						!currentUser.get().getId().contains("SUPERADMIN")) {
					response.setMessage("Can't fetch data of other company's employee");
				} else {
					userResponse = userResponseWrapperFunction.apply(userObject.get());
					response = new APIResponseWrapper<>(userResponse);
				}
			} else {
				response.setMessage("No Data Found");
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
			throw e;
		}
		response.add(linkTo(methodOn(UserController.class).getUserProfileByPernr(ssn)).withSelfRel());
		
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/update_user_authentication/{ssn}",
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({UserRole.AccessRole.USER_GROUP, UserRole.AccessRole.SUPERIOR_GROUP, UserRole.AccessRole.ADMIN_GROUP})
	public APIResponseWrapper<UserResponseWrapper> getUpdateUserAuthentication(
			@PathVariable String ssn,
			@RequestParam(value = "old_password", required = false) String oldPassword,
			@RequestParam(value = "new_password", required = false) String newPassword) throws Exception {
		
		UserResponseWrapper userResponse = null;
		APIResponseWrapper<UserResponseWrapper> response = new APIResponseWrapper<>(userResponse);
		try {
			Function<User, UserResponseWrapper> userResponseWrapperFunction	= (User user) -> new UserResponseWrapper(user);
			Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
			Optional<User> userObject = userQueryService.findByPernr(Long.valueOf(ssn));
			//validation SSN Self Employee
			if (userObject.isPresent()) {
				//validation, Can't change password of other self employee
				if (userObject.get().getEmployee().getPernr().equals(currentUser.get().getEmployee().getPernr())) {
					//validation old password
					UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
							currentUser.get().getUsername(), oldPassword);
					securityConfig.authenticationManagerBean().authenticate(authenticationToken);
					
					//update new password
					currentUser.get().setPassword(BCrypt.hashpw(newPassword, BCrypt.gensalt()));
					currentUser = userCommandService.save(currentUser.get(), currentUser.get());
					
					userResponse = userResponseWrapperFunction.apply(userObject.get());
					response = new APIResponseWrapper<>(userResponse);
				}
				else {
					response.setMessage("Can't change password of other self employee");
				}
			}
			else {
				response.setMessage("No Data Found");
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
			response.setMessage(e.getMessage());
			return response;
			//throw e;
		}
		response.add(linkTo(methodOn(UserController.class).getUpdateUserAuthentication(ssn, oldPassword, newPassword)).withSelfRel());
		
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/employee_name/{employeeName}", 
	produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<UserResponseWrapper>> findByEmployeeName(@PathVariable String employeeName) throws Exception {
		ArrayData<UserResponseWrapper> bunchOfUser = new ArrayData<>();
		APIResponseWrapper<ArrayData<UserResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfUser);
		
		try {
			List<User> listOfUser = new ArrayList<>();
			
			if (employeeName == null || employeeName.length() < 3) {
				response.setMessage("No Data Found");
			} else {
				Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
				Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
				
				String bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
				if (currentUser.get().getId().contains("SUPERADMIN")) {
					bukrs = "ALL";
				}
				
				String superiorPosition = null;
				// If NOT ADMIN (ROLE SUPERIOR ONLY) set superiorPosition parameter as additional filter
				if (!currentUser.get().getRoles().contains(adminRole.get())) {
					superiorPosition = currentUser.get().getEmployee().getPosition().getId();
				}
				
				Optional<Collection<User>> collectionOfUser = superiorPosition == null ?
						userQueryService.findByEmployeeName(bukrs, "%".concat(employeeName.toUpperCase()).concat("%")) :
						userQueryService.findByEmployeeName(bukrs, "%".concat(employeeName.toUpperCase()).concat("%"), superiorPosition);
				if (collectionOfUser.isPresent() && collectionOfUser.get().size() > 0) {
					listOfUser = collectionOfUser.get().stream().collect(Collectors.toList());
					List<UserResponseWrapper> records = new ArrayList<>();
					for (User user : listOfUser) {
						records.add(new UserResponseWrapper(user));
					}
					bunchOfUser.setItems(records.toArray(new UserResponseWrapper[records.size()]));
				} else {
					response.setMessage("No Data Found");
				}
			}
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(UserController.class).findByEmployeeName(employeeName)).withSelfRel());
		
		return response;
	}
	
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({UserRole.AccessRole.ADMIN_GROUP})
	public APIResponseWrapper<?> create(@RequestBody @Validated UserRequestWrapper userRequestWrapper,
			BindingResult result) throws Exception {
		if (result.hasErrors()) {
			return new APIResponseWrapper<>(
					new ArrayData<>(ExceptionResponseWrapper.getErrors(result.getFieldErrors(), HttpMethod.POST)),
					"Validation Error!");
		}
		
		APIResponseWrapper<?> response = new APIResponseWrapper<UserResponseWrapper>();
		
		Optional<User> currentUser = userQueryService.findByUsername(currentUser());
		
		User newUser = new User(userRequestWrapper.getUsername(), userRequestWrapper.getPassword(), null, null);
		newUser.setEmail(userRequestWrapper.getEmail());
		newUser.setPhotoLink(userRequestWrapper.getPhotoLink());
		//Optional<Role> role = roleQueryService.findByName(Optional.ofNullable(userRequestWrapper.getRoleName()));
		for (String role : userRequestWrapper.getRoleNames()) {
			Optional<Role> roleName = roleQueryService.findByName(Optional.ofNullable(role));
			newUser.addRole(roleName.get());
		}
		//newUser.addRole(role.get());
		newUser.setPassword(BCrypt.hashpw(userRequestWrapper.getPassword(), BCrypt.gensalt()));
		newUser.setSystem(false);
		ArrayList<IT0001> it0001Records = (ArrayList<IT0001>) it0001QueryService.findByPernr(userRequestWrapper.getSsn());
		
		if (it0001Records.isEmpty()) {
			MasterDataSoapObject masterDataSoapObject = new MasterDataSoapObject(null, null, String.valueOf(userRequestWrapper.getSsn()));
			List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.IT0001_FETCH.service(),
					BAPIFunction.IT0001_FETCH_URI.service(), masterDataSoapObject);
			
			for (int i = 0; i < list.size(); i++) {
				if (i > 0 && list.get(i).get("sn") != null && Long.valueOf(list.get(i).get("sn")).longValue() == userRequestWrapper.getSsn()) {
					IT0001 it0001 = new IT0001Mapper(list.get(i), commonServiceFactory, currentUser.get()).getIT0001();
				    try {
						Optional<IT0001> savedEntity = it0001CommandService.save(it0001, currentUser.get());
						if (savedEntity.isPresent()) {
							it0001Records.add(savedEntity.get());
							ITCompositeKeysNoSubtype id = savedEntity.get().getId();
							// save new IT0002 
							IT0002 it0002 = new IT0002(id);
							it0002.setSeqnr(0L);
							it0002.setNachn(savedEntity.get().getEname());
							it0002.setCname(savedEntity.get().getEname());
							// set DOB
							if (list.get(i).get("dob") != null && !list.get(i).get("dob").equals("0000-00-00")) {
								it0002.setGbdat(CommonDateFunction.convertDateRequestParameterIntoDate(list.get(i).get("dob")));
								// change begda into the valid DOB
								ITCompositeKeysNoSubtype newId = new ITCompositeKeysNoSubtype(id.getPernr(), id.getEndda(), it0002.getGbdat());
								it0002.setId(newId);
							}
							// set gender
							if (list.get(i).get("sexname") != null && !list.get(i).get("sexname").equals(StringUtils.EMPTY)) {
								it0002.setGesch(list.get(i).get("sexname").equalsIgnoreCase("female") ? "2" : "1");
							}
							// set communication language
							if (list.get(i).get("sprsl") != null && !list.get(i).get("sprsl").equals(StringUtils.EMPTY)) {
								String sprslId = list.get(i).get("sprsl").toUpperCase();
								Optional<T002T> t002tObject = commonServiceFactory.getT002TQueryService().findById(Optional.of(sprslId));
								it0002.setSprsl(t002tObject.orElse(null));
							}
							// set marital status
							if (list.get(i).get("maritalcd") != null && !list.get(i).get("maritalcd").equals(StringUtils.EMPTY)) {
								String famstId = list.get(i).get("maritalcd").toUpperCase();
								Optional<T502T> t502tObject = commonServiceFactory.getT502TQueryService().findById(Optional.of(famstId));
								it0002.setFamst(t502tObject.orElse(null));
							}
							it0002.setStatus(DocumentStatus.PUBLISHED);
							Optional<IT0002> savedit0002 = it0002CommandService.save(it0002, currentUser.get());
							if (!savedit0002.isPresent()) {
								response.setMessage("Failed to save IT0002 object. ");
							}
							// set tax id (IT0241)
							if (list.get(i).get("taxid") != null && !list.get(i).get("taxid").equals(StringUtils.EMPTY)) {
								IT0241 it0241 = new IT0241(id);
								it0241.setSeqnr(0L);
								it0241.setTaxId(list.get(i).get("taxid"));
								it0241.setMarrd(list.get(i).get("marrd"));
								it0241.setSpben(list.get(i).get("spben"));
								it0241.setRefno(list.get(i).get("refno"));
								it0241.setDepnd(list.get(i).get("depnd"));
								it0241.setRdate(list.get(i).get("rdate") != null && !list.get(i).get("rdate").equals("0000-00-00") ?
										CommonDateFunction.convertDateRequestParameterIntoDate(list.get(i).get("rdate")) : null);
								it0241.setStatus(DocumentStatus.PUBLISHED);
								Optional<IT0241> savedit0241 = it0241CommandService.save(it0241, currentUser.get());
								if (!savedit0241.isPresent()) {
									response.setMessage(StringUtils.defaultString(response.getMessage()).concat("Failed to save IT0241 object. "));
								}
							}
							// set blood type (IT0184)
							String bloodType = StringUtils.defaultString(list.get(i).get("bloodtype"), "-");
							ITCompositeKeys it0184key = new ITCompositeKeys(id.getPernr(), SAPInfoType.RESUME_TEXTS.infoType(), "90", id.getEndda(), id.getBegda()); 
							IT0184 it0184 = new IT0184(it0184key);
							it0184.setSeqnr(0L);
							it0184.setItxex(list.get(i).get("itxex"));
							it0184.setText1(bloodType);
							it0184.setStatus(DocumentStatus.PUBLISHED);
							Optional<IT0184> savedit0184 = it0184CommandService.save(it0184, currentUser.get());
							if (!savedit0184.isPresent()) {
								response.setMessage(StringUtils.defaultString(response.getMessage()).concat("Failed to save IT0184 object. "));
							}
							// set Task Monitoring (IT0019)
							if (list.get(i).get("tmart") != null && !list.get(i).get("tmart").equals(StringUtils.EMPTY)) {
								IT0019 it0019 = new IT0019Mapper(list.get(i), commonServiceFactory, currentUser.get()).getIT0019();
								it0019.setSeqnr(0L);
								Optional<IT0019> savedit0019 = it0019CommandService.save(it0019, currentUser.get());
								if (!savedit0019.isPresent()) {
									response.setMessage(StringUtils.defaultString(response.getMessage()).concat("Failed to save IT0019 object. "));
								}
							}
							// set Object on Loan (IT0040)
							if (list.get(i).get("leihg") != null && !list.get(i).get("leihg").equals(StringUtils.EMPTY)) {
								String subty = list.get(i).get("leihg");
								ITCompositeKeys it0040key = new ITCompositeKeys(id.getPernr(), SAPInfoType.EMPLOYEE_INVENTORY.infoType(),
										subty, id.getEndda(), id.getBegda());
								IT0040 it0040 = new IT0040(it0040key);
								it0040.setSeqnr(0L);
								it0040.setLeihg(new T591S(new T591SKey(SAPInfoType.EMPLOYEE_INVENTORY.infoType(), subty)));
								it0040.setAnzkl(list.get(i).get("anzkl") != null && !list.get(i).get("anzkl").equals(StringUtils.EMPTY) ?
										Double.valueOf(list.get(i).get("anzkl")) : 0);
								Optional<T538T> zeinh = commonServiceFactory.getT538TQueryService().findById(Optional.ofNullable(list.get(i).get("zeinh")));
								if (zeinh.isPresent()) {
									it0040.setZeinh(zeinh.get());
								}
								it0040.setLobnr(list.get(i).get("lobnr"));
								it0040.setStatus(DocumentStatus.PUBLISHED);
								Optional<IT0040> savedit0040 = it0040CommandService.save(it0040, currentUser.get());
								if (!savedit0040.isPresent()) {
									response.setMessage(StringUtils.defaultString(response.getMessage()).concat("Failed to save IT0040 object. "));
								}
							}
							// set Object on Recurring Payment (IT0014)
							if (list.get(i).get("lgart") != null && !list.get(i).get("lgart").equals(StringUtils.EMPTY)) {
								String subty = list.get(i).get("lgart");
								ITCompositeKeys it0014key = new ITCompositeKeys(id.getPernr(), SAPInfoType.RECURRING_PAYMENTS.infoType(),
										subty, id.getEndda(), id.getBegda());
								IT0014 it0014 = new IT0014(it0014key);
								it0014.setSeqnr(0L);
								it0014.setOpken(list.get(i).get("opken"));
								it0014.setBetrg(list.get(i).get("betrg") != null ? Double.valueOf(list.get(i).get("betrg")) : 0.00);
								it0014.setWaers(list.get(i).get("waers"));
								Optional<T512T> lgart = commonServiceFactory.getT512TQueryService().findById(Optional.ofNullable(list.get(i).get("lgart")));
								if (lgart.isPresent()) {
									it0014.setLgart(lgart.get());
								}
								Optional<IT0014> savedit0014 = it0014CommandService.save(it0014, currentUser.get());
								if (!savedit0014.isPresent()) {
									response.setMessage(StringUtils.defaultString(response.getMessage()).concat("Failed to save IT0014 object. "));
								}
							}
							
						}
						
						// save list of IT0021 (fetch from Mulesoft)
						String resultMessage = it0021CommandService.saveListFromMulesoft(
								muleConsumerService, commonServiceFactory, userRequestWrapper.getSsn(), currentUser.get());
						response.setMessage(StringUtils.defaultString(response.getMessage()).concat(resultMessage));
						/*
						List<Map<String, String>> listIT = muleConsumerService.getFromSAP(BAPIFunction.INFOTYPE_FETCH.service(),
								BAPIFunction.INFOTYPE_FETCH_FAMILY_URI.service(), masterDataSoapObject);
						if (!listIT.isEmpty()) {
							Set<String> setIT = new HashSet<String>();
							for (int it21 = 0; it21 < listIT.size(); it21++) {
								if (listIT.get(it21).get("field1") != null && Long.valueOf(listIT.get(it21).get("field1")).longValue() == userRequestWrapper.getSsn()) {
									IT0021 it0021 = new IT0021Mapper(listIT.get(it21), commonServiceFactory).getIT0021();
									String key = it0021.getId().toStringId();
									if (setIT.contains(key)) {
										// duplicate key, increment begda based on seqnr (SAP objectid)
										Calendar cal = Calendar.getInstance();
										cal.setTime(it0021.getId().getBegda());
										int objectId = it0021.getSeqnr().intValue();
										cal.add(Calendar.DATE, objectId);
										ITCompositeKeys newId = new ITCompositeKeys(it0021.getId().getPernr(), it0021.getId().getInfty(),
												it0021.getId().getSubty(), it0021.getId().getEndda(), cal.getTime());
										it0021.setId(newId);
									} else {
										setIT.add(key);
									}
									Optional<IT0021> savedITEntity = it0021CommandService.save(it0021, currentUser.get());
									if (!savedITEntity.isPresent()) {
										response.setMessage(StringUtils.defaultString(response.getMessage()).concat("Failed to save IT0021 object. "));
									}
								}
							}
						}
						*/
						
						// save list of IT0009 (fetch from Mulesoft)
						resultMessage = it0009CommandService.saveListFromMulesoft(
								muleConsumerService, commonServiceFactory, userRequestWrapper.getSsn(), currentUser.get());
						response.setMessage(StringUtils.defaultString(response.getMessage()).concat(resultMessage));
						/*
						List<Map<String, String>> listIT = muleConsumerService.getFromSAP(BAPIFunction.INFOTYPE_FETCH.service(),
								BAPIFunction.INFOTYPE_FETCH_BANK_URI.service(), masterDataSoapObject);
						if (!listIT.isEmpty()) {
							Set<String> setIT = new HashSet<String>();
							for (int it = 0; it < listIT.size(); it++) {
								if (listIT.get(it).get("field1") != null && Long.valueOf(listIT.get(it).get("field1")).longValue() == userRequestWrapper.getSsn()) {
									IT0009 it0009 = new IT0009Mapper(listIT.get(it), commonServiceFactory).getIT0009();
									String key = it0009.getId().toStringId();
									if (setIT.contains(key)) {
										// duplicate key, increment begda based on seqnr (SAP objectid)
										Calendar cal = Calendar.getInstance();
										cal.setTime(it0009.getId().getBegda());
										int objectId = it0009.getSeqnr().intValue();
										cal.add(Calendar.DATE, objectId);
										ITCompositeKeys newId = new ITCompositeKeys(it0009.getId().getPernr(), it0009.getId().getInfty(),
												it0009.getId().getSubty(), it0009.getId().getEndda(), cal.getTime());
										it0009.setId(newId);
									} else {
										setIT.add(key);
									}
									Optional<IT0009> savedITEntity = it0009CommandService.save(it0009, currentUser.get());
									if (!savedITEntity.isPresent()) {
										response.setMessage(StringUtils.defaultString(response.getMessage()).concat("Failed to save IT0009 object. "));
									}
								}
							}
						}
						*/
						
						// save list of IT0041 (fetch from Mulesoft)
						resultMessage = it0041CommandService.saveListFromMulesoft(
								muleConsumerService, commonServiceFactory, userRequestWrapper.getSsn(), currentUser.get());
						response.setMessage(StringUtils.defaultString(response.getMessage()).concat(resultMessage));
						
						break;				    	
					} catch (Exception e) {
						response.setMessage(e.getMessage());
						e.printStackTrace();
						return response;
					}
				}
			}
			
			it0001Records = (ArrayList<IT0001>) it0001QueryService.findByPernr(userRequestWrapper.getSsn());
		}
		
		IT0001 it0001 = it0001Records.get(0);
		newUser.setEmployee(it0001);
		newUser.setCreatedById(currentUser.get().getId());
		newUser.setUpdatedById(currentUser.get().getId());
				
		try {
			Optional<User> savedUser = userCommandService.save(newUser, currentUser.get());
			UserResponseWrapper userResponse = new UserResponseWrapper(savedUser.get());
			userResponse.setCreatedBy(new AuthorResponseWrapper(currentUser.get()));
			userResponse.setUpdatedBy(new AuthorResponseWrapper(currentUser.get()));
			userResponse.add(linkTo(methodOn(UserController.class)
					.getUserProfileByUsername(savedUser.get().getUsername())).withRel(UserResponseWrapper.RESOURCE));
			response = new APIResponseWrapper<UserResponseWrapper>(userResponse);
			String msg = StringUtils.defaultString(response.getMessage());
			response.setMessage(msg + "User with username '" + userRequestWrapper.getUsername() + "' has been successfully created!");
			response.add(linkTo(methodOn(UserController.class).create(userRequestWrapper, result)).withSelfRel());		
			
			boolean sendEmail = true;
			// check email domains to be excluded (no sending emails)
			if (env.getProperty("mail.notification.exclude.domains") != null) {
				String filter = env.getProperty("mail.notification.exclude.domains");
				String[] domains = filter.split(",");
				for (int i = 0; i < domains.length; i++) {
					if (savedUser.get().getEmail().contains(domains[i].trim())) {
						sendEmail = false;
						break;
					}
				}
			}
			
			if (sendEmail) {
			String body = "Dear <b>".concat(savedUser.get().getEmployee().getSname()).concat("</b><br><br>")
					.concat("Your ABM HCMS account has been successfully created.<br><br>")
					.concat("Username : ").concat(savedUser.get().getUsername())
					.concat("<br>Password : ").concat(userRequestWrapper.getPassword())
					.concat("<br><br>Please login to ABM HCMS Application by using the provided information.")
					.concat("<br>Otherwise, you may login by using your current Active Directory / Windows logon.");
			commonServiceFactory.getMailService().sendMail(savedUser.get().getEmail(), "Welcome to ABM HCMS Application", body);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return response;
	}
	
	@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({UserRole.AccessRole.USER_GROUP, UserRole.AccessRole.SUPERIOR_GROUP, UserRole.AccessRole.ADMIN_GROUP})
	public APIResponseWrapper<?> edit(@RequestBody @Validated UserRequestWrapper userRequestWrapper,
			BindingResult result) throws Exception {
		if (result.hasErrors()) {
			return new APIResponseWrapper<>(
					new ArrayData<>(ExceptionResponseWrapper.getErrors(result.getFieldErrors(), HttpMethod.PUT)),
					"Validation error!");
		}
		
		APIResponseWrapper<?> response = null;
		
		try {
			boolean isDataChanged = false;
			
			Optional<User> currentUser = userQueryService.findByUsername(currentUser());
			if (StringUtils.isNotBlank(userRequestWrapper.getEmail()) 
					&& !userRequestWrapper.getEmail().equalsIgnoreCase(currentUser.get().getEmail())) {
				currentUser.get().setEmail(userRequestWrapper.getEmail());
				isDataChanged = true;
			}
			
			if (StringUtils.isNotBlank(userRequestWrapper.getPassword())) {
				currentUser.get().setPassword(BCrypt.hashpw(userRequestWrapper.getPassword(), BCrypt.gensalt()));
				isDataChanged = true;
			}
			
			if (isDataChanged) {
				currentUser = userCommandService.save(currentUser.get(), currentUser.get());
			}
			
			UserResponseWrapper userResponse = new UserResponseWrapper(currentUser.get());
			
			userResponse.setUpdatedBy(isDataChanged ? new AuthorResponseWrapper(
					currentUser.get()) : null);
			userResponse.add(linkTo(methodOn(UserController.class)
					.getUserProfileByUsername(currentUser().get())).withRel(UserResponseWrapper.RESOURCE));
			response = new APIResponseWrapper<UserResponseWrapper>(userResponse);
			response.setMessage(isDataChanged ? "User profile has been successfully updated." : "No changes in the data.");
			response.add(linkTo(methodOn(UserController.class).edit(userRequestWrapper, result)).withSelfRel());
		} catch (Exception e) {
			throw e;
		}
		
		return response;
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(userValidator);
	}	
}