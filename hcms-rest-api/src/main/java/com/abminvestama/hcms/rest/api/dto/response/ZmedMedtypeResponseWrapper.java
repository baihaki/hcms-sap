package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class ZmedMedtypeResponseWrapper extends ResourceSupport {

	private Date datab;
	private Date datbi;
	
	private String bukrs;
	private String kodemed;
	private String descr;
	
	private ZmedMedtypeResponseWrapper() {}
	
	public ZmedMedtypeResponseWrapper(ZmedMedtype zmedMedtype) {
		if (zmedMedtype == null) {
			new ZmedMedtypeResponseWrapper();
		} else {
			this
			//.setBukrs(zmedMedtype.getBukrs() != null ? StringUtils.defaultString(zmedMedtype.getBukrs().getBukrs(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setDatab(zmedMedtype.getDatab())
			.setDatbi(zmedMedtype.getDatbi())
			.setBukrs(StringUtils.defaultString(zmedMedtype.getBukrs().getBukrs(), StringUtils.EMPTY))
			.setKodemed(StringUtils.defaultString(zmedMedtype.getKodemed(), StringUtils.EMPTY))
			.setDescr(StringUtils.defaultString(zmedMedtype.getDescr(), StringUtils.EMPTY));			
		}
	}
	


	
	
	@JsonProperty("company_code")
	public String getBukrs() {
		return bukrs;
	}
	
	private ZmedMedtypeResponseWrapper setBukrs(String bukrs) {
		this.bukrs = bukrs;
		return this;
	}
	


	
	
	@JsonProperty("medical_type")
	public String getKodemed() {
		return kodemed;
	}
	
	private ZmedMedtypeResponseWrapper setKodemed(String kodemed) {
		this.kodemed = kodemed;
		return this;
	}
	

	
	@JsonProperty("description")
	public String getDescr() {
		return descr;
	}
	
	private ZmedMedtypeResponseWrapper setDescr(String descr) {
		this.descr = descr;
		return this;
	}
	


	
	
	@JsonProperty("valid_from_date")
	public Date getDatab() {
		return datab;
	}
	
	private ZmedMedtypeResponseWrapper setDatab(Date datab) {
		this.datab = datab;
		return this;
	}
	


	
	
	@JsonProperty("valid_to_date")
	public Date getDatbi() {
		return datbi;
	}
	
	private ZmedMedtypeResponseWrapper setDatbi(Date datbi) {
		this.datbi = datbi;
		return this;
	}
}

