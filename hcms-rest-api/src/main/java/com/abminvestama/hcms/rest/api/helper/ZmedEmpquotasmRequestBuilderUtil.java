package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotasmQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedEmpquotasmRequestWrapper;

@Component("ZmedEmpquotasmRequestBuilderUtil")
@Transactional(readOnly = true)
public class ZmedEmpquotasmRequestBuilderUtil {

	

	private T001QueryService t001QueryService;
	private ZmedEmpquotasmQueryService zmedEmpquotasmQueryService;
	
	@Autowired
	void setZmedEmpquotasmQueryService(ZmedEmpquotasmQueryService zmedEmpquotasmQueryService) {
		this.zmedEmpquotasmQueryService = zmedEmpquotasmQueryService;
	}
	
	@Autowired
	void setT001QueryService(T001QueryService t001QueryService) {
		this.t001QueryService = t001QueryService;
	}
	
	public RequestObjectComparatorContainer<ZmedEmpquotasm, ZmedEmpquotasmRequestWrapper, String> compareAndReturnUpdatedData(ZmedEmpquotasmRequestWrapper requestPayload, ZmedEmpquotasm zmedEmpquotasmDB) {
		
		RequestObjectComparatorContainer<ZmedEmpquotasm, ZmedEmpquotasmRequestWrapper, String> objectContainer = null; 
		
	if (zmedEmpquotasmDB == null) {
		zmedEmpquotasmDB = new ZmedEmpquotasm();
		
		try {
			Optional<ZmedEmpquotasm> existingZmedEmpquotasm = zmedEmpquotasmQueryService.findOneByCompositeKey(
				requestPayload.getPernr(), requestPayload.getBukrs(), 
				requestPayload.getPersg(), requestPayload.getPersk(),
				requestPayload.getFatxt(), requestPayload.getKodequo(), 
				CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
				CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
		
		if (existingZmedEmpquotasm.isPresent()) {
			throw new DataViolationException("Duplicate ZmedEmpquotasm Data for [ssn=" + requestPayload.getPernr());
		}
		zmedEmpquotasmDB.setId(
				new ZmedCompositeKeyEmpquotasm(
						requestPayload.getPernr(), requestPayload.getBukrs(), 
						requestPayload.getPersg(), requestPayload.getPersk(),
						requestPayload.getFatxt(), requestPayload.getKodequo(), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi())));
		
			zmedEmpquotasmDB.setDatab(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()));
			zmedEmpquotasmDB.setDatbi(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
			zmedEmpquotasmDB.setExten(Byte.parseByte(requestPayload.getExten()));
			zmedEmpquotasmDB.setKodequo(requestPayload.getKodequo());
			zmedEmpquotasmDB.setPersg(requestPayload.getPersg());
			zmedEmpquotasmDB.setPersk(requestPayload.getPersk());
			zmedEmpquotasmDB.setFatxt(requestPayload.getFatxt());
			zmedEmpquotasmDB.setPernr(requestPayload.getPernr());
			zmedEmpquotasmDB.setQuotamt(Double.parseDouble(requestPayload.getQuotamt()));			
			Optional<T001> bnka = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));				
			zmedEmpquotasmDB.setBukrs(bnka.isPresent() ? bnka.get() : null);
			//zmedEmpquotasmDB.setBukrs(requestPayload.getBukrs());
			
		} catch (Exception ignored) {
			System.err.println(ignored);
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedEmpquotasmDB, requestPayload);
		
	} else {
		
		if (StringUtils.isNotBlank(requestPayload.getPersg())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPersg(), zmedEmpquotasmDB.getPersg())) {
				zmedEmpquotasmDB.setPersg(requestPayload.getPersg());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getPersk())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPersk(), zmedEmpquotasmDB.getPersk())) {
				zmedEmpquotasmDB.setPersk(requestPayload.getPersk());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getFatxt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getFatxt(), zmedEmpquotasmDB.getFatxt())) {
				zmedEmpquotasmDB.setFatxt(requestPayload.getFatxt());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getKodequo())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getKodequo(), zmedEmpquotasmDB.getKodequo())) {
				zmedEmpquotasmDB.setKodequo(requestPayload.getKodequo());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedEmpquotasmDB.getBukrs() != null ? zmedEmpquotasmDB.getBukrs().getBukrs() : StringUtils.EMPTY)) {
				try {
					Optional<T001> newT001 = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));
					if (newT001.isPresent()) {
						zmedEmpquotasmDB.setBukrs(newT001.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					//T535N not found... cancel the update process
				}
			}
		}
		
		/*if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedEmpquotasmDB.getBukrs())) {
				zmedEmpquotasmDB.setBukrs(requestPayload.getBukrs());
				requestPayload.setDataChanged(true);
			}
		}*/

			
		
		if (StringUtils.isNotBlank(requestPayload.getExten())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getExten(), zmedEmpquotasmDB.getExten().toString())) {
				zmedEmpquotasmDB.setExten(Byte.parseByte(requestPayload.getExten()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getQuotamt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getQuotamt(), zmedEmpquotasmDB.getQuotamt().toString())) {
				zmedEmpquotasmDB.setQuotamt(Double.parseDouble(requestPayload.getQuotamt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getDatab())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), (zmedEmpquotasmDB.getDatab()))) {
				zmedEmpquotasmDB.setDatab(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()));
				requestPayload.setDataChanged(true);
			}
		}
		
		
		if (StringUtils.isNotBlank(requestPayload.getDatbi())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()), (zmedEmpquotasmDB.getDatbi())) ){
				zmedEmpquotasmDB.setDatbi(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
				requestPayload.setDataChanged(true);
			}
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedEmpquotasmDB, requestPayload);	
				
	}
	
	JSONObject json = new JSONObject(requestPayload);
	objectContainer.setEventData(json.toString());
	
	return objectContainer;
}
}