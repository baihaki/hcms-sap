package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.IT0022;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyCostype;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.service.api.business.query.ZmedCostypeQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedCostypeRequestWrapper;



@Component("ZmedCostypeRequestBuilderUtil")
@Transactional(readOnly = true)
public class ZmedCostypeRequestBuilderUtil {
	

	private T001QueryService t001QueryService;
	private ZmedCostypeQueryService zmedCostypeQueryService;
	
	@Autowired
	void setZmedCostypeQueryService(ZmedCostypeQueryService zmedCostypeQueryService) {
		this.zmedCostypeQueryService = zmedCostypeQueryService;
	}
	
	@Autowired
	void setT001QueryService(T001QueryService t001QueryService) {
		this.t001QueryService = t001QueryService;
	}
	
	public RequestObjectComparatorContainer<ZmedCostype, ZmedCostypeRequestWrapper, String> compareAndReturnUpdatedData(ZmedCostypeRequestWrapper requestPayload, ZmedCostype zmedCostypeDB) {
		
		RequestObjectComparatorContainer<ZmedCostype, ZmedCostypeRequestWrapper, String> objectContainer = null; 
		
	if (zmedCostypeDB == null) {
		zmedCostypeDB = new ZmedCostype();
		
		try {
			Optional<ZmedCostype> existingZmedCostype = zmedCostypeQueryService.findOneByCompositeKey(
					requestPayload.getBukrs(), requestPayload.getKodemed(), requestPayload.getKodecos(),
					CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
					CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
			
			if (existingZmedCostype.isPresent()) {
				throw new DataViolationException("Duplicate ZmedCostype Data for [ssn=" + requestPayload.getKodemed());
			}
			zmedCostypeDB.setId(
					new ZmedCompositeKeyCostype(requestPayload.getBukrs(), requestPayload.getKodemed(),
							requestPayload.getKodecos(), 
							CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
							CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi())));
			zmedCostypeDB.setDatab(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()));
			zmedCostypeDB.setDatbi(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
			zmedCostypeDB.setDescr(requestPayload.getDescr());
			zmedCostypeDB.setKodemed(requestPayload.getKodemed());
			zmedCostypeDB.setKodecos(requestPayload.getKodecos());			
			Optional<T001> bnka = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));				
			zmedCostypeDB.setBukrs(bnka.isPresent() ? bnka.get() : null);
			//zmedCostypeDB.setBukrs(requestPayload.getBukrs());
			
		} catch (Exception ignored) {
			System.err.println(ignored);
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedCostypeDB, requestPayload);
		
	} else {
		
		if (StringUtils.isNotBlank(requestPayload.getDescr())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDescr(), zmedCostypeDB.getDescr())) {
				zmedCostypeDB.setDescr(requestPayload.getDescr());
				requestPayload.setIsDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getKodemed())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getKodemed(), zmedCostypeDB.getKodemed())) {
				zmedCostypeDB.setKodemed(requestPayload.getKodemed());
				requestPayload.setIsDataChanged(true);
			}
		}
		
		
		/*if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedCostypeDB.getBukrs())) {
				zmedCostypeDB.setBukrs(requestPayload.getBukrs());
				requestPayload.setIsDataChanged(true);
			}
		}*/
		
		if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedCostypeDB.getBukrs() != null ? zmedCostypeDB.getBukrs().getBukrs() : StringUtils.EMPTY)) {
				try {
					Optional<T001> newT001 = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));
					if (newT001.isPresent()) {
						zmedCostypeDB.setBukrs(newT001.get());
						requestPayload.setIsDataChanged(true);
					}
				} catch (Exception e) {
					//T535N not found... cancel the update process
				}
			}
		}

			
		
		if (StringUtils.isNotBlank(requestPayload.getKodecos())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getKodecos(), zmedCostypeDB.getKodecos())) {
				zmedCostypeDB.setKodecos(requestPayload.getKodecos());
				requestPayload.setIsDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getDatab())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), (zmedCostypeDB.getDatab()))) {
				zmedCostypeDB.setDatab(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()));
				requestPayload.setIsDataChanged(true);
			}
		}
		
		
		if (StringUtils.isNotBlank(requestPayload.getDatbi())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()), (zmedCostypeDB.getDatbi())) ){
				zmedCostypeDB.setDatbi(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
				requestPayload.setIsDataChanged(true);
			}
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedCostypeDB, requestPayload);	
				
	}
	
	JSONObject json = new JSONObject(requestPayload);
	objectContainer.setEventData(json.toString());
	
	return objectContainer;
}
}
