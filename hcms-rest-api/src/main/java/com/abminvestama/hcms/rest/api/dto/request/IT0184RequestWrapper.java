package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add text1 field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public class IT0184RequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = -8016599130340609254L;
	
	private String itxex;
	private String text1;
	
	@JsonProperty("text_data")
	public String getItxex() {
		return itxex;
	}
	@JsonProperty("text_value")
	public String getText1() {
		return text1;
	}
}