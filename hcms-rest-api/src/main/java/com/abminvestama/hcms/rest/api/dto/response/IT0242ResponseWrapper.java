package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0242;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Wrapper class for <strong>BPJS Data</strong> (i.e. IT0242 in SAP)
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add attachmentType, attachmentTypeText, attachmentPath field</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class IT0242ResponseWrapper extends ResourceSupport {

	private long pernr;
	private Date endda;
	private Date begda;
	
	private String jamId;
	private String marst;
	private String documentStatus;
	
	private String attachmentType;
	private String attachmentTypeText;
	private String attachmentPath;
	
	private IT0242ResponseWrapper() {}
	
	public IT0242ResponseWrapper(IT0242 it0242) {
		if (it0242 == null) {
			new IT0242ResponseWrapper();
		} else {
			this
				.setPernr(it0242.getId().getPernr())
				.setEndda(it0242.getId().getEndda())
				.setBegda(it0242.getId().getBegda())
				.setJamId(StringUtils.defaultString(it0242.getJamId(), null))
				.setMarst(StringUtils.defaultString(it0242.getMarst(), null))
				.setAttachmentType(it0242.getAttachment() != null ? StringUtils.defaultString(it0242.getAttachment().getId().getSubty(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentTypeText(it0242.getAttachment() != null ? StringUtils.defaultString(it0242.getAttachment().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentPath(StringUtils.defaultString(it0242.getAttachmentPath(), StringUtils.EMPTY))
				.setDocumentStatus(it0242.getStatus());				
		}		
	}
	
	/**
	 * GET Employee SSN.
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	private IT0242ResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	private IT0242ResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}
	
	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	private IT0242ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * GET BPJS ID.
	 * 
	 * @return
	 */
	@JsonProperty("jam_id")
	public String getJamId() {
		return jamId;
	}
	
	private IT0242ResponseWrapper setJamId(String jamId) {
		this.jamId = jamId;
		return this;
	}
	
	/**
	 * GET Marital Status.
	 * 
	 * @return
	 */
	@JsonProperty("marital_status")
	public String getMarst() {
		return marst;
	}
	
	private IT0242ResponseWrapper setMarst(String marst) {
		this.marst = marst;
		return this;
	}
	
	/**
	 * GET Document Status.
	 * 
	 * @return
	 */
	@JsonProperty("document_status")
	public String getDocumentStatus() {
		return documentStatus;
	}
	
	private IT0242ResponseWrapper setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus.name();
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type")
	public String getAttachmentType() {
		return attachmentType;
	}
	
	private IT0242ResponseWrapper setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
		return this;
	}
	
	/**
	 * GET Attachment Type Text.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type_text")
	public String getAttachmentTypeText() {
		return attachmentTypeText;
	}
	
	private IT0242ResponseWrapper setAttachmentTypeText(String attachmentTypeText) {
		this.attachmentTypeText = attachmentTypeText;
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_path")
	public String getAttachmentPath() {
		return attachmentPath;
	}
	
	private IT0242ResponseWrapper setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
		return this;
	}
}