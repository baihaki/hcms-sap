package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ZmedQuotypeRequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = -2317050437190237970L;
	
	private String bukrs;
	private String kodequo;
	private String persen;
	private String datab;
	private String datbi;
	private String descr;
	
	@JsonProperty("company_code")
	public String getBukrs() {
		return bukrs;
	}
	
	
	@JsonProperty("balance_percentage")
	public String getPersen() {
		return persen;
	}
	
	
	@JsonProperty("quota_type")
	public String getKodequo() {
		return kodequo;
	}
	
	
	@JsonProperty("valid_from_date")
	public String getDatab() {
		return datab;
	}
	
	
	@JsonProperty("valid_to_date")
	public String getDatbi() {
		return datbi;
	}
	
	
	@JsonProperty("description")
	public String getDescr() {
		return descr;
	}
}
