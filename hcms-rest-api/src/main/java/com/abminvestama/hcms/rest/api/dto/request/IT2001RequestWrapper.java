package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add field: address, phone, mobile, notes, reason, infotype, bukrs, process</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add field: start, end</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public class IT2001RequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = -3044978063551232664L;
	
	private String awart;
	private double abwtg;
	private double stdaz;
	private double kaltg;
	private String start;
	private String end;
	private String address;
	private String phone;
	private String mobile;
	private String notes;
	private String reason;
	private String infotype;
	private String process;
	
	@JsonIgnore
	private String bukrs;
	
	@JsonProperty("absence_type")
	public String getAwart() {
		return awart;
	}
	
	@JsonProperty("absence_days")
	public double getAbwtg() {
		return abwtg;
	}
	
	@JsonProperty("absence_hours")
	public double getStdaz() {
		return stdaz;
	}
	
	@JsonProperty("calendar_days")
	public double getKaltg() {
		return kaltg;
	}
	
	@JsonProperty("start_time")
	public String getStart() {
		return start;
	}
	
	@JsonProperty("end_time")
	public String getEnd() {
		return end;
	}
	
	@JsonProperty("address_on_leave")
	public String getAddress() {
		return address;
	}

	@JsonProperty("telephone")
	public String getPhone() {
		return phone;
	}

	@JsonProperty("mobile_phone")
	public String getMobile() {
		return mobile;
	}

	@JsonProperty("notes")
	public String getNotes() {
		return notes;
	}

	@JsonProperty("reason_for_revision")
	public String getReason() {
		return reason;
	}
	
	@JsonProperty("infotype")
	public String getInfotype() {
		return infotype;
	}
	
	@JsonProperty("process")
	public String getProcess() {
		return process;
	}

	public String getBukrs() {
		return bukrs;
	}

	public void setBukrs(String bukrs) {
		this.bukrs = bukrs;
	}
	
}