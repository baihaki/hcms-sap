package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.entity.T002T;
import com.abminvestama.hcms.core.service.api.business.query.T002TQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@RestController
@RequestMapping("/api/v1/language")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class T002TController extends AbstractResource {
	
	private T002TQueryService t002tQueryService;
	
	@Autowired
	void setT002TQueryService(T002TQueryService t002tQueryService) {
		this.t002tQueryService = t002tQueryService;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/page/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<T002T>> fetchPage(@PathVariable int pageNumber, 
			@RequestParam(required = true, value = "username", defaultValue = "") String username,
			@RequestParam(required = true, value = "auth_token", defaultValue = "") String authToken) throws Exception {
		
		if (authToken == null || authToken.isEmpty()) {
			throw new Exception("No token provided");
		}
		
		Page<T002T> resultPage = t002tQueryService.fetchAllWithPaging(pageNumber);
		
		APIResponseWrapper<Page<T002T>> response = new APIResponseWrapper<>();
		response.setData(resultPage);
		Link self = linkTo(methodOn(T002TController.class).fetchPage(pageNumber, username, authToken)).withSelfRel();
		response.add(self);
		
		return response;
	}	
}