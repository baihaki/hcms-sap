package com.abminvestama.hcms.rest.api.validator;

import java.time.DateTimeException;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.Position;
import com.abminvestama.hcms.core.model.entity.PositionRelationType;
import com.abminvestama.hcms.core.service.api.business.query.PositionQueryService;
import com.abminvestama.hcms.core.service.api.business.query.PositionRelationTypeQueryService;
import com.abminvestama.hcms.rest.api.dto.request.PositionRelationRequestWrapper;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Component("positionRelationValidator")
public class PositionRelationValidator implements Validator {
	
	private PositionRelationTypeQueryService positionRelationTypeQueryService;
	private PositionQueryService positionQueryService;

	@Autowired
	void setPositionRelationTypeQueryService(PositionRelationTypeQueryService positionRelationTypeQueryService) {
		this.positionRelationTypeQueryService = positionRelationTypeQueryService;
	}
	
	@Autowired
	void setPositionQueryService(PositionQueryService positionQueryService) {
		this.positionQueryService = positionQueryService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return PositionRelationRequestWrapper.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		final PositionRelationRequestWrapper request = (PositionRelationRequestWrapper) target;
		
		try {
			if (StringUtils.isBlank(request.getPositionRelationTypeId())) {
				errors.rejectValue("positionRelationType", null,
						"'position_relation_type_id' is missing from the payload!");
			} else {
				Optional<PositionRelationType> positionRelationType = positionRelationTypeQueryService
						.findById(Optional.of(request.getPositionRelationTypeId()));
				if (!positionRelationType.isPresent()) {
					errors.rejectValue("positionRelationType", null, "Cannot find 'position_relation_type_id' " + request.getPositionRelationTypeId());
				}
			}
			
			if (StringUtils.isBlank(request.getFromPositionId())) {
				errors.rejectValue("fromPosition", null, "'from_position_id' is missing from the payload!");
			} else {
				Optional<Position> fromPosition = positionQueryService.findById(Optional.of(request.getFromPositionId()));
				if (!fromPosition.isPresent()) {
					errors.rejectValue("fromPosition", null, "Cannot find 'from_position_id' " + request.getFromPositionId());
				}
			}
			
			if (StringUtils.isBlank(request.getToPositionId())) {
				errors.rejectValue("toPosition", null, "'to_position_id' is missing from the payload!");
			} else {
				Optional<Position> toPosition = positionQueryService.findById(Optional.of(request.getToPositionId()));
				if (!toPosition.isPresent()) {
					errors.rejectValue("toPosition", null, "Cannot find 'to_position_id' " + request.getToPositionId());
				}
			}			
			
			if (StringUtils.isNotBlank(request.getValidFrom())) {
				CommonDateFunction.convertDateRequestParameterIntoDate(request.getValidFrom());
			}
			
			if (StringUtils.isNotBlank(request.getValidThru())) {
				CommonDateFunction.convertDateRequestParameterIntoDate(request.getValidThru());
			}
		} catch (DateTimeException e) { 
			errors.rejectValue("validFrom", null, "'valid_from' is not valid! " + e.getMessage());
		} catch (Exception e) {
			errors.rejectValue("positionRelationType", null, e.getMessage());
		}
	}
}