package com.abminvestama.hcms.rest.api.dto.response;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.Tevenvw;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class TevenvwResponseWrapper extends ResourceSupport {

	private Long pernr;
	private String ename; 		// Employee Name
	private String ldate; 		// Logical Date
	private String begagtime;	// Begin time actual(machine)
	private String begltime; 	// Begin Logical Time
	private String endagtime;	// End time actual(machine)
	private String endltime; 	// End Logical Time
//	private Double wahour; 		// Working Actual Hour
	private Double wdhour;		// Working Delay Hour
	private Double whour; 		// Working Hour
//	private Double phour; 		// Productivity Hour
	private String begprocess; 	// Begin Process
	private String endprocess; 	// End Process
	private String begtrxcode; 	// Begin Transaction Code
	private String endtrxcode; 	// End Transaction Code
	private String begreason; 	// Begin Reason
	private String endreason; 	// End Reason
//	private String firstdoor;
//	private String lastdoor;
	private String divisionName;
	private String departmentName;
	private String begaprvnote; // Begin Approval Note
	private String endaprvnote; // End Approval Note

	private TevenvwResponseWrapper() {
	}

	public TevenvwResponseWrapper(Tevenvw tevenvw) {
		if (tevenvw == null) {
			new TevenvwResponseWrapper();
		} else {
			String DEFAULT_FORMAT_DATE = "yyyy-MM-dd";
			String DEFAULT_FORMAT_TIME = "HH:mm";
			DateFormat formatDate = new SimpleDateFormat(DEFAULT_FORMAT_DATE);
			DateFormat formatTime = new SimpleDateFormat(DEFAULT_FORMAT_TIME);

			this.setPernr(tevenvw.getPernr()).setEname(StringUtils.defaultString(tevenvw.getEname()))
					.setLdate(tevenvw.getLdate() != null ? formatDate.format(tevenvw.getLdate()) : "")
					.setBegagtime(tevenvw.getBegagtime() != null ? formatTime.format(tevenvw.getBegagtime()) : "")
					.setBegltime(tevenvw.getBegltime() != null ? formatTime.format(tevenvw.getBegltime()) : "")
					.setEndagtime(tevenvw.getEndagtime() != null ? formatTime.format(tevenvw.getEndagtime()) : "")
					.setEndltime(tevenvw.getEndltime() != null ? formatTime.format(tevenvw.getEndltime()) : "")
//					.setWahour(tevenvw.getWahour() != null ? tevenvw.getWahour() : 0L)
					.setWdhour(tevenvw.getWdhour() != null ? tevenvw.getWdhour() : 0L)
					.setWhour(tevenvw.getWhour() != null ? tevenvw.getWhour() : 0L)
//					.setPhour(tevenvw.getPhour() != null ? tevenvw.getPhour() : 0L)
					.setBegprocess(StringUtils.defaultString(tevenvw.getBegprocess()))
					.setEndprocess(StringUtils.defaultString(tevenvw.getEndprocess()))
					.setBegtrxcode(StringUtils.defaultString(tevenvw.getBegTrxCode()))
					.setEndtrxcode(StringUtils.defaultString(tevenvw.getEndTrxCode()))
					.setBegreason(StringUtils.defaultString(tevenvw.getBegreason()))
					.setEndreason(StringUtils.defaultString(tevenvw.getEndreason()))
					.setDivisionName(StringUtils.defaultString(tevenvw.getDivisionName()))
					.setDepartmentName(StringUtils.defaultString(tevenvw.getDepartmentName()))
					.setBegaprvnote(StringUtils.defaultString(tevenvw.getBegaprvnote()))
					.setEndaprvnote(StringUtils.defaultString(tevenvw.getEndaprvnote()));
		}
	}

	/**
	 * GET Personal Information.
	 * 
	 * @return
	 */
	@JsonProperty("pernr")
	public Long getPernr() {
		return pernr;
	}

	private TevenvwResponseWrapper setPernr(Long pernr) {
		this.pernr = pernr;
		return this;
	}

	/**
	 * GET Employee Name.
	 * 
	 * @return
	 */
	@JsonProperty("ename")
	public String getEname() {
		return ename;
	}

	public TevenvwResponseWrapper setEname(String ename) {
		this.ename = ename;
		return this;
	}

	/**
	 * GET Logical Date.
	 * 
	 * @return
	 */
	@JsonProperty("ldate")
	public String getLdate() {
		return ldate;
	}

	public TevenvwResponseWrapper setLdate(String ldate) {
		this.ldate = ldate;
		return this;
	}

	/**
	 * GET Begin time actual(machine).
	 * 
	 * @return
	 */
	@JsonProperty("begagtime")
	public String getBegagtime() {
		return begagtime;
	}

	public TevenvwResponseWrapper setBegagtime(String begagtime) {
		this.begagtime = begagtime;
		return this;
	}

	/**
	 * GET Begin Logical Time.
	 * 
	 * @return
	 */
	@JsonProperty("begltime")
	public String getBegltime() {
		return begltime;
	}

	public TevenvwResponseWrapper setBegltime(String begltime) {
		this.begltime = begltime;
		return this;
	}

	/**
	 * GET End time actual(machine).
	 * 
	 * @return
	 */
	@JsonProperty("endagtime")
	public String getEndagtime() {
		return endagtime;
	}

	public TevenvwResponseWrapper setEndagtime(String endagtime) {
		this.endagtime = endagtime;
		return this;
	}

	/**
	 * GET End Logical Time.
	 * 
	 * @return
	 */
	@JsonProperty("endltime")
	public String getEndltime() {
		return endltime;
	}

	public TevenvwResponseWrapper setEndltime(String endltime) {
		this.endltime = endltime;
		return this;
	}

	/**
	 * GET Working Actual Hour.
	 * 
	 * @return
	 */
//	@JsonProperty("wahour")
//	public Double getWahour() {
//		return wahour;
//	}
//
//	public TevenvwResponseWrapper setWahour(Double wahour) {
//		this.wahour = wahour;
//		return this;
//	}

	/**
	 * GET Working Delay Hour.
	 * 
	 * @return
	 */
	@JsonProperty("wdhour")
	public Double getWdhour() {
		return wdhour;
	}

	public TevenvwResponseWrapper setWdhour(Double wdhour) {
		this.wdhour = wdhour;
		return this;
	}

	/**
	 * GET Working Hour.
	 * 
	 * @return
	 */
	@JsonProperty("whour")
	public Double getWhour() {
		return whour;
	}

	public TevenvwResponseWrapper setWhour(Double whour) {
		this.whour = whour;
		return this;
	}

	/**
	 * GET Productivity Hour.
	 * 
	 * @return
	 */
//	@JsonProperty("phour")
//	public Double getPhour() {
//		return phour;
//	}
//
//	public TevenvwResponseWrapper setPhour(Double phour) {
//		this.phour = phour;
//		return this;
//	}

	/**
	 * GET Begin Process(TEVEN check in).
	 * 
	 * @return
	 */
	@JsonProperty("begprocess")
	public String getBegprocess() {
		return begprocess;
	}

	public TevenvwResponseWrapper setBegprocess(String begprocess) {
		this.begprocess = begprocess;
		return this;
	}

	/**
	 * GET End Process(TEVEN check out).
	 * 
	 * @return
	 */
	@JsonProperty("endprocess")
	public String getEndprocess() {
		return endprocess;
	}

	public TevenvwResponseWrapper setEndprocess(String endprocess) {
		this.endprocess = endprocess;
		return this;
	}

	/**
	 * GET Begin Transaction Code(TEVEN check in)
	 * 
	 */
	@JsonProperty("begtrxcode")
	public String getBegtrxcode() {
		return begtrxcode;
	}

	public TevenvwResponseWrapper setBegtrxcode(String begtrxcode) {
		this.begtrxcode = begtrxcode;
		return this;
	}

	/**
	 * GET Begin Transaction Code(TEVEN check out).
	 * 
	 * @return
	 */
	@JsonProperty("endtrxcode")
	public String getEndtrxcode() {
		return endtrxcode;
	}

	public TevenvwResponseWrapper setEndtrxcode(String endtrxcode) {
		this.endtrxcode = endtrxcode;
		return this;
	}

	/**
	 * get first door location(Automatic Create by Machine)
	 * 
	 * @return
	@JsonProperty("firstdoor")
	public String getFirstdoor() {
		return firstdoor;
	}

	public TevenvwResponseWrapper setFirstdoor(String firstdoor) {
		this.firstdoor = firstdoor;
		return this;
	}
	 */

	/**
	 * get last door location(Automatic Create by Machine)
	 * 
	 * @return
	@JsonProperty("lastdoor")
	public String getLastdoor() {
		return lastdoor;
	}

	public TevenvwResponseWrapper setLastdoor(String lastdoor) {
		this.lastdoor = lastdoor;
		return this;
	}
	 */

	/**
	 * Begin Reason
	 * 
	 * @return
	 */
	@JsonProperty("begreason")
	public String getBegreason() {
		return begreason;
	}

	public TevenvwResponseWrapper setBegreason(String begreason) {
		this.begreason = begreason;
		return this;
	}

	/**
	 * End Reason
	 * 
	 * @return
	 */
	@JsonProperty("endreason")
	public String getEndreason() {
		return endreason;
	}

	public TevenvwResponseWrapper setEndreason(String endreason) {
		this.endreason = endreason;
		return this;
	}

	/**
	 * Division Name
	 * 
	 * @return
	 */
	@JsonProperty("divisionName")
	public String getDivisionName() {
		return divisionName;
	}

	public TevenvwResponseWrapper setDivisionName(String divisionName) {
		this.divisionName = divisionName;
		return this;
	}

	/**
	 * Department Name
	 * 
	 * @return
	 */
	@JsonProperty("departmentName")
	public String getDepartmentName() {
		return departmentName;
	}

	public TevenvwResponseWrapper setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
		return this;
	}

	/**
	 * Begin Approval Note
	 * 
	 * @return
	 */
	@JsonProperty("begaprvnote")
	public String getBegaprvnote() {
		return begaprvnote;
	}

	public TevenvwResponseWrapper setBegaprvnote(String begaprvnote) {
		this.begaprvnote = begaprvnote;
		return this;
	}

	/**
	 * End Approval Note
	 * 
	 * @return
	 */
	@JsonProperty("endaprvnote")
	public String getEndaprvnote() {
		return endaprvnote;
	}

	public TevenvwResponseWrapper setEndaprvnote(String endaprvnote) {
		this.endaprvnote = endaprvnote;
		return this;
	}

}