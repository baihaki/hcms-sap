package com.abminvestama.hcms.rest.api.dto.helper;

import com.abminvestama.hcms.core.model.entity.Role;

/**
 * 
 * @author wijanarko (wijanarko777@gmx.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 * <code>UserLdap</code> for validation user LDAP
 * 
 * @see Role
 */
public class UserLdap {
	private String domain;
	private String username;
	private String password;
	private String choice;
	private String searchTerm;
	private Boolean connectionStatus;

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getChoice() {
		return choice;
	}

	public void setChoice(String choice) {
		this.choice = choice;
	}

	public String getSearchTerm() {
		return searchTerm;
	}

	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}

	public Boolean isConnectionStatus() {
		return connectionStatus;
	}

	public void setConnectionStatus(Boolean connectionStatus) {
		this.connectionStatus = connectionStatus;
	}

	public Boolean getConnectionStatus() {
		return connectionStatus;
	}

}
