package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.IT0002;
import com.abminvestama.hcms.core.model.entity.IT0021;
import com.abminvestama.hcms.core.model.entity.IT0184;
import com.abminvestama.hcms.core.model.entity.T706S;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0002QueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0021QueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0184QueryService;
import com.abminvestama.hcms.core.service.api.business.query.T706SQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.MedPatientResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.T706SResponseWrapper;

@RestController
@RequestMapping("/api/v1/trip_schema")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class T706SController extends AbstractResource {

	private T706SQueryService t706sQueryService;
	
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	
	
	@Autowired
	void setT706SQueryService(T706SQueryService t706sQueryService) {
		this.t706sQueryService = t706sQueryService;
	}
	
	
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	
	
	
	
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/morei/{morei}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<T706SResponseWrapper>>
	/*APIResponseWrapper<?>*/ findByMoreiNE(@PathVariable String morei) throws Exception {
		
		ArrayData<T706SResponseWrapper> empCost = new ArrayData<>();
		APIResponseWrapper<ArrayData<T706SResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(empCost);
		
		try {
					List<T706S> listOfT706S = t706sQueryService.findByMoreiNE(morei).stream().collect(Collectors.toList());

					if (listOfT706S.isEmpty()) {
						response.setMessage("No Data Found in t706s");
					} else {
						List<T706SResponseWrapper> records = new ArrayList<>();
						//records.add(new MedPatientResponseWrapper(it0001));
						for (T706S Zmed : listOfT706S) {
							records.add(new T706SResponseWrapper(Zmed));
						}
						empCost.setItems(records.toArray(new T706SResponseWrapper[records.size()]));							
					}			
		} catch (NumberFormatException e) { 
			throw e;
		} catch (UsernameNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(T706SController.class).findByMoreiNE(morei)).withSelfRel());
		return response;
	}
	
	
		
		

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}

}
