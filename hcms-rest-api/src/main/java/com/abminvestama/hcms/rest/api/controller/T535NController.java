package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.entity.T535N;
import com.abminvestama.hcms.core.service.api.business.query.T535NQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@RestController
@RequestMapping("/api/v1/master_title")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class T535NController extends AbstractResource {

	private T535NQueryService t535nQueryService;
	
	@Autowired
	void setT535NQueryService(T535NQueryService t535nQueryService) {
		this.t535nQueryService = t535nQueryService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/page/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<T535N>> fetchPage(@PathVariable int pageNumber, 
			@RequestParam(value = "art", required = true) String art) throws Exception {
		
		Page<T535N> resultPage = null;
		
		if (StringUtils.isNotEmpty(art)) {
		    resultPage = t535nQueryService.fetchByArtWithPaging(art, pageNumber);
		} else {
		    //resultPage = t535nQueryService.fetchAlldWithPaging(pageNumber);
		}
		
		APIResponseWrapper<Page<T535N>> response = new APIResponseWrapper<>();
		response.setData(resultPage);
		Link self = linkTo(methodOn(T535NController.class).fetchPage(pageNumber, art)).withSelfRel();
		response.add(self);
		
		return response;
	}				
}