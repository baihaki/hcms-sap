package com.abminvestama.hcms.rest.api.helper;

import java.text.ParseException;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.CaOrder;
import com.abminvestama.hcms.core.model.entity.CaStlDtl;
import com.abminvestama.hcms.core.service.api.business.query.CaOrderQueryService;
import com.abminvestama.hcms.core.service.api.business.query.CaStlDtlQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.CaStlDtlRequestWrapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author wijanarko (wijanarko@gmx.com) <br>
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 * 
 */
@Component("CaStlDtlRequestBuilderUtil")
@Transactional(readOnly = true)
public class CaStlDtlRequestBuilderUtil {
	private T001QueryService t001QueryService;
	private UserQueryService userQueryService;
	private CaStlDtlQueryService caStlDtlQueryService;
	private CaOrderQueryService caOrderQueryService;

	@Autowired
	void setT001QueryService(T001QueryService t001QueryService) {
		this.t001QueryService = t001QueryService;
	}

	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}

	@Autowired
	void setCaStlDtlQueryService(CaStlDtlQueryService caStlDtlQueryService) {
		this.caStlDtlQueryService = caStlDtlQueryService;
	}

	@Autowired
	void setCaOrderQueryService(CaOrderQueryService caOrderQueryService) {
		this.caOrderQueryService = caOrderQueryService;
	}
	
	/**
	 * Compare CaStlDtl request payload with existing CaStlDtl in the database.
	 * Update existing CaStlDtl data with the latest data comes from the request payload. 
	 * 
	 * @param requestPayload request data 
	 * @param caStlDtlDB current existing CaStlDtl in the database.
	 * @return updated CaStlDtl object to be persisted into the database.
	 * @throws Exception 
	 * @throws ParseException 
	 */
	public RequestObjectComparatorContainer<CaStlDtl, CaStlDtlRequestWrapper, String> compareAndReturnUpdatedData(
			CaStlDtlRequestWrapper requestPayload, CaStlDtl caStlDtlDB) throws Exception {

		RequestObjectComparatorContainer<CaStlDtl, CaStlDtlRequestWrapper, String> objectContainer = null; 
		
		if (caStlDtlDB == null) {
			caStlDtlDB = new CaStlDtl();

			if (!StringUtils.isNotEmpty(requestPayload.getReqNo())) {
				throw new DataViolationException("Cash Advance Settlement Detail - Request No. is empty");
			}
			caStlDtlDB.setReqNo(requestPayload.getReqNo());

			if (requestPayload.getItemNo() == null) {
				throw new DataViolationException("Cash Advance Settlement Detail - Item No. is empty");
			}
			caStlDtlDB.setItemNo(requestPayload.getItemNo());

			// validation duplicate by Unix Key
			Optional<CaStlDtl> existingCaStlDtl = caStlDtlQueryService
					.findOneByReqnoAndItemno(requestPayload.getReqNo(), requestPayload.getItemNo());
			if (existingCaStlDtl.isPresent()) {
				throw new DataViolationException("Duplicate Cash Advance Settlement Detail Data : "
						+ "Request No = " + existingCaStlDtl.get().getReqNo() + ", "
						+ "Item No = " + existingCaStlDtl.get().getItemNo() + " ");
			}

			if (requestPayload.getStlDate() == null) {
				throw new DataViolationException("Cash Advance Settlement Detail - Description is empty");
			}
			caStlDtlDB.setStlDate(requestPayload.getStlDate());

			if (!StringUtils.isNotEmpty(requestPayload.getDescription())) {
				throw new DataViolationException("Cash Advance Settlement Detail - Description is empty");
			}
			caStlDtlDB.setDescription(requestPayload.getDescription());

			// Default Amount = 0
			caStlDtlDB.setAmount(requestPayload.getAmount() != null ? new Double(requestPayload.getAmount()) : 0.0);

			if (!StringUtils.isNotEmpty(requestPayload.getCurrency())) {
				throw new DataViolationException("Cash Advance Settlement Detail - Currency is empty");
			}
			caStlDtlDB.setCurrency(requestPayload.getCurrency());
			
			if (requestPayload.getOrderId() == null) {
				throw new DataViolationException("Cash Advance Settlement Detail - Order Id is empty");
			}
			else {
				// Validation Order ID
				// 1. get GL Account - Internal Order Item by Order ID
				// 2. validation GL Account - Internal Order Item is exist
				Optional<CaOrder> existingCaorder;
				existingCaorder = caOrderQueryService.findById(Optional.ofNullable(requestPayload.getOrderId()));
				if (!existingCaorder.isPresent()) {
					throw new DataViolationException("Cash Advance Settlement Detail - Order Id not exist");
				}
				caStlDtlDB.setOrderId(existingCaorder.get());	// MANDATORY
			}
			
			caStlDtlDB.setPoolBudget(requestPayload.getPoolBudget());
			
		}
		// EDIT ITEM
		else {
			// edit statement
		}
		objectContainer = new RequestObjectComparatorContainer<>(caStlDtlDB, requestPayload);

		JSONObject json = new JSONObject(requestPayload);
		objectContainer.setEventData(json.toString());

		return objectContainer;
	}

}
