package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.constant.UserRole;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.service.api.business.query.RoleQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.RoleResponseWrapper;

/**
 * 
 * @author yauritux@gmail.com
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@RestController
@RequestMapping("/api/v1/roles")
@Transactional(readOnly = true)
public class RoleController extends AbstractResource {

	private RoleQueryService roleQueryService;
	
	@Autowired
	void setRoleQueryService(RoleQueryService roleQueryService) {
		this.roleQueryService = roleQueryService;
	}
	
	@RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({UserRole.AccessRole.USER_GROUP, UserRole.AccessRole.SUPERIOR_GROUP, UserRole.AccessRole.ADMIN_GROUP})
	public APIResponseWrapper<ArrayData<RoleResponseWrapper>> fetchAll() throws Exception {
		ArrayData<RoleResponseWrapper> bunchOfRole = new ArrayData<>();
		APIResponseWrapper<ArrayData<RoleResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfRole);
		
		try {
			Optional<List<Role>> listOfRole = roleQueryService.fetchAll().map(roles -> {
				List<Role> roleRecords = new ArrayList<>();
				for (Role role : roles) {
					roleRecords.add(role);
				}
				return roleRecords;
			});
			
			if (!listOfRole.isPresent() || listOfRole.get().isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<RoleResponseWrapper> records = new ArrayList<>();
				for (Role role : listOfRole.get()) {
					records.add(new RoleResponseWrapper(role));
				}
				bunchOfRole.setItems(records.toArray(new RoleResponseWrapper[records.size()]));
			}
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(RoleController.class).fetchAll()).withSelfRel());
		return response;
	}
}
