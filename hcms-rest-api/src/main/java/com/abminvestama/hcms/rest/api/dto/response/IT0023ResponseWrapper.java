package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0023;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Wrapper class for <strong>Employment History</strong> (i.e. IT0023 in SAP).
 * 
 * @since 1.0.0
 * @version 2.0.0
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.0</td><td>Baihaki</td><td>Add posName, posRoles field</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add Constructor of Read from EventData -> IT0023ResponseWrapper(IT0023, String, CommonServiceFactory)</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add attachmentType, attachmentTypeText, attachmentPath field</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add documentStatus field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 * 
 */
@JsonInclude(Include.NON_NULL)
public class IT0023ResponseWrapper extends ResourceSupport {

	private long pernr;
	private Date endda;
	private Date begda;
	
	private String arbgb;
	private String ort01;
	private String land1;
	private String land1Text;
	private String branc;
	private String brancText;
	private long taete;
	private String taeteText;
	private String ansvx;
	private String documentStatus;
	
	private String attachmentType;
	private String attachmentTypeText;
	private String attachmentPath;
	
	private String posName;
	private String posRoles;
	
	private  IT0023ResponseWrapper() {}
	
	public IT0023ResponseWrapper(IT0023 it0023) {
		if (it0023 == null) {
			new IT0023ResponseWrapper();
		} else {
			this
				.setPernr(it0023.getId().getPernr())
				.setEndda(it0023.getId().getEndda()).setBegda(it0023.getId().getBegda())
				.setArbgb(StringUtils.defaultString(it0023.getArbgb(), StringUtils.EMPTY))
				.setOrt01(StringUtils.defaultString(it0023.getOrt01(), StringUtils.EMPTY))
				.setLand1(it0023.getLand1() != null ? StringUtils.defaultString(it0023.getLand1().getLand1(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setLand1Text(it0023.getLand1() != null ? StringUtils.defaultString(it0023.getLand1().getLandx(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setBranc(it0023.getBranc() != null ? StringUtils.defaultString(it0023.getBranc().getBrsch(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setBrancText(it0023.getBranc() != null ? StringUtils.defaultString(it0023.getBranc().getBrtxt(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setTaete(it0023.getTaete() != null ? it0023.getTaete().getTaete() : 0L)
				.setTaeteText(it0023.getTaete() != null ? StringUtils.defaultString(it0023.getTaete().getLtext(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAnsvx(StringUtils.defaultString(it0023.getAnsvx(), StringUtils.EMPTY))
				.setAttachmentType(it0023.getAttachment() != null ? StringUtils.defaultString(it0023.getAttachment().getId().getSubty(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentTypeText(it0023.getAttachment() != null ? StringUtils.defaultString(it0023.getAttachment().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentPath(StringUtils.defaultString(it0023.getAttachmentPath(), StringUtils.EMPTY))
				.setPosName(StringUtils.defaultString(it0023.getPosName(), StringUtils.EMPTY))
				.setPosRoles(StringUtils.defaultString(it0023.getPosRoles(), StringUtils.EMPTY))
				.setDocumentStatus(it0023.getStatus());
		}
	}

	public IT0023ResponseWrapper(IT0023 currentData, String eventData, CommonServiceFactory serviceFactory) throws JSONException, Exception {
		this(currentData);
		IT0023ResponseWrapper itWrapper = (IT0023ResponseWrapper) new EventDataWrapper(eventData, this).getItWrapper();
		String[] ignoreProperties = EventDataWrapper.getNullPropertyNames(itWrapper);
		BeanUtils.copyProperties(itWrapper, this, ignoreProperties);
	}
	
	/**
	 * GET Employee SSN.
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	public IT0023ResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	public IT0023ResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}
	
	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	public IT0023ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * GET Employer.
	 * 
	 * @return
	 */
	@JsonProperty("employer")
	public String getArbgb() {
		return arbgb;
	}

	public IT0023ResponseWrapper setArbgb(String arbgb) {
		this.arbgb = arbgb;
		return this;
	}
	
	/**
	 * GET City.
	 * 
	 * @return
	 */
	@JsonProperty("city")
	public String getOrt01() {
		return ort01;
	}
	
	public IT0023ResponseWrapper setOrt01(String ort01) {
		this.ort01 = ort01;
		return this;
	}
	
	/**
	 * GET Country Key.
	 * 
	 * @return
	 */
	@JsonProperty("country_key")
	public String getLand1() {
		return land1;
	}
	
	public IT0023ResponseWrapper setLand1(String land1) {
		this.land1 = land1;
		return this;
	}
	
	/**
	 * GET Country Name.
	 * 
	 * @return
	 */
	@JsonProperty("country_name")
	public String getLand1Text() {
		return land1Text;
	}
	
	public IT0023ResponseWrapper setLand1Text(String land1Text) {
		this.land1Text = land1Text;
		return this;
	}
	
	/**
	 * GET Industry Code.
	 * 
	 * @return
	 */
	@JsonProperty("industry_code")
	public String getBranc() {
		return branc;
	}
	
	public IT0023ResponseWrapper setBranc(String branc) {
		this.branc = branc;
		return this;
	}
	
	/**
	 * GET Industry Name.
	 * 
	 * @return
	 */
	@JsonProperty("industry_name")
	public String getBrancText() {
		return brancText;
	}
	
	public IT0023ResponseWrapper setBrancText(String brancText) {
		this.brancText = brancText;
		return this;
	}
	
	/**
	 * GET Job Code.
	 * 
	 * @return
	 */
	@JsonProperty("job_code")
	public long getTaete() {
		return taete;
	}
	
	public IT0023ResponseWrapper setTaete(long taete) {
		this.taete = taete;
		return this;
	}
	
	/**
	 * GET Job Name.
	 * 
	 * @return
	 */
	@JsonProperty("job_name")
	public String getTaeteText() {
		return taeteText;
	}
	
	public IT0023ResponseWrapper setTaeteText(String taeteText) {
		this.taeteText = taeteText;
		return this;
	}
	
	/**
	 * GET Work Contract.
	 * 
	 * @return
	 */
	@JsonProperty("work_contract")
	public String getAnsvx() {
		return ansvx;
	}
	
	public IT0023ResponseWrapper setAnsvx(String ansvx) {
		this.ansvx = ansvx;
		return this;
	}
	
	/**
	 * GET Document Status.
	 * 
	 * @return
	 */
	@JsonProperty("document_status")
	public String getDocumentStatus() {
		return documentStatus;
	}
	
	public IT0023ResponseWrapper setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus.name();
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type")
	public String getAttachmentType() {
		return attachmentType;
	}
	
	public IT0023ResponseWrapper setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
		return this;
	}
	
	/**
	 * GET Attachment Type Text.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type_text")
	public String getAttachmentTypeText() {
		return attachmentTypeText;
	}
	
	public IT0023ResponseWrapper setAttachmentTypeText(String attachmentTypeText) {
		this.attachmentTypeText = attachmentTypeText;
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_path")
	public String getAttachmentPath() {
		return attachmentPath;
	}
	
	public IT0023ResponseWrapper setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
		return this;
	}
	
	@JsonProperty("position_name")
	public String getPosName() {
		return posName;
	}
	
	public IT0023ResponseWrapper setPosName(String posName) {
		this.posName = posName;
		return this;
	}
	
	@JsonProperty("position_roles")
	public String getPosRoles() {
		return posRoles;
	}
	
	public IT0023ResponseWrapper setPosRoles(String posRoles) {
		this.posRoles = posRoles;
		return this;
	}
}