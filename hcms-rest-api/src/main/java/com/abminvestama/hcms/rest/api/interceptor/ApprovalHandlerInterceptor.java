package com.abminvestama.hcms.rest.api.interceptor;

import java.time.DateTimeException;
import java.util.Collection;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.PositionRelation;
import com.abminvestama.hcms.core.model.entity.PositionRelationType;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Component("approvalHandlerInterceptor")
public class ApprovalHandlerInterceptor extends HandlerInterceptorAdapter {

	private CommonServiceFactory commonServiceFactory;
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object handler) throws Exception {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		
		Object principal = authentication.getPrincipal();
		
		if (principal instanceof String && ((String) principal).equalsIgnoreCase("anonymousUser")) {
			throw new UsernameNotFoundException("No user logged in.");
		}		
		
		String username = null;
		if (principal instanceof User) {
			username = ((User) principal).getUsername();
		} else if (principal instanceof org.springframework.security.core.userdetails.User) {
			username = ((org.springframework.security.core.userdetails.User) principal).getUsername();
		} else {
			throw new UsernameNotFoundException("No user logged in.");
		}
		
		System.out.println("ApprovalHandlerInterceptor::preHandle::username = " + username);
		System.out.println("SSN = " + request.getHeader("ssn"));
		System.out.println("Subtype = " + request.getHeader("subtype"));
		System.out.println("End Date = " + request.getHeader("endda"));
		System.out.println("Begin Date = " + request.getHeader("begda"));
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(Optional.ofNullable(username));
		
		if (!currentUser.isPresent()) {
			throw new AuthorizationException(
					"Insufficient Privileges. Please login either as an 'ADMIN' or 'SUPERIOR'!");
		}
		
		if (StringUtils.isEmpty(request.getHeader("ssn")) || request.getHeader("ssn").equals("0")) {
			throw new DataViolationException("Cannot find ssn from the request header!");
		}
		/*if (StringUtils.isEmpty(request.getHeader("infotype")) || request.getHeader("infotype").equals("0")) {
			throw new DataViolationException("Cannot find infotype from the request header!");
		}
		if (StringUtils.isBlank(request.getHeader("subtype"))) {
			throw new DataViolationException("Cannot find subtype from the request header!");
		}*/
		if (StringUtils.isBlank(request.getHeader("endda"))) {
			throw new DataViolationException("Cannot find endda from the request header!");
		}
		if (StringUtils.isBlank(request.getHeader("begda"))) {
			throw new DataViolationException("Cannot find begda from the request header!");
		}
		
		try {
			CommonDateFunction.convertDateRequestParameterIntoDate(request.getHeader("endda"));
			CommonDateFunction.convertDateRequestParameterIntoDate(request.getHeader("begda"));
		} catch (DateTimeException dte) {
			throw dte;
		}
		
		long ssn = Long.valueOf(request.getHeader("ssn"));
		
		if (currentUser.get().getEmployee().getPernr() == ssn) {
			throw new AuthorizationException("You can't approve your own data!");
		}		
		/*
		Optional<HCMSEntity> entity = commonServiceFactory.getHCMSEntityQueryService().findByEntityName(request.getHeader("infotype").toUpperCase());
		
		if (!entity.isPresent()) {
			throw new EntityNotFoundException("Cannot find Entity '" + request.getHeader("infotype") + "' !!!");
		}						
		*/
		Optional<Role> role = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		
		//StringBuffer insufficientMsg = new StringBuffer();
		
		if (!currentUser.get().getRoles().contains(role.get())) { // If user is not an 'ADMIN', then only 'SUPERIOR' of this user is allowed to do approval

			Optional<PositionRelationType> positionRelationType 
				= commonServiceFactory.getPositionRelationTypeQueryService().findByName("REPORT_TO");
			Optional<IT0001> it0001 = commonServiceFactory.getIT0001QueryService().findOneByCompositeKey(ssn, 
					CommonDateFunction.convertDateRequestParameterIntoDate(request.getHeader("endda")), 
					CommonDateFunction.convertDateRequestParameterIntoDate(request.getHeader("begda")));
			
			if (!positionRelationType.isPresent()) {
				//insufficientMsg.replace(0, insufficientMsg.length(), "Insufficient data. Employee must have a Supervisor or Admin to get an approval!");
				throw new DataViolationException("Insufficient data. Employee must have a Supervisor or Admin to get an approval!");
			}
			if (!it0001.isPresent()) {
				//insufficientMsg.replace(0, insufficientMsg.length(), "Insufficient data. Employee's position cannot be found!");
				throw new DataViolationException("Insufficient data. Employee's position cannot be found!");
			}
			Collection<PositionRelation> positionRelations = commonServiceFactory.getPositionRelationQueryService()
					.findByFromPositionAndToPositionAndRelationType(
					it0001.get().getPosition().getId(), 
					currentUser.get().getEmployee().getPosition().getId(),
					positionRelationType.get().getName());
			if (positionRelations == null || positionRelations.isEmpty()) {
				//insufficientMsg.replace(0, insufficientMsg.length(), "Insufficient Privileges ! Only 'SUPERIOR' of this user is authorized to do the approval !");
				throw new AuthorizationException("Insufficient Privileges ! Only 'SUPERIOR' of this user is authorized to do the approval !");
			}
		}		
		
		return true;
	}
}
