package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.core.model.entity.IT0077;
import com.abminvestama.hcms.core.model.entity.T505S;
import com.abminvestama.hcms.core.model.entity.T505SKey;
import com.abminvestama.hcms.core.service.api.business.query.T505SQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0077RequestWrapper;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Component("it0077RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT0077RequestBuilderUtil {

	private T505SQueryService t505sQueryService;
	
	@Autowired
	void setT505SQueryService(T505SQueryService t505sQueryService) {
		this.t505sQueryService = t505sQueryService;
	}
	
	/**
	 * Compare IT0077 requested payload with an existing IT0077 in the database.
	 * Update the existing IT0077 data with the latest data comes from the requested payload. 
	 * 
	 * @param requestPayload requested data 
	 * @param it0077DB current existing IT0077 in the database.
	 * @return updated IT0077 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0077, IT0077RequestWrapper, String> compareAndReturnUpdatedData(IT0077RequestWrapper requestPayload, IT0077 it0077DB) {
		if (it0077DB == null) {
			it0077DB = new IT0077();
		} else {
			
			if (StringUtils.isNotBlank(requestPayload.getRacky())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getRacky(), it0077DB.getRacky().getId().getRacky())) {
					try {
						T505SKey key = new T505SKey(StringUtils.defaultString(requestPayload.getMolga(), "32"), requestPayload.getRacky());
						Optional<T505S> newT505S = t505sQueryService.findById(Optional.ofNullable(key));
						if (newT505S.isPresent()) {
							it0077DB.setRacky(newT505S.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T505S not found... cancel the update process
					}
				}
			}
			
		}
		
		return new RequestObjectComparatorContainer<>(it0077DB, requestPayload);
	}								
}