package com.abminvestama.hcms.rest.api.validator;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.abminvestama.hcms.core.model.entity.Position;
import com.abminvestama.hcms.core.model.entity.Workflow;
import com.abminvestama.hcms.core.model.entity.WorkflowStep;
import com.abminvestama.hcms.core.service.api.business.query.PositionQueryService;
import com.abminvestama.hcms.core.service.api.business.query.WorkflowQueryService;
import com.abminvestama.hcms.core.service.api.business.query.WorkflowStepQueryService;
import com.abminvestama.hcms.rest.api.dto.request.WorkflowStepRequestWrapper;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Component("workflowStepValidator")
public class WorkflowStepValidator implements Validator {
	
	private PositionQueryService positionQueryService;
	private WorkflowQueryService workflowQueryService;
	private WorkflowStepQueryService workflowStepQueryService;
	
	@Autowired
	void setPositionQueryService(PositionQueryService positionQueryService) {
		this.positionQueryService = positionQueryService;
	}
	
	@Autowired
	void setWorkflowQueryService(WorkflowQueryService workflowQueryService) {
		this.workflowQueryService = workflowQueryService;
	}
	
	@Autowired
	void setWorkflowStepQueryService(WorkflowStepQueryService workflowStepQueryService) {
		this.workflowStepQueryService = workflowStepQueryService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return WorkflowStepRequestWrapper.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		final WorkflowStepRequestWrapper request = (WorkflowStepRequestWrapper) target;
		
		boolean passed = true;
		
		if (StringUtils.isBlank(request.getWorkflowId())) {
			errors.rejectValue("workflowId", null, "'workflow_id' is missing from the payload!");
			passed = false;
		} else {
			try {
				Optional<Workflow> workflow = workflowQueryService.findById(Optional.of(request.getWorkflowId()));
				if (!workflow.isPresent()) {
					errors.rejectValue("workflowId", null, "'workflow_id' " + request.getWorkflowId() + " doesn't exist!");
					passed = false;
				}
			} catch (Exception e) {
				errors.rejectValue("workflowId", null, e.getMessage());
				passed = false;
			}
		}
		
		if (StringUtils.isBlank(request.getPositionId())) {
			errors.rejectValue("positionId", null, "'position_id' is missing from the payload!");
			passed = false;
		} else {
			try {
				Optional<Position> position = positionQueryService.findById(Optional.of(request.getPositionId()));
				if (!position.isPresent()) {
					errors.rejectValue("positionId", null, "'position_id' " + request.getPositionId() + " doesn't exist!");
					passed = false;
				}
			} catch (Exception e) {
				errors.rejectValue("positionId", null, e.getMessage());
				passed = false;
			}
		}
		
		JSONObject json = new JSONObject(request);
		
		if (passed) {
			Optional<WorkflowStep> workflowStep = workflowStepQueryService.findByWorkflowIdAndSeqAndPositionId(request.getWorkflowId(), 
					(json.has("sequence_no") && !json.isNull("sequence_no") ? request.getSeq() : 1), request.getPositionId());
			if (workflowStep.isPresent()) {
				errors.rejectValue("workflowId", null, "WorkflowType Step already exists!");
				errors.rejectValue("name", null, "WorkflowType Step already exists!");
				errors.rejectValue("seq", null, "WorkflowType Step already exists!");
				errors.rejectValue("positionId", null, "WorkflowType Step already exists!");
			}
		}
	}
}