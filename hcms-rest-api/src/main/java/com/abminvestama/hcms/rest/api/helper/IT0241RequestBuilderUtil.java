package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.Attachment;
import com.abminvestama.hcms.core.model.entity.IT0241;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.business.query.AttachmentQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0241QueryService;
import com.abminvestama.hcms.core.service.util.FileAttachmentService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0241RequestWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Adjust attachment fields on EventData Json</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData: Add attachment fields</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData to handle Create</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("it0241RequestBuilderUtil")
public class IT0241RequestBuilderUtil {
	
	private IT0241QueryService it0241QueryService;
	private AttachmentQueryService attachmentQueryService;
	private FileAttachmentService fileAttachmentService;
	
	@Autowired
	void setIT0241QueryService(IT0241QueryService it0241QueryService) {
		this.it0241QueryService = it0241QueryService;
	}
	
	@Autowired
	void setAttachmentQueryService(AttachmentQueryService attachmentQueryService) {
		this.attachmentQueryService = attachmentQueryService;
	}
	
	@Autowired
	void setFileAttachmentService(FileAttachmentService fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}


	/**
	 * Compare IT0241 requested payload with an existing IT0241 in the database.
	 * Update the existing IT0241 data with the latest data comes from the requested payload. 
	 * 
	 * @param requestPayload requested data 
	 * @param it0241DB current existing IT0241 in the database.
	 * @return updated IT0241 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0241, IT0241RequestWrapper, String> compareAndReturnUpdatedData(IT0241RequestWrapper requestPayload, IT0241 it0241DB) {
		
		RequestObjectComparatorContainer<IT0241, IT0241RequestWrapper, String> objectContainer = null;
		
		if (it0241DB == null) {
			it0241DB = new IT0241();
			try {
				Optional<IT0241> existingIT0241 = it0241QueryService.findOneByCompositeKey(
						requestPayload.getPernr(), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda()));
				
				if (existingIT0241.isPresent()) {
					throw new DataViolationException("Duplicate IT0241 Data for [ssn=" + requestPayload.getPernr() + ",end_date=" +
							requestPayload.getEndda() + ",begin_date=" + requestPayload.getBegda());
				}
				
				it0241DB.setId(
						new ITCompositeKeysNoSubtype(requestPayload.getPernr(), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda())));
				it0241DB.setTaxId(requestPayload.getTaxId());
				it0241DB.setMarrd(requestPayload.getMarrd());
				it0241DB.setSpben(requestPayload.getSpben());
				it0241DB.setRefno(requestPayload.getRefno());
				it0241DB.setDepnd(requestPayload.getDepnd());
				it0241DB.setRdate(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getRdate()));
				
				it0241DB.setSeqnr(1L);
				it0241DB.setStatus(DocumentStatus.INITIAL_DRAFT);
				
				it0241DB = saveAttachment(it0241DB, requestPayload);
				
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			
			objectContainer = new RequestObjectComparatorContainer<>(it0241DB, requestPayload);
			
		} else {
			
			if (StringUtils.isNotBlank(requestPayload.getTaxId())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getTaxId(), it0241DB.getTaxId())) {
					it0241DB.setTaxId(requestPayload.getTaxId());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getMarrd())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getMarrd(), it0241DB.getMarrd())) {
					it0241DB.setMarrd(requestPayload.getMarrd());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getSpben())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getSpben(), it0241DB.getSpben())) {
					it0241DB.setSpben(requestPayload.getSpben());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getRefno())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getRefno(), it0241DB.getRefno())) {
					it0241DB.setRefno(requestPayload.getRefno());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getDepnd())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDepnd(), it0241DB.getDepnd())) {
					it0241DB.setDepnd(requestPayload.getDepnd());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getRdate())) {
				if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getRdate()), it0241DB.getRdate())) {
					it0241DB.setRdate(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getRdate()));
					requestPayload.setDataChanged(true);
				}
			}
			
			it0241DB.setSeqnr(it0241DB.getSeqnr().longValue() + 1);
			try {
				it0241DB = saveAttachment(it0241DB, requestPayload);
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			it0241DB.setSeqnr(it0241DB.getSeqnr().longValue() - 1);
			
			objectContainer = new RequestObjectComparatorContainer<>(it0241DB, requestPayload);
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = "";
		try {
			jsonString = objectMapper.writeValueAsString(requestPayload);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}		
		
		//JSONObject json = new JSONObject(requestPayload);
		JSONObject json = new JSONObject(jsonString);
		// Adjust attachment fields on EventData Json
		json.remove("attachment1_type");
		json.remove("attachment1_base64");
		json.remove("attachment1_filename");
		json.remove("subtype");
		if (it0241DB.getAttachment() != null)
			json.put("attachment1_type", it0241DB.getAttachment().getId().getSubty());
		if (it0241DB.getAttachmentPath() != null)
		    json.put("attachment1_path", it0241DB.getAttachmentPath());
		// ******************************************
		
		//if (it0241DB.getStatus() != null)
			//json.put("prev_document_status", it0241DB.getStatus().name());
		
		objectContainer.setEventData(json.toString());
		
		return objectContainer;
	}
	
	private IT0241 saveAttachment(IT0241 it0241DB, IT0241RequestWrapper requestPayload) throws Exception {
		
		T591SKey attachmentKey = new T591SKey(SAPInfoType.TAX_DATA.infoType(), requestPayload.getAttach1Type());
		Optional<Attachment> attachment = attachmentQueryService.findById(Optional.ofNullable(attachmentKey));
		it0241DB.setAttachment(attachment.isPresent() ? attachment.get() : null);
		String base64 = requestPayload.getAttach1Content();
		String filename = requestPayload.getAttach1Filename();
		if (StringUtils.isNotEmpty(base64) && StringUtils.isNotEmpty(filename)) {
			String ext = filename.split("\\.")[(filename.split("\\.")).length-1];
			String formatFilename = it0241DB.getId().toStringId(SAPInfoType.TAX_DATA.infoType()).
					concat("_").concat(it0241DB.getSeqnr().toString()).concat(".").concat(ext);
		    String path = fileAttachmentService.save(formatFilename, base64);
		    it0241DB.setAttachmentPath(path);
		    requestPayload.setDataChanged(true);
		}
		
		return it0241DB;
		
	}
}