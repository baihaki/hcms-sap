package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.entity.Phdat;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.PhdatQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.PhdatResponseWrapper;

/**
 * 
 * @author wijanarko (wijanarko777@gmx.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@RestController
@RequestMapping("/api/v1/public_holiday")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class PhdatController extends AbstractResource {

	private HCMSEntityQueryService hcmsEntityQueryService;
	private EventLogCommandService eventLogCommandService;
	private UserQueryService userQueryService;
	private PhdatQueryService phdatQueryService;

	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}

	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}

	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	void setPhdatService(PhdatQueryService phdatQueryService) {
		this.phdatQueryService = phdatQueryService;
	}

	/**
	 * @author wijanarko (wijanarko777@gmx.com)
	 * 
	 * Get Public Holiday, default erdat = sysdate.
	 * 
	 * @param pernr
	 * @param criteria
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/criteria/{pernr}", headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<PhdatResponseWrapper>> findByCriteria(
			@PathVariable Long pernr,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "phType", required = false) String phType,
			@RequestParam(value = "orderBy", required = false, defaultValue = "erdat") String orderBy,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		
		ArrayData<PhdatResponseWrapper> bunchOfPhdat = new ArrayData<>();
		APIResponseWrapper<ArrayData<PhdatResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfPhdat);

		//VALIDATION
		//Check validation user request
		Optional<User> user = userQueryService.findByPernr(pernr);
		if(!user.isPresent()) {
			response.setMessage("No Data Found");
		}
		//Default startDate & endDate = SysDate
		Date sDate = startDate == null || startDate.isEmpty() ? new Date() : new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
		Date eDate = endDate == null || endDate.isEmpty() ? new Date() : new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
		
		try {
			List<Phdat> listOfPhdat = new ArrayList<>();
			Page<Phdat> data = phdatQueryService.fetchAllByStartDateAndEndDateWithOrdeBy(sDate, eDate, phType, orderBy, pageNumber);
			if (data != null && data.hasContent()) {
				listOfPhdat = data.getContent();
				List<PhdatResponseWrapper> records = new ArrayList<>();
				for (Phdat phdat : listOfPhdat) {
					records.add(new PhdatResponseWrapper(phdat));
				}
				bunchOfPhdat.setItems(records.toArray(new PhdatResponseWrapper[records.size()]));
			} else {
				response.setMessage("No Data Found");
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}
		response.add(linkTo(methodOn(PhdatController.class).findByCriteria(pernr, startDate, endDate, phType, orderBy, pageNumber)).withSelfRel());
		return response;
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		// binder.addValidators(itValidator);
	}
}