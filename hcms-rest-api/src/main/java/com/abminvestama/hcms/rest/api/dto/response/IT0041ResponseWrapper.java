package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.IT0041;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Fix: check for null object in setting dar02-dar12 values</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class IT0041ResponseWrapper extends ResourceSupport {

	private long pernr;
	private Date endda;
	private Date begda;
	
	private long seqnr;
	
	private String dar01;
	private String dar01Text;
	private Date dat01;
	
	private String dar02;
	private String dar02Text;
	private Date dat02;

	private String dar03;
	private String dar03Text;
	private Date dat03;
	
	private String dar04;
	private String dar04Text;
	private Date dat04;
	
	private String dar05;
	private String dar05Text;
	private Date dat05;
	
	private String dar06;
	private String dar06Text;
	private Date dat06;
	
	private String dar07;
	private String dar07Text;
	private Date dat07;
	
	private String dar08;
	private String dar08Text;
	private Date dat08;
	
	private String dar09;
	private String dar09Text;
	private Date dat09;
	
	private String dar10;
	private String dar10Text;
	private Date dat10;
	
	private String dar11;
	private String dar11Text;
	private Date dat11;
	
	private String dar12;
	private String dar12Text;
	private Date dat12;	
	
	private IT0041ResponseWrapper() {}
	
	public IT0041ResponseWrapper(IT0041 it0041) {
		if (it0041 == null) {
			new IT0041ResponseWrapper();
		} else {
			this
			.setPernr(it0041.getId().getPernr())
			.setEndda(it0041.getId().getEndda()).setBegda(it0041.getId().getBegda())
			.setSeqnr(it0041.getSeqnr() != null ? it0041.getSeqnr().longValue() : 0L)
			.setDar01(StringUtils.defaultString(it0041.getDar01().getDatar(), null))
			.setDar01Text(StringUtils.defaultString(it0041.getDar01().getDtext(), null))
			.setDat01(it0041.getDat01())
			.setDar02(StringUtils.defaultString(it0041.getDar02() != null ? it0041.getDar02().getDatar() : null, null))
			.setDar02Text(StringUtils.defaultString(it0041.getDar02() != null ? it0041.getDar02().getDtext() : null, null))
			.setDat02(it0041.getDat02())
			.setDar03(StringUtils.defaultString(it0041.getDar03() != null ? it0041.getDar03().getDatar() : null, null))
			.setDar03Text(StringUtils.defaultString(it0041.getDar03() != null ? it0041.getDar03().getDtext() : null, null))
			.setDat03(it0041.getDat03())
			.setDar04(StringUtils.defaultString(it0041.getDar04() != null ? it0041.getDar04().getDatar() : null, null))
			.setDar04Text(StringUtils.defaultString(it0041.getDar04() != null ? it0041.getDar04().getDtext() : null, null))
			.setDat04(it0041.getDat04())
			.setDar05(StringUtils.defaultString(it0041.getDar05() != null ? it0041.getDar05().getDatar() : null, null))
			.setDar05Text(StringUtils.defaultString(it0041.getDar05() != null ? it0041.getDar05().getDtext() : null, null))
			.setDat05(it0041.getDat05())
			.setDar06(StringUtils.defaultString(it0041.getDar06() != null ? it0041.getDar06().getDatar() : null, null))
			.setDar06Text(StringUtils.defaultString(it0041.getDar06() != null ? it0041.getDar06().getDtext() : null, null))
			.setDat06(it0041.getDat06())
			.setDar07(StringUtils.defaultString(it0041.getDar07() != null ? it0041.getDar07().getDatar() : null, null))
			.setDar07Text(StringUtils.defaultString(it0041.getDar07() != null ? it0041.getDar07().getDtext() : null, null))
			.setDat07(it0041.getDat07())
			.setDar08(StringUtils.defaultString(it0041.getDar08() != null ? it0041.getDar08().getDatar() : null, null))
			.setDar08Text(StringUtils.defaultString(it0041.getDar08() != null ? it0041.getDar08().getDtext() : null, null))
			.setDat08(it0041.getDat08())
			.setDar09(StringUtils.defaultString(it0041.getDar09() != null ? it0041.getDar09().getDatar() : null, null))
			.setDar09Text(StringUtils.defaultString(it0041.getDar09() != null ? it0041.getDar09().getDtext() : null, null))
			.setDat09(it0041.getDat09())
			.setDar10(StringUtils.defaultString(it0041.getDar10() != null ? it0041.getDar10().getDatar() : null, null))
			.setDar10Text(StringUtils.defaultString(it0041.getDar10() != null ? it0041.getDar10().getDtext() : null, null))
			.setDat10(it0041.getDat10())
			.setDar11(StringUtils.defaultString(it0041.getDar11() != null ? it0041.getDar11().getDatar() : null, null))
			.setDar11Text(StringUtils.defaultString(it0041.getDar11() != null ? it0041.getDar11().getDtext() : null, null))
			.setDat11(it0041.getDat11())
			.setDar12(StringUtils.defaultString(it0041.getDar12() != null ? it0041.getDar12().getDatar() : null, null))
			.setDar12Text(StringUtils.defaultString(it0041.getDar12() != null ? it0041.getDar12().getDtext() : null, null))
			.setDat12(it0041.getDat12());			
		}
	}
	
	/**
	 * GET Employee ID/SSN.
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	private IT0041ResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	private IT0041ResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}
	
	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	private IT0041ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * GET Infotype Record No.
	 * 
	 * @return
	 */
	@JsonProperty("infotype_record_no")
	public long getSeqnr() {
		return seqnr;
	}
	
	private IT0041ResponseWrapper setSeqnr(long seqnr) {
		this.seqnr = seqnr;
		return this;
	}

	@JsonProperty("date_type1")
	public String getDar01() {
		return dar01;
	}

	private IT0041ResponseWrapper setDar01(String dar01) {
		this.dar01 = dar01;
		return this;
	}

	@JsonProperty("date_type1_name")
	public String getDar01Text() {
		return dar01Text;
	}

	private IT0041ResponseWrapper setDar01Text(String dar01Text) {
		this.dar01Text = dar01Text;
		return this;
	}

	@JsonProperty("date_type1_date")
	public Date getDat01() {
		return dat01;
	}

	private IT0041ResponseWrapper setDat01(Date dat01) {
		this.dat01 = dat01;
		return this;
	}

	@JsonProperty("date_type2")
	public String getDar02() {
		return dar02;
	}

	private IT0041ResponseWrapper setDar02(String dar02) {
		this.dar02 = dar02;
		return this;
	}

	@JsonProperty("date_type2_name")
	public String getDar02Text() {
		return dar02Text;
	}

	private IT0041ResponseWrapper setDar02Text(String dar02Text) {
		this.dar02Text = dar02Text;
		return this;
	}

	@JsonProperty("date_type2_date")
	public Date getDat02() {
		return dat02;
	}

	private IT0041ResponseWrapper setDat02(Date dat02) {
		this.dat02 = dat02;
		return this;
	}

	@JsonProperty("date_type3")
	public String getDar03() {
		return dar03;
	}

	private IT0041ResponseWrapper setDar03(String dar03) {
		this.dar03 = dar03;
		return this;
	}

	@JsonProperty("date_type3_name")
	public String getDar03Text() {
		return dar03Text;
	}

	private IT0041ResponseWrapper setDar03Text(String dar03Text) {
		this.dar03Text = dar03Text;
		return this;
	}

	@JsonProperty("date_type3_date")
	public Date getDat03() {
		return dat03;
	}

	private IT0041ResponseWrapper setDat03(Date dat03) {
		this.dat03 = dat03;
		return this;
	}

	@JsonProperty("date_type4")
	public String getDar04() {
		return dar04;
	}

	private IT0041ResponseWrapper setDar04(String dar04) {
		this.dar04 = dar04;
		return this;
	}

	@JsonProperty("date_type4_name")
	public String getDar04Text() {
		return dar04Text;
	}

	private IT0041ResponseWrapper setDar04Text(String dar04Text) {
		this.dar04Text = dar04Text;
		return this;
	}

	@JsonProperty("date_type4_date")
	public Date getDat04() {
		return dat04;
	}

	private IT0041ResponseWrapper setDat04(Date dat04) {
		this.dat04 = dat04;
		return this;
	}

	@JsonProperty("date_type5")
	public String getDar05() {
		return dar05;
	}

	private IT0041ResponseWrapper setDar05(String dar05) {
		this.dar05 = dar05;
		return this;
	}

	@JsonProperty("date_type5_name")
	public String getDar05Text() {
		return dar05Text;
	}

	private IT0041ResponseWrapper setDar05Text(String dar05Text) {
		this.dar05Text = dar05Text;
		return this;
	}

	@JsonProperty("date_type5_date")
	public Date getDat05() {
		return dat05;
	}

	private IT0041ResponseWrapper setDat05(Date dat05) {
		this.dat05 = dat05;
		return this;
	}

	@JsonProperty("date_type6")
	public String getDar06() {
		return dar06;
	}

	private IT0041ResponseWrapper setDar06(String dar06) {
		this.dar06 = dar06;
		return this;
	}

	@JsonProperty("date_type6_name")
	public String getDar06Text() {
		return dar06Text;
	}

	private IT0041ResponseWrapper setDar06Text(String dar06Text) {
		this.dar06Text = dar06Text;
		return this;
	}

	@JsonProperty("date_type6_date")
	public Date getDat06() {
		return dat06;
	}

	private IT0041ResponseWrapper setDat06(Date dat06) {
		this.dat06 = dat06;
		return this;
	}

	@JsonProperty("date_type7")
	public String getDar07() {
		return dar07;
	}

	private IT0041ResponseWrapper setDar07(String dar07) {
		this.dar07 = dar07;
		return this;
	}

	@JsonProperty("date_type7_name")
	public String getDar07Text() {
		return dar07Text;
	}

	private IT0041ResponseWrapper setDar07Text(String dar07Text) {
		this.dar07Text = dar07Text;
		return this;
	}

	@JsonProperty("date_type7_date")
	public Date getDat07() {
		return dat07;
	}

	private IT0041ResponseWrapper setDat07(Date dat07) {
		this.dat07 = dat07;
		return this;
	}

	@JsonProperty("date_type8")
	public String getDar08() {
		return dar08;
	}

	private IT0041ResponseWrapper setDar08(String dar08) {
		this.dar08 = dar08;
		return this;
	}

	@JsonProperty("date_type8_name")
	public String getDar08Text() {
		return dar08Text;
	}

	private IT0041ResponseWrapper setDar08Text(String dar08Text) {
		this.dar08Text = dar08Text;
		return this;
	}

	@JsonProperty("date_type8_date")
	public Date getDat08() {
		return dat08;
	}

	private IT0041ResponseWrapper setDat08(Date dat08) {
		this.dat08 = dat08;
		return this;
	}

	@JsonProperty("date_type9")
	public String getDar09() {
		return dar09;
	}

	private IT0041ResponseWrapper setDar09(String dar09) {
		this.dar09 = dar09;
		return this;
	}

	@JsonProperty("date_type9_name")
	public String getDar09Text() {
		return dar09Text;
	}

	private IT0041ResponseWrapper setDar09Text(String dar09Text) {
		this.dar09Text = dar09Text;
		return this;
	}

	@JsonProperty("date_type9_date")
	public Date getDat09() {
		return dat09;
	}

	private IT0041ResponseWrapper setDat09(Date dat09) {
		this.dat09 = dat09;
		return this;
	}

	@JsonProperty("date_type10")
	public String getDar10() {
		return dar10;
	}

	private IT0041ResponseWrapper setDar10(String dar10) {
		this.dar10 = dar10;
		return this;
	}

	@JsonProperty("date_type10_name")
	public String getDar10Text() {
		return dar10Text;
	}

	private IT0041ResponseWrapper setDar10Text(String dar10Text) {
		this.dar10Text = dar10Text;
		return this;
	}

	@JsonProperty("date_type10_date")
	public Date getDat10() {
		return dat10;
	}

	private IT0041ResponseWrapper setDat10(Date dat10) {
		this.dat10 = dat10;
		return this;
	}

	@JsonProperty("date_type11")
	public String getDar11() {
		return dar11;
	}

	private IT0041ResponseWrapper setDar11(String dar11) {
		this.dar11 = dar11;
		return this;
	}

	@JsonProperty("date_type11_name")
	public String getDar11Text() {
		return dar11Text;
	}

	private IT0041ResponseWrapper setDar11Text(String dar11Text) {
		this.dar11Text = dar11Text;
		return this;
	}

	@JsonProperty("date_type11_date")
	public Date getDat11() {
		return dat11;
	}

	private IT0041ResponseWrapper setDat11(Date dat11) {
		this.dat11 = dat11;
		return this;
	}

	@JsonProperty("date_type12")
	public String getDar12() {
		return dar12;
	}

	private IT0041ResponseWrapper setDar12(String dar12) {
		this.dar12 = dar12;
		return this;
	}

	@JsonProperty("date_type12_name")
	public String getDar12Text() {
		return dar12Text;
	}

	private IT0041ResponseWrapper setDar12Text(String dar12Text) {
		this.dar12Text = dar12Text;
		return this;
	}

	@JsonProperty("date_type12_date")
	public Date getDat12() {
		return dat12;
	}

	private IT0041ResponseWrapper setDat12(Date dat12) {
		this.dat12 = dat12;
		return this;
	}
}