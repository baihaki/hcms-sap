package com.abminvestama.hcms.rest.api.dto.response;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.Workflow;
import com.abminvestama.hcms.rest.api.controller.WorkflowController;
import com.abminvestama.hcms.rest.api.model.constant.HCMSResourceIdentifier;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@JsonInclude(Include.NON_NULL)
public class WorkflowResponseWrapper extends ResourceSupport {

	public static final String RESOURCE = HCMSResourceIdentifier.WORKFLOW.label();
	
	private String uuid;
	private String name;
	private String description;
	private AuthorResponseWrapper createdBy;
	private AuthorResponseWrapper updatedBy;
	private Link link;
	
	public WorkflowResponseWrapper(Workflow workflow) {
		this.uuid = workflow.getId();
		this.name = workflow.getName();
		this.description = workflow.getDescription();
		try {
			this.link = linkTo(methodOn(WorkflowController.class).fetchById(workflow.getId()))
				.withRel(RESOURCE);
		} catch (Exception e) {}
	}
	
	@JsonProperty("id")
	public String getUuid() {
		return uuid;
	}
	
	@JsonProperty("workflow_name")
	public String getName() {
		return name;
	}
	
	@JsonProperty("workflow_description")
	public String getDescription() {
		return description;
	}
	
	@JsonProperty("created_by")
	public AuthorResponseWrapper getCreatedBy() {
		return createdBy;
	}
	
	public WorkflowResponseWrapper setCreatedBy(Link userLink) {
		AuthorResponseWrapper createdBy = new AuthorResponseWrapper(userLink);
		this.createdBy = createdBy;
		return this;
	}
	
	public WorkflowResponseWrapper setCreatedBy(User user, Link userLink) {
		AuthorResponseWrapper createdBy = new AuthorResponseWrapper(user, userLink);
		this.createdBy = createdBy;
		return this;
	}
	
	@JsonProperty("updated_by")
	public AuthorResponseWrapper getUpdatedBy() {
		return updatedBy;
	}
	
	public WorkflowResponseWrapper setUpdatedBy(Link userLink) {
		AuthorResponseWrapper updatedBy = new AuthorResponseWrapper(userLink);
		this.updatedBy = updatedBy;
		return this;
	}
	
	public WorkflowResponseWrapper setUpdatedBy(User user, Link userLink) {
		AuthorResponseWrapper updatedBy = new AuthorResponseWrapper(user, userLink);
		this.updatedBy = updatedBy;
		return this;
	}
	
	@JsonProperty("href")
	public Link getLink() {
		return link;
	}	
}