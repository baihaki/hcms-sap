package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class ZmedCostypeResponseWrapper extends ResourceSupport {

	private Date datab;
	private Date datbi;
	
	private String bukrs;
	private String kodemed;
	private String kodecos;
	private String descr;
	
	private ZmedCostypeResponseWrapper() {}
	
	public ZmedCostypeResponseWrapper(ZmedCostype zmedCostype) {
		if (zmedCostype == null) {
			new ZmedCostypeResponseWrapper();
		} else {
			this.setDatab(zmedCostype.getDatab())
			.setDatbi(zmedCostype.getDatbi())			
			.setBukrs(StringUtils.defaultString(zmedCostype.getBukrs().getBukrs(), StringUtils.EMPTY))
			.setKodecos(StringUtils.defaultString(zmedCostype.getKodecos(), StringUtils.EMPTY))
			.setKodemed(StringUtils.defaultString(zmedCostype.getKodemed(), StringUtils.EMPTY))
			.setDescr(StringUtils.defaultString(zmedCostype.getDescr(), StringUtils.EMPTY));			
		}
	}
	


	
	
	@JsonProperty("company_code")
	public String getBukrs() {
		return bukrs;
	}
	
	private ZmedCostypeResponseWrapper setBukrs(String bukrs) {
		this.bukrs = bukrs;
		return this;
	}
	


	
	
	@JsonProperty("medical_type")
	public String getKodemed() {
		return kodemed;
	}
	
	private ZmedCostypeResponseWrapper setKodemed(String kodemed) {
		this.kodemed = kodemed;
		return this;
	}
	


	
	
	@JsonProperty("cost_type")
	public String getKodecos() {
		return kodecos;
	}
	
	private ZmedCostypeResponseWrapper setKodecos(String kodecos) {
		this.kodecos = kodecos;
		return this;
	}
	
	
	@JsonProperty("description")
	public String getDescr() {
		return descr;
	}
	
	private ZmedCostypeResponseWrapper setDescr(String descr) {
		this.descr = descr;
		return this;
	}
	


	
	
	@JsonProperty("valid_from_date")
	public Date getDatab() {
		return datab;
	}
	
	private ZmedCostypeResponseWrapper setDatab(Date datab) {
		this.datab = datab;
		return this;
	}
	


	
	
	@JsonProperty("valid_to_date")
	public Date getDatbi() {
		return datbi;
	}
	
	private ZmedCostypeResponseWrapper setDatbi(Date datbi) {
		this.datbi = datbi;
		return this;
	}
}

