package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.IT0040;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@JsonInclude(Include.NON_NULL)
public class IT0040ResponseWrapper extends ResourceSupport {
	
	private long pernr;
	private String subty;
	private Date endda;
	private Date begda;
	
	private String subtyText;
	
	private String itxex;
	private String leihg;
	private String leihgText;
	private double anzkl;
	private String zeinh;
	private String zeinhText;
	private String lobnr;
	
	private IT0040ResponseWrapper() {}
	
	public IT0040ResponseWrapper(IT0040 it0040) {
		if (it0040 == null) {
			new IT0040ResponseWrapper();
		} else {
			this
			.setPernr(it0040.getId().getPernr())
			.setSubty(it0040.getSubty() != null ? 
					StringUtils.defaultString(it0040.getSubty().getId() != null ? 
							it0040.getSubty().getId().getSubty() : StringUtils.EMPTY, StringUtils.EMPTY) 
					: it0040.getId().getSubty())
			.setEndda(it0040.getId().getEndda()).setBegda(it0040.getId().getBegda())
			.setSubtyText(it0040.getSubty() != null ? StringUtils.defaultString(it0040.getSubty().getStext(), StringUtils.EMPTY) 
					: StringUtils.defaultString(it0040.getId().getSubty(), StringUtils.EMPTY))
			.setItxex(StringUtils.defaultString(it0040.getItxex(), StringUtils.EMPTY))
			.setLeihg(StringUtils.defaultString(it0040.getLeihg() != null ? it0040.getLeihg().getId().getSubty(): StringUtils.EMPTY, StringUtils.EMPTY))
			.setLeihgText(StringUtils.defaultString(it0040.getLeihg() != null ? it0040.getLeihg().getStext() : StringUtils.EMPTY, StringUtils.EMPTY))
			.setAnzkl(it0040.getAnzkl() != null ? it0040.getAnzkl().doubleValue() : 0.0)
			.setZeinh(StringUtils.defaultString(it0040.getZeinh() != null ? it0040.getZeinh().getZeinh() : StringUtils.EMPTY, StringUtils.EMPTY))
			.setZeinhText(StringUtils.defaultString(it0040.getZeinh() != null ? it0040.getZeinh().getEtext() : StringUtils.EMPTY, StringUtils.EMPTY))
			.setLobnr(StringUtils.defaultString(it0040.getLobnr(), StringUtils.EMPTY));					
		}
	}
	
	/**
	 * GET Employee SSN.
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	private IT0040ResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	@JsonProperty("subtype")
	public String getSubty() {
		return subty;
	}
	
	private IT0040ResponseWrapper setSubty(String subty) {
		this.subty = subty;
		return this;
	}
	
	/**
	 * GET Subtype Name/Text.
	 * 
	 * @return
	 */
	@JsonProperty("subtype_text")
	public String getSubtyText() {
		return subtyText;
	}
	
	private IT0040ResponseWrapper setSubtyText(String subtyText) {
		this.subtyText = subtyText;
		return this;
	}
	
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	private IT0040ResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}
	
	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	private IT0040ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * GET Text Exists.
	 * 
	 * @return
	 */
	@JsonProperty("text_exists")
	public String getItxex() {
		return itxex;
	}
	
	private IT0040ResponseWrapper setItxex(String itxex) {
		this.itxex = itxex;
		return this;
	}
	
	/**
	 * GET Object on Loan.
	 * 
	 * @return
	 */
	@JsonProperty("object_on_loan")
	public String getLeihg() {
		return leihg;
	}
	
	private IT0040ResponseWrapper setLeihg(String leihg) {
		this.leihg = leihg;
		return this;
	}
	
	/**
	 * GET Object on Loan (Text/Name).
	 * 
	 * @return
	 */
	@JsonProperty("object_on_loan_text")
	public String getLeihgText() {
		return leihgText;
	}
	
	private IT0040ResponseWrapper setLeihgText(String leihgText) {
		this.leihgText = leihgText;
		return this;
	}
	
	/**
	 * GET Number /unit.
	 * 
	 * @return
	 */
	@JsonProperty("number_per_unit")
	public double getAnzkl() {
		return anzkl;
	}
	
	private IT0040ResponseWrapper setAnzkl(double anzkl) {
		this.anzkl = anzkl;
		return this;
	}
	
	/***
	 * GET Unit of time/meas.
	 * 
	 * @return
	 */
	@JsonProperty("unit_of_time")
	public String getZeinh() {
		return zeinh;
	}
	
	private IT0040ResponseWrapper setZeinh(String zeinh) {
		this.zeinh = zeinh;
		return this;
	}
	
	/**
	 * GET Unit of time/meas. (Text/Name).
	 * 
	 * @return
	 */
	@JsonProperty("unit_of_time_text")
	public String getZeinhText() {
		return zeinhText;
	}
	
	private IT0040ResponseWrapper setZeinhText(String zeinhText) {
		this.zeinhText = zeinhText;
		return this;
	}
	
	/**
	 * GET Loan Object Number.
	 * 
	 * @return
	 */
	@JsonProperty("loan_object_number")
	public String getLobnr() {
		return lobnr;
	}
	
	private IT0040ResponseWrapper setLobnr(String lobnr) {
		this.lobnr = lobnr;
		return this;
	}
}