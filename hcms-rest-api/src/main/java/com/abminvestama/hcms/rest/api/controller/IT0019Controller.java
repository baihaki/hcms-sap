package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0019;
import com.abminvestama.hcms.core.model.entity.IT0019;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0019CommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0019QueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.helper.IT0019SoapObject;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0019RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.IT0019ResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.IT0019RequestBuilderUtil;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@RestController
@RequestMapping("/api/v1/task_monitoring")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class IT0019Controller extends AbstractResource {

	private IT0019CommandService it0019CommandService;
	private IT0019QueryService it0019QueryService;
	
	

	private HCMSEntityQueryService hcmsEntityQueryService;
	private EventLogCommandService eventLogCommandService;
	
	private IT0019RequestBuilderUtil it0019RequestBuilderUtil;
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	

	private MuleConsumerService muleConsumerService;
	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	
	@Autowired
	void setIT0019CommandService(IT0019CommandService it0019CommandService) {
		this.it0019CommandService = it0019CommandService;
	}
	
	@Autowired
	void setIT0019QueryService(IT0019QueryService it0019QueryService) {
		this.it0019QueryService = it0019QueryService;
	}
	
	@Autowired
	void setIT0019RequestBuilderUtil(IT0019RequestBuilderUtil it0019RequestBuilderUtil) {
		this.it0019RequestBuilderUtil = it0019RequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0019ResponseWrapper>> findOneByPernr(@PathVariable String pernr) throws Exception {
		ArrayData<IT0019ResponseWrapper> bunchOfIT0019 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0019ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0019);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0019> listOfIT0019 = it0019QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			
			if (listOfIT0019.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0019ResponseWrapper> records = new ArrayList<>();
				for (IT0019 it0019 : listOfIT0019) {
					records.add(new IT0019ResponseWrapper(it0019));
				}
				bunchOfIT0019.setItems(records.toArray(new IT0019ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0019Controller.class).findOneByPernr(pernr)).withSelfRel());
		return response;
	}		
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated IT0019RequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			IT0019ResponseWrapper responseWrapper = new IT0019ResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					
					Optional<HCMSEntity> it0019Entity = hcmsEntityQueryService.findByEntityName(IT0019.class.getSimpleName());
					if (!it0019Entity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + IT0019.class.getSimpleName() + "' !!!");
					}
					
					
					Optional<IT0019> it0019 = it0019QueryService.findOneByCompositeKey(request.getPernr(), request.getSubty(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
					if (!it0019.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					RequestObjectComparatorContainer<IT0019, IT0019RequestWrapper, String> updatedContainer = it0019RequestBuilderUtil.compareAndReturnUpdatedData(request, it0019.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
					if (updatedContainer.getEntity().isPresent()) {
						updatedContainer.getEntity().get().setSeqnr(it0019.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
						it0019 = it0019CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}
				
					if (!it0019.isPresent()) {
						throw new CannotPersistException("Cannot update IT0019 (Monitoring of Tasks) data. Please check your data!");
					}
				
					

					StringBuilder key = new StringBuilder("pernr=" + it0019.get().getId().getPernr());
					key.append(",infty=" + it0019.get().getId().getInfty());
					key.append(",subty=" + it0019.get().getId().getSubty());
					key.append(",endda=" + it0019.get().getId().getEndda());
					key.append(",begda=" + it0019.get().getId().getBegda());
					EventLog eventLog = new EventLog(it0019Entity.get(), key.toString(), OperationType.UPDATE, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					
					

					//IT0019SoapObject it0019SoapObject = new IT0019SoapObject(updatedContainer.get()); 
					//muleConsumerService.postToSAP(BAPIFunction.IT0019_CREATE.service(), "taskMonitoring", it0019SoapObject);
					
					
					responseWrapper = new IT0019ResponseWrapper(it0019.get());
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(IT0019Controller.class).edit(request, result)).withSelfRel());
		return response;
	}	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(itValidator);
	}	
}