package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0242;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0242CommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0242QueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.helper.IT0242SoapObject;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0242RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.IT0242ResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.IT0242RequestBuilderUtil;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/bpjs_info")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class IT0242Controller extends AbstractResource {

	private IT0242CommandService it0242CommandService;
	private IT0242QueryService it0242QueryService;
	
	private IT0242RequestBuilderUtil it0242RequestBuilderUtil;
	
	private Validator itValidator;
	
	private UserQueryService userQueryService;
	
	private HCMSEntityQueryService hcmsEntityQueryService;
	private EventLogCommandService eventLogCommandService;
	private CommonServiceFactory commonServiceFactory;
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}

	private MuleConsumerService muleConsumerService;
	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}
	
	@Autowired
	void setIT0242CommandService(IT0242CommandService it0242CommandService) {
		this.it0242CommandService = it0242CommandService;
	}
	
	@Autowired
	void setIT0242QueryService(IT0242QueryService it0242QueryService) {
		this.it0242QueryService = it0242QueryService;
	}
	
	@Autowired
	void setIT0242RequestBuilderUtil(IT0242RequestBuilderUtil it0242RequestBuilderUtil) {
		this.it0242RequestBuilderUtil = it0242RequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	@Secured({AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/status/{status}/page/{pageNumber}", 
	produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0242ResponseWrapper>> findByDocumentStatus(@PathVariable String status, 
			@PathVariable int pageNumber) throws Exception {
		ArrayData<IT0242ResponseWrapper> bunchOfIT0242 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0242ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0242);
		
		try {
			Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
			
			Page<IT0242> it0242Records = it0242QueryService.findByStatus(status, pageNumber, currentUser.get().getEmployee());
			
			if (it0242Records == null || !it0242Records.hasContent()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0242ResponseWrapper> records = new ArrayList<>();
				
				it0242Records.forEach(it0242 -> {
					records.add(new IT0242ResponseWrapper(it0242));
				});
				
				bunchOfIT0242.setItems(records.toArray(new IT0242ResponseWrapper[records.size()]));
			}
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0242Controller.class).findByDocumentStatus(status, pageNumber)).withSelfRel());
		
		return response;
	}
		
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0242ResponseWrapper>> findByPernr(@PathVariable String pernr, 
			@RequestParam(value = "status", required = false) String status, 
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		ArrayData<IT0242ResponseWrapper> bunchOfIT0242 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0242ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0242);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0242> listOfIT0242 = new ArrayList<>();
			
			if (StringUtils.isNotEmpty(status)) {
				Page<IT0242> data = it0242QueryService.findByPernrAndStatus(numOfPernr, status, pageNumber);
				if (data.hasContent()) {
					listOfIT0242 = data.getContent();
				}
			} else {
				listOfIT0242 = it0242QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			}
			
			if (listOfIT0242.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0242ResponseWrapper> records = new ArrayList<>();
				for (IT0242 it0242 : listOfIT0242) {
					records.add(new IT0242ResponseWrapper(it0242));
				}
				bunchOfIT0242.setItems(records.toArray(new IT0242ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0242Controller.class).findByPernr(pernr, status, pageNumber)).withSelfRel());
		return response;
	}
	/*
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0242ResponseWrapper>> findOneByPernr(@PathVariable String pernr) throws Exception {
		ArrayData<IT0242ResponseWrapper> bunchOfIT0242 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0242ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0242);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0242> listOfIT0242 = it0242QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			
			if (listOfIT0242.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				
				List<IT0242ResponseWrapper> records = new ArrayList<>();
				for (IT0242 it0242 : listOfIT0242) {
					records.add(new IT0242ResponseWrapper(it0242));
				}				
				bunchOfIT0242.setItems(records.toArray(new IT0242ResponseWrapper[records.size()]));				
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0242Controller.class).findOneByPernr(pernr)).withSelfRel());
		return response;
	}		
	*/
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated IT0242RequestWrapper request, BindingResult result)
			throws CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> it0242Entity = hcmsEntityQueryService.findByEntityName(IT0242.class.getSimpleName());
			if (!it0242Entity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + IT0242.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<IT0242, IT0242RequestWrapper, String> newObjectContainer 
					= it0242RequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				/*
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				*/
				
				Optional<IT0242> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
					newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = it0242CommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create IT0242 (Tax Info) data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("pernr=" + savedEntity.get().getId().getPernr());
				key.append(",endda=" + savedEntity.get().getId().getEndda());
				key.append(",begda=" + savedEntity.get().getId().getBegda());
				EventLog eventLog = new EventLog(it0242Entity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}

				//IT0242SoapObject it0242SoapObject = new IT0242SoapObject(savedEntity.get()); 
				//muleConsumerService.postToSAP(BAPIFunction.IT0242_CREATE.service(), BAPIFunction.IT0242_CREATE_URI.service(), it0242SoapObject);
				
				IT0242ResponseWrapper responseWrapper = new IT0242ResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;	
			} /* catch (AuthorizationException e) { 
				throw e;
			}*/ catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(IT0242Controller.class).create(request, result)).withSelfRel());		
		return response;
	}
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated IT0242RequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			IT0242ResponseWrapper responseWrapper = new IT0242ResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<HCMSEntity> it0242Entity = hcmsEntityQueryService.findByEntityName(IT0242.class.getSimpleName());
					if (!it0242Entity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + IT0242.class.getSimpleName() + "' !!!");
					}
					
					Optional<IT0242> it0242 = it0242QueryService.findOneByCompositeKey(request.getPernr(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
					if (!it0242.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					RequestObjectComparatorContainer<IT0242, IT0242RequestWrapper, String> updatedContainer 
						= it0242RequestBuilderUtil.compareAndReturnUpdatedData(request, it0242.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
					if (updatedContainer.getEntity().isPresent()) {
						updatedContainer.getEntity().get().setSeqnr(it0242.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
						it0242 = it0242CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}
				
					if (!it0242.isPresent()) {
						throw new CannotPersistException("Cannot update IT0242 (BPJS Data) data. Please check your data!");
					}
					
					StringBuilder key = new StringBuilder("pernr=" + it0242.get().getId().getPernr());
					key.append(",endda=" + it0242.get().getId().getEndda());
					key.append(",begda=" + it0242.get().getId().getBegda());
					EventLog eventLog = new EventLog(it0242Entity.get(), key.toString(), OperationType.UPDATE, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					
					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}

					//IT0242SoapObject it0242SoapObject = new IT0242SoapObject(savedEntity.get()); 
					//muleConsumerService.postToSAP(BAPIFunction.IT0242_CREATE.service(), "bpjsInfo", it0242SoapObject);
					
					responseWrapper = new IT0242ResponseWrapper(it0242.get());
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(IT0242Controller.class).edit(request, result)).withSelfRel());
		return response;
	}		
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(itValidator);
	}		
}