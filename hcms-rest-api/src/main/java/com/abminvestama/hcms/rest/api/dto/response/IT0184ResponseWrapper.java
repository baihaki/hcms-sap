package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0184;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add Constructor of Read from EventData -> IT0184ResponseWrapper(IT0184, String, CommonServiceFactory)</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add text1 field</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add documentStatus field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class IT0184ResponseWrapper extends ResourceSupport {

	private long pernr;
	
	private String subty;
	
	private String subtyText;
	
	private Date endda;
	
	private Date begda;

	private long seqnr;
	
	private String itxex;
	
	private String text1;
	
	private String documentStatus;
	
	private IT0184ResponseWrapper() {}
	
	public IT0184ResponseWrapper(IT0184 it0184) {
		if (it0184 == null) {
			new IT0184ResponseWrapper();
		} else {
			this
			.setPernr(it0184.getId().getPernr())
			.setSubty(it0184.getSubty() != null ? 
					StringUtils.defaultString(it0184.getSubty().getId() != null ? it0184.getSubty().getId().getSubty() : StringUtils.EMPTY) : it0184.getId().getSubty())
			.setSubtyText(it0184.getSubty() != null ? StringUtils.defaultString(it0184.getSubty().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setEndda(it0184.getId().getEndda()).setBegda(it0184.getId().getBegda())
			.setSeqnr(it0184.getSeqnr() != null ? it0184.getSeqnr().longValue() : 0l)
			.setItxex(StringUtils.defaultString(it0184.getItxex(), StringUtils.EMPTY))
			.setText1(StringUtils.defaultString(it0184.getText1(), StringUtils.EMPTY))
			.setDocumentStatus(it0184.getStatus());
		}
	}

	public IT0184ResponseWrapper(IT0184 currentData, String eventData, CommonServiceFactory serviceFactory) throws JSONException, Exception {
		this(currentData);
		IT0184ResponseWrapper itWrapper = (IT0184ResponseWrapper) new EventDataWrapper(eventData, this).getItWrapper();
		String[] ignoreProperties = EventDataWrapper.getNullPropertyNames(itWrapper);
		BeanUtils.copyProperties(itWrapper, this, ignoreProperties);
	}
	
	/**
	 * GET Employee SSN.
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	public IT0184ResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	@JsonProperty("subtype")
	public String getSubty() {
		return subty;
	}
	
	public IT0184ResponseWrapper setSubty(String subty) {
		this.subty = subty;
		return this;
	}
	
	/**
	 * GET Subtype Text.
	 * 
	 * @return
	 */
	@JsonProperty("subtype_text")
	public String getSubtyText() {
		return subtyText;
	}
	
	public IT0184ResponseWrapper setSubtyText(String subtyText) {
		this.subtyText = subtyText;
		return this;
	}
	
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	public IT0184ResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}
	
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	public IT0184ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * GET Infotype Record No.
	 * 
	 * @return
	 */
	@JsonProperty("infotype_record_no")
	public long getSeqnr() {
		return seqnr;
	}
	
	public IT0184ResponseWrapper setSeqnr(long seqnr) {
		this.seqnr = seqnr;
		return this;
	}
	
	/**
	 * GET Text Exists (Data).
	 * 
	 * @return
	 */
	@JsonProperty("text_data")
	public String getItxex() {
		return itxex;
	}
	
	public IT0184ResponseWrapper setItxex(String itxex) {
		this.itxex = itxex;
		return this;
	}
	
	/**
	 * GET Value Text.
	 * 
	 * @return
	 */
	@JsonProperty("text_value")
	public String getText1() {
		return text1;
	}
	
	public IT0184ResponseWrapper setText1(String text1) {
		this.text1 = text1;
		return this;
	}
	
	/**
	 * GET Document Status.
	 * 
	 * @return
	 */
	@JsonProperty("document_status")
	public String getDocumentStatus() {
		return documentStatus;
	}
	
	public IT0184ResponseWrapper setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus.name();
		return this;
	}
}