package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add attach1Type, attach1Content, attach1Filename field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public class IT0241RequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = -5581846940324540688L;
	
	private String taxId;
	private String marrd;
	private String spben;
	private String refno;
	private String depnd;
	private String rdate;

	private String attach1Type;
	private String attach1Content;
	private String attach1Filename;
	
	@JsonProperty("tax_id")
	public String getTaxId() {
		return taxId;
	}
	
	@JsonProperty("married_for_tax")
	public String getMarrd() {
		return marrd;
	}
	
	@JsonProperty("spouse_benefit")
	public String getSpben() {
		return spben;
	}
	
	@JsonProperty("unemployment_ref_no")
	public String getRefno() {
		return refno;
	}
	
	@JsonProperty("num_of_tax_dependents")
	public String getDepnd() {
		return depnd;
	}
	
	@JsonProperty("npwp_registration_date")
	public String getRdate() {
		return rdate;
	}
	
	@JsonProperty("attachment1_type")
	public String getAttach1Type() {
		return attach1Type;
	}
	
	@JsonProperty("attachment1_base64")
	public String getAttach1Content() {
		return attach1Content;
	}
	
	@JsonProperty("attachment1_filename")
	public String getAttach1Filename() {
		return attach1Filename;
	}
}