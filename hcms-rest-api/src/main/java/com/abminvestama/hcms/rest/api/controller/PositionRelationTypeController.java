package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.entity.PositionRelationType;
import com.abminvestama.hcms.core.service.api.business.query.PositionRelationTypeQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@RestController
@RequestMapping("/api/v2/position_relation_types")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class PositionRelationTypeController extends AbstractResource {

	private PositionRelationTypeQueryService positionRelationTypeQueryService;
	
	@Autowired
	void setPositionRelationTypeQueryService(PositionRelationTypeQueryService positionRelationTypeQueryService) {
		this.positionRelationTypeQueryService = positionRelationTypeQueryService;
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<List<PositionRelationType>> fetchAll( 
			@RequestParam(required = true, value = "username", defaultValue = "") String username,
			@RequestParam(required = true, value = "auth_token", defaultValue = "") String authToken) throws Exception {
		
		if (authToken == null || authToken.isEmpty()) {
			throw new Exception("No token provided");
		}

		APIResponseWrapper<List<PositionRelationType>> response = new APIResponseWrapper<>();
		
		positionRelationTypeQueryService.fetchAll().ifPresent(prts -> {
			response.setData(prts.stream().collect(Collectors.toList()));
		});
		
		Link self = linkTo(methodOn(PositionRelationTypeController.class).fetchAll(username, authToken)).withSelfRel();				
		response.add(self);
		
		return response;
	}		
}