package com.abminvestama.hcms.rest.api.helper;

import org.apache.commons.lang3.StringUtils;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.T042Z;
import com.abminvestama.hcms.core.model.entity.T705H;
import com.abminvestama.hcms.core.model.entity.T705P;
import com.abminvestama.hcms.core.model.entity.TEVEN;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.TEVEN;
import com.abminvestama.hcms.core.model.entity.T554T;
import com.abminvestama.hcms.core.model.entity.TEVENKey;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.query.T705HQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T705PQueryService;
import com.abminvestama.hcms.core.service.api.business.query.TEVENQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T554TQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.TEVENRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.TEVENRequestWrapper;


@Component("tevenRequestBuilderUtil")
@Transactional(readOnly = true)
public class TEVENRequestBuilderUtil {
	

	private TEVENQueryService tevenQueryService;

	@Autowired
	void setTEVENQueryService(TEVENQueryService tevenQueryService) {
		this.tevenQueryService = tevenQueryService;
	}
	
	private UserQueryService userQueryService;
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
private T705PQueryService t705pQueryService;
	
	@Autowired
	void setT705PQueryService(T705PQueryService t705pQueryService) {
		this.t705pQueryService = t705pQueryService;
	}
	
private T705HQueryService t705hQueryService;
	
	@Autowired
	void setT705HQueryService(T705HQueryService t705hQueryService) {
		this.t705hQueryService = t705hQueryService;
	}
	
	
	private T554TQueryService t554tQueryService;
	
	@Autowired
	void setT554TQueryService(T554TQueryService t554tQueryService) {
		this.t554tQueryService = t554tQueryService;
	}

	/**
	 * Compare TEVEN request payload with existing TEVEN in the database.
	 * Update existing TEVEN data with the latest data comes from the request payload. 
	 * 
	 * @param requestPayload request data 
	 * @param tevenDB current existing TEVEN in the database.
	 * @return updated TEVEN object to be persisted into the database.
	 * @throws ParseException 
	 */
	public RequestObjectComparatorContainer<TEVEN, TEVENRequestWrapper, String> compareAndReturnUpdatedData(TEVENRequestWrapper requestPayload, TEVEN tevenDB) throws ParseException {

		RequestObjectComparatorContainer<TEVEN, TEVENRequestWrapper, String> objectContainer = null; 
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		
		
		if (tevenDB == null) {
			tevenDB = new TEVEN();
			
//			try {
//				Optional<TEVEN> existingTEVEN = tevenQueryService.findOneByCompositeKey(
//						requestPayload.getPdsnr());

				Optional<T705P> t705p = t705pQueryService.findBySatza(requestPayload.getSatza());
				tevenDB.setPmbde(t705p != null && t705p.isPresent() ? t705p.get().getPmbde() : null);
				tevenDB.setSatza(t705p.isPresent() ? t705p.get() : null);
				
				Optional<TEVEN> existingTEVEN = tevenQueryService.findOneByCompositeKeys(
						new Long(requestPayload.getPernr()),
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getLdate()),
						tevenDB.getSatza());
				
				if (existingTEVEN.isPresent()) {
					throw new DataViolationException("Duplicate TEVEN Data : "
							+ "S.N = " + requestPayload.getPernr() + ", "
									+ requestPayload.getLdate() + " "
									+ "[ " + requestPayload.getSatza() + " ]");
				}
				
//				tevenDB.setId(new TEVENKey(requestPayload.getPdsnr()));
				
				//Optional<T554T> bnka = t554tQueryService.findById(Optional.ofNullable(requestPayload.getAwart()));
				//tevenDB.setAwart(bnka.isPresent() ? bnka.get() : null);
				
				Optional<User> employee = userQueryService.findByPernr(requestPayload.getPernr());
				tevenDB.setPernr(employee.get().getEmployee().getPernr());
				
				if (requestPayload.getTerid() != null && !requestPayload.getTerid().isEmpty())
					tevenDB.setTerid(requestPayload.getTerid());

				if (requestPayload.getAbwgr() != null && requestPayload.getAbwgr().length() > 0) {
					Optional<T705H> t705h = t705hQueryService.findByPinco(requestPayload.getAbwgr());
					tevenDB.setAbwgr(t705h.isPresent() ? t705h.get() : null);
				}
				//tevenDB.setAbwgr(requestPayload.getAbwgr());
				if (requestPayload.getExlga() != null && !requestPayload.getExlga().isEmpty())
					tevenDB.setExlga(requestPayload.getExlga());
				if (requestPayload.getZeinh() != null && !requestPayload.getZeinh().isEmpty())
					tevenDB.setZeinh(requestPayload.getZeinh());
				tevenDB.setHrazl(requestPayload.getHrazl() != null ? requestPayload.getHrazl() : 0);
				tevenDB.setHrbet(requestPayload.getHrbet() != null ? requestPayload.getHrbet() : 0);
				if (requestPayload.getOrigf() != null && !requestPayload.getOrigf().isEmpty())
					tevenDB.setOrigf(requestPayload.getOrigf());
				if (requestPayload.getDallf() != null && !requestPayload.getDallf().isEmpty())
					tevenDB.setDallf(requestPayload.getDallf());
				if (requestPayload.getPdcUsrup() != null && !requestPayload.getPdcUsrup().isEmpty())
					tevenDB.setPdcUsrup(requestPayload.getPdcUsrup());
				tevenDB.setUname(requestPayload.getUname());
				tevenDB.setZausw(requestPayload.getZausw());
				
				//DateFormat formatter = new SimpleDateFormat("hh:mm");
				
				if (StringUtils.isNotEmpty(requestPayload.getErdat()))
					tevenDB.setErdat(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getErdat()));
				if (requestPayload.getErtim() != null && !requestPayload.getErtim().isEmpty())
					tevenDB.setErtim(formatter.parse(requestPayload.getErtim()));
				if (requestPayload.getAedtm() != null && !requestPayload.getAedtm().isEmpty())
					tevenDB.setAedtm(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getAedtm()));
				
				tevenDB.setLdate(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getLdate()));
				tevenDB.setLtime(formatter.parse(requestPayload.getLtime()));
				
				tevenDB.setProcess(requestPayload.getProcess());
				tevenDB.setTrxcode(requestPayload.getTrxcode());
				tevenDB.setReason(requestPayload.getReason());
				
				//tevenDB.setSeqnr(1L);
//			} catch (Exception ignored) {
//				System.err.println(ignored);
//			}
			
			objectContainer = new RequestObjectComparatorContainer<>(tevenDB, requestPayload);
			
			
			
		} else {
			
			
			if (requestPayload.getPernr() != (tevenDB.getPernr() != null ? tevenDB.getPernr() : 0)) {
				Optional<User> employee = userQueryService.findByPernr(requestPayload.getPernr());
				tevenDB.setPernr(employee.get().getEmployee().getPernr());
				requestPayload.setIsDataChanged(true);
			}
			
			
			
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getSatza(), tevenDB.getSatza() != null ? tevenDB.getSatza().getSatza() : "")) {
				try {
					Optional<T705P> newT705P = t705pQueryService.findBySatza(requestPayload.getSatza());
					if (newT705P.isPresent()) {
						tevenDB.setSatza(newT705P.get());
						requestPayload.setIsDataChanged(true);
					}
				} catch (Exception e) {
					// zlsch not found..., ignore change
				}
			}

			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getTerid(), tevenDB.getTerid())) {
				tevenDB.setTerid(requestPayload.getTerid());
				requestPayload.setIsDataChanged(true);
			}

			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAbwgr(), tevenDB.getAbwgr() != null ? tevenDB.getAbwgr().getPinco() : "")) {
				try {
					Optional<T705H> newT705H = t705hQueryService.findByPinco(requestPayload.getAbwgr());
					if (newT705H.isPresent()) {
						tevenDB.setAbwgr(newT705H.get());
						requestPayload.setIsDataChanged(true);
					}
				} catch (Exception e) {
					// zlsch not found..., ignore change
				}
			}

			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getExlga(), tevenDB.getExlga())) {
				tevenDB.setExlga(requestPayload.getExlga());
				requestPayload.setIsDataChanged(true);
			}

			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getZeinh(), tevenDB.getZeinh())) {
				tevenDB.setZeinh(requestPayload.getZeinh());
				requestPayload.setIsDataChanged(true);
			}
			
			

			if (StringUtils.isNotEmpty(requestPayload.getErdat()) && CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getErdat()), tevenDB.getErdat())) {
				tevenDB.setErdat(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getErdat()));
				requestPayload.setIsDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getLdate()), tevenDB.getLdate())) {
				tevenDB.setLdate(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getLdate()));
				requestPayload.setIsDataChanged(true);
			}
			

			//DateFormat formatter = new SimpleDateFormat("hh:mm");
			
			if (!LocalTime.parse(requestPayload.getLtime()).equals(tevenDB.getLtime())) {
				tevenDB.setLtime(formatter.parse(requestPayload.getLtime()));
				requestPayload.setIsDataChanged(true);
			}
			if (StringUtils.isNotEmpty(requestPayload.getErtim()) && !LocalTime.parse(requestPayload.getErtim()).equals(tevenDB.getErtim())) {
				tevenDB.setErtim(formatter.parse(requestPayload.getErtim()));
				requestPayload.setIsDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getAedtm()), tevenDB.getAedtm())) {
				tevenDB.setAedtm(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getAedtm()));
				requestPayload.setIsDataChanged(true);
			}
			
			

			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDallf(), tevenDB.getDallf())) {
				tevenDB.setDallf(requestPayload.getDallf());
				requestPayload.setIsDataChanged(true);
			}

			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPdcUsrup(), tevenDB.getPdcUsrup())) {
				tevenDB.setPdcUsrup(requestPayload.getPdcUsrup());
				requestPayload.setIsDataChanged(true);
			}

			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getUname(), tevenDB.getUname())) {
				tevenDB.setUname(requestPayload.getUname());
				requestPayload.setIsDataChanged(true);
			}

			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getOrigf(), tevenDB.getOrigf())) {
				tevenDB.setOrigf(requestPayload.getOrigf());
				requestPayload.setIsDataChanged(true);
			}
			
			


			if (requestPayload.getZausw() != null) {
				tevenDB.setZausw(requestPayload.getZausw());
				requestPayload.setIsDataChanged(true);
			}
			
			if (requestPayload.getHrazl() != null) {
				tevenDB.setHrazl(requestPayload.getHrazl());
				requestPayload.setIsDataChanged(true);
			}

			if (requestPayload.getHrbet() != null) {
				tevenDB.setHrbet(requestPayload.getHrbet());
				requestPayload.setIsDataChanged(true);
			}

			if (StringUtils.isNotEmpty(requestPayload.getProcess())) {
				tevenDB.setProcess(requestPayload.getProcess());
				requestPayload.setIsDataChanged(true);
			}

			if (StringUtils.isNotEmpty(requestPayload.getTrxcode())) {
				tevenDB.setTrxcode(requestPayload.getTrxcode());
				requestPayload.setIsDataChanged(true);
			}
			
			if (StringUtils.isNotEmpty(requestPayload.getReason())) {
				tevenDB.setReason(requestPayload.getReason());
				requestPayload.setIsDataChanged(true);
			}
			
			objectContainer = new RequestObjectComparatorContainer<>(tevenDB, requestPayload);			
		}
		
		JSONObject json = new JSONObject(requestPayload);
		objectContainer.setEventData(json.toString());
		
		return objectContainer;
	}			
}