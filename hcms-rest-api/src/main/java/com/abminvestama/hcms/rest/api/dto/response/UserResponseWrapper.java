package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Calendar;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.rest.api.model.constant.HCMSResourceIdentifier;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Format employeeStartDate and employeeEndDate field into yyyy-MM-dd</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add employeeEndDate field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class UserResponseWrapper extends ResourceSupport {

	public static final String RESOURCE = HCMSResourceIdentifier.USER.label();
	
	private String kostl;//new
	private String kostxt;//new
	private String uuid;
	private String username;
	private String emailAddress;
	private long pernr;
	private String fullname;
	private String photoLink;
	private String employeeStartDate;
	private String employeeEndDate;
	private String employeePosition;
	private String companyName;
	private String companyCode;
	private String companyLocation;
	private String workArea;
	private String workSubArea;
	private String organizationalUnit;
	private String positionId;
	private String position;
	private String job;
	private String empGroup;
	private String empSubGroup;
	private String payrollArea;
	private RoleResponseWrapper[] roles;
	private AuthorResponseWrapper createdBy;
	private AuthorResponseWrapper updatedBy;
	
	public UserResponseWrapper(User user) {
		this.uuid = user.getId();
		this.username = user.getUsername();
		this.emailAddress = user.getEmail();
		this.pernr = user.getEmployee().getId().getPernr(); //user.getEmployee().getPernr();
		this.fullname = user.getEmployee().getSname();
		this.photoLink = user.getPhotoLink();
		Calendar dateOfJoined = Calendar.getInstance();
		/*
		dateOfJoined.setTime(user.getEmployee().getId().getBegda());
		this.employeeStartDate = new StringBuilder(
				CommonDateFunction.getMonthFullName(dateOfJoined.get(Calendar.MONTH))
				).append(" ").append(dateOfJoined.get(Calendar.DAY_OF_MONTH))
				.append(", ").append(dateOfJoined.get(Calendar.YEAR)).toString();
		*/
		this.employeeStartDate = CommonDateFunction.convertDateToStringYMD(user.getEmployee().getId().getBegda());
		/*
		dateOfJoined.setTime(user.getEmployee().getId().getEndda());
		this.employeeEndDate = new StringBuilder(
				CommonDateFunction.getMonthFullName(dateOfJoined.get(Calendar.MONTH))
				).append(" ").append(dateOfJoined.get(Calendar.DAY_OF_MONTH))
				.append(", ").append(dateOfJoined.get(Calendar.YEAR)).toString();
		*/
		this.employeeEndDate = CommonDateFunction.convertDateToStringYMD(user.getEmployee().getId().getEndda());
		this.employeePosition = user.getEmployee().getT528t() != null ? user.getEmployee().getT528t().getPlstx() : StringUtils.EMPTY;
		this.companyName = user.getEmployee().getT500p() != null ? user.getEmployee().getT500p().getBukrs().getButxt() : StringUtils.EMPTY;
		this.companyCode = user.getEmployee().getT500p() != null ? user.getEmployee().getT500p().getBukrs().getBukrs() : StringUtils.EMPTY;
		
		this.companyLocation = user.getEmployee().getV001pall() != null ? user.getEmployee().getV001pall().getBtext() : StringUtils.EMPTY;
		this.workArea = user.getEmployee().getT500p() != null ? user.getEmployee().getT500p().getPbtxt() : StringUtils.EMPTY;
		this.workSubArea = user.getEmployee().getV001pall() != null ? user.getEmployee().getV001pall().getBtext() : StringUtils.EMPTY;;
		
		this.organizationalUnit = user.getEmployee().getT527x().getOrgtx();
		this.positionId = user.getEmployee().getPosition().getId();
		this.position = user.getEmployee().getT528t().getPlstx();
		this.job = user.getEmployee().getT513s().getStltx();
		this.empGroup = user.getEmployee().getT501t().getPgtxt();
		this.empSubGroup = user.getEmployee().getT503k().getPersk();
		this.payrollArea = user.getEmployee().getT549t().getAbktx();
		this.kostl = user.getEmployee().getCskt().getKostl();
		this.kostxt = user.getEmployee().getCskt().getKltxt();
		
		this.roles = user.getRoles().stream().collect(Collectors.toList()).stream().map(role -> {
			return new RoleResponseWrapper(role);
		}).collect(Collectors.toList()).toArray(new RoleResponseWrapper[user.getRoles().size()]);
	}
	
	@JsonProperty("cost_centre")
	public String getKostl() {
		return kostl;
	}
	
	@JsonProperty("cost_centre_text")
	public String getKostxt() {
		return kostxt;
	}
	
	@JsonProperty("system-id")
	public String getUuid() {
		return uuid;
	}
	
	@JsonProperty("username")
	public String getUsername() {
		return username;
	}
	
	@JsonProperty("email_address")
	public String getEmailAddress() {
		return emailAddress;
	}
	
	@JsonProperty("pernr")
	public long getPernr() {
		return pernr;
	}
	
	@JsonProperty("fullname")
	public String getFullname() {
		return fullname;
	}
	
	@JsonProperty("photo_link")
	public String getPhotoLink() {
		return photoLink;
	}
	
	@JsonProperty("employee_start_date")
	public String getEmployeeStartDate() {
		return employeeStartDate;
	}
	
	@JsonProperty("employee_end_date")
	public String getEmployeeEndDate() {
		return employeeEndDate;
	}
	
	@JsonProperty("employee_position")
	public String getEmployeePosition() {
		return employeePosition;
	}
	
	@JsonProperty("company_name")
	public String getCompanyName() {
		return companyName;
	}
	
	@JsonProperty("company_code")
	public String getCompanCode() {
		return companyCode;
	}
	
	@JsonProperty("company_location")
	public String getCompanyLocation() {
		return companyLocation;
	}
	
	@JsonProperty("work_area")
	public String getWorkArea() {
		return workArea;
	}
	
	@JsonProperty("work_sub_area")
	public String getWorkSubArea() {
		return workSubArea;
	}
	
	@JsonProperty("org_unit")
	public String getOrganizationalUnit() {
		return organizationalUnit;
	}
	
	@JsonProperty("position_id")
	public String getPositionId() {
		return positionId;
	}
	
	@JsonProperty("position")
	public String getPosition() {
		return position;
	}
	
	@JsonProperty("job")
	public String getJob() {
		return job;
	}
	
	@JsonProperty("emp_group")
	public String getEmpGroup() {
		return empGroup;
	}
	
	@JsonProperty("emp_sub_group")
	public String getEmpSubGroup() {
		return empSubGroup;
	}
	
	@JsonProperty("payroll_area")
	public String getPayrollArea() {
		return payrollArea;
	}
	
	@JsonProperty("roles")
	public RoleResponseWrapper[] getRoles() {
		return roles;
	}
	
	@JsonProperty("created_by")
	public AuthorResponseWrapper getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(AuthorResponseWrapper createdBy) {
		this.createdBy = createdBy;
	}
	
	public void setCreatedBy(User user, Link userLink) {
		AuthorResponseWrapper createdBy = new AuthorResponseWrapper(user, userLink);
		this.createdBy = createdBy;
	}
	
	@JsonProperty("updated_by")
	public AuthorResponseWrapper getUpdatedBy() {
		return updatedBy;
	}
	
	public void setUpdatedBy(AuthorResponseWrapper updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public void setUpdatedBy(User user, Link userLink) {
		AuthorResponseWrapper updatedBy = new AuthorResponseWrapper(user, userLink);
		this.updatedBy = updatedBy;
	}
}