package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.rest.api.model.constant.HCMSResourceIdentifier;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@JsonInclude(Include.NON_NULL)
public class EmployeePositionRelationResponseWrapper extends ResourceSupport {

	public static final String RESOURCE = HCMSResourceIdentifier.EMPLOYEE.label();
	
	private long pernr;
	private Date endda;
	private Date begda;
	private String sname;
	private String ename;
	private String positionId;
	private String positionCode;
	private String positionName;
	private String relationName;
	private long toEmployeePernr;
	private String toEmployeeName;
	private String toEmployeePositionId;
	private String toEmployeePositionCode;
	private String toEmployeePositionName;
	
	private Date lastModifiedOn;
	
	private EmployeePositionRelationResponseWrapper() {}
	
	public EmployeePositionRelationResponseWrapper(IT0001 fromIT0001, IT0001 toIT0001, String relationName) {
		if (fromIT0001 == null && toIT0001 == null) {
			new EmployeePositionRelationResponseWrapper();
		} else {
			if (fromIT0001 != null) {
				this.setPernr(fromIT0001.getPernr())
					.setEndda(fromIT0001.getId().getEndda())
					.setBegda(fromIT0001.getId().getBegda())
					.setSname(fromIT0001.getSname())
					.setEname(fromIT0001.getEname())
					.setPositionId(fromIT0001.getPosition() != null ? fromIT0001.getPosition().getId() : StringUtils.EMPTY)
					.setPositionCode(fromIT0001.getPosition() != null ? fromIT0001.getPosition().getCode() : StringUtils.EMPTY)
					.setPositionName(fromIT0001.getPosition() != null ? fromIT0001.getPosition().getName() : StringUtils.EMPTY)
					.setLastModifiedOn(fromIT0001.getAedtm());
			}
			if (toIT0001 != null) {
				this.setToEmployeePernr(toIT0001.getPernr())
					.setToEmployeeName(toIT0001.getSname())
					.setToEmployeePositionId(toIT0001.getPosition() != null ? toIT0001.getPosition().getId() : StringUtils.EMPTY)
					.setToEmployeePositionCode(toIT0001.getPosition() != null ? toIT0001.getPosition().getCode() : StringUtils.EMPTY)
					.setToEmployeePositionName(toIT0001.getPosition() != null ? toIT0001.getPosition().getName() : StringUtils.EMPTY);
			}
			if (StringUtils.isNotBlank(relationName)) {
				this.setRelationName(relationName);
			}
		}
	}
	
	/**
	 * GET Employee SSN.
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	private EmployeePositionRelationResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	private EmployeePositionRelationResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}
	
	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	private EmployeePositionRelationResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * GET Last Name First Name.
	 * 
	 * @return
	 */
	@JsonProperty("full_name")
	public String getSname() {
		return sname;
	}
	
	private EmployeePositionRelationResponseWrapper setSname(String sname) {
		this.sname = sname;
		return this;
	}
	
	/**
	 * GET Employee/Appl.Name.
	 * 
	 * @return
	 */
	@JsonProperty("employee_name")
	public String getEname() {
		return ename;
	}
	
	private EmployeePositionRelationResponseWrapper setEname(String ename) {
		this.ename = ename;
		return this;
	}
	
	/**
	 * GET Employee Position ID.
	 * 
	 * @return
	 */
	@JsonProperty("position_id")
	public String getPositionId() {
		return positionId;
	}
	
	private EmployeePositionRelationResponseWrapper setPositionId(String positionId) {
		this.positionId = positionId;
		return this;
	}
	
	/**
	 * GET Employee Position Code.
	 * 
	 * @return
	 */
	@JsonProperty("position_code")
	public String getPositionCode() {
		return positionCode;
	}
	
	private EmployeePositionRelationResponseWrapper setPositionCode(String positionCode) {
		this.positionCode = positionCode;
		return this;
	}
	
	/**
	 * GET Employee Position Name.
	 * 
	 * @return
	 */
	@JsonProperty("position_name")
	public String getPositionName() {
		return positionName;
	}
	
	private EmployeePositionRelationResponseWrapper setPositionName(String positionName) {
		this.positionName = positionName;
		return this;
	}
	
	/**
	 * GET Relation Name.
	 * 
	 * @return
	 */
	@JsonProperty("relation_name")
	public String getRelationName() {
		return relationName;
	}
	
	private EmployeePositionRelationResponseWrapper setRelationName(String relationName) {
		this.relationName = relationName;
		return this;
	}
	
	/**
	 * GET To Employee SSN.
	 * 
	 * @return
	 */
	@JsonProperty("to_employee_ssn")
	public long getToEmployeePernr() {
		return toEmployeePernr;
	}
	
	private EmployeePositionRelationResponseWrapper setToEmployeePernr(long toEmployeePernr) {
		this.toEmployeePernr = toEmployeePernr;
		return this;
	}
	
	/**
	 * GET To Employee Name.
	 * 
	 * @return
	 */
	@JsonProperty("to_employee_name")
	public String getToEmployeeName() {
		return toEmployeeName;
	}
	
	private EmployeePositionRelationResponseWrapper setToEmployeeName(String toEmployeeName) {
		this.toEmployeeName = toEmployeeName;
		return this;
	}
	
	/**
	 * GET To Employee Position ID.
	 * 
	 * @return
	 */
	@JsonProperty("to_employee_position_id")
	public String getToEmployeePositionId() {
		return toEmployeePositionId;
	}
	
	private EmployeePositionRelationResponseWrapper setToEmployeePositionId(String toEmployeePositionId) {
		this.toEmployeePositionId = toEmployeePositionId;
		return this;
	}
	
	/**
	 * GET To Employee Position Code.
	 * 
	 * @return
	 */
	@JsonProperty("to_employee_position_code")
	public String getToEmployeePositionCode() {
		return toEmployeePositionCode;
	}
	
	private EmployeePositionRelationResponseWrapper setToEmployeePositionCode(String toEmployeePositionCode) {
		this.toEmployeePositionCode = toEmployeePositionCode;
		return this;
	}
	
	/**
	 * GET To Employee Position Name.
	 * 
	 * @return
	 */
	@JsonProperty("to_employee_position_name")
	public String getToEmployeePositionName() {
		return toEmployeePositionName;
	}
	
	private EmployeePositionRelationResponseWrapper setToEmployeePositionName(String toEmployeePositionName) {
		this.toEmployeePositionName = toEmployeePositionName;
		return this;
	}
	
	/**
	 * GET Last Modified Date.
	 * 
	 * @return
	 */
	@JsonProperty("last_modified_on")
	public Date getLastModifiedOn() {
		return lastModifiedOn;
	}
	
	private EmployeePositionRelationResponseWrapper setLastModifiedOn(Date lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
		return this;
	}
}