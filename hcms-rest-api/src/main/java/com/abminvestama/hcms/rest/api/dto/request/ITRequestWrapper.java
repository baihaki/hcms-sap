package com.abminvestama.hcms.rest.api.dto.request;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 * 
 * Base class for all request payload objects. 
 *
 */
public class ITRequestWrapper implements Serializable {

	private static final long serialVersionUID = 5393202597259317592L;
	
	private long pernr;
	private String subty;
	private String endda;
	private String begda;
	
	@JsonIgnore
	private boolean dataChanged;
	
	@JsonIgnore
	private String definedSubty;
	
	public boolean isDataChanged() {
		return dataChanged;
	}
	
	public void setDataChanged(boolean dataChanged) {
		this.dataChanged = dataChanged;
	}
	
	public String getDefinedSubty() {
		return definedSubty;
	}

	public void setDefinedSubty(String definedSubty) {
		this.definedSubty = definedSubty;
		if (subty == null)
			subty = definedSubty;
		if (endda == null)
			endda = "9999-12-31";
		if (begda == null)
			begda = String.format("%1$tY-%1$tm-%1$td", new Date());
	}

	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	@JsonProperty("subtype")
	public String getSubty() {
		return subty;
	}
	
	@JsonProperty("end_date")
	public String getEndda() {
		return endda;
	}
	
	@JsonProperty("begin_date")
	public String getBegda() {
		return begda;
	}
}