package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add field: photoLink</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public class UserRequestWrapper {

	private String username;
	private String email;
	private String password;
	private String confirmPassword;
	private String photoLink;
	
	/* for new record */
	private long ssn;
	private String endda;
	private String begda;
	private String[] roleNames;
	
	private boolean isEditRequest;
	
	public UserRequestWrapper() {}
	
	public UserRequestWrapper(String username, String password, String confirmPassword,
			String email) {
		this.username = username;
		this.password = password;
		this.confirmPassword = confirmPassword;
		this.email = email;
	}
	
	@JsonProperty("username")
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	@JsonProperty("email")
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@JsonProperty("password")
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	@JsonProperty("confirm_password")
	public String getConfirmPassword() {
		return confirmPassword;
	}
	
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	@JsonProperty("photo_link")
	public String getPhotoLink() {
		return photoLink;
	}

	public void setPhotoLink(String photoLink) {
		this.photoLink = photoLink;
	}

	@JsonProperty("ssn")
	public long getSsn() {
		return ssn;
	}
	
	public void setSsn(long ssn) {
		this.ssn = ssn;
	}
	
	@JsonProperty("end_date")
	public String getEndda() {
		return endda;
	}
	
	public void setEndda(String endda) {
		this.endda = endda;
	}
	
	@JsonProperty("begin_date")
	public String getBegda() {
		return begda;
	}
	
	public void setBegda(String begda) {
		this.begda = begda;
	}
	
	@JsonProperty("role_names")
	public String[] getRoleNames() {
		return roleNames;
	}
	
	public void setRoleNames(String[] roleNames) {
		this.roleNames = roleNames;
	}

	@JsonProperty("is_edit")
	public boolean isEditRequest() {
		return isEditRequest;
	}
	
	public void setIsEditRequest(boolean isEditRequest) {
		this.isEditRequest = isEditRequest;
	}
}