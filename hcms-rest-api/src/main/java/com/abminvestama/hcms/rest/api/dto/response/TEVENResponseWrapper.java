package com.abminvestama.hcms.rest.api.dto.response;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.TEVEN;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class TEVENResponseWrapper extends ResourceSupport {

	private String tevenid;
	private Long pdsnr;
	private long pernr;
	
	/*private Date ldate;
	private Date ltime;
	private Date erdat;
	private Date ertim;*/
	private String ldate;
	private String ltime;
	private String erdat;
	private String ertim;
	
	private String pmbde;
	private String satza;
	private String terid;
	private String abwgr;
	private String exlga;
	private String zeinh;

	private Double hrazl;
	private Double hrbet;
	private String origf;
	private String dallf;
	private String pdcUsrup;
	
	private String uname;
	//private Date aedtm;
	private Long zausw;
	private String aedtm;
	
	private Long process;
	private String reason;
	private String trxCode;
	private String submittedBy;
	private String submittedNameBy;
	private Date submittedAt;
	private String approved1By;
	private String approvedByPernr1;
	private String approvedName1By;
	private Date approved1At;
	private String approved2By;
	private String approvedByPernr2;
	private String approvedName2By;
	private Date approved2At;
	private String approved3By;
	private String approvedByPernr3;
	private String approvedName3By;
	private Date approved3At;
	private String approved4By;
	private String approvedByPernr4;
	private String approvedName4By;
	private Date approved4At;
	private String approved5By;
	private String approvedByPernr5;
	private String approvedName5By;
	private Date approved5At;
	private String approved6By; //Approved HRAdmin
	private String approvedName6By;
	private Date approved6At;
	private String approvalNote;
	private String divisionName; //Personnel Area Text(table t500p - SAP)
	
	private TEVENResponseWrapper() {}
	
	public TEVENResponseWrapper(TEVEN teven) {
		if (teven == null) {
			new TEVENResponseWrapper();
		} else {
			DateFormat formatter = new SimpleDateFormat("HH:mm");
			
			this
			.setTevenid(teven.getTevenid())
			.setPdsnr(teven.getPdsnr())
			.setPernr(teven.getPernr() != null ? teven.getPernr() : null)
			.setDivisionName(teven.getPernr() != null ? teven.getZdivision() : null)
			.setLdate(teven.getLdate() != null ? teven.getLdate().toString() : null)
			.setLtime(teven.getLtime() != null ? formatter.format(teven.getLtime()) : null)
			.setErdat(teven.getErdat() != null ? teven.getErdat().toString() : null)
			.setErtim(teven.getErtim() != null ? formatter.format(teven.getErtim()) : null)
			.setPmbde(teven.getSatza() != null ? teven.getSatza().getPmbde() : null)
			.setSatza(teven.getSatza() != null ? teven.getSatza().getSatza() : null)
			.setTerid(teven.getTerid() != null && !teven.getTerid().isEmpty() ? teven.getTerid() : null)
			.setAbwgr(teven.getAbwgr() != null && teven.getAbwgr().getPinco()!= null && !teven.getAbwgr().getPinco().isEmpty() ? teven.getAbwgr().getPinco() : null)
			.setExlga(teven.getExlga() != null && !teven.getExlga().isEmpty() ? teven.getExlga() : null)
			.setZeinh(teven.getZeinh() != null && !teven.getZeinh().isEmpty() ? teven.getZeinh() : null)
			.setOrigf(teven.getOrigf() != null && !teven.getOrigf().isEmpty() ? teven.getOrigf() : null)
			.setDallf(teven.getDallf() != null && !teven.getDallf().isEmpty() ? teven.getDallf() : null)
			.setUname(teven.getUname() != null && !teven.getUname().isEmpty() ? teven.getUname() : null)
			.setPdcUsrup(teven.getPdcUsrup() != null && !teven.getPdcUsrup().isEmpty() ? teven.getPdcUsrup() : null)
			.setAedtm(teven.getAedtm() != null ? teven.getAedtm().toString() : null)
			.setHrazl(teven.getHrazl() != null ? teven.getHrazl().doubleValue() : 0)
			.setHrbet(teven.getHrbet() != null ? teven.getHrbet().doubleValue() : 0)
			.setZausw(teven.getZausw() != null ? teven.getZausw() : 0)
			.setProcess(teven.getProcess() != null && !teven.getProcess().isEmpty() ? new Long(teven.getProcess()) : 0)
			.setReason(teven.getReason() != null && !teven.getReason().isEmpty() ? teven.getReason() : null)
			.setTrxCode(teven.getTrxcode())
			.setSubmittedBy(teven.getSubmittedBy() != null ? teven.getSubmittedBy().getId() : null)
			.setSubmittedNameBy(teven.getSubmittedNameBy())
			.setSubmittedAt(teven.getSubmittedAt())
			.setApproved1By(teven.getApproved1By() != null ? teven.getApproved1By().getId() : null)
			.setApprovedByPernr1(teven.getApproved1By() != null ? teven.getApprovedByPernr1().toString() : null)
			.setApprovedName1By(teven.getApprovedName1By())
			.setApproved1At(teven.getApproved1At())
			.setApproved2By(teven.getApproved2By() != null ? teven.getApproved2By().getId() : null)
			.setApprovedByPernr2(teven.getApproved2By() != null ? teven.getApprovedByPernr2().toString() : null)
			.setApprovedName2By(teven.getApprovedName2By())
			.setApproved2At(teven.getApproved2At())
			.setApproved3By(teven.getApproved3By() != null ? teven.getApproved3By().getId() : null)
			.setApprovedByPernr3(teven.getApproved3By() != null ? teven.getApprovedByPernr3().toString() : null)
			.setApprovedName3By(teven.getApprovedName3By())
			.setApproved3At(teven.getApproved3At())
			.setApproved4By(teven.getApproved4By() != null ? teven.getApproved4By().getId() : null)
			.setApprovedByPernr4(teven.getApproved4By() != null ? teven.getApprovedByPernr4().toString() : null)
			.setApprovedName4By(teven.getApprovedName4By())
			.setApproved4At(teven.getApproved2At())
			.setApproved5By(teven.getApproved5By() != null ? teven.getApproved5By().getId() : null)
			.setApprovedByPernr5(teven.getApproved5By() != null ? teven.getApprovedByPernr5().toString() : null)
			.setApprovedName5By(teven.getApprovedName5By())
			.setApproved5At(teven.getApproved5At())
			.setApproved6By(teven.getApproved6By() != null ? teven.getApproved6By().getId() : null)
			.setApprovedName6By(teven.getApprovedName6By())
			.setApproved6At(teven.getApproved6At())
			.setApprovalNote(teven.getApprovalNote());
			
		}
	}
	

	/**
	 * GET Employee SSN.
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	private TEVENResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	
	@JsonProperty("tevenid")
	public String getTevenid() {
		return tevenid;
	}
	
	
	private TEVENResponseWrapper setTevenid(String tevenid) {
		this.tevenid = tevenid;
		return this;
	}


	@JsonProperty("pdsnr")
	public Long getPdsnr() {
		return pdsnr;
	}
	
	private TEVENResponseWrapper setPdsnr(Long pdsnr) {
		this.pdsnr = pdsnr;
		return this;
	}
	
	
	@JsonProperty("zausw")
	public Long getZausw() {
		return zausw;
	}
	
	private TEVENResponseWrapper setZausw(Long zausw) {
		this.zausw = zausw;
		return this;
	}
	
	
		
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("ldate")
	public String getLdate() {
		return ldate;
	}
	
	private TEVENResponseWrapper setLdate(String ldate) {
		this.ldate = ldate;
		return this;
	}
	
	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	@JsonProperty("ltime")
	public String getLtime() {
		return ltime;
	}
	
	private TEVENResponseWrapper setLtime(String ltime) {
		this.ltime = ltime;
		return this;
	}
	
	
	
	@JsonProperty("erdat")
	public String getErdat() {
		return erdat;
	}
	
	private TEVENResponseWrapper setErdat(String erdat) {
		this.erdat = erdat;
		return this;
	}
	
	
	
	@JsonProperty("ertim")
	public String getErtim() {
		return ertim;
	}
	
	private TEVENResponseWrapper setErtim(String ertim) {
		this.ertim = ertim;
		return this;
	}
	
	/**
	 * @return the pmbde
	 */
	@JsonProperty("pmbde")
	public String getPmbde() {
		return pmbde;
	}

	/**
	 * @param pmbde the pmbde to set
	 */
	public TEVENResponseWrapper setPmbde(String pmbde) {
		this.pmbde = pmbde;
		return this;
	}

	/**
	 * GET Subtype name.
	 * 
	 * @return
	 */
	@JsonProperty("satza")
	public String getSatza() {
		return satza;
	}
	
	private TEVENResponseWrapper setSatza(String satza) {
		this.satza = satza;
		return this;
	}
	

	@JsonProperty("terid")
	public String getTerid() {
		return terid;
	}
	
	private TEVENResponseWrapper setTerid(String terid) {
		this.terid = terid;
		return this;
	}
	
	
	@JsonProperty("abwgr")
	public String getAbwgr() {
		return abwgr;
	}
	
	private TEVENResponseWrapper setAbwgr(String abwgr) {
		this.abwgr = abwgr;
		return this;
	}
	
	
	@JsonProperty("exlga")
	public String getExlga() {
		return exlga;
	}
	
	private TEVENResponseWrapper setExlga(String exlga) {
		this.exlga = exlga;
		return this;
	}
	
	
	@JsonProperty("zeinh")
	public String getZeinh() {
		return zeinh;
	}
	
	private TEVENResponseWrapper setZeinh(String zeinh) {
		this.zeinh = zeinh;
		return this;
	}
	
	/**
	 * GET Quota Text.
	 * 
	 * @return
	 */
	@JsonProperty("uname")
	public String getUname() {
		return uname;
	}
	
	private TEVENResponseWrapper setUname(String uname) {
		this.uname = uname;
		return this;
	}
	
	
	
	@JsonProperty("origf")
	public String getOrigf() {
		return origf;
	}
	
	private TEVENResponseWrapper setOrigf(String origf) {
		this.origf = origf;
		return this;
	}
	
	
	@JsonProperty("dallf")
	public String getDallf() {
		return dallf;
	}
	
	private TEVENResponseWrapper setDallf(String dallf) {
		this.dallf = dallf;
		return this;
	}
	
	
	@JsonProperty("pdc_usrup")
	public String getPdcUsrup() {
		return pdcUsrup;
	}
	
	private TEVENResponseWrapper setPdcUsrup(String pdcUsrup) {
		this.pdcUsrup = pdcUsrup;
		return this;
	}
	
	
	
	
	/**
	 * GET Quota Number.
	 * 
	 * @return
	 */
	@JsonProperty("hrazl")
	public Double getHrazl() {
		return hrazl;
	}
	
	private TEVENResponseWrapper setHrazl(Double hrazl) {
		this.hrazl = hrazl;
		return this;
	}
	
	/**
	 * GET Deduction.
	 * 
	 * @return
	 */
	@JsonProperty("hrbet")
	public Double getHrbet() {
		return hrbet;
	}
	
	private TEVENResponseWrapper setHrbet(Double hrbet) {
		this.hrbet = hrbet;
		return this;
	}
	
	
	
	
	
	/**
	 * GET Deduction From (Date).
	 * 
	 * @return
	 */
	@JsonProperty("aedtm")
	public String getAedtm() {
		return aedtm;
	}
	
	private TEVENResponseWrapper setAedtm(String aedtm) {
		this.aedtm = aedtm;
		return this;
	}
	
	/**
	 * GET status process time event.
	 * 
	 * @return
	 */
	@JsonProperty("process")
	public Long getProcess() {
		return process;
	}
	
	private TEVENResponseWrapper setProcess(Long process) {
		this.process = process;
		return this;
	}
	
	/**
	 * GET reason time event to edit item.
	 * 
	 * @return
	 */
	@JsonProperty("reason")
	public String getReason() {
		return reason;
	}
	
	private TEVENResponseWrapper setReason(String reason) {
		this.reason = reason;
		return this;
	}

	/**
	 * GET transaction code.
	 * 
	 * @return
	 */
	@JsonProperty("trxcode")
	public String getTrxCode() {
		return trxCode;
	}

	public TEVENResponseWrapper setTrxCode(String trxCode) {
		this.trxCode = trxCode;
		return this;
	}

	@JsonProperty("submitted_by")
	public String getSubmittedBy() {
		return submittedBy;
	}

	public TEVENResponseWrapper setSubmittedBy(String submittedBy) {
		this.submittedBy = submittedBy;
		return this;
	}

	@JsonProperty("submittedName_by")
	public String getSubmittedNameBy() {
		return submittedNameBy;
	}

	public TEVENResponseWrapper setSubmittedNameBy(String submittedNameBy) {
		this.submittedNameBy = submittedNameBy;
		return this;
	}

	public Date getSubmittedAt() {
		return submittedAt;
	}

	public TEVENResponseWrapper setSubmittedAt(Date submittedAt) {
		this.submittedAt = submittedAt;
		return this;
	}

	@JsonProperty("approved1_by")
	public String getApproved1By() {
		return approved1By;
	}

	public TEVENResponseWrapper setApproved1By(String approved1By) {
		this.approved1By = approved1By;
		return this;
	}
	
	@JsonProperty("approvedByPernr1")
	public String getApprovedByPernr1() {
		return approvedByPernr1;
	}
	
	public TEVENResponseWrapper setApprovedByPernr1(String approvedByPernr1) {
		this.approvedByPernr1 = approvedByPernr1;
		return this;
	}

	@JsonProperty("approvedName1_by")
	public String getApprovedName1By() {
		return approvedName1By;
	}

	public TEVENResponseWrapper setApprovedName1By(String approvedName1By) {
		this.approvedName1By = approvedName1By;
		return this;
	}

	public Date getApproved1At() {
		return approved1At;
	}

	public TEVENResponseWrapper setApproved1At(Date approved1At) {
		this.approved1At = approved1At;
		return this;
	}

	@JsonProperty("approved2_by")
	public String getApproved2By() {
		return approved2By;
	}

	public TEVENResponseWrapper setApproved2By(String approved2By) {
		this.approved2By = approved2By;
		return this;
	}

	@JsonProperty("approvedByPernr2")
	public String getApprovedByPernr2() {
		return approvedByPernr2;
	}

	public TEVENResponseWrapper setApprovedByPernr2(String approvedByPernr2) {
		this.approvedByPernr2 = approvedByPernr2;
		return this;
	}

	@JsonProperty("approvedName2_by")
	public String getApprovedName2By() {
		return approvedName2By;
	}

	public TEVENResponseWrapper setApprovedName2By(String approvedName2By) {
		this.approvedName2By = approvedName2By;
		return this;
	}

	public Date getApproved2At() {
		return approved2At;
	}

	public TEVENResponseWrapper setApproved2At(Date approved2At) {
		this.approved2At = approved2At;
		return this;
	}

	@JsonProperty("approved3_by")
	public String getApproved3By() {
		return approved3By;
	}

	public TEVENResponseWrapper setApproved3By(String approved3By) {
		this.approved3By = approved3By;
		return this;
	}

	@JsonProperty("approvedByPernr3")
	public String getApprovedByPernr3() {
		return approvedByPernr3;
	}

	public TEVENResponseWrapper setApprovedByPernr3(String approvedByPernr3) {
		this.approvedByPernr3 = approvedByPernr3;
		return this;
	}

	@JsonProperty("approvedName3_by")
	public String getApprovedName3By() {
		return approvedName3By;
	}

	public TEVENResponseWrapper setApprovedName3By(String approvedName3By) {
		this.approvedName3By = approvedName3By;
		return this;
	}

	public Date getApproved3At() {
		return approved3At;
	}

	public TEVENResponseWrapper setApproved3At(Date approved3At) {
		this.approved3At = approved3At;
		return this;
	}

	@JsonProperty("approved4_by")
	public String getApproved4By() {
		return approved4By;
	}

	public TEVENResponseWrapper setApproved4By(String approved4By) {
		this.approved4By = approved4By;
		return this;
	}

	@JsonProperty("approvedByPernr4")
	public String getApprovedByPernr4() {
		return approvedByPernr4;
	}

	public TEVENResponseWrapper setApprovedByPernr4(String approvedByPernr4) {
		this.approvedByPernr4 = approvedByPernr4;
		return this;
	}

	@JsonProperty("approvedName4_by")
	public String getApprovedName4By() {
		return approvedName4By;
	}

	public TEVENResponseWrapper setApprovedName4By(String approvedName4By) {
		this.approvedName4By = approvedName4By;
		return this;
	}

	public Date getApproved4At() {
		return approved4At;
	}

	public TEVENResponseWrapper setApproved4At(Date approved4At) {
		this.approved4At = approved4At;
		return this;
	}

	@JsonProperty("approved5_by")
	public String getApproved5By() {
		return approved5By;
	}

	public TEVENResponseWrapper setApproved5By(String approved5By) {
		this.approved5By = approved5By;
		return this;
	}

	@JsonProperty("approvedByPernr5")
	public String getApprovedByPernr5() {
		return approvedByPernr5;
	}

	public TEVENResponseWrapper setApprovedByPernr5(String approvedByPernr5) {
		this.approvedByPernr5 = approvedByPernr5;
		return this;
	}

	@JsonProperty("approvedName5_by")
	public String getApprovedName5By() {
		return approvedName5By;
	}

	public TEVENResponseWrapper setApprovedName5By(String approvedName5By) {
		this.approvedName5By = approvedName5By;
		return this;
	}

	public Date getApproved5At() {
		return approved5At;
	}

	public TEVENResponseWrapper setApproved5At(Date approved5At) {
		this.approved5At = approved5At;
		return this;
	}

	@JsonProperty("approved6_by")
	public String getApproved6By() {
		return approved6By;
	}

	public TEVENResponseWrapper setApproved6By(String approved6By) {
		this.approved6By = approved6By;
		return this;
	}

	@JsonProperty("approvedName6_by")
	public String getApprovedName6By() {
		return approvedName6By;
	}

	public TEVENResponseWrapper setApprovedName6By(String approvedName6By) {
		this.approvedName6By = approvedName6By;
		return this;
	}

	public Date getApproved6At() {
		return approved6At;
	}

	public TEVENResponseWrapper setApproved6At(Date approved6At) {
		this.approved6At = approved6At;
		return this;
	}

	@JsonProperty("approvalNote")
	public String getApprovalNote() {
		return approvalNote;
	}

	public TEVENResponseWrapper setApprovalNote(String approvalNote) {
		this.approvalNote = approvalNote;
		return this;
	}

	@JsonProperty("divisionName")
	public String getDivisionName() {
		return divisionName;
	}

	public TEVENResponseWrapper setDivisionName(String divisionName) {
		this.divisionName = divisionName;
		return this;
	}

}