package com.abminvestama.hcms.rest.api.dto.request;


import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public class CaTransactionRequestWrapper extends ITRequestWrapper {
	
	private static final long serialVersionUID = 2590933218644730708L;

	private CaReqHdrRequestWrapper trxHdr;

	private CaReqDtlRequestWrapper[] trxItm;
	
	@JsonProperty("trx_header")		
	public CaReqHdrRequestWrapper getTrxHdr() {
		return trxHdr;
	}
	
	@JsonProperty("trx_item")
	public CaReqDtlRequestWrapper[] getTrxItm() {
		return trxItm;
	}

}