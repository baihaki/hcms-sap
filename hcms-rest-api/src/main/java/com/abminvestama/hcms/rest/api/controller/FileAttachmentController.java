package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.service.util.FileAttachmentService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.FileAttachmentResponseWrapper;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@RestController
@RequestMapping("/api/v2/file_attachment")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class FileAttachmentController extends AbstractResource {

	private FileAttachmentService fileAttachmentService;
	
	@Autowired
	void setFileAttachmentService(FileAttachmentService fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<FileAttachmentResponseWrapper> fetchBase64(@RequestParam(value = "path", required = true) String path) throws Exception {
		
		String base64 = fileAttachmentService.open(path);
		
		APIResponseWrapper<FileAttachmentResponseWrapper> response = new APIResponseWrapper<>();
		FileAttachmentResponseWrapper fileAttachment = new FileAttachmentResponseWrapper(path, base64);
		response.setData(fileAttachment);
		Link self = linkTo(methodOn(FileAttachmentController.class).fetchBase64(path)).withSelfRel();
		response.add(self);
		
		return response;
	}				
}