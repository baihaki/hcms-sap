package com.abminvestama.hcms.rest.api.dto.response;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 2.0.0
 * @version 2.0.0
 * @author anasuya (anasuyahirai@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class DemographyCountResponseWrapper extends ResourceSupport {
	
	private long count;
	
	
	private DemographyCountResponseWrapper() {}
	
	public DemographyCountResponseWrapper(Long num) {
		if (num == null) {
			new DemographyCountResponseWrapper();
		} else {
			this
			.setCount(num);
						
		}
	}
	
	/**
	 * GET Count.
	 * 
	 * @return
	 */
	@JsonProperty("count")
	public long getCount() {
		return count;
	}
	
	private DemographyCountResponseWrapper setCount(Long count) {
		this.count = count;
		return this;
	}
	
}