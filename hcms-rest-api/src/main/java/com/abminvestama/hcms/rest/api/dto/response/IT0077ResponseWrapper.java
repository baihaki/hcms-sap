package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.IT0077;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@JsonInclude(Include.NON_NULL)
public class IT0077ResponseWrapper extends ResourceSupport {
	
	private long pernr;
	private Date endda;
	private Date begda;

	private String molga;
	private String racky;
	private String ltext;

	private IT0077ResponseWrapper() {}
	
	public IT0077ResponseWrapper(IT0077 it0077) {
		if (it0077 == null) {
			new IT0077ResponseWrapper();
		} else {
			
			this
			.setPernr(it0077.getId().getPernr())
			.setEndda(it0077.getId().getEndda()).setBegda(it0077.getId().getBegda())
			.setMolga(it0077.getRacky() != null ? it0077.getRacky().getId().getMolga() : StringUtils.EMPTY)
			.setRacky(it0077.getRacky() != null ? it0077.getRacky().getId().getRacky() : StringUtils.EMPTY)
			.setLtext(it0077.getRacky() != null ? it0077.getRacky().getLtext() : StringUtils.EMPTY);
		}
	}
	
	/**
	 * GET Employee SSN/ID.
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	private IT0077ResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	private IT0077ResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}
	
	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	private IT0077ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * GET Country Grouping Code.
	 * 
	 * @return
	 */
	@JsonProperty("country_grouping_code")
	public String getMolga() {
		return molga;
	}
	
	private IT0077ResponseWrapper setMolga(String molga) {
		this.molga = molga;
		return this;
	}
	
	/**
	 * GET Ethnic Origin Code.
	 * 
	 * @return
	 */
	@JsonProperty("ethnic_origin_code")
	public String getRacky() {
		return racky;
	}
	
	private IT0077ResponseWrapper setRacky(String racky) {
		this.racky = racky;
		return this;
	}
	
	/**
	 * GET Ethnic Name/Description.
	 * 
	 * @return
	 */
	@JsonProperty("ethnic_name")
	public String getLtext() {
		return ltext;
	}
	
	private IT0077ResponseWrapper setLtext(String ltext) {
		this.ltext = ltext;
		return this;
	}
}