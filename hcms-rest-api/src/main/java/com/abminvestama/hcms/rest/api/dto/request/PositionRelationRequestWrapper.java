package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class PositionRelationRequestWrapper {

	private String positionRelationTypeId;
	private String fromPositionId;
	private String toPositionId;
	private String validFrom;
	private String validThru;
	
	private boolean isEditRequest;
	
	public PositionRelationRequestWrapper() {}
	
	public PositionRelationRequestWrapper(String positionRelationTypeId, String fromPositionId,
			String toPositionId) {
		this.positionRelationTypeId = positionRelationTypeId;
		this.fromPositionId = fromPositionId;
		this.toPositionId = toPositionId;
	}
	
	@JsonProperty("position_relation_type_id")
	public String getPositionRelationTypeId() {
		return positionRelationTypeId;
	}
	
	public void setPositionRelationTypeId(String positionRelationTypeId) {
		this.positionRelationTypeId = positionRelationTypeId;
	}
	
	@JsonProperty("from_position_id")
	public String getFromPositionId() {
		return fromPositionId;
	}
	
	public void setFromPositionId(String fromPositionId) {
		this.fromPositionId = fromPositionId;
	}
	
	@JsonProperty("to_position_id")
	public String getToPositionId() {
		return toPositionId;
	}
	
	public void setToPositionId(String toPositionId) {
		this.toPositionId = toPositionId;
	}
	
	@JsonProperty("valid_from")
	public String getValidFrom() {
		return validFrom;
	}
	
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}
	
	@JsonProperty("valid_thru")
	public String getValidThru() {
		return validThru;
	}
	
	public void setValidThru(String validThru) {
		this.validThru = validThru;
	}
	
	@JsonProperty("is_edit")
	public boolean isEditRequest() {
		return isEditRequest;
	}
	
	public void setIsEditRequest(boolean isEditRequest) {
		this.isEditRequest = isEditRequest;
	}
}