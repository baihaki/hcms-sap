package com.abminvestama.hcms.rest.api.dto.response;


import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class ZmedQuotausedResponseWrapper extends ResourceSupport {

	
	private String gjahr;	
	private String bukrs;
	private String kodequo;
	private String pernr;
	private String quotamt;
	
private ZmedQuotausedResponseWrapper() {}
	
	public ZmedQuotausedResponseWrapper(ZmedQuotaused zmedQuotaused) {
		if (zmedQuotaused == null) {
			new ZmedQuotausedResponseWrapper();
		} else {
			this
			//.setBukrs(zmedQuotaused.getBukrs() != null ? StringUtils.defaultString(zmedQuotaused.getBukrs().getBukrs(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setKodequo(StringUtils.defaultString(zmedQuotaused.getKodequo(), StringUtils.EMPTY))
			.setBukrs(StringUtils.defaultString(zmedQuotaused.getBukrs().getBukrs(), StringUtils.EMPTY))
			.setPernr(StringUtils.defaultString(zmedQuotaused.getPernr().toString(), StringUtils.EMPTY))
			.setGjahr(StringUtils.defaultString(zmedQuotaused.getGjahr().toString(), StringUtils.EMPTY))
			.setQuotamt(StringUtils.defaultString(zmedQuotaused.getQuotamt().toString(), StringUtils.EMPTY));			
		}
	}
	

	@JsonProperty("gjahr")
	public String getgjahr() {
		return gjahr;
	}
	
	private ZmedQuotausedResponseWrapper setGjahr(String gjahr) {
		this.gjahr = gjahr;
		return this;
	}
	
	
	@JsonProperty("company_code")
	public String getBukrs() {
		return bukrs;
	}
	
	private ZmedQuotausedResponseWrapper setBukrs(String bukrs) {
		this.bukrs = bukrs;
		return this;
	}
	


	
	
	@JsonProperty("quota_type")
	public String getKodequo() {
		return kodequo;
	}
	
	private ZmedQuotausedResponseWrapper setKodequo(String kodequo) {
		this.kodequo = kodequo;
		return this;
	}
	


	
	
	@JsonProperty("pernr")
	public String getPernr() {
		return pernr;
	}
	
	private ZmedQuotausedResponseWrapper setPernr(String pernr) {
		this.pernr = pernr;
		return this;
	}
	
	
	@JsonProperty("quotamt")
	public String getQuotamt() {
		return quotamt;
	}
	
	private ZmedQuotausedResponseWrapper setQuotamt(String quotamt) {
		this.quotamt = quotamt;
		return this;
	}
	

	
}