package com.abminvestama.hcms.rest.api.validator;

import java.util.*;
import javax.naming.*;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import com.abminvestama.hcms.core.model.constant.MainConstants;

/**
 * 
 * @author wijanarko777@gmx.com
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class LdapValidator {
	private static Logger logger = LoggerFactory.getLogger(LdapValidator.class);
	
	private String message;

	public boolean ldapAuthentication(String username, String passWord, Environment envProp) {
		Hashtable<String, Object> env = new Hashtable<String, Object>();
		String domain = envProp.getProperty("ldap.domain"); // domain name of AD server (ex:qtech.co.id), not domain name of website
		String initContextFactory = envProp.getProperty("ldap.context-factory"); // initial context factory
		String providerUrl = envProp.getProperty("ldap.dns-hostname");
		String securityAuthentication = envProp.getProperty("ldap.security-authentication");
		String securityPrincipal = username + "@" + domain; // username + "@" + "qtech.co.id";
		LdapContext ctx = null;
		
		setMessage(1, "DOMAIN: " + domain);
		setMessage(1, "INITIAL_CONTEXT_FACTORY: " + initContextFactory);
		env.put(Context.INITIAL_CONTEXT_FACTORY, initContextFactory);
		
		// IP and port of LDAP
		setMessage(1, "PROVIDER_URL: " + providerUrl);
		env.put(Context.PROVIDER_URL, providerUrl);
		
		try {
			
			setMessage(1, "Initial LDAP CONTEXT");
			ctx = new InitialLdapContext(env, null);
			
			//Authenticate
			setMessage(1, "SECURITY_AUTHENTICATION: " + securityAuthentication);
			ctx.addToEnvironment(Context.SECURITY_AUTHENTICATION, securityAuthentication);
			
			// replace with user DN
			setMessage(1, "SECURITY_PRINCIPAL: " + securityPrincipal);
			ctx.addToEnvironment(Context.SECURITY_PRINCIPAL, securityPrincipal);
			
			//setMessage(1, "SECURITY_CREDENTIALS: " + passWord);
			passWord = passWord != null && !passWord.isEmpty() ? passWord : "ATREUS.GLOBAL";
			ctx.addToEnvironment(Context.SECURITY_CREDENTIALS, passWord);
			ctx.reconnect(null); // throws NamingException if creds wrong
			
			setMessage(0, "Authentication Success!");
			closeContext(ctx);
			return true;
		} catch (AuthenticationException ex) { 
			setMessage(1, "Authentication Exception: " + ex.toString());
			closeContext(ctx);
			return false;
		} catch (NamingException ex) {
			setMessage(1, "Authentication failed: " + ex.toString());
			closeContext(ctx);
			return false;
		} catch (Exception ex) {
			setMessage(2, "Authentication failed ! Exception: " + ex.toString());
			closeContext(ctx);
			return false;
		}
	}

	public String getMessage() {
		return message;
	}

	private void setMessage(int errorStatus, String message) {
		this.message = message;

		if(errorStatus > 1) {
			logger.debug(message);
		}
		else {
			logger.info(message);
		}
	}

	private void closeContext(Context ctx) {
		try 
	      { 
	         if (ctx != null) 
	         { 
	            ctx.close(); 
	         } 
	      } 
	      catch (NamingException e) 
	      { 
	         logger.warn("Exception occurred when tried to close context", e); 
	      } 
	}
}
