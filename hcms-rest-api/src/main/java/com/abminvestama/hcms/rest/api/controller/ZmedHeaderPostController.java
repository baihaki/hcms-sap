package com.abminvestama.hcms.rest.api.controller;



import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.ZmedHeaderPost;
import com.abminvestama.hcms.core.model.entity.ZmedHeaderPost;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedHeaderPostCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedHeaderPostQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedHeaderPostRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.ZmedHeaderPostRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.ZmedHeaderPostResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ZmedHeaderPostResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.ZmedHeaderPostRequestBuilderUtil;




@RestController
@RequestMapping("/api/v1/medical_header_post")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ZmedHeaderPostController extends AbstractResource {
	
	private ZmedHeaderPostCommandService zmedHeaderPostCommandService;
	private ZmedHeaderPostQueryService zmedHeaderPostQueryService;
	private ZmedHeaderPostRequestBuilderUtil zmedHeaderPostRequestBuilderUtil;
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	@Autowired
	void setZmedHeaderPostCommandService(ZmedHeaderPostCommandService zmedHeaderPostCommandService) {
		this.zmedHeaderPostCommandService = zmedHeaderPostCommandService;
	}
	
	@Autowired
	void setZmedHeaderPostQueryService(ZmedHeaderPostQueryService zmedHeaderPostQueryService) {
		this.zmedHeaderPostQueryService = zmedHeaderPostQueryService;
	}
	
	@Autowired
	void setZmedHeaderPostRequestBuilderUtil(ZmedHeaderPostRequestBuilderUtil zmedHeaderPostRequestBuilderUtil) {
		this.zmedHeaderPostRequestBuilderUtil = zmedHeaderPostRequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	
	
	
	
	

	
	
	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated ZmedHeaderPostRequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> zmedHeaderPostEntity = hcmsEntityQueryService.findByEntityName(ZmedHeaderPost.class.getSimpleName());
			if (!zmedHeaderPostEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + ZmedHeaderPost.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<ZmedHeaderPost, ZmedHeaderPostRequestWrapper, String> newObjectContainer 
					= zmedHeaderPostRequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				
				Optional<ZmedHeaderPost> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
				//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = zmedHeaderPostCommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create ZmedHeaderPost data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("objType=" + savedEntity.get().getId().getObjType());
				key.append(",objKey=" + savedEntity.get().getId().getObjKey());
				key.append(",objSys=" + savedEntity.get().getId().getObjSys());
				
				EventLog eventLog = new EventLog(zmedHeaderPostEntity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				
				ZmedHeaderPostResponseWrapper responseWrapper = new ZmedHeaderPostResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(ZmedHeaderPostController.class).create(request, result)).withSelfRel());		
		return response;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated ZmedHeaderPostRequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			ZmedHeaderPostResponseWrapper responseWrapper = new ZmedHeaderPostResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<ZmedHeaderPost> zmedHeaderPost = zmedHeaderPostQueryService.findOneByCompositeKey(
							request.getObjType(),
							request.getObjKey(), request.getObjSys());
					if (!zmedHeaderPost.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					RequestObjectComparatorContainer<ZmedHeaderPost, ZmedHeaderPostRequestWrapper, String> updatedContainer = zmedHeaderPostRequestBuilderUtil.compareAndReturnUpdatedData(request, zmedHeaderPost.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
				//	Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				//
				//	if (updatedContainer.getEntity().isPresent()) {
				//		updatedContainer.getEntity().get().setSeqnr(zmedHeaderPost.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
				//		zmedHeaderPost = zmedHeaderPostCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				//	}
				
				//	if (!zmedHeaderPost.isPresent()) {
				//		throw new CannotPersistException("Cannot update ZmedHeaderPost (Personal Information) data. Please check your data!");
				//	}
				
					responseWrapper = new ZmedHeaderPostResponseWrapper(zmedHeaderPost.get());
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(ZmedHeaderPostController.class).edit(request, result)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}
	
	
}
