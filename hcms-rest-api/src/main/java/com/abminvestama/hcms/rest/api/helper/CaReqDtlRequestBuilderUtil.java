package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.CaReqDtlKey;
import com.abminvestama.hcms.core.model.entity.CaReqDtl;
import com.abminvestama.hcms.core.service.api.business.query.CaReqDtlQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.CaReqDtlRequestWrapper;


/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("caReqDtlRequestBuilderUtil")
@Transactional(readOnly = true)
public class CaReqDtlRequestBuilderUtil {
	

	private CaReqDtlQueryService caReqDtlQueryService;
	
	@Autowired
	void setCaReqDtlQueryService(CaReqDtlQueryService caReqDtlQueryService) {
		this.caReqDtlQueryService = caReqDtlQueryService;
	}
	
	public RequestObjectComparatorContainer<CaReqDtl, CaReqDtlRequestWrapper, String> compareAndReturnUpdatedData(CaReqDtlRequestWrapper requestPayload, CaReqDtl caReqDtlDB) {
		
		RequestObjectComparatorContainer<CaReqDtl, CaReqDtlRequestWrapper, String> objectContainer = null; 
		
	if (caReqDtlDB == null) {
		caReqDtlDB = new CaReqDtl();
		
		try {
			//------------------------------------------------

			Optional<CaReqDtl> existingCaReqDtl = caReqDtlQueryService.findOneByCompositeKey(
					StringUtils.defaultString(requestPayload.getReqNo(), ""), requestPayload.getItemNo());
			
			if (existingCaReqDtl.isPresent()) {
				throw new DataViolationException("Duplicate CaReqDtl Data for [Request Number=" + requestPayload.getReqNo()
						+ ", Item Number=" + requestPayload.getItemNo());
			}
			caReqDtlDB.setId(
					new CaReqDtlKey(StringUtils.defaultString(requestPayload.getReqNo(), ""), requestPayload.getItemNo()));
			
			//------------------------------------------------
			
			caReqDtlDB.setDescription(requestPayload.getDescription());
			caReqDtlDB.setPricePerUnit(requestPayload.getPricePerUnit());
			caReqDtlDB.setQuantity(requestPayload.getQuantity());
			caReqDtlDB.setAmount(requestPayload.getAmount());
			caReqDtlDB.setRemarks(requestPayload.getRemarks());
			
		} catch (Exception ignored) {
			System.err.println(ignored);
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(caReqDtlDB, requestPayload);
		
	} else {
		
		if (StringUtils.isNotBlank(requestPayload.getDescription())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDescription(), caReqDtlDB.getDescription())) {
				caReqDtlDB.setDescription(requestPayload.getDescription());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (requestPayload.getPricePerUnit() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getPricePerUnit(), caReqDtlDB.getPricePerUnit())) {
				caReqDtlDB.setPricePerUnit(requestPayload.getPricePerUnit());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (requestPayload.getQuantity() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getQuantity(), caReqDtlDB.getQuantity())) {
				caReqDtlDB.setQuantity(requestPayload.getQuantity());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (requestPayload.getAmount() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getAmount(), caReqDtlDB.getAmount())) {
				caReqDtlDB.setAmount(requestPayload.getAmount());
				requestPayload.setDataChanged(true);
			}
		}

		if (StringUtils.isNotBlank(requestPayload.getRemarks())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getRemarks(), caReqDtlDB.getRemarks())) {
				caReqDtlDB.setRemarks(requestPayload.getRemarks());
				requestPayload.setDataChanged(true);
			}
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(caReqDtlDB, requestPayload);	
				
	}
	
	JSONObject json = new JSONObject(requestPayload);
	objectContainer.setEventData(json.toString());
	
	return objectContainer;
}
}
