package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add fields: address, phone</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add attach1Type, attach1Content, attach1Filename field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public class IT0021RequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = 6742574229923918283L;
	
	private String famsa;
	private String fgbdt;
	private String fgbld;
	private String fanat;
	private String fasex;
	private String favor;
	private String fanam;
	private String fgbot;
	private String fcnam;
	private int fknzn;
	private String fnmzu;
	private String title;

	private String attach1Type;
	private String attach1Content;
	private String attach1Filename;
	
	private String address;
	private String phone;
	
	@JsonProperty("family_member")
	public String getFamsa() {
		return famsa;
	}
	
	@JsonProperty("date_of_birth")
	public String getFgbdt() {
		return fgbdt;
	}
	
	@JsonProperty("country_of_birth")
	public String getFgbld() {
		return fgbld;
	}
	
	@JsonProperty("nationality")
	public String getFanat() {
		return fanat;
	}
	
	@JsonProperty("gender")
	public String getFasex() {
		return fasex;
	}
	
	@JsonProperty("first_name")
	public String getFavor() {
		return favor;
	}
	
	@JsonProperty("last_name")
	public String getFanam() {
		return fanam;
	}
	
	@JsonProperty("birth_place")
	public String getFgbot() {
		return fgbot;
	}
	
	@JsonProperty("full_name")
	public String getFcnam() {
		return fcnam;
	}
	
	@JsonProperty("ind_name")
	public int getFknzn() {
		return fknzn;
	}
	
	@JsonProperty("other_title")
	public String getFnmzu() {
		return fnmzu;
	}
	
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}
	
	@JsonProperty("attachment1_type")
	public String getAttach1Type() {
		return attach1Type;
	}
	
	@JsonProperty("attachment1_base64")
	public String getAttach1Content() {
		return attach1Content;
	}
	
	@JsonProperty("attachment1_filename")
	public String getAttach1Filename() {
		return attach1Filename;
	}
	
	@JsonProperty("address")
	public String getAddress() {
		return address;
	}
	
	@JsonProperty("phone")
	public String getPhone() {
		return phone;
	}
}