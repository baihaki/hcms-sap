package com.abminvestama.hcms.rest.api.controller;



import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedMedtypeCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedMedtypeQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedMedtypeRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.ZmedMedtypeRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.ZmedMedtypeResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ZmedMedtypeResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.ZmedMedtypeRequestBuilderUtil;




@RestController
@RequestMapping("/api/v1/medical_med_type")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ZmedMedtypeController extends AbstractResource {
	
	private ZmedMedtypeCommandService zmedMedtypeCommandService;
	private ZmedMedtypeQueryService zmedMedtypeQueryService;
	private ZmedMedtypeRequestBuilderUtil zmedMedtypeRequestBuilderUtil;
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	@Autowired
	void setZmedMedtypeCommandService(ZmedMedtypeCommandService zmedMedtypeCommandService) {
		this.zmedMedtypeCommandService = zmedMedtypeCommandService;
	}
	
	@Autowired
	void setZmedMedtypeQueryService(ZmedMedtypeQueryService zmedMedtypeQueryService) {
		this.zmedMedtypeQueryService = zmedMedtypeQueryService;
	}
	
	@Autowired
	void setZmedMedtypeRequestBuilderUtil(ZmedMedtypeRequestBuilderUtil zmedMedtypeRequestBuilderUtil) {
		this.zmedMedtypeRequestBuilderUtil = zmedMedtypeRequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	
	
	
	

	
	
	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated ZmedMedtypeRequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> zmedMedtypeEntity = hcmsEntityQueryService.findByEntityName(ZmedMedtype.class.getSimpleName());
			if (!zmedMedtypeEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + ZmedMedtype.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<ZmedMedtype, ZmedMedtypeRequestWrapper, String> newObjectContainer 
					= zmedMedtypeRequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				
				Optional<ZmedMedtype> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
				//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = zmedMedtypeCommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create ZmedMedtype data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("bukrs=" + savedEntity.get().getId().getBukrs());
				key.append(",kodemed=" + savedEntity.get().getId().getKodemed());
				key.append(",datab=" + savedEntity.get().getId().getDatab());
				key.append(",datbi=" + savedEntity.get().getId().getDatbi());
				/*StringBuilder key = new StringBuilder("bukrs=" + request.getBukrs());
				key.append(",kodemed=" + request.getKodemed());
				key.append(",kodecos=" + request.getKodecos());
				key.append(",datab=" + request.getDatab());
				key.append(",datbi=" + request.getDatbi());*/
				EventLog eventLog = new EventLog(zmedMedtypeEntity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				
				ZmedMedtypeResponseWrapper responseWrapper = new ZmedMedtypeResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(ZmedMedtypeController.class).create(request, result)).withSelfRel());		
		return response;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated ZmedMedtypeRequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			ZmedMedtypeResponseWrapper responseWrapper = new ZmedMedtypeResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<ZmedMedtype> zmedMedtype = zmedMedtypeQueryService.findOneByCompositeKey(
							request.getBukrs(),
							request.getKodemed(), 
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getDatab()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getDatbi()));
					if (!zmedMedtype.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					RequestObjectComparatorContainer<ZmedMedtype, ZmedMedtypeRequestWrapper, String> updatedContainer = zmedMedtypeRequestBuilderUtil.compareAndReturnUpdatedData(request, zmedMedtype.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
				//	Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				//
				//	if (updatedContainer.getEntity().isPresent()) {
				//		updatedContainer.getEntity().get().setSeqnr(zmedMedtype.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
				//		zmedMedtype = zmedMedtypeCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				//	}
				
				//	if (!zmedMedtype.isPresent()) {
				//		throw new CannotPersistException("Cannot update ZmedMedtype (Personal Information) data. Please check your data!");
				//	}
				
					responseWrapper = new ZmedMedtypeResponseWrapper(zmedMedtype.get());
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(ZmedMedtypeController.class).edit(request, result)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/bukrs/{bukrs}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<ZmedMedtypeResponseWrapper>> findByBukrs(@PathVariable String bukrs, 
			@RequestParam(value = "datbi", required = false) String datbi, 
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		ArrayData<ZmedMedtypeResponseWrapper> bunchOfZmedMedtype = new ArrayData<>();
		APIResponseWrapper<ArrayData<ZmedMedtypeResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfZmedMedtype);
		
		try {
			List<ZmedMedtype> listOfZmedMedtype = new ArrayList<>();
			
			if (StringUtils.isNotEmpty(datbi)) {
				listOfZmedMedtype = zmedMedtypeQueryService.findByBukrs(bukrs, 
						CommonDateFunction.convertDateRequestParameterIntoDate(datbi)).stream().collect(Collectors.toList());
			} 
			
			if (listOfZmedMedtype.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<ZmedMedtypeResponseWrapper> records = new ArrayList<>();
				for (ZmedMedtype ZmedMedtype : listOfZmedMedtype) {
					records.add(new ZmedMedtypeResponseWrapper(ZmedMedtype));
				}
				bunchOfZmedMedtype.setItems(records.toArray(new ZmedMedtypeResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(ZmedMedtypeController.class).findByBukrs(bukrs, datbi, pageNumber)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}
	
	
}
