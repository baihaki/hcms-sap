package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.Attachment;
import com.abminvestama.hcms.core.model.entity.BNKA;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.T005T;
import com.abminvestama.hcms.core.model.entity.T042Z;
import com.abminvestama.hcms.core.model.entity.T591S;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.business.query.AttachmentQueryService;
import com.abminvestama.hcms.core.service.api.business.query.BNKAQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0009QueryService;
import com.abminvestama.hcms.core.service.api.business.query.T005TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T042ZQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T591SQueryService;
import com.abminvestama.hcms.core.service.util.FileAttachmentService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0009RequestWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Adjust attachment fields on EventData Json</td></tr>
 *     <tr><td>1.0.2</td><td>Yauri</td><td>Add method: copy</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData: Add attachment fields, Add method: saveAttachment</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("it0009RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT0009RequestBuilderUtil {

	private T591SQueryService t591sQueryService;
	private T042ZQueryService t042zQueryService;
	private T005TQueryService t005tQueryService;
	private BNKAQueryService bnkaQueryService;
	private IT0009QueryService it0009QueryService;
	private AttachmentQueryService attachmentQueryService;
	private FileAttachmentService fileAttachmentService;
	
	@Autowired
	void setT591SQueryService(T591SQueryService t591sQueryService) {
		this.t591sQueryService = t591sQueryService;
	}
	
	@Autowired
	void setT042ZQueryService(T042ZQueryService t042zQueryService) {
		this.t042zQueryService = t042zQueryService;
	}
	
	@Autowired
	void setT005TQueryService(T005TQueryService t005tQueryService) {
		this.t005tQueryService = t005tQueryService;
	}
	
	@Autowired
	void setBNKAQueryService(BNKAQueryService bnkaQueryService) {
		this.bnkaQueryService = bnkaQueryService;
	}
	
	@Autowired
	void setIT0009QueryService(IT0009QueryService it0009QueryService) {
		this.it0009QueryService = it0009QueryService;
	}
	
	@Autowired
	void setAttachmentQueryService(AttachmentQueryService attachmentQueryService) {
		this.attachmentQueryService = attachmentQueryService;
	}
	
	@Autowired
	void setFileAttachmentService(FileAttachmentService fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}	
	
	public IT0009 copy(IT0009 it0009DB) {
		IT0009 currentIT0009 = new IT0009();
		currentIT0009.setId(it0009DB.getId());
		currentIT0009.setAnzhl(it0009DB.getAnzhl());
		currentIT0009.setBankl(it0009DB.getBankl());
		currentIT0009.setBankn(it0009DB.getBankn());
		currentIT0009.setBanks(it0009DB.getBanks());
		currentIT0009.setBetrg(it0009DB.getBetrg());
		currentIT0009.setBkort(it0009DB.getBkort());
		currentIT0009.setBkplz(it0009DB.getBkplz());
		currentIT0009.setBnksa(it0009DB.getBnksa());
		currentIT0009.setEmftx(it0009DB.getEmftx());
		currentIT0009.setSeqnr(it0009DB.getSeqnr());
		currentIT0009.setStatus(it0009DB.getStatus());
		currentIT0009.setWaers(it0009DB.getWaers());
		currentIT0009.setZlsch(it0009DB.getZlsch());
		currentIT0009.setZweck(it0009DB.getZweck());
		return currentIT0009;
	}

	/**
	 * Compare IT0009 request payload with existing IT0009 in the database.
	 * Update existing IT0009 data with the latest data comes from the request payload. 
	 * 
	 * @param requestPayload request data 
	 * @param it0009DB current existing IT0009 in the database.
	 * @return updated IT0009 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0009, IT0009RequestWrapper, String> compareAndReturnUpdatedData(
			IT0009RequestWrapper requestPayload, IT0009 it0009DB) throws DataViolationException {
		
		RequestObjectComparatorContainer<IT0009, IT0009RequestWrapper, String> objectContainer = null; 
		
		if (it0009DB == null) {
			it0009DB = new IT0009();
			try {
				Optional<IT0009> existingIT0009 = it0009QueryService.findOneByCompositeKey(
						requestPayload.getPernr(), requestPayload.getSubty(), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda()));
				
				if (existingIT0009.isPresent()) {
					throw new DataViolationException("Duplicate IT0009 Data for [ssn=" + requestPayload.getPernr() + ",end_date=" +
							requestPayload.getEndda() + ",begin_date=" + requestPayload.getBegda());
				}
				
				it0009DB.setId(
						new ITCompositeKeys(requestPayload.getPernr(), SAPInfoType.BANK_DETAILS.infoType(), requestPayload.getSubty(), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda())));
				it0009DB.setAnzhl(requestPayload.getAnzhl());
				Optional<BNKA> bnka = bnkaQueryService.findById(Optional.ofNullable(requestPayload.getBankl()));
				it0009DB.setBankl(bnka.isPresent() ? bnka.get() : null);
				it0009DB.setBankn(requestPayload.getBankn());
				Optional<T005T> t005t = t005tQueryService.findById(Optional.ofNullable(requestPayload.getBanks()));
				it0009DB.setBanks(t005t.isPresent() ? t005t.get() : null);
				it0009DB.setBetrg(requestPayload.getBetrg());
				it0009DB.setBkort(requestPayload.getBkort());
				it0009DB.setBkplz(requestPayload.getBkplz());
				T591SKey bnksaKey = new T591SKey(SAPInfoType.BANK_DETAILS.infoType(), requestPayload.getSubty()); // bnksa = subty
				Optional<T591S> t591s = t591sQueryService.findById(Optional.ofNullable(bnksaKey));
				it0009DB.setBnksa(t591s.isPresent() ? t591s.get() : null);
				it0009DB.setEmftx(requestPayload.getEmftx());
				it0009DB.setSeqnr(1L);
				it0009DB.setStatus(DocumentStatus.INITIAL_DRAFT);
				it0009DB.setWaers(requestPayload.getWaers());
				Optional<T042Z> t042z = t042zQueryService.findById(Optional.ofNullable(requestPayload.getZlsch()));
				it0009DB.setZlsch(t042z.isPresent() ? t042z.get() : null);
				it0009DB.setZweck(requestPayload.getZweck());
				
				it0009DB = saveAttachment(it0009DB, requestPayload);
				
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			
			objectContainer = new RequestObjectComparatorContainer<>(it0009DB, requestPayload);
			
		} else {
			if (requestPayload.getBetrg() != (it0009DB.getBetrg() != null ? it0009DB.getBetrg().doubleValue() : 0.0)) {
				it0009DB.setBetrg(requestPayload.getBetrg());
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getWaers(), it0009DB.getWaers())) {
				it0009DB.setWaers(StringUtils.defaultString(requestPayload.getWaers()));
				requestPayload.setDataChanged(true);
			}
			if (requestPayload.getAnzhl() != (it0009DB.getAnzhl() != null ? it0009DB.getAnzhl().doubleValue() : 0.0)) {
				it0009DB.setAnzhl(requestPayload.getAnzhl());
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBnksa(), it0009DB.getBnksa() != null ? it0009DB.getBnksa().getId().getSubty() : StringUtils.EMPTY)) {
				try {
					T591SKey bnksaKey = new T591SKey(SAPInfoType.BANK_DETAILS.infoType(), requestPayload.getBnksa());
					Optional<T591S> newT591S = t591sQueryService.findById(Optional.ofNullable(bnksaKey));
					if (newT591S.isPresent()) {
						it0009DB.setBnksa(newT591S.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					// bnksa not found..., ignore change
				}
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getZlsch(), it0009DB.getZlsch() != null ? it0009DB.getZlsch().getZlsch() : "")) {
				try {
					Optional<T042Z> newT042Z = t042zQueryService.findById(Optional.ofNullable(requestPayload.getZlsch()));
					if (newT042Z.isPresent()) {
						it0009DB.setZlsch(newT042Z.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					// zlsch not found..., ignore change
				}
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getEmftx(), it0009DB.getEmftx())) {
				it0009DB.setEmftx(requestPayload.getEmftx());
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBkplz(), it0009DB.getBkplz())) {
				it0009DB.setBkplz(requestPayload.getBkplz());
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBkort(), it0009DB.getBkort())) {
				it0009DB.setBkort(requestPayload.getBkort());
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBanks(), it0009DB.getBanks() != null ? it0009DB.getBanks().getLand1() : "")) {
				try {
					Optional<T005T> newT005T = t005tQueryService.findById(Optional.ofNullable(requestPayload.getBanks()));
					if (newT005T.isPresent()) {
						it0009DB.setBanks(newT005T.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					// banks not found..., ignore change
				}
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBankl(), it0009DB.getBankl() != null ? it0009DB.getBankl().getBankl() : "")) {
				try {
					Optional<BNKA> newBNKA = bnkaQueryService.findById(Optional.ofNullable(requestPayload.getBankl()));
					if (newBNKA.isPresent()) {
						it0009DB.setBankl(newBNKA.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					// bnka not found..., ignore change
				}
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBankn(), it0009DB.getBankn())) {
				it0009DB.setBankn(requestPayload.getBankn());
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getZweck(), it0009DB.getZweck())) {
				it0009DB.setZweck(requestPayload.getZweck());
				requestPayload.setDataChanged(true);
			}
			
			it0009DB.setSeqnr(it0009DB.getSeqnr().longValue() + 1);
			try {
				it0009DB = saveAttachment(it0009DB, requestPayload);
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			it0009DB.setSeqnr(it0009DB.getSeqnr().longValue() - 1);
			
			objectContainer = new RequestObjectComparatorContainer<>(it0009DB, requestPayload);			
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = "";
		try {
			jsonString = objectMapper.writeValueAsString(requestPayload);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		
		//JSONObject json = new JSONObject(requestPayload);
		JSONObject json = new JSONObject(jsonString);
		// Adjust attachment fields on EventData Json
		json.remove("attachment1_type");
		json.remove("attachment1_base64");
		json.remove("attachment1_filename");
		if (it0009DB.getAttachment() != null)
			json.put("attachment1_type", it0009DB.getAttachment().getId().getSubty());
		if (it0009DB.getAttachmentPath() != null)
		    json.put("attachment1_path", it0009DB.getAttachmentPath());
		// ******************************************
		
		//if (it0009DB.getStatus() != null)
		    //json.put("prev_document_status", it0009DB.getStatus().name());
		objectContainer.setEventData(json.toString());
		
		return objectContainer;
	}
	
	private IT0009 saveAttachment(IT0009 it0009DB, IT0009RequestWrapper requestPayload) throws Exception {
		
		T591SKey attachmentKey = new T591SKey(SAPInfoType.BANK_DETAILS.infoType(), requestPayload.getAttach1Type());
		Optional<Attachment> attachment = attachmentQueryService.findById(Optional.ofNullable(attachmentKey));
		it0009DB.setAttachment(attachment.isPresent() ? attachment.get() : null);
		String base64 = requestPayload.getAttach1Content();
		String filename = requestPayload.getAttach1Filename();
		if (StringUtils.isNotEmpty(base64) && StringUtils.isNotEmpty(filename)) {
			String ext = filename.split("\\.")[(filename.split("\\.")).length-1];
			String formatFilename = it0009DB.getId().toStringId().concat("_").concat(it0009DB.getSeqnr().toString()).concat(".").concat(ext);
		    String path = fileAttachmentService.save(formatFilename, base64);
		    it0009DB.setAttachmentPath(path);
		    requestPayload.setDataChanged(true);
		}
		
		return it0009DB;
		
	}
}