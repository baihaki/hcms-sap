package com.abminvestama.hcms.rest.api.dto.response;

import com.abminvestama.hcms.core.model.entity.CaDeptsCostCenter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class CaDeptsCostCenterResponseWrapper {
	
	private Long deptCostCenterId;
	private Long orgeh;
	private String orgtx;
	private String endda;
	private String kokrs;
	private String kostl;
	private String ktext;
	private String kltxt;
	private String mcds3;
	
	private CaDeptsCostCenterResponseWrapper() {}
	
	public CaDeptsCostCenterResponseWrapper(CaDeptsCostCenter caDeptsCostCenter) {
		if (caDeptsCostCenter == null) {
			new CaDeptsCostCenterResponseWrapper();
		}
		else {
			this
			.setDeptCostCenterId(caDeptsCostCenter.getId())
			.setOrgeh(caDeptsCostCenter.getT527x() != null ? caDeptsCostCenter.getT527x().getOrgeh() : 0L)
			.setOrgtx(caDeptsCostCenter.getT527x() != null ? caDeptsCostCenter.getT527x().getOrgtx() : "")
			.setKokrs(caDeptsCostCenter.getCskt() != null ? caDeptsCostCenter.getCskt().getKokrs() : "")
			.setKostl(caDeptsCostCenter.getCskt() != null ? caDeptsCostCenter.getCskt().getKostl() : "")
			.setKtext(caDeptsCostCenter.getCskt() != null ? caDeptsCostCenter.getCskt().getKtext() : "")
			.setKltxt(caDeptsCostCenter.getCskt() != null ? caDeptsCostCenter.getCskt().getKltxt() : "")
			.setMcds3(caDeptsCostCenter.getCskt() != null ? caDeptsCostCenter.getCskt().getMcds3() : "")
			;
		}
	}

	@JsonProperty("id")
	public Long getDeptCostCenterId() {
		return deptCostCenterId;
	}

	public CaDeptsCostCenterResponseWrapper setDeptCostCenterId(Long deptCostCenterId) {
		this.deptCostCenterId = deptCostCenterId;
		return this;
	}

	@JsonProperty("orgeh")
	public Long getOrgeh() {
		return orgeh;
	}

	public CaDeptsCostCenterResponseWrapper setOrgeh(Long orgeh) {
		this.orgeh = orgeh;
		return this;
	}

	@JsonProperty("orgtx")
	public String getOrgtx() {
		return orgtx;
	}

	public CaDeptsCostCenterResponseWrapper setOrgtx(String orgtx) {
		this.orgtx = orgtx;
		return this;
	}

	@JsonProperty("endda")
	public String getEndda() {
		return endda;
	}

	public void setEndda(String endda) {
		this.endda = endda;
	}

	@JsonProperty("kokrs")
	public String getKokrs() {
		return kokrs;
	}

	public CaDeptsCostCenterResponseWrapper setKokrs(String kokrs) {
		this.kokrs = kokrs;
		return this;
	}

	@JsonProperty("kostl")
	public String getKostl() {
		return kostl;
	}

	public CaDeptsCostCenterResponseWrapper setKostl(String kostl) {
		this.kostl = kostl;
		return this;
	}

	@JsonProperty("ktext")
	public String getKtext() {
		return ktext;
	}

	public CaDeptsCostCenterResponseWrapper setKtext(String ktext) {
		this.ktext = ktext;
		return this;
	}

	@JsonProperty("kltxt")
	public String getKltxt() {
		return kltxt;
	}

	public CaDeptsCostCenterResponseWrapper setKltxt(String kltxt) {
		this.kltxt = kltxt;
		return this;
	}

	@JsonProperty("mcds3")
	public String getMcds3() {
		return mcds3;
	}

	public CaDeptsCostCenterResponseWrapper setMcds3(String mcds3) {
		this.mcds3 = mcds3;
		return this;
	}

}
