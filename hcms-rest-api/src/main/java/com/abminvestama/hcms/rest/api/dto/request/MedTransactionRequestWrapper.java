package com.abminvestama.hcms.rest.api.dto.request;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MedTransactionRequestWrapper extends ITRequestWrapper {
	
	private static final long serialVersionUID = -803734657203054809L;

	private ZmedTrxhdrRequestWrapper trxHdr;

	private ZmedTrxitmRequestWrapper[] trxItm;
	
	@JsonProperty("trx_header")		
	public ZmedTrxhdrRequestWrapper getTrxHdr() {
		return trxHdr;
	}
	
	@JsonProperty("trx_item")
	public ZmedTrxitmRequestWrapper[] getTrxItm() {
		return trxItm;
	}
	
	
/*
	@JsonProperty("company_code")
	private String bukrs;
	
	@JsonProperty("claim_number")
	private String clmno;
	
	@JsonProperty("date_created")
	private String billdt;
	
	@JsonProperty("medical_type")
	private String kodemed;
	
	@JsonProperty("relat")
	private String relat;

	@JsonProperty("umur")
	private String umur;
	
	@JsonProperty("pasien")
	private String pasien;

	@JsonProperty("transaction_header")
	private ZmedTrxhdrRequestWrapper head;
	
	@JsonProperty("transaction_details")
	public TransactionDetails[] details;
	
	
	
	
	public TransactionDetails[] getDetails() {
		return details;
	}
	
	

	public ZmedTrxhdrRequestWrapper getHeader() {
		return head;
	}
	
	
	public class TransactionDetails{
		
		@JsonProperty("cost_type")
		private String kodecos;
		
		@JsonProperty("cost_types")
		private String zcostyp;
		
		@JsonProperty("claim_amount")
		private String clmamt;
		
		@JsonProperty("rej_amount")
		private String rejamt;
		
		@JsonProperty("app_amount")
		private String appamt;
		
		@JsonProperty("item_number")
		String itmno;
		
		public void setItmno(String itmno) {
			this.itmno = itmno;
		}

		
		public String getItmno() {
			return itmno;
		}
		
		
		public String getKodecos() {
			return kodecos;
		}
			
		
		
		public String getZcostyp() {
			return zcostyp;
		}
		
		
		public String getClmamt() {
			return clmamt;
		}
		
		
		public String getRejamt() {
			return rejamt;
		}
		
		
		public String getAppamt() {
			return appamt;
		}
		
		
	}
	
	
	
	public String getBukrs() {
		return bukrs;
	}
	
	
	public String getClmno() {
		return clmno;
	}
	
	
	
	public String getBilldt() {
		return billdt;
	}
	
	
	
	public String getKodemed() {
		return kodemed;
	}
	
	
	
	public String getRelat() {
		return relat;
	}
	
	
	public String getUmur() {
		return umur;
	}
	
	
	public String getPasien() {
		return pasien;
	}*/
	
}