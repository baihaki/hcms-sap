package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotaQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedEmpquotaRequestWrapper;

@Component("ZmedEmpquotaRequestBuilderUtil")
@Transactional(readOnly = true)
public class ZmedEmpquotaRequestBuilderUtil {

	

	private T001QueryService t001QueryService;
	private ZmedEmpquotaQueryService zmedEmpquotaQueryService;
	
	@Autowired
	void setZmedEmpquotaQueryService(ZmedEmpquotaQueryService zmedEmpquotaQueryService) {
		this.zmedEmpquotaQueryService = zmedEmpquotaQueryService;
	}
	
	@Autowired
	void setT001QueryService(T001QueryService t001QueryService) {
		this.t001QueryService = t001QueryService;
	}
	
	public RequestObjectComparatorContainer<ZmedEmpquota, ZmedEmpquotaRequestWrapper, String> compareAndReturnUpdatedData(ZmedEmpquotaRequestWrapper requestPayload, ZmedEmpquota zmedEmpquotaDB) {
		
		RequestObjectComparatorContainer<ZmedEmpquota, ZmedEmpquotaRequestWrapper, String> objectContainer = null; 
		
	if (zmedEmpquotaDB == null) {
		zmedEmpquotaDB = new ZmedEmpquota();
		
		try {
			//Optional<ZmedEmpquota> existingZmedEmpquota = zmedEmpquotaQueryService.findById(requestPayload.getKodemed());

			Optional<ZmedEmpquota> existingZmedEmpquota = zmedEmpquotaQueryService.findOneByCompositeKey(
					requestPayload.getBukrs(), requestPayload.getPersg(), requestPayload.getPersk(),
					requestPayload.getFatxt(), requestPayload.getKodequo(),
					CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
					CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
			
		if (existingZmedEmpquota.isPresent()) {
				throw new DataViolationException("Duplicate ZmedEmpquota Data for [ssn=" + requestPayload.getKodequo());
			}
		zmedEmpquotaDB.setId(
				new ZmedCompositeKeyEmpquota(requestPayload.getBukrs(), requestPayload.getPersg(), requestPayload.getPersk(),
						requestPayload.getFatxt(), requestPayload.getKodequo(),
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi())));
			zmedEmpquotaDB.setDatab(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()));
			zmedEmpquotaDB.setDatbi(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
			zmedEmpquotaDB.setExten(Byte.parseByte(requestPayload.getExten()));
			zmedEmpquotaDB.setKodequo(requestPayload.getKodequo());
			zmedEmpquotaDB.setPersg(requestPayload.getPersg());
			zmedEmpquotaDB.setPersk(requestPayload.getPersk());
			zmedEmpquotaDB.setFatxt(requestPayload.getFatxt());
			zmedEmpquotaDB.setQuotamt(Double.parseDouble(requestPayload.getQuotamt()));			
			Optional<T001> bnka = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));				
			zmedEmpquotaDB.setBukrs(bnka.isPresent() ? bnka.get() : null);
			//zmedEmpquotaDB.setBukrs(requestPayload.getBukrs());
		} catch (Exception ignored) {
			System.err.println(ignored);
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedEmpquotaDB, requestPayload);
		
	} else {
		
		if (StringUtils.isNotBlank(requestPayload.getPersg())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPersg(), zmedEmpquotaDB.getPersg())) {
				zmedEmpquotaDB.setPersg(requestPayload.getPersg());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getPersk())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPersk(), zmedEmpquotaDB.getPersk())) {
				zmedEmpquotaDB.setPersk(requestPayload.getPersk());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getFatxt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getFatxt(), zmedEmpquotaDB.getFatxt())) {
				zmedEmpquotaDB.setFatxt(requestPayload.getFatxt());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getKodequo())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getKodequo(), zmedEmpquotaDB.getKodequo())) {
				zmedEmpquotaDB.setKodequo(requestPayload.getKodequo());
				requestPayload.setDataChanged(true);
			}
		}
		
		

		
		/*if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedEmpquotaDB.getBukrs())) {
				zmedEmpquotaDB.setBukrs(requestPayload.getBukrs());
				requestPayload.setDataChanged(true);
			}
		}*/
		if (StringUtils.isNotBlank(requestPayload.getBukrs())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBukrs(), zmedEmpquotaDB.getBukrs() != null ? zmedEmpquotaDB.getBukrs().getBukrs() : StringUtils.EMPTY)) {
				try {
					Optional<T001> newT001 = t001QueryService.findById(Optional.ofNullable(requestPayload.getBukrs()));
					if (newT001.isPresent()) {
						zmedEmpquotaDB.setBukrs(newT001.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					//T535N not found... cancel the update process
				}
			}
		}

			
		
		if (StringUtils.isNotBlank(requestPayload.getExten())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getExten(), zmedEmpquotaDB.getExten().toString())) {
				zmedEmpquotaDB.setExten(Byte.parseByte(requestPayload.getExten()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getQuotamt())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getQuotamt(), zmedEmpquotaDB.getQuotamt().toString())) {
				zmedEmpquotaDB.setQuotamt(Double.parseDouble(requestPayload.getQuotamt()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getDatab())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()), (zmedEmpquotaDB.getDatab()))) {
				zmedEmpquotaDB.setDatab(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatab()));
				requestPayload.setDataChanged(true);
			}
		}
		
		
		if (StringUtils.isNotBlank(requestPayload.getDatbi())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()), (zmedEmpquotaDB.getDatbi())) ){
				zmedEmpquotaDB.setDatbi(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatbi()));
				requestPayload.setDataChanged(true);
			}
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(zmedEmpquotaDB, requestPayload);	
				
	}
	
	JSONObject json = new JSONObject(requestPayload);
	objectContainer.setEventData(json.toString());
	
	return objectContainer;
}
}