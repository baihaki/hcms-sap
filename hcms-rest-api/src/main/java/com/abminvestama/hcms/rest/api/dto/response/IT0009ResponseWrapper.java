package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.BNKA;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.T005T;
import com.abminvestama.hcms.core.model.entity.T042Z;
import com.abminvestama.hcms.core.model.entity.T591S;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.7
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.7</td><td>Baihaki</td><td>Add fields: banklBranch</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Fix getUpdatedFields bug: Update the current referred fields i.e. text fields</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Modify Constructor of Read from EventData -> IT0009ResponseWrapper(IT0009, String, CommonServiceFactory)</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add fields: banklText, Fix Response fields (code/text pairs)</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add fields: bnksaText, zlschText</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add attachmentType, attachmentTypeText, attachmentPath field</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add banksText field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 * 
 * Wrapper class for <strong>Bank Details</strong> (i.e. IT0009 in SAP)
 *
 */
@JsonInclude(Include.NON_NULL)
public class IT0009ResponseWrapper extends ResourceSupport {

	private long pernr;
	
	private String subty;
	
	private String subtyText;
	
	private Date endda;
	
	private Date begda;
	
	private double betrg;
	
	private String waers;
	
	private double anzhl;
	
	private String bnksa;
	
	private String bnksaText;
	
	private String zlsch;
	
	private String zlschText;
	
	private String emftx;
	
	private String bkplz;
	
	private String bkort;
	
	private String banks;
	
	private String banksText;
	
	private String bankl;
	
	private String banklText;
	
	private String banklBranch;
	
	private String bankn;
	
	private String zweck;	
	
	private String documentStatus;
	
	private String attachmentType;
	private String attachmentTypeText;
	private String attachmentPath;
	
	private  IT0009ResponseWrapper() {}
	
	public IT0009ResponseWrapper(IT0009 it0009) {
		if (it0009 == null) {
			new IT0009ResponseWrapper();
		} else {
			this
				.setPernr(it0009.getId().getPernr())
				.setSubty(it0009.getSubty() != null ? 
						StringUtils.defaultString(it0009.getSubty().getId() != null ? it0009.getSubty().getId().getSubty() : StringUtils.EMPTY) : it0009.getId().getSubty())
				.setSubtyText(it0009.getSubty() != null ?
						StringUtils.defaultString(it0009.getSubty().getId() != null ? it0009.getSubty().getStext() : StringUtils.EMPTY) : 
							it0009.getBnksa() != null ? StringUtils.defaultString(it0009.getBnksa().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setEndda(it0009.getId().getEndda()).setBegda(it0009.getId().getBegda())
				.setBetrg(it0009.getBetrg() != null ? it0009.getBetrg() : 0.0)
				.setWaers(it0009.getWaers()).setAnzhl(it0009.getAnzhl() != null ? it0009.getAnzhl().doubleValue() : 0.0)
				//.setBnksa(it0009.getBnksa() != null ? StringUtils.defaultString(it0009.getBnksa().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
				
				.setBnksa(it0009.getSubty() != null ?
						StringUtils.defaultString(it0009.getSubty().getId() != null ? it0009.getSubty().getId().getSubty() : StringUtils.EMPTY) : it0009.getId().getSubty())
				.setBnksaText(it0009.getSubty() != null ?
						StringUtils.defaultString(it0009.getSubty().getId() != null ? it0009.getSubty().getStext() : StringUtils.EMPTY) : 
							it0009.getBnksa() != null ? StringUtils.defaultString(it0009.getBnksa().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setZlsch(it0009.getZlsch() != null ? it0009.getZlsch().getZlsch() : StringUtils.EMPTY)
				.setZlschText(it0009.getZlsch() != null ? StringUtils.defaultString(it0009.getZlsch().getText1(), StringUtils.EMPTY) : StringUtils.EMPTY)
				
				.setEmftx(StringUtils.defaultString(it0009.getEmftx(), StringUtils.EMPTY))
				.setBkplz(StringUtils.defaultString(it0009.getBkplz(), StringUtils.EMPTY))
				.setBkort(StringUtils.defaultString(it0009.getBkort(), StringUtils.EMPTY))
				.setBanks(it0009.getBanks() != null ? StringUtils.defaultString(it0009.getBanks().getLand1(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setBanksText(it0009.getBanks() != null ? StringUtils.defaultString(it0009.getBanks().getLandx(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setBankl(it0009.getBankl() != null ? StringUtils.defaultString(it0009.getBankl().getBankl(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setBanklText(it0009.getBankl() != null ? StringUtils.defaultString(it0009.getBankl().getBanka(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setBanklBranch(it0009.getBankl() != null ? StringUtils.defaultString(it0009.getBankl().getBrnch(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setBankn(StringUtils.defaultString(it0009.getBankn(), StringUtils.EMPTY))
				.setZweck(StringUtils.defaultString(it0009.getZweck(), StringUtils.EMPTY))
				.setAttachmentType(it0009.getAttachment() != null ? StringUtils.defaultString(it0009.getAttachment().getId().getSubty(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentTypeText(it0009.getAttachment() != null ? StringUtils.defaultString(it0009.getAttachment().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentPath(StringUtils.defaultString(it0009.getAttachmentPath(), StringUtils.EMPTY))
				.setDocumentStatus(it0009.getStatus());
		}
	}
	
	public IT0009ResponseWrapper(IT0009 currentData, String eventData, CommonServiceFactory serviceFactory) throws JSONException, Exception {
		this(currentData);
		IT0009ResponseWrapper itWrapper = (IT0009ResponseWrapper) new EventDataWrapper(eventData, this).getItWrapper();
		String[] ignoreProperties = EventDataWrapper.getNullPropertyNames(itWrapper);
		BeanUtils.copyProperties(itWrapper, this, ignoreProperties);
		
		// Update the current referred fields i.e. text fields
		Optional<T005T> banksObject = serviceFactory.getT005TQueryService().findById(Optional.ofNullable(banks));
		banksText = banksObject.isPresent() ? banksObject.get().getLandx() : StringUtils.EMPTY;
		Optional<BNKA> banklObject = serviceFactory.getBNKAQueryService().findById(Optional.ofNullable(bankl));
		banklText = banklObject.isPresent() ? banklObject.get().getBanka() : StringUtils.EMPTY;
		T591SKey bnksaKey = new T591SKey(SAPInfoType.BANK_DETAILS.infoType(), bnksa);
		Optional<T591S> bnksaObject = serviceFactory.getT591SQueryService().findById(Optional.ofNullable(bnksaKey));
		bnksaText = bnksaObject.isPresent() ? bnksaObject.get().getStext() : StringUtils.EMPTY;
		Optional<T042Z> zlschObject = serviceFactory.getT042ZQueryService().findById(Optional.ofNullable(zlsch));
		zlschText = zlschObject.isPresent() ? zlschObject.get().getText1() : StringUtils.EMPTY;
		
		/*
		if (currentData != null && (!StringUtils.isEmpty(eventData))) {
			try {
				JSONObject json = new JSONObject(eventData);
				this.setBetrg(json.has("betrg") ? json.getDouble("betrg") : currentData.getBetrg());
				this.setWaers(json.has("waers") ? json.getString("waers") : currentData.getWaers());
				this.setAnzhl(json.has("anzhl") ? json.getDouble("anzhl") : currentData.getAnzhl());
				if (json.has("subty")) { // bnksa = subty
					Optional<T591SKey> bnksaId = Optional.of(new T591SKey("0009", json.getString("subty")));
					Optional<T591S> bnksa = serviceFactory.getT591SQueryService().findById(bnksaId);
					this.setBnksa(bnksa.get().getId().getSubty());
					this.setBnksaText(bnksa.get().getStext());
				} else { // bnksa = subty
					Optional<T591SKey> bnksaId = Optional.of(new T591SKey("0009", currentData.getId().getSubty()));
					Optional<T591S> bnksa = serviceFactory.getT591SQueryService().findById(bnksaId);
					this.setBnksa(bnksa.get().getId().getSubty());
					this.setBnksaText(bnksa.get().getStext());
				}
				if (json.has("zlsch")) {
					Optional<T042Z> zlsch = serviceFactory.getT042ZQueryService().findById(Optional.ofNullable(json.getString("zlsch")));
					this.setZlsch(zlsch.get().getZlsch());
					this.setZlschText(zlsch.get().getText1());
				} else {
					this.setZlsch(currentData.getZlsch().getZlsch());
					this.setZlschText(currentData.getZlsch().getText1());
				}
				this.setEmftx(json.has("emftx") ? json.getString("emftx") : currentData.getEmftx());
				this.setBkplz(json.has("bkplz") ? json.getString("bkplz") : currentData.getBkplz());
				this.setBkort(json.has("bkort") ? json.getString("bkort") : currentData.getBkort());
				if (json.has("banks")) {
					Optional<T005T> banks = serviceFactory.getT005TQueryService().findById(Optional.ofNullable(json.getString("banks")));
					this.setBanks(banks.get().getLand1());
					this.setBanklText(banks.get().getLandx());
				} else {
					this.setBanks(currentData.getBanks().getLand1());
					this.setBanksText(currentData.getBanks().getLandx());
				}
				if (json.has("bankl")) {
					Optional<BNKA> bnka = serviceFactory.getBNKAQueryService().findById(Optional.ofNullable(json.getString("bankl")));
					this.setBankl(bnka.get().getBankl());
					this.setBanklText(bnka.get().getBanka());
				} else {
					this.setBankl(currentData.getBankl().getBankl());
					this.setBanklText(currentData.getBankl().getBanka());
				}
				this.setBankn(json.has("bankn") ? json.getString("bankn") : currentData.getBankn());
				this.setZweck(json.has("zweck") ? json.getString("zweck") : currentData.getZweck());
				if (json.has("attach1_subty")) {
					T591SKey attachmentKey = new T591SKey(SAPInfoType.BANK_DETAILS.infoType(), json.getString("attach1_subty"));
					Optional<Attachment> attachment = serviceFactory.getAttachmentQueryService().findById(Optional.ofNullable(attachmentKey));
					this.setAttachmentType(attachment.get().getId().getSubty());
					this.setAttachmentTypeText(attachment.get().getStext());
				} else {
					this.setAttachmentType(currentData.getAttachment().getId().getSubty());
					this.setAttachmentTypeText(currentData.getAttachment().getStext());
				}
				this.setAttachmentPath(json.has("attach1_path") ? json.getString("attach1_path") : currentData.getAttachmentPath());
				
			} catch (JSONException e) {
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
		*/
	}

	/**
	 * GET Employee No./SSN
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	public IT0009ResponseWrapper setPernr(Long pernr) {
		this.pernr = pernr;
		return this;
	}

	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	@JsonProperty("subtype")
	public String getSubty() {
		return subty;
	}
	
	public IT0009ResponseWrapper setSubty(String subty) {
		this.subty = subty;
		return this;
	}
	
	/**
	 * GET Subtype Name/Text.
	 * 
	 * @return
	 */
	@JsonProperty("subtype_text")
	public String getSubtyText() {
		return subtyText;
	}
	
	public IT0009ResponseWrapper setSubtyText(String subtyText) {
		this.subtyText = subtyText;
		return this;
	}

	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	public IT0009ResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}

	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	public IT0009ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * GET Standard Value.
	 *  
	 * @return
	 */
	@JsonProperty("standard_value")
	public double getBetrg() {
		return betrg;
	}
	
	public IT0009ResponseWrapper setBetrg(double betrg) {
		this.betrg = betrg;
		return this;
	}

	/**
	 * GET Payment Currency.
	 * 
	 * @return
	 */
	@JsonProperty("payment_currency")
	public String getWaers() {
		return waers;
	}
	
	public IT0009ResponseWrapper setWaers(String waers) {
		this.waers = waers;
		return this;
	}

	/**
	 * GET Standard Percentage.
	 * 
	 * @return
	 */
	@JsonProperty("standard_percentage")
	public double getAnzhl() {
		return anzhl;
	}
	
	public IT0009ResponseWrapper setAnzhl(double anzhl) {
		this.anzhl = anzhl;
		return this;
	}

	/**
	 * GET Bank Details Type.
	 * 
	 * @return
	 */
	@JsonProperty("bank_details_type")
	public String getBnksa() {
		return bnksa;
	}
	
	public IT0009ResponseWrapper setBnksa(String bnksa) {
		this.bnksa = bnksa;
		return this;
	}

	/**
	 * GET Bank Details Type.
	 * 
	 * @return
	 */
	@JsonProperty("bank_details_type_text")
	public String getBnksaText() {
		return bnksaText;
	}
	
	public IT0009ResponseWrapper setBnksaText(String bnksaText) {
		this.bnksaText = bnksaText;
		return this;
	}

	/**
	 * GET Payment Method.
	 * 
	 * @return
	 */
	@JsonProperty("payment_method")
	public String getZlsch() {
		return zlsch;
	}
	
	public IT0009ResponseWrapper setZlsch(String zlsch) {
		this.zlsch = zlsch;
		return this;
	}

	/**
	 * GET Payment Method.
	 * 
	 * @return
	 */
	@JsonProperty("payment_method_text")
	public String getZlschText() {
		return zlschText;
	}
	
	public IT0009ResponseWrapper setZlschText(String zlschText) {
		this.zlschText = zlschText;
		return this;
	}

	/**
	 * GET Payee.
	 * 
	 * @return
	 */
	@JsonProperty("payee")
	public String getEmftx() {
		return emftx;
	}
	
	public IT0009ResponseWrapper setEmftx(String emftx) {
		this.emftx = emftx;
		return this;
	}

	/**
	 * GET Postal Code
	 * 
	 * @return
	 */
	@JsonProperty("postal_code")
	public String getBkplz() {
		return bkplz;
	}
	
	public IT0009ResponseWrapper setBkplz(String bkplz) {
		this.bkplz = bkplz;
		return this;
	}

	/**
	 * GET City.
	 * 
	 * @return
	 */
	@JsonProperty("city")
	public String getBkort() {
		return bkort;
	}
	
	public IT0009ResponseWrapper setBkort(String bkort) {
		this.bkort = bkort;
		return this;
	}

	/**
	 * GET Bank Country.
	 * 
	 * @return
	 */
	@JsonProperty("bank_country")
	public String getBanks() {
		return banks;
	}
	
	public IT0009ResponseWrapper setBanks(String banks) {
		this.banks = banks;
		return this;
	}

	/**
	 * GET Bank Country Text.
	 * 
	 * @return
	 */
	@JsonProperty("bank_country_text")
	public String getBanksText() {
		return banksText;
	}
	
	public IT0009ResponseWrapper setBanksText(String banksText) {
		this.banksText = banksText;
		return this;
	}

	/**
	 * GET Bank Key.
	 * 
	 * @return
	 */
	@JsonProperty("bank_key")
	public String getBankl() {
		return bankl;
	}
	
	public IT0009ResponseWrapper setBankl(String bankl) {
		this.bankl = bankl;
		return this;
	}

	/**
	 * GET Bank Key Text (Bank Name).
	 * 
	 * @return
	 */
	@JsonProperty("bank_key_text")
	public String getBanklText() {
		return banklText;
	}
	
	public IT0009ResponseWrapper setBanklText(String banklText) {
		this.banklText = banklText;
		return this;
	}

	/**
	 * GET Bank Key Branch (Bank Branch).
	 * 
	 * @return
	 */
	@JsonProperty("bank_key_branch")
	public String getBanklBranch() {
		return banklBranch;
	}
	
	public IT0009ResponseWrapper setBanklBranch(String banklBranch) {
		this.banklBranch = banklBranch;
		return this;
	}

	/**
	 * GET Bank Account.
	 * 
	 * @return
	 */
	@JsonProperty("bank_account")
	public String getBankn() {
		return bankn;
	}
	
	public IT0009ResponseWrapper setBankn(String bankn) {
		this.bankn = bankn;
		return this;
	}

	/**
	 * GET Purpose.
	 * 
	 * @return
	 */
	@JsonProperty("purpose")
	public String getZweck() {
		return zweck;
	}	
	
	public IT0009ResponseWrapper setZweck(String zweck) {
		this.zweck = zweck;
		return this;
	}
	
	/**
	 * GET Document Status.
	 * 
	 * @return
	 */
	@JsonProperty("document_status")
	public String getDocumentStatus() {
		return documentStatus;
	}
	
	public IT0009ResponseWrapper setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus.name();
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type")
	public String getAttachmentType() {
		return attachmentType;
	}
	
	public IT0009ResponseWrapper setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
		return this;
	}
	
	/**
	 * GET Attachment Type Text.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type_text")
	public String getAttachmentTypeText() {
		return attachmentTypeText;
	}
	
	public IT0009ResponseWrapper setAttachmentTypeText(String attachmentTypeText) {
		this.attachmentTypeText = attachmentTypeText;
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_path")
	public String getAttachmentPath() {
		return attachmentPath;
	}
	
	public IT0009ResponseWrapper setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
		return this;
	}
}