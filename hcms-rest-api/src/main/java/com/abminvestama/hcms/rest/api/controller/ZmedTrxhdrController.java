package com.abminvestama.hcms.rest.api.controller;



import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;















import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedTrxhdrCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedTrxitmCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0009QueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedTrxhdrQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedTrxitmQueryService;
import com.abminvestama.hcms.core.service.helper.ZmedTrxhdrMapper;
import com.abminvestama.hcms.core.service.helper.ZmedTrxhdrSoapObject;
import com.abminvestama.hcms.core.service.helper.ZmedTrxhdrsSoapObject;
import com.abminvestama.hcms.core.service.helper.ZmedTrxitmSoapObject;
import com.abminvestama.hcms.core.service.util.MailService;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.ZmedTrxhdrRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.ZmedTrxhdrResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.ZmedTrxhdrRequestBuilderUtil;



/**
 * 
 * @since 1.0.0
 * @version 1.0.11
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.11</td><td>Baihaki</td><td>Change getClmdt to getAprdt as year parameter for synchronize SAP medical document number</td></tr>
 *     <tr><td>1.0.10</td><td>Baihaki</td><td>Don't send email notification email for temporary claim number</td></tr>
 *     <tr><td>1.0.9</td><td>Baihaki</td><td>Add: Send notification email to Admin on create/edit</td></tr>
 *     <tr><td>1.0.8</td><td>Baihaki</td><td>Set Main Bank Detail on create, and on synchronize document status (Paid status)</td></tr>
 *     <tr><td>1.0.7</td><td>Baihaki</td><td>setPaymentStatus to default "N" on create end edit</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Add parameter "finance_payment_status" on findByCriteria to filter by paymentStatus or not (optional)</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Change BAPIFunction.ZMEDDOCTRXHDR_FETCH to BAPIFunction.ZMEDDOCTRXHDR_FETCH_ABM (add docno2, paymentStatus2)</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: updateDocumentStatus, synchronize Medical Document Status from SAP</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: findByCriteria</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add Post to MuleSoft on create method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add Post to MuleSoft on edit method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/medical_trxhdr")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ZmedTrxhdrController extends AbstractResource {
	
	private ZmedTrxhdrCommandService zmedTrxhdrCommandService;
	private ZmedTrxhdrQueryService zmedTrxhdrQueryService;
	private ZmedTrxitmQueryService zmedTrxitmQueryService;
	private ZmedTrxhdrRequestBuilderUtil zmedTrxhdrRequestBuilderUtil;
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	private MuleConsumerService muleConsumerService;
	private ZmedTrxitmCommandService zmedTrxitmCommandService;
	private MailService mailService;
	private CommonServiceFactory commonServiceFactory;
	private IT0009QueryService it0009QueryService;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	@Autowired
	void setZmedTrxhdrCommandService(ZmedTrxhdrCommandService zmedTrxhdrCommandService) {
		this.zmedTrxhdrCommandService = zmedTrxhdrCommandService;
	}
	
	@Autowired
	void setZmedTrxhdrQueryService(ZmedTrxhdrQueryService zmedTrxhdrQueryService) {
		this.zmedTrxhdrQueryService = zmedTrxhdrQueryService;
	}
	
	@Autowired
	void setZmedTrxitmQueryService(ZmedTrxitmQueryService zmedTrxitmQueryService) {
		this.zmedTrxitmQueryService = zmedTrxitmQueryService;
	}
	
	@Autowired
	void setZmedTrxhdrRequestBuilderUtil(ZmedTrxhdrRequestBuilderUtil zmedTrxhdrRequestBuilderUtil) {
		this.zmedTrxhdrRequestBuilderUtil = zmedTrxhdrRequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	void setIT0009QueryService(IT0009QueryService it0009QueryService) {
		this.it0009QueryService = it0009QueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}

	@Autowired
	void setZmedTrxitmCommandService(ZmedTrxitmCommandService zmedTrxitmCommandService) {
		this.zmedTrxitmCommandService = zmedTrxitmCommandService;
	}

	@Autowired
	void setMailService(MailService mailService) {
		this.mailService = mailService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	private void updateDocumentStatus(List<ZmedTrxhdr> listZmedTrxhdr) {
		
		List<ZmedTrxhdr> listZmedTrxhdrForSync =  new ArrayList<ZmedTrxhdr>();
		List<ZmedTrxhdr> listZmedTrxhdrForCurrentSync = new ArrayList<ZmedTrxhdr>();
		listZmedTrxhdr.forEach(zmedTrxhdr -> {
			// sync / update document status only if last sync was 12 minutes past
			if (zmedTrxhdr.getLastSync() == null ||
					(new Date().getTime() - zmedTrxhdr.getLastSync().getTime()) / (60 * 1000) % 60 >= 12)
			listZmedTrxhdrForSync.add(zmedTrxhdr);
			listZmedTrxhdrForCurrentSync.add(zmedTrxhdr);
		});
		if (!listZmedTrxhdrForSync.isEmpty()) {
			String bukrs = null;
			
			// update v1.0.11 -- change getClmdt to getAprdt, parameter year should be taken from approval date (not claim date)
			String year = CommonDateFunction.convertDateToStringY(listZmedTrxhdrForSync.get(0).getAprdt());
			List<ZmedTrxhdr> listZmedTrxhdrForNextSync =  new ArrayList<ZmedTrxhdr>();
			for (ZmedTrxhdr zmedTrxhdr : listZmedTrxhdrForCurrentSync) {
				if (!year.equals(CommonDateFunction.convertDateToStringY(zmedTrxhdr.getAprdt()))) {
					listZmedTrxhdrForNextSync.add(zmedTrxhdr);
					listZmedTrxhdrForSync.remove(zmedTrxhdr);
				}
			}
			// update document status of past year medical transaction
			if (listZmedTrxhdrForNextSync.size() > 0) {
				System.out.println("Update document status of past year medical transaction. Year: " +
						CommonDateFunction.convertDateToStringY(listZmedTrxhdrForNextSync.get(0).getAprdt()) );
				updateDocumentStatus(listZmedTrxhdrForNextSync);
			}
				
			try {
				Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
				bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			ZmedTrxhdrsSoapObject zmedTrxhdrsSoapObject = new ZmedTrxhdrsSoapObject(bukrs, year);
			
			List<ZmedTrxhdrSoapObject> listZmedTrxhdrSoapObject =  new ArrayList<ZmedTrxhdrSoapObject>();
			for (ZmedTrxhdr zmedTrxhdr : listZmedTrxhdrForSync) {
				listZmedTrxhdrSoapObject.add(new ZmedTrxhdrSoapObject(
						zmedTrxhdr.getBukrs().getBukrs(), zmedTrxhdr.getZclmno(), zmedTrxhdr.getPernr().toString()));
			}
			zmedTrxhdrsSoapObject.setListTrxHeader(listZmedTrxhdrSoapObject);
			
			List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.ZMEDDOCTRXHDR_FETCH_ABM.service(),
					BAPIFunction.ZMEDDOCTRXHDR_FETCH_ABM_URI.service(), zmedTrxhdrsSoapObject);
			
			for (int i = 0; i < list.size(); i++) {
				// synchronize ZmedTrxhdr Document Status fields (in different thread) with the fetched fields from SAP
				if (i > 0 && list.get(i).get("zclmno") != null) {
					ZmedTrxhdr zmedTrxhdrDB = listZmedTrxhdrForSync.get(i-1);
					ZmedTrxhdr zmedTrxhdrEntity = new ZmedTrxhdrMapper(list.get(i)).getZmedTrxhdr();
					for (ZmedTrxhdr item : listZmedTrxhdrForSync) {
						if (list.get(i).get("zclmno").equals(item.getZclmno())) {
							zmedTrxhdrDB =  item;
							break;
						}
					}
					

					// Synchronize ZmedTrxhdr with SAP to update any reversed medical claim (process=4)
					ZmedTrxhdrSoapObject zmedTrxhdrSoapObject = new ZmedTrxhdrSoapObject(null, zmedTrxhdrDB.getId().getZclmno() , null);
					List<Map<String, String>> listReturn = muleConsumerService.getFromSAP(BAPIFunction.ZMEDTRXHDR_FETCH.service(),
							BAPIFunction.ZMEDTRXHDR_FETCH_URI.service(), zmedTrxhdrSoapObject);
					// synchronize ZmedTrxhdr fields with the fetched fields from SAP
					if (listReturn.get(0).get("zclmno") != null) {
						T001 currentBukrs = zmedTrxhdrDB.getBukrs();
						String currentProcess = zmedTrxhdrDB.getProcess();
						String currentReason = zmedTrxhdrDB.getReason();
						String currentBanka = zmedTrxhdrDB.getBanka();
						String currentBankn = zmedTrxhdrDB.getBankn();
						zmedTrxhdrDB = new ZmedTrxhdrMapper(listReturn.get(0)).getZmedTrxhdr();
						zmedTrxhdrDB.setBukrs(currentBukrs);
						zmedTrxhdrDB.setProcess(currentProcess);
						zmedTrxhdrDB.setReason(currentReason);
					    zmedTrxhdrDB.setBanka(currentBanka);
					    zmedTrxhdrDB.setBankn(currentBankn);
						if ("REVS".equals(zmedTrxhdrDB.getClmst())) {
							zmedTrxhdrDB.setProcess("4");
						}
					}
					
					
					zmedTrxhdrDB.setPaymentStatus(zmedTrxhdrEntity.getPaymentStatus());
					zmedTrxhdrDB.setPaymentDate(zmedTrxhdrEntity.getPaymentDate());
					zmedTrxhdrDB.setDocno(zmedTrxhdrEntity.getDocno());
					zmedTrxhdrDB.setLastSync(new Date());
					zmedTrxhdrDB.setPaymentStatus2(zmedTrxhdrEntity.getPaymentStatus2());
					zmedTrxhdrDB.setDocno2(zmedTrxhdrEntity.getDocno2());
					if (zmedTrxhdrEntity.getPaymentStatus().equals("P") || zmedTrxhdrEntity.getPaymentStatus2().equals("P")) {
						// Set Employee's Main Bank Detail
						Page<IT0009> page = it0009QueryService.findByPernrAndStatus(zmedTrxhdrDB.getPernr().longValue(), DocumentStatus.PUBLISHED.toString(), 1);
						if (page.hasContent() && page.getContent().get(0).getSubty().getId().getSubty().equals("0")) {
							IT0009 mainBank = page.getContent().get(0);
							zmedTrxhdrDB.setBanka(mainBank.getBankl() != null ? StringUtils.defaultString(mainBank.getBankl().getBanka()) : StringUtils.EMPTY);
							zmedTrxhdrDB.setBankn(StringUtils.defaultString(mainBank.getBankn()));
						}
					}
					if (StringUtils.isNotEmpty(zmedTrxhdrEntity.getDocno2()) && "CRTE".equals(zmedTrxhdrDB.getClmst())) {
						// if synchronized ZmedTrxhdr has document number but the clmst field is "CRTE" then force to set clmst = "APPR" (approved)
						zmedTrxhdrDB.setClmst("APPR");
						zmedTrxhdrDB.setProcess("2");
					}
					if (StringUtils.isNotEmpty(zmedTrxhdrEntity.getDocno2()) && "APPR".equals(zmedTrxhdrDB.getClmst())) {
						// if synchronized ZmedTrxhdr has document number and clmst field is "APPR" then force to set process = "2" (approved)
						zmedTrxhdrDB.setProcess("2");
					}
					
				    //zmedTrxhdrCommandService.threadUpdate(zmedTrxhdrDB);
				    try {
						zmedTrxhdrCommandService.save(zmedTrxhdrDB, null);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			
		}
		
	}

	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_criteria", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<ZmedTrxhdrResponseWrapper>> findByCriteria(
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "process", required = false) String process,
			@RequestParam(value = "finance_payment_status", required = false, defaultValue = "") String status,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {

		Page<ZmedTrxhdrResponseWrapper> responsePage = new PageImpl<ZmedTrxhdrResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<ZmedTrxhdrResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<ZmedTrxhdr> resultPage = null;
		String bukrs = null;
		Long pernr = null;
		String[] processes = StringUtils.isEmpty(process) ? null : process.split(",");
		String notStatus = status;
		if (status.equalsIgnoreCase("N")) {
			notStatus = "P";
		} else if (status.equalsIgnoreCase("P")) {
			notStatus = "N";
		}
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		
		try {
			if (StringUtils.isNotEmpty(ssn)) {
				pernr = Long.valueOf(ssn);
				if (!currentUser.get().getRoles().contains(adminRole.get()) &&
						!currentUser.get().getEmployee().getPernr().equals(pernr) ) {
					throw new AuthorizationException(
							"Insufficient Privileges. You can't fetch data of other Employee.");
				}
				resultPage = (processes != null && processes.length > 1) ?
						zmedTrxhdrQueryService.findByPernrAndProcessesWithPaging(pernr.longValue(), processes, notStatus, pageNumber) :
							zmedTrxhdrQueryService.findByPernrAndProcessWithPaging(pernr.longValue(), process, notStatus, pageNumber);
			} else {
				// no specified ssn, MUST BE ADMIN
				if (!currentUser.get().getRoles().contains(adminRole.get())) {
					throw new AuthorizationException(
							"Insufficient Privileges. Please login as 'ADMIN'!");
				}
				bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
				resultPage = (processes != null && processes.length > 1) ?
						zmedTrxhdrQueryService.findByBukrsAndProcessesWithPaging(bukrs, processes, notStatus, pageNumber) :
							zmedTrxhdrQueryService.findByBukrsAndProcessWithPaging(bukrs, process, notStatus, pageNumber);
			}
			
			updateDocumentStatus(resultPage.getContent());
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			
			if (resultPage.hasContent()) {

				List<ZmedTrxhdrResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(zmedTrxhdr -> {
					records.add(new ZmedTrxhdrResponseWrapper(zmedTrxhdr));
				});
				responsePage = new PageImpl<ZmedTrxhdrResponseWrapper>(records, zmedTrxhdrQueryService.createPageRequest(pageNumber),
						resultPage.getTotalElements());
				response.setData(responsePage);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(ZmedTrxhdrController.class).findByCriteria(ssn, process, status, pageNumber)).withSelfRel());
		
		return response;
	}


	@RequestMapping(method = RequestMethod.GET, value = "/pernr/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<ZmedTrxhdrResponseWrapper>> findByPernr(@PathVariable String pernr, 
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		
		
		ArrayData<ZmedTrxhdrResponseWrapper> bunchOfZmedTrxhdr = new ArrayData<>();
		APIResponseWrapper<ArrayData<ZmedTrxhdrResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfZmedTrxhdr);
		
		try {
			List<ZmedTrxhdr> listOfZmedTrxhdr = new ArrayList<>();
			
			//if (StringUtils.isNotEmpty(datbi)) {
				
				/*if(StringUtils.isNotEmpty(kodemed)){
					listOfZmedTrxhdr = zmedTrxhdrQueryService.findByKodemed(bukrs, kodemed, 
						CommonDateFunction.convertDateRequestParameterIntoDate(datbi)).stream().collect(Collectors.toList());
			
				} else */{
					listOfZmedTrxhdr = zmedTrxhdrQueryService.findByPernr(Long.parseLong(pernr)).stream().collect(Collectors.toList());
				
				}
			//}
			
			if (listOfZmedTrxhdr.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				updateDocumentStatus(listOfZmedTrxhdr);
				List<ZmedTrxhdrResponseWrapper> records = new ArrayList<>();
				for (ZmedTrxhdr ZmedTrxhdr : listOfZmedTrxhdr) {
					records.add(new ZmedTrxhdrResponseWrapper(ZmedTrxhdr));
				}
				bunchOfZmedTrxhdr.setItems(records.toArray(new ZmedTrxhdrResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(ZmedTrxhdrController.class).findByPernr(pernr, pageNumber)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/bukrs/{bukrs}/clmno/{clmno}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<ZmedTrxhdrResponseWrapper>> findOneByKey(@PathVariable String bukrs, 
			@PathVariable String clmno,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		
		
		ArrayData<ZmedTrxhdrResponseWrapper> bunchOfZmedTrxhdr = new ArrayData<>();
		APIResponseWrapper<ArrayData<ZmedTrxhdrResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfZmedTrxhdr);
		
		try {
			Optional<ZmedTrxhdr> zmedTrxhdr = zmedTrxhdrQueryService.findOneByCompositeKey(bukrs, clmno);
				
			
			if (!zmedTrxhdr.isPresent()) {
				response.setMessage("No Data Found");
			} else {
				List<ZmedTrxhdr> listOfZmedTrxhdr = new ArrayList<>();
				listOfZmedTrxhdr.add(zmedTrxhdr.get());
				updateDocumentStatus(listOfZmedTrxhdr);
				List<ZmedTrxhdrResponseWrapper> records = new ArrayList<>();
				//for (ZmedTrxhdr ZmedTrxhdr : listOfZmedTrxhdr) {
					records.add(new ZmedTrxhdrResponseWrapper(zmedTrxhdr.get()));
				//}
				bunchOfZmedTrxhdr.setItems(records.toArray(new ZmedTrxhdrResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(ZmedTrxhdrController.class).findOneByKey(bukrs, clmno, pageNumber)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated ZmedTrxhdrRequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> zmedTrxhdrHCMSEntity = hcmsEntityQueryService.findByEntityName(ZmedTrxhdr.class.getSimpleName());
			if (!zmedTrxhdrHCMSEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + ZmedTrxhdr.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<ZmedTrxhdr, ZmedTrxhdrRequestWrapper, String> newObjectContainer 
					= zmedTrxhdrRequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login first!");
				}
				
				// employee can't create claim for other employee, except Admin
				if (currentUser.get().getEmployee().getPernr().longValue() != request.getPernr() &&
						!currentUser.get().getRoles().contains(adminRole.get())) {
					throw new AuthorizationException(
							"Insufficient Privileges. Please login as 'ADMIN'!");
				}		
				
				Optional<ZmedTrxhdr> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
				//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = zmedTrxhdrCommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create ZmedTrxhdr data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("bukrs=" + savedEntity.get().getId().getBukrs());
				key.append(",zclmno=" + savedEntity.get().getId().getZclmno());
				
				EventLog eventLog = new EventLog(zmedTrxhdrHCMSEntity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				// Don't send email notification email for temporary claim number
				eventLog.setMailNotification(false);
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}

				ZmedTrxhdrResponseWrapper responseWrapper = new ZmedTrxhdrResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);
				
				/* Post to MuleSoft */
				
				Optional<ZmedTrxhdr> zmedTrxhdr = savedEntity;
				
				ZmedTrxhdr zmedTrxhdrEntity = zmedTrxhdr.get();
				ArrayList<ZmedTrxitm> listZmedTrxitm = (ArrayList<ZmedTrxitm>) zmedTrxitmQueryService.findByClmno(zmedTrxhdrEntity.getZclmno()).
						stream().collect(Collectors.toList());
				
				ZmedTrxhdrSoapObject zmedTrxhdrSoapObject = new ZmedTrxhdrSoapObject(zmedTrxhdrEntity, request.getProcess());
				List<ZmedTrxitmSoapObject> zmedTrxitmSoapObjects = new ArrayList<ZmedTrxitmSoapObject>();
				
				for (int i=0; i<listZmedTrxitm.size(); i++) {
					zmedTrxitmSoapObjects.add(new ZmedTrxitmSoapObject(listZmedTrxitm.get(i)));
				}
				zmedTrxhdrSoapObject.setTrxItems(zmedTrxitmSoapObjects);

				HashMap<String, String> resultMap = (HashMap<String, String>) muleConsumerService.
						postToSAPSync(BAPIFunction.ZMEDTRXHDR_CREATE.service(), BAPIFunction.ZMEDTRXHDR_CREATE_URI.service(), zmedTrxhdrSoapObject);
				if (resultMap.get("errorCode") == StringUtils.EMPTY && resultMap.get("message") != StringUtils.EMPTY &&
						resultMap.get("message").split(" ").length > 1) {
					String oldClaimNo = zmedTrxhdrEntity.getZclmno();
					String message = resultMap.get("message");
					String returnClaimNo = message.split(" ")[2];
					// update the claim number returned from SAP, and save the updated zmedTrxhdr to database
					zmedTrxhdr = zmedTrxhdrCommandService.updateZclmno(returnClaimNo, zmedTrxhdrEntity, currentUser.isPresent() ? currentUser.get() : null);
					if (!zmedTrxhdr.isPresent()) {
						throw new CannotPersistException("SAP Result: Cannot update ZmedTrxhdr (Medical Transaction Header) data. Please check your data!");
					}
					
					zmedTrxhdrSoapObject = new ZmedTrxhdrSoapObject(null, returnClaimNo, null);
					List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.ZMEDTRXHDR_FETCH.service(),
							BAPIFunction.ZMEDTRXHDR_FETCH_URI.service(), zmedTrxhdrSoapObject);
					// synchronize ZmedTrxhdr fields (in different thread) with the fetched fields from SAP
					if (list.get(0).get("zclmno") != null) {
					    zmedTrxhdrEntity = new ZmedTrxhdrMapper(list.get(0)).getZmedTrxhdr();
					    zmedTrxhdrEntity.setBukrs(zmedTrxhdr.get().getBukrs());
					    zmedTrxhdrEntity.setProcess(zmedTrxhdr.get().getProcess());
					    zmedTrxhdrEntity.setPaymentStatus("N");
					    // Set Employee's Main Bank Detail
						Page<IT0009> page = it0009QueryService.findByPernrAndStatus(zmedTrxhdrEntity.getPernr().longValue(), DocumentStatus.PUBLISHED.toString(), 1);
						if (page.hasContent() && page.getContent().get(0).getSubty().getId().getSubty().equals("0")) {
							IT0009 mainBank = page.getContent().get(0);
							zmedTrxhdrEntity.setBanka(mainBank.getBankl() != null ? StringUtils.defaultString(mainBank.getBankl().getBanka()) : StringUtils.EMPTY);
							zmedTrxhdrEntity.setBankn(StringUtils.defaultString(mainBank.getBankn()));
						}
					    zmedTrxhdrCommandService.threadUpdate(zmedTrxhdrEntity, eventLogCommandService, eventLog, currentUser.get());
					}
						
					for (ZmedTrxitm zmedTrxitm : listZmedTrxitm) {
						//zmedTrxitm.setClmno(returnClaimNo);
						Optional<ZmedTrxitm> zmedTrxitmUpdate = zmedTrxitmCommandService.updateClmno(returnClaimNo, zmedTrxitm,
								currentUser.isPresent() ? currentUser.get() : null);
						if (!zmedTrxitmUpdate.isPresent()) {
							throw new CannotPersistException("SAP Result: Cannot update ZmedTrxitm (Medical Transaction Item) data. Please check your data!");
						}
					}
					
					Optional<User> submitUser = userQueryService.findByPernr(zmedTrxhdr.get().getPernr());
					if (submitUser.isPresent()) {
						User user = submitUser.get();/*
						String processed = "CREATED.";
						if (zmedTrxhdr.get().getProcess().equals("2")) {
							processed = "APPROVED by your HR Admin.";
						} else if (zmedTrxhdr.get().getProcess().equals("3")) {
							processed = "REJECTED by your HR Admin with following reason:<br>".concat(zmedTrxhdr.get().getReason());
						}
				        mailService.sendMail(user.getEmail(), "CREATE ".
				    		concat(zmedTrxhdrHCMSEntity.get().getEntityName()),
				    		"Dear <b>".concat(user.getEmployee().getSname()).concat("</b><br><br>Your Medical Claim Number <b>".
					    	concat(returnClaimNo).concat("</b> has been ").
					    	concat(processed).concat("<br><br>Thank You.")));*/
						String processed = "<br><br>has been created, and waiting for approval from HR.";
						String adminProcessed = "<br><br>has been created, and waiting for your approval.";
						if (zmedTrxhdr.get().getProcess().equals("2")) {
							processed = "<br><br>has been approved, and waiting for payment from finance.";
							adminProcessed = "<br><br>has been approved by you, and waiting for payment from finance.";
						} else if (zmedTrxhdr.get().getProcess().equals("3")) {
							processed = "<br><br>has been rejected by HR Admin due to following reason :<br>".concat(
									StringUtils.defaultString(zmedTrxhdr.get().getReason()));
							adminProcessed = "<br><br>has been rejected by you due to following reason :<br>".concat(
									StringUtils.defaultString(zmedTrxhdr.get().getReason()));
						}
						eventLog.setStatus(zmedTrxhdr.get().getProcess());
						Map<String, String> composeMap = mailService.composeEmail(eventLog, user);
						if (StringUtils.isNotEmpty(composeMap.get("subject"))) {
							String mailBody = composeMap.get("body").replace("<br><br>".concat(mailService.getMailMedicalUpdate()), processed);
							// replace old claim number into the returned SAP claim number
							if (zmedTrxhdr.get().getProcess().equals("1")) {
								mailBody = mailBody.replace(oldClaimNo, returnClaimNo);
							}
						    mailService.sendMail(user.getEmail(), composeMap.get("subject"), mailBody);
						    
						    // Send notification email to Admin(s)
						    Optional<Collection<User>> users = userQueryService.fetchAdminByCompany(user.getEmployee().getT500p().getBukrs().getBukrs());
						    if (users.isPresent()) {
						    	List<User> adminUsers = users.get().stream().collect(Collectors.toList());
						        mailBody = composeMap.get("body").replace("Dear <b>".concat(user.getEmployee().getSname()), "Dear <b>HR ADMIN")
						    			.replace("Your medical claim below", "User medical claim below")
						    			.replace("<br><br>".concat(mailService.getMailMedicalUpdate()), adminProcessed)
						    			.replace("medical/page", "tasks/tasks_medical/page");
								// replace old claim number into the returned SAP claim number
								if (zmedTrxhdr.get().getProcess().equals("1")) {
									mailBody = mailBody.replace(oldClaimNo, returnClaimNo);
								}
						    	for (User adminUser : adminUsers) {
						    		mailService.sendMail(adminUser.getEmail(), composeMap.get("subject"), mailBody);
						    	}
						    }
						    
						}
					}
					
					responseWrapper = new ZmedTrxhdrResponseWrapper(zmedTrxhdrEntity);
					response = new APIResponseWrapper<>(responseWrapper);
					response.setMessage(message);
				    
				} else {
					zmedTrxhdrEntity.setProcess("0");
					responseWrapper = new ZmedTrxhdrResponseWrapper(zmedTrxhdrEntity);
					response = new APIResponseWrapper<>(responseWrapper);
					//response.setMessage("Cannot post Medical Transaction to SAP " + resultMap.get("errorMessage") +
							//". Data has been saved for Admin to Update/Re-Post to SAP");
					response.setMessage("Error: " + resultMap.get("errorMessage") + ".");
					System.out.println("MULESOFT FAILED on Medical Create - ".concat(response.getMessage()));
				}
				
				/* ************************** */	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(ZmedTrxhdrController.class).create(request, result)).withSelfRel());		
		return response;
	}

	
	
	
	
	
	
	
	
	
	
	
	@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated ZmedTrxhdrRequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			ZmedTrxhdrResponseWrapper responseWrapper = new ZmedTrxhdrResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<HCMSEntity> zmedTrxhdrHCMSEntity = hcmsEntityQueryService.findByEntityName(ZmedTrxhdr.class.getSimpleName());
					if (!zmedTrxhdrHCMSEntity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + ZmedTrxhdr.class.getSimpleName() + "' !!!");
					}
					
					Optional<ZmedTrxhdr> zmedTrxhdr = zmedTrxhdrQueryService.findOneByCompositeKey(
							request.getBukrs(),
							request.getZclmno());
					if (!zmedTrxhdr.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
					
					String currentProcess = zmedTrxhdr.get().getProcess();
				
					RequestObjectComparatorContainer<ZmedTrxhdr, ZmedTrxhdrRequestWrapper, String> updatedContainer =
							zmedTrxhdrRequestBuilderUtil.compareAndReturnUpdatedData(request, zmedTrxhdr.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
					
					if (updatedContainer.getEntity().isPresent()) {
						//updatedContainer.getEntity().get().setSeqnr(zmedTrxhdr.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated
						//zmedTrxhdr = zmedTrxhdrCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}
				
					if (!zmedTrxhdr.isPresent()) {
						throw new CannotPersistException("Cannot update ZmedTrxhdr (Medical Transaction Header) data. Please check your data!");
					}
					
					StringBuilder key = new StringBuilder("bukrs=" + zmedTrxhdr.get().getId().getBukrs());
					key.append(",zclmno=" + zmedTrxhdr.get().getId().getZclmno());
					
					EventLog eventLog = new EventLog(zmedTrxhdrHCMSEntity.get(), key.toString(), OperationType.UPDATE, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					eventLog.setMailNotification(false);
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					
					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}
				
					responseWrapper = new ZmedTrxhdrResponseWrapper(zmedTrxhdr.get());
					response = new APIResponseWrapper<>(responseWrapper);
					
					/* Post to MuleSoft */
					
					ZmedTrxhdr zmedTrxhdrEntity = zmedTrxhdr.get();
					ArrayList<ZmedTrxitm> listZmedTrxitm = (ArrayList<ZmedTrxitm>) zmedTrxitmQueryService.findByClmno(zmedTrxhdrEntity.getZclmno()).
							stream().collect(Collectors.toList());
					
					ZmedTrxhdrSoapObject zmedTrxhdrSoapObject = new ZmedTrxhdrSoapObject(zmedTrxhdrEntity, request.getProcess());
					List<ZmedTrxitmSoapObject> zmedTrxitmSoapObjects = new ArrayList<ZmedTrxitmSoapObject>();
					
					for (int i=0; i<listZmedTrxitm.size(); i++) {
						zmedTrxitmSoapObjects.add(new ZmedTrxitmSoapObject(listZmedTrxitm.get(i)));
					}
					zmedTrxhdrSoapObject.setTrxItems(zmedTrxitmSoapObjects);

					HashMap<String, String> resultMap = (HashMap<String, String>) muleConsumerService.
							postToSAPSync(BAPIFunction.ZMEDTRXHDR_CREATE.service(), BAPIFunction.ZMEDTRXHDR_CREATE_URI.service(), zmedTrxhdrSoapObject);
					if (resultMap.get("errorCode") == StringUtils.EMPTY && resultMap.get("message") != StringUtils.EMPTY &&
							resultMap.get("message").split(" ").length > 1) {
						String message = resultMap.get("message");
						String returnClaimNo = message.split(" ")[2];
						// update the claim number returned from SAP, and save the updated zmedTrxhdr to database
						//zmedTrxhdrEntity.setZclmno(zmedTrxhdrEntity.getZclmno());
						//zmedTrxhdr = zmedTrxhdrCommandService.save(zmedTrxhdrEntity, currentUser.isPresent() ? currentUser.get() : null);
						zmedTrxhdr = zmedTrxhdrCommandService.updateZclmno(returnClaimNo, zmedTrxhdrEntity, currentUser.isPresent() ? currentUser.get() : null);
						//zmedTrxhdr = zmedTrxhdrCommandService.save(zmedTrxhdrEntity, currentUser.isPresent() ? currentUser.get() : null);
						if (!zmedTrxhdr.isPresent()) {
							throw new CannotPersistException("SAP Result: Cannot update ZmedTrxhdr (Medical Transaction Header) data. Please check your data!");
						}
						
						zmedTrxhdrSoapObject = new ZmedTrxhdrSoapObject(null, returnClaimNo, null);
						List<Map<String, String>> list = muleConsumerService.getFromSAP(BAPIFunction.ZMEDTRXHDR_FETCH.service(),
								BAPIFunction.ZMEDTRXHDR_FETCH_URI.service(), zmedTrxhdrSoapObject);
						// synchronize ZmedTrxhdr fields (in different thread) with the fetched fields from SAP
						if (list.get(0).get("zclmno") != null) {
						    zmedTrxhdrEntity = new ZmedTrxhdrMapper(list.get(0)).getZmedTrxhdr();
						    zmedTrxhdrEntity.setBukrs(zmedTrxhdr.get().getBukrs());
						    zmedTrxhdrEntity.setProcess(zmedTrxhdr.get().getProcess());
						    zmedTrxhdrEntity.setPaymentStatus("N");
						    zmedTrxhdrEntity.setReason(zmedTrxhdr.get().getReason());
						    zmedTrxhdrEntity.setBanka(zmedTrxhdr.get().getBanka());
						    zmedTrxhdrEntity.setBankn(zmedTrxhdr.get().getBankn());
						    zmedTrxhdrCommandService.threadUpdate(zmedTrxhdrEntity, eventLogCommandService, eventLog, currentUser.get());
						}
							
						for (ZmedTrxitm zmedTrxitm : listZmedTrxitm) {
							//zmedTrxitm.setClmno(returnClaimNo);
							//Optional<ZmedTrxitm> zmedTrxitmUpdate = zmedTrxitmCommandService.save(zmedTrxitm, currentUser.isPresent() ? currentUser.get() : null);
							Optional<ZmedTrxitm> zmedTrxitmUpdate = zmedTrxitmCommandService.updateClmno(returnClaimNo, zmedTrxitm,
									currentUser.isPresent() ? currentUser.get() : null);
							if (!zmedTrxitmUpdate.isPresent()) {
								throw new CannotPersistException("SAP Result: Cannot update ZmedTrxitm (Medical Transaction Item) data. Please check your data!");
							}
						}
						
						Optional<User> submitUser = userQueryService.findByPernr(zmedTrxhdr.get().getPernr());
						if (submitUser.isPresent()) {
							User user = submitUser.get();/*
							String processed = "CREATED.";
							if (zmedTrxhdr.get().getProcess().equals("2")) {
								processed = "APPROVED by your HR Admin.";
							} else if (zmedTrxhdr.get().getProcess().equals("3")) {
								processed = "REJECTED by your HR Admin with following reason:<br>".concat(zmedTrxhdr.get().getReason());
							}
					        mailService.sendMail(user.getEmail(), "UPDATE ".
					    		concat(zmedTrxhdrHCMSEntity.get().getEntityName()),
					    		"Dear <b>".concat(user.getEmployee().getSname()).concat("</b><br><br>Your Medical Claim Number <b>".
					    		concat(returnClaimNo).concat("</b> has been ").
					    		concat(processed).concat("<br><br>Thank You.")));*/
							String processed = "<br><br>has been created, and waiting for approval from HR.";
							String adminProcessed = "<br><br>has been created, and waiting for your approval.";
							if (zmedTrxhdr.get().getProcess().equals("2")) {
								processed = "<br><br>has been approved, and waiting for payment from finance.";
								adminProcessed = "<br><br>has been approved by you, and waiting for payment from finance.";
							} else if (zmedTrxhdr.get().getProcess().equals("3")) {
								processed = "<br><br>has been rejected by HR Admin due to following reason :<br>".concat(StringUtils.defaultString(zmedTrxhdr.get().getReason()));
								adminProcessed = "<br><br>has been rejected by you due to following reason :<br>".concat(StringUtils.defaultString(zmedTrxhdr.get().getReason()));

							}
							eventLog.setStatus(zmedTrxhdr.get().getProcess());
							Map<String, String> composeMap = mailService.composeEmail(eventLog, user);
							if (StringUtils.isNotEmpty(composeMap.get("subject"))) {
								String mailBody = composeMap.get("body").replace("<br><br>".concat(mailService.getMailMedicalUpdate()), processed);
							    mailService.sendMail(user.getEmail(), composeMap.get("subject"), mailBody);
							    
							    // Send notification email to Admin
							    Optional<Collection<User>> users = userQueryService.fetchAdminByCompany(user.getEmployee().getT500p().getBukrs().getBukrs());
							    if (users.isPresent()) {
							    	List<User> adminUsers = users.get().stream().collect(Collectors.toList());
							        mailBody = composeMap.get("body").replace("Dear <b>".concat(user.getEmployee().getSname()), "Dear <b>HR ADMIN")
							    			.replace("Your medical claim below", "User medical claim below")
							    			.replace("<br><br>".concat(mailService.getMailMedicalUpdate()), adminProcessed)
							    			.replace("medical/page", "tasks/tasks_medical/page");
							    	for (User adminUser : adminUsers) {
							    		// send email only to current user
							    		if (adminUser.equals(currentUser.get()))
							    		    mailService.sendMail(adminUser.getEmail(), composeMap.get("subject"), mailBody);
							    	}
							    }
							}
						}
						
						responseWrapper = new ZmedTrxhdrResponseWrapper(zmedTrxhdrEntity);
						response = new APIResponseWrapper<>(responseWrapper);
						response.setMessage(message);
					    
					} else {
						zmedTrxhdrEntity.setProcess(currentProcess);
						responseWrapper = new ZmedTrxhdrResponseWrapper(zmedTrxhdrEntity);
						response = new APIResponseWrapper<>(responseWrapper);
						//response.setMessage("Cannot post Medical Transaction to SAP " + resultMap.get("errorMessage") +
								//". Data has been saved for Admin to Update/Re-Post to SAP");
						response.setMessage("Error: " + resultMap.get("errorMessage") +".");
						System.out.println("MULESOFT FAILED on Medical Update - ".concat(response.getMessage()));
						//throw new CannotPersistException("Cannot post Medical Transaction to SAP! All data operation will be rollback to initial state.");
					}
					
					/* ************************** */
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(ZmedTrxhdrController.class).edit(request, result)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	
	
	
	
	
	
	


	@RequestMapping(method = RequestMethod.GET, value = "/bukrs/{bukrs}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<ZmedTrxhdrResponseWrapper>> findByBukrs(@PathVariable String bukrs, 
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		
		
		ArrayData<ZmedTrxhdrResponseWrapper> bunchOfZmedTrxhdr = new ArrayData<>();
		APIResponseWrapper<ArrayData<ZmedTrxhdrResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfZmedTrxhdr);
		
		try {
			List<ZmedTrxhdr> listOfZmedTrxhdr = new ArrayList<>();
			
			//if (StringUtils.isNotEmpty(datbi)) {
				
				/*if(StringUtils.isNotEmpty(kodemed)){
					listOfZmedTrxhdr = zmedTrxhdrQueryService.findByKodemed(bukrs, kodemed, 
						CommonDateFunction.convertDateRequestParameterIntoDate(datbi)).stream().collect(Collectors.toList());
			
				} else */{
					listOfZmedTrxhdr = zmedTrxhdrQueryService.findByBukrs(bukrs).stream().collect(Collectors.toList());
				
				}
			//}
			
			if (listOfZmedTrxhdr.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				updateDocumentStatus(listOfZmedTrxhdr);
				List<ZmedTrxhdrResponseWrapper> records = new ArrayList<>();
				for (ZmedTrxhdr ZmedTrxhdr : listOfZmedTrxhdr) {
					records.add(new ZmedTrxhdrResponseWrapper(ZmedTrxhdr));
				}
				bunchOfZmedTrxhdr.setItems(records.toArray(new ZmedTrxhdrResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(ZmedTrxhdrController.class).findByBukrs(bukrs, pageNumber)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/clmno/{clmno}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<ZmedTrxhdrResponseWrapper>> findByZclmno(@PathVariable String clmno, 
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		
		
		ArrayData<ZmedTrxhdrResponseWrapper> bunchOfZmedTrxhdr = new ArrayData<>();
		APIResponseWrapper<ArrayData<ZmedTrxhdrResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfZmedTrxhdr);
		
		try {
			List<ZmedTrxhdr> listOfZmedTrxhdr = new ArrayList<>();
			
			//if (StringUtils.isNotEmpty(datbi)) {
				
				/*if(StringUtils.isNotEmpty(kodemed)){
					listOfZmedTrxhdr = zmedTrxhdrQueryService.findByKodemed(bukrs, kodemed, 
						CommonDateFunction.convertDateRequestParameterIntoDate(datbi)).stream().collect(Collectors.toList());
			
				} else */{
					listOfZmedTrxhdr = zmedTrxhdrQueryService.findByClmno(clmno).stream().collect(Collectors.toList());
				
				}
			//}
			
			if (listOfZmedTrxhdr.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				updateDocumentStatus(listOfZmedTrxhdr);
				List<ZmedTrxhdrResponseWrapper> records = new ArrayList<>();
				for (ZmedTrxhdr ZmedTrxhdr : listOfZmedTrxhdr) {
					records.add(new ZmedTrxhdrResponseWrapper(ZmedTrxhdr));
				}
				bunchOfZmedTrxhdr.setItems(records.toArray(new ZmedTrxhdrResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(ZmedTrxhdrController.class).findByZclmno(clmno, pageNumber)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	
	


	@RequestMapping(method = RequestMethod.GET, value = "/", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<ZmedTrxhdrResponseWrapper>> findAll(/*@PathVariable String bukrs,*/ 
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		
		
		ArrayData<ZmedTrxhdrResponseWrapper> bunchOfZmedTrxhdr = new ArrayData<>();
		APIResponseWrapper<ArrayData<ZmedTrxhdrResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfZmedTrxhdr);
		
		try {
			List<ZmedTrxhdr> listOfZmedTrxhdr = new ArrayList<>();
			
			//if (StringUtils.isNotEmpty(datbi)) {
				
				/*if(StringUtils.isNotEmpty(kodemed)){
					listOfZmedTrxhdr = zmedTrxhdrQueryService.findByKodemed(bukrs, kodemed, 
						CommonDateFunction.convertDateRequestParameterIntoDate(datbi)).stream().collect(Collectors.toList());
			
				} else */{
					listOfZmedTrxhdr = zmedTrxhdrQueryService.fetchAll().get().stream().collect(Collectors.toList());
				
				}
			//}
			
			if (listOfZmedTrxhdr.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<ZmedTrxhdrResponseWrapper> records = new ArrayList<>();
				for (ZmedTrxhdr ZmedTrxhdr : listOfZmedTrxhdr) {
					records.add(new ZmedTrxhdrResponseWrapper(ZmedTrxhdr));
				}
				bunchOfZmedTrxhdr.setItems(records.toArray(new ZmedTrxhdrResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(ZmedTrxhdrController.class).findAll( pageNumber)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}
	
	
}
