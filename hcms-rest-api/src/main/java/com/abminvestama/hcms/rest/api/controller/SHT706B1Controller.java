package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.SHT706B1;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.SHT706B1QueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.MedPatientResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.SHT706B1ResponseWrapper;

@RestController
@RequestMapping("/api/v1/exp_type")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class SHT706B1Controller extends AbstractResource {

	private SHT706B1QueryService sht706b1QueryService;
	
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	
	
	@Autowired
	void setSHT706B1QueryService(SHT706B1QueryService sht706b1QueryService) {
		this.sht706b1QueryService = sht706b1QueryService;
	}
	
	
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	
	
	
	
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/morei/{morei}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<SHT706B1ResponseWrapper>>
	/*APIResponseWrapper<?>*/ findByMoreiNE(@PathVariable String morei) throws Exception {
		
		ArrayData<SHT706B1ResponseWrapper> empCost = new ArrayData<>();
		APIResponseWrapper<ArrayData<SHT706B1ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(empCost);
		
		try {
					List<SHT706B1> listOfSHT706B1 = sht706b1QueryService.findByMoreiNE(morei).stream().collect(Collectors.toList());

					if (listOfSHT706B1.isEmpty()) {
						response.setMessage("No Data Found in sht706b1");
					} else {
						List<SHT706B1ResponseWrapper> records = new ArrayList<>();
						//records.add(new MedPatientResponseWrapper(it0001));
						for (SHT706B1 Zmed : listOfSHT706B1) {
							records.add(new SHT706B1ResponseWrapper(Zmed));
						}
						empCost.setItems(records.toArray(new SHT706B1ResponseWrapper[records.size()]));							
					}			
		} catch (NumberFormatException e) { 
			throw e;
		} catch (UsernameNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(SHT706B1Controller.class).findByMoreiNE(morei)).withSelfRel());
		return response;
	}
	
	
		
		

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}

}
