package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.WeakHashMap;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.TimeManagementConstants;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.Position;
import com.abminvestama.hcms.core.model.entity.PositionRelation;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.T527X;
import com.abminvestama.hcms.core.model.entity.T527XKey;
import com.abminvestama.hcms.core.model.entity.T705P;
import com.abminvestama.hcms.core.model.entity.TEVEN;
import com.abminvestama.hcms.core.model.entity.Tevenvw;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.TEVENCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T527XQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T705PQueryService;
import com.abminvestama.hcms.core.service.api.business.query.TEVENQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.util.MailService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.TEVENRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.TEVENResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.TevenvwResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.TEVENRequestBuilderUtil;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@RestController
@RequestMapping("/api/v1/clock_in_out")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class TEVENController extends AbstractResource {

	@Autowired
    private JavaMailSender mailSender;
	private MailService mailService;
	
	@Autowired
	private Environment envProperties;
	
	private TEVENCommandService tevenCommandService;
	private TEVENQueryService tevenQueryService;
	private TEVENRequestBuilderUtil tevenRequestBuilderUtil;

	private HCMSEntityQueryService hcmsEntityQueryService;
	private EventLogCommandService eventLogCommandService;
	
	private CommonServiceFactory commonServiceFactory;
	private UserQueryService userQueryService;
	
	private T705PQueryService t705pQueryService;
	private T527XQueryService t527xQueryService;
	
	//private Validator itValidator;
	
	

	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	
	
	@Autowired
	void setTEVENCommandService(TEVENCommandService tevenCommandService) {
		this.tevenCommandService = tevenCommandService;
	}
	
	@Autowired
	void setTEVENQueryService(TEVENQueryService tevenQueryService) {
		this.tevenQueryService = tevenQueryService;
	}
	
	@Autowired
	void setTEVENRequestBuilderUtil(TEVENRequestBuilderUtil tevenRequestBuilderUtil) {
		this.tevenRequestBuilderUtil = tevenRequestBuilderUtil;
	}

	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}

	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}

	@Autowired
	void setT705PQueryService(T705PQueryService t705pQueryService) {
		this.t705pQueryService = t705pQueryService;
	}

	@Autowired
	void setT527XQueryService(T527XQueryService t527xQueryService) {
		this.t527xQueryService = t527xQueryService;
	}

	@Autowired
	void setMailService(MailService mailService) {
		this.mailService = mailService;
	}
	
	/*@Autowired
	@Qualifier("itValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}*/
	
	
	
	
	/*
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<TEVENResponseWrapper>> findByPernr(@PathVariable Long pernr,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		
		ArrayData<TEVENResponseWrapper> bunchOfTEVEN = new ArrayData<>();
		APIResponseWrapper<ArrayData<TEVENResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfTEVEN);
		
		try {
			List<TEVEN> listOfTEVEN = new ArrayList<>();
			
			if(StringUtils.isNotEmpty(startDate)){
			
				if(!StringUtils.isNotEmpty(endDate)){

					Page<TEVEN> data = tevenQueryService.findByPernr(pernr, 
							CommonDateFunction.convertDateRequestParameterIntoDate(startDate),
							CommonDateFunction.convertDateRequestParameterIntoDate(startDate), pageNumber);
					if (data.hasContent()) {
						listOfTEVEN = data.getContent();
					} 
				} else {

					Page<TEVEN> data = tevenQueryService.findByPernr(pernr, 
							CommonDateFunction.convertDateRequestParameterIntoDate(startDate),
							CommonDateFunction.convertDateRequestParameterIntoDate(endDate), pageNumber);
					if (data.hasContent()) {
						listOfTEVEN = data.getContent();
					} 
				}
			} else {
				
				Page<TEVEN> data = tevenQueryService.findByPernr(pernr, pageNumber);
				if (data.hasContent()) {
					listOfTEVEN = data.getContent();
				} 				
			}
			
			
			
			
			if (listOfTEVEN.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<TEVENResponseWrapper> records = new ArrayList<>();
				for (TEVEN teven : listOfTEVEN) {
					records.add(new TEVENResponseWrapper(teven));
				}
				bunchOfTEVEN.setItems(records.toArray(new TEVENResponseWrapper[records.size()]));
			}
			
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(TEVENController.class).findByPernr(pernr, startDate, endDate, pageNumber)).withSelfRel());
		return response;
	}	
	*/

	/**
	 * @author wijanarko (wijanarko777@gmx.com)
	 * 
	 * Get List TEVEN VIEW - Check In And Check Out
	 * 
	 * @param pernr
	 * @param ssn -1:get all employee, <-1:get all employee without SSN, >0:get employee by SSN
	 * @param startDate
	 * @param endDate
	 * @param process
	 * @param trx
	 * @param lateFlag to show Late item, 1: show late; 0: skip criteria
	 * @param orderBy
	 * @param pageNumber
	 * @return List TEVEN View
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.GET, value = "/sn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<TevenvwResponseWrapper>> findAllEmpByCriteria(
			@PathVariable Long pernr,
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "process", required = false) String process,
			@RequestParam(value = "trx", required = false) String trx,
			@RequestParam(value = "lateFlag", required = false, defaultValue = "0") String lateFlag,
			@RequestParam(value = "orderBy", required = false, defaultValue = "ldate DESC, pernr") String orderBy,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		
		Page<TevenvwResponseWrapper> responsePage = new PageImpl<TevenvwResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<TevenvwResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		//read parameter
		pernr = pernr  != null ? -1L : pernr;
		Long sn = ssn == null || ssn.isEmpty() ? null : new Long(ssn); 
		
		//Authorization to get employee
		String roleName = "ROLE_USER";
		
		//User REQUESTOR
		Optional<User> currentUser = null;
		Optional<User> ssnUser = null;
		
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService()
				.findByName(Optional.of(TimeManagementConstants.ROLE_ADMIN));
		Optional<Role> superiorRole = commonServiceFactory.getRoleQueryService()
				.findByName(Optional.of(TimeManagementConstants.ROLE_SUPERIOR));
		
		currentUser = userQueryService.findByUsername(currentUser());
		if (sn != null && sn != -1L)
			ssnUser = userQueryService.findByPernr(Math.abs(sn));
		
		//RULE HR ADMIN
		if (currentUser.get().getRoles().contains(adminRole.get())) {
			if (sn == null) {
				roleName = TimeManagementConstants.ROLE_ADMIN_WITH_HR_ADMIN; //get All employee with HR ADMIN by Company code
			}
			else if (sn < 0L && sn != -1L) {
				roleName = TimeManagementConstants.ROLE_ADMIN_WITHOUT_HR_ADMIN; //get All employee without HR ADMIN by Company code
			}
			else if (sn > 0L) {
				roleName = TimeManagementConstants.ROLE_ADMIN; //Show employee by SN only
			}
		}
		//RULE SUPERIOR
		else if (currentUser.get().getRoles().contains(superiorRole.get())) {
			if (sn == null) {
				roleName = TimeManagementConstants.ROLE_SUPERIOR_WITH_SUPERIOR; //get All Employee(With Superior) by Position Relation Superior
			}
			else if (sn < 0L && sn != -1L) {
				roleName = TimeManagementConstants.ROLE_SUPERIOR_WITHOUT_SUPERIOR; //get All Employee(Without Superior) by Position Relation Superior
			}
			else if (sn > 0L) {
				roleName = TimeManagementConstants.ROLE_SUPERIOR; //Show employee by SN only
			}
		}
		//RULE USER ONLY
		else {
			roleName = TimeManagementConstants.ROLE_USER;
		}
		
		try {
			Date tmpStartDate = StringUtils.isNotEmpty(startDate) ? CommonDateFunction.convertDateRequestParameterIntoDate(startDate) : null;
			Date tmpEndDate = StringUtils.isNotEmpty(endDate) ? CommonDateFunction.convertDateRequestParameterIntoDate(endDate) : null;
			Page<Tevenvw> resultPage = tevenQueryService.fetchAllEmpByCriteria(roleName, currentUser.get(),
					(ssnUser != null ? ssnUser.get() : null), tmpStartDate, tmpEndDate, process, trx, lateFlag, orderBy, pageNumber);
			if (resultPage == null || !resultPage.hasContent())
				response.setMessage("No Data Found");
			else {
				List<TevenvwResponseWrapper> records = new ArrayList<>();
				Optional<User> emp = null;
				Optional<T527X> dept = null;
				Optional<T527XKey> deptKey = null;
				for (Tevenvw tevenvw : resultPage.getContent()) {
					emp = userQueryService.findByPernr(tevenvw.getPernr());
					tevenvw.setDivisionName(emp.get().getEmployee().getT500p().getPbtxt());
					tevenvw.setDepartmentName(emp.get().getEmployee().getT527x().getOrgtx());
				}
				resultPage.getContent().forEach(tevenvw -> {
					records.add(new TevenvwResponseWrapper(tevenvw));
				});
				if (pageNumber != -1L) {
					responsePage = new PageImpl<TevenvwResponseWrapper> (records, tevenQueryService.createPageRequest(pageNumber),
							resultPage.getTotalElements());
				}
				else {
					responsePage = new PageImpl<TevenvwResponseWrapper> (records, new PageRequest(0, resultPage.getSize()), resultPage.getTotalElements());
				}
				response.setData(responsePage);
			}
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(TEVENController.class).findAllEmpByCriteria(pernr, ssn, startDate, endDate,
				process, trx, lateFlag, orderBy, pageNumber)).withSelfRel());
		return response;
	}

	/**
	 * Select single row
	 * @param pernr
	 * @param ldate
	 * @param satza
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/singlerow/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<TEVENResponseWrapper>> findOneByCompositeKeys(@PathVariable Long pernr,
			@RequestParam(value = "ldate", required = false) String ldate,
			@RequestParam(value = "satza", required = false) String satza) throws Exception {
		
		ArrayData<TEVENResponseWrapper> bunchOfTEVEN = new ArrayData<>();
		APIResponseWrapper<ArrayData<TEVENResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfTEVEN);
		
		try {
			if(pernr == null) {
				response.setMessage("S.N is empty");
				return response;
			} else if(ldate == null || ldate.isEmpty()) {
				response.setMessage("Date is empty");
				return response;
			} else if(satza == null || satza.isEmpty()) {
				response.setMessage("Time Event Type is empty");
				return response;
			}
			
			TEVEN tevenDB = new TEVEN();
			Optional<T705P> t705p = t705pQueryService.findBySatza(satza);
			if(!t705p.isPresent()) {
				response.setMessage("Time Event Type not found");
				return response;
			}
			tevenDB.setPmbde(t705p != null && t705p.isPresent() ? t705p.get().getPmbde() : null);
			tevenDB.setSatza(t705p.isPresent() ? t705p.get() : null);
			
			Optional<TEVEN> optTevens = tevenQueryService.findOneByCompositeKeys(
					pernr,
					CommonDateFunction.convertDateRequestParameterIntoDate(ldate),
					tevenDB.getSatza());
			if(!optTevens.isPresent()) {
				response.setMessage("No Data Found");
			} else {
				List<TEVENResponseWrapper> records = new ArrayList<>();
				records.add(new TEVENResponseWrapper(optTevens.get()));
				bunchOfTEVEN.setItems(records.toArray(new TEVENResponseWrapper[records.size()]));
			}
			
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(TEVENController.class).findOneByCompositeKeys(pernr, ldate, satza)).withSelfRel());
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/criteria/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<TEVENResponseWrapper>> findAllByCriteria(@PathVariable Long pernr,
			@RequestParam(value = "ldate", required = false) String ldate,
			@RequestParam(value = "satza", required = false) String satza) throws Exception {
		
		ArrayData<TEVENResponseWrapper> bunchOfTEVEN = new ArrayData<>();
		APIResponseWrapper<ArrayData<TEVENResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfTEVEN);
		
		try {
			if(pernr == null) {
				response.setMessage("S.N is empty");
				return response;
			} else if(ldate == null || ldate.isEmpty()) {
				response.setMessage("Date is empty");
				return response;
			} else if(satza == null || satza.isEmpty()) {
				response.setMessage("Time Event Type is empty");
				return response;
			}
			
			TEVEN tevenDB = new TEVEN();
			Optional<T705P> t705p = t705pQueryService.findBySatza(satza);
			if(!t705p.isPresent()) {
				response.setMessage("Time Event Type not found");
				return response;
			}
			tevenDB.setPmbde(t705p != null && t705p.isPresent() ? t705p.get().getPmbde() : null);
			tevenDB.setSatza(t705p.isPresent() ? t705p.get() : null);
			
			Optional<TEVEN> optTevens = tevenQueryService.findOneByCompositeKeys(
					new Long(pernr),
					CommonDateFunction.convertDateRequestParameterIntoDate(ldate),
					tevenDB.getSatza());
			if(!optTevens.isPresent()) {
				response.setMessage("No Data Found");
			} else {
				List<TEVENResponseWrapper> records = new ArrayList<>();
				records.add(new TEVENResponseWrapper(optTevens.get()));
				bunchOfTEVEN.setItems(records.toArray(new TEVENResponseWrapper[records.size()]));
			}
			
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(TEVENController.class).findOneByCompositeKeys(pernr, ldate, satza)).withSelfRel());
		return response;
	}

	/**
	 * 
	 * @author wijanarko (wijanarko777@gmx.com)
	 * 
	 * Get data teven
	 * 
	 * @param pernr
	 * @param startDate
	 * @param endDate
	 * @param pageNumber
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<TEVENResponseWrapper>> findByPernr(@PathVariable Long pernr,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		
		ArrayData<TEVENResponseWrapper> bunchOfTEVEN = new ArrayData<>();
		APIResponseWrapper<ArrayData<TEVENResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfTEVEN);
		
		try {
			List<TEVEN> listOfTEVEN = new ArrayList<>();
			
			if(StringUtils.isNotEmpty(startDate)){
			
				if(!StringUtils.isNotEmpty(endDate)){

					Page<TEVEN> data = tevenQueryService.findByPernr(pernr, 
							CommonDateFunction.convertDateRequestParameterIntoDate(startDate),
							CommonDateFunction.convertDateRequestParameterIntoDate(startDate), pageNumber);
					if (data.hasContent()) {
						listOfTEVEN = data.getContent();
					} 
				} else {

					Page<TEVEN> data = tevenQueryService.findByPernr(pernr, 
							CommonDateFunction.convertDateRequestParameterIntoDate(startDate),
							CommonDateFunction.convertDateRequestParameterIntoDate(endDate), pageNumber);
					if (data.hasContent()) {
						listOfTEVEN = data.getContent();
					} 
				}
			} else {
				
				Page<TEVEN> data = tevenQueryService.findByPernr(pernr, pageNumber);
				if (data.hasContent()) {
					listOfTEVEN = data.getContent();
				} 				
			}
			
			
			
			
			if (listOfTEVEN.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<TEVENResponseWrapper> records = new ArrayList<>();
				for (TEVEN teven : listOfTEVEN) {
					records.add(new TEVENResponseWrapper(teven));
				}
				bunchOfTEVEN.setItems(records.toArray(new TEVENResponseWrapper[records.size()]));
			}
			
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(TEVENController.class).findByPernr(pernr, startDate, endDate, pageNumber)).withSelfRel());
		return response;
	}
	
	

	
	
	
	
	/**
	 * Modify by WIJANARKO
	 * 
	 * @param request <br>
	 * 			trxCode: C=Create, E=Edit, S=Submit/Approved, R=Rejected
	 * @param result
	 * 			
	 * @return <br>
	 * 			trxCode:<p>
	 * 			AC=Auto-Created, AE=Editable, AS1=WaitSPV, AS2=WaitHrAdmin, <br>
	 * 			AF1=FinalApproveSPV, AF2=FinalRejectSPV, <br>
	 * 			AF3=FinalApproveHrAdmin, AF4=FinalRejectHrAdmin, <br>
	 * 			MC=Manual-Created, ME=Editable, MS1=WaitSPV, MS2=WaitHrAdmin, <br>
	 * 			MF1=FinalApproveSPV, MF2=FinalRejectSPV, <br>
	 * 			MF3=FinalApproveHrAdmin, MF4=FinalRejectHrAdmin</p>
	 * 			process:<p>
	 * 			[EMP - Not HrAdmin]<br>
	 * 			0=Create/Editable, 1=Submitted, 2=SPV1, 3=SPV2....6=SPV5, 7=HrAdmin<br>
	 * 			[EMP - HrAdmin]<br>
	 * 			0=Create/Editable, 1=Submitted, 2=SPV</p> 
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated TEVENRequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean hasAccess = false;
			boolean isDataChanged = false;
			
			TEVENResponseWrapper responseWrapper = new TEVENResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			
			edit: {
				try {
					//Validation Target USER TEVEN
					Optional<T705P> t705p = t705pQueryService.findBySatza(request.getSatza());
					if(!t705p.isPresent()) {
						throw new Exception("Time Event Type not found");
					}
					
					Optional<TEVEN> teven = tevenQueryService.findOneByCompositeKeys(request.getPernr(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getLdate()), t705p.get());
					if (!teven.isPresent()) {
						throw new Exception("Cannot update data. No Data Found!");
					}
					
					String hasCurrentUserRole = null;
					//USER REQUESTOR
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
					//Target USER TEVEN
					Optional<User> emp = userQueryService.findByPernr(request.getPernr());
					
					Optional<Role> adminRole = commonServiceFactory.getRoleQueryService()
							.findByName(Optional.of(TimeManagementConstants.ROLE_ADMIN));
					Optional<Role> superiorRole = commonServiceFactory.getRoleQueryService()
							.findByName(Optional.of(TimeManagementConstants.ROLE_SUPERIOR));
					
					//RULE USER
					if (request.getPernr() == currentUser.get().getEmployee().getPernr()) {
						hasCurrentUserRole = TimeManagementConstants.ROLE_USER;
					}
					//RULE HR ADMIN
					else if (currentUser.get().getRoles().contains(adminRole.get())) {
						hasCurrentUserRole = TimeManagementConstants.ROLE_ADMIN;
					}
					//RULE SUPERIOR
					else if (currentUser.get().getRoles().contains(superiorRole.get())) {
						hasCurrentUserRole = TimeManagementConstants.ROLE_SUPERIOR;
					}
					
					//Validation after Submitted
					if (new Long(teven.get().getProcess()) > 0L) {
						//validation SELF APPROVE
						if (currentUser.get().getEmployee().getPernr() == request.getPernr()) {
							throw new Exception("Unauthorized to self approved!");
						}
						
						//validation Duplicate APPROVE
						boolean multApproveFlag = false;
						if (teven.get().getApproved1By() != null && teven.get().getApproved1By().getId().equals(currentUser.get().getId())) {
							multApproveFlag = true;
						}
						if (teven.get().getApproved2By() != null && teven.get().getApproved2By().getId().equals(currentUser.get().getId())) {
							multApproveFlag = true;
						}
						if (teven.get().getApproved3By() != null && teven.get().getApproved3By().getId().equals(currentUser.get().getId())) {
							multApproveFlag = true;
						}
						if (teven.get().getApproved4By() != null && teven.get().getApproved4By().getId().equals(currentUser.get().getId())) {
							multApproveFlag = true;
						}
						if (teven.get().getApproved5By() != null && teven.get().getApproved5By().getId().equals(currentUser.get().getId())) {
							multApproveFlag = true;
						}
						if (teven.get().getApproved6By() != null && teven.get().getApproved6By().getId().equals(currentUser.get().getId())) {
							multApproveFlag = true;
						}
						
						if (multApproveFlag) {
							throw new Exception("Unauthorized duplicate approve!");
						}
					}
					
					// VALIDATION(waiting approval transaction) - RELASE STRATEGY
					if (request.getTrxcode().contains("S")) {
						// 1. RULE_USER (can not change other user)
						if (hasCurrentUserRole.equals(TimeManagementConstants.ROLE_USER)
								&& currentUser.get().getEmployee().getPernr() != request.getPernr()) {
//							response.setMessage("Unauthorized to change item!");
//							return response;
							throw new Exception("Unauthorized to change item!");
						}
						// 2.RULE_SUPERIOR & ROLE_HR_ADMIN
						else if (hasCurrentUserRole.equals(TimeManagementConstants.ROLE_SUPERIOR) 
									|| hasCurrentUserRole.equals(TimeManagementConstants.ROLE_ADMIN)) {
							// Validation USER_TEVEN with SUPERIOR
							Position superiorPosition = currentUser.get().getEmployee().getPosition();
							Position empPosition = emp.get().getEmployee().getPosition();
							List<PositionRelation> empPositionRelations = commonServiceFactory
									.getPositionRelationQueryService().findByToPositionId(superiorPosition.getId()).stream()
									.collect(Collectors.toList());
							//boolean hasAccess = false;
							for (PositionRelation empPositionRelation : empPositionRelations) {
								if (empPositionRelation.getFromPosition().getId().equals(empPosition.getId())) {
									hasAccess = true;
								}
							}
							// validation SUPERIOR only, HR ADMIN skip validation.
							if (!hasAccess && hasCurrentUserRole.equals(TimeManagementConstants.ROLE_SUPERIOR)) {
//								response.setMessage("Unauthorized superior to change item!");
//								return response;
								throw new Exception("Unauthorized superior to change item!");
							}
						}
					}
					
					// fetch all user's Superior
					List<PositionRelation> empPositionRelations = commonServiceFactory.getPositionRelationQueryService()
							.findByFromPositionid(emp.get().getEmployee().getPosition().getId()).stream()
							.collect(Collectors.toList());
					int totalSuperior = empPositionRelations.size();
					
					//Optional<TEVEN> teven = tevenQueryService.findOneByCompositeKey(request.getPdsnr());
					/*
					 * SOURCE, move to line //Validation Target USER TEVEN
					 * 
					Optional<T705P> t705p = t705pQueryService.findBySatza(request.getSatza());
					if(!t705p.isPresent()) {
						response.setMessage("Time Event Type not found");
						return response;
					}
					
					Optional<TEVEN> teven = tevenQueryService.findOneByCompositeKeys(request.getPernr(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getLdate()), t705p.get());
					if (!teven.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
					*/
					Date ldate = teven.get().getLdate();
					Date ltime = teven.get().getLtime();
					Date agdate = teven.get().getAgdate();
					Date agtime = teven.get().getAgtime();
					String trxCode = teven.get().getTrxcode();
					Long process = Long.parseLong(teven.get().getProcess());
					String approvalNote = teven.get().getApprovalNote() != null ? teven.get().getApprovalNote() : "";
				
					RequestObjectComparatorContainer<TEVEN, TEVENRequestWrapper, String> updatedContainer 
						= tevenRequestBuilderUtil.compareAndReturnUpdatedData(request, teven.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
//						response.setMessage("No data changed. No need to perform the update.");
//						break edit;
						throw new Exception("No data changed. No need to perform the update.");
					}
				
					//Optional<User> currentUser = userQueryService.findByUsername(currentUser());
					
					//DATA READY TO EDIT
					if (updatedContainer.getEntity().isPresent()) {
						Date thisDate = new Date();
						
						//CHANGE BY
						updatedContainer.getEntity().get().setUname(currentUser.get().getId());
						updatedContainer.getEntity().get().setAedtm(thisDate);
						
						//EDIT & SUBMIT
						if (process == 0L) {
							//EDIT
							if (request.getTrxcode().contains("E")) {
								/* 
								 * sudah di handle QUERY HCMS - SCHEDULER EVERY DAY(ABSENSI)
								 * 
								//For reset data actual MST
								if (trxCode.contains("A")
										&& updatedContainer.getEntity().get().getAgdate() == null) {
									updatedContainer.getEntity().get().setAgdate(ldate);
									updatedContainer.getEntity().get().setAgtime(ltime);
								}
								*/
								updatedContainer.getEntity().get().setTrxcode(trxCode.substring(0,1) + "E");
								updatedContainer.getEntity().get().setSubmittedBy(null);
								updatedContainer.getEntity().get().setSubmittedAt(null);
								updatedContainer.getEntity().get().setApproved1By(null);
								updatedContainer.getEntity().get().setApproved1At(null);
								updatedContainer.getEntity().get().setApproved2By(null);
								updatedContainer.getEntity().get().setApproved2At(null);
								updatedContainer.getEntity().get().setApproved3By(null);
								updatedContainer.getEntity().get().setApproved3At(null);
								updatedContainer.getEntity().get().setApproved4By(null);
								updatedContainer.getEntity().get().setApproved4At(null);
								updatedContainer.getEntity().get().setApproved5By(null);
								updatedContainer.getEntity().get().setApproved5At(null);
								updatedContainer.getEntity().get().setApproved6By(null);
								updatedContainer.getEntity().get().setApproved6At(null);
							}
							//SUBMIT
							else if (request.getTrxcode().contains("S")) {
								updatedContainer.getEntity().get().setSubmittedBy(currentUser.get());
								updatedContainer.getEntity().get().setSubmittedAt(thisDate);
								updatedContainer.getEntity().get().setTrxcode(trxCode.substring(0, 1) + "S1");
								process++;
								updatedContainer.getEntity().get().setProcess("" + process);
							}
							else {
//								response.setMessage("Invalid transaction.");
//								break edit;
								throw new Exception("Invalid transaction.");
							}
						}
						//SUPERIOR & HR ADMIN
						else if (request.getTrxcode().contains("S")
								|| request.getTrxcode().contains("R")
								) {
							if (process == 1L) {
								updatedContainer.getEntity().get().setApproved1By(currentUser.get());
								updatedContainer.getEntity().get().setApproved1At(thisDate);
							}
							else if (process == 2L) {
								updatedContainer.getEntity().get().setApproved2By(currentUser.get());
								updatedContainer.getEntity().get().setApproved2At(thisDate);
							}
							else if (process == 3L) {
								updatedContainer.getEntity().get().setApproved3By(currentUser.get());
								updatedContainer.getEntity().get().setApproved3At(thisDate);
							}
							else if (process == 4L) {
								updatedContainer.getEntity().get().setApproved4By(currentUser.get());
								updatedContainer.getEntity().get().setApproved4At(thisDate);
							}
							else if (process == 5L) {
								updatedContainer.getEntity().get().setApproved5By(currentUser.get());
								updatedContainer.getEntity().get().setApproved5At(thisDate);
							}
							else if (process == 6L) {
								updatedContainer.getEntity().get().setApproved6By(currentUser.get());
								updatedContainer.getEntity().get().setApproved6At(thisDate);
							}
							
							String reqApprovalMsg = "";
							String approvalStatus = "";
							approvalNote += new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(thisDate) + " ";
							approvalNote += "by " + currentUser.get().getEmployee().getEname() + " (STATUS_APPROVAL):" + System.lineSeparator();
							approvalNote += "APPROVAL_MSG" + System.lineSeparator();
							
							// APPROVE
							if (request.getTrxcode().contains("S")) {
								approvalStatus = "APPROVED";
								
								// EMPLOYEE(HR ADMIN)
								// *)Enhancement case Company SS & CK: approval to the SUPERIOR level only.
								String approvalSuperiorOnly = envProperties.getProperty("workflow.teven.approval.superiorOnly");
								if (emp.get().getRoles().contains(adminRole.get()) 
										|| approvalSuperiorOnly.contains(emp.get().getEmployee().getT500p().getBukrs().getBukrs())) {
									// SUPERIOR - APPROVED(FINAL State)
									updatedContainer.getEntity().get().setTrxcode(trxCode.substring(0, 1) + "F1");
								}
								// EMPLOYEE(Not HR ADMIN)
								else {
									// HR ADMIN - APPROVED
									if (trxCode.contains("S2") 
											&& currentUser.get().getRoles().contains(adminRole.get())) {
										updatedContainer.getEntity().get().setTrxcode(trxCode.substring(0,1) + "F3");
									}
									//SUPERIOR - APPROVED
									else if (trxCode.contains("S1") 
												&& currentUser.get().getRoles().contains(superiorRole.get())){
										// validation total superior
										if (process.intValue() == totalSuperior) {
											// SUPERIOR - LAST APPROVED & NEXT LEVEL HR ADMIN
											updatedContainer.getEntity().get().setTrxcode(trxCode.substring(0,1) + "S2");
										} else {
											// SUPERIOR - APPROVED(Not Yet LAST)
											updatedContainer.getEntity().get().setTrxcode(trxCode.substring(0,1) + "S1");
										}
									}
									else {
										throw new Exception("Unauthorized approve!");
									}
								}
							}
							// REJECT
							else if (request.getTrxcode().contains("R")) {
								approvalStatus = "REJECTED";
								
								//Reset data actual MST(TEVEN TYPE CODE = Automatic)
								if (trxCode.contains("A")) {
									updatedContainer.getEntity().get().setLdate(agdate);
									updatedContainer.getEntity().get().setLtime(agtime);
								}
								// HR ADMIN - REJECTED
								if (trxCode.contains("S2") 
										&& currentUser.get().getRoles().contains(adminRole.get())) {
									updatedContainer.getEntity().get().setTrxcode(trxCode.substring(0,1) + "F4");
								}
								// SUPERIOR - REJECTED
								else if (trxCode.contains("S1") 
										&& currentUser.get().getRoles().contains(superiorRole.get())) {
									updatedContainer.getEntity().get().setTrxcode(trxCode.substring(0,1) + "F2");
								}
								else {
									throw new Exception("Unauthorized approve!");
								}
							}
							
							//set "approvalNote"(from Front End)
							reqApprovalMsg = "- " + (request.getApprovalNote() != null ? request.getApprovalNote() : "");
							approvalNote = approvalNote.replace("STATUS_APPROVAL", approvalStatus);
							approvalNote = approvalNote.replace("APPROVAL_MSG", reqApprovalMsg);
							updatedContainer.getEntity().get().setApprovalNote(approvalNote);
							
							process++;
							updatedContainer.getEntity().get().setProcess("" + process);
						}
						else {
//							response.setMessage("Invalid transaction.");
//							break edit;
							throw new Exception("Invalid transaction.");
						}
						teven = tevenCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						
						// SEND MAIL
						tevenSendMail(currentUser.get(), emp.get(), updatedContainer.getEntity().get());
					}
				
					if (!teven.isPresent()) {
						throw new CannotPersistException("Cannot update TEVEN (Absence Quota Types) data. Please check your data!");
					}
					
					// SAVE TO EVENT LOG
					tevenSaveToEventLog(teven, OperationType.UPDATE, updatedContainer, currentUser);
				
					responseWrapper = new TEVENResponseWrapper(teven.get());
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(TEVENController.class).edit(request, result)).withSelfRel());
		return response;
	}	

	/**
	 * SAVE TO EVENT LOG
	 * 
	 * @param teven
	 * @param operationType
	 * @param currentUser
	 */
	private void tevenSaveToEventLog(Optional<TEVEN> teven, OperationType operationType,
			RequestObjectComparatorContainer<TEVEN, TEVENRequestWrapper, String> objContainer,
			Optional<User> currentUser) {
		StringBuilder key = new StringBuilder("tevenid=" + teven.get().getTevenid());
		Optional<HCMSEntity> tevenEntity = hcmsEntityQueryService.findByEntityName(TEVEN.class.getSimpleName());
		
		if (!tevenEntity.isPresent()) 
			throw new EntityNotFoundException("Cannot find Entity '" + TEVEN.class.getSimpleName() + "' !!!");
		
		EventLog eventLog = new EventLog(tevenEntity.get(), key.toString(), operationType, objContainer.getEventData().get());
		eventLog.setStatus("process=" + teven.get().getProcess() + ",trxcode=" + teven.get().getTrxcode());
		Optional<EventLog> updateEventLog;
		try {
			updateEventLog = eventLogCommandService.save(eventLog, currentUser.get());
			if (!updateEventLog.isPresent())
				throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
		} catch (ConstraintViolationException | CannotPersistException | NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	 * SEND TO MAIL (process >= 1)
	 */
	private void tevenSendMail(User currentUser, User requestorEmployee, TEVEN teven) {
		// [EMPLOYEE / HR ADMIN] - Submitted
		if (Long.parseLong(teven.getProcess()) == 1L) {
			// TO EMPLOYEE
			mailTemplate(TimeManagementConstants.TEVEN_MAIL_SUBMIT_TO_EMP, currentUser, requestorEmployee, teven);
			
			// TO SUPERIOR
			mailTemplate(TimeManagementConstants.TEVEN_MAIL_SUBMIT_TO_SPV, currentUser, requestorEmployee, teven);
		}
		// SUPERIOR - Approval
		else if (Long.parseLong(teven.getProcess()) > 1L && teven.getTrxcode().contains("S1")) {
			// TO EMPLOYEE
			mailTemplate(TimeManagementConstants.TEVEN_MAIL_SPV_TO_EMP, currentUser, requestorEmployee, teven);
			
			// TO HR ADMIN
			mailTemplate(TimeManagementConstants.TEVEN_MAIL_SPV_TO_HR, currentUser, requestorEmployee, teven);
		}
		// LAST SUPERIOR - Approval
		else if (teven.getTrxcode().contains("S2")) {
			// TO EMPLOYEE
			mailTemplate(TimeManagementConstants.TEVEN_MAIL_SPV_TO_EMP, currentUser, requestorEmployee, teven);
			
			// TO HR ADMIN
			mailTemplate(TimeManagementConstants.TEVEN_MAIL_SPV_TO_HR, currentUser, requestorEmployee, teven);
		}
		// LAST SUPERIOR(HR ADMIN) or HR ADMIN
		else if (teven.getTrxcode().contains("F")) {
			// SUPERIOR(HR ADMIN) - SEND MAIL TO EMPLOYEE
			if (teven.getTrxcode().contains("F1") || teven.getTrxcode().contains("F2")) {
				// F1: SUPERIOR - APPROVED, F2: SUPERIOR - REJECTED
				mailTemplate(TimeManagementConstants.TEVEN_MAIL_SPV_TO_EMP, currentUser, requestorEmployee, teven);
			}
			
			// HR ADMIN - SEND MAIL TO EMPLOYEE
			else if (teven.getTrxcode().contains("F3") || teven.getTrxcode().contains("F4")) {
				// F3: SUPERIOR - APPROVED, F4: SUPERIOR - REJECTED
				mailTemplate(TimeManagementConstants.TEVEN_MAIL_HR_TO_EMP, currentUser, requestorEmployee, teven);
			}
		}
	}
	
	private void mailTemplate(String action, User currentUser, User requestorEmployee, TEVEN teven) {
		int process 		= new Integer(teven.getProcess());
		String satza 		= teven.getSatza().getSatza();
		String ssn			= teven.getPernr().toString();
		String employeeName = requestorEmployee.getEmployee().getEname(); //target TEVEN of employee
		String trxCode 		= teven.getTrxcode();
		
		String ACTION_CODE	= "";
		String DEAR_NAME	= "";
		String ACTOR_INFO	= "";
		String MAIL_TO		= "";
		String MAIL_SUBJECT	= "UPDATE Attendance TEVEN_TRX ";
		String MAIL_BODY	= "";
		String MAIL_HREF	= "http://localhost:3000";
		String mailHeader	= "";
		String mailContent	= "";
		String mailFooter	= "";
		String sDateTime	= "";
		
		MAIL_HREF = ((JavaMailSenderImpl) mailSender).getJavaMailProperties().getProperty("mail.notification.url", "abmhcms.abm-investama.co.id");
		sDateTime = new SimpleDateFormat("dd-MM-yyyy").format(teven.getLdate()) + " " + 
					new SimpleDateFormat("HH.mm").format(teven.getLtime());
		
		MAIL_HREF = MAIL_HREF.concat("time_events/create_time_events?");
		MAIL_HREF = MAIL_HREF.concat("ac=ACTION_CODE");
		MAIL_HREF = MAIL_HREF.concat("&ssn=").concat(ssn);
		MAIL_HREF = MAIL_HREF.concat("&ldate=").concat(new SimpleDateFormat("yyyy-MM-dd").format(teven.getLdate()));
		MAIL_HREF = MAIL_HREF.concat("&satza=").concat(teven.getSatza().getSatza());
		
		mailHeader += "Dear <b>DEAR_NAME</b><br><br>";
		mailHeader += "ACTOR_INFO for Attendance below :<br><br>";
		mailHeader += "Name			= " + employeeName + "<br>";
		mailHeader += "SN			= " + ssn + "<br>";
		mailHeader += "TEVEN_TYPE	= " + sDateTime + "<br><br>";

		// 1. BY [EMPLOYEE / HR ADMIN] - SUBMIT
		if (process == 1) {
			// TO EMPLOYEE
			if (TimeManagementConstants.TEVEN_MAIL_SUBMIT_TO_EMP.equals(action)) {
				mailContent += "has been RELEASED, Your Superior and HR Admin will review your submitted data prior to approval.<br>";
				
				ACTION_CODE	= TimeManagementConstants.LIST_SUMMARY_CODE;
				DEAR_NAME	= employeeName;
				ACTOR_INFO	= "Your request";
				MAIL_TO		= currentUser.getEmail();
			}
			// TO SUPERIOR
			else if(TimeManagementConstants.TEVEN_MAIL_SUBMIT_TO_SPV.equals(action)) {
				WeakHashMap<String, String> superiorMails = getMailToSuperior(requestorEmployee);
				mailContent += "has been released, waiting for your review and approval.<br>";
				ACTION_CODE = TimeManagementConstants.TASK_SUMMARY_CODE;
				DEAR_NAME	= (String)superiorMails.get("MAIL_RECIPIENT");
				ACTOR_INFO	= "User request";
				MAIL_TO 	= (String)superiorMails.get("MAIL_TO");
			}
		}
		// 2. BY SUPERIOR - APPROVAL
		else if (trxCode.contains("S")) {
			mailContent += "has been <strong>Approved</strong> by Superior[" + (process-1) + "] ";
			mailContent += "(" + currentUser.getEmployee().getEname() + ") ";
			
			// BY SUPERIOR
			if (trxCode.contains("S1")) {
				mailContent += "and waiting for next approval Superior<br>";
				
				// TO EMPLOYEE
				if (TimeManagementConstants.TEVEN_MAIL_SPV_TO_EMP.equals(action)) {
					ACTION_CODE = TimeManagementConstants.LIST_SUMMARY_CODE;
					DEAR_NAME 	= employeeName;
					ACTOR_INFO	= "Your request";
					MAIL_TO		= requestorEmployee.getEmail();
				}
				// TO HR ADMIN
				/*
				else if (TimeManagementConstants.TEVEN_MAIL_SPV_TO_HR.equals(action)) {
					ACTION_CODE = TimeManagementConstants.TASK_SUMMARY_CODE;
					DEAR_NAME	= "HR Admin";
					ACTOR_INFO	= "User request";
					MAIL_TO 	= getMailToHRAdmin(requestorEmployee);
				}
				*/
			}
			// BY LAST SUPERIOR
			else if (trxCode.contains("S2")) {
				mailContent += "and waiting for next approval HR Admin<br>";
				
				// TO EMPLOYEE
				if (TimeManagementConstants.TEVEN_MAIL_SPV_TO_EMP.equals(action)) {
					ACTION_CODE = TimeManagementConstants.LIST_SUMMARY_CODE;
					DEAR_NAME	= employeeName;
					ACTOR_INFO	= "Your request";
					MAIL_TO 	= requestorEmployee.getEmail();
				}
				// TO HR ADMIN
				else if (TimeManagementConstants.TEVEN_MAIL_SPV_TO_HR.equals(action)) { 
					ACTION_CODE = TimeManagementConstants.TASK_SUMMARY_CODE;
					DEAR_NAME 	= "HR Admin";
					ACTOR_INFO	= "User request";
					MAIL_TO 	= getMailToHRAdmin(requestorEmployee); // All email HR ADMIN
				}
			}
			
		}
		// 3. BY [SUPERIOR / HR ADMIN] - LAST APPROVAL
		else if (trxCode.contains("F")) {
		    // SUPERIOR - APPROVED
			if (trxCode.contains("F1")) {
				mailContent += "has been APPROVED by your Superior and PUBLISH status<br>";
			}
			// SUPERIOR - REJECTED
			else if (trxCode.contains("F2")) {
				mailContent += "has been REJECTED by your Superior due to following REASON:<br>";
				mailContent += teven.getApprovalNote().replace(System.lineSeparator(), "<br>") + "<br>";
			}
			// HR ADMIN - APPROVED
			else if (trxCode.contains("F3")) {
				mailContent += "has been APPROVED by your HR Admin and PUBLISH status<br>";
			}
			// HR ADMIN - REJECTED
			else if (trxCode.contains("F4")) {
				mailContent += "has been REJECTED by your HR Admin due to following REASON:<br>";
				mailContent += teven.getApprovalNote().replace(System.lineSeparator(), "<br>") + "<br>";
			}
			
			// TO EMPLOYEE
			ACTION_CODE = TimeManagementConstants.LIST_SUMMARY_CODE;
			DEAR_NAME 	= employeeName;
			ACTOR_INFO	= "Your request";
			MAIL_TO 	= requestorEmployee.getEmail();
		}
		mailFooter += "To view this request please click this <a href='MAIL_HREF'>Show Request</a>";
		
		MAIL_SUBJECT = MAIL_SUBJECT.replace("TEVEN_TRX", (satza.equals("P10") ? "Clock In [" : "Clock Out [") + sDateTime) + "]";
		mailHeader	= mailHeader.replace("TEVEN_TYPE", satza.equals("P10") ? "<strong>Clock In</strong>" : "<strong>Clock Out</strong>");
		mailHeader 	= mailHeader.replace("DEAR_NAME", DEAR_NAME);
		mailHeader	= mailHeader.replace("ACTOR_INFO", ACTOR_INFO);
		mailFooter	= mailFooter.replace("MAIL_HREF", MAIL_HREF);
		mailFooter	= mailFooter.replace("ACTION_CODE", ACTION_CODE);
		
		MAIL_BODY = mailHeader + mailContent + mailFooter;
		
		mailService.sendMail(MAIL_TO, MAIL_SUBJECT + " : ", MAIL_BODY);
	}

	public WeakHashMap<String, String> getMailToSuperior(User userEmployee) {
		WeakHashMap<String, String> superiorMails = new WeakHashMap();
		String MAIL_RECIPIENT = "";
		String MAIL_TO = "";
		List<PositionRelation> empPositionRelations = commonServiceFactory.getPositionRelationQueryService()
				.findByFromPositionid(userEmployee.getEmployee().getPosition().getId()).stream().collect(Collectors.toList());
		for (PositionRelation positionRelation : empPositionRelations) {
			Collection<IT0001> it0001 = commonServiceFactory.getIT0001QueryService().findByPositionId(positionRelation.getToPosition().getId());
			List<IT0001> list = new ArrayList<IT0001>(it0001);
			Optional<User> emp = userQueryService.findByPernr(list.get(0).getPernr());
			
			if (!MAIL_TO.isEmpty()) {
				MAIL_TO += ", ";
				MAIL_RECIPIENT += ", ";
			}
			MAIL_TO += emp.get().getEmail(); //collection all email superior
			MAIL_RECIPIENT += emp.get().getEmployee().getEname();
		}
		
		superiorMails.put("MAIL_RECIPIENT", MAIL_RECIPIENT); // Dear Name(Superior Name)
		superiorMails.put("MAIL_TO", MAIL_TO); 				 // Email Name
		
		return superiorMails;
	}

	public String getMailToHRAdmin(User userEmployee) {
		String MAIL_TO = "";
		List<User> adminUsers = new ArrayList<User>();
	    Optional<Collection<User>> users = userQueryService.fetchAdminByCompany(
	    		userEmployee.getEmployee().getT500p().getBukrs().getBukrs());
	    if (users.isPresent()) {
	    	adminUsers = users.get().stream().collect(Collectors.toList());
	    	for (User adminUser : adminUsers) {
	    		if (!MAIL_TO.isEmpty()) MAIL_TO += ", ";
				MAIL_TO += adminUser.getEmail(); //collection all email HR ADMIN
	    	}
	    }
		return MAIL_TO;
	}

	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody /*@Validated*/ TEVENRequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> tevenEntity = hcmsEntityQueryService.findByEntityName(TEVEN.class.getSimpleName());
			if (!tevenEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + TEVEN.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<TEVEN, TEVENRequestWrapper, String> newObjectContainer 
					= tevenRequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				Optional<User> emp = userQueryService.findByPernr(request.getPernr());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				
				Optional<TEVEN> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
					Date thisDate = new Date();
					//String thisTime = new SimpleDateFormat(TimeManagementConstants.FORMAT_HH24_MM).format(thisDate);
					
					newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					newObjectContainer.getEntity().get().setErdat(thisDate);
					newObjectContainer.getEntity().get().setErtim(thisDate);
					newObjectContainer.getEntity().get().setProcess("0");
					newObjectContainer.getEntity().get().setTrxcode("MC");
					//status transaction = sendTo
					if (request.getTrxcode().contains("S")) {
						newObjectContainer.getEntity().get().setProcess("1");
						newObjectContainer.getEntity().get().setTrxcode("MS1");
						newObjectContainer.getEntity().get().setSubmittedBy(currentUser.get());
						newObjectContainer.getEntity().get().setSubmittedAt(thisDate);
						
						// SEND MAIL
						tevenSendMail(currentUser.get(), emp.get(), newObjectContainer.getEntity().get());
					}
					savedEntity = tevenCommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create TEVEN (Bank Details) data. Please check your data!");
				}
				
				// SAVE TO EVENT LOG
				tevenSaveToEventLog(savedEntity, OperationType.CREATE, newObjectContainer, currentUser);
				
				/*
				StringBuilder key = new StringBuilder("tevenid=" + savedEntity.get().getTevenid());
				EventLog eventLog = new EventLog(tevenEntity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				eventLog.setStatus("process=" + savedEntity.get().getProcess() + ",trxcode=" + savedEntity.get().getTrxcode());
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				*/
				
				TEVENResponseWrapper responseWrapper = new TEVENResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(TEVENController.class).create(request, result)).withSelfRel());		
		return response;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/deltev/{ssn}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<TevenvwResponseWrapper>> deleteItem(
			@PathVariable String ssn,
			@RequestParam(value = "ldate", required = false) String ldate,
			@RequestParam(value = "satza", required = false) String satza) throws Exception {
		ArrayData<TevenvwResponseWrapper> bunchOfTevenvw = new ArrayData<>();
		APIResponseWrapper<ArrayData<TevenvwResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfTevenvw);
		
		Long sn = ssn != null && !ssn.isEmpty() ? new Long(ssn) : null; 
		Optional<User> currentUser = userQueryService.findByUsername(currentUser());
		if(currentUser == null) {
			response.setMessage("Unauthorized User Login");
			response.add(linkTo(methodOn(TEVENController.class).deleteItem(ssn, ldate, satza)).withSelfRel());
			return response;
		}
		
		//Not RULE HR ADMIN, Can not delete other SSN.
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(TimeManagementConstants.ROLE_ADMIN));
		if (!currentUser.get().getRoles().contains(adminRole.get())
				&& !currentUser.get().getEmployee().getPernr().equals(sn)) {
			response.setMessage("Unauthorized delete ssn: " + sn);
			response.add(linkTo(methodOn(TEVENController.class).deleteItem(ssn, ldate, satza)).withSelfRel());
			return response;
		}
		//System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx user name: " + currentUser.get().getUsername());

		// Validation user request
		Optional<T705P> t705p = t705pQueryService.findBySatza(satza);
		if(!t705p.isPresent()) {
			response.setMessage("Time Event Type not found");
			response.add(linkTo(methodOn(TEVENController.class).deleteItem(ssn, ldate, satza)).withSelfRel());
			return response;
		}
		// Validation time event type
		Optional<TEVEN> teven = tevenQueryService.findOneByCompositeKeys(sn,
				CommonDateFunction.convertDateRequestParameterIntoDate(ldate), t705p.get());
		if (!teven.isPresent()) {
			response.setMessage("Cannot update data. No Data Found!");
			response.add(linkTo(methodOn(TEVENController.class).deleteItem(ssn, ldate, satza)).withSelfRel());
			return response;
		}
		
		int resultValue = tevenQueryService.deleteItem(teven.get());
		if (resultValue != 1) {
			response.setMessage("Delete ssn: " + ssn + " is fail!");
			response.add(linkTo(methodOn(TEVENController.class).deleteItem(ssn, ldate, satza)).withSelfRel());
			return response;
		}
		
		// SAVE TO EVENT LOG
		StringBuilder key = new StringBuilder("tevenid=" + teven.get().getTevenid());
		Optional<HCMSEntity> tevenEntity = hcmsEntityQueryService.findByEntityName(TEVEN.class.getSimpleName());
		if (!tevenEntity.isPresent()) {
			throw new EntityNotFoundException("Cannot find Entity '" + TEVEN.class.getSimpleName() + "' !!!");
		}
		JSONObject json = new JSONObject(teven.get());
		EventLog eventLog = new EventLog(tevenEntity.get(), key.toString(), OperationType.DELETE, json.toString());
		Optional<EventLog> deletedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
		if (!deletedEventLog.isPresent()) {
			throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
		}
		
		System.out.println(response.getMessage() + " delete item [teven id]: " + teven.get().getTevenid());
		
		response.add(linkTo(methodOn(TEVENController.class).deleteItem(ssn, ldate, satza)).withSelfRel());
		return response;
	}
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}
}