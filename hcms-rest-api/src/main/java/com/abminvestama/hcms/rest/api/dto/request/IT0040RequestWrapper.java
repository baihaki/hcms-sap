package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class IT0040RequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = -5671054070195220662L;
	
	private String itxex;
	private String leihg;
	private double anzkl;
	private String zeinh;
	private String lobnr;
	
	/**
	 * GET Text Exists. 
	 * 
	 * @return
	 */
	@JsonProperty("text_exists")
	public String getItxex() {
		return itxex;
	}
	
	/**
	 * GET Object on Loan (object code)
	 * @return
	 */
	@JsonProperty("object_on_loan")
	public String getLeihg() {
		return leihg;
	}
	
	/**
	 * GET Number/Unit.
	 * 
	 * @return
	 */
	@JsonProperty("number_per_unit")
	public double getAnzkl() {
		return anzkl;
	}
	
	/**
	 * GET Unit of time/meas.
	 * @return
	 */
	@JsonProperty("unit_of_time")
	public String getZeinh() {
		return zeinh;
	}
	
	/**
	 * GET Loan Object Number.
	 * 
	 * @return
	 */
	@JsonProperty("loan_object_number")
	public String getLobnr() {
		return lobnr;
	}
}