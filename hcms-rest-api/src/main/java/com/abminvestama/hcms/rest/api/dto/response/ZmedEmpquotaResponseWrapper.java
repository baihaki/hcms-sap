package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class ZmedEmpquotaResponseWrapper extends ResourceSupport  {

	private Date datab;
	private Date datbi;
	
	private String bukrs;
	private String persg;
	private String persk;
	private String fatxt;
	private String kodequo;
	private String exten;
	private String quotamt;
	
	private ZmedEmpquotaResponseWrapper() {}
	
	public ZmedEmpquotaResponseWrapper(ZmedEmpquota zmedEmpquota) {
		if (zmedEmpquota == null) {
			new ZmedEmpquotaResponseWrapper();
		} else {
			this
			//.setBukrs(zmedEmpquota.getBukrs() != null ? StringUtils.defaultString(zmedEmpquota.getBukrs().getBukrs(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setDatab(zmedEmpquota.getDatab())
			.setDatbi(zmedEmpquota.getDatbi())
			.setBukrs(StringUtils.defaultString(zmedEmpquota.getBukrs().getBukrs(), StringUtils.EMPTY))
			.setPersk(StringUtils.defaultString(zmedEmpquota.getPersk(), StringUtils.EMPTY))
			.setPersg(StringUtils.defaultString(zmedEmpquota.getPersg(), StringUtils.EMPTY))
			.setFatxt(StringUtils.defaultString(zmedEmpquota.getFatxt(), StringUtils.EMPTY))
			.setKodequo(StringUtils.defaultString(zmedEmpquota.getKodequo(), StringUtils.EMPTY))
			.setExten(StringUtils.defaultString(zmedEmpquota.getExten().toString(), StringUtils.EMPTY))
			.setQuotamt(StringUtils.defaultString(zmedEmpquota.getQuotamt().toString(), StringUtils.EMPTY));			
		}
	}
	
	

	@JsonProperty("company_code")
	public String getBukrs() {
		return bukrs;
	}
	
	private ZmedEmpquotaResponseWrapper setBukrs(String bukrs) {
		this.bukrs = bukrs;
		return this;
	}
	


	
	
	@JsonProperty("fatxt")
	public String getFatxt() {
		return fatxt;
	}
	
	private ZmedEmpquotaResponseWrapper setFatxt(String fatxt) {
		this.fatxt = fatxt;
		return this;
	}
	


	
	
	@JsonProperty("quota_type")
	public String getKodequo() {
		return kodequo;
	}
	
	private ZmedEmpquotaResponseWrapper setKodequo(String kodequo) {
		this.kodequo = kodequo;
		return this;
	}
	
	
	@JsonProperty("exten")
	public String getExten() {
		return exten;
	}
	
	private ZmedEmpquotaResponseWrapper setExten(String exten) {
		this.exten = exten;
		return this;
	}
	
	
	@JsonProperty("quotamt")
	public String getQuotamt() {
		return quotamt;
	}
	
	private ZmedEmpquotaResponseWrapper setQuotamt(String quotamt) {
		this.quotamt = quotamt;
		return this;
	}
	
	
	@JsonProperty("persg")
	public String getPersg() {
		return persg;
	}
	
	private ZmedEmpquotaResponseWrapper setPersg(String persg) {
		this.persg = persg;
		return this;
	}
	
	
	@JsonProperty("persk")
	public String getPersk() {
		return persk;
	}
	
	private ZmedEmpquotaResponseWrapper setPersk(String persk) {
		this.persk = persk;
		return this;
	}

	
	
	@JsonProperty("valid_from_date")
	public Date getDatab() {
		return datab;
	}
	
	private ZmedEmpquotaResponseWrapper setDatab(Date datab) {
		this.datab = datab;
		return this;
	}
	


	
	
	@JsonProperty("valid_to_date")
	public Date getDatbi() {
		return datbi;
	}
	
	private ZmedEmpquotaResponseWrapper setDatbi(Date datbi) {
		this.datbi = datbi;
		return this;
	}

}