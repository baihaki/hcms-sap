package com.abminvestama.hcms.rest.api.dto.response;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.SubT706B1;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class SubT706B1ResponseWrapper extends ResourceSupport {

	private String morei;
	private String spkzl;
	private String subSpkzl;
	private String subSptxt;
	private String subForm;

	private String sptxt;
	
	SubT706B1ResponseWrapper() {}
	
	public SubT706B1ResponseWrapper(SubT706B1 subt706b1) {
		this.morei = subt706b1.getId().getMorei();
		this.spkzl = subt706b1.getId().getSpkzl();
		this.subSpkzl = subt706b1.getId().getSubSpkzl();
		this.subSptxt = subt706b1.getSubSptxt();
		this.subForm = String.valueOf(subt706b1.getSubForm());
		this.sptxt = subt706b1.getSht706b1() != null ? StringUtils.defaultString(subt706b1.getSht706b1().getSptxt()) : StringUtils.EMPTY; 
	}
	
	@JsonProperty("morei")
	public String getMorei() {
		return morei;
	}
	
	@JsonProperty("expense_type")
	public String getSpkzl() {
		return spkzl;
	}
	
	@JsonProperty("expense_type_text")
	public String getSptxt() {
		return sptxt;
	}
	
	@JsonProperty("sub_expense_type")
	public String getSubSpkzl() {
		return subSpkzl;
	}
	
	@JsonProperty("sub_expense_type_text")
	public String getSubSptxt() {
		return subSptxt;
	}
	
	@JsonProperty("sub_form")
	public String getSubForm() {
		return subForm;
	}
}