package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public class CaReqHdrRequestWrapper extends ITRequestWrapper {
	
	private static final long serialVersionUID = 2646176092395282191L;
	
	private String reqNo;
	private Double amount;
	private String currency;
	private String description;
	private String attachment;
	private Integer process;
	private String bankName;
	private String bankAccount;
	private String notes;
	private String advanceCode;
	private String bukrs;
	private String acctAp;
	private String reason;

	private String attach1Content;
	private String attach1Filename;
	
	@JsonProperty("req_no")
	public String getReqNo() {
		return reqNo;
	}
	
	@JsonProperty("amount")
	public Double getAmount() {
		return amount;
	}
	
	@JsonProperty("currency")
	public String getCurrency() {
		return currency;
	}
	
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}
	
	@JsonProperty("attachment")
	public String getAttachment() {
		return attachment;
	}
	
	@JsonProperty("process")
	public Integer getProcess() {
		return process;
	}

	@JsonProperty("bank_name")
	public String getBankName() {
		return bankName;
	}
	
	@JsonProperty("bank_account")
	public String getBankAccount() {
		return bankAccount;
	}
	
	@JsonProperty("notes")
	public String getNotes() {
		return notes;
	}
	
	@JsonProperty("advance_code")
	public String getAdvanceCode() {
		return advanceCode;
	}
	
	@JsonProperty("bukrs")
	public String getBukrs() {
		return bukrs;
	}
	
	@JsonProperty("acct_ap")
	public String getAcctAp() {
		return acctAp;
	}
	
	@JsonProperty("reason")
	public String getReason() {
		return reason;
	}
	
	@JsonProperty("attachment1_base64")
	public String getAttach1Content() {
		return attach1Content;
	}
	
	@JsonProperty("attachment1_filename")
	public String getAttach1Filename() {
		return attach1Filename;
	}
}