package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.Collection;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotastfQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.ExpenseQuotaResponseWrapper;

/**
 * 
 * @since 2.0.0
 * @version 2.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v2/expense_quotas")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ExpenseQuotaController extends AbstractResource {


	private ZmedEmpquotastfQueryService zmedEmpquotastfQueryService;
	private UserQueryService userQueryService;
		
	@Autowired
	void setZmedEmpquotastfQueryService(ZmedEmpquotastfQueryService zmedEmpquotastfQueryService) {
		this.zmedEmpquotastfQueryService = zmedEmpquotastfQueryService;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_company/{bukrs}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<ExpenseQuotaResponseWrapper>> findByBukrs(
			@PathVariable String bukrs,
			@RequestParam(value = "persg", required = false, defaultValue = "ALL") String persg,
			@RequestParam(value = "persk", required = false, defaultValue = "ALL") String persk,
			@RequestParam(value = "date", required = false, defaultValue = "NOW") String date) throws Exception {
			
			ArrayData<ExpenseQuotaResponseWrapper> bunchOfExpenseQuota = new ArrayData<>();
			APIResponseWrapper<ArrayData<ExpenseQuotaResponseWrapper>> response = new APIResponseWrapper<>();
			response.setData(bunchOfExpenseQuota);
			
			Date searchDate = new Date();
			
			try {
				
				if (!"NOW".equals(date)) {
					searchDate = CommonDateFunction.convertDateRequestParameterIntoDate(date);
				}
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				if (currentUser.isPresent()) {
					if ("ALL".equals(persg))
						persg = currentUser.get().getEmployee().getT501t().getPersg();
					if ("ALL".equals(persk))
						persk = currentUser.get().getEmployee().getT503k().getMandt();
				}
				
				List<ZmedEmpquotastf> listOfExpenseQuota = new ArrayList<>();
				Optional<Collection<ZmedEmpquotastf>> collectionOfExpenseQuota =
						zmedEmpquotastfQueryService.findExpenseQuotasByBukrs(bukrs, persg, persk, searchDate);
				
				if (!collectionOfExpenseQuota.isPresent() || collectionOfExpenseQuota.get().isEmpty()) {
					persg = "ALL";
					persk = "ALL";
					collectionOfExpenseQuota =
							zmedEmpquotastfQueryService.findExpenseQuotasByBukrs(bukrs, persg, persk, searchDate);
				}
				

				if (collectionOfExpenseQuota.isPresent() && collectionOfExpenseQuota.get().size() > 0) {
					listOfExpenseQuota = collectionOfExpenseQuota.get().stream().collect(Collectors.toList());
					List<ExpenseQuotaResponseWrapper> records = new ArrayList<>();
					for (ZmedEmpquotastf expenseQuota : listOfExpenseQuota) {
						records.add(new ExpenseQuotaResponseWrapper(expenseQuota));
					}
					bunchOfExpenseQuota.setItems(records.toArray(new ExpenseQuotaResponseWrapper[records.size()]));
				} else {
					response.setMessage("No Data Found");
				}

			} catch (Exception e) {
				throw e;
			}

		response.add(linkTo(methodOn(ExpenseQuotaController.class).findByBukrs(bukrs, persg, persk, date)).withSelfRel());
		return response;
	}
	
	

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}

}
