package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.entity.T517T;
import com.abminvestama.hcms.core.model.entity.T517X;
import com.abminvestama.hcms.core.model.entity.T517Z;
import com.abminvestama.hcms.core.service.api.business.query.T517TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T517XQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T517ZQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add method: findBySchoolType</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/study_branches")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class T517XController extends AbstractResource {

	private T517XQueryService t517xQueryService;
	private T517TQueryService t517tQueryService;
	private T517ZQueryService t517zQueryService;
	
	@Autowired
	void setT517XQueryService(T517XQueryService t517xQueryService) {
		this.t517xQueryService = t517xQueryService;
	}
	
	@Autowired
	void setT517TQueryService(T517TQueryService t517tQueryService) {
		this.t517tQueryService = t517tQueryService;
	}
	
	@Autowired
	void setT517ZQueryService(T517ZQueryService t517zQueryService) {
		this.t517zQueryService = t517zQueryService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/page/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<T517X>> fetchPage(@PathVariable int pageNumber, 
			@RequestParam(required = true, value = "username", defaultValue = "") String username,
			@RequestParam(required = true, value = "auth_token", defaultValue = "") String authToken) throws Exception {
		
		if (authToken == null || authToken.isEmpty()) {
			throw new Exception("No token provided");
		}
		
		Page<T517X> resultPage = t517xQueryService.findAllWithPaging(pageNumber);
		
		APIResponseWrapper<Page<T517X>> response = new APIResponseWrapper<>();
		response.setData(resultPage);
		Link self = linkTo(methodOn(T517XController.class).fetchPage(pageNumber, username, authToken)).withSelfRel();
		response.add(self);
		
		return response;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/school_type/{slart}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<T517X>> findBySchoolType(@PathVariable String slart) throws Exception {
		ArrayData<T517X> bunchOfT517X = new ArrayData<>();
		APIResponseWrapper<ArrayData<T517X>> response = new APIResponseWrapper<>();
		response.setData(bunchOfT517X);
		
		try {
			List<T517Z> listOfT517Z = new ArrayList<>();
			
			Optional<T517T> t517t = t517tQueryService.findById(Optional.ofNullable(slart));
			if (t517t.isPresent()) {
				listOfT517Z = t517zQueryService.findBySlart(t517t.get()).stream().collect(Collectors.toList());
			}
			
			if (listOfT517Z.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<T517X> records = new ArrayList<>();
				for (T517Z t517z : listOfT517Z) {
					records.add(t517z.getFaart());
				}
				bunchOfT517X.setItems(records.toArray(new T517X[records.size()])); 
			}
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(T517XController.class).findBySchoolType(slart)).withSelfRel());
		return response;
	}
}