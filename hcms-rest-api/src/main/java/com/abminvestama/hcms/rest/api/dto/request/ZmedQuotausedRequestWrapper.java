package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ZmedQuotausedRequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = -2317050437190237970L;
	
	private String bukrs;
	private int gjahr;
	private long pernr;
	private String kodequo;
	private String quotamt;
	
	@JsonProperty("company_code")
	public String getBukrs() {
		return bukrs;
	}
	
	
	@JsonProperty("fiscal_year")
	public int getGjahr() {
		return gjahr;
	}
	
	
	@JsonProperty("pernr")
	public long getPernr() {
		return pernr;
	}
	
	
	@JsonProperty("quota_type")
	public String getKodequo() {
		return kodequo;
	}
	
	
	@JsonProperty("wage_type_amount")
	public String getQuotamt() {
		return quotamt;
	}
}