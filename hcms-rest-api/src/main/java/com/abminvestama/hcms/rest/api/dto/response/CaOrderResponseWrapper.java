package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.CaOrder;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class CaOrderResponseWrapper extends ResourceSupport {

	private CaOrderResponseWrapper() {}
	
	public CaOrderResponseWrapper(CaOrder caOrder) {
		if (caOrder == null) {
			new CaOrderResponseWrapper();
		}
		else {
			// DEPARTEMENT COST CENTER (NOT ORGEH)
			if (caOrder.getCskt() != null) {
				
			}
			//Long orgeh = 0L;
			//String orgtx = "";
			//if (caOrder.getCskt() != null && caOrder.getDeptsCostCenter().getT527x() != null) {
			//	orgeh = caOrder.getDeptsCostCenter().getT527x().getOrgeh();
			//	orgtx = caOrder.getDeptsCostCenter().getT527x().getOrgtx();
			//}
			
			// GL ACCOUNT
			Long glAccountId = 0L;
			String glAccountText = "";
			if (caOrder.getGlAccount() != null) {
				glAccountId = caOrder.getGlAccount().getId();
				glAccountText = caOrder.getGlAccount().getGlAcctText();
			}
			
			this
			.setOrderId(caOrder.getId())
			.setDescription(defaultString(caOrder.getDescription()))
			//.setOrgeh(orgeh)
			//.setOrgtx(defaultString(orgtx))
			.setKostl(caOrder.getCskt() != null ? defaultString(caOrder.getCskt().getKostl()) : "")
			.setKltxt(caOrder.getCskt() != null ? defaultString(caOrder.getCskt().getKltxt()) : "")
			.setGlAccountId(glAccountId)
			.setGlAccountText(glAccountText)
			.setBegda(dateToStringYMD(caOrder.getBegda()))
			.setEndda(dateToStringYMD(caOrder.getEndda()))
			;
		}
	}
	
	private Long orderId;
	private String description;
	//private Long orgeh;
	//private String orgtx;
	private String kostl;
	private String kltxt;
	private Long glAccountId;
	private String glAccountText;
	private String begda;
	private String endda;
	
	@JsonProperty("id")
	public Long getOrderId() {
		return orderId;
	}

	public CaOrderResponseWrapper setOrderId(Long orderId) {
		this.orderId = orderId;
		return this;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	public CaOrderResponseWrapper setDescription(String description) {
		this.description = description;
		return this;
	}

//	@JsonProperty("orgeh")
//	public Long getOrgeh() {
//		return orgeh;
//	}
//
//	public CaOrderResponseWrapper setOrgeh(Long orgeh) {
//		this.orgeh = orgeh;
//		return this;
//	}
//
//	@JsonProperty("orgtx")
//	public String getOrgtx() {
//		return orgtx;
//	}
//
//	public CaOrderResponseWrapper setOrgtx(String orgtx) {
//		this.orgtx = orgtx;
//		return this;
//	}

	@JsonProperty("kostl")
	public String getKostl() {
		return kostl;
	}

	public CaOrderResponseWrapper setKostl(String kostl) {
		this.kostl = kostl;
		return this;
	}

	@JsonProperty("kltxt")
	public String getKltxt() {
		return kltxt;
	}

	public CaOrderResponseWrapper setKltxt(String kltxt) {
		this.kltxt = kltxt;
		return this;
	}

	@JsonProperty("gl_account_id")
	public Long getGlAccountId() {
		return glAccountId;
	}

	public CaOrderResponseWrapper setGlAccountId(Long glAccountId) {
		this.glAccountId = glAccountId;
		return this;
	}

	@JsonProperty("gl_account_text")
	public String getGlAccountText() {
		return glAccountText;
	}

	public CaOrderResponseWrapper setGlAccountText(String glAccountText) {
		this.glAccountText = glAccountText;
		return this;
	}

	@JsonProperty("begda")
	public String getBegda() {
		return begda;
	}

	public CaOrderResponseWrapper setBegda(String begda) {
		this.begda = begda;
		return this;
	}

	@JsonProperty("endda")
	public String getEndda() {
		return endda;
	}

	public CaOrderResponseWrapper setEndda(String endda) {
		this.endda = endda;
		return this;
	}

	private String dateToStringYMD(Date date) {
		return date != null ? CommonDateFunction.convertDateToStringYMD(date) : "";
	}

	private String defaultString(String str) {
		return str != null ? str : "";
	}
}
