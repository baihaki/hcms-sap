package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0077;
import com.abminvestama.hcms.core.model.entity.IT0077;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0077CommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0077QueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.helper.IT0077SoapObject;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0077RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.IT0077ResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.IT0077RequestBuilderUtil;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@RestController
@RequestMapping("/api/v1/additional_personal_data")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class IT0077Controller extends AbstractResource {

	private IT0077CommandService it0077CommandService;
	private IT0077QueryService it0077QueryService;
	private IT0077RequestBuilderUtil it0077RequestBuilderUtil;
	

	private HCMSEntityQueryService hcmsEntityQueryService;
	private EventLogCommandService eventLogCommandService;
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;

	private MuleConsumerService muleConsumerService;
	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}

	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	
	@Autowired
	void setIT0077CommandService(IT0077CommandService it0077CommandService) {
		this.it0077CommandService = it0077CommandService;
	}
	
	@Autowired
	void setIT0077QueryService(IT0077QueryService it0077QueryService) {
		this.it0077QueryService = it0077QueryService;
	}
	
	@Autowired
	void setIT0077RequestBuilderUtil(IT0077RequestBuilderUtil it0077RequestBuilderUtil) {
		this.it0077RequestBuilderUtil = it0077RequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0077ResponseWrapper>> findOneByPernr(@PathVariable String pernr) throws Exception {
		ArrayData<IT0077ResponseWrapper> bunchOfIT0077 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0077ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0077);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0077> listOfIT0077 = it0077QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			
			if (listOfIT0077.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0077ResponseWrapper> records = new ArrayList<>();
				for (IT0077 it0077 : listOfIT0077) {
					records.add(new IT0077ResponseWrapper(it0077));
				}
				bunchOfIT0077.setItems(records.toArray(new IT0077ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0077Controller.class).findOneByPernr(pernr)).withSelfRel());
		return response;
	}	
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated IT0077RequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			IT0077ResponseWrapper responseWrapper = new IT0077ResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					

					Optional<HCMSEntity> it0077Entity = hcmsEntityQueryService.findByEntityName(IT0077.class.getSimpleName());
					if (!it0077Entity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + IT0077.class.getSimpleName() + "' !!!");
					}					
					
					
					Optional<IT0077> it0077 = it0077QueryService.findOneByCompositeKey(request.getPernr(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
					if (!it0077.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					RequestObjectComparatorContainer<IT0077, IT0077RequestWrapper, String> updatedContainer = it0077RequestBuilderUtil.compareAndReturnUpdatedData(request, it0077.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
					if (updatedContainer.getEntity().isPresent()) {
						updatedContainer.getEntity().get().setSeqnr(it0077.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
						it0077 = it0077CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}
				
					if (!it0077.isPresent()) {
						throw new CannotPersistException("Cannot update IT0077 (Additional Personal Data) data. Please check your data!");
					}
					
					

					StringBuilder key = new StringBuilder("pernr=" + it0077.get().getId().getPernr());
					//key.append(",infty=" + it0077.get().getId().getInfty());
					//key.append(",subty=" + it0077.get().getId().getSubty());
					key.append(",endda=" + it0077.get().getId().getEndda());
					key.append(",begda=" + it0077.get().getId().getBegda());
					EventLog eventLog = new EventLog(it0077Entity.get(), key.toString(), OperationType.UPDATE, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					
					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}					

					//IT0077SoapObject it0077SoapObject = new IT0077SoapObject(savedEntity.get()); 
					//muleConsumerService.postToSAP(BAPIFunction.IT0077_CREATE.service(), "additionalPersonalData", it0077SoapObject);
					
					responseWrapper = new IT0077ResponseWrapper(it0077.get());
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(IT0077Controller.class).edit(request, result)).withSelfRel());
		return response;
	}		
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(itValidator);
	}		
}