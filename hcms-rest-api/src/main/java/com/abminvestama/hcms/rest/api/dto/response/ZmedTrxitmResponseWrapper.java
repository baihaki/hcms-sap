package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class ZmedTrxitmResponseWrapper  extends ResourceSupport {

	
	private Date billdt;
	
	private String bukrs;
	private String clmno;
	private String itmno;
	private String kodemed;
	private String kodecos;
	private String zcostyp;
	private String clmamt;
	private String rejamt;
	private String appamt;
	private String relat;
	private String umur;
	private String pasien;

	private ZmedTrxitmResponseWrapper() {}
	
	
	public ZmedTrxitmResponseWrapper(ZmedTrxitm zmedTrxitm) {
		if (zmedTrxitm == null) {
			new ZmedTrxitmResponseWrapper();
		} else {
			this
			//.setBukrs(zmedTrxitm.getBukrs() != null ? StringUtils.defaultString(zmedTrxitm.getBukrs().getBukrs(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setBilldt(zmedTrxitm.getBilldt())
			.setBukrs(zmedTrxitm.getBukrs() != null ? StringUtils.defaultString(zmedTrxitm.getBukrs().getBukrs(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setClmno(StringUtils.defaultString(zmedTrxitm.getClmno(), StringUtils.EMPTY))
			.setItmno(StringUtils.defaultString(zmedTrxitm.getItmno(), StringUtils.EMPTY))
			.setKodemed(StringUtils.defaultString(zmedTrxitm.getKodemed(), StringUtils.EMPTY))
			.setKodecos(StringUtils.defaultString(zmedTrxitm.getKodecos(), StringUtils.EMPTY))
			.setZcostyp(StringUtils.defaultString(zmedTrxitm.getZcostyp(), StringUtils.EMPTY))
			.setClmamt(StringUtils.defaultString(zmedTrxitm.getClmamt().toString(), StringUtils.EMPTY))
			.setRejamt(StringUtils.defaultString(zmedTrxitm.getRejamt().toString(), StringUtils.EMPTY))
			.setAppamt(StringUtils.defaultString(zmedTrxitm.getAppamt().toString(), StringUtils.EMPTY))
			.setRelat(StringUtils.defaultString(zmedTrxitm.getRelat(), StringUtils.EMPTY))
			.setUmur(StringUtils.defaultString(zmedTrxitm.getUmur(), StringUtils.EMPTY))
			.setPasien(StringUtils.defaultString(zmedTrxitm.getPasien(), StringUtils.EMPTY));			
		}
	}
	


	
	
	@JsonProperty("company_code")
	public String getBukrs() {
		return bukrs;
	}
	
	private ZmedTrxitmResponseWrapper setBukrs(String bukrs) {
		this.bukrs = bukrs;
		return this;
	}
	


	
	
	@JsonProperty("clmno")
	public String getClmno() {
		return clmno;
	}
	
	private ZmedTrxitmResponseWrapper setClmno(String clmno) {
		this.clmno = clmno;
		return this;
	}
	


	
	
	@JsonProperty("pasien")
	public String getPasien() {
		return pasien;
	}
	
	private ZmedTrxitmResponseWrapper setPasien(String pasien) {
		this.pasien = pasien;
		return this;
	}
	
	
	@JsonProperty("umur")
	public String getUmur() {
		return umur;
	}
	
	private ZmedTrxitmResponseWrapper setUmur(String umur) {
		this.umur = umur;
		return this;
	}
	
	
	
	@JsonProperty("relat")
	public String getRelat() {
		return relat;
	}
	
	private ZmedTrxitmResponseWrapper setRelat(String relat) {
		this.relat = relat;
		return this;
	}
	
	
	
	@JsonProperty("itmno")
	public String getItmno() {
		return itmno;
	}
	
	private ZmedTrxitmResponseWrapper setItmno(String itmno) {
		this.itmno = itmno;
		return this;
	}
	
	
	
	@JsonProperty("kodemed")
	public String getKodemed() {
		return kodemed;
	}
	
	private ZmedTrxitmResponseWrapper setKodemed(String kodemed) {
		this.kodemed = kodemed;
		return this;
	}
	
	
	
	@JsonProperty("kodecos")
	public String getKodecos() {
		return kodecos;
	}
	
	private ZmedTrxitmResponseWrapper setKodecos(String kodecos) {
		this.kodecos = kodecos;
		return this;
	}
	


	@JsonProperty("zcostyp")
	public String getZcostyp() {
		return zcostyp;
	}
	
	private ZmedTrxitmResponseWrapper setZcostyp(String zcostyp) {
		this.zcostyp = zcostyp;
		return this;
	}
	
	
	@JsonProperty("clmamt")
	public String getClmamt() {
		return clmamt;
	}
	
	private ZmedTrxitmResponseWrapper setClmamt(String clmamt) {
		this.clmamt = clmamt;
		return this;
	}
	
	
	
	@JsonProperty("rejamt")
	public String getRejamt() {
		return rejamt;
	}
	
	private ZmedTrxitmResponseWrapper setRejamt(String rejamt) {
		this.rejamt = rejamt;
		return this;
	}
	


	@JsonProperty("appamt")
	public String getAppamt() {
		return appamt;
	}
	
	private ZmedTrxitmResponseWrapper setAppamt(String appamt) {
		this.appamt = appamt;
		return this;
	}
	
	
	
	
	@JsonProperty("bill_date")
	public Date getBilldt() {
		return billdt;
	}
	
	private ZmedTrxitmResponseWrapper setBilldt(Date billdt) {
		this.billdt = billdt;
		return this;
	}
	
}

