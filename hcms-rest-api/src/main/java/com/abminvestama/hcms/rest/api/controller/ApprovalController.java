package com.abminvestama.hcms.rest.api.controller;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.exception.WorkflowNotFoundException;
import com.abminvestama.hcms.core.model.constant.ApprovalAction;
import com.abminvestama.hcms.core.model.constant.ApprovalType;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.EntityCategory;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.constant.WorkflowType;
import com.abminvestama.hcms.core.model.entity.ApprovalCategory;
import com.abminvestama.hcms.core.model.entity.ApprovalEventLog;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.PositionRelation;
import com.abminvestama.hcms.core.model.entity.PositionRelationType;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.Workflow;
import com.abminvestama.hcms.core.model.entity.WorkflowStep;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ApprovalEventLogResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.EventDataWrapper;

/**
 * Base Controller for Approval on all Infotypes
 * 
 * @since 2.0.0
 * @version 2.0.5
 * @author baihaki (baihaki.pru@gmail.com)<br>yauri (yauritux@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.5</td><td>Baihaki</td><td>Approve: Add "_change" suffix to muleUri on UPDATE BAPI (IT0105)</td></tr>
 *     <tr><td>2.0.4</td><td>Baihaki</td><td>Approve IT0105: Any update on user's email should reflect to m_user table</td></tr>
 *     <tr><td>2.0.3</td><td>Baihaki</td><td>Implement Approve on UPDATED_DRAFT and UPDATE_IN_PROGRESS</td></tr>
 *     <tr><td>2.0.2</td><td>Baihaki</td><td>Convert methods to Reflection: getUpdatedFields (based on IT0009Controller)</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Convert methods to Reflection: approve, reject (based on IT0009Controller)</td></tr>
 *     <tr><td>2.0.0</td><td>Yauri</td><td>Development release (IT0009Controller)</td></tr>
 * </table>
 *
 */
@Component
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = { CannotPersistException.class, Exception.class })
public class ApprovalController extends AbstractResource {

	private CommonServiceFactory commonServiceFactory;
	private MuleConsumerService muleConsumerService;
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	@Autowired
	public void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}
	
	/**
	 * 
	 * Controller method for approve the reflected infotype's entity object
	 * 
	 * @param keyMap Map contains composite key of the entity object and Mulesoft service to be called
	 * @param className entity name as stored in cfg_entity
	 * @param queryService the infotype's query service
	 * @param commandService the infotype's command service
	 * @param soapObjectClass the converted Soap Object sent to Mulesoft service
	 * @param requestBuilder the infotype's request builder for Approve Update proccess
	 * @param requestWrapper the infotype's Request Wrapper Class
	 * @return the {@link ApprovalEventLogResponseWrapper} response
	 * @throws AuthorizationException
	 * @throws NumberFormatException
	 * @throws CannotPersistException
	 * @throws DataViolationException
	 * @throws EntityNotFoundException
	 * @throws WorkflowNotFoundException
	 * @throws Exception
	 */
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> approve(Map<String, String> keyMap, String className, Object queryService,
			Object commandService, Class<?> soapObjectClass, Object requestBuilder, Object requestWrapper)
					throws AuthorizationException, NumberFormatException, CannotPersistException,
					DataViolationException, EntityNotFoundException, WorkflowNotFoundException, Exception {
		
		String pernr = keyMap.get("ssn");
		String infotype = keyMap.get("infotype");
		String subtype = keyMap.get("subtype");
		String endda = keyMap.get("endda");
		String begda = keyMap.get("begda");
		
		String muleService = keyMap.get("muleService");
		String muleUri = keyMap.get("muleUri");
		
		boolean hasSubtype = (subtype != null);
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
			
			if (!currentUser.isPresent()) {
				throw new AuthorizationException(
						"Insufficient Privileges. Please login either as 'ADMIN' or 'SUPERIOR'!");
			}
			
			long ssn = Long.valueOf(pernr);
			
			if (currentUser.get().getEmployee().getPernr().longValue() == ssn) {
				throw new AuthorizationException("You can't approve your own data!");
			}
			
			Optional<HCMSEntity> hcmsEntity = commonServiceFactory.getHCMSEntityQueryService().findByEntityName(className);
			if (!hcmsEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + className + "' !!!");
			}
			
			Method findOneByCompositeKey;
			Optional<?> itObject = Optional.empty();
			if (hasSubtype) {
				findOneByCompositeKey = queryService.getClass().getMethod(
						"findOneByCompositeKey", Long.class, String.class, Date.class, Date.class);
				itObject = (Optional<?>) findOneByCompositeKey.invoke(queryService, ssn, subtype, 
						CommonDateFunction.convertDateRequestParameterIntoDate(endda), 
						CommonDateFunction.convertDateRequestParameterIntoDate(begda));
			} else {
				findOneByCompositeKey = queryService.getClass().getMethod(
						"findOneByCompositeKey", Long.class, Date.class, Date.class);
				itObject = (Optional<?>) findOneByCompositeKey.invoke(queryService, ssn, 
						CommonDateFunction.convertDateRequestParameterIntoDate(endda), 
						CommonDateFunction.convertDateRequestParameterIntoDate(begda));
			} 
			
			if (!itObject.isPresent()) {
				throw new EntityNotFoundException("Data can't be found!");
			}		
			
			Method getStatus = itObject.get().getClass().getMethod("getStatus");
			DocumentStatus documentStatus = (DocumentStatus) getStatus.invoke(itObject.get());
			if (documentStatus == DocumentStatus.PUBLISHED) {
				throw new DataViolationException("Data is a 'PUBLISHED' data. No need for approval.");
			}	
			
			if (documentStatus == DocumentStatus.DRAFT_REJECTED || documentStatus == DocumentStatus.UPDATE_REJECTED) {
				throw new DataViolationException("You cannot approve a 'REJECTED' data. Please update/fix it first!");
			}
			
			Optional<Role> role = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
			
			StringBuffer insufficientMsg = new StringBuffer();
			
			if (!currentUser.get().getRoles().contains(role.get())) { // If user is not an 'ADMIN', then only 'SUPERIOR' of this user is allowed to do approval
 
				Optional<PositionRelationType> positionRelationType 
					= commonServiceFactory.getPositionRelationTypeQueryService().findByName("REPORT_TO");
				Optional<IT0001> it0001 = commonServiceFactory.getIT0001QueryService().findOneByCompositeKey(ssn, 
						CommonDateFunction.convertDateRequestParameterIntoDate(endda), 
						CommonDateFunction.convertDateRequestParameterIntoDate(begda));
				
				if (!positionRelationType.isPresent()) {
					insufficientMsg.replace(0, insufficientMsg.length(), "Insufficient data. Employee must have a Supervisor or Admin to get an approval!");
					//throw new DataViolationException("Insufficient data. Employee must have a Supervisor or Admin to get an approval!");
				}
				if (!it0001.isPresent()) {
					insufficientMsg.replace(0, insufficientMsg.length(), "Insufficient data. Employee's position cannot be found!");
					//throw new DataViolationException("Insufficient data. Employee's position cannot be found!");
				}
				Collection<PositionRelation> positionRelations = commonServiceFactory.getPositionRelationQueryService()
						.findByFromPositionAndToPositionAndRelationType(
						it0001.get().getPosition().getId(), 
						currentUser.get().getEmployee().getPosition().getId(),
						positionRelationType.get().getName());
				if (positionRelations == null || positionRelations.isEmpty()) {
					insufficientMsg.replace(0, insufficientMsg.length(), "Insufficient Privileges ! Only 'SUPERIOR' of this user is authorized to do the approval !");
					//throw new AuthorizationException("Insufficient Privileges ! Only 'SUPERIOR' of this user is authorized to do the approval !");
				}
			}
			
			// Check the operation type (Create or Update) from the document_status, with one from the following condition
			// 1. if document_status is INITIAL_DRAFT, then the operation_type is CREATE
			// 2. if document_status either UPDATED_DRAFT or UPDATE_IN_PROGRESS, then the operation type is UPDATE
			
			StringBuffer objectId = new StringBuffer("pernr=").append(ssn);
			if (hasSubtype) {
				objectId.append(",infty=").append(infotype);
				objectId.append(",subty=").append(subtype);
			}
			objectId.append(",endda=").append(endda);
			objectId.append(",begda=").append(begda);
			
			Optional<Workflow> workflow = Optional.empty();
			Optional<ApprovalCategory> approvalCategory = Optional.empty();
			Collection<ApprovalEventLog> approvalEventLogs = Collections.emptyList();
			Optional<Collection<WorkflowStep>> workflowSteps = Optional.empty();
			/*
			switch (documentStatus) {
				case INITIAL_DRAFT:
					workflow = commonServiceFactory.getWorkflowQueryService().findByName(WorkflowType.CREATE_HR_MASTER.name());
					if (!workflow.isPresent()) {
						throw new WorkflowNotFoundException("Cannot find workflow for this kind of approval!");
					}
					approvalCategory = commonServiceFactory.getApprovalCategoryQueryService().findByWorkflowAndEntityCategory(workflow.get(), EntityCategory.HR_MASTER_DATA);
					if (!approvalCategory.isPresent()) {
						throw new EntityNotFoundException("Cannot find approval category!");
					}
					
					approvalEventLogs = commonServiceFactory.getApprovalEventLogQueryService()
							.findByEntityIdAndObjectIdAndApprovalAction(hcmsEntity.get().getId(), 
							objectId.toString(), ApprovalAction.APPROVED);
					
					workflowSteps = commonServiceFactory.getWorkflowStepQueryService()
							.findByWorkflowAndSeq(workflow.get(), (approvalEventLogs.isEmpty() ? 1 : approvalEventLogs.size() + 1));

					if (!workflowSteps.isPresent()) {
						throw new WorkflowNotFoundException("Cannot find workflow for this operation! Please define the workflow first!");
					}
					
					WorkflowStep workflowStep = workflowSteps.get().stream().collect(Collectors.toList()).get(0);
					
					if (insufficientMsg.length() > 0 && (!workflowStep.getPosition().getId().equalsIgnoreCase(currentUser.get().getEmployee().getPosition().getId()))) {
						throw new DataViolationException("Only 'SUPERIOR' or 'ADMIN' can do the approval!");
					}
					
					Method setStatus = itObject.get().getClass().getMethod(
									"setStatus", DocumentStatus.class);
					
					if (workflowStep.getApprovalType() == ApprovalType.ONE_SUPERIOR) {
						setStatus.invoke(itObject.get(), DocumentStatus.PUBLISHED);
						Method save = commandService.getClass().getMethod(
								"save", itObject.get().getClass(), currentUser.get().getClass());
						save.invoke(commandService, itObject.get(), currentUser.get());
						
						Constructor<?> cons = soapObjectClass.getConstructor(itObject.get().getClass());
						Object itSoapObject = cons.newInstance(itObject.get()); 
						muleConsumerService.postToSAP(muleService, muleUri, itSoapObject);
					}
					
					break;
				case UPDATED_DRAFT:
				case UPDATE_IN_PROGRESS:
					workflow = commonServiceFactory.getWorkflowQueryService().findByName(WorkflowType.UPDATE_HR_MASTER.name());
					if (!workflow.isPresent()) {
						throw new WorkflowNotFoundException("Cannot find workflow for this kind of approval!");
					}
					approvalCategory = commonServiceFactory.getApprovalCategoryQueryService().findByWorkflowAndEntityCategory(workflow.get(), EntityCategory.HR_MASTER_DATA);
					if (!approvalCategory.isPresent()) {
						throw new EntityNotFoundException("Cannot find approval category!");
					}
					
					approvalEventLogs = commonServiceFactory.getApprovalEventLogQueryService()
							.findByEntityIdAndObjectIdAndApprovalAction(hcmsEntity.get().getId(), 
							objectId.toString(), ApprovalAction.APPROVED);

					workflowSteps = commonServiceFactory.getWorkflowStepQueryService()
							.findByWorkflowAndSeq(workflow.get(), (approvalEventLogs.isEmpty() ? 1 : approvalEventLogs.size() + 1));
					
					break;
					
				default: throw new DataViolationException("Invalid Operation. Can't do approval for this 'PUBLISHED' document!");
			}
			*/
			
			// ********** begin of version 2.0.3
			// *********************************
			
			Date lastCreatedDate = null;
			
			Collection<EventLog> eventLogRecords = commonServiceFactory.getEventLogQueryService().findByEntityIdAndObjectId(
					hcmsEntity.get().getId(), objectId.toString());
			int size = eventLogRecords.stream().collect(Collectors.toList()).size();
			EventLog lastEventLog = eventLogRecords.stream().collect(Collectors.toList()).get(size-1);

			switch (documentStatus) {
				case INITIAL_DRAFT:
					workflow = commonServiceFactory.getWorkflowQueryService().findByName(WorkflowType.CREATE_HR_MASTER.name());
					
					break;
				case UPDATED_DRAFT:
				case UPDATE_IN_PROGRESS:
					workflow = commonServiceFactory.getWorkflowQueryService().findByName(WorkflowType.UPDATE_HR_MASTER.name());
					
					if (!eventLogRecords.isEmpty()) {
						for (int i = 0; i < size; i ++) {
							lastEventLog = eventLogRecords.stream().collect(Collectors.toList()).get(i);
							if (lastEventLog.getOperationType() != OperationType.APPROVAL)
								break;
						}
						String eventData = StringUtils.defaultString(lastEventLog.getEventData(), StringUtils.EMPTY);
						JSONObject json = new JSONObject(eventData);
						String attachmentPath = (String) json.remove("attachment1_path");
						Object itWrapper = new EventDataWrapper(json.toString(), requestWrapper).getItWrapper();
						
						Method compareAndReturnUpdatedData = requestBuilder.getClass().getMethod(
								"compareAndReturnUpdatedData", requestWrapper.getClass(), itObject.get().getClass());
						Object updatedContainer = compareAndReturnUpdatedData.invoke(requestBuilder, itWrapper, itObject.get());
						
						Method getEntity = updatedContainer.getClass().getMethod("getEntity");
						itObject = (Optional<?>) getEntity.invoke(updatedContainer);
						if (!StringUtils.isEmpty(attachmentPath) && itObject.isPresent()) {
							try {
								Method setAttachmentPath = itObject.get().getClass().getMethod("setAttachmentPath", String.class);
								setAttachmentPath.invoke(itObject.get(), attachmentPath);
							} catch (Exception e) {
								// do nothing
							}
						}
						
						if (infotype.equals(SAPInfoType.COMMUNICATION_TYPE.infoType())) {
							muleUri = muleUri.concat("_change");
						}
					}
					
					break;
					
				default: throw new DataViolationException("Invalid Operation. Can't do approval for this 'PUBLISHED' document!");
			}
			
			lastCreatedDate = lastEventLog.getCreatedAt();
			
			if (!workflow.isPresent()) {
				throw new WorkflowNotFoundException("Cannot find workflow for this kind of approval!");
			}
			approvalCategory = commonServiceFactory.getApprovalCategoryQueryService().findByWorkflowAndEntityCategory(workflow.get(), EntityCategory.HR_MASTER_DATA);
			if (!approvalCategory.isPresent()) {
				throw new EntityNotFoundException("Cannot find approval category!");
			}
			
			approvalEventLogs = commonServiceFactory.getApprovalEventLogQueryService()
					.findByEntityIdAndObjectIdAndApprovalAction(hcmsEntity.get().getId(), 
					objectId.toString(), ApprovalAction.APPROVED, lastCreatedDate);
			
			workflowSteps = commonServiceFactory.getWorkflowStepQueryService()
					.findByWorkflowAndSeq(workflow.get(), (approvalEventLogs.isEmpty() ? 1 : approvalEventLogs.size() + 1));

			if (!workflowSteps.isPresent()) {
				throw new WorkflowNotFoundException("Cannot find workflow for this operation! Please define the workflow first!");
			}
			
			WorkflowStep workflowStep = workflowSteps.get().stream().collect(Collectors.toList()).get(0);
			
			if (insufficientMsg.length() > 0 && (!workflowStep.getPosition().getId().equalsIgnoreCase(currentUser.get().getEmployee().getPosition().getId()))) {
				throw new DataViolationException("Only 'SUPERIOR' or 'ADMIN' can do the approval!");
			}
			
			Method setStatus = itObject.get().getClass().getMethod(
							"setStatus", DocumentStatus.class);
			
			if (workflowStep.getApprovalType() == ApprovalType.ONE_SUPERIOR) {
				setStatus.invoke(itObject.get(), DocumentStatus.PUBLISHED);
				Method save = commandService.getClass().getMethod(
						"save", itObject.get().getClass(), currentUser.get().getClass());
				save.invoke(commandService, itObject.get(), currentUser.get());
				
				Constructor<?> cons = soapObjectClass.getConstructor(itObject.get().getClass());
				Object itSoapObject = cons.newInstance(itObject.get()); 
				//muleConsumerService.postToSAP(muleService, muleUri, itSoapObject);
				muleConsumerService.postToSAPAsync(muleService, muleUri, itSoapObject, commandService, itObject.get());
				
				// any update on user's email should reflect to m_user table 
				if (hasSubtype && infotype.equals(SAPInfoType.COMMUNICATION_TYPE.infoType()) && subtype.equals("0010")) {
					Method getId = itObject.get().getClass().getMethod("getId");
					ITCompositeKeys id = (ITCompositeKeys) getId.invoke(itObject.get());
					Long userPernr = id.getPernr();
					Method getUsridLong = itObject.get().getClass().getMethod("getUsridLong");
					String email = (String) getUsridLong.invoke(itObject.get());
					Optional<User> userObject = commonServiceFactory.getUserQueryService().findByPernr(userPernr);
					if (userObject.isPresent() && !StringUtils.defaultString(userObject.get().getEmail()).equals(email)) {
						userObject.get().setEmail(email);
						commonServiceFactory.getUserCommandService().save(userObject.get(), currentUser.get());
					}
					
				}
			}
			

			// *********************************
			// ********** end of version 2.0.3
						
			/*
			Collection<EventLog> eventLogRecords = commonServiceFactory.getEventLogQueryService().findByEntityIdAndObjectIdAndOperationType(
					it0009Entity.get().getId(), objectId.toString(), 
					(it0009.get().getStatus() == DocumentStatus.INITIAL_DRAFT ? OperationType.CREATE : OperationType.UPDATE));
			*/
			//EventLog eventLog = eventLogRecords.isEmpty() ? null : eventLogRecords.stream().collect(Collectors.toList()).get(0); // get latest 		
			
			// Record (Append) to trx_event_log
			StringBuffer eventData = new StringBuffer("{\"dataChanged\": false, \"prev_document_status\":\"");
			eventData.append(documentStatus);
			eventData.append("\", \"document_status\":\"");
			eventData.append(DocumentStatus.PUBLISHED);
			eventData.append("\"}");
			
			EventLog eventLog = new EventLog(hcmsEntity.get(), objectId.toString(), OperationType.APPROVAL, eventData.toString());
			eventLog.setMailNotification(false);			
			eventLog.setStatus(DocumentStatus.PUBLISHED.toString());
			Optional<EventLog> savedEventLog = commonServiceFactory.getEventLogCommandService().save(eventLog, currentUser.get());
			
			if (!savedEventLog.isPresent()) {
				throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
			}
			
			ApprovalEventLog approvalEventLog = new ApprovalEventLog(hcmsEntity.get(), objectId.toString(), currentUser.get().getEmployee().getPernr(), 
					currentUser.get().getEmployee().getPosition(), ApprovalAction.APPROVED);

			
			Optional<User> submitUser = commonServiceFactory.getUserQueryService().findByPernr(ssn);
			approvalEventLog.setEventLog(eventLog);
			approvalEventLog.setSubmitUser(submitUser.isPresent() ? submitUser.get() : null);
			
			Optional<ApprovalEventLog> savedApprovalEventLog = commonServiceFactory.getApprovalEventLogCommandService().save(approvalEventLog, currentUser.get());
			
			if (!savedApprovalEventLog.isPresent()) {
				throw new CannotPersistException("Cannot approve document! Operation failed.");
			}
			
			//Optional<User> submitUser = commonServiceFactory.getUserQueryService().findByPernr(ssn);
			if (submitUser.isPresent()) {
				User user = submitUser.get();/*
				String processed = "APPROVED";
				commonServiceFactory.getMailService().sendMail(user.getEmail(), "UPDATE ".
		    		concat(hcmsEntity.get().getEntityName()),
		    		"Dear <b>".concat(user.getEmployee().getSname()).concat("</b><br><br>Your <b>".
		    		concat(hcmsEntity.get().getEntityName()).concat("</b> has been ").
		    		concat(processed).concat(" by your HR Admin or Superior.").concat("<br><br>Thank You.")));*/
				Map<String, String> composeMap = commonServiceFactory.getMailService().composeEmail(eventLog, user);
				if (StringUtils.isNotEmpty(composeMap.get("subject"))) {
					String mailBody = composeMap.get("body").replace(commonServiceFactory.getMailService().getMailMasterDataUpdate(),
							commonServiceFactory.getMailService().getMailMasterDataApproved());
					commonServiceFactory.getMailService().sendMail(user.getEmail(), composeMap.get("subject"), mailBody);
				}
			}
			
			ApprovalEventLogResponseWrapper approvalEventLogResponse = new ApprovalEventLogResponseWrapper(approvalEventLog);
			
			response.setData(approvalEventLogResponse);
			
		} catch (EntityNotFoundException e) {
			throw e;
		} catch (WorkflowNotFoundException e) {
			throw e;
		} catch (DataViolationException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		//response.add(linkTo(methodOn(ApprovalController.class).approve(pernr, subtype, endda, begda)).withSelfRel());				
		return response;
	}
	
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> reject(Map<String, String> keyMap, String className, Object queryService,
			Object commandService) 
					throws AuthorizationException, NumberFormatException, CannotPersistException, 
					DataViolationException, EntityNotFoundException, Exception {
		
		String pernr = keyMap.get("ssn");
		String infotype = keyMap.get("infotype");
		String subtype = keyMap.get("subtype");
		String endda = keyMap.get("endda");
		String begda = keyMap.get("begda");
		
		boolean hasSubtype = (subtype != null);
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
			
			if (!currentUser.isPresent()) {
				throw new AuthorizationException(
						"Insufficient privileges. Please login either as 'ADMIN' or 'SUPERIOR'!");						
			}
			
			long ssn = Long.valueOf(pernr);			
			
			if (currentUser.get().getEmployee().getPernr() == ssn) {
				throw new AuthorizationException("You can't reject your own data!");
			}						
			
			Optional<HCMSEntity> hcmsEntity = commonServiceFactory.getHCMSEntityQueryService().findByEntityName(className);
			if (!hcmsEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + className + "' !!!");
			}
			
			Method findOneByCompositeKey;
			Optional<?> itObject = Optional.empty();
			if (hasSubtype) {
				findOneByCompositeKey = queryService.getClass().getMethod(
						"findOneByCompositeKey", Long.class, String.class, Date.class, Date.class);
				itObject = (Optional<?>) findOneByCompositeKey.invoke(queryService, ssn, subtype, 
						CommonDateFunction.convertDateRequestParameterIntoDate(endda), 
						CommonDateFunction.convertDateRequestParameterIntoDate(begda));
			} else {
				findOneByCompositeKey = queryService.getClass().getMethod(
						"findOneByCompositeKey", Long.class, Date.class, Date.class);
				itObject = (Optional<?>) findOneByCompositeKey.invoke(queryService, ssn, 
						CommonDateFunction.convertDateRequestParameterIntoDate(endda), 
						CommonDateFunction.convertDateRequestParameterIntoDate(begda));
			}
			
			if (!itObject.isPresent()) {
				throw new EntityNotFoundException("Data can't be found!");
			}
			
			Method getStatus = itObject.get().getClass().getMethod("getStatus");
			DocumentStatus documentStatus = (DocumentStatus) getStatus.invoke(itObject.get());
			
			if (documentStatus == DocumentStatus.PUBLISHED) {
				throw new DataViolationException("You cannot reject 'PUBLISHED' document!");
			}		
			
			if (documentStatus == DocumentStatus.DRAFT_REJECTED || documentStatus == DocumentStatus.UPDATE_REJECTED) {
				throw new DataViolationException("Document had already been rejected.");
			}

			Method setStatus = itObject.get().getClass().getMethod("setStatus", DocumentStatus.class);
			String status = DocumentStatus.DRAFT_REJECTED.toString();
			
			// Do a 'reject' logic to either INITIAL_DRAFT or UPDATED_DRAFT or UPDATE_IN_PROGRESS document (merely change the 'document_status')
			if (documentStatus == DocumentStatus.INITIAL_DRAFT || documentStatus == DocumentStatus.UPDATED_DRAFT) {
				setStatus.invoke(itObject.get(), DocumentStatus.DRAFT_REJECTED);
			} else if (documentStatus == DocumentStatus.UPDATE_IN_PROGRESS) {
				setStatus.invoke(itObject.get(), DocumentStatus.UPDATE_REJECTED);
				status = DocumentStatus.UPDATE_REJECTED.toString();
			}
			
			StringBuilder key = new StringBuilder("pernr=" + pernr);
			if (hasSubtype) {
			    key.append(",infty=").append(infotype);
			    key.append(",subty=").append(subtype);
			}
			key.append(",endda=").append(endda);
			key.append(",begda=").append(begda);
			
			Collection<EventLog> eventLogRecords = commonServiceFactory.getEventLogQueryService().findByEntityIdAndObjectId(hcmsEntity.get().getId(), key.toString());
			
			EventLog lastEventLog = eventLogRecords.isEmpty() ? null : eventLogRecords.stream().collect(Collectors.toList()).get(0);
			
			EventLog eventLog = new EventLog(hcmsEntity.get(), key.toString(), OperationType.DOCUMENT_REJECT,
					(!StringUtils.isEmpty(lastEventLog.getEventData()) ? lastEventLog.getEventData() : ""));
			eventLog.setMailNotification(false);
			eventLog.setStatus(status);
			Optional<EventLog> savedEventLog = commonServiceFactory.getEventLogCommandService().save(eventLog, currentUser.get());
			
			if (!savedEventLog.isPresent()) {
				throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
			}
			
			ApprovalEventLog approvalEventLog = new ApprovalEventLog(hcmsEntity.get(), key.toString(), currentUser.get().getEmployee().getPernr(), 
					currentUser.get().getEmployee().getPosition(), ApprovalAction.REJECTED);

			
			Optional<User> submitUser = commonServiceFactory.getUserQueryService().findByPernr(ssn);
			//approvalEventLog.setEventLog(eventLog);
			//approvalEventLog.setSubmitUser(submitUser.isPresent() ? submitUser.get() : null);
			
			Optional<ApprovalEventLog> savedApprovalEventLog = commonServiceFactory.getApprovalEventLogCommandService().save(approvalEventLog, currentUser.get());
			
			if (!savedApprovalEventLog.isPresent()) {
				throw new CannotPersistException("Cannot reject document! Operation failed.");
			}
			if (submitUser.isPresent()) {
				User user = submitUser.get();/*
				String processed = "APPROVED";
				commonServiceFactory.getMailService().sendMail(user.getEmail(), "UPDATE ".
		    		concat(hcmsEntity.get().getEntityName()),
		    		"Dear <b>".concat(user.getEmployee().getSname()).concat("</b><br><br>Your <b>".
		    		concat(hcmsEntity.get().getEntityName()).concat("</b> has been ").
		    		concat(processed).concat(" by your HR Admin or Superior.").concat("<br><br>Thank You.")));*/
				Map<String, String> composeMap = commonServiceFactory.getMailService().composeEmail(eventLog, user);
				if (StringUtils.isNotEmpty(composeMap.get("subject"))) {
					String mailBody = composeMap.get("body").replace(commonServiceFactory.getMailService().getMailMasterDataUpdate(),
							commonServiceFactory.getMailService().getMailMasterDataRejected());
					commonServiceFactory.getMailService().sendMail(user.getEmail(), composeMap.get("subject"), mailBody);
				}
			}
			
			ApprovalEventLogResponseWrapper approvalEventLogResponse = new ApprovalEventLogResponseWrapper(approvalEventLog);
			
			response.setData(approvalEventLogResponse);
			
		} catch (AuthorizationException e) { 
			throw e;
		} catch (NumberFormatException e) { 
			throw e;
		} catch (CannotPersistException e) {
			throw e;
		} catch (DataViolationException e) {
			throw e;
		} catch (EntityNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		//response.add(linkTo(methodOn(ApprovalController.class).reject(pernr, subtype, endda, begda)).withSelfRel());		
		return response;
	}
	
	public APIResponseWrapper<?> getUpdatedFields(Map<String, String> keyMap, String className, Object queryService,
			APIResponseWrapper<?> response, Class<?> responseWrapper)
			throws DataViolationException, EntityNotFoundException, Exception {

		String pernr = keyMap.get("ssn");
		String infotype = keyMap.get("infotype");
		String subtype = keyMap.get("subtype");
		String endda = keyMap.get("endda");
		String begda = keyMap.get("begda");
		
		boolean hasSubtype = (subtype != null);
		
		try {
			
			long ssn = Long.valueOf(pernr);	
			
			Method findOneByCompositeKey;
			Optional<?> itObject = Optional.empty();
			if (hasSubtype) {
				findOneByCompositeKey = queryService.getClass().getMethod(
						"findOneByCompositeKey", Long.class, String.class, Date.class, Date.class);
				itObject = (Optional<?>) findOneByCompositeKey.invoke(queryService, ssn, subtype, 
						CommonDateFunction.convertDateRequestParameterIntoDate(endda), 
						CommonDateFunction.convertDateRequestParameterIntoDate(begda));
			} else {
				findOneByCompositeKey = queryService.getClass().getMethod(
						"findOneByCompositeKey", Long.class, Date.class, Date.class);
				itObject = (Optional<?>) findOneByCompositeKey.invoke(queryService, ssn, 
						CommonDateFunction.convertDateRequestParameterIntoDate(endda), 
						CommonDateFunction.convertDateRequestParameterIntoDate(begda));
			}
			
			if (!itObject.isPresent()) {
				throw new EntityNotFoundException("Data can't be found!");
			}
			Method getStatus = itObject.get().getClass().getMethod("getStatus");
			DocumentStatus documentStatus = (DocumentStatus) getStatus.invoke(itObject.get());
			if (documentStatus != DocumentStatus.UPDATE_IN_PROGRESS &&
					documentStatus != DocumentStatus.UPDATED_DRAFT &&
					documentStatus != DocumentStatus.UPDATE_REJECTED &&
					documentStatus != DocumentStatus.DRAFT_REJECTED ) {
				throw new DataViolationException("No Data Changes! Data is up-to-date.");
			}
			
			Optional<HCMSEntity> hcmsEntity = commonServiceFactory.getHCMSEntityQueryService().findByEntityName(className);
			if (!hcmsEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + className + "' !!!");
			}

			StringBuffer objectId = new StringBuffer("pernr=").append(ssn);
			if (hasSubtype) {
				objectId.append(",infty=").append(infotype);
				objectId.append(",subty=").append(subtype);
			}
			objectId.append(",endda=").append(endda);
			objectId.append(",begda=").append(begda);
			
			Collection<EventLog> eventLogRecords = commonServiceFactory.getEventLogQueryService().findByEntityIdAndObjectId(
					hcmsEntity.get().getId(), objectId.toString());
			
			EventLog eventLog = eventLogRecords.isEmpty() ? null : eventLogRecords.stream().collect(Collectors.toList()).get(0);
			
			Constructor<?> cons = responseWrapper.getConstructor(itObject.get().getClass(), String.class, commonServiceFactory.getClass());
			Method setData = response.getClass().getMethod("setData", Object.class);
			setData.invoke(response, cons.newInstance(itObject.get(), eventLog.getEventData(), commonServiceFactory));
			
		} catch (DataViolationException e) { 
			throw e;
		} catch (EntityNotFoundException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		//response.add(linkTo(methodOn(ApprovalController.class).getUpdatedFields(ssn, subtype, endda, begda)).withSelfRel());
		return response;
	}

}
