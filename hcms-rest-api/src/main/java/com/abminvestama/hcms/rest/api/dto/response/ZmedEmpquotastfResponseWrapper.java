package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class ZmedEmpquotastfResponseWrapper extends ResourceSupport  {

	private Date datab;
	private Date datbi;
	
	private String bukrs;
	private String persg;
	private String persk;
	private String kodequo;
	private String stell;
	private String quotamt;
	
	private ZmedEmpquotastfResponseWrapper() {}
	
	public ZmedEmpquotastfResponseWrapper(ZmedEmpquotastf zmedEmpquotastf) {
		if (zmedEmpquotastf == null) {
			new ZmedEmpquotastfResponseWrapper();
		} else {
			this
			//.setBukrs(zmedEmpquotastf.getBukrs() != null ? StringUtils.defaultString(zmedEmpquotastf.getBukrs().getBukrs(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setDatab(zmedEmpquotastf.getDatab())
			.setDatbi(zmedEmpquotastf.getDatbi())
			.setBukrs(StringUtils.defaultString(zmedEmpquotastf.getBukrs().getBukrs(), StringUtils.EMPTY))
			.setPersk(StringUtils.defaultString(zmedEmpquotastf.getPersk(), StringUtils.EMPTY))
			.setPersg(StringUtils.defaultString(zmedEmpquotastf.getPersg(), StringUtils.EMPTY))
			.setStell(StringUtils.defaultString(zmedEmpquotastf.getStell().toString(), StringUtils.EMPTY))
			.setKodequo(StringUtils.defaultString(zmedEmpquotastf.getKodequo(), StringUtils.EMPTY))
			.setQuotamt(StringUtils.defaultString(zmedEmpquotastf.getQuotamt().toString(), StringUtils.EMPTY));			
		}
	}
	
	

	@JsonProperty("company_code")
	public String getBukrs() {
		return bukrs;
	}
	
	private ZmedEmpquotastfResponseWrapper setBukrs(String bukrs) {
		this.bukrs = bukrs;
		return this;
	}
	

	@JsonProperty("stell")
	public String getStell() {
		return stell;
	}
	
	private ZmedEmpquotastfResponseWrapper setStell(String stell) {
		this.stell = stell;
		return this;
	}
	
	

	
	
	@JsonProperty("quota_type")
	public String getKodequo() {
		return kodequo;
	}
	
	private ZmedEmpquotastfResponseWrapper setKodequo(String kodequo) {
		this.kodequo = kodequo;
		return this;
	}
	
	
	
	
	@JsonProperty("quotamt")
	public String getQuotamt() {
		return quotamt;
	}
	
	private ZmedEmpquotastfResponseWrapper setQuotamt(String quotamt) {
		this.quotamt = quotamt;
		return this;
	}
	
	
	@JsonProperty("persg")
	public String getPersg() {
		return persg;
	}
	
	private ZmedEmpquotastfResponseWrapper setPersg(String persg) {
		this.persg = persg;
		return this;
	}
	
	
	@JsonProperty("persk")
	public String getPersk() {
		return persk;
	}
	
	private ZmedEmpquotastfResponseWrapper setPersk(String persk) {
		this.persk = persk;
		return this;
	}

	
	
	@JsonProperty("valid_from_date")
	public Date getDatab() {
		return datab;
	}
	
	private ZmedEmpquotastfResponseWrapper setDatab(Date datab) {
		this.datab = datab;
		return this;
	}
	


	
	
	@JsonProperty("valid_to_date")
	public Date getDatbi() {
		return datbi;
	}
	
	private ZmedEmpquotastfResponseWrapper setDatbi(Date datbi) {
		this.datbi = datbi;
		return this;
	}

}