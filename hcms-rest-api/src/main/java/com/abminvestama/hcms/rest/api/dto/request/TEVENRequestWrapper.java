package com.abminvestama.hcms.rest.api.dto.request;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;


public class TEVENRequestWrapper extends ZmedRequestWrapper {

	private static final long serialVersionUID = -4536274729925117737L;
	

	private String tevenid;
	private long pdsnr;
	private long pernr;
	
	private String ldate;
	private String ltime;
	private String erdat;
	private String ertim;
	
	private String pmdbe;
	private String satza;
	private String terid;
	private String abwgr;
	private String exlga;
	private String zeinh;

	private Double hrazl;
	private Double hrbet;
	private String origf;
	private String dallf;
	private String pdcUsrup;
	
	private String uname;
	private String aedtm;
	private Long zausw;
	private String process;
	private String reason;
	
	private String trxcode;
	private String submittedBy;
	private String approved1By;
	private String approved2By;
	private String approved3By;
	private String approved4By;
	private String approved5By;
	private String approved6By;
	private String approvalNote;
	

	@JsonProperty("tevenid")
	public String getTevenid() {
		return tevenid;
	}

	@JsonProperty("pdsnr")
	public long getPdsnr() {
		return pdsnr;
	}

	@JsonProperty("pernr")
	public long getPernr() {
		return pernr;
	}

	@JsonProperty("ldate")
	public String getLdate() {
		return ldate;
	}

	@JsonProperty("ltime")
	public String getLtime() {
		return ltime;
	}

	@JsonProperty("erdat")
	public String getErdat() {
		return erdat;
	}

	@JsonProperty("ertim")
	public String getErtim() {
		return ertim;
	}

	@JsonProperty("pmdbe")
	public String getPmdbe() {
		return pmdbe;
	}
	
	@JsonProperty("satza")
	public String getSatza() {
		return satza;
	}

	@JsonProperty("terid")
	public String getTerid() {
		return terid;
	}

	@JsonProperty("abwgr")
	public String getAbwgr() {
		return abwgr;
	}

	@JsonProperty("exlga")
	public String getExlga() {
		return exlga;
	}

	@JsonProperty("zeinh")
	public String getZeinh() {
		return zeinh;
	}

	@JsonProperty("hrazl")
	public Double getHrazl() {
		return hrazl;
	}

	@JsonProperty("hrbet")
	public Double getHrbet() {
		return hrbet;
	}

	@JsonProperty("origf")
	public String getOrigf() {
		return origf;
	}

	@JsonProperty("dallf")
	public String getDallf() {
		return dallf;
	}

	@JsonProperty("pdc_usrup")
	public String getPdcUsrup() {
		return pdcUsrup;
	}

	@JsonProperty("uname")
	public String getUname() {
		return uname;
	}

	@JsonProperty("aedtm")
	public String getAedtm() {
		return aedtm;
	}
	
	@JsonProperty("zausw")
	public Long getZausw() {
		return zausw;
	}
	
	@JsonProperty("process")
	public String getProcess() {
		return process;
	}
	
	@JsonProperty("reason")
	public String getReason() {
		return reason;
	}
	
	@JsonProperty("trxcode")
	public String getTrxcode() {
		return trxcode;
	}
	
	@JsonProperty("submitted_by")
	public String getSubmittedBy() {
		return submittedBy;
	}

	@JsonProperty("approved1_by")
	public String getApproved1By() {
		return approved1By;
	}

	@JsonProperty("approved2_by")
	public String getApproved2By() {
		return approved2By;
	}

	@JsonProperty("approved3_by")
	public String getApproved3By() {
		return approved3By;
	}

	@JsonProperty("approved4_by")
	public String getApproved4By() {
		return approved4By;
	}

	@JsonProperty("approved5_by")
	public String getApproved5By() {
		return approved5By;
	}

	@JsonProperty("approved6_by")
	public String getApproved6By() {
		return approved6By;
	}

	@JsonProperty("approvalNote")
	public String getApprovalNote() {
		return approvalNote;
	}
	
}