package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.constant.ApprovalAction;
import com.abminvestama.hcms.core.model.constant.ApprovalType;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.UserRole;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.ApprovalCategory;
import com.abminvestama.hcms.core.model.entity.ApprovalEventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.HCMSEntityApproval;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.Position;
import com.abminvestama.hcms.core.model.entity.PositionRelation;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.WorkflowStep;
import com.abminvestama.hcms.core.service.api.business.query.EventLogQueryService;
import com.abminvestama.hcms.rest.api.dto.request.ApprovalEventLogRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ApprovalEventLogResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 * 
 * TODO:: MUST REFACTOR !!!. MUST REFACTOR !!!. MUST REFACTOR before UAT !!!.
 *
 */
@RestController
@RequestMapping("/api/v2/approvals")
public class ApprovalEventLogController extends AbstractResource {

	@PersistenceContext(type = PersistenceContextType.TRANSACTION)	
	private EntityManager em;
	
	private EventLogQueryService eventLogQueryService;
	
	private Validator approvalEventLogValidator;
	
	@Autowired
	@Qualifier("approvalEventLogValidator")
	void setApprovalEventLogValidator(Validator approvalEventLogValidator) {
		this.approvalEventLogValidator = approvalEventLogValidator;
	}
	
	@Autowired
	void setEventLogQueryService(EventLogQueryService eventLogQueryService) {
		this.eventLogQueryService = eventLogQueryService;
	}
	
	@Secured({ AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP })
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@SuppressWarnings("unchecked")
	@Transactional(
			readOnly = false, propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, 
			rollbackFor = { CannotPersistException.class, NumberFormatException.class, Exception.class 
	})	
	public APIResponseWrapper<?> create(@RequestBody @Validated ApprovalEventLogRequestWrapper request, 
			BindingResult result) throws AuthorizationException, CannotPersistException, NumberFormatException, Exception {
		
		if (result.hasErrors()) {
			return new APIResponseWrapper<>(
					new ArrayData<>(ExceptionResponseWrapper.getErrors(result.getFieldErrors(), HttpMethod.POST)),
					"Validation Error!");			
		}
		
		APIResponseWrapper<?> response = null;
				
		try {

			Query userQuery = em.createNativeQuery("select * from m_user where user_name = :username", User.class);
			userQuery.setParameter("username", (currentUser().isPresent() ? currentUser().get() : ""));
			Optional<User> currentUser = Optional.ofNullable((User) userQuery.getSingleResult());
			
			if (!currentUser.isPresent()) {
				throw new UsernameNotFoundException("No User logged in!");
			}
			
			List<IT0001> it0001List = em.createNativeQuery("select * from it0001 where pernr = :pernr order by endda desc, begda desc", IT0001.class)
				.setParameter("pernr", Long.valueOf(request.getSubordinateSSN())).getResultList();
			if (it0001List == null || it0001List.isEmpty()) {
				throw new CannotPersistException("Cannot perform approval. Cannot find data for employee with SSN '" + request.getSubordinateSSN() + "'!");
			}
			
			if (currentUser.get().getEmployee().getPernr() == it0001List.get(0).getPernr()) {
				throw new CannotPersistException(
						"Cannot perform approval. You can't approve your data by your own. Please ask either your Superior or Admin for approval!"
				);
			}			
			
			Position subordinatePosition = it0001List.get(0).getPosition();
			
			Role role = (Role) em.createNativeQuery("select * from m_role where role_name = :role", Role.class)
					.setParameter("role", UserRole.ROLE_ADMIN.toString()).getSingleResult();

			List<PositionRelation> positionRelations = em.createNativeQuery(
					"select * from position_relations where from_position_id = :fromPositionId and to_position_id = :toPositionId", PositionRelation.class)
				.setParameter("fromPositionId", subordinatePosition.getId())
				.setParameter("toPositionId", request.getPerformedByPositionId())
				.getResultList();
			
			if (!currentUser.get().getRoles().contains(role)) {
				if (positionRelations == null || positionRelations.isEmpty()) {
					throw new AuthorizationException("Cannot perform Approval. Only 'SUPERIOR' of this user is authorized to do the approval !");
				}				
			}			
			
			Optional<HCMSEntity> hcmsEntity = Optional.ofNullable((HCMSEntity) em.createNativeQuery(
					"select * from cfg_entity where id = :entityId", HCMSEntity.class)
							.setParameter("entityId", request.getEntityId()).getSingleResult());

			if (!hcmsEntity.isPresent()) {
				throw new CannotPersistException("Cannot perform approval. Cannot find Entity with ID '" + request.getEntityId() + "'!");
			}
			
			Optional<Position> superPosition = Optional.ofNullable((Position) em.createNativeQuery(
					"select * from m_positions where id = :positionId", Position.class)
						.setParameter("positionId", request.getPerformedByPositionId()).getSingleResult());
			
			if (!superPosition.isPresent()) {
				throw new CannotPersistException("Cannot perform approval. Cannot find Position with ID '" + request.getPerformedByPositionId() + "'!");
			}
						
			Optional<HCMSEntityApproval> entityApproval 
				= Optional.ofNullable((HCMSEntityApproval) em.createNativeQuery(
					"select * from cfg_entity_approval where cfg_entity_id = :entityId and operation_type = :operationType", HCMSEntityApproval.class)
					.setParameter("entityId", request.getEntityId())
					//.setParameter("operationType", OperationType.valueOf(request.getOperationType()))
					.setParameter("operationType", request.getOperationType())
					.getSingleResult());
			
			if (!entityApproval.isPresent()) {
				throw new CannotPersistException("Cannot perform approval. No Approval Configuration found..please contact your Admin!");
			}
			
			Optional<ApprovalCategory> approvalCategory
				= Optional.ofNullable((ApprovalCategory) em.createNativeQuery(
						"select * from cfg_approval_category where id = :approvalCategoryId", ApprovalCategory.class)
					.setParameter("approvalCategoryId", entityApproval.get().getApprovalCategory().getId())
					.getSingleResult());
			
			if (!approvalCategory.isPresent()) {
				throw new CannotPersistException("Cannot perform approval. No Approval Configuration found..please contact your Admin!");
			}
			
			// searching for workflow_steps
			List<WorkflowStep> workflowSteps = em.createNativeQuery(
					"select * from workflow_steps where m_workflow_id = :workflowId order by seq", WorkflowStep.class)
				.setParameter("workflowId", approvalCategory.get().getWorkflow().getId())
				.getResultList();
			
			// use sequence_type, approval_type, and m_position_id to decide whether document_status should be updated to 'PUBLISHED' or not.
			
			List<ApprovalEventLog> approvalEventLogRecords = em.createNativeQuery(
					"select * from trx_approval_event_log where cfg_entity_id = :hcmsEntity and entity_object_id = :entityObjectId", ApprovalEventLog.class
			).setParameter("hcmsEntity", hcmsEntity.get().getId()).setParameter("entityObjectId", request.getEntityObjectId()).getResultList();
			
			StringTokenizer token = new StringTokenizer(request.getEntityObjectId(), ",");
			
			String prevStatus = "";
			String newStatus = "";
			
			
			if (approvalEventLogRecords.isEmpty()) { // no admin/superior has approved yet
				
				WorkflowStep ws = workflowSteps.get(0);
				
				if (ws.getApprovalType() == ApprovalType.ONE_SUPERIOR) {
					
					if (ws.getPosition().getId().equals(superPosition.get().getId())) {
						
						if (token.countTokens() == 5) {							
							int q = em.createNativeQuery(
									"update " + hcmsEntity.get().getEntityName().toLowerCase() + " set document_status = "+ (entityApproval.get().getOperationType().equals("CREATE")?"PUBLISHED" : (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATED_DRAFT" : (entityApproval.get().getOperationType().equals("DELETE")?"DRAFT_REJECTED" : "INITIAL_DRAFT"  )  ) ) 
									+" where document_status = 'INITIAL_DRAFT' AND "
									+"pernr = :pernr AND infty = :infty AND subty = :subty AND endda = :endda AND begda = :begda")
							.setParameter("pernr", Long.parseLong(token.nextToken().split("=")[1]))
							.setParameter("subty", token.nextToken().split("=")[1])							
							.setParameter("infty", token.nextToken().split("=")[1])
							.setParameter("endda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
							.setParameter("begda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
							.executeUpdate();
							
							prevStatus = "INITIAL_DRAFT";
							newStatus = (entityApproval.get().getOperationType().equals("CREATE")?"PUBLISHED" : (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATED_DRAFT" : (entityApproval.get().getOperationType().equals("DELETE")?"DRAFT_REJECTED" : "INITIAL_DRAFT"  )  ) );
							
							
							if(q <= 0) {								

								q = em.createNativeQuery(
										"update " + hcmsEntity.get().getEntityName().toLowerCase() + " set document_status = "+ (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATE_IN_PROGRESS" : "PUBLISHED"  ) 
										+" where document_status = 'PUBLISHED' AND "
										+"pernr = :pernr AND infty = :infty AND subty = :subty AND endda = :endda AND begda = :begda")
										.setParameter("pernr", Long.parseLong(token.nextToken().split("=")[1]))
										.setParameter("subty", token.nextToken().split("=")[1])							
										.setParameter("infty", token.nextToken().split("=")[1])
										.setParameter("endda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
										.setParameter("begda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
										.executeUpdate();

								prevStatus = "PUBLISHED";
								newStatus = (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATE_IN_PROGRESS" : "PUBLISHED"  ) ;

								if(q <= 0) {								

									q = em.createNativeQuery(
											"update " + hcmsEntity.get().getEntityName().toLowerCase() + " set document_status = "+ (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATED_DRAFT" : "DRAFT_REJECTED"  ) 
											+" where document_status = 'DRAFT_REJECTED' AND "
											+"pernr = :pernr AND infty = :infty AND subty = :subty AND endda = :endda AND begda = :begda")
											.setParameter("pernr", Long.parseLong(token.nextToken().split("=")[1]))
											.setParameter("subty", token.nextToken().split("=")[1])							
											.setParameter("infty", token.nextToken().split("=")[1])
											.setParameter("endda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
											.setParameter("begda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
											.executeUpdate();

									prevStatus = "DRAFT_REJECTED";
									newStatus =  (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATED_DRAFT" : "DRAFT_REJECTED"  ) ;

									if(q <= 0) {								

										q = em.createNativeQuery(
												"update " + hcmsEntity.get().getEntityName().toLowerCase() + " set document_status = "+ (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATE_IN_PROGRESS" : "UPDATE_REJECTED"  ) 
												+" where document_status = 'UPDATE_REJECTED' AND "
												+"pernr = :pernr AND infty = :infty AND subty = :subty AND endda = :endda AND begda = :begda")
												.setParameter("pernr", Long.parseLong(token.nextToken().split("=")[1]))
												.setParameter("subty", token.nextToken().split("=")[1])							
												.setParameter("infty", token.nextToken().split("=")[1])
												.setParameter("endda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
												.setParameter("begda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
												.executeUpdate();

										prevStatus = "UPDATE_REJECTED";
										newStatus =  (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATE_IN_PROGRESS" : "UPDATE_REJECTED"  ) ;

										if(q <= 0) {								

											q = em.createNativeQuery(
													"update " + hcmsEntity.get().getEntityName().toLowerCase() + " set document_status = "+ (entityApproval.get().getOperationType().equals("DELETE")?"UPDATE_REJECTED" : (entityApproval.get().getOperationType().equals("CREATE")? "PUBLISHED" :"UPDATE_IN_PROGRESS"  ) )
													+" where document_status = 'UPDATE_IN_PROGRESS' AND "
													+"pernr = :pernr AND infty = :infty AND subty = :subty AND endda = :endda AND begda = :begda")
													.setParameter("pernr", Long.parseLong(token.nextToken().split("=")[1]))
													.setParameter("subty", token.nextToken().split("=")[1])							
													.setParameter("infty", token.nextToken().split("=")[1])
													.setParameter("endda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
													.setParameter("begda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
													.executeUpdate();

											prevStatus = "UPDATE_IN_PROGRESS";
											newStatus =  (entityApproval.get().getOperationType().equals("DELETE")?"UPDATE_REJECTED" : (entityApproval.get().getOperationType().equals("CREATE")? "PUBLISHED" :"UPDATE_IN_PROGRESS"  ) );

											if(q <= 0) {								

												q = em.createNativeQuery(
														"update " + hcmsEntity.get().getEntityName().toLowerCase() + " set document_status = "+ (entityApproval.get().getOperationType().equals("DELETE")?"DRAFT_REJECTED" : (entityApproval.get().getOperationType().equals("CREATE")? "PUBLISHED" :"UPDATED_DRAFT"  ) )
														+" where document_status = 'UPDATED_DRAFT' AND "
														+"pernr = :pernr AND infty = :infty AND subty = :subty AND endda = :endda AND begda = :begda")
														.setParameter("pernr", Long.parseLong(token.nextToken().split("=")[1]))
														.setParameter("subty", token.nextToken().split("=")[1])							
														.setParameter("infty", token.nextToken().split("=")[1])
														.setParameter("endda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
														.setParameter("begda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
														.executeUpdate();

												prevStatus = "UPDATED_DRAFT";
												newStatus = (entityApproval.get().getOperationType().equals("DELETE")?"DRAFT_REJECTED" : (entityApproval.get().getOperationType().equals("CREATE")? "PUBLISHED" :"UPDATED_DRAFT"  ) );

											}
										}
									}
								}
							}
						}
						else if (token.countTokens() == 3) {							
							int q = em.createNativeQuery(
									"update " + hcmsEntity.get().getEntityName().toLowerCase() + " set document_status = "+ (entityApproval.get().getOperationType().equals("CREATE")?"PUBLISHED" : (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATED_DRAFT" : (entityApproval.get().getOperationType().equals("DELETE")?"DRAFT_REJECTED" : "INITIAL_DRAFT"  )  ) ) 
									+" where document_status = 'INITIAL_DRAFT' AND "
									+"pernr = :pernr AND endda = :endda AND  = :begda")
							.setParameter("pernr", Long.parseLong(token.nextToken().split("=")[1]))
							.setParameter("endda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
							.setParameter("begda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
							.executeUpdate();
							
							prevStatus = "INITIAL_DRAFT";
							newStatus = (entityApproval.get().getOperationType().equals("CREATE")?"PUBLISHED" : (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATED_DRAFT" : (entityApproval.get().getOperationType().equals("DELETE")?"DRAFT_REJECTED" : "INITIAL_DRAFT"  )  ) );
							
							
							if(q <= 0) {								

								q = em.createNativeQuery(
										"update " + hcmsEntity.get().getEntityName().toLowerCase() + " set document_status = "+ (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATE_IN_PROGRESS" : "PUBLISHED"  ) 
										+" where document_status = 'PUBLISHED' AND "
										+"pernr = :pernr AND endda = :endda AND begda = :begda")
										.setParameter("pernr", Long.parseLong(token.nextToken().split("=")[1]))
										.setParameter("endda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
										.setParameter("begda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
										.executeUpdate();

								prevStatus = "PUBLISHED";
								newStatus = (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATE_IN_PROGRESS" : "PUBLISHED"  ) ;

								if(q <= 0) {								

									q = em.createNativeQuery(
											"update " + hcmsEntity.get().getEntityName().toLowerCase() + " set document_status = "+ (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATED_DRAFT" : "DRAFT_REJECTED"  ) 
											+" where document_status = 'DRAFT_REJECTED' AND "
											+"pernr = :pernr AND endda = :endda AND begda = :begda")
											.setParameter("pernr", Long.parseLong(token.nextToken().split("=")[1]))
											.setParameter("endda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
											.setParameter("begda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
											.executeUpdate();

									prevStatus = "DRAFT_REJECTED";
									newStatus =  (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATED_DRAFT" : "DRAFT_REJECTED"  ) ;

									if(q <= 0) {								

										q = em.createNativeQuery(
												"update " + hcmsEntity.get().getEntityName().toLowerCase() + " set document_status = "+ (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATE_IN_PROGRESS" : "UPDATE_REJECTED"  ) 
												+" where document_status = 'UPDATE_REJECTED' AND "
												+"pernr = :pernr AND endda = :endda AND begda = :begda")
												.setParameter("pernr", Long.parseLong(token.nextToken().split("=")[1]))
												.setParameter("endda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
												.setParameter("begda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
												.executeUpdate();

										prevStatus = "UPDATE_REJECTED";
										newStatus =  (entityApproval.get().getOperationType().equals("UPDATE")?"UPDATE_IN_PROGRESS" : "UPDATE_REJECTED"  ) ;

										if(q <= 0) {								

											q = em.createNativeQuery(
													"update " + hcmsEntity.get().getEntityName().toLowerCase() + " set document_status = "+ (entityApproval.get().getOperationType().equals("DELETE")?"UPDATE_REJECTED" : (entityApproval.get().getOperationType().equals("CREATE")? "PUBLISHED" :"UPDATE_IN_PROGRESS"  ) )
													+" where document_status = 'UPDATE_IN_PROGRESS' AND "
													+"pernr = :pernr AND endda = :endda AND begda = :begda")
													.setParameter("pernr", Long.parseLong(token.nextToken().split("=")[1]))
													.setParameter("endda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
													.setParameter("begda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
													.executeUpdate();

											prevStatus = "UPDATE_IN_PROGRESS";
											newStatus =  (entityApproval.get().getOperationType().equals("DELETE")?"UPDATE_REJECTED" : (entityApproval.get().getOperationType().equals("CREATE")? "PUBLISHED" :"UPDATE_IN_PROGRESS"  ) );

											if(q <= 0) {								

												q = em.createNativeQuery(
														"update " + hcmsEntity.get().getEntityName().toLowerCase() + " set document_status = "+ (entityApproval.get().getOperationType().equals("DELETE")?"DRAFT_REJECTED" : (entityApproval.get().getOperationType().equals("CREATE")? "PUBLISHED" :"UPDATED_DRAFT"  ) )
														+" where document_status = 'UPDATED_DRAFT' AND "
														+"pernr = :pernr AND endda = :endda AND begda = :begda")
														.setParameter("pernr", Long.parseLong(token.nextToken().split("=")[1]))
														.setParameter("endda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
														.setParameter("begda", CommonDateFunction.convertDateRequestParameterIntoDate(token.nextToken().split("=")[1]))
														.executeUpdate();

												prevStatus = "UPDATED_DRAFT";
												newStatus = (entityApproval.get().getOperationType().equals("DELETE")?"DRAFT_REJECTED" : (entityApproval.get().getOperationType().equals("CREATE")? "PUBLISHED" :"UPDATED_DRAFT"  ) );

											}
										}
									}
								}
							}
						}
					}
				}
			} else { // at least one admin/superior has approved
				System.out.println("ApprovalEventLogController::create::at least one superior/admin has approved");
			}
			
			long ssn = Long.valueOf(request.getPerformedBySSN());
			
			ApprovalEventLog approvalEventLog;
			int rowsAffected;
			
			if(request.getOperationType().equals(OperationType.DOCUMENT_REJECT.name())
					|| request.getOperationType().equals(OperationType.DELETE.name())){

				approvalEventLog = new ApprovalEventLog(hcmsEntity.get(), request.getEntityObjectId(), ssn, superPosition.get(), ApprovalAction.REJECTED);
				rowsAffected = em.createNativeQuery(
						"insert into trx_approval_event_log (performed_by_ssn, performed_by_position_id, created_by_id, cfg_entity_id, entity_object_id) " +
						"values(?, ?, ?, ?, ?)")
						.setParameter(1, Long.valueOf(request.getPerformedBySSN()))
						.setParameter(2, request.getPerformedByPositionId())
						.setParameter(3, currentUser.get().getId())
						.setParameter(4, hcmsEntity.get().getId())
						.setParameter(5, request.getEntityObjectId())
						.executeUpdate();
				
			}else{
				approvalEventLog = new ApprovalEventLog(hcmsEntity.get(), request.getEntityObjectId(), ssn, superPosition.get(), ApprovalAction.APPROVED);
				rowsAffected = em.createNativeQuery(
						"insert into trx_approval_event_log (performed_by_ssn, performed_by_position_id, created_by_id, cfg_entity_id, entity_object_id) " +
						"values(?, ?, ?, ?, ?)")
						.setParameter(1, Long.valueOf(request.getPerformedBySSN()))
						.setParameter(2, request.getPerformedByPositionId())
						.setParameter(3, currentUser.get().getId())
						.setParameter(4, hcmsEntity.get().getId())
						.setParameter(5, request.getEntityObjectId())
						.executeUpdate();
			}
			
			
			if (rowsAffected == 0) {
				throw new CannotPersistException("Cannot perform approval. Cannot insert approval event log!");
			}			
			
			rowsAffected = em.createNativeQuery(
					"insert into trx_event_log(cfg_entity_id,entity_object_id,operation_type,event_data,created_by_id) values(?, ?, ?, ?, ?)"
					).setParameter(1, hcmsEntity.get().getId())
					.setParameter(2, request.getEntityObjectId())
					.setParameter(3, request.getOperationType())  
					.setParameter(4, "{\"dataChanged\":false,\"prev_document_status\":\""+prevStatus+"\",\"document_status\":\""+newStatus+"\"}")
					.setParameter(5, currentUser.get().getId())
					.executeUpdate();
			
			if (rowsAffected == 0) {
				throw new CannotPersistException("Cannot perform approval. Cannot insert event log!");
			}			
			
			ApprovalEventLogResponseWrapper approvalEventLogResponseWrapper = new ApprovalEventLogResponseWrapper(approvalEventLog);
			response = new APIResponseWrapper<ApprovalEventLogResponseWrapper>(approvalEventLogResponseWrapper);
			response.setMessage("Success");
		} catch (AuthorizationException e) {
			throw e;
		} catch (CannotPersistException e) {
			throw e;
		} catch (NumberFormatException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(ApprovalEventLogController.class).create(request, result)).withSelfRel());
		
		return response;
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(approvalEventLogValidator);
	}		
}