package com.abminvestama.hcms.rest.api.model.constant;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public enum HCMSResourceIdentifier {

	USER_SESSION("user_session"),
	USER("user"),
	EMPLOYEE("employee"),
	ROLE("role"),
	POSITION("position"),
	POSITION_RELATION("position_relation"),
	T001("organizational_assignment"),
	IT0006("addresses"),
	WORKFLOW("workflow"),
	WORKFLOW_STEPS("workflow_steps"),
	EDUCATION_OR_TRAINING("education_or_training"),
	SELF("self");
	
	private String label;
	
	private HCMSResourceIdentifier(String label) {
		this.label = label;
	}
	
	public String label() {
		return label;
	}
}