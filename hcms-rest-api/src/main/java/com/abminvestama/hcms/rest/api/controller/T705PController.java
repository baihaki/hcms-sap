package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.entity.T705P;
import com.abminvestama.hcms.core.service.api.business.query.T705PQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.T705PResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.TEVENResponseWrapper;

/**
 * 
 * @author wijanarko (wijanarko777@gmx.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@RestController
@RequestMapping("/api/v1/time_event_type")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class T705PController extends AbstractResource {
	private T705PQueryService t705PQueryService;
	
	@Autowired
	void setT705PQueryService(T705PQueryService t705PQueryService) {
		this.t705PQueryService = t705PQueryService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "look_tvt", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<T705PResponseWrapper>> findAllTimeEventType(
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {

		ArrayData<T705PResponseWrapper> bunchOfT705P = new ArrayData<>();
		APIResponseWrapper<ArrayData<T705PResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfT705P);
		
		try {
			List<T705P> listOfT705P = new ArrayList<>();
			Page<T705P> data = t705PQueryService.fetchAllT705p(pageNumber);
			if (data.hasContent()) {
				listOfT705P = data.getContent();
			}
			
			if (listOfT705P.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<T705PResponseWrapper> records = new ArrayList<>();
				for (T705P t705p : listOfT705P) {
					records.add(new T705PResponseWrapper(t705p));
				}
				bunchOfT705P.setItems(records.toArray(new T705PResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(T705PController.class).findAllTimeEventType(pageNumber)).withSelfRel());
		return response;
	}
}
