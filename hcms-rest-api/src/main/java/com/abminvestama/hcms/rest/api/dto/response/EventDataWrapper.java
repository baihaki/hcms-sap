package com.abminvestama.hcms.rest.api.dto.response;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Base Wrapper for read Event Data (trx_event_log) on all Infotypes
 * 
 * @since 2.0.0
 * @version 2.0.1
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public class EventDataWrapper {
	
	Object itWrapper;
	
	private EventDataWrapper() {
	}
	
	public EventDataWrapper(String eventData, Object responseWrapper) throws JSONException, Exception {
		this();
		itWrapper = responseWrapper;
		if (!StringUtils.isEmpty(eventData)) {
			ObjectMapper m = new ObjectMapper();
			itWrapper = m.readValue(eventData, responseWrapper.getClass());
		}
	}

	public Object getItWrapper() {
		return itWrapper;
	}
	
	public static String[] getNullPropertyNames (Object source) {
	    final BeanWrapper src = new BeanWrapperImpl(source);
	    java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

	    Set<String> emptyNames = new HashSet<String>();
	    for(java.beans.PropertyDescriptor pd : pds) {
	        Object srcValue = src.getPropertyValue(pd.getName());
	        if (srcValue == null) emptyNames.add(pd.getName());
	    }
	    String[] result = new String[emptyNames.size()];
	    return emptyNames.toArray(result);
	}
	
	
}