package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.Position;
import com.abminvestama.hcms.core.model.entity.PositionRelation;
import com.abminvestama.hcms.core.model.entity.PositionRelationType;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.command.PositionRelationCommandService;
import com.abminvestama.hcms.core.service.api.business.query.PositionQueryService;
import com.abminvestama.hcms.core.service.api.business.query.PositionRelationQueryService;
import com.abminvestama.hcms.core.service.api.business.query.PositionRelationTypeQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.request.PositionRelationRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.AuthorResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.PositionRelationResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.UserResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@RestController
@RequestMapping("/api/v2/position_relations")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class PositionRelationController extends AbstractResource {

	private PositionRelationCommandService positionRelationCommandService;
	private PositionRelationQueryService positionRelationQueryService;
	
	private PositionQueryService positionQueryService;
	private PositionRelationTypeQueryService positionRelationTypeQueryService;
	private UserQueryService userQueryService;
	
	private Validator positionRelationValidator;

	@Autowired
	void setPositionRelationCommandService(PositionRelationCommandService positionRelationCommandService) {
		this.positionRelationCommandService = positionRelationCommandService;
	}

	@Autowired
	void setPositionRelationQueryService(PositionRelationQueryService positionRelationQueryService) {
		this.positionRelationQueryService = positionRelationQueryService;
	}
	
	@Autowired
	void setPositionQueryService(PositionQueryService positionQueryService) {
		this.positionQueryService = positionQueryService;
	}
	
	@Autowired
	void setPositionRelationTypeQueryService(PositionRelationTypeQueryService positionRelationTypeQueryService) {
		this.positionRelationTypeQueryService = positionRelationTypeQueryService;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("positionRelationValidator")
	void setPositionRelationValidator(Validator positionRelationValidator) {
		this.positionRelationValidator = positionRelationValidator;
	}
	
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/page/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<PositionRelation>> fetchPage(@PathVariable int pageNumber) throws Exception {
		
		Page<PositionRelation> resultPage = positionRelationQueryService.fetchAllWithPaging(pageNumber);
		
		APIResponseWrapper<Page<PositionRelation>> response = new APIResponseWrapper<>();
		response.setData(resultPage);
		Link self = linkTo(methodOn(PositionRelationController.class).fetchPage(pageNumber)).withSelfRel();
		response.add(self);
		
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated PositionRelationRequestWrapper requestWrapper,
			BindingResult result) throws Exception {
				
		if (result.hasErrors()) {
			return new APIResponseWrapper<>(
					new ArrayData<>(ExceptionResponseWrapper.getErrors(result.getFieldErrors(), HttpMethod.POST)),
					"Validation Error!");
		}
		
		APIResponseWrapper<?> response = null;
		
		Optional<User> currentUser = userQueryService.findByUsername(currentUser());		
		
		try {
			if (!currentUser.isPresent()) {
				throw new UsernameNotFoundException("No User logged in!");
			}
		
			Optional<PositionRelationType> positionRelationType = positionRelationTypeQueryService.findById(
					Optional.of(requestWrapper.getPositionRelationTypeId()));
			Optional<Position> fromPosition = positionQueryService.findById(Optional.of(requestWrapper.getFromPositionId()));
			Optional<Position> toPosition = positionQueryService.findById(Optional.of(requestWrapper.getToPositionId()));
			
			PositionRelation newPositionRelation = new PositionRelation();
			newPositionRelation.setPositionRelationType(positionRelationType.get());
			newPositionRelation.setFromPosition(fromPosition.get());
			newPositionRelation.setToPosition(toPosition.get());
			if (StringUtils.isNotBlank(requestWrapper.getValidFrom())) {
				newPositionRelation.setValidFrom(CommonDateFunction.convertDateRequestParameterIntoDate(requestWrapper.getValidFrom()));
			}
			if (StringUtils.isNotBlank(requestWrapper.getValidThru())) {
				newPositionRelation.setValidThru(CommonDateFunction.convertDateRequestParameterIntoDate(requestWrapper.getValidThru()));
			}
			newPositionRelation.setCreatedById(currentUser.get().getId());
			newPositionRelation.setUpdatedById(currentUser.get().getId());
			
			Optional<PositionRelation> savedEntity = positionRelationCommandService.save(newPositionRelation, currentUser.get());
			PositionRelationResponseWrapper positionRelationResponse = new PositionRelationResponseWrapper(savedEntity.get());
			positionRelationResponse.setCreatedBy(new AuthorResponseWrapper(currentUser.get()));
			positionRelationResponse.setUpdatedBy(new AuthorResponseWrapper(currentUser.get()));
			positionRelationResponse.add(linkTo(methodOn(UserController.class)
					.getUserProfileByUsername(currentUser().get())).withRel(UserResponseWrapper.RESOURCE));
			response = new APIResponseWrapper<PositionRelationResponseWrapper>(positionRelationResponse);
			response.setMessage("Successfully created new 'Position Relation'");
			response.add(linkTo(methodOn(PositionRelationController.class).create(requestWrapper, result)).withSelfRel());						
		} catch (UsernameNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		return response;
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(positionRelationValidator);
	}	
}