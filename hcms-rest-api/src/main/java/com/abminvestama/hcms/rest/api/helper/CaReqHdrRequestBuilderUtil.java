package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.CaReqHdr;
import com.abminvestama.hcms.core.service.api.business.query.CaReqHdrQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.core.service.util.FileAttachmentService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.CaReqHdrRequestWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("caReqHdrRequestBuilderUtil")
@Transactional(readOnly = true)
public class CaReqHdrRequestBuilderUtil {
	

	private CaReqHdrQueryService caReqHdrQueryService;
	private FileAttachmentService fileAttachmentService;
	private T001QueryService t001QueryService;
	
	@Autowired
	void setCaReqHdrQueryService(CaReqHdrQueryService caReqHdrQueryService) {
		this.caReqHdrQueryService = caReqHdrQueryService;
	}
	
	@Autowired
	void setFileAttachmentService(FileAttachmentService fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}
	
	@Autowired
	void setT001QueryService(T001QueryService t001QueryService) {
		this.t001QueryService = t001QueryService;
	}
	
	public RequestObjectComparatorContainer<CaReqHdr, CaReqHdrRequestWrapper, String> compareAndReturnUpdatedData(CaReqHdrRequestWrapper requestPayload, CaReqHdr caReqHdrDB) {
		
		RequestObjectComparatorContainer<CaReqHdr, CaReqHdrRequestWrapper, String> objectContainer = null; 
		
	if (caReqHdrDB == null) {
		caReqHdrDB = new CaReqHdr();
		
		try {
			//------------------------------------------------
			
			Optional<CaReqHdr> existingCaReqHdr = caReqHdrQueryService.findById(Optional.ofNullable(requestPayload.getReqNo()));
			
			if (existingCaReqHdr.isPresent()) {
				throw new DataViolationException("Duplicate CaReqHdr Data for [Request Number=" + requestPayload.getReqNo());
			}
			caReqHdrDB.setReqNo(requestPayload.getReqNo());
			
			//------------------------------------------------
			
			caReqHdrDB.setPernr(requestPayload.getPernr());
			caReqHdrDB.setAmount(requestPayload.getAmount());
			caReqHdrDB.setCurrency(requestPayload.getCurrency());
			caReqHdrDB.setDescription(requestPayload.getDescription());
			caReqHdrDB.setProcess(requestPayload.getProcess());
			caReqHdrDB.setBankName(requestPayload.getBankName());
			caReqHdrDB.setBankAccount(requestPayload.getBankAccount());
			caReqHdrDB.setNotes(requestPayload.getNotes());			
			caReqHdrDB.setAdvanceCode(requestPayload.getAdvanceCode());
			Optional<T001> bukrs = t001QueryService.findByBukrs(requestPayload.getBukrs());
			caReqHdrDB.setBukrs(bukrs.isPresent() ? bukrs.get() : null);
			caReqHdrDB.setAcctAp(requestPayload.getAcctAp());
			caReqHdrDB.setReason(requestPayload.getReason());
			
			caReqHdrDB = saveAttachment(caReqHdrDB, requestPayload);
			
			
		} catch (Exception ignored) {
			System.err.println(ignored);
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(caReqHdrDB, requestPayload);
		
	} else {
		
		if (requestPayload.getAmount() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getAmount(), caReqHdrDB.getAmount())) {
				caReqHdrDB.setAmount(requestPayload.getAmount());
				requestPayload.setDataChanged(true);
			}
		}

		if (StringUtils.isNotBlank(requestPayload.getCurrency())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getCurrency(), caReqHdrDB.getCurrency())) {
				caReqHdrDB.setCurrency(requestPayload.getCurrency());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getDescription())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getDescription(), caReqHdrDB.getDescription())) {
				caReqHdrDB.setDescription(requestPayload.getDescription());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (requestPayload.getProcess() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getProcess(), caReqHdrDB.getProcess())) {
				caReqHdrDB.setProcess(requestPayload.getProcess());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getBankName())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBankName(), caReqHdrDB.getBankName())) {
				caReqHdrDB.setDescription(requestPayload.getBankName());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getBankAccount())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBankAccount(), caReqHdrDB.getBankAccount())) {
				caReqHdrDB.setDescription(requestPayload.getBankAccount());
				requestPayload.setDataChanged(true);
			}
		}

		if (StringUtils.isNotBlank(requestPayload.getNotes())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getNotes(), caReqHdrDB.getNotes())) {
				caReqHdrDB.setNotes(requestPayload.getNotes());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getAdvanceCode())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAdvanceCode(), caReqHdrDB.getAdvanceCode())) {
				caReqHdrDB.setAdvanceCode(requestPayload.getAdvanceCode());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getAcctAp())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAcctAp(), caReqHdrDB.getAcctAp())) {
				caReqHdrDB.setAcctAp(requestPayload.getAcctAp());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getReason())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getReason(), caReqHdrDB.getReason())) {
				caReqHdrDB.setReason(requestPayload.getReason());
				requestPayload.setDataChanged(true);
			}
		}
		
		try {
			caReqHdrDB = saveAttachment(caReqHdrDB, requestPayload);
		} catch (Exception ignored) {
			System.err.println(ignored);
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(caReqHdrDB, requestPayload);
				
	}
	
	ObjectMapper objectMapper = new ObjectMapper();
	String jsonString = "";
	try {
		jsonString = objectMapper.writeValueAsString(requestPayload);
	} catch (JsonProcessingException e) {
		e.printStackTrace();
	}
	
	//JSONObject json = new JSONObject(requestPayload);
	JSONObject json = new JSONObject(jsonString);
	// Adjust attachment fields on EventData Json
	json.remove("attachment1_base64");
	json.remove("attachment1_filename");
	if (caReqHdrDB.getAttachment() != null)
	    json.put("attachment1_path", caReqHdrDB.getAttachment());
	
	objectContainer.setEventData(json.toString());
	
	return objectContainer;
}
	
    public CaReqHdr saveAttachment(CaReqHdr caReqHdrDB, CaReqHdrRequestWrapper requestPayload) throws Exception {
		
		String base64 = requestPayload.getAttach1Content();
		String filename = requestPayload.getAttach1Filename();
		if (StringUtils.isNotEmpty(base64) && StringUtils.isNotEmpty(filename)) {
			String ext = filename.split("\\.")[(filename.split("\\.")).length-1];
			String formatFilename = "CaReqHdr".concat("_").concat(caReqHdrDB.getPernr().toString()).concat("_").concat(
					caReqHdrDB.getReqNo()).concat(".").concat(ext);
		    String path = fileAttachmentService.save(formatFilename, base64);
		    caReqHdrDB.setAttachment(path);
		    requestPayload.setDataChanged(true);
		}
		
		return caReqHdrDB;
		
	}
	
}
