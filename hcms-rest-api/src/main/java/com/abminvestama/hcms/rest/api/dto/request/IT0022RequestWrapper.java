package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add attach1Type, attach1Content, attach1Filename field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public class IT0022RequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = -6702420198984433220L;

	private String slart;
	private String insti;
	private String sland;
	private long ausbi;
	private String slabs;
	private double anzkl;
	private String anzeh;
	private String sltp1;
	private String sltp2;
	private String emark;

	private String attach1Type;
	private String attach1Content;
	private String attach1Filename;
	
	
	@JsonProperty("emark")
	public String getEmark() {
		return emark;
	}
	/**
	 * GET Education Est.
	 * 
	 * @return
	 */
	@JsonProperty("education_est_code")
	public String getSlart() {
		return slart;
	}
	
	/**
	 * GET Institute/Location.
	 * 
	 * @return
	 */
	@JsonProperty("institute")
	public String getInsti() {
		return insti;
	}
	
	/**
	 * GET Country Key.
	 * 
	 * @return
	 */
	@JsonProperty("country_key")
	public String getSland() {
		return sland;
	}
	
	/**
	 * GET Education/Training Code.
	 * 
	 * @return
	 */
	@JsonProperty("edu_or_training_code")
	public long getAusbi() {
		return ausbi;
	}
	
	/**
	 * GET Certificate Code.
	 * 
	 * @return
	 */
	@JsonProperty("certificate_code")
	public String getSlabs() {
		return slabs;
	}
	
	/**
	 * GET Duration of Courses.
	 * 
	 * @return
	 */
	@JsonProperty("duration_of_courses")
	public double getAnzkl() {
		return anzkl;
	}
	
	/**
	 * GET Unit of Time/Meas.
	 * 
	 * @return
	 */
	@JsonProperty("unit_of_time_code")
	public String getAnzeh() {
		return anzeh;
	}
	
	/**
	 * GET Branch of Study 1.
	 * 
	 * @return
	 */
	@JsonProperty("branch_study1_code")
	public String getSltp1() {
		return sltp1;
	}
	
	/**
	 * GET Branch of Study 2.
	 * 
	 * @return
	 */
	@JsonProperty("branch_study2_code")
	public String getSltp2() {
		return sltp2;
	}
	
	@JsonProperty("attachment1_type")
	public String getAttach1Type() {
		return attach1Type;
	}
	
	@JsonProperty("attachment1_base64")
	public String getAttach1Content() {
		return attach1Content;
	}
	
	@JsonProperty("attachment1_filename")
	public String getAttach1Filename() {
		return attach1Filename;
	}
}