package com.abminvestama.hcms.rest.api.helper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.PtrvHeadKey;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.T500L;
import com.abminvestama.hcms.core.model.entity.T702N;
import com.abminvestama.hcms.core.model.entity.PtrvHead;
import com.abminvestama.hcms.core.service.api.business.query.PtrvHeadQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.T500LQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T702NQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T702OQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T706SQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.PtrvHeadRequestWrapper;


/**
 * 
 * @since 1.0.0
 * @version 1.0.6
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Update operation: set new dates and times</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add fields: totalAmount, pulseRejectAmount, fuelRejectAmount</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add field: reason</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add fields: lastPrevMonth, lastThisMonth, form</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add fields: bukrs, process</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add field: kunde</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("PtrvHeadRequestBuilderUtil")
@Transactional(readOnly = true)
public class PtrvHeadRequestBuilderUtil {
	

	private PtrvHeadQueryService ptrvHeadQueryService;
	
	@Autowired
	void setPtrvHeadQueryService(PtrvHeadQueryService ptrvHeadQueryService) {
		this.ptrvHeadQueryService = ptrvHeadQueryService;
	}
	
	private T500LQueryService t500lQueryService;
	
	@Autowired
	void setT500LQueryService(T500LQueryService t500lQueryService) {
		this.t500lQueryService = t500lQueryService;
	}
	
	private T702NQueryService t702nQueryService;
	
	@Autowired
	void setT702NQueryService(T702NQueryService t702nQueryService) {
		this.t702nQueryService = t702nQueryService;
	}
	
	private T706SQueryService t706sQueryService;
	
	@Autowired
	void setT706SQueryService(T706SQueryService t706sQueryService) {
		this.t706sQueryService = t706sQueryService;
	}
	
	private T702OQueryService t702oQueryService;
	
	@Autowired
	void setT702OQueryService(T702OQueryService t702oQueryService) {
		this.t702oQueryService = t702oQueryService;
	}
	
	private T001QueryService t001QueryService;
	
	@Autowired
	void setT001QueryService(T001QueryService t001QueryService) {
		this.t001QueryService = t001QueryService;
	}
	
	public RequestObjectComparatorContainer<PtrvHead, PtrvHeadRequestWrapper, String> compareAndReturnUpdatedData(PtrvHeadRequestWrapper requestPayload, PtrvHead ptrvHeadDB) {
		
		RequestObjectComparatorContainer<PtrvHead, PtrvHeadRequestWrapper, String> objectContainer = null; 
		
	if (ptrvHeadDB == null) {
		ptrvHeadDB = new PtrvHead();
		
		try {
			//------------------------------------------------
			//Integrate with SAP and get reinr
			Optional<PtrvHead> existingPtrvHead = ptrvHeadQueryService.findOneByCompositeKey(
					requestPayload.getHdvrs(), requestPayload.getPernr(), requestPayload.getReinr());
			
			if (existingPtrvHead.isPresent()) {
				throw new DataViolationException("Duplicate PtrvHead Data for [ssn=" + requestPayload.getReinr());
			}
			ptrvHeadDB.setId(
					new PtrvHeadKey(requestPayload.getPernr(), requestPayload.getReinr(), requestPayload.getHdvrs()));
			
			//------------------------------------------------
			
			
			Optional<T500L> bnka = t500lQueryService.findById(Optional.ofNullable(requestPayload.getMolga()));				
			ptrvHeadDB.setMolga(bnka.isPresent() ? bnka.get() : null);
			Optional<T702N> bnka1 = t702nQueryService.findById(Optional.ofNullable(requestPayload.getMorei()));				
			ptrvHeadDB.setMorei(bnka1.isPresent() ? bnka1.get() : null);
			/*
			Optional<T706S> bnka2 = t706sQueryService.findById(requestPayload.getSchem());				
			ptrvHeadDB.setSchem(bnka2.isPresent() ? bnka2.get() : null);
			Optional<T702O> bnka3 = t702oQueryService.findById(requestPayload.getZland());				
			ptrvHeadDB.setZland(bnka3.isPresent() ? bnka3.get() : null);
			*/
			//ptrvHeadDB.setMolga(requestPayload.getMolga());
			//ptrvHeadDB.setMorei(requestPayload.getMorei());
			ptrvHeadDB.setSchem(requestPayload.getSchem());
			ptrvHeadDB.setZort1(requestPayload.getZort1());
			ptrvHeadDB.setZland(requestPayload.getZland());			
			ptrvHeadDB.setUname(requestPayload.getUname());
			ptrvHeadDB.setRepid(requestPayload.getRepid());
			ptrvHeadDB.setDantn(requestPayload.getDantn());
			ptrvHeadDB.setFintn(requestPayload.getFintn());			
			ptrvHeadDB.setRequest(requestPayload.getRequest());
			ptrvHeadDB.setTravelPlan(requestPayload.getTravelPlan());
			ptrvHeadDB.setExpenses(requestPayload.getExpenses());
			ptrvHeadDB.setAbordnung(requestPayload.getAbordnung());
			ptrvHeadDB.setKunde(requestPayload.getKunde());
			ptrvHeadDB.setDatv1(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatv1()));
			ptrvHeadDB.setDatb1(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatb1()));
			Optional<T001> bukrs = t001QueryService.findByBukrs(requestPayload.getBukrs());//				
			ptrvHeadDB.setBukrs(bukrs.isPresent() ? bukrs.get() : null);
			ptrvHeadDB.setProcess(requestPayload.getProcess());
			ptrvHeadDB.setLastPrevMonth(requestPayload.getLastPrevMonth());
			ptrvHeadDB.setLastThisMonth(requestPayload.getLastThisMonth());
			ptrvHeadDB.setForm(requestPayload.getForm() != null ? requestPayload.getForm().shortValue() : 0);
			ptrvHeadDB.setReason(requestPayload.getReason());
			
			DateFormat formatter = new SimpleDateFormat("hh:mm");
			 //Date date = formatter.parse(str);
			
			ptrvHeadDB.setExchangeDate(requestPayload.getExchangeDate() != null ?
					CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getExchangeDate()) : null);
			/*
			ptrvHeadDB.setUhrb1Dienst(formatter.parse(requestPayload.getUhrb1Dienst()));
			ptrvHeadDB.setDatb1Dienst(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatb1Dienst()));
			ptrvHeadDB.setUrhv1Dienst(formatter.parse(requestPayload.getUhrv1Dienst()));
			ptrvHeadDB.setDatv1Dienst(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatv1Dienst()));
			ptrvHeadDB.setDatReduc1(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatReduc1()));
			ptrvHeadDB.setDatReduc2(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatReduc2()));
			ptrvHeadDB.setStTrgtg(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getStTrgtg()));
			ptrvHeadDB.setStTrgall(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getStTrgall()));
			ptrvHeadDB.setTimes(formatter.parse(requestPayload.getTimes()));
			ptrvHeadDB.setDates(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDates()));
			ptrvHeadDB.setEndrg(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndrg()));
			ptrvHeadDB.setUhrv1(formatter.parse(requestPayload.getUhrv1()));
			ptrvHeadDB.setUhrb1(formatter.parse(requestPayload.getUhrb1()));
			ptrvHeadDB.setDath1(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDath1()));
			ptrvHeadDB.setUhrh1(formatter.parse(requestPayload.getUhrh1()));
			ptrvHeadDB.setDatr1(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatr1()));
			ptrvHeadDB.setUhrr1(formatter.parse(requestPayload.getUhrr1()));
			*/
			ptrvHeadDB.setDates(new Date());
			ptrvHeadDB.setTimes(new Date());
			ptrvHeadDB.setTotalAmount(requestPayload.getTotalAmount());
			
			JSONObject json = new JSONObject();
			if (requestPayload.getPulseRejectAmount() != null) {
				json.put("CP01", requestPayload.getPulseRejectAmount().doubleValue());
			}
			if (requestPayload.getFuelRejectAmount() != null) {
				json.put("GA01", requestPayload.getFuelRejectAmount().doubleValue());
			}
			if (json.length() > 0) {
				ptrvHeadDB.setRejectAmounts(json.toString());
			}
			
			
		} catch (Exception ignored) {
			System.err.println(ignored);
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(ptrvHeadDB, requestPayload);
		
	} else {

		if (StringUtils.isNotBlank(requestPayload.getSchem())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getSchem(), ptrvHeadDB.getSchem())) {
				ptrvHeadDB.setSchem(requestPayload.getSchem());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getZland())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getZland(), ptrvHeadDB.getZland())) {
				ptrvHeadDB.setZland(requestPayload.getZland());
				requestPayload.setDataChanged(true);
			}
		}

		if (StringUtils.isNotBlank(requestPayload.getKunde())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getKunde(), ptrvHeadDB.getKunde())) {
				ptrvHeadDB.setKunde(requestPayload.getKunde());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getDatv1())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatv1()), ptrvHeadDB.getDatv1()) ){
				ptrvHeadDB.setDatv1(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatv1()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getDatb1())) {
			if (CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatb1()), ptrvHeadDB.getDatb1()) ){
				ptrvHeadDB.setDatb1(CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getDatb1()));
				requestPayload.setDataChanged(true);
			}
		}
		
		if (StringUtils.isNotBlank(requestPayload.getProcess())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getProcess(), ptrvHeadDB.getProcess())) {
				ptrvHeadDB.setProcess(requestPayload.getProcess());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (requestPayload.getLastPrevMonth() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getLastPrevMonth(), ptrvHeadDB.getLastPrevMonth())) {
				ptrvHeadDB.setLastPrevMonth(requestPayload.getLastPrevMonth());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (requestPayload.getLastThisMonth() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getLastThisMonth(), ptrvHeadDB.getLastThisMonth())) {
				ptrvHeadDB.setLastThisMonth(requestPayload.getLastThisMonth());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (requestPayload.getForm() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getForm().shortValue(), ptrvHeadDB.getForm())) {
				ptrvHeadDB.setForm(requestPayload.getForm().shortValue());
				requestPayload.setDataChanged(true);
			}
		}

		
		if (StringUtils.isNotBlank(requestPayload.getReason())) {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getReason(), ptrvHeadDB.getReason())) {
				ptrvHeadDB.setReason(requestPayload.getReason());
				requestPayload.setDataChanged(true);
			}
		}
		
		if (requestPayload.getTotalAmount() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getTotalAmount(), ptrvHeadDB.getTotalAmount())) {
				ptrvHeadDB.setTotalAmount(requestPayload.getTotalAmount());
				requestPayload.setDataChanged(true);
			}
		}
		
		JSONObject json = new JSONObject();
		if (ptrvHeadDB.getRejectAmounts() != null) {
			try {
			    json = new JSONObject(ptrvHeadDB.getRejectAmounts());
			} catch (Exception e) {
				// do nothing
			}
		}
		if (requestPayload.getPulseRejectAmount() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getPulseRejectAmount().doubleValue(), json.optDouble("CP01", 0))) {
				json.put("CP01", requestPayload.getPulseRejectAmount().doubleValue());
				requestPayload.setDataChanged(true);
			}
		}
		if (requestPayload.getFuelRejectAmount() != null) {
			if (CommonComparatorFunction.isDifferentNumberValues(requestPayload.getFuelRejectAmount().doubleValue(), json.optDouble("GA01", 0))) {
				json.put("GA01", requestPayload.getFuelRejectAmount().doubleValue());
				requestPayload.setDataChanged(true);
			}
		}
		ptrvHeadDB.setRejectAmounts(json.toString());
		
		// Update operation: set new dates and times
		if (requestPayload.isDataChanged()) {
			ptrvHeadDB.setDates(new Date());
			ptrvHeadDB.setTimes(new Date());
		}
		
		objectContainer = new RequestObjectComparatorContainer<>(ptrvHeadDB, requestPayload);	
				
	}
	
	JSONObject json = new JSONObject(requestPayload);
	objectContainer.setEventData(json.toString());
	
	return objectContainer;
}
}
