package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.CaOrder;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.query.CaOrderQueryService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.CaOrderResponseWrapper;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v3/ca_order")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CaOrderController extends AbstractResource {
	
	private EventLogCommandService eventLogCommandService;
	private HCMSEntityQueryService hcmsEntityQueryService;
	private UserQueryService userQueryService;
	private CommonServiceFactory commonServiceFactory;
	private CaOrderQueryService caOrderQueryService;

	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}

	@Autowired
	void setCaOrderQueryService(CaOrderQueryService caOrderQueryService) {
		this.caOrderQueryService = caOrderQueryService;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/order_id/{orderId}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<CaOrderResponseWrapper> findOneByOrderId(@PathVariable Long orderId)
			throws Exception {
		
		APIResponseWrapper<CaOrderResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			Optional<CaOrder> caOrderObject = caOrderQueryService.findById(Optional.ofNullable(orderId));
			
			if (!caOrderObject.isPresent()) {
				response.setMessage("No Data Found");
				response.setData(new CaOrderResponseWrapper(null));
			} else {
				/*
				ArrayList<CaOrder> listCaOrder = (ArrayList<CaOrder>) caOrderQueryService.findById(Optional.ofNullable(orderId));).
						stream().collect(Collectors.toList());
				CaReqDtl[] caReqDtl = listCaReqDtl.isEmpty() ? new CaReqDtl[0] : new CaReqDtl[listCaReqDtl.size()];
				List<CaReqHdr> listOfCaReqHdr = new ArrayList<>();
				listOfCaReqHdr.add(caReqHdrObject.get());
				updateDocumentStatus(listOfCaReqHdr);
				*/
				response.setData(new CaOrderResponseWrapper(caOrderObject.get()));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(CaOrderController.class).findOneByOrderId(orderId)).withSelfRel());
		return response;
	}

	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_criteria", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<CaOrderResponseWrapper>> findByCriteria(
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "orgeh", required = false) Long orgeh,
			@RequestParam(value = "glacctid", required = false) Long glAccountId,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {

		Page<CaOrderResponseWrapper> responsePage = new PageImpl<CaOrderResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<CaOrderResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<CaOrder> resultPage = null;
		String bukrs = null;
		Long pernr = null;
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> headRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_HEAD"));
		Optional<Role> directorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_DIRECTOR"));
		Optional<Role> gaRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_GA"));
		Optional<Role> accountingRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ACCOUNTING"));
		Optional<Role> treasuryRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_TREASURY"));
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		Optional<Role> superiorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_SUPERIOR"));
		
		try {
			if (orgeh != null && glAccountId != null) {
				resultPage = caOrderQueryService.findByCriteria(orgeh, glAccountId, pageNumber);
			}
			else if (orgeh != null) {
				if (orgeh == -1L) {
					resultPage = caOrderQueryService.fetchAllWithPaging(pageNumber);
				}
				else {
					resultPage = caOrderQueryService.findByOrgeh(orgeh, pageNumber);
				}
			}
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			
			if (resultPage.hasContent()) {

				List<CaOrderResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(caOrder -> {
					records.add(new CaOrderResponseWrapper(caOrder));
				});
				responsePage = new PageImpl<CaOrderResponseWrapper>(records,
						caOrderQueryService.createPageRequest(pageNumber == -1 ? 1 : pageNumber),
						resultPage.getTotalElements());
				response.setData(responsePage);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(CaOrderController.class).findByCriteria(ssn, orgeh, glAccountId, pageNumber)).withSelfRel());
		
		return response;
	}
}
