package com.abminvestama.hcms.rest.api.controller;



import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;
import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;
import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedTrxhdrCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedTrxitmCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedTrxhdrQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedTrxitmQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.MedTransactionRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.ZmedTrxhdrRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.ZmedTrxitmRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.ZmedTrxitmRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.MedPatientResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.MedTransactionResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ZmedTrxhdrResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ZmedTrxitmResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ZmedTrxitmResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.ZmedTrxhdrRequestBuilderUtil;
import com.abminvestama.hcms.rest.api.helper.ZmedTrxitmRequestBuilderUtil;


/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add/Modify createBulk method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/medical_transaction")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class MedTransactionController extends AbstractResource {
	
	private ZmedTrxitmCommandService zmedTrxitmCommandService;
	private ZmedTrxitmQueryService zmedTrxitmQueryService;
	private ZmedTrxitmRequestBuilderUtil zmedTrxitmRequestBuilderUtil;
	

	private ZmedTrxhdrCommandService zmedTrxhdrCommandService;
	private ZmedTrxhdrQueryService zmedTrxhdrQueryService;
	private ZmedTrxhdrRequestBuilderUtil zmedTrxhdrRequestBuilderUtil;
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	@Autowired
	void setZmedTrxitmCommandService(ZmedTrxitmCommandService zmedTrxitmCommandService) {
		this.zmedTrxitmCommandService = zmedTrxitmCommandService;
	}
	
	@Autowired
	void setZmedTrxitmQueryService(ZmedTrxitmQueryService zmedTrxitmQueryService) {
		this.zmedTrxitmQueryService = zmedTrxitmQueryService;
	}
	
	@Autowired
	void setZmedTrxitmRequestBuilderUtil(ZmedTrxitmRequestBuilderUtil zmedTrxitmRequestBuilderUtil) {
		this.zmedTrxitmRequestBuilderUtil = zmedTrxitmRequestBuilderUtil;
	}
	
	
	@Autowired
	void setZmedTrxhdrCommandService(ZmedTrxhdrCommandService zmedTrxhdrCommandService) {
		this.zmedTrxhdrCommandService = zmedTrxhdrCommandService;
	}
	
	@Autowired
	void setZmedTrxhdrQueryService(ZmedTrxhdrQueryService zmedTrxhdrQueryService) {
		this.zmedTrxhdrQueryService = zmedTrxhdrQueryService;
	}
	
	@Autowired
	void setZmedTrxhdrRequestBuilderUtil(ZmedTrxhdrRequestBuilderUtil zmedTrxhdrRequestBuilderUtil) {
		this.zmedTrxhdrRequestBuilderUtil = zmedTrxhdrRequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	
	
	
	
	

	
	
	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated //MedTransactionRequestWrapper request[]
			ZmedTrxitmRequestWrapper request[], BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		
		
		

		ArrayData<ZmedTrxitmResponseWrapper> bunchOfZmed = new ArrayData<>();
		APIResponseWrapper<ArrayData<ZmedTrxitmResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfZmed);
		
		//APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
		} else {
						
			Optional<HCMSEntity> zmedTrxitmEntity = hcmsEntityQueryService.findByEntityName(ZmedTrxitm.class.getSimpleName());
			if (!zmedTrxitmEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + ZmedTrxitm.class.getSimpleName() + "' !!!");
			}
			
			try {
				int size = request.length;
				//ZmedTrxitmRequestWrapper requestPayload[] = new ZmedTrxitmRequestWrapper [size];
				
				RequestObjectComparatorContainer<ZmedTrxitm, ZmedTrxitmRequestWrapper, String> newObjectContainer; 
					//=	new RequestObjectComparatorContainer<ZmedTrxitm, ZmedTrxitmRequestWrapper, String> [size];
				 
				

				List<ZmedTrxitmResponseWrapper> records = new ArrayList<>();
				
				Integer itmno = 1;
				//String lastItmno = zmedTrxitmQueryService.findLastId();
				for(int i = 0; i < size; i++){
					
					//auto increment from SAP
					//itmno = lastItmno.substring(0, lastItmno.length()-5)+(Integer.parseInt(lastItmno.substring(lastItmno.length()-5,lastItmno.length()-1))+1);
					
					request[i].setItmno(itmno.toString());
					newObjectContainer= zmedTrxitmRequestBuilderUtil.compareAndReturnUpdatedData(request[i], null);
					
										
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
					
					if (!currentUser.isPresent()) {
						throw new AuthorizationException(
								"Insufficient create privileges. Please login as Admin!");						
					}				
					
					Optional<ZmedTrxitm> savedEntity = newObjectContainer.getEntity();
					
					if (newObjectContainer.getEntity().isPresent()) {
					//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
						savedEntity = zmedTrxitmCommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}		
					
					if (!savedEntity.isPresent()) {
						throw new CannotPersistException("Cannot create ZmedTrxitm data. Please check your data!");
					}
					
					StringBuilder key = new StringBuilder("itmno=" + savedEntity.get().getId().getItmno());
					
					EventLog eventLog = new EventLog(zmedTrxitmEntity.get(), key.toString(), OperationType.CREATE, 
							(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					
					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}

					ZmedTrxitmResponseWrapper responseWrapper = new ZmedTrxitmResponseWrapper(savedEntity.get());
					
					records.add(new ZmedTrxitmResponseWrapper(savedEntity.get()));
					
					 itmno = itmno+1;
				}
					

				bunchOfZmed.setItems(records.toArray(new ZmedTrxitmResponseWrapper[records.size()]));
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(MedTransactionController.class).create(request, result)).withSelfRel());		
		return response;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated ZmedTrxitmRequestWrapper request[], BindingResult result) throws Exception {
		
		//APIResponseWrapper<?> response = null;
		ArrayData<ZmedTrxitmResponseWrapper> bunchOfZmed = new ArrayData<>();
		APIResponseWrapper<ArrayData<ZmedTrxitmResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfZmed);
		
		if (result.hasErrors()) {
			
		} else {
			boolean isDataChanged = false;
			
			int size = request.length;
			
			List<ZmedTrxitmResponseWrapper> records = new ArrayList<>();
			
			//ZmedTrxitmResponseWrapper responseWrapper = new ZmedTrxitmResponseWrapper(null);
			
			//response = new APIResponseWrapper<>(responseWrapper);
			
			Optional<User> currentUser = userQueryService.findByUsername(currentUser());
			
			if (!currentUser.isPresent()) {
				throw new AuthorizationException(
						"Insufficient create privileges. Please login as Admin!");						
			}	
			
			edit: {
				try {
					
					for(int i = 0; i < size; i++){
						
						Optional<ZmedTrxitm> zmedTrxitm = zmedTrxitmQueryService.findOneByCompositeKey(
								request[i].getItmno(), request[i].getClmno());
						if (!zmedTrxitm.isPresent()) {
							response.setMessage("Cannot update data. No Data Found!");
							break edit;
						}

						RequestObjectComparatorContainer<ZmedTrxitm, ZmedTrxitmRequestWrapper, String> updatedContainer = zmedTrxitmRequestBuilderUtil.compareAndReturnUpdatedData(request[i], zmedTrxitm.get());

						if (updatedContainer.getRequestPayload().isPresent()) {
							isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
						}

						if (!isDataChanged) {
							response.setMessage(response.getMessage() + "No data changed for item_number " + request[i].getItmno() + ". ");
							//break edit;
						} else {							
							if (updatedContainer.getEntity().isPresent()) {
								zmedTrxitm = zmedTrxitmCommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
							}
						}

						//responseWrapper = new ZmedTrxitmResponseWrapper(zmedTrxitm.get());
						//response = new APIResponseWrapper<>(responseWrapper);

						records.add(new ZmedTrxitmResponseWrapper(zmedTrxitm.get()));

					}

					bunchOfZmed.setItems(records.toArray(new ZmedTrxitmResponseWrapper[records.size()]));
					
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(MedTransactionController.class).edit(request, result)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE, value = "/bulk")
	public APIResponseWrapper<?> createBulk(@RequestBody  MedTransactionRequestWrapper request[] //@Validated
	, BindingResult result)
	throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		
		
		

		ArrayData<MedTransactionResponseWrapper> bunchOfZmed = new ArrayData<>();
		APIResponseWrapper<ArrayData<MedTransactionResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfZmed);
		
		if (result.hasErrors()) {
		} else {
						
			Optional<HCMSEntity> zmedTrxitmEntity = hcmsEntityQueryService.findByEntityName(ZmedTrxitm.class.getSimpleName());
			if (!zmedTrxitmEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + ZmedTrxitm.class.getSimpleName() + "' !!!");
			}
			
			Optional<HCMSEntity> zmedTrxhdrEntity = hcmsEntityQueryService.findByEntityName(ZmedTrxhdr.class.getSimpleName());
			if (!zmedTrxhdrEntity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + ZmedTrxhdr.class.getSimpleName() + "' !!!");
			}
			
			try {
				int size = request.length;
				RequestObjectComparatorContainer<ZmedTrxhdr, ZmedTrxhdrRequestWrapper, String> newObjectContainerHdr; 				
				RequestObjectComparatorContainer<ZmedTrxitm, ZmedTrxitmRequestWrapper, String> newObjectContainerItm; 
				 
				List<MedTransactionResponseWrapper> records = new ArrayList<>();				
				
				for(int j = 0; j<size; j++){
				
					ZmedTrxhdrRequestWrapper TrxHdr = request[j].getTrxHdr();
					newObjectContainerHdr= zmedTrxhdrRequestBuilderUtil.compareAndReturnUpdatedData(TrxHdr, null);						
					
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
					if (!currentUser.isPresent()) {
						throw new AuthorizationException(
								"Insufficient create privileges. Please login as Admin!");						
					}	
					
					Optional<ZmedTrxhdr> savedEntity = newObjectContainerHdr.getEntity();

					if (newObjectContainerHdr.getEntity().isPresent()) {
						//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
						savedEntity = zmedTrxhdrCommandService.save(newObjectContainerHdr.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}		

					if (!savedEntity.isPresent()) {
						throw new CannotPersistException("Cannot create ZmedTrxitm data. Please check your data!");
					}

					StringBuilder key = new StringBuilder("bukrs=" + savedEntity.get().getId().getBukrs());
					key.append(",zclmno=" + savedEntity.get().getId().getZclmno());
					
					EventLog eventLog = new EventLog(zmedTrxhdrEntity.get(), key.toString(), OperationType.CREATE, 
							(newObjectContainerHdr.getEventData().isPresent() ? newObjectContainerHdr.getEventData().get() : ""));
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());

					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}

					int itmno = 1;
					//String itmno = "";
					//String lastItmno = zmedTrxitmQueryService.findLastId();
					
					ZmedTrxitm savedEntityItems[] = new ZmedTrxitm[request[j].getTrxItm().length];
					
					for(int i = 0; i < size; i++){
						
						request[j].getTrxItm()[i].setItmno(String.valueOf(itmno));
						newObjectContainerItm= zmedTrxitmRequestBuilderUtil.compareAndReturnUpdatedData(request[j].getTrxItm()[i], null);

						Optional<ZmedTrxitm> savedEntityItm = newObjectContainerItm.getEntity();

						if (newObjectContainerItm.getEntity().isPresent()) {
							//	newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
							savedEntityItm = zmedTrxitmCommandService.save(newObjectContainerItm.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						}		

						if (!savedEntityItm.isPresent()) {
							throw new CannotPersistException("Cannot create ZmedTrxitm data. Please check your data!");
						}

						StringBuilder keyItm = new StringBuilder("itmno=" + savedEntityItm.get().getId().getItmno());
						keyItm.append(",clmno=" + savedEntityItm.get().getId().getClmno());

						EventLog eventLogItm = new EventLog(zmedTrxitmEntity.get(), keyItm.toString(), OperationType.CREATE, 
								(newObjectContainerItm.getEventData().isPresent() ? newObjectContainerItm.getEventData().get() : ""));
						Optional<EventLog> savedEventLogItm = eventLogCommandService.save(eventLogItm, currentUser.get());

						if (!savedEventLogItm.isPresent()) {
							throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
						}
						
						savedEntityItems[i] = savedEntityItm.get();

						itmno++;
					}
					
					records.add(new MedTransactionResponseWrapper(savedEntity.get(), savedEntityItems));
					
				}
				bunchOfZmed.setItems(records.toArray(new MedTransactionResponseWrapper[records.size()]));
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(MedTransactionController.class).createBulk(request, result)).withSelfRel());		
		return response;
	}
	
	
	
	
	
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}
	
	
}
