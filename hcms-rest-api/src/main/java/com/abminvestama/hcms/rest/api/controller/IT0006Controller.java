package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.exception.WorkflowNotFoundException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0006;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0006CommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0006QueryService;
import com.abminvestama.hcms.core.service.api.business.query.ITQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.helper.IT0006SoapObject;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0006RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ApprovalEventLogResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.IT0006ResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.IT0006RequestBuilderUtil;

/**
 * 
 * @since 1.0.0
 * @version 1.0.5
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add methods: findByCriteria</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: approve, reject, getUpdatedFields</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Modify edit method: update document only change the status</td></tr>
 *     <tr><td>1.0.2</td><td>Anasuya</td><td>Modify findOneByPernr into findByPernrAndSubty method</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add create, findByDocumentStatus method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/address")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class IT0006Controller extends AbstractResource {

	private IT0006CommandService it0006CommandService;
	private IT0006QueryService it0006QueryService;
	private IT0006RequestBuilderUtil it0006RequestBuilderUtil;

	private HCMSEntityQueryService hcmsEntityQueryService;//new
	private EventLogCommandService eventLogCommandService;//new
	
	private UserQueryService userQueryService;
	private Validator itValidator;
	private CommonServiceFactory commonServiceFactory;
	private ApprovalController approvalController;
	private ITQueryService itQueryService;
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	@Autowired
	void setIT0006CommandService(IT0006CommandService it0006CommandService) {
		this.it0006CommandService = it0006CommandService;
	}
	
	@Autowired
	void setIT0006QueryService(IT0006QueryService it0006QueryService) {
		this.it0006QueryService = it0006QueryService;
	}	
	
	@Autowired
	void setIT0006RequestBuilderUtil(IT0006RequestBuilderUtil it0006RequestBuilderUtil) {
		this.it0006RequestBuilderUtil = it0006RequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itValidator")
	public void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	@Autowired
	void setApprovalController(ApprovalController approvalController) {
		this.approvalController = approvalController;
	}
	
	@Autowired
	void setITQueryService(ITQueryService itQueryService) {
		this.itQueryService = itQueryService;
	}
	
	@SuppressWarnings("unchecked")
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_criteria", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<IT0006ResponseWrapper>> findByCriteria(
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "exclude_status", required = false, defaultValue="PUBLISHED,DRAFT_REJECTED,UPDATE_REJECTED") String excludeStatus,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {		

		Page<IT0006ResponseWrapper> responsePage = new PageImpl<IT0006ResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<IT0006ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<IT0006> resultPage = null;
		String bukrs = null;
		Long pernr = null;
		String[] excludes = excludeStatus.split(",");
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		
		if (StringUtils.isNotEmpty(ssn)) {
			pernr = Long.valueOf(ssn);
			if (!currentUser.get().getRoles().contains(adminRole.get()) &&
					!currentUser.get().getEmployee().getPernr().equals(pernr) ) {
				throw new AuthorizationException(
						"Insufficient Privileges. You can't fetch data of other Employee.");
			}
		} else {
			// no specified ssn, MUST BE ADMIN
			if (!currentUser.get().getRoles().contains(adminRole.get())) {
				throw new AuthorizationException(
						"Insufficient Privileges. Please login as 'ADMIN'!");
			}
			bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
		}
		
		try {
			resultPage = (Page<IT0006>) itQueryService.fetchDistinctByCriteriaWithPaging(pageNumber, bukrs, pernr, excludes,
					new IT0006(), currentUser.get().getEmployee());
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			
			if (resultPage.hasContent()) {

				List<IT0006ResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(it0006 -> {
					records.add(new IT0006ResponseWrapper(it0006));
				});
				responsePage = new PageImpl<IT0006ResponseWrapper>(records, it0006QueryService.createPageRequest(pageNumber),
						resultPage.getTotalElements());
				response.setData(responsePage);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0006Controller.class).findByCriteria(ssn, excludeStatus, pageNumber)).withSelfRel());
		
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.PUT, value = "/approve")
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> approve(
			@RequestHeader(required = true, value = "ssn") String pernr,
			@RequestHeader(required = true, value = "subtype") String subtype,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda)
					throws AuthorizationException, NumberFormatException, CannotPersistException,
					DataViolationException, EntityNotFoundException, WorkflowNotFoundException, Exception {
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", pernr);
			keyMap.put("infotype", SAPInfoType.ADDRESS_DETAILS.infoType());
			keyMap.put("subtype", subtype);
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			keyMap.put("muleService", BAPIFunction.IT0006_CREATE.service());
			keyMap.put("muleUri", BAPIFunction.IT0006_CREATE_URI.service());
			
			String className = IT0006.class.getSimpleName();
			response = approvalController.approve(keyMap, className, it0006QueryService, it0006CommandService,
					IT0006SoapObject.class, it0006RequestBuilderUtil, new IT0006RequestWrapper());

		} catch (EntityNotFoundException e) {
			throw e;
		} catch (WorkflowNotFoundException e) {
			throw e;
		} catch (DataViolationException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0006Controller.class).approve(pernr, subtype, endda, begda)).withSelfRel());				
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.PUT, value = "/reject")
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> reject(
			@RequestHeader(required = true, value = "ssn") String pernr,
			@RequestHeader(required = true, value = "subtype") String subtype,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda) 
					throws AuthorizationException, NumberFormatException, CannotPersistException, 
					DataViolationException, EntityNotFoundException, Exception {
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", pernr);
			keyMap.put("infotype", SAPInfoType.ADDRESS_DETAILS.infoType());
			keyMap.put("subtype", subtype);
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			
			String className = IT0006.class.getSimpleName();
			response = approvalController.reject(keyMap, className, it0006QueryService, it0006CommandService);
			
		} catch (AuthorizationException e) { 
			throw e;
		} catch (NumberFormatException e) { 
			throw e;
		} catch (CannotPersistException e) {
			throw e;
		} catch (DataViolationException e) {
			throw e;
		} catch (EntityNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0006Controller.class).reject(pernr, subtype, endda, begda)).withSelfRel());		
		return response;
	}
	
	@SuppressWarnings("unchecked")
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_updated_fields")
	public APIResponseWrapper<IT0006ResponseWrapper> getUpdatedFields(
			@RequestHeader(required = true, value = "ssn") String ssn,
			@RequestHeader(required = true, value = "subtype") String subtype,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda
			) throws DataViolationException, EntityNotFoundException, Exception {

		APIResponseWrapper<IT0006ResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", ssn);
			keyMap.put("infotype", SAPInfoType.ADDRESS_DETAILS.infoType());
			keyMap.put("subtype", subtype);
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			
			String className = IT0006.class.getSimpleName();
			response = (APIResponseWrapper<IT0006ResponseWrapper>) approvalController.getUpdatedFields(
					keyMap, className, it0006QueryService, response, IT0006ResponseWrapper.class);
			
		} catch (DataViolationException e) { 
			throw e;
		} catch (EntityNotFoundException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0006Controller.class).getUpdatedFields(ssn, subtype, endda, begda)).withSelfRel());
		return response;
	}
	
	/*@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({UserRole.AccessRole.USER_GROUP, UserRole.AccessRole.ADMIN_GROUP, UserRole.AccessRole.SUPERIOR_GROUP})
	public APIResponseWrapper<IT0006ResponseWrapper> findOneByPernr(@PathVariable String pernr) throws Exception {
		IT0006ResponseWrapper it0006Response = null;
		APIResponseWrapper<IT0006ResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0006> listOfIT0006 = it0006QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			
			if (listOfIT0006.isEmpty()) {
				it0006Response = new IT0006ResponseWrapper(null);
				response.setMessage("No Data Found");
			} else {
				IT0006 it0006 = listOfIT0006.get(0);
				it0006Response = new IT0006ResponseWrapper(it0006);
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.setData(it0006Response);
		response.add(linkTo(methodOn(IT0006Controller.class).findOneByPernr(pernr)).withSelfRel());
		return response;
	}*/
	
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}",
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0006ResponseWrapper>> findByPernrAndSubty(@PathVariable String pernr,
			@RequestParam(required = false) String status,
			@RequestParam(required = false, defaultValue = "1") String page, @RequestParam(required = false, defaultValue = "1") String subty) throws Exception {
		
		

		ArrayData<IT0006ResponseWrapper> bunchOfIT0006 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0006ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0006);
		
		
		/*IT0006ResponseWrapper it0006Response = null;
		APIResponseWrapper<IT0006ResponseWrapper> response = new APIResponseWrapper<>();
		*/
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			Page<IT0006> listOfIT0006 = it0006QueryService.findByPernrAndSubty(numOfPernr, status, page, subty);
			
			
			
			if (listOfIT0006==null || !listOfIT0006.hasContent()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0006ResponseWrapper> records = new ArrayList<>();
				
				listOfIT0006.forEach ( it0006 -> {
					records.add(new IT0006ResponseWrapper(it0006));
				});
				bunchOfIT0006.setItems(records.toArray(new IT0006ResponseWrapper[records.size()]));
			}
			
			
			
			
			
			/*if (listOfIT0006.hasContent()) {
				it0006Response = new IT0006ResponseWrapper(null);
				response.setMessage("No Data Found");
			} else {
				IT0006 it0006 = listOfIT0006.get(0);
				it0006Response = new IT0006ResponseWrapper(it0006);
			}*/
		} catch (NumberFormatException nfe) { 
			throw nfe;
		} catch (Exception e) {
			throw e;
		}
		response.add(linkTo(methodOn(IT0006Controller.class).findByPernrAndSubty(pernr, status, page, subty)).withSelfRel());
		
		return response;
		
		
		/*response.setData(it0006Response);
		response.add(linkTo(methodOn(IT0006Controller.class).findOneByPernrAndSubty(pernr, status, page, subty)).withSelfRel());
		return response;*/
	}
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated IT0006RequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			IT0006ResponseWrapper responseWrapper = new IT0006ResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {

					Optional<HCMSEntity> it0006Entity = hcmsEntityQueryService.findByEntityName(IT0006.class.getSimpleName());
					if (!it0006Entity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + IT0006.class.getSimpleName() + "' !!!");
					}	
					
					
					
					Optional<IT0006> it0006 = it0006QueryService.findOneByCompositeKey(request.getPernr(), request.getSubty(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
					if (!it0006.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					// since version 1.0.3
					IT0006 currentIT0006 = new IT0006();
					BeanUtils.copyProperties(it0006.get(), currentIT0006);
				
					RequestObjectComparatorContainer<IT0006, IT0006RequestWrapper, String> updatedContainer
					//= it0006RequestBuilderUtil.compareAndReturnUpdatedData(request, it0006.get());
					// since version 1.0.3
					= it0006RequestBuilderUtil.compareAndReturnUpdatedData(request, currentIT0006);
				
					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
					if (updatedContainer.getEntity().isPresent()) {
						//updatedContainer.getEntity().get().setSeqnr(it0006.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
						//it0006 = it0006CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						// since version 1.0.3, update document only change the status 
						if (it0006.get().getStatus() == DocumentStatus.INITIAL_DRAFT) {
							it0006.get().setStatus(DocumentStatus.UPDATED_DRAFT);
							currentIT0006.setStatus(DocumentStatus.UPDATED_DRAFT);
						} else if (it0006.get().getStatus() == DocumentStatus.PUBLISHED) {
							it0006.get().setStatus(DocumentStatus.UPDATE_IN_PROGRESS);
							currentIT0006.setStatus(DocumentStatus.UPDATE_IN_PROGRESS);
						}
					}
				
					if (!it0006.isPresent()) {
						throw new CannotPersistException("Cannot update IT0006 (Address) data. Please check your data!");
					}
					
					StringBuilder key = new StringBuilder("pernr=" + it0006.get().getId().getPernr());
					key.append(",infty=" + it0006.get().getId().getInfty());
					key.append(",subty=" + it0006.get().getId().getSubty());
					key.append(",endda=" + it0006.get().getId().getEndda());
					key.append(",begda=" + it0006.get().getId().getBegda());
					EventLog eventLog = new EventLog(it0006Entity.get(), key.toString(), OperationType.UPDATE, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					eventLog.setStatus(it0006.get().getStatus().toString());
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					
					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}
					
					//responseWrapper = new IT0006ResponseWrapper(it0006.get());
					// since version 1.0.3
					responseWrapper = new IT0006ResponseWrapper(currentIT0006);
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(IT0006Controller.class).edit(request, result)).withSelfRel());
		return response;
	}
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated IT0006RequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> it0006Entity = hcmsEntityQueryService.findByEntityName(IT0006.class.getSimpleName());
			if (!it0006Entity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + IT0006.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<IT0006, IT0006RequestWrapper, String> newObjectContainer 
					= it0006RequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				
				Optional<IT0006> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
					newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = it0006CommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create IT0006 (Bank Details) data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("pernr=" + savedEntity.get().getId().getPernr());
				key.append(",infty=" + savedEntity.get().getId().getInfty());
				key.append(",subty=" + savedEntity.get().getId().getSubty());
				key.append(",endda=" + CommonDateFunction.convertDateToStringYMD(savedEntity.get().getId().getEndda()));
				key.append(",begda=" + CommonDateFunction.convertDateToStringYMD(savedEntity.get().getId().getBegda()));
				EventLog eventLog = new EventLog(it0006Entity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				eventLog.setStatus(savedEntity.get().getStatus().toString());
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				
				IT0006ResponseWrapper responseWrapper = new IT0006ResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(IT0006Controller.class).create(request, result)).withSelfRel());		
		return response;
	}
	
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/status/{status}/page/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0006ResponseWrapper>> findByDocumentStatus(@PathVariable String status, @PathVariable int pageNumber) throws Exception {
		
		ArrayData<IT0006ResponseWrapper> bunchOfIT0006 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0006ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0006);
		
		try {
			Page<IT0006> listOfIT0006 = it0006QueryService.findByStatus(status, pageNumber);
			
			if (listOfIT0006==null || !listOfIT0006.hasContent()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0006ResponseWrapper> records = new ArrayList<>();
				
				listOfIT0006.forEach ( it0006 -> {
					records.add(new IT0006ResponseWrapper(it0006));
				});
				bunchOfIT0006.setItems(records.toArray(new IT0006ResponseWrapper[records.size()]));
			}
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0006Controller.class).findByDocumentStatus(status, pageNumber)).withSelfRel());
		
		return response;
	}
	
	/*
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0006ResponseWrapper>> findByPernr(@PathVariable String pernr, 
			@RequestParam(value = "status", required = false) String status, 
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		ArrayData<IT0006ResponseWrapper> bunchOfIT0006 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0006ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0006);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0006> listOfIT0006 = new ArrayList<>();
			
			if (StringUtils.isNotEmpty(status)) {
				Page<IT0006> data = it0006QueryService.findByPernrAndStatus(numOfPernr, status, pageNumber);
				if (data.hasContent()) {
					listOfIT0006 = data.getContent();
				}
			} else {
				listOfIT0006 = it0006QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			}
			
			if (listOfIT0006.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0006ResponseWrapper> records = new ArrayList<>();
				for (IT0006 it0006 : listOfIT0006) {
					records.add(new IT0006ResponseWrapper(it0006));
				}
				bunchOfIT0006.setItems(records.toArray(new IT0006ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0006Controller.class).findByPernr(pernr, status, pageNumber)).withSelfRel());
		return response;
	}	
	*/
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(itValidator);
	}
}