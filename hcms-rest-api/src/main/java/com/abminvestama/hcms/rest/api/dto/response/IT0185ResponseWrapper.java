package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0185;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Wrapper class for <strong>Personal IDs</strong> (i.e. IT0185 in SAP)
 *
 * @since 1.0.0
 * @version 1.0.4
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add Constructor of Read from EventData -> IT0185ResponseWrapper(IT0185, String, CommonServiceFactory)</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add attachmentType, attachmentTypeText, attachmentPath field</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add ictypText field</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add documentStatus field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 * 
 */
@JsonInclude(Include.NON_NULL)
public class IT0185ResponseWrapper extends ResourceSupport {
	
	private long pernr;
	private String subty;
	private String subtyText;
	private Date endda;
	private Date begda;
	private String ictyp;
	private String ictypText;
	private String icnum;
	private String auth1;
	private String fpdat;
	private String expid;
	private String isspl;
	private String iscot;
	private String iscotText;
	private String documentStatus;
	
	private String attachmentType;
	private String attachmentTypeText;
	private String attachmentPath;
	
	private IT0185ResponseWrapper() {}
	
	public IT0185ResponseWrapper(IT0185 it0185) {
		if (it0185 == null) {
			new IT0185ResponseWrapper();
		} else {
			this
				.setPernr(it0185.getId().getPernr())
				.setSubty(it0185.getSubty() != null ? 
						StringUtils.defaultString(it0185.getSubty().getId() != null ? it0185.getSubty().getId().getSubty() : StringUtils.EMPTY, 
								StringUtils.EMPTY) 
						: it0185.getId().getSubty())
				.setSubtyText(it0185.getSubty() != null ? StringUtils.defaultString(it0185.getSubty().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setEndda(it0185.getId().getEndda()).setBegda(it0185.getId().getBegda())
				.setIctyp(it0185.getIctyp() != null ? StringUtils.defaultString(it0185.getIctyp().getIctyp(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setIctypText(it0185.getIctyp() != null ? StringUtils.defaultString(it0185.getIctyp().getIctxt(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setIcnum(StringUtils.defaultString(it0185.getIcnum(), StringUtils.EMPTY))
				.setAuth1(StringUtils.defaultString(it0185.getAuth1(), StringUtils.EMPTY))
				.setFpdat(it0185.getFpdat() != null ? CommonDateFunction.convertDateToStringYMD(it0185.getFpdat()) : StringUtils.EMPTY)
				.setExpid(it0185.getExpid() != null ? CommonDateFunction.convertDateToStringYMD(it0185.getExpid()) : StringUtils.EMPTY)
				.setIsspl(StringUtils.defaultString(it0185.getIsspl(), StringUtils.EMPTY))
				.setIscot(it0185.getIscot() != null ? StringUtils.defaultString(it0185.getIscot().getLand1(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setIscotText(it0185.getIscot() != null ? StringUtils.defaultString(it0185.getIscot().getLandx(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentType(it0185.getAttachment() != null ? StringUtils.defaultString(it0185.getAttachment().getId().getSubty(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentTypeText(it0185.getAttachment() != null ? StringUtils.defaultString(it0185.getAttachment().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
				.setAttachmentPath(StringUtils.defaultString(it0185.getAttachmentPath(), StringUtils.EMPTY))
				.setDocumentStatus(it0185.getStatus());
		}
	}

	public IT0185ResponseWrapper(IT0185 currentData, String eventData, CommonServiceFactory serviceFactory) throws JSONException, Exception {
		this(currentData);
		IT0185ResponseWrapper itWrapper = (IT0185ResponseWrapper) new EventDataWrapper(eventData, this).getItWrapper();
		String[] ignoreProperties = EventDataWrapper.getNullPropertyNames(itWrapper);
		BeanUtils.copyProperties(itWrapper, this, ignoreProperties);
	}
	
	/**
	 * GET Employee SSN.
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	public IT0185ResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	@JsonProperty("subtype")
	public String getSubty() {
		return subty;
	}
	
	public IT0185ResponseWrapper setSubty(String subty) {
		this.subty = subty;
		return this;
	}
	
	/**
	 * GET Subtype Name/Text.
	 * 
	 * @return
	 */
	@JsonProperty("subtype_text")
	public String getSubtyText() {
		return subtyText;
	}
	
	public IT0185ResponseWrapper setSubtyText(String subtyText) {
		this.subtyText = subtyText;
		return this;
	}
	
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	public IT0185ResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}
	
	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	public IT0185ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * GET IC Type.
	 * 
	 */
	@JsonProperty("ic_type")
	public String getIctyp() {
		return ictyp;
	}
	
	public IT0185ResponseWrapper setIctyp(String ictyp) {
		this.ictyp = ictyp;
		return this;
	}
	
	/**
	 * GET IC Type.
	 * 
	 */
	@JsonProperty("ic_type_text")
	public String getIctypText() {
		return ictypText;
	}
	
	public IT0185ResponseWrapper setIctypText(String ictypText) {
		this.ictypText = ictypText;
		return this;
	}
	
	/**
	 * GET IC Number.
	 * 
	 * @return
	 */
	@JsonProperty("ic_number")
	public String getIcnum() {
		return icnum;
	}
	
	public IT0185ResponseWrapper setIcnum(String icnum) {
		this.icnum = icnum;
		return this;
	}
	
	/**
	 * GET Issuing Authority.
	 * 
	 * @return
	 */
	@JsonProperty("issuing_authority")
	public String getAuth1() {
		return auth1;
	}
	
	public IT0185ResponseWrapper setAuth1(String auth1) {
		this.auth1 = auth1;
		return this;
	}
	
	/**
	 * GET Issuing Date.
	 * 
	 * @return
	 */
	@JsonProperty("issuing_date")
	public String getFpdat() {
		return fpdat;
	}
	
	public IT0185ResponseWrapper setFpdat(String fpdat) {
		this.fpdat = fpdat;
		return this;
	}
	
	/**
	 * GET Expiry Date.
	 * 
	 * @return
	 */
	@JsonProperty("expiry_date")
	public String getExpid() {
		return expid;
	}
	
	public IT0185ResponseWrapper setExpid(String expid) {
		this.expid = expid;
		return this;
	}
	
	/**
	 * GET Place of Issue.
	 * 
	 * @return
	 */
	@JsonProperty("place_of_issue")
	public String getIsspl() {
		return isspl;
	}
	
	public IT0185ResponseWrapper setIsspl(String isspl) {
		this.isspl = isspl;
		return this;
	}
	
	/**
	 * GET Issuing Country.
	 * 
	 * @return
	 */
	@JsonProperty("country_of_issue")
	public String getIscot() {
		return iscot;
	}
	
	public IT0185ResponseWrapper setIscot(String iscot) {
		this.iscot = iscot;
		return this;
	}
	
	/**
	 * GET Issuing Country.
	 * 
	 * @return
	 */
	@JsonProperty("country_of_issue_text")
	public String getIscotText() {
		return iscotText;
	}
	
	public IT0185ResponseWrapper setIscotText(String iscotText) {
		this.iscotText = iscotText;
		return this;
	}
	
	/**
	 * GET Document Status.
	 * 
	 * @return
	 */
	@JsonProperty("document_status")
	public String getDocumentStatus() {
		return documentStatus;
	}
	
	public IT0185ResponseWrapper setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus.name();
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type")
	public String getAttachmentType() {
		return attachmentType;
	}
	
	public IT0185ResponseWrapper setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
		return this;
	}
	
	/**
	 * GET Attachment Type Text.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type_text")
	public String getAttachmentTypeText() {
		return attachmentTypeText;
	}
	
	public IT0185ResponseWrapper setAttachmentTypeText(String attachmentTypeText) {
		this.attachmentTypeText = attachmentTypeText;
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_path")
	public String getAttachmentPath() {
		return attachmentPath;
	}
	
	public IT0185ResponseWrapper setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
		return this;
	}
}