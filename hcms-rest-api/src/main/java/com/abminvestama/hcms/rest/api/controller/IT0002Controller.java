package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.exception.WorkflowNotFoundException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0002;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0002CommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0002QueryService;
import com.abminvestama.hcms.core.service.api.business.query.ITQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.helper.IT0002SoapObject;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0002RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ApprovalEventLogResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.DemographyCountResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.IT0002ResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.IT0002RequestBuilderUtil;

/**

 * TODO: Refactoring (i.e. Removes boilerplate code within create and update services)
 
 * @since 1.0.0
 * @version 1.0.5
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)<br>anasuya (anasuyahirai@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add methods: findByCriteria</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: approve, reject, getUpdatedFields</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Modify edit method: update document only change the status</td></tr>
 *     <tr><td>1.0.2</td><td>Anasuya</td><td>Add methods: countByGenderAndBukrs, countByAgeAndBukrs</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add postToSAP (IT0002SoapObject, MuleConsumerService)</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/personal_info")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class IT0002Controller extends AbstractResource {
	
	private IT0002CommandService it0002CommandService;
	private IT0002QueryService it0002QueryService;
	private IT0002RequestBuilderUtil it0002RequestBuilderUtil;
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	private MuleConsumerService muleConsumerService;
	private CommonServiceFactory commonServiceFactory;
	private ApprovalController approvalController;
	private ITQueryService itQueryService;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	@Autowired
	void setIT0002CommandService(IT0002CommandService it0002CommandService) {
		this.it0002CommandService = it0002CommandService;
	}
	
	@Autowired
	void setIT0002QueryService(IT0002QueryService it0002QueryService) {
		this.it0002QueryService = it0002QueryService;
	}
	
	@Autowired
	void setIT0002RequestBuilderUtil(IT0002RequestBuilderUtil it0002RequestBuilderUtil) {
		this.it0002RequestBuilderUtil = it0002RequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	@Autowired
	void setApprovalController(ApprovalController approvalController) {
		this.approvalController = approvalController;
	}
	
	@Autowired
	void setITQueryService(ITQueryService itQueryService) {
		this.itQueryService = itQueryService;
	}
	
	@SuppressWarnings("unchecked")
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_criteria", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<IT0002ResponseWrapper>> findByCriteria(
			@RequestParam(value = "ssn", required = false) String ssn,
			@RequestParam(value = "exclude_status", required = false, defaultValue="PUBLISHED,DRAFT_REJECTED,UPDATE_REJECTED") String excludeStatus,
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {		

		Page<IT0002ResponseWrapper> responsePage = new PageImpl<IT0002ResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<IT0002ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<IT0002> resultPage = null;
		String bukrs = null;
		Long pernr = null;
		String[] excludes = excludeStatus.split(",");
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		
		if (StringUtils.isNotEmpty(ssn)) {
			pernr = Long.valueOf(ssn);
			if (!currentUser.get().getRoles().contains(adminRole.get()) &&
					!currentUser.get().getEmployee().getPernr().equals(pernr) ) {
				throw new AuthorizationException(
						"Insufficient Privileges. You can't fetch data of other Employee.");
			}
		} else {
			// no specified ssn, MUST BE ADMIN
			if (!currentUser.get().getRoles().contains(adminRole.get())) {
				throw new AuthorizationException(
						"Insufficient Privileges. Please login as 'ADMIN'!");
			}
			bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
		}
		
		try {
			resultPage = (Page<IT0002>) itQueryService.fetchDistinctByCriteriaWithPaging(pageNumber, bukrs, pernr, excludes,
					new IT0002(), currentUser.get().getEmployee());
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			
			if (resultPage.hasContent()) {

				List<IT0002ResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(it0002 -> {
					records.add(new IT0002ResponseWrapper(it0002));
				});
				responsePage = new PageImpl<IT0002ResponseWrapper>(records, it0002QueryService.createPageRequest(pageNumber),
						resultPage.getTotalElements());
				response.setData(responsePage);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0002Controller.class).findByCriteria(ssn, excludeStatus, pageNumber)).withSelfRel());
		
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.PUT, value = "/approve")
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> approve(
			@RequestHeader(required = true, value = "ssn") String pernr,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda)
					throws AuthorizationException, NumberFormatException, CannotPersistException,
					DataViolationException, EntityNotFoundException, WorkflowNotFoundException, Exception {
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", pernr);
			keyMap.put("infotype", SAPInfoType.PERSONAL_DATA.infoType());
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			keyMap.put("muleService", BAPIFunction.IT0002_CREATE.service());
			keyMap.put("muleUri", BAPIFunction.IT0002_CREATE_URI.service());
			
			String className = IT0002.class.getSimpleName();
			response = approvalController.approve(keyMap, className, it0002QueryService, it0002CommandService,
					IT0002SoapObject.class, it0002RequestBuilderUtil, new IT0002RequestWrapper());

		} catch (EntityNotFoundException e) {
			throw e;
		} catch (WorkflowNotFoundException e) {
			throw e;
		} catch (DataViolationException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0002Controller.class).approve(pernr, endda, begda)).withSelfRel());				
		return response;
	}
	
	@Secured({AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.PUT, value = "/reject")
	public APIResponseWrapper<ApprovalEventLogResponseWrapper> reject(
			@RequestHeader(required = true, value = "ssn") String pernr,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda) 
					throws AuthorizationException, NumberFormatException, CannotPersistException, 
					DataViolationException, EntityNotFoundException, Exception {
		
		APIResponseWrapper<ApprovalEventLogResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", pernr);
			keyMap.put("infotype", SAPInfoType.PERSONAL_DATA.infoType());
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			
			String className = IT0002.class.getSimpleName();
			response = approvalController.reject(keyMap, className, it0002QueryService, it0002CommandService);
			
		} catch (AuthorizationException e) { 
			throw e;
		} catch (NumberFormatException e) { 
			throw e;
		} catch (CannotPersistException e) {
			throw e;
		} catch (DataViolationException e) {
			throw e;
		} catch (EntityNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0002Controller.class).reject(pernr, endda, begda)).withSelfRel());		
		return response;
	}
	
	@SuppressWarnings("unchecked")
	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/get_updated_fields")
	public APIResponseWrapper<IT0002ResponseWrapper> getUpdatedFields(
			@RequestHeader(required = true, value = "ssn") String ssn,
			@RequestHeader(required = true, value = "endda") String endda,
			@RequestHeader(required = true, value = "begda") String begda
			) throws DataViolationException, EntityNotFoundException, Exception {

		APIResponseWrapper<IT0002ResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			
			Map<String, String> keyMap = new HashMap<String, String>();
			keyMap.put("ssn", ssn);
			keyMap.put("infotype", SAPInfoType.PERSONAL_DATA.infoType());
			keyMap.put("endda", endda);
			keyMap.put("begda", begda);
			
			String className = IT0002.class.getSimpleName();
			response = (APIResponseWrapper<IT0002ResponseWrapper>) approvalController.getUpdatedFields(
					keyMap, className, it0002QueryService, response, IT0002ResponseWrapper.class);
			
		} catch (DataViolationException e) { 
			throw e;
		} catch (EntityNotFoundException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0002Controller.class).getUpdatedFields(ssn, endda, begda)).withSelfRel());
		return response;
	}
	
	
	
	/*
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<IT0002ResponseWrapper> findOneByPernr(@PathVariable String pernr) throws Exception {
		IT0002ResponseWrapper it0002Response = null;
		APIResponseWrapper<IT0002ResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0002> listOfIT0002 = it0002QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			
			if (listOfIT0002.isEmpty()) {
				it0002Response = new IT0002ResponseWrapper(null);
				response.setMessage("No Data Found");
			} else {
				IT0002 it0002 = listOfIT0002.get(0);
				it0002Response = new IT0002ResponseWrapper(it0002);
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.setData(it0002Response);
		response.add(linkTo(methodOn(IT0002Controller.class).findOneByPernr(pernr)).withSelfRel());
		return response;
	}		*/
	
	
	
	
	
	
	
	
	
	
	
	/*@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated IT0002RequestWrapper request, 
			BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
					ExceptionResponseWrapper.getErrors(result.getFieldErrors(), HttpMethod.POST)), "Validation error!");
		} else {
			IT0002ResponseWrapper responseWrapper = new IT0002ResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			Optional<User> currentUser = userQueryService.findByUsername(currentUser());			
			
			try {
				Optional<IT0002> it0002 = it0002QueryService.findOneByCompositeKey(request.getPernr(), 
						CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
				
				if (it0002.isPresent()) {
					// data already exist, thus do an update operation
					RequestObjectComparatorContainer<IT0002, IT0002RequestWrapper, String> updatedContainer 
						= it0002RequestBuilderUtil.compareAndReturnUpdatedData(request, it0002.get());
					
					if (updatedContainer.getEntity().isPresent()) {
						updatedContainer.getEntity().get().setSeqnr(it0002.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
						it0002 = it0002CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}
				
					if (!it0002.isPresent()) {
						throw new CannotPersistException("Data already exists and no changes on the data.");
					}
				
					responseWrapper = new IT0002ResponseWrapper(it0002.get());
					response = new APIResponseWrapper<>(responseWrapper);					
				} else {
					// create new IT0002
					ITCompositeKeysNoSubtype id = new ITCompositeKeysNoSubtype(request.getPernr(), 
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()), 
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
					IT0002 newIT0002 = new IT0002(id);
					//.newIT0002.set
				}
			} catch (Exception e) {
				throw e;
			}
		}
		
		return response;
	}*/
	
	
	
	
	//@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated IT0002RequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> it0002Entity = hcmsEntityQueryService.findByEntityName(IT0002.class.getSimpleName());
			if (!it0002Entity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + IT0002.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<IT0002, IT0002RequestWrapper, String> newObjectContainer 
					= it0002RequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient privileges. Please login!");						
				}
				
				Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
				if (currentUser.get().getEmployee().getPernr().longValue() != request.getPernr() && 
						!currentUser.get().getRoles().contains(adminRole.get())) {
					throw new AuthorizationException(
							"You don't have privilege to create/update other employee data. Only Admin can do that!");
				}
				
				Optional<IT0002> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
					newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = it0002CommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create IT0002 (Bank Details) data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("pernr=" + savedEntity.get().getId().getPernr());
				//key.append(",infty=" + savedEntity.get().getId().getInfty());
				//key.append(",subty=" + savedEntity.get().getId().getSubty());
				key.append(",endda=" + CommonDateFunction.convertDateToStringYMD(savedEntity.get().getId().getEndda()));
				key.append(",begda=" + CommonDateFunction.convertDateToStringYMD(savedEntity.get().getId().getBegda()));
				EventLog eventLog = new EventLog(it0002Entity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				eventLog.setStatus(savedEntity.get().getStatus().toString());
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				
				//IT0002SoapObject it0002SoapObject = new IT0002SoapObject(savedEntity.get()); 
				//muleConsumerService.postToSAP(BAPIFunction.IT0002_CREATE.service(), BAPIFunction.IT0002_CREATE_URI.service(), it0002SoapObject);
				
				IT0002ResponseWrapper responseWrapper = new IT0002ResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(IT0002Controller.class).create(request, result)).withSelfRel());		
		return response;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated IT0002RequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			IT0002ResponseWrapper responseWrapper = new IT0002ResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<HCMSEntity> it0002Entity = hcmsEntityQueryService.findByEntityName(IT0002.class.getSimpleName());
					if (!it0002Entity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + IT0002.class.getSimpleName() + "' !!!");
					}	
					
					
					Optional<IT0002> it0002 = it0002QueryService.findOneByCompositeKey(request.getPernr(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
					if (!it0002.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					// since version 1.0.3
					IT0002 currentIT0002 = new IT0002();
					BeanUtils.copyProperties(it0002.get(), currentIT0002);
				
					RequestObjectComparatorContainer<IT0002, IT0002RequestWrapper, String> updatedContainer =
							//it0002RequestBuilderUtil.compareAndReturnUpdatedData(request, it0002.get());
						    // since version 1.0.3
						    it0002RequestBuilderUtil.compareAndReturnUpdatedData(request, currentIT0002);

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
					
					if (!currentUser.isPresent()) {
						throw new AuthorizationException(
								"Insufficient privileges. Please login!");						
					}
					
					Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
					if (currentUser.get().getEmployee().getPernr().longValue() != request.getPernr() && 
							!currentUser.get().getRoles().contains(adminRole.get())) {
						throw new AuthorizationException(
								"You don't have privilege to create/update other employee data. Only Admin can do that!");
					}
				
					if (updatedContainer.getEntity().isPresent()) {
						//updatedContainer.getEntity().get().setSeqnr(it0002.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
						//it0002 = it0002CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
						// since version 1.0.3, update document only change the status 
						if (it0002.get().getStatus() == DocumentStatus.INITIAL_DRAFT) {
							it0002.get().setStatus(DocumentStatus.UPDATED_DRAFT);
							currentIT0002.setStatus(DocumentStatus.UPDATED_DRAFT);
						} else if (it0002.get().getStatus() == DocumentStatus.PUBLISHED) { 
							it0002.get().setStatus(DocumentStatus.UPDATE_IN_PROGRESS);
							currentIT0002.setStatus(DocumentStatus.UPDATE_IN_PROGRESS);
						}
					}
				
					if (!it0002.isPresent()) {
						throw new CannotPersistException("Cannot update IT0002 (Personal Information) data. Please check your data!");
					}
					
					
					
					StringBuilder key = new StringBuilder("pernr=" + it0002.get().getId().getPernr());
					//key.append(",infty=" + it0002.get().getId().getInfty());
					//key.append(",subty=" + it0002.get().getId().getSubty());
					key.append(",endda=" + it0002.get().getId().getEndda());
					key.append(",begda=" + it0002.get().getId().getBegda());
					EventLog eventLog = new EventLog(it0002Entity.get(), key.toString(), OperationType.UPDATE, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					eventLog.setStatus(it0002.get().getStatus().toString());
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					
					if (!savedEventLog.isPresent()) {
						throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
					}	
					
					
					
				
					//responseWrapper = new IT0002ResponseWrapper(it0002.get());
					// since version 1.0.3
					responseWrapper = new IT0002ResponseWrapper(currentIT0002);
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(IT0002Controller.class).edit(request, result)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0002ResponseWrapper>> findByPernrAndStatus(@PathVariable String pernr, 
			@RequestParam(value = "status", required = false) String status, 
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {
		ArrayData<IT0002ResponseWrapper> bunchOfIT0002 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0002ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0002);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0002> listOfIT0002 = new ArrayList<>();
			
			if (StringUtils.isNotEmpty(status)) {
				Page<IT0002> data = it0002QueryService.findByPernrAndStatus(numOfPernr, status, pageNumber);
				if (data.hasContent()) {
					listOfIT0002 = data.getContent();
				}
			} else {
				listOfIT0002 = it0002QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			}
			
			if (listOfIT0002.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0002ResponseWrapper> records = new ArrayList<>();
				for (IT0002 it0002 : listOfIT0002) {
					records.add(new IT0002ResponseWrapper(it0002));
				}
				bunchOfIT0002.setItems(records.toArray(new IT0002ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0002Controller.class).findByPernrAndStatus(pernr, status, pageNumber)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	
	
	
	
	
	@Secured({AccessRole.USER_GROUP, AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE,
			headers = "Accept=application/json", value = "/bukrs/{bukrs}")
	public APIResponseWrapper<DemographyCountResponseWrapper> countByGenderAndBukrs(
			@PathVariable String bukrs, 
			@RequestParam(value = "gender", required = false) String gesch ) throws Exception {
		
		APIResponseWrapper<DemographyCountResponseWrapper> response = new APIResponseWrapper<>();
		//response.setData(employeePositionResponses);
		
		try {
			//Long numOfPernr = Long.valueOf(pernr);
			
			Optional<User> currentUser = userQueryService.findByUsername(currentUser());
			
			if (!currentUser.isPresent()) {
				throw new UsernameNotFoundException("No User Logged-in!");
			}
			
			Long count = it0002QueryService.countByGenderAndBukrs(gesch, bukrs);
			
			if (count == null) {
				response.setMessage("No Data Found");
			} else {
				response.setData(new DemographyCountResponseWrapper( count));								
			}
		} catch (NumberFormatException e) { 
			throw e;
		} catch (UsernameNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0002Controller.class).countByGenderAndBukrs(bukrs,gesch)).withSelfRel());		
		return response;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Secured({AccessRole.USER_GROUP, AccessRole.SUPERIOR_GROUP, AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE,
			headers = "Accept=application/json", value = "/bukrs/{bukrs}/begda/{dat1}/endda/{dat2}")
	public APIResponseWrapper<DemographyCountResponseWrapper> countByAgeAndBukrs(
			@PathVariable String bukrs, 
			@PathVariable String dat1,
			@PathVariable String dat2) throws Exception {
		
		APIResponseWrapper<DemographyCountResponseWrapper> response = new APIResponseWrapper<>();
		//response.setData(employeePositionResponses);
		
		try {
			//Long numOfPernr = Long.valueOf(pernr);
			
			Optional<User> currentUser = userQueryService.findByUsername(currentUser());
			
			if (!currentUser.isPresent()) {
				throw new UsernameNotFoundException("No User Logged-in!");
			}
			
			Long count = it0002QueryService.countByAgeAndBukrs(dat1, dat2, bukrs);
			
			if (count == null) {
				response.setMessage("No Data Found");
			} else {
				response.setData(new DemographyCountResponseWrapper( count));								
			}
		} catch (NumberFormatException e) { 
			throw e;
		} catch (UsernameNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT0002Controller.class).countByAgeAndBukrs(bukrs,dat1,dat2)).withSelfRel());		
		return response;
	}
	
	
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(itValidator);
	}
}