package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.ZmedQuotype;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class ZmedQuotypeResponseWrapper extends ResourceSupport {

	private Date datab;
	private Date datbi;
	
	private String bukrs;
	private String kodequo;
	private String persen;
	private String descr;
	
	private ZmedQuotypeResponseWrapper() {}
	
	public ZmedQuotypeResponseWrapper(ZmedQuotype zmedQuotype) {
		if (zmedQuotype == null) {
			new ZmedQuotypeResponseWrapper();
		} else {
			this
			//.setBukrs(zmedQuotype.getBukrs() != null ? StringUtils.defaultString(zmedQuotype.getBukrs().getBukrs(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setDatab(zmedQuotype.getDatab())
			.setDatbi(zmedQuotype.getDatbi())
			.setBukrs(StringUtils.defaultString(zmedQuotype.getBukrs().getBukrs(), StringUtils.EMPTY))
			.setKodequo(StringUtils.defaultString(zmedQuotype.getKodequo(), StringUtils.EMPTY))
			.setPersen(StringUtils.defaultString(zmedQuotype.getPersen().toString(), StringUtils.EMPTY))
			.setDescr(StringUtils.defaultString(zmedQuotype.getDescr(), StringUtils.EMPTY));			
		}
	}
	


	
	
	@JsonProperty("company_code")
	public String getBukrs() {
		return bukrs;
	}
	
	private ZmedQuotypeResponseWrapper setBukrs(String bukrs) {
		this.bukrs = bukrs;
		return this;
	}
	


	
	
	@JsonProperty("quota_type")
	public String getKodequo() {
		return kodequo;
	}
	
	private ZmedQuotypeResponseWrapper setKodequo(String kodequo) {
		this.kodequo = kodequo;
		return this;
	}
	


	
	
	@JsonProperty("persen")
	public String getPersen() {
		return persen;
	}
	
	private ZmedQuotypeResponseWrapper setPersen(String persen) {
		this.persen = persen;
		return this;
	}
	
	
	@JsonProperty("description")
	public String getDescr() {
		return descr;
	}
	
	private ZmedQuotypeResponseWrapper setDescr(String descr) {
		this.descr = descr;
		return this;
	}
	


	
	
	@JsonProperty("valid_from_date")
	public Date getDatab() {
		return datab;
	}
	
	private ZmedQuotypeResponseWrapper setDatab(Date datab) {
		this.datab = datab;
		return this;
	}
	


	
	
	@JsonProperty("valid_to_date")
	public Date getDatbi() {
		return datbi;
	}
	
	private ZmedQuotypeResponseWrapper setDatbi(Date datbi) {
		this.datbi = datbi;
		return this;
	}
}

