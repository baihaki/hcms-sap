package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.CaReqDtl;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class CaReqDtlResponseWrapper extends ResourceSupport {
	
	private Short itemNo;
	private String reqNo;
	private String description;
	private Double pricePerUnit;
	private Integer quantity;
	private Double amount;
	private String remarks;
	private String createdByUsername;
	private String createdByFullname;
	private Date createdAt;
	private String updatedByUsername;
	private String updatedByFullname;
	private Date updatedAt;
	
	private CaReqDtlResponseWrapper() {}
	
	public CaReqDtlResponseWrapper(CaReqDtl caReqDtl) {
		if (caReqDtl == null) {
			new CaReqDtlResponseWrapper();
		} else {
			this
			.setItemNo(caReqDtl.getId().getItemNo())
			.setReqNo(caReqDtl.getId().getReqNo())		
			.setDescription(caReqDtl.getDescription() != null ? StringUtils.defaultString(caReqDtl.getDescription(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setPricePerUnit(caReqDtl.getPricePerUnit() != null ? caReqDtl.getPricePerUnit() : 0)
			.setQuantity(caReqDtl.getQuantity() != null ? caReqDtl.getQuantity() : 0)	
			.setAmount(caReqDtl.getAmount() != null ? caReqDtl.getAmount() : 0)
			.setRemarks(StringUtils.defaultString(caReqDtl.getRemarks(), StringUtils.EMPTY))
			.setCreatedByUsername(caReqDtl.getCreatedBy() != null ? caReqDtl.getCreatedBy().getUsername() : StringUtils.EMPTY)
			.setCreatedByFullname(caReqDtl.getCreatedBy() != null ? caReqDtl.getCreatedBy().getEmployee().getEname() : StringUtils.EMPTY)
			.setCreatedAt(caReqDtl.getCreatedAt())
			.setUpdatedByUsername(caReqDtl.getUpdatedBy() != null ? caReqDtl.getUpdatedBy().getUsername() : StringUtils.EMPTY)
			.setUpdatedByFullname(caReqDtl.getUpdatedBy() != null ? caReqDtl.getUpdatedBy().getEmployee().getEname() : StringUtils.EMPTY)
			.setUpdatedAt(caReqDtl.getUpdatedAt());
		}
	}

	@JsonProperty("req_no")
	public String getReqNo() {
		return reqNo;
	}
	
	private CaReqDtlResponseWrapper setReqNo(String reqNo) {
		this.reqNo = reqNo;
		return this;
	}
	
	@JsonProperty("item_no")
	public Short getItemNo() {
		return itemNo;
	}
	
	private CaReqDtlResponseWrapper setItemNo(Short itemNo) {
		this.itemNo = itemNo;
		return this;
	}	
	
	@JsonProperty("amount")
	public Double getAmount() {
		return amount;
	}
	
	private CaReqDtlResponseWrapper setAmount(Double amount) {
		this.amount = amount;
		return this;
	}
	
	@JsonProperty("price_per_unit")
	public Double getPricePerUnit() {
		return pricePerUnit;
	}
	
	private CaReqDtlResponseWrapper setPricePerUnit(Double pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
		return this;
	}
	
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}
	
	private CaReqDtlResponseWrapper setDescription(String description) {
		this.description = description;
		return this;
	}
	
	@JsonProperty("remarks")
	public String getRemarks() {
		return remarks;
	}
	
	private CaReqDtlResponseWrapper setRemarks(String remarks) {
		this.remarks = remarks;
		return this;
	}
	
	@JsonProperty("quantity")
	public Integer getQuantity() {
		return quantity;
	}
	
	private CaReqDtlResponseWrapper setQuantity(Integer quantity) {
		this.quantity = quantity;
		return this;
	}

	@JsonProperty("created_by_username")
	public String getCreatedByUsername() {
		return createdByUsername;
	}

	public CaReqDtlResponseWrapper setCreatedByUsername(String createdByUsername) {
		this.createdByUsername = createdByUsername;
		return this;
	}

	@JsonProperty("created_by_fullname")
	public String getCreatedByFullname() {
		return createdByFullname;
	}

	public CaReqDtlResponseWrapper setCreatedByFullname(String createdByFullname) {
		this.createdByFullname = createdByFullname;
		return this;
	}

	@JsonProperty("created_at")
	public Date getCreatedAt() {
		return createdAt;
	}

	public CaReqDtlResponseWrapper setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	@JsonProperty("updated_by_username")
	public String getUpdatedByUsername() {
		return updatedByUsername;
	}

	public CaReqDtlResponseWrapper setUpdatedByUsername(String updatedByUsername) {
		this.updatedByUsername = updatedByUsername;
		return this;
	}

	@JsonProperty("updated_by_fullname")
	public String getUpdatedByFullname() {
		return updatedByFullname;
	}

	public CaReqDtlResponseWrapper setUpdatedByFullname(String updatedByFullname) {
		this.updatedByFullname = updatedByFullname;
		return this;
	}

	@JsonProperty("updated_at")
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public CaReqDtlResponseWrapper setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
		return this;
	}
	
}

