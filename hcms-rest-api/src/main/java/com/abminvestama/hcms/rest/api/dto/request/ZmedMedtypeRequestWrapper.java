package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ZmedMedtypeRequestWrapper extends ITRequestWrapper {

	
	private static final long serialVersionUID = -2317050437190237970L;
	
	private String bukrs;
	private String kodemed;
	private String datab;
	private String datbi;
	private String descr;
	
	@JsonProperty("company_code")
	public String getBukrs() {
		return bukrs;
	}
	
	
	@JsonProperty("medical_type")
	public String getKodemed() {
		return kodemed;
	}
	
	
	
	@JsonProperty("valid_from_date")
	public String getDatab() {
		return datab;
	}
	
	
	@JsonProperty("valid_to_date")
	public String getDatbi() {
		return datbi;
	}
	
	
	@JsonProperty("description")
	public String getDescr() {
		return descr;
	}
}