package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.entity.CSKT;
import com.abminvestama.hcms.core.service.api.business.query.CSKTQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;

/**
 * 
 * @author yauritux@gmail.com
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@RestController
@RequestMapping("/api/v1/cskt")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CSKTController extends AbstractResource {

	private CSKTQueryService csktQueryService;
	
	@Autowired
	void setCSKTQueryService(CSKTQueryService csktQueryService) {
		this.csktQueryService = csktQueryService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/page/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<CSKT>> fetchPage(@PathVariable int pageNumber) throws Exception {

		Page<CSKT> resultPage = null;

		resultPage = csktQueryService.fetchAllWithPaging(pageNumber);

		APIResponseWrapper<Page<CSKT>> response = new APIResponseWrapper<>();
		response.setData(resultPage);
		Link self = linkTo(methodOn(CSKTController.class).fetchPage(pageNumber)).withSelfRel();
		response.add(self);

		return response;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/empcskt/page/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<CSKT>> fetchAllEmpCSKTPage(@PathVariable int pageNumber,
			@RequestParam(value = "page_size", required = true, defaultValue = "10") int pageSize) throws Exception {
		
		Page<CSKT> resultPage = null;
		
		resultPage = csktQueryService.fetchAllEmpCSKTPage(pageNumber, pageSize);
		
		APIResponseWrapper<Page<CSKT>> response = new APIResponseWrapper<>();
		response.setData(resultPage);
		Link self = linkTo(methodOn(CSKTController.class).fetchPage(pageNumber)).withSelfRel();
		response.add(self);
		
		return response;
	}
}