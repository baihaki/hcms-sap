package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public class CaReqDtlRequestWrapper extends ITRequestWrapper {
	
	private static final long serialVersionUID = 7683212456742212483L;
	
	private Short itemNo;
	private String reqNo;
	private String description;
	private Double pricePerUnit;
	private Integer quantity;
	private Double amount;
	private String remarks;

	public void setItemNo(Short itemNo) {
		this.itemNo = itemNo;
	}
	
	public void setReqNo(String reqNo) {
		this.reqNo = reqNo;
	}

	@JsonProperty("item_no")
	public Short getItemNo() {
		return itemNo;
	}

	@JsonProperty("req_no")
	public String getReqNo() {
		return reqNo;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("price_per_unit")
	public Double getPricePerUnit() {
		return pricePerUnit;
	}

	@JsonProperty("quantity")
	public Integer getQuantity() {
		return quantity;
	}

	@JsonProperty("amount")
	public Double getAmount() {
		return amount;
	}

	@JsonProperty("remarks")
	public String getRemarks() {
		return remarks;
	}
	
}