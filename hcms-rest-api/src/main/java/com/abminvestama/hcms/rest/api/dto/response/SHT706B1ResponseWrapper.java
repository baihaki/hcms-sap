package com.abminvestama.hcms.rest.api.dto.response;

import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.SHT706B1;
import com.abminvestama.hcms.rest.api.model.constant.HCMSResourceIdentifier;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SHT706B1ResponseWrapper extends ResourceSupport {
	
	public static final String RESOURCE = HCMSResourceIdentifier.EDUCATION_OR_TRAINING.label();

	private String morei;
	private String spkzl;
	private String sptxt;
	
	SHT706B1ResponseWrapper() {}
	
	public SHT706B1ResponseWrapper(SHT706B1 sht706b1) {
		this.morei = sht706b1.getMorei();
		this.spkzl = sht706b1.getSpklz();
		this.sptxt = sht706b1.getSptxt();
	}
	
	@JsonProperty("morei")
	public String getMorei() {
		return morei;
	}
	
	@JsonProperty("spkzl")
	public String getSpkzl() {
		return spkzl;
	}
	
	@JsonProperty("sptxt")
	public String getStext() {
		return sptxt;
	}
}