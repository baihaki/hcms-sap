package com.abminvestama.hcms.rest.api.validator;

import java.util.Collection;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.abminvestama.hcms.common.util.CommonTextValidator;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.SAPSynchronizeEntity;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.query.IT0001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.RoleQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.request.UserRequestWrapper;

/**
 * 
 * Bean validator for some SAP InfoType objects. Conform to Bean Validation 1.0 (JSR-303) 
 * and Bean Validation 1.1 (JSR-349) Validation Framework.
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Remove validation of "Cannot find SSN"</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 * 
 */
@Component("userValidator")
public class UserValidator implements Validator {
	
	private UserQueryService userQueryService;
	private RoleQueryService roleQueryService;
	private IT0001QueryService it0001QueryService;
	
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	void setRoleQueryService(RoleQueryService roleQueryService) {
		this.roleQueryService = roleQueryService;
	}
	
	@Autowired
	void setIT0001QueryService(IT0001QueryService it0001QueryService) { 
		this.it0001QueryService = it0001QueryService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return UserRequestWrapper.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		final UserRequestWrapper request = (UserRequestWrapper) target;
		if (StringUtils.isBlank(request.getUsername())) {
			errors.rejectValue("username", null, "'username' is missing from the payload!");
		} else {
			if (!request.isEditRequest()) {
				Optional<User> user = userQueryService.findByUsername(Optional.ofNullable(request.getUsername()));
				if (user.isPresent()) {
					errors.rejectValue("username", null, "Duplicate 'username' " + request.getUsername() + "!");
				}
			}
		}
		if (StringUtils.isBlank(request.getEmail())) {
			errors.rejectValue("email", null, "'email' is missing from the payload!");
		} else {
			if (!CommonTextValidator.validateEmail(request.getEmail())) {
				errors.rejectValue("email", null, "Invalid 'email'!");
			}
		}
		
		if (!request.isEditRequest()) {
			JSONObject jsonObject = new JSONObject(request);
			if (jsonObject.has("ssn") && jsonObject.isNull("ssn")) {
				errors.rejectValue("ssn", null, "'Employee SSN' is missing from the payload!");
			} // Remove validation of "Cannot find SSN"
			/* else {
				Collection<IT0001> it0001Records = it0001QueryService.findByPernr(request.getSsn());
				if (it0001Records.isEmpty()) {
					errors.rejectValue("ssn", null, "Cannot find SSN '" + request.getSsn() + "'!");
				}
			}*/
						
			if (StringUtils.isBlank("password")) {
				errors.rejectValue("password", null, "'password' is missing from the payload!");
			} else {
				if (!request.getPassword().equals(request.getConfirmPassword())) {
					errors.rejectValue("confirm_password", "both password and confirm_password should be equal!");
				}
			}
				
			for (String roleName : request.getRoleNames()) {
				Optional<Role> role = roleQueryService.findByName(Optional.ofNullable(roleName));
				if (!role.isPresent()) {
					errors.rejectValue("roleNames", null, "Cannot find Role '" + roleName + "'!");
				}
			}			
		}	 	
	}
}