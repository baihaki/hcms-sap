package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.entity.T517T;
import com.abminvestama.hcms.core.model.entity.T519T;
import com.abminvestama.hcms.core.model.entity.T517A;
import com.abminvestama.hcms.core.model.entity.T519T;
import com.abminvestama.hcms.core.service.api.business.query.T517TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T517AQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T519TQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add method: findBySchoolType</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/certificate")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class T519TController extends AbstractResource {

	private T519TQueryService t519tQueryService;
	private T517TQueryService t517tQueryService;
	private T517AQueryService t517aQueryService;
	
	@Autowired
	void setT591TQueryService(T519TQueryService t519tQueryService) {
		this.t519tQueryService = t519tQueryService;
	}
	
	@Autowired
	void setT517TQueryService(T517TQueryService t517tQueryService) {
		this.t517tQueryService = t517tQueryService;
	}
	
	@Autowired
	void setT517AQueryService(T517AQueryService t517aQueryService) {
		this.t517aQueryService = t517aQueryService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/page/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<T519T>> fetchPage(@PathVariable int pageNumber, 
			@RequestParam(required = true, value = "username", defaultValue = "") String username,
			@RequestParam(required = true, value = "auth_token", defaultValue = "") String authToken) throws Exception {
		
		if (authToken == null || authToken.isEmpty()) {
			throw new Exception("No token provided");
		}
		
		Page<T519T> resultPage = t519tQueryService.findAllWithPaging(pageNumber);
		
		APIResponseWrapper<Page<T519T>> response = new APIResponseWrapper<>();
		response.setData(resultPage);
		Link self = linkTo(methodOn(T519TController.class).fetchPage(pageNumber, username, authToken)).withSelfRel();
		response.add(self);
		
		return response;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/school_type/{slart}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<T519T>> findBySchoolType(@PathVariable String slart) throws Exception {
		ArrayData<T519T> bunchOfT519T = new ArrayData<>();
		APIResponseWrapper<ArrayData<T519T>> response = new APIResponseWrapper<>();
		response.setData(bunchOfT519T);
		
		try {
			List<T517A> listOfT517A = new ArrayList<>();
			
			Optional<T517T> t517t = t517tQueryService.findById(Optional.ofNullable(slart));
			if (t517t.isPresent()) {
				listOfT517A = t517aQueryService.findBySlart(t517t.get()).stream().collect(Collectors.toList());
			}
			
			if (listOfT517A.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<T519T> records = new ArrayList<>();
				for (T517A t517a : listOfT517A) {
					records.add(t517a.getSlabs());
				}
				bunchOfT519T.setItems(records.toArray(new T519T[records.size()])); 
			}
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(T519TController.class).findBySchoolType(slart)).withSelfRel());
		return response;
	}
}