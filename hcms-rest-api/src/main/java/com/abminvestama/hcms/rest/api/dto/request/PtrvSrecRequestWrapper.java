package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add fields: startVolume, endVolume, sumVolume</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add fields: morei, subExpType</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 * 
 */
public class PtrvSrecRequestWrapper extends ITRequestWrapper {

	private static final long serialVersionUID = -2317050437190237970L;

	private String mandt;
	private long reinr;
	private int perio;
	private String receiptno;
	
	private String expType;
	private Double recAmount;
	private String recCurr;
	private Double recRate;
	private Double locAmount;
	private String locCurr;
	private String recDate;
	private String shorttxt;
	private String paperReceipt;
	
	private String morei;
	private String subExpType;
	
	private Double startVolume;
	private Double endVolume;
	private Double sumVolume;

	public void setReceiptno(String receiptno) {
		this.receiptno = receiptno;
	}
	
	@JsonProperty("mandt")
	public String getMandt() {
		return mandt;
	}

	@JsonProperty("reinr")
	public long getReinr() {
		return reinr;
	}

	@JsonProperty("perio")
	public int getPerio() {
		return perio;
	}

	@JsonProperty("receiptno")
	public String getReceiptno() {
		return receiptno;
	}

	@JsonProperty("exp_type")
	public String getExpType() {
		return expType;
	}

	@JsonProperty("rec_amount")
	public Double getRecAmount() {
		return recAmount;
	}

	@JsonProperty("rec_curr")
	public String getRecCurr() {
		return recCurr;
	}

	@JsonProperty("rec_rate")
	public Double getRecRate() {
		return recRate;
	}

	@JsonProperty("loc_amount")
	public Double getLocAmount() {
		return locAmount;
	}

	@JsonProperty("loc_curr")
	public String getLocCurr() {
		return locCurr;
	}

	@JsonProperty("rec_date")
	public String getRecDate() {
		return recDate;
	}

	@JsonProperty("shorttxt")
	public String getShorttxt() {
		return shorttxt;
	}

	@JsonProperty("paper_receipt")
	public String getPaperReceipt() {
		return paperReceipt;
	}

	@JsonProperty("morei")
	public String getMorei() {
		return morei;
	}

	@JsonProperty("sub_exp_type")
	public String getSubExpType() {
		return subExpType;
	}

	@JsonProperty("start_volume")
	public Double getStartVolume() {
		return startVolume;
	}

	@JsonProperty("end_volume")
	public Double getEndVolume() {
		return endVolume;
	}

	@JsonProperty("sum_volume")
	public Double getSumVolume() {
		return sumVolume;
	}

}