package com.abminvestama.hcms.rest.api.validator;

import java.util.Collection;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.Position;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.PositionQueryService;
import com.abminvestama.hcms.rest.api.dto.request.ApprovalEventLogRequestWrapper;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Component("approvalEventLogValidator")
public class ApprovalEventLogValidator implements Validator {

	private HCMSEntityQueryService hcmsEntityQueryService;
	
	private PositionQueryService positionQueryService;
	
	private IT0001QueryService it0001QueryService;
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setPositionQueryService(PositionQueryService positionQueryService) {
		this.positionQueryService = positionQueryService;
	}
	
	@Autowired
	void setIT0001QueryService(IT0001QueryService it0001QueryService) {
		this.it0001QueryService = it0001QueryService;
	}
	
	@Override
	public boolean supports(Class<?> clazz) {
		return ApprovalEventLogRequestWrapper.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		final ApprovalEventLogRequestWrapper request = (ApprovalEventLogRequestWrapper) target;
		
		if (StringUtils.isEmpty(request.getEntityId())) {
			errors.rejectValue("entity_id", null, "'entity_id' is missing!");
		} else {
			try {
				Optional<HCMSEntity> hcmsEntity = hcmsEntityQueryService.findById(Optional.of(request.getEntityId()));
				if (!hcmsEntity.isPresent()) {
					errors.rejectValue("entity_id", null, "Cannot find entity with ID '" + request.getEntityId() + "'!");
				}
			} catch (Exception e) {
				errors.rejectValue("entity_id", null, e.getMessage());
			}
		}
		
		if (StringUtils.isEmpty(request.getEntityObjectId())) {
			errors.rejectValue("entity_object_id", null, "'entity_object_id' is missing!");
		} 
		
		if (StringUtils.isEmpty(request.getSubordinateSSN())) {
			errors.rejectValue("subordinate_ssn", null, "'subordinate_ssn' is missing!");
		} else {
			try {
				Long pernr = Long.valueOf(request.getSubordinateSSN());
				Collection<IT0001> it0001Records = it0001QueryService.findByPernr(pernr);
				if (it0001Records == null || it0001Records.isEmpty()) {
					errors.rejectValue("subordinate_ssn", null, "Cannot find ssn '" + request.getSubordinateSSN() + "'!");
				}
			} catch (NumberFormatException e) {
				errors.rejectValue("subordinate_ssn", null, e.getMessage());
			}
		}
		
		if (StringUtils.isEmpty(request.getPerformedBySSN())) {
			errors.rejectValue("performed_by_ssn", null, "'performed_by_ssn' is missing!");
		} else {
			try {
				Long pernr = Long.valueOf(request.getPerformedBySSN());
				Collection<IT0001> it0001Records = it0001QueryService.findByPernr(pernr);
				if (it0001Records == null || it0001Records.isEmpty()) {
					errors.rejectValue("approved_by_ssn", null, "Cannot find ssn '" + request.getPerformedBySSN() + "'!");
				}
			} catch (NumberFormatException e) {
				errors.rejectValue("approved_by_ssn", null, e.getMessage());
			}
		}
		
		if (StringUtils.isEmpty(request.getPerformedByPositionId())) {
			errors.rejectValue("performed_by_position_id", null, "'performed_by_position_id' is missing!");
		} else {
			try {
				Optional<Position> position = positionQueryService.findById(Optional.ofNullable(request.getPerformedByPositionId()));
				if (!position.isPresent()) {
					errors.rejectValue("performed_by_position_id", null, "Cannot find position ID '" + request.getPerformedByPositionId() + "'!");
				}
			} catch (Exception e) {
				errors.rejectValue("performed_by_position_id", null, e.getMessage());
			}
		}
	}
}