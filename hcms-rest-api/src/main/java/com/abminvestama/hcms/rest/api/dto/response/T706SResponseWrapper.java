package com.abminvestama.hcms.rest.api.dto.response;

import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.T706S;
import com.abminvestama.hcms.rest.api.model.constant.HCMSResourceIdentifier;
import com.fasterxml.jackson.annotation.JsonProperty;

public class T706SResponseWrapper extends ResourceSupport {
	
	public static final String RESOURCE = HCMSResourceIdentifier.EDUCATION_OR_TRAINING.label();

	private String morei;
	private String schem;
	private String stext;
	
	T706SResponseWrapper() {}
	
	public T706SResponseWrapper(T706S t706s) {
		this.morei = t706s.getMorei();
		this.schem = t706s.getSchem();
		this.stext = t706s.getStext();
	}
	
	@JsonProperty("morei")
	public String getMorei() {
		return morei;
	}
	
	@JsonProperty("schem")
	public String getSchem() {
		return schem;
	}
	
	@JsonProperty("stext")
	public String getStext() {
		return stext;
	}
}