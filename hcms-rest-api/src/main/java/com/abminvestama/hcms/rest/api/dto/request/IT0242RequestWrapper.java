package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add attach1Type, attach1Content, attach1Filename field</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public class IT0242RequestWrapper extends ITRequestWrapper {
	
	private static final long serialVersionUID = -1610416889288981311L;
	
	private String jamId;
	private String marst;

	private String attach1Type;
	private String attach1Content;
	private String attach1Filename;
	
	@JsonProperty("jam_id")
	public String getJamId() {
		return jamId;
	}
	
	@JsonProperty("marital_status")
	public String getMarst() {
		return marst;
	}
	
	@JsonProperty("attachment1_type")
	public String getAttach1Type() {
		return attach1Type;
	}
	
	@JsonProperty("attachment1_base64")
	public String getAttach1Content() {
		return attach1Content;
	}
	
	@JsonProperty("attachment1_filename")
	public String getAttach1Filename() {
		return attach1Filename;
	}
}