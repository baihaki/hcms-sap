package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.IT0019;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * Wrapper Class (VO) for IT0019 (Monitoring of Tasks).
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add attachmentType, attachmentTypeText, attachmentPath field</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add fields: text1, text2, text3, termn</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class IT0019ResponseWrapper extends ResourceSupport {

	private long pernr;	
	private String subty;
	private String subtyText;
	private Date endda;
	private Date begda;
	
	private long seqnr;
	private String itxex;
	private String tmart;
	private String tmtxt;
	private Date termn;
	private Date mndat;
	private String bvmrk;
	private int tmjhr;
	private int tmmon;
	private int tmtag;
	private int mnjhr;
	private int mnmon;
	private int mntag;

	private String text1;
	private String text2;
	private String text3;
	
	private String attachmentType;
	private String attachmentTypeText;
	private String attachmentPath;
	
	private IT0019ResponseWrapper() {}
	
	public IT0019ResponseWrapper(IT0019 it0019) {
		if (it0019 == null) {
			new IT0019ResponseWrapper();
		} else {
			this
			.setPernr(it0019.getId().getPernr())
			.setSubty(it0019.getSubty() != null ? 
					StringUtils.defaultString(it0019.getSubty().getId() != null ? it0019.getSubty().getId().getSubty() : StringUtils.EMPTY) : it0019.getId().getSubty())
			.setSubtyText(it0019.getSubty() != null ? StringUtils.defaultString(it0019.getSubty().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setEndda(it0019.getId().getEndda()).setBegda(it0019.getId().getBegda())
			.setSeqnr(it0019.getSeqnr() != null ? it0019.getSeqnr().longValue() : 0L)
			.setItxex(StringUtils.defaultString(it0019.getItxex(), StringUtils.EMPTY))
			.setTmart(it0019.getTmart() != null ? StringUtils.defaultString(it0019.getTmart().getTmart(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setTmtxt(it0019.getTmart() != null ? StringUtils.defaultString(it0019.getTmart().getTmtxt(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setTermn(it0019.getTermn())
			.setMndat(it0019.getMndat())
			.setBvmrk(StringUtils.defaultString(it0019.getBvmrk(), StringUtils.EMPTY))
			.setTmjhr(it0019.getTmjhr() != null ? it0019.getTmjhr().intValue() : 0)
			.setTmmon(it0019.getTmmon() != null ? it0019.getTmmon().intValue() : 0)
			.setTmtag(it0019.getTmtag() != null ? it0019.getTmtag().intValue() : 0)
			.setMnjhr(it0019.getMnjhr() != null ? it0019.getMnjhr().intValue() : 0)
			.setMnmon(it0019.getMnmon() != null ? it0019.getMnmon().intValue() : 0)
			.setText1(StringUtils.defaultString(it0019.getText1(), StringUtils.EMPTY))
			.setText2(StringUtils.defaultString(it0019.getText2(), StringUtils.EMPTY))
			.setText3(StringUtils.defaultString(it0019.getText3(), StringUtils.EMPTY))
			.setAttachmentType(it0019.getAttachment() != null ? StringUtils.defaultString(it0019.getAttachment().getId().getSubty(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setAttachmentTypeText(it0019.getAttachment() != null ? StringUtils.defaultString(it0019.getAttachment().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setAttachmentPath(StringUtils.defaultString(it0019.getAttachmentPath(), StringUtils.EMPTY))
			.setMntag(it0019.getMntag() != null ? it0019.getMntag().intValue() : 0);
		}
	}
	
	/**
	 * GET Employee SSN.
	 * 
	 * @return
	 */
	public long getPernr() {
		return pernr;
	}
	
	private IT0019ResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	public String getSubty() {
		return subty;
	}
	
	private IT0019ResponseWrapper setSubty(String subty) {
		this.subty = subty;
		return this;
	}
	
	/**
	 * GET Subtype Name/Text.
	 * 
	 * @return
	 */
	public String getSubtyText() {
		return subtyText;
	}
	
	private IT0019ResponseWrapper setSubtyText(String subtyText) {
		this.subtyText = subtyText;
		return this;
	}
	
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	public Date getEndda() {
		return endda;
	}
	
	private IT0019ResponseWrapper setEndda(Date endda) {
		this.endda = endda; 
		return this;
	}
	
	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	public Date getBegda() {
		return begda;
	}
	
	private IT0019ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * GET infotype record no.
	 * 
	 * @return
	 */
	public long getSeqnr() {
		return seqnr;
	}
	
	private IT0019ResponseWrapper setSeqnr(long seqnr) {
		this.seqnr = seqnr;
		return this;
	}
	
	/**
	 * GET Text Exists.
	 * 
	 * @return
	 */
	public String getItxex() {
		return itxex;
	}
	
	private IT0019ResponseWrapper setItxex(String itxex) {
		this.itxex = itxex;
		return this;
	}
	
	/**
	 * GET Task Type.
	 * 
	 * @return
	 */
	public String getTmart() {
		return tmart;
	}
	
	private IT0019ResponseWrapper setTmart(String tmart) {
		this.tmart = tmart;
		return this;
	}
	
	/**
	 * GET Task Type Name.
	 * 
	 * @return
	 */
	public String getTmtxt() {
		return tmtxt;
	}
	
	private IT0019ResponseWrapper setTmtxt(String tmtxt) {
		this.tmtxt = tmtxt;
		return this;
	}
	
	/**
	 * GET Date of Task.
	 * 
	 * @return
	 */
	public Date getTermn() {
		return termn;
	}
	
	private IT0019ResponseWrapper setTermn(Date termn) {
		this.termn = termn;
		return this;
	}
	
	/**
	 * GET Reminder Date.
	 * 
	 * @return
	 */
	public Date getMndat() {
		return mndat;
	}
	
	private IT0019ResponseWrapper setMndat(Date mndat) {
		this.mndat = mndat;
		return this;
	}
	
	/**
	 * GET Processing Indicator.
	 * 
	 * @return
	 */
	public String getBvmrk() {
		return bvmrk;
	}
	
	private IT0019ResponseWrapper setBvmrk(String bvmrk) {
		this.bvmrk = bvmrk;
		return this;
	}
	
	/**
	 * GET Year of Date.
	 * 
	 * @return
	 */
	public int getTmjhr() {
		return tmjhr;
	}
	
	private IT0019ResponseWrapper setTmjhr(int tmjhr) {
		this.tmjhr = tmjhr;
		return this;
	}
	
	/**
	 * GET Month of Date.
	 * 
	 * @return
	 */
	public int getTmmon() {
		return tmmon;
	}
	
	private IT0019ResponseWrapper setTmmon(int tmmon) {
		this.tmmon = tmmon;
		return this;
	}
	
	/**
	 * GET Day of Date.
	 * 
	 * @return
	 */
	public int getTmtag() {
		return tmtag;
	}
	
	private IT0019ResponseWrapper setTmtag(int tmtag) {
		this.tmtag = tmtag;
		return this;
	}
	
	/**
	 * GET Year of Reminder.
	 * 
	 * @return
	 */
	public int getMnjhr() {
		return mnjhr;
	}
	
	private IT0019ResponseWrapper setMnjhr(int mnjhr) {
		this.mnjhr = mnjhr;
		return this;
	}
	
	/**
	 * GET Month of Reminder.
	 * 
	 * @return
	 */
	public int getMnmon() {
		return mnmon;
	}
	
	private IT0019ResponseWrapper setMnmon(int mnmon) {
		this.mnmon = mnmon;
		return this;
	}
	
	/**
	 * GET Day of Reminder.
	 * 
	 * @return
	 */
	public int getMntag() {
		return mntag;
	}
	
	private IT0019ResponseWrapper setMntag(int mntag) {
		this.mntag = mntag;
		return this;
	}
	
	/**
	 * GET Value Text.
	 * 
	 * @return
	 */
	@JsonProperty("comment1")
	public String getText1() {
		return text1;
	}
	
	private IT0019ResponseWrapper setText1(String text1) {
		this.text1 = text1;
		return this;
	}
	
	/**
	 * GET Value Text.
	 * 
	 * @return
	 */
	@JsonProperty("comment2")
	public String getText2() {
		return text2;
	}
	
	private IT0019ResponseWrapper setText2(String text2) {
		this.text2 = text2;
		return this;
	}
	
	/**
	 * GET Value Text.
	 * 
	 * @return
	 */
	@JsonProperty("comment3")
	public String getText3() {
		return text3;
	}
	
	private IT0019ResponseWrapper setText3(String text3) {
		this.text3 = text3;
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type")
	public String getAttachmentType() {
		return attachmentType;
	}
	
	private IT0019ResponseWrapper setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
		return this;
	}
	
	/**
	 * GET Attachment Type Text.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_type_text")
	public String getAttachmentTypeText() {
		return attachmentTypeText;
	}
	
	private IT0019ResponseWrapper setAttachmentTypeText(String attachmentTypeText) {
		this.attachmentTypeText = attachmentTypeText;
		return this;
	}
	
	/**
	 * GET Attachment Type.
	 * 
	 * @return
	 */
	@JsonProperty("attachment1_path")
	public String getAttachmentPath() {
		return attachmentPath;
	}
	
	private IT0019ResponseWrapper setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
		return this;
	}
}