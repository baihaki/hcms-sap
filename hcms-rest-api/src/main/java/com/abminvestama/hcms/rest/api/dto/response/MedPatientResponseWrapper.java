package com.abminvestama.hcms.rest.api.dto.response;


import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.IT0021;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class MedPatientResponseWrapper extends ResourceSupport {

	private String name;
	
	private MedPatientResponseWrapper() {}
	
	public MedPatientResponseWrapper(IT0001 zmed) {
		if (zmed == null) {
			new MedPatientResponseWrapper();
		} else {
			this
			.setName(StringUtils.defaultString(zmed.getEname(), StringUtils.EMPTY));			
		}
	}
	
	public MedPatientResponseWrapper(IT0021 zmed) {
		if (zmed == null) {
			new MedPatientResponseWrapper();
		} else {
			this
			.setName(StringUtils.defaultString(zmed.getFcnam(), StringUtils.EMPTY));			
		}
	}
		
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	
	private MedPatientResponseWrapper setName(String name) {
		this.name = name;
		return this;
	}
}
