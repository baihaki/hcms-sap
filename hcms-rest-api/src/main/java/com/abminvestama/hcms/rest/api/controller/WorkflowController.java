package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.exception.WorkflowNotFoundException;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.Workflow;
import com.abminvestama.hcms.core.service.api.business.command.WorkflowCommandService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.api.business.query.WorkflowQueryService;
import com.abminvestama.hcms.rest.api.dto.request.WorkflowRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.AuthorResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.WorkflowResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@RestController
@RequestMapping("/api/v2/workflows")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class WorkflowController extends AbstractResource {

	private WorkflowCommandService workflowCommandService;
	private WorkflowQueryService workflowQueryService;
	
	private UserQueryService userQueryService;
	
	private Validator workflowValidator;
	
	@Autowired
	void setWorkflowCommandService(WorkflowCommandService workflowCommandService) {
		this.workflowCommandService = workflowCommandService;
	}
	
	@Autowired
	void setWorkflowQueryService(WorkflowQueryService workflowQueryService) {
		this.workflowQueryService = workflowQueryService;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("workflowValidator")
	void setWorkflowValidator(Validator workflowValidator) {
		this.workflowValidator = workflowValidator;
	}
	
	@Secured({AccessRole.USER_GROUP, AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<WorkflowResponseWrapper> fetchById(@PathVariable String id) throws WorkflowNotFoundException, Exception {
		APIResponseWrapper<WorkflowResponseWrapper> response = new APIResponseWrapper<>();
		
		try {			
			Optional<Workflow> workflow = workflowQueryService.findById(Optional.ofNullable(id));
			if (!workflow.isPresent()) {
				throw new WorkflowNotFoundException("WorkflowType ID '" + id + "' does not exist!");
			} 
				
			response.setData(new WorkflowResponseWrapper(workflow.get()));
		} catch (WorkflowNotFoundException e) { 
			throw e;
		} catch (Exception e) {
			throw e;
		}
		
		Link self = linkTo(methodOn(WorkflowController.class).fetchById(id)).withSelfRel();
		response.add(self);				
		
		return response;
	}
	
	@Secured({AccessRole.USER_GROUP, AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/page/{pageNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<List<WorkflowResponseWrapper>> fetchPage(@PathVariable int pageNumber) throws Exception {
		Page<Workflow> resultPage = workflowQueryService.fetchAllWithPaging(pageNumber);
		
		List<WorkflowResponseWrapper> records = new ArrayList<>();
		
		APIResponseWrapper<List<WorkflowResponseWrapper>> response = new APIResponseWrapper<>();
		
		try {
			resultPage.forEach(workflow -> {
	
				try {
					records.add(
						new WorkflowResponseWrapper(workflow).setCreatedBy(
							linkTo(methodOn(UserController.class).getUserProfileById(workflow.getCreatedById()))
							.withRel(AuthorResponseWrapper.RESOURCE)
						).setUpdatedBy(
							linkTo(methodOn(UserController.class).getUserProfileById(workflow.getUpdatedById()))
							.withRel(AuthorResponseWrapper.RESOURCE)
						)
					);
				} catch (Exception ignored) {
				}
			});
		} catch (Exception e) {
			throw e;
		}
		
		response.setData(records);
		Link self = linkTo(methodOn(WorkflowController.class).fetchPage(pageNumber)).withSelfRel();
		response.add(self);		
		
		return response;
	}
	
	@Secured({AccessRole.ADMIN_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated WorkflowRequestWrapper workflowRequest, 
			BindingResult result) throws Exception {
		
		if (result.hasErrors()) {
			return new APIResponseWrapper<>(
					new ArrayData<>(ExceptionResponseWrapper.getErrors(result.getFieldErrors(), HttpMethod.POST)),
					"Validation Error!");
		}
		
		APIResponseWrapper<?> response = null;
		
		Optional<User> currentUser = userQueryService.findByUsername(currentUser());	
		
		try {
			if (!currentUser.isPresent()) {
				throw new UsernameNotFoundException("No User logged in!");
			}
			
			Workflow workflow = new Workflow();
			workflow.setName(workflowRequest.getName());
			workflow.setDescription(workflowRequest.getDescription());
			
			Optional<Workflow> savedEntity = workflowCommandService.save(workflow, currentUser.get());
			
			WorkflowResponseWrapper workflowResponseWrapper = new WorkflowResponseWrapper(workflow)
					.setCreatedBy(currentUser.get(), 
						linkTo(methodOn(UserController.class).getUserProfileByUsername(currentUser.get().getUsername()))
						.withRel(AuthorResponseWrapper.RESOURCE))
					.setUpdatedBy(currentUser.get(),
						linkTo(methodOn(UserController.class).getUserProfileByUsername(currentUser.get().getUsername()))
						.withRel(AuthorResponseWrapper.RESOURCE));
			response = new APIResponseWrapper<WorkflowResponseWrapper>(workflowResponseWrapper);
			response.setMessage("Successfully created new workflow '" + workflowRequest.getName() + "', WorkflowType ID = '" + 
					savedEntity.get().getId() + "'");
			
			response.add(linkTo(methodOn(WorkflowController.class).create(workflowRequest, result)).withSelfRel());									
		} catch (Exception e) {
			throw e;
		}
		
		return response;
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(workflowValidator);
	}	
}