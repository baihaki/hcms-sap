package com.abminvestama.hcms.rest.api.dto.response;

import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.T705P;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class T705PResponseWrapper extends ResourceSupport {
	private String pmbde;
	private String satza;
	private String ddtext;
	
	private T705PResponseWrapper() {}
	
	public T705PResponseWrapper(T705P t705p) {
		if (t705p == null) {
			new T705PResponseWrapper();
		} else {
			this
				.setPmbde(t705p.getPmbde())
				.setSatza(t705p.getSatza())
				.setDdtext(t705p.getDdtext());
		}
	}

	@JsonProperty("pmbde")
	public String getPmbde() {
		return pmbde;
	}

	public T705PResponseWrapper setPmbde(String pmbde) {
		this.pmbde = pmbde;
		return this;
	}

	@JsonProperty("satza")
	public String getSatza() {
		return satza;
	}

	public T705PResponseWrapper setSatza(String satza) {
		this.satza = satza;
		return this;
	}

	@JsonProperty("ddtext")
	public String getDdtext() {
		return ddtext;
	}

	public T705PResponseWrapper setDdtext(String ddtext) {
		this.ddtext = ddtext;
		return this;
	}

}
