package com.abminvestama.hcms.rest.api.dto.response;

import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.T518B;
import com.abminvestama.hcms.rest.api.model.constant.HCMSResourceIdentifier;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class T518BResponseWrapper extends ResourceSupport {
	
	public static final String RESOURCE = HCMSResourceIdentifier.EDUCATION_OR_TRAINING.label();

	private long ausbi;
	private String atext;
	
	T518BResponseWrapper() {}
	
	public T518BResponseWrapper(T518B t518b) {
		this.ausbi = t518b.getAusbi();
		this.atext = t518b.getAtext();
	}
	
	@JsonProperty("code")
	public long getAusbi() {
		return ausbi;
	}
	
	@JsonProperty("education_or_training")
	public String getAtext() {
		return atext;
	}
}