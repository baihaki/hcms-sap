package com.abminvestama.hcms.rest.api.dto.response;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.Phdat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class PhdatResponseWrapper extends ResourceSupport {

	private String pinco;	// public holiday type
	private String phdtxt;	// public holiday type description
	private String erdat;	// public holiday date
	private String phtxt;	// public holiday description

	private PhdatResponseWrapper() {
	}

	public PhdatResponseWrapper(Phdat phdat) {
		if (phdat == null) {
			new PhdatResponseWrapper();
		} else {
			String DEFAULT_FORMAT_DATE = "yyyy-MM-dd";
			DateFormat formatDate = new SimpleDateFormat(DEFAULT_FORMAT_DATE);

			this.setPinco(StringUtils.defaultString(phdat.getPinco()))
				.setPhdtxt(StringUtils.defaultString(phdat.getPhdtxt()))
				.setErdat(phdat.getId() != null && phdat.getId().getErdat() != null ? formatDate.format(phdat.getId().getErdat()) : "")
				.setPhtxt(StringUtils.defaultString(phdat.getPhtxt()));
		}
	}

	/**
	 * GET public holiday type.
	 * 
	 * @return
	 */
	@JsonProperty("pinco")
	public String getPinco() {
		return pinco;
	}

	public PhdatResponseWrapper setPinco(String pinco) {
		this.pinco = pinco;
		return this;
	}

	/**
	 * GET public holiday type description.
	 * 
	 * @return
	 */
	public String getPhdtxt() {
		return phdtxt;
	}

	public PhdatResponseWrapper setPhdtxt(String phdtxt) {
		this.phdtxt = phdtxt;
		return this;
	}

	/**
	 * GET public holiday date.
	 * 
	 * @return
	 */
	public String getErdat() {
		return erdat;
	}

	public PhdatResponseWrapper setErdat(String erdat) {
		this.erdat = erdat;
		return this;
	}

	/**
	 * GET public holiday description.
	 * 
	 * @return
	 */
	public String getPhtxt() {
		return phtxt;
	}

	public PhdatResponseWrapper setPhtxt(String phtxt) {
		this.phtxt = phtxt;
		return this;
	}

}