package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.ZmedHeaderPost;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class ZmedHeaderPostResponseWrapper extends ResourceSupport {

	
	private Date docDate;
	private Date pstngDate;
	private Date transDate;
	private Date vatdate;
	
	private String objType;
	private String objKey;
	private String objSys;
	private String busAct;
	private String username;
	private String headerTxt;
	private String compCode;
	private String fiscYear;
	private String fisPeriod;
	private String docType;
	private String refDocNo;
	private String acDocNo;
	private String objKeyR;
	private String reasonRev;
	private String compoAcc;
	private String refDocNoLong;
	private String accPrinciple;
	private String negPosting;
	private String objKeyInv;
	private String billCategory;
	private String logsys;
	
	
private ZmedHeaderPostResponseWrapper() {}
	
	public ZmedHeaderPostResponseWrapper(ZmedHeaderPost zmedHeaderPost) {
		if (zmedHeaderPost == null) {
			new ZmedHeaderPostResponseWrapper();
		} else {
			this
			//.setCompCode(zmedHeaderPost.getCompCode() != null ? StringUtils.defaultString(zmedHeaderPost.getCompCode().getBukrs(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setDocDate(zmedHeaderPost.getDocDate())
			.setPstngDate(zmedHeaderPost.getPstngDate())
			.setTransDate(zmedHeaderPost.getTransDate())
			.setVatdate(zmedHeaderPost.getVatdate())
			.setCompCode(StringUtils.defaultString(zmedHeaderPost.getCompCode().getBukrs(), StringUtils.EMPTY))
			.setObjType(StringUtils.defaultString(zmedHeaderPost.getObjType(), StringUtils.EMPTY))
			.setObjKey(StringUtils.defaultString(zmedHeaderPost.getObjKey(), StringUtils.EMPTY))
			.setObjSys(StringUtils.defaultString(zmedHeaderPost.getObjSys(), StringUtils.EMPTY))
			.setBusAct(StringUtils.defaultString(zmedHeaderPost.getBusAct(), StringUtils.EMPTY))
			.setUsername(StringUtils.defaultString(zmedHeaderPost.getUsername(), StringUtils.EMPTY))
			.setHeaderTxt(StringUtils.defaultString(zmedHeaderPost.getHeaderTxt(), StringUtils.EMPTY))
			.setFiscYear(StringUtils.defaultString(zmedHeaderPost.getFiscYear().toString(), StringUtils.EMPTY))
			.setFisPeriod(StringUtils.defaultString(zmedHeaderPost.getFisPeriod().toString(), StringUtils.EMPTY))
			.setDocType(StringUtils.defaultString(zmedHeaderPost.getDocType(), StringUtils.EMPTY))
			.setRefDocNo(StringUtils.defaultString(zmedHeaderPost.getRefDocNo(), StringUtils.EMPTY))
			.setAcDocNo(StringUtils.defaultString(zmedHeaderPost.getAcDocNo(), StringUtils.EMPTY))
			.setObjKeyR(StringUtils.defaultString(zmedHeaderPost.getObjKeyR(), StringUtils.EMPTY))
			.setReasonRev(StringUtils.defaultString(zmedHeaderPost.getReasonRev(), StringUtils.EMPTY))
			.setCompoAcc(StringUtils.defaultString(zmedHeaderPost.getCompoAcc(), StringUtils.EMPTY))
			.setRefDocNoLong(StringUtils.defaultString(zmedHeaderPost.getRefDocNoLong(), StringUtils.EMPTY))
			.setAccPrinciple(StringUtils.defaultString(zmedHeaderPost.getAccPrinciple(), StringUtils.EMPTY))
			.setNegPosting(StringUtils.defaultString(zmedHeaderPost.getNegPosting(), StringUtils.EMPTY))
			.setObjKeyInv(StringUtils.defaultString(zmedHeaderPost.getObjKeyInv(), StringUtils.EMPTY))
			.setLogsys(StringUtils.defaultString(zmedHeaderPost.getLogsys(), StringUtils.EMPTY))
			.setBillCategory(StringUtils.defaultString(zmedHeaderPost.getBillCategory(), StringUtils.EMPTY));			
		}
	}
	


	
	
	@JsonProperty("obj_type")
	public String getObjType() {
		return objType;
	}
	
	private ZmedHeaderPostResponseWrapper setObjType(String objType) {
		this.objType = objType;
		return this;
	}
	


	
	
	@JsonProperty("obj_key")
	public String getObjKey() {
		return objKey;
	}
	
	private ZmedHeaderPostResponseWrapper setObjKey(String objKey) {
		this.objKey = objKey;
		return this;
	}
	


	
	
	@JsonProperty("obj_sys")
	public String getObjSys() {
		return objSys;
	}
	
	private ZmedHeaderPostResponseWrapper setObjSys(String objSys) {
		this.objSys = objSys;
		return this;
	}
	
	
	@JsonProperty("bus_act")
	public String getBusAct() {
		return busAct;
	}
	
	private ZmedHeaderPostResponseWrapper setBusAct(String busAct) {
		this.busAct = busAct;
		return this;
	}
	
	

	@JsonProperty("company_code")
	public String getCompCode() {
		return compCode;
	}
	
	private ZmedHeaderPostResponseWrapper setCompCode(String compCode) {
		this.compCode = compCode;
		return this;
	}
	


	
	
	@JsonProperty("username")
	public String getUsername() {
		return username;
	}
	
	private ZmedHeaderPostResponseWrapper setUsername(String username) {
		this.username = username;
		return this;
	}
	


	
	
	@JsonProperty("header_txt")
	public String getHeaderTxt() {
		return headerTxt;
	}
	
	private ZmedHeaderPostResponseWrapper setHeaderTxt(String headerTxt) {
		this.headerTxt = headerTxt;
		return this;
	}
	
	
	@JsonProperty("fisc_year")
	public String getFiscYear() {
		return fiscYear;
	}
	
	private ZmedHeaderPostResponseWrapper setFiscYear(String fiscYear) {
		this.fiscYear = fiscYear;
		return this;
	}
	
	
	

	@JsonProperty("fis_period")
	public String getFisPeriod() {
		return fisPeriod;
	}
	
	private ZmedHeaderPostResponseWrapper setFisPeriod(String fisPeriod) {
		this.fisPeriod = fisPeriod;
		return this;
	}
	


	
	
	@JsonProperty("ref_doc_no")
	public String getRefDocNo() {
		return refDocNo;
	}
	
	private ZmedHeaderPostResponseWrapper setRefDocNo(String refDocNo) {
		this.refDocNo = refDocNo;
		return this;
	}
	


	
	
	@JsonProperty("doc_type")
	public String getDocType() {
		return docType;
	}
	
	private ZmedHeaderPostResponseWrapper setDocType(String docType) {
		this.docType = docType;
		return this;
	}
	
	
	@JsonProperty("acc_doc_no")
	public String getAcDocNo() {
		return acDocNo;
	}
	
	private ZmedHeaderPostResponseWrapper setAcDocNo(String acDocNo) {
		this.acDocNo = acDocNo;
		return this;
	}
	
	


	@JsonProperty("obj_key_r")
	public String getObjKeyR() {
		return objKeyR;
	}
	
	private ZmedHeaderPostResponseWrapper setObjKeyR(String objKeyR) {
		this.objKeyR = objKeyR;
		return this;
	}
	


	
	
	@JsonProperty("reason_rev")
	public String getReasonRev() {
		return reasonRev;
	}
	
	private ZmedHeaderPostResponseWrapper setReasonRev(String reasonRev) {
		this.reasonRev = reasonRev;
		return this;
	}
	


	
	
	@JsonProperty("compo_acc")
	public String getCompoAcc() {
		return compoAcc;
	}
	
	private ZmedHeaderPostResponseWrapper setCompoAcc(String compoAcc) {
		this.compoAcc = compoAcc;
		return this;
	}
	
	
	@JsonProperty("ref_doc_no_long")
	public String getRefDocNoLong() {
		return refDocNoLong;
	}
	
	private ZmedHeaderPostResponseWrapper setRefDocNoLong(String refDocNoLong) {
		this.refDocNoLong = refDocNoLong;
		return this;
	}
	
	
	
	@JsonProperty("acc_principle")
	public String getAccPrinciple() {
		return accPrinciple;
	}
	
	private ZmedHeaderPostResponseWrapper setAccPrinciple(String accPrinciple) {
		this.accPrinciple = accPrinciple;
		return this;
	}
	


	
	
	@JsonProperty("neg_posting")
	public String getNegPosting() {
		return negPosting;
	}
	
	private ZmedHeaderPostResponseWrapper setNegPosting(String negPosting) {
		this.negPosting = negPosting;
		return this;
	}
	


	
	
	@JsonProperty("obj_key_inv")
	public String getObjKeyInv() {
		return objKeyInv;
	}
	
	private ZmedHeaderPostResponseWrapper setObjKeyInv(String objKeyInv) {
		this.objKeyInv = objKeyInv;
		return this;
	}
	
	
	@JsonProperty("bill_category")
	public String getBillCategory() {
		return billCategory;
	}
	
	private ZmedHeaderPostResponseWrapper setBillCategory(String billCategory) {
		this.billCategory = billCategory;
		return this;
	}
	
	
	@JsonProperty("logsys")
	public String getLogsys() {
		return logsys;
	}
	
	private ZmedHeaderPostResponseWrapper setLogsys(String logsys) {
		this.logsys = logsys;
		return this;
	}
	
	
	
	
	


	
	
	@JsonProperty("doc_date")
	public Date getDocDate() {
		return docDate;
	}
	
	private ZmedHeaderPostResponseWrapper setDocDate(Date docDate) {
		this.docDate = docDate;
		return this;
	}
	
	
	@JsonProperty("pstng_date")
	public Date getPstngDate() {
		return pstngDate;
	}
	
	private ZmedHeaderPostResponseWrapper setPstngDate(Date pstngDate) {
		this.pstngDate = pstngDate;
		return this;
	}
	
	
	
	@JsonProperty("transdate_date")
	public Date getTransDate() {
		return transDate;
	}
	
	private ZmedHeaderPostResponseWrapper setTransDate(Date transDate) {
		this.transDate = transDate;
		return this;
	}
	


	
	
	@JsonProperty("vatdate")
	public Date getVatdate() {
		return vatdate;
	}
	
	private ZmedHeaderPostResponseWrapper setVatdate(Date vatdate) {
		this.vatdate = vatdate;
		return this;
	}
}
