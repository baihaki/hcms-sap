package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT2001;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.9
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.9</td><td>Baihaki</td><td>Add fields: superior5ApprovedByUsername, superior5ApprovedByFullname, superior5ApprovedAt</td></tr>
 *     <tr><td>1.0.8</td><td>Baihaki</td><td>Add fields: superior4ApprovedByUsername, superior4ApprovedByFullname, superior4ApprovedAt</td></tr>
 *     <tr><td>1.0.7</td><td>Baihaki</td><td>Rename fields: adminApprovedBy, adminApprovedAt to superior3ApprovedBy, superior3ApprovedAt</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Add fields: adminApprovedByUsername, adminApprovedByFullname, adminApprovedAt</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add fields: superior2ApprovedByUsername, superior2ApprovedByFullname, superior2ApprovedAt</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add fields: superiorApprovedByUsername, superiorApprovedByFullname, superiorApprovedAt</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add fields: createdByUsername, createdByFullname, createdAt</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add fields: Add field: address, phone, mobile, notes, reason, infotype, bukrs, process</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add field: start, end</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class IT2001ResponseWrapper extends ResourceSupport {

	private long pernr;
	private String subty;
	private String subtyText;
	private Date endda;	
	private Date begda;
	
	private long seqnr;
	private String awart;
	private String atext;
	private double abwtg;
	private double stdaz;
	private double kaltg;
	private String start;
	private String end;
	private String address;
	private String phone;
	private String mobile;
	private String notes;
	private String reason;
	private String infotype;
	  
	private String bukrs;
	private String process;
	  
	private String createdByUsername;
	private String createdByFullname;
	private Date createdAt;
	private String superiorApprovedByUsername;
	private String superiorApprovedByFullname;
	private Date superiorApprovedAt;
	private String superior2ApprovedByUsername;
	private String superior2ApprovedByFullname;
	private Date superior2ApprovedAt;
	private String superior3ApprovedByUsername;
	private String superior3ApprovedByFullname;
	private Date superior3ApprovedAt;
	private String superior4ApprovedByUsername;
	private String superior4ApprovedByFullname;
	private Date superior4ApprovedAt;
	private String superior5ApprovedByUsername;
	private String superior5ApprovedByFullname;
	private Date superior5ApprovedAt;
	
	private IT2001ResponseWrapper() {}
	
	public IT2001ResponseWrapper(IT2001 it2001) {
		if (it2001 == null) {
			new IT2001ResponseWrapper();
		} else {
			this
			.setPernr(it2001.getId().getPernr())
			.setSubty(it2001.getSubty() != null ? 
					StringUtils.defaultString(it2001.getSubty().getId() != null ? it2001.getSubty().getId().getSubty() : StringUtils.EMPTY) : it2001.getId().getSubty())
			.setSubtyText(it2001.getSubty() != null ? StringUtils.defaultString(it2001.getSubty().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setEndda(it2001.getId().getEndda()).setBegda(it2001.getId().getBegda())
			.setSeqnr(it2001.getSeqnr() != null ? it2001.getSeqnr().longValue() : 0L)
			.setAwart(it2001.getAwart() != null ? StringUtils.defaultString(it2001.getAwart().getAwart(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setAtext(it2001.getAwart() != null ? StringUtils.defaultString(it2001.getAwart().getAtext(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setAbwtg(it2001.getAbwtg() != null ? it2001.getAbwtg().doubleValue() : 0.0)
			.setStdaz(it2001.getStdaz() != null ? it2001.getStdaz().doubleValue() : 0.0)
			.setKaltg(it2001.getKaltg() != null ? it2001.getKaltg().doubleValue() : 0.0)
			.setStart(it2001.getStart() != null ? CommonDateFunction.convertDateToStringHMS(it2001.getStart()) : StringUtils.EMPTY)
			.setEnd(it2001.getEnd() != null ? CommonDateFunction.convertDateToStringHMS(it2001.getEnd()) : StringUtils.EMPTY)
			.setAddress(StringUtils.defaultString(it2001.getAddress()))
			.setPhone(StringUtils.defaultString(it2001.getPhone()))
			.setMobile(StringUtils.defaultString(it2001.getMobile()))
			.setNotes(StringUtils.defaultString(it2001.getNotes()))
			.setReason(StringUtils.defaultString(it2001.getReason()))
			.setInfotype(StringUtils.defaultString(it2001.getId().getInfty()))
			.setBukrs(StringUtils.defaultString(it2001.getBukrs().getBukrs(), StringUtils.EMPTY))
			.setProcess(StringUtils.defaultString(it2001.getProcess(), StringUtils.EMPTY))
			.setCreatedByUsername(it2001.getCreatedBy() != null ? it2001.getCreatedBy().getUsername() : StringUtils.EMPTY)
			.setCreatedByFullname(it2001.getCreatedBy() != null ? it2001.getCreatedBy().getEmployee().getEname() : StringUtils.EMPTY)
			.setCreatedAt(it2001.getCreatedAt())
			.setSuperiorApprovedByUsername(it2001.getSuperiorApprovedBy() != null ? it2001.getSuperiorApprovedBy().getUsername() : StringUtils.EMPTY)
			.setSuperiorApprovedByFullname(it2001.getSuperiorApprovedBy() != null ? it2001.getSuperiorApprovedBy().getEmployee().getEname() : StringUtils.EMPTY)
			.setSuperiorApprovedAt(it2001.getSuperiorApprovedAt())
			.setSuperior2ApprovedByUsername(it2001.getSuperior2ApprovedBy() != null ? it2001.getSuperior2ApprovedBy().getUsername() : StringUtils.EMPTY)
			.setSuperior2ApprovedByFullname(it2001.getSuperior2ApprovedBy() != null ? it2001.getSuperior2ApprovedBy().getEmployee().getEname() : StringUtils.EMPTY)
			.setSuperior2ApprovedAt(it2001.getSuperior2ApprovedAt())
			.setSuperior3ApprovedByUsername(it2001.getSuperior3ApprovedBy() != null ? it2001.getSuperior3ApprovedBy().getUsername() : StringUtils.EMPTY)
			.setSuperior3ApprovedByFullname(it2001.getSuperior3ApprovedBy() != null ? it2001.getSuperior3ApprovedBy().getEmployee().getEname() : StringUtils.EMPTY)
			.setSuperior3ApprovedAt(it2001.getSuperior3ApprovedAt())
			.setSuperior4ApprovedByUsername(it2001.getApproved4By() != null ? it2001.getApproved4By().getUsername() : StringUtils.EMPTY)
			.setSuperior4ApprovedByFullname(it2001.getApproved4By() != null ? it2001.getApproved4By().getEmployee().getEname() : StringUtils.EMPTY)
			.setSuperior4ApprovedAt(it2001.getApproved4At())
			.setSuperior5ApprovedByUsername(it2001.getApproved5By() != null ? it2001.getApproved5By().getUsername() : StringUtils.EMPTY)
			.setSuperior5ApprovedByFullname(it2001.getApproved5By() != null ? it2001.getApproved5By().getEmployee().getEname() : StringUtils.EMPTY)
			.setSuperior5ApprovedAt(it2001.getApproved5At());
		}
	}
	
	/**
	 * GET Employee SSN/ID.
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	private IT2001ResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	@JsonProperty("subtype")
	public String getSubty() {
		return subty;
	}
	
	private IT2001ResponseWrapper setSubty(String subty) {
		this.subty = subty;
		return this;
	}
	
	/**
	 * GET Subtype Text.
	 * 
	 * @return
	 */
	@JsonProperty("subtype_text")
	public String getSubtyText() {
		return subtyText;
	}
	
	private IT2001ResponseWrapper setSubtyText(String subtyText) {
		this.subtyText = subtyText;
		return this;
	}
	
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	private IT2001ResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}
	
	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	private IT2001ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * GET Infotype Record No.
	 * 
	 * @return
	 */
	@JsonProperty("infotype_record_no")
	public long getSeqnr() {
		return seqnr;
	}
	
	private IT2001ResponseWrapper setSeqnr(long seqnr) {
		this.seqnr = seqnr;
		return this;
	}
	
	/**
	 * GET Absence Type.
	 * 
	 * @return
	 */
	@JsonProperty("absence_type")
	public String getAwart() {
		return awart;
	}
	
	private IT2001ResponseWrapper setAwart(String awart) {
		this.awart = awart;
		return this;
	}
	
	/**
	 * GET Absence Type Name.
	 * 
	 * @return
	 */
	@JsonProperty("absence_type_name")
	public String getAtext() {
		return atext;
	}
	
	private IT2001ResponseWrapper setAtext(String atext) {
		this.atext = atext;
		return this;
	}
	
	/**
	 * GET Absence Days.
	 * 
	 * @return
	 */
	@JsonProperty("absence_days")
	public double getAbwtg() {
		return abwtg;
	}
	
	private IT2001ResponseWrapper setAbwtg(double abwtg) {
		this.abwtg = abwtg;
		return this;
	}
	
	/**
	 * GET Absence Hours.
	 * 
	 * @return
	 */
	@JsonProperty("absence_hours")
	public double getStdaz() {
		return stdaz;
	}
	
	private IT2001ResponseWrapper setStdaz(double stdaz) {
		this.stdaz = stdaz;
		return this;
	}
	
	/**
	 * GET Calendar Days.
	 * 
	 * @return
	 */
	@JsonProperty("calendar_days")
	public double getKaltg() {
		return kaltg;
	}
	
	private IT2001ResponseWrapper setKaltg(double kaltg) {
		this.kaltg = kaltg;
		return this;
	}
	
	/**
	 * GET Start Time.
	 * 
	 * @return
	 */
	@JsonProperty("start_time")
	public String getStart() {
		return start;
	}
	
	private IT2001ResponseWrapper setStart(String start) {
		this.start = start;
		return this;
	}
	
	/**
	 * GET End Time.
	 * 
	 * @return
	 */
	@JsonProperty("end_time")
	public String getEnd() {
		return end;
	}
	
	private IT2001ResponseWrapper setEnd(String end) {
		this.end = end;
		return this;
	}

	@JsonProperty("address_on_leave")
	public String getAddress() {
		return address;
	}

	private IT2001ResponseWrapper setAddress(String address) {
		this.address = address;
		return this;
	}

	@JsonProperty("telephone")
	public String getPhone() {
		return phone;
	}

	private IT2001ResponseWrapper setPhone(String phone) {
		this.phone = phone;
		return this;
	}

	@JsonProperty("mobile_phone")
	public String getMobile() {
		return mobile;
	}

	private IT2001ResponseWrapper setMobile(String mobile) {
		this.mobile = mobile;
		return this;
	}

	@JsonProperty("notes")
	public String getNotes() {
		return notes;
	}

	private IT2001ResponseWrapper setNotes(String notes) {
		this.notes = notes;
		return this;
	}

	@JsonProperty("reason_for_revision")
	public String getReason() {
		return reason;
	}

	private IT2001ResponseWrapper setReason(String reason) {
		this.reason = reason;
		return this;
	}

	@JsonProperty("infotype")
	public String getInfotype() {
		return infotype;
	}

	private IT2001ResponseWrapper setInfotype(String infotype) {
		this.infotype = infotype;
		return this;
	}
	
	@JsonProperty("bukrs")
	public String getBukrs() {
		return bukrs;
	}
	
	private IT2001ResponseWrapper setBukrs(String bukrs) {
		this.bukrs = bukrs;
		return this;
	}
	
	@JsonProperty("process")
	public String getProcess() {
		return process;
	}
	
	private IT2001ResponseWrapper setProcess(String process) {
		this.process = process;
		return this;
	}

	@JsonProperty("created_by_username")
	public String getCreatedByUsername() {
		return createdByUsername;
	}

	public IT2001ResponseWrapper setCreatedByUsername(String createdByUsername) {
		this.createdByUsername = createdByUsername;
		return this;
	}

	@JsonProperty("created_by_fullname")
	public String getCreatedByFullname() {
		return createdByFullname;
	}

	public IT2001ResponseWrapper setCreatedByFullname(String createdByFullname) {
		this.createdByFullname = createdByFullname;
		return this;
	}

	@JsonProperty("created_at")
	public Date getCreatedAt() {
		return createdAt;
	}

	public IT2001ResponseWrapper setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	@JsonProperty("superior_approved_by_username")
	public String getSuperiorApprovedByUsername() {
		return superiorApprovedByUsername;
	}

	public IT2001ResponseWrapper setSuperiorApprovedByUsername(String superiorApprovedByUsername) {
		this.superiorApprovedByUsername = superiorApprovedByUsername;
		return this;
	}

	@JsonProperty("superior_approved_by_fullname")
	public String getSuperiorApprovedByFullname() {
		return superiorApprovedByFullname;
	}

	public IT2001ResponseWrapper setSuperiorApprovedByFullname(String superiorApprovedByFullname) {
		this.superiorApprovedByFullname = superiorApprovedByFullname;
		return this;
	}

	@JsonProperty("superior_approved_at")
	public Date getSuperiorApprovedAt() {
		return superiorApprovedAt;
	}

	public IT2001ResponseWrapper setSuperiorApprovedAt(Date superiorApprovedAt) {
		this.superiorApprovedAt = superiorApprovedAt;
		return this;
	}

	@JsonProperty("superior2_approved_by_username")
	public String getSuperior2ApprovedByUsername() {
		return superior2ApprovedByUsername;
	}

	public IT2001ResponseWrapper setSuperior2ApprovedByUsername(String superior2ApprovedByUsername) {
		this.superior2ApprovedByUsername = superior2ApprovedByUsername;
		return this;
	}

	@JsonProperty("superior2_approved_by_fullname")
	public String getSuperior2ApprovedByFullname() {
		return superior2ApprovedByFullname;
	}

	public IT2001ResponseWrapper setSuperior2ApprovedByFullname(String superior2ApprovedByFullname) {
		this.superior2ApprovedByFullname = superior2ApprovedByFullname;
		return this;
	}

	@JsonProperty("superior2_approved_at")
	public Date getSuperior2ApprovedAt() {
		return superior2ApprovedAt;
	}

	public IT2001ResponseWrapper setSuperior2ApprovedAt(Date superior2ApprovedAt) {
		this.superior2ApprovedAt = superior2ApprovedAt;
		return this;
	}

	@JsonProperty("superior3_approved_by_username")
	public String getSuperior3ApprovedByUsername() {
		return superior3ApprovedByUsername;
	}

	public IT2001ResponseWrapper setSuperior3ApprovedByUsername(String superior3ApprovedByUsername) {
		this.superior3ApprovedByUsername = superior3ApprovedByUsername;
		return this;
	}

	@JsonProperty("superior3_approved_by_fullname")
	public String getSuperior3ApprovedByFullname() {
		return superior3ApprovedByFullname;
	}

	public IT2001ResponseWrapper setSuperior3ApprovedByFullname(String superior3ApprovedByFullname) {
		this.superior3ApprovedByFullname = superior3ApprovedByFullname;
		return this;
	}

	@JsonProperty("superior3_approved_at")
	public Date getSuperior3ApprovedAt() {
		return superior3ApprovedAt;
	}

	public IT2001ResponseWrapper setSuperior3ApprovedAt(Date superior3ApprovedAt) {
		this.superior3ApprovedAt = superior3ApprovedAt;
		return this;
	}

	@JsonProperty("superior4_approved_by_username")
	public String getSuperior4ApprovedByUsername() {
		return superior4ApprovedByUsername;
	}

	public IT2001ResponseWrapper setSuperior4ApprovedByUsername(String superior4ApprovedByUsername) {
		this.superior4ApprovedByUsername = superior4ApprovedByUsername;
		return this;
	}

	@JsonProperty("superior4_approved_by_fullname")
	public String getSuperior4ApprovedByFullname() {
		return superior4ApprovedByFullname;
	}

	public IT2001ResponseWrapper setSuperior4ApprovedByFullname(String superior4ApprovedByFullname) {
		this.superior4ApprovedByFullname = superior4ApprovedByFullname;
		return this;
	}

	@JsonProperty("superior4_approved_at")
	public Date getSuperior4ApprovedAt() {
		return superior4ApprovedAt;
	}

	public IT2001ResponseWrapper setSuperior4ApprovedAt(Date superior4ApprovedAt) {
		this.superior4ApprovedAt = superior4ApprovedAt;
		return this;
	}

	@JsonProperty("superior5_approved_by_username")
	public String getSuperior5ApprovedByUsername() {
		return superior5ApprovedByUsername;
	}

	public IT2001ResponseWrapper setSuperior5ApprovedByUsername(String superior5ApprovedByUsername) {
		this.superior5ApprovedByUsername = superior5ApprovedByUsername;
		return this;
	}

	@JsonProperty("superior5_approved_by_fullname")
	public String getSuperior5ApprovedByFullname() {
		return superior5ApprovedByFullname;
	}

	public IT2001ResponseWrapper setSuperior5ApprovedByFullname(String superior5ApprovedByFullname) {
		this.superior5ApprovedByFullname = superior5ApprovedByFullname;
		return this;
	}

	@JsonProperty("superior5_approved_at")
	public Date getSuperior5ApprovedAt() {
		return superior5ApprovedAt;
	}

	public IT2001ResponseWrapper setSuperior5ApprovedAt(Date superior5ApprovedAt) {
		this.superior5ApprovedAt = superior5ApprovedAt;
		return this;
	}
}