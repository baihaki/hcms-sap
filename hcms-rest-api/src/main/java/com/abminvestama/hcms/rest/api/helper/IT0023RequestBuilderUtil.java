package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.Attachment;
import com.abminvestama.hcms.core.model.entity.IT0023;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.T005T;
import com.abminvestama.hcms.core.model.entity.T016T;
import com.abminvestama.hcms.core.model.entity.T513C;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.business.query.AttachmentQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0023QueryService;
import com.abminvestama.hcms.core.service.api.business.query.T005TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T016TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T513CQueryService;
import com.abminvestama.hcms.core.service.util.FileAttachmentService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0023RequestWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @since 1.0.0
 * @version 2.0.0
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.0</td><td>Baihaki</td><td>Add posName, posRoles field</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Adjust attachment fields on EventData Json</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData: Add attachment fields</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Fix: add setAnsvx on compareAndReturnUpdatedData method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData to handle Create</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("it0023RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT0023RequestBuilderUtil {

	private T513CQueryService t513cQueryService;
	private T005TQueryService t005tQueryService;
	private T016TQueryService t016tQueryService;
	private IT0023QueryService it0023QueryService;
	private AttachmentQueryService attachmentQueryService;
	private FileAttachmentService fileAttachmentService;
	
	@Autowired
	void setT513CQueryService(T513CQueryService t513cQueryService) {
		this.t513cQueryService = t513cQueryService;
	}
	
	@Autowired
	void setT005TQueryService(T005TQueryService t005tQueryService) {
		this.t005tQueryService = t005tQueryService;
	}
	
	@Autowired
	void setT016TQueryService(T016TQueryService t016tQueryService) {
		this.t016tQueryService = t016tQueryService;
	}
	
	@Autowired
	void setIT0023QueryService(IT0023QueryService it0023QueryService) {
		this.it0023QueryService = it0023QueryService;
	}
	
	@Autowired
	void setAttachmentQueryService(AttachmentQueryService attachmentQueryService) {
		this.attachmentQueryService = attachmentQueryService;
	}
	
	@Autowired
	void setFileAttachmentService(FileAttachmentService fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}
	
	/**
	 * Compare IT0023 requested payload with an existing IT0023 in the database.
	 * Update the existing IT0023 data with the latest data comes from the requested payload. 
	 * 
	 * @param requestPayload requested data 
	 * @param it0023DB current existing IT0023 in the database.
	 * @return updated IT0023 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0023, IT0023RequestWrapper, String> compareAndReturnUpdatedData(IT0023RequestWrapper requestPayload, IT0023 it0023DB) {
		
		RequestObjectComparatorContainer<IT0023, IT0023RequestWrapper, String> objectContainer = null;
		
		if (it0023DB == null) {
			it0023DB = new IT0023();
			try {
				Optional<IT0023> existingIT0023 = it0023QueryService.findOneByCompositeKey(
						requestPayload.getPernr(),
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda()));
				
				if (existingIT0023.isPresent()) {
					throw new DataViolationException("Duplicate IT0023 Data for [ssn=" + requestPayload.getPernr() + ",end_date=" +
							requestPayload.getEndda() + ",begin_date=" + requestPayload.getBegda());
				}
				
				it0023DB.setId(
						new ITCompositeKeysNoSubtype(requestPayload.getPernr(),
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda())));
				it0023DB.setArbgb(requestPayload.getArbgb());
				it0023DB.setOrt01(requestPayload.getOrt01());
				Optional<T005T> land1 = t005tQueryService.findById(Optional.ofNullable(requestPayload.getLand1()));
				it0023DB.setLand1(land1.isPresent() ? land1.get() : null);
				Optional<T016T> branc = t016tQueryService.findById(Optional.ofNullable(requestPayload.getBranc()));
				it0023DB.setBranc(branc.isPresent() ? branc.get() : null);
				Optional<T513C> taete = t513cQueryService.findById(Optional.ofNullable(requestPayload.getTaete()));
				it0023DB.setTaete(taete.isPresent() ? taete.get() : null);
				it0023DB.setAnsvx(requestPayload.getAnsvx());
				it0023DB.setSeqnr(1L);
				it0023DB.setStatus(DocumentStatus.INITIAL_DRAFT);
				it0023DB.setPosName(requestPayload.getPosName());
				it0023DB.setPosRoles(requestPayload.getPosRoles());
				
				it0023DB = saveAttachment(it0023DB, requestPayload);
				
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			
			objectContainer = new RequestObjectComparatorContainer<>(it0023DB, requestPayload);
		} else {
			
			if (StringUtils.isNotBlank(requestPayload.getArbgb())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getArbgb(), it0023DB.getArbgb())) {
					it0023DB.setArbgb(requestPayload.getArbgb());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getOrt01())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getOrt01(), it0023DB.getOrt01())) {
					it0023DB.setOrt01(requestPayload.getOrt01());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getLand1())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getLand1(), it0023DB.getLand1() != null ? it0023DB.getLand1().getLand1() : StringUtils.EMPTY)) {
					try {
						Optional<T005T> newT005T = t005tQueryService.findById(Optional.ofNullable(requestPayload.getLand1().trim()));
						if (newT005T.isPresent()) {
							it0023DB.setLand1(newT005T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T005T not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getBranc())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getBranc(), it0023DB.getBranc() != null ? it0023DB.getBranc().getBrsch() : StringUtils.EMPTY)) {
					try {
						Optional<T016T> newT016T = t016tQueryService.findById(Optional.ofNullable(requestPayload.getBranc()));
						if (newT016T.isPresent()) {
							it0023DB.setBranc(newT016T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T016T not found... cancel the update process
					}
				}
			}
			if (requestPayload.getTaete() != 0) {
				if (CommonComparatorFunction.isDifferentNumberValues(Long.valueOf(requestPayload.getTaete()), it0023DB.getTaete() != null ? it0023DB.getTaete().getTaete().longValue() : 0l)) {
					try {
						Optional<T513C> newT513C = t513cQueryService.findById(Optional.ofNullable(requestPayload.getTaete()));
						if (newT513C.isPresent()) {
							it0023DB.setTaete(newT513C.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						//T513C not found... cancel the update process
					}
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getAnsvx())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAnsvx(), it0023DB.getAnsvx())) {
					it0023DB.setAnsvx(requestPayload.getAnsvx());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getPosName())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPosName(), it0023DB.getPosName())) {
					it0023DB.setPosName(requestPayload.getPosName());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getPosRoles())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPosRoles(), it0023DB.getPosRoles())) {
					it0023DB.setPosRoles(requestPayload.getPosRoles());
					requestPayload.setDataChanged(true);
				}
			}
			
			it0023DB.setSeqnr(it0023DB.getSeqnr().longValue() + 1);
			try {
				it0023DB = saveAttachment(it0023DB, requestPayload);
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			it0023DB.setSeqnr(it0023DB.getSeqnr().longValue() - 1);
			
			objectContainer = new RequestObjectComparatorContainer<>(it0023DB, requestPayload);
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = "";
		try {
			jsonString = objectMapper.writeValueAsString(requestPayload);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}		
		
		//JSONObject json = new JSONObject(requestPayload);
		JSONObject json = new JSONObject(jsonString);
		// Adjust attachment fields on EventData Json
		json.remove("attachment1_type");
		json.remove("attachment1_base64");
		json.remove("attachment1_filename");
		json.remove("subtype");
		if (it0023DB.getAttachment() != null)
			json.put("attachment1_type", it0023DB.getAttachment().getId().getSubty());
		if (it0023DB.getAttachmentPath() != null)
		    json.put("attachment1_path", it0023DB.getAttachmentPath());
		// ******************************************
		
		//if (it0021DB.getStatus() != null)
			//json.put("prev_document_status", it0021DB.getStatus().name());
		
		objectContainer.setEventData(json.toString());
		
		return objectContainer;
	}
	
	private IT0023 saveAttachment(IT0023 it0023DB, IT0023RequestWrapper requestPayload) throws Exception {
		
		T591SKey attachmentKey = new T591SKey(SAPInfoType.EMPLOYMENT_HISTORY.infoType(), requestPayload.getAttach1Type());
		Optional<Attachment> attachment = attachmentQueryService.findById(Optional.ofNullable(attachmentKey));
		it0023DB.setAttachment(attachment.isPresent() ? attachment.get() : null);
		String base64 = requestPayload.getAttach1Content();
		String filename = requestPayload.getAttach1Filename();
		if (StringUtils.isNotEmpty(base64) && StringUtils.isNotEmpty(filename)) {
			String ext = filename.split("\\.")[(filename.split("\\.")).length-1];
			String formatFilename = it0023DB.getId().toStringId(SAPInfoType.EMPLOYMENT_HISTORY.infoType()).
					concat("_").concat(it0023DB.getSeqnr().toString()).concat(".").concat(ext);
		    String path = fileAttachmentService.save(formatFilename, base64);
		    it0023DB.setAttachmentPath(path);
		    requestPayload.setDataChanged(true);
		}
		
		return it0023DB;
		
	}
}