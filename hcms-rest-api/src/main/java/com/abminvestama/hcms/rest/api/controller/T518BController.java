package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.core.model.entity.T518B;
import com.abminvestama.hcms.core.service.api.business.query.T518BQueryService;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.T518BResponseWrapper;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@RestController
@RequestMapping("/api/v1/education_or_training")
@Transactional(readOnly = true)
public class T518BController extends AbstractResource {

	private T518BQueryService t518bQueryService;
	
	@Autowired
	void setT518BQueryService(T518BQueryService t518bQueryService) {
		this.t518bQueryService = t518bQueryService;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/page/{pageNumber}", 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<T518BResponseWrapper>> fetchData(@PathVariable int pageNumber) throws Exception {
		ArrayData<T518BResponseWrapper> bunchOfT518B = new ArrayData<>();
		APIResponseWrapper<ArrayData<T518BResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfT518B);
		
		try {
			Page<T518B> data = t518bQueryService.findAll(pageNumber);
			List<T518B> records = (data.hasContent() ? data.getContent() : new ArrayList<>());
			
			Function<T518B, T518BResponseWrapper> converter = (T518B t518b) -> new T518BResponseWrapper(t518b);
			
			List<T518BResponseWrapper> responseWrappers = records.stream().map(t518b -> converter.apply(t518b)).collect(Collectors.toList());
			bunchOfT518B.setItems(responseWrappers.toArray(new T518BResponseWrapper[responseWrappers.size()]));
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(T518BController.class).fetchData(pageNumber)).withSelfRel());
		return response;		
	}
}
