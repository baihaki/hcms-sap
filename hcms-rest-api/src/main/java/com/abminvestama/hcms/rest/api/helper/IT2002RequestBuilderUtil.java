package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.IT2002;
import com.abminvestama.hcms.core.model.entity.IT2002;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.T554T;
import com.abminvestama.hcms.core.service.api.business.query.IT2002QueryService;
import com.abminvestama.hcms.core.service.api.business.query.T554TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T591SQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT2002RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.IT2002RequestWrapper;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Component("it2002RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT2002RequestBuilderUtil {

	
	private IT2002QueryService it2002QueryService;

	@Autowired
	void setIT2002QueryService(IT2002QueryService it2002QueryService) {
		this.it2002QueryService = it2002QueryService;
	}
	
	
	private T591SQueryService t591sQueryService;
	
	@Autowired
	void setT591SQueryService(T591SQueryService t591sQueryService) {
		this.t591sQueryService = t591sQueryService;
	}
	
	private T554TQueryService t554tQueryService;
	
	@Autowired
	void setT554TQueryService(T554TQueryService t554tQueryService) {
		this.t554tQueryService = t554tQueryService;
	}

	/**
	 * Compare IT2002 request payload with existing IT2002 in the database.
	 * Update existing IT2002 data with the latest data comes from the request payload. 
	 * 
	 * @param requestPayload request data 
	 * @param it2002DB current existing IT2002 in the database.
	 * @return updated IT2002 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT2002, IT2002RequestWrapper, String> compareAndReturnUpdatedData(IT2002RequestWrapper requestPayload, IT2002 it2002DB) {
		
		RequestObjectComparatorContainer<IT2002, IT2002RequestWrapper, String> objectContainer = null; 
				
		if (it2002DB == null) {
			it2002DB = new IT2002();
			

			try {
				Optional<IT2002> existingIT2002 = it2002QueryService.findOneByCompositeKey(
						requestPayload.getPernr(), requestPayload.getSubty(), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda()));
				
				if (existingIT2002.isPresent()) {
					throw new DataViolationException("Duplicate IT2002 Data for [ssn=" + requestPayload.getPernr() + ",end_date=" +
							requestPayload.getEndda() + ",begin_date=" + requestPayload.getBegda());
				}
				
				it2002DB.setId(
						new ITCompositeKeys(requestPayload.getPernr(), "2002", requestPayload.getSubty(), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda())));
				
				Optional<T554T> bnka = t554tQueryService.findById(Optional.ofNullable(requestPayload.getAwart()));
				it2002DB.setAwart(bnka.isPresent() ? bnka.get() : null);
				it2002DB.setAbwtg(requestPayload.getAbwtg());
				it2002DB.setStdaz(requestPayload.getStdaz());
				it2002DB.setKaltg(requestPayload.getKaltg());
				it2002DB.setSeqnr(1L);
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			
			objectContainer = new RequestObjectComparatorContainer<>(it2002DB, requestPayload);
			
			
			
			
		} else {
			if (requestPayload.getAwart() != null) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAwart(), it2002DB.getAwart().getAwart())) {
					try {
						Optional<T554T> newT554T = t554tQueryService.findById(Optional.ofNullable(requestPayload.getAwart()));
						if (newT554T.isPresent()) {
							it2002DB.setAwart(newT554T.get());
							requestPayload.setDataChanged(true);
						}
					} catch (Exception e) {
						// t554t not found... ignore change
					}
				}
			}
			
			if (requestPayload.getAbwtg() != (it2002DB.getAbwtg() != null ? it2002DB.getAbwtg().doubleValue() : 0.0)) {
				it2002DB.setAbwtg(requestPayload.getAbwtg());
				requestPayload.setDataChanged(true);
			}
			
			if (requestPayload.getStdaz() != (it2002DB.getStdaz() != null ? it2002DB.getStdaz().doubleValue() : 0.0)) {
				it2002DB.setStdaz(requestPayload.getStdaz());
				requestPayload.setDataChanged(true);
			}
			
			if (requestPayload.getKaltg() != (it2002DB.getKaltg() != null ? it2002DB.getKaltg().doubleValue() : 0.0)) {
				it2002DB.setKaltg(requestPayload.getKaltg());
				requestPayload.setDataChanged(true);
			}
			objectContainer = new RequestObjectComparatorContainer<>(it2002DB, requestPayload);			
		}
		
		JSONObject json = new JSONObject(requestPayload);
		objectContainer.setEventData(json.toString());
		
		return objectContainer;
	}			
}