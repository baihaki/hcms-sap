package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MedDentalRequestWrapper extends ITRequestWrapper  {

	private static final long serialVersionUID = -2317050437190237970L;
	
	private long pernr;
	private String datbi;
	private String datab;
	
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	@JsonProperty("valid_from_date")
	public String getDatab() {
		return datab;
	}
	
	
	@JsonProperty("valid_to_date")
	public String getDatbi() {
		return datbi;
	}
	
}

