package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.IT2002;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@JsonInclude(Include.NON_NULL)
public class IT2002ResponseWrapper extends ResourceSupport {

	private long pernr;
	private String subty;
	private String subtyText;
	private Date endda;	
	private Date begda;
	
	private long seqnr;
	private String awart;
	private String atext;
	private double abwtg;
	private double stdaz;
	private double kaltg;
	
	private IT2002ResponseWrapper() {}
	
	public IT2002ResponseWrapper(IT2002 it2002) {
		if (it2002 == null) {
			new IT2002ResponseWrapper();
		} else {
			this
			.setPernr(it2002.getId().getPernr())
			.setSubty(it2002.getSubty() != null ? 
					StringUtils.defaultString(it2002.getSubty().getId() != null ? it2002.getSubty().getId().getSubty() : StringUtils.EMPTY) : it2002.getId().getSubty())
			.setSubtyText(it2002.getSubty() != null ? StringUtils.defaultString(it2002.getSubty().getStext(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setEndda(it2002.getId().getEndda())
			.setBegda(it2002.getId().getBegda())
			.setSeqnr(it2002.getSeqnr() != null ? it2002.getSeqnr().longValue() : 0L)
			.setAwart(it2002.getAwart() != null ? StringUtils.defaultString(it2002.getAwart().getAwart(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setAtext(it2002.getAwart() != null ? StringUtils.defaultString(it2002.getAwart().getAtext(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setAbwtg(it2002.getAbwtg() != null ? it2002.getAbwtg().doubleValue() : 0.0)
			.setStdaz(it2002.getStdaz() != null ? it2002.getStdaz().doubleValue() : 0.0)
			.setKaltg(it2002.getKaltg() != null ? it2002.getKaltg().doubleValue() : 0.0);
		}
	}
	
	/**
	 * GET Employee SSN/ID.
	 * 
	 * @return
	 */
	@JsonProperty("ssn")
	public long getPernr() {
		return pernr;
	}
	
	private IT2002ResponseWrapper setPernr(long pernr) {
		this.pernr = pernr;
		return this;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	@JsonProperty("subtype")
	public String getSubty() {
		return subty;
	}
	
	private IT2002ResponseWrapper setSubty(String subty) {
		this.subty = subty;
		return this;
	}
	
	/**
	 * GET Subtype Text.
	 * 
	 * @return
	 */
	@JsonProperty("subtype_text")
	public String getSubtyText() {
		return subtyText;
	}
	
	private IT2002ResponseWrapper setSubtyText(String subtyText) {
		this.subtyText = subtyText;
		return this;
	}
	
	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	@JsonProperty("end_date")
	public Date getEndda() {
		return endda;
	}
	
	private IT2002ResponseWrapper setEndda(Date endda) {
		this.endda = endda;
		return this;
	}
	
	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	@JsonProperty("begin_date")
	public Date getBegda() {
		return begda;
	}
	
	private IT2002ResponseWrapper setBegda(Date begda) {
		this.begda = begda;
		return this;
	}
	
	/**
	 * GET Infotype Record No.
	 * 
	 * @return
	 */
	@JsonProperty("infotype_record_no")
	public long getSeqnr() {
		return seqnr;
	}
	
	private IT2002ResponseWrapper setSeqnr(long seqnr) {
		this.seqnr = seqnr;
		return this;
	}
	
	/**
	 * GET Attendance Type.
	 * 
	 * @return
	 */
	@JsonProperty("attendance_type")
	public String getAwart() {
		return awart;
	}
	
	private IT2002ResponseWrapper setAwart(String awart) {
		this.awart = awart;
		return this;
	}
	
	/**
	 * GET Attendance Type Name.
	 * 
	 * @return
	 */
	@JsonProperty("attendance_type_name")
	public String getAtext() {
		return atext;
	}
	
	private IT2002ResponseWrapper setAtext(String atext) {
		this.atext = atext;
		return this;
	}
	
	/**
	 * GET Attendance Days.
	 * 
	 * @return
	 */
	@JsonProperty("attendance_days")
	public double getAbwtg() {
		return abwtg;
	}
	
	private IT2002ResponseWrapper setAbwtg(double abwtg) {
		this.abwtg = abwtg;
		return this;
	}
	
	/**
	 * GET Attendance Hours.
	 * 
	 * @return
	 */
	@JsonProperty("attendance_hours")
	public double getStdaz() {
		return stdaz;
	}
	
	private IT2002ResponseWrapper setStdaz(double stdaz) {
		this.stdaz = stdaz;
		return this;
	}
	
	/**
	 * GET Calendar Days.
	 * 
	 * @return
	 */
	@JsonProperty("calendar_days")
	public double getKaltg() {
		return kaltg;
	}
	
	private IT2002ResponseWrapper setKaltg(double kaltg) {
		this.kaltg = kaltg;
		return this;
	}	
}