package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class ZmedEmpquotasmResponseWrapper extends ResourceSupport  {

	private Date datab;
	private Date datbi;
	private String pernr;
	private String bukrs;
	private String persg;
	private String persk;
	private String fatxt;
	private String kodequo;
	private String exten;
	private String quotamt;
	
	private ZmedEmpquotasmResponseWrapper() {}
	
	public ZmedEmpquotasmResponseWrapper(ZmedEmpquotasm zmedEmpquotasm) {
		if (zmedEmpquotasm == null) {
			new ZmedEmpquotasmResponseWrapper();
		} else {
			this
			//.setBukrs(zmedEmpquotasm.getBukrs() != null ? StringUtils.defaultString(zmedEmpquotasm.getBukrs().getBukrs(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setDatab(zmedEmpquotasm.getDatab())
			.setDatbi(zmedEmpquotasm.getDatbi())
			.setBukrs(StringUtils.defaultString(zmedEmpquotasm.getBukrs().getBukrs(), StringUtils.EMPTY))
			.setPersk(StringUtils.defaultString(zmedEmpquotasm.getPersk(), StringUtils.EMPTY))
			.setPersg(StringUtils.defaultString(zmedEmpquotasm.getPersg(), StringUtils.EMPTY))
			.setFatxt(StringUtils.defaultString(zmedEmpquotasm.getFatxt().toString(), StringUtils.EMPTY))
			.setKodequo(StringUtils.defaultString(zmedEmpquotasm.getKodequo(), StringUtils.EMPTY))
			.setPernr(StringUtils.defaultString(zmedEmpquotasm.getPernr().toString(), StringUtils.EMPTY))
			.setExten(StringUtils.defaultString(zmedEmpquotasm.getExten().toString(), StringUtils.EMPTY))
			.setQuotamt(StringUtils.defaultString(zmedEmpquotasm.getQuotamt().toString(), StringUtils.EMPTY));			
		}
	}
	
	

	@JsonProperty("company_code")
	public String getBukrs() {
		return bukrs;
	}
	
	private ZmedEmpquotasmResponseWrapper setBukrs(String bukrs) {
		this.bukrs = bukrs;
		return this;
	}
	

	@JsonProperty("pernr")
	public String getPernr() {
		return pernr;
	}
	
	private ZmedEmpquotasmResponseWrapper setPernr(String pernr) {
		this.pernr = pernr;
		return this;
	}
	
	@JsonProperty("exten")
	public String getExten() {
		return exten;
	}
	
	private ZmedEmpquotasmResponseWrapper setExten(String exten) {
		this.exten = exten;
		return this;
	}

	
	
	@JsonProperty("fatxt")
	public String getFatxt() {
		return fatxt;
	}
	
	private ZmedEmpquotasmResponseWrapper setFatxt(String fatxt) {
		this.fatxt = fatxt;
		return this;
	}
	


	
	
	@JsonProperty("quota_type")
	public String getKodequo() {
		return kodequo;
	}
	
	private ZmedEmpquotasmResponseWrapper setKodequo(String kodequo) {
		this.kodequo = kodequo;
		return this;
	}
	
	
	
	
	@JsonProperty("quotamt")
	public String getQuotamt() {
		return quotamt;
	}
	
	private ZmedEmpquotasmResponseWrapper setQuotamt(String quotamt) {
		this.quotamt = quotamt;
		return this;
	}
	
	
	@JsonProperty("persg")
	public String getPersg() {
		return persg;
	}
	
	private ZmedEmpquotasmResponseWrapper setPersg(String persg) {
		this.persg = persg;
		return this;
	}
	
	
	@JsonProperty("persk")
	public String getPersk() {
		return persk;
	}
	
	private ZmedEmpquotasmResponseWrapper setPersk(String persk) {
		this.persk = persk;
		return this;
	}

	
	
	@JsonProperty("valid_from_date")
	public Date getDatab() {
		return datab;
	}
	
	private ZmedEmpquotasmResponseWrapper setDatab(Date datab) {
		this.datab = datab;
		return this;
	}
	


	
	
	@JsonProperty("valid_to_date")
	public Date getDatbi() {
		return datbi;
	}
	
	private ZmedEmpquotasmResponseWrapper setDatbi(Date datbi) {
		this.datbi = datbi;
		return this;
	}

}