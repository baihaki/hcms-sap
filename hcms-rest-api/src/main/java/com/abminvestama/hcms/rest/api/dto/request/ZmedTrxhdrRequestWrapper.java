package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add field: reason</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add field: process</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 * 
 */
public class ZmedTrxhdrRequestWrapper  extends ITRequestWrapper {

	
	private static final long serialVersionUID = -2317050437190237970L;
	
	private String bukrs;
	private String zclmno;
	private long pernr;
	private String orgtx;
	private String zdivision;
	private String ename;
	private String clmst;
	private String clmamt;
	private String rejamt;
	private String appamt;
	private String pagu1;
	private String pagu2;
	private String pagudent;
	private String pagu2byemp;
	private String totamt;
	private String pasien;
	private String penyakit;
	private String tglmasuk;
	private String tglkeluar;
	private String clmdt;
	private String clmby;
	private String aprdt;
	private String aprby;
	private String paydt;
	private String payby;
	private String paymethod;
	private String belnr;
	private String jamin;
	private String notex;
	private String billdt;
	private String kodemed;
	private String medtydesc;
	private String revdt;
	private String revby;
	
	private String process;
	private String reason;
	
	@JsonProperty("bukrs")
	public String getBukrs() {
		return bukrs;
	}
	
	@JsonProperty("zclmno")
	public String getZclmno() {
		return zclmno;
	}
	
	@JsonProperty("pernr")
	public long getPernr() {
		return pernr;
	}
	
	@JsonProperty("orgtx")
	public String getOrgtx() {
		return orgtx;
	}
	
	@JsonProperty("zdivision")
	public String getZdivision() {
		return zdivision;
	}
	
	@JsonProperty("ename")
	public String getEname() {
		return ename;
	}
	
	@JsonProperty("clmst")
	public String getClmst() {
		return clmst;
	}
	
	@JsonProperty("clmamt")
	public String getClmamt() {
		return clmamt;
	}
	
	@JsonProperty("rejamt")
	public String getRejamt() {
		return rejamt;
	}
	
	@JsonProperty("appamt")
	public String getAppamt() {
		return appamt;
	}
	
	@JsonProperty("pagu1")
	public String getPagu1() {
		return pagu1;
	}
	
	@JsonProperty("pagu2")
	public String getPagu2() {
		return pagu2;
	}
	
	@JsonProperty("pagudent")
	public String getPagudent() {
		return pagudent;
	}
	
	@JsonProperty("pagu2byemp")
	public String getPagu2byemp() {
		return pagu2byemp;
	}
	
	@JsonProperty("totamt")
	public String getTotamt() {
		return totamt;
	}
	
	@JsonProperty("pasien")
	public String getPasien() {
		return pasien;
	}
	
	@JsonProperty("penyakit")
	public String getPenyakit() {
		return penyakit;
	}
	
	@JsonProperty("tglmasuk")
	public String getTglmasuk() {
		return tglmasuk;
	}
	
	@JsonProperty("tglkeluar")
	public String getTglkeluar() {
		return tglkeluar;
	}
	
	@JsonProperty("clmdt")
	public String getClmdt() {
		return clmdt;
	}
	
	@JsonProperty("clmby")
	public String getClmby() {
		return clmby;
	}
	
	@JsonProperty("aprdt")
	public String getAprdt() {
		return aprdt;
	}
	
	@JsonProperty("aprby")
	public String getAprby() {
		return aprby;
	}
	
	@JsonProperty("paydt")
	public String getPaydt() {
		return paydt;
	}
	
	@JsonProperty("payby")
	public String getPayby() {
		return payby;
	}
	
	@JsonProperty("paymethod")
	public String getPaymethod() {
		return paymethod;
	}
	
	@JsonProperty("belnr")
	public String getBelnr() {
		return belnr;
	}
	
	@JsonProperty("jamin")
	public String getJamin() {
		return jamin;
	}
	
	@JsonProperty("notex")
	public String getNotex() {
		return notex;
	}
	
	@JsonProperty("billdt")
	public String getBilldt() {
		return billdt;
	}
	
	@JsonProperty("med_code")
	public String getKodemed() {
		return kodemed;
	}
	
	@JsonProperty("medtydesc")
	public String getMedtydesc() {
		return medtydesc;
	}
	
	@JsonProperty("revdt")
	public String getRevdt() {
		return revdt;
	}
	
	@JsonProperty("revby")
	public String getRevby() {
		return revby;
	}
	
	@JsonProperty("process")
	public String getProcess() {
		return process;
	}

	@JsonProperty("reason")
	public String getReason() {
		return reason;
	}
	
}
