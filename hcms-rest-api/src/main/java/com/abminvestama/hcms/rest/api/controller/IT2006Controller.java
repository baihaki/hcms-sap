package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.constant.UserRole.AccessRole;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT2006;
import com.abminvestama.hcms.core.model.entity.IT2006;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT2006CommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT2006QueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT2006RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.request.IT2006RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.IT2006ResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.IT2006ResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.IT2006RequestBuilderUtil;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add findAllByAdmin method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findByPernr method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@RestController
@RequestMapping("/api/v1/absence_quotas")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class IT2006Controller extends AbstractResource {

	private IT2006CommandService it2006CommandService;
	private IT2006QueryService it2006QueryService;
	private IT2006RequestBuilderUtil it2006RequestBuilderUtil;

	private HCMSEntityQueryService hcmsEntityQueryService;
	private EventLogCommandService eventLogCommandService;
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	private CommonServiceFactory commonServiceFactory;
	

	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	
	
	@Autowired
	void setIT2006CommandService(IT2006CommandService it2006CommandService) {
		this.it2006CommandService = it2006CommandService;
	}
	
	@Autowired
	void setIT2006QueryService(IT2006QueryService it2006QueryService) {
		this.it2006QueryService = it2006QueryService;
	}
	
	@Autowired
	void setIT2006RequestBuilderUtil(IT2006RequestBuilderUtil it2006RequestBuilderUtil) {
		this.it2006RequestBuilderUtil = it2006RequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}

	@Secured({AccessRole.USER_GROUP})
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<Page<IT2006ResponseWrapper>> findAllByAdmin(
			@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber) throws Exception {

		Page<IT2006ResponseWrapper> responsePage = new PageImpl<IT2006ResponseWrapper>(new ArrayList<>());
		APIResponseWrapper<Page<IT2006ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(responsePage);
		
		Page<IT2006> resultPage = null;
		String bukrs = "";
		
		Optional<User> currentUser = commonServiceFactory.getUserQueryService().findByUsername(currentUser());
		Optional<Role> adminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_ADMIN"));
		Optional<Role> superiorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of("ROLE_SUPERIOR"));
		
		try {
			// no specified ssn, MUST BE ADMIN / SUPERIOR 
			if (!currentUser.get().getRoles().contains(adminRole.get()) && !currentUser.get().getRoles().contains(superiorRole.get())) {
				throw new AuthorizationException(
						"Insufficient Privileges. Please login as 'ADMIN' or 'SUPERIOR'!");
			}
			
			User superiorUser = null;
			
			// If NOT ADMIN (ROLE SUPERIOR ONLY) set superiorUser parameter as additional filter
			if (!currentUser.get().getRoles().contains(adminRole.get())) {
				superiorUser = currentUser.get();
			}
			bukrs = currentUser.get().getEmployee().getT500p().getBukrs().getBukrs();
			resultPage = it2006QueryService.findByBukrsWithPaging(bukrs, superiorUser, pageNumber);
			
			if (resultPage == null || !resultPage.hasContent()) {
				response.setMessage("No Data Found");
			}
			
			if (resultPage.hasContent()) {

				List<IT2006ResponseWrapper> records = new ArrayList<>();
				
				resultPage.getContent().forEach(it2006 -> {
					records.add(new IT2006ResponseWrapper(it2006));
				});
				responsePage = new PageImpl<IT2006ResponseWrapper>(records, it2006QueryService.createPageRequest(pageNumber),
						resultPage.getTotalElements());
				response.setData(responsePage);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT2006Controller.class).findAllByAdmin(pageNumber)).withSelfRel());
		
		return response;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/last/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<IT2006ResponseWrapper> findOneByPernr(@PathVariable String pernr) throws Exception {
		IT2006ResponseWrapper it2006Response = null;
		APIResponseWrapper<IT2006ResponseWrapper> response = new APIResponseWrapper<>();
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT2006> listOfIT2006 = it2006QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			
			if (listOfIT2006.isEmpty()) {
				it2006Response = new IT2006ResponseWrapper(null);
				response.setMessage("No Data Found");
			} else {
				Optional<User> user = commonServiceFactory.getUserQueryService().findByPernr(numOfPernr);
				String employeeName =  user.isPresent() ? user.get().getEmployee().getSname() : StringUtils.EMPTY;
				IT2006 it2006 = listOfIT2006.get(0);
				it2006Response = new IT2006ResponseWrapper(it2006);
				it2006Response.setEmployeeName(employeeName);
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.setData(it2006Response);
		response.add(linkTo(methodOn(IT2006Controller.class).findOneByPernr(pernr)).withSelfRel());
		return response;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT2006ResponseWrapper>> findByPernr(@PathVariable String pernr) throws Exception {
		ArrayData<IT2006ResponseWrapper> it2006Responses = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT2006ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(it2006Responses);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT2006> listOfIT2006 = it2006QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			List<IT2006ResponseWrapper> records = new ArrayList<IT2006ResponseWrapper>();
			
			if (listOfIT2006.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				Optional<User> user = commonServiceFactory.getUserQueryService().findByPernr(numOfPernr);
				String employeeName =  user.isPresent() ? user.get().getEmployee().getSname() : StringUtils.EMPTY;
				for (IT2006 it2006 : listOfIT2006) {
					IT2006ResponseWrapper responseWrapper = new IT2006ResponseWrapper(it2006);
					responseWrapper.setEmployeeName(employeeName);
					records.add(responseWrapper);
				}
				it2006Responses.setItems(records.toArray(new IT2006ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}
		
		response.add(linkTo(methodOn(IT2006Controller.class).findByPernr(pernr)).withSelfRel());
		return response;
	}	
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated IT2006RequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			IT2006ResponseWrapper responseWrapper = new IT2006ResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					Optional<IT2006> it2006 = it2006QueryService.findOneByCompositeKey(request.getPernr(), request.getSubty(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
					if (!it2006.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					RequestObjectComparatorContainer<IT2006, IT2006RequestWrapper, String> updatedContainer 
						= it2006RequestBuilderUtil.compareAndReturnUpdatedData(request, it2006.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
					if (updatedContainer.getEntity().isPresent()) {
						it2006 = it2006CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}
				
					if (!it2006.isPresent()) {
						throw new CannotPersistException("Cannot update IT2006 (Absence Quota Types) data. Please check your data!");
					}
				
					responseWrapper = new IT2006ResponseWrapper(it2006.get());
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(IT2006Controller.class).edit(request, result)).withSelfRel());
		return response;
	}	
	
	
	
	
	
	

	@Secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
		consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> create(@RequestBody @Validated IT2006RequestWrapper request, BindingResult result)
			throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                    ExceptionResponseWrapper.getErrors(
                       result.getFieldErrors(), HttpMethod.PUT
                    )
                ), "Validation error!");
		} else {
						
			Optional<HCMSEntity> it2006Entity = hcmsEntityQueryService.findByEntityName(IT2006.class.getSimpleName());
			if (!it2006Entity.isPresent()) {
				throw new EntityNotFoundException("Cannot find Entity '" + IT2006.class.getSimpleName() + "' !!!");
			}
			
			try {
				
				RequestObjectComparatorContainer<IT2006, IT2006RequestWrapper, String> newObjectContainer 
					= it2006RequestBuilderUtil.compareAndReturnUpdatedData(request, null);
				
				Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
				if (!currentUser.isPresent()) {
					throw new AuthorizationException(
							"Insufficient create privileges. Please login as Admin!");						
				}				
				
				Optional<IT2006> savedEntity = newObjectContainer.getEntity();
				
				if (newObjectContainer.getEntity().isPresent()) {
					newObjectContainer.getEntity().get().setUname(currentUser.get().getUsername());
					savedEntity = it2006CommandService.save(newObjectContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
				}		
				
				if (!savedEntity.isPresent()) {
					throw new CannotPersistException("Cannot create IT2006 (Bank Details) data. Please check your data!");
				}
				
				StringBuilder key = new StringBuilder("pernr=" + savedEntity.get().getId().getPernr());
				key.append(",infty=" + savedEntity.get().getId().getInfty());
				key.append(",subty=" + savedEntity.get().getId().getSubty());
				key.append(",endda=" + savedEntity.get().getId().getEndda());
				key.append(",begda=" + savedEntity.get().getId().getBegda());
				EventLog eventLog = new EventLog(it2006Entity.get(), key.toString(), OperationType.CREATE, 
						(newObjectContainer.getEventData().isPresent() ? newObjectContainer.getEventData().get() : ""));
				Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
				
				if (!savedEventLog.isPresent()) {
					throw new CannotPersistException("Cannot persist event log! All data operation will be rollback to initial state.");
				}
				
				IT2006ResponseWrapper responseWrapper = new IT2006ResponseWrapper(savedEntity.get());
				response = new APIResponseWrapper<>(responseWrapper);	
				
			} catch (DataViolationException e) { 
				throw e;
			} catch (EntityNotFoundException e) { 
				throw e;
			} catch (AuthorizationException e) { 
				throw e;
			} catch (CannotPersistException e) { 
				throw e;
			} catch (Exception e) {
				throw e;
			}
		}
				
		response.add(linkTo(methodOn(IT2006Controller.class).create(request, result)).withSelfRel());		
		return response;
	}
	
	
	
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(itValidator);
	}
}