package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.Attachment;
import com.abminvestama.hcms.core.model.entity.IT0006;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.T005T;
import com.abminvestama.hcms.core.model.entity.T005U;
import com.abminvestama.hcms.core.model.entity.T591S;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.business.query.AttachmentQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0006QueryService;
import com.abminvestama.hcms.core.service.api.business.query.T005TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T005UQueryService;
import com.abminvestama.hcms.core.service.util.FileAttachmentService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0006RequestWrapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.4
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData: Add land1, state</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Adjust attachment fields on EventData Json</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData: Add attachment fields</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Modify compareAndReturnUpdatedData to handle Create</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("it0006RequestBuilderUtil")
@Transactional(readOnly = true)
public class IT0006RequestBuilderUtil {

	private T005UQueryService t005uQueryService;
	private T005TQueryService t005tQueryService;
	
	private IT0006QueryService it0006QueryService;
	private AttachmentQueryService attachmentQueryService;
	private FileAttachmentService fileAttachmentService;
	
	@Autowired
	void setT005UQueryService(T005UQueryService t005uQueryService) {
		this.t005uQueryService = t005uQueryService;
	}
	
	@Autowired
	void setT005TQueryService(T005TQueryService t005tQueryService) {
		this.t005tQueryService = t005tQueryService;
	}
	
	
	@Autowired
	void setIT0006QueryService(IT0006QueryService it0006QueryService) {
		this.it0006QueryService = it0006QueryService;
	}
	
	@Autowired
	void setAttachmentQueryService(AttachmentQueryService attachmentQueryService) {
		this.attachmentQueryService = attachmentQueryService;
	}
	
	@Autowired
	void setFileAttachmentService(FileAttachmentService fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}
	
	/**
	 * Compare IT0006 request payload with existing IT0006 in the database.
	 * Update existing IT0006 data with the latest data comes from the request payload. 
	 * 
	 * @param requestPayload request data 
	 * @param it0006DB current existing IT0006 in the database.
	 * @return updated IT0006 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0006, IT0006RequestWrapper, String> compareAndReturnUpdatedData(IT0006RequestWrapper requestPayload, IT0006 it0006DB) {
		
		RequestObjectComparatorContainer<IT0006, IT0006RequestWrapper, String> objectContainer = null; 
		
		if (it0006DB == null) {
			it0006DB = new IT0006();
			
			try {
				Optional<IT0006> existingIT0006 = it0006QueryService.findOneByCompositeKey(
						requestPayload.getPernr(), requestPayload.getSubty(), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda()));
				
				if (existingIT0006.isPresent()) {
					throw new DataViolationException("Duplicate IT0006 Data for [ssn=" + requestPayload.getPernr() + ",end_date=" +
							requestPayload.getEndda() + ",begin_date=" + requestPayload.getBegda());
				}
				
				it0006DB.setId(
						new ITCompositeKeys(requestPayload.getPernr(), "0006", requestPayload.getSubty(), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda())));
				
				it0006DB.setAnssa(requestPayload.getAnssa());
				it0006DB.setCom01(requestPayload.getCom01());
				it0006DB.setEntk2(requestPayload.getEntk2());
				it0006DB.setEntkm(requestPayload.getEntkm());
				it0006DB.setName2(requestPayload.getName2());
				it0006DB.setNum01(requestPayload.getNum01());
				it0006DB.setOrt01(requestPayload.getOrt01());
				it0006DB.setOrt02(requestPayload.getOrt02());
				it0006DB.setPstlz(requestPayload.getPstlz());
				it0006DB.setStras(requestPayload.getStras());
				it0006DB.setTelnr(requestPayload.getTelnr());
				Optional<T005T> t005t = t005tQueryService.findById(Optional.ofNullable(requestPayload.getLand1()));
				it0006DB.setT005t(t005t.isPresent() ? t005t.get() : null);
				Optional<T005U> t005u = t005uQueryService.findById(Optional.ofNullable(requestPayload.getState()));
				it0006DB.setT005u(t005u.isPresent() ? t005u.get() : null);
				it0006DB.setLocat(requestPayload.getLocat());
				it0006DB.setSeqnr(1L);
				it0006DB.setStatus(DocumentStatus.INITIAL_DRAFT);
				
				it0006DB = saveAttachment(it0006DB, requestPayload);
				
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			
			objectContainer = new RequestObjectComparatorContainer<>(it0006DB, requestPayload);
			
		} else {
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getAnssa(), it0006DB.getAnssa())) {
				it0006DB.setAnssa(StringUtils.defaultString(requestPayload.getAnssa(), ""));
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getName2(), it0006DB.getName2())) {
				it0006DB.setName2(StringUtils.defaultString(requestPayload.getName2(), ""));
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getStras(), it0006DB.getStras())) {
				it0006DB.setStras(StringUtils.defaultString(requestPayload.getStras(), ""));
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getOrt01(), it0006DB.getOrt01())) {
				it0006DB.setOrt01(StringUtils.defaultString(requestPayload.getOrt01(), ""));
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getOrt02(), it0006DB.getOrt02())) {
				it0006DB.setOrt02(StringUtils.defaultString(requestPayload.getOrt02(), ""));
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getPstlz(), it0006DB.getPstlz())) {
				it0006DB.setPstlz(StringUtils.defaultString(requestPayload.getPstlz(), ""));
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getTelnr(), it0006DB.getTelnr())) {
				it0006DB.setTelnr(StringUtils.defaultString(requestPayload.getTelnr(), ""));
				requestPayload.setDataChanged(true);
			}			
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getOrt02(), it0006DB.getOrt02())) {
				it0006DB.setOrt02(StringUtils.defaultString(requestPayload.getOrt02(), ""));
				requestPayload.setDataChanged(true);
			}			
			if (requestPayload.getEntkm() != (it0006DB.getEntkm() != null ? it0006DB.getEntkm().doubleValue() : 0.0)) {
				it0006DB.setEntkm(requestPayload.getEntkm());
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getLocat(), it0006DB.getLocat())) {
				it0006DB.setLocat(StringUtils.defaultString(requestPayload.getLocat(), ""));
				requestPayload.setDataChanged(true);
			}			
			if (requestPayload.getEntk2() != (it0006DB.getEntk2() != null ? it0006DB.getEntk2().doubleValue() : 0.0)) {
				it0006DB.setEntk2(requestPayload.getEntk2());
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getCom01(), it0006DB.getCom01())) {
				it0006DB.setCom01(StringUtils.defaultString(requestPayload.getCom01(), ""));
				requestPayload.setDataChanged(true);
			}			
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getNum01(), it0006DB.getNum01())) {
				it0006DB.setNum01(StringUtils.defaultString(requestPayload.getNum01(), ""));
				requestPayload.setDataChanged(true);
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getLand1(), it0006DB.getT005t() != null ?
					it0006DB.getT005t().getLand1() : StringUtils.EMPTY)) {
				try {
					Optional<T005T> t005t = t005tQueryService.findById(Optional.ofNullable(requestPayload.getLand1()));
					if (t005t.isPresent()) {
						it0006DB.setT005t(t005t.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					// t005t not found..., ignore change
				}
			}
			if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getState(), it0006DB.getT005u() != null ?
					it0006DB.getT005u().getBland() : StringUtils.EMPTY)) {
				try {
					Optional<T005U> t005u = t005uQueryService.findById(Optional.ofNullable(requestPayload.getState()));
					if (t005u.isPresent()) {
						it0006DB.setT005u(t005u.get());
						requestPayload.setDataChanged(true);
					}
				} catch (Exception e) {
					// t005u not found..., ignore change
				}
			}
			
			it0006DB.setSeqnr(it0006DB.getSeqnr().longValue() + 1);
			try {
				it0006DB = saveAttachment(it0006DB, requestPayload);
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			it0006DB.setSeqnr(it0006DB.getSeqnr().longValue() - 1);
			
			objectContainer = new RequestObjectComparatorContainer<>(it0006DB, requestPayload);
		}
		
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = "";
		try {
			jsonString = objectMapper.writeValueAsString(requestPayload);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}		
		
		//JSONObject json = new JSONObject(requestPayload);
		JSONObject json = new JSONObject(jsonString);
		// Adjust attachment fields on EventData Json
		json.remove("attachment1_type");
		json.remove("attachment1_base64");
		json.remove("attachment1_filename");
		if (it0006DB.getAttachment() != null)
			json.put("attachment1_type", it0006DB.getAttachment().getId().getSubty());
		if (it0006DB.getAttachmentPath() != null)
		    json.put("attachment1_path", it0006DB.getAttachmentPath());
		// ******************************************
		
		//if (it0006DB.getStatus() != null)
			//json.put("prev_document_status", it0006DB.getStatus().name());
		objectContainer.setEventData(json.toString());
		
		return  objectContainer;
	}
	
	private IT0006 saveAttachment(IT0006 it0006DB, IT0006RequestWrapper requestPayload) throws Exception {
		
		T591SKey attachmentKey = new T591SKey(SAPInfoType.ADDRESS_DETAILS.infoType(), requestPayload.getAttach1Type());
		Optional<Attachment> attachment = attachmentQueryService.findById(Optional.ofNullable(attachmentKey));
		it0006DB.setAttachment(attachment.isPresent() ? attachment.get() : null);
		String base64 = requestPayload.getAttach1Content();
		String filename = requestPayload.getAttach1Filename();
		if (StringUtils.isNotEmpty(base64) && StringUtils.isNotEmpty(filename)) {
			String ext = filename.split("\\.")[(filename.split("\\.")).length-1];
			String formatFilename = it0006DB.getId().toStringId().concat("_").concat(it0006DB.getSeqnr().toString()).concat(".").concat(ext);
		    String path = fileAttachmentService.save(formatFilename, base64);
		    it0006DB.setAttachmentPath(path);
		    requestPayload.setDataChanged(true);
		}
		
		return it0006DB;
		
	}
	
}