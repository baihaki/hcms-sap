package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.AuthorizationException;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedEmpquotaCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedEmpquotasmCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedEmpquotastfCommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotaQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotasmQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotastfQueryService;
import com.abminvestama.hcms.rest.api.dto.request.MedDentalRequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.MedDentalResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.ZmedEmpquotaRequestBuilderUtil;
import com.abminvestama.hcms.rest.api.helper.ZmedEmpquotasmRequestBuilderUtil;
import com.abminvestama.hcms.rest.api.helper.ZmedEmpquotastfRequestBuilderUtil;

@RestController
@RequestMapping("/api/v1/medical_dental")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class MedDentalController extends AbstractResource {


	//private UserCommandService it0001CommandService;
	private IT0001QueryService it0001QueryService;
	//private UserRequestBuilderUtil it0001RequestBuilderUtil;
	
	private ZmedEmpquotaCommandService zmedEmpquotaCommandService;
	private ZmedEmpquotaQueryService zmedEmpquotaQueryService;
	private ZmedEmpquotaRequestBuilderUtil zmedEmpquotaRequestBuilderUtil;
	

	private ZmedEmpquotasmCommandService zmedEmpquotasmCommandService;
	private ZmedEmpquotasmQueryService zmedEmpquotasmQueryService;
	private ZmedEmpquotasmRequestBuilderUtil zmedEmpquotasmRequestBuilderUtil;
	

	private ZmedEmpquotastfCommandService zmedEmpquotastfCommandService;
	private ZmedEmpquotastfQueryService zmedEmpquotastfQueryService;
	private ZmedEmpquotastfRequestBuilderUtil zmedEmpquotastfRequestBuilderUtil;
	
	private HCMSEntityQueryService hcmsEntityQueryService; //new
	private EventLogCommandService eventLogCommandService; //new
	
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;
	
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}//new
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}//new
	
	
	
	@Autowired
	void setIT0001QueryService(IT0001QueryService it0001QueryService) {
		this.it0001QueryService = it0001QueryService;
	}
	
	
	
	@Autowired
	void setZmedEmpquotaCommandService(ZmedEmpquotaCommandService zmedEmpquotaCommandService) {
		this.zmedEmpquotaCommandService = zmedEmpquotaCommandService;
	}
	
	@Autowired
	void setZmedEmpquotaQueryService(ZmedEmpquotaQueryService zmedEmpquotaQueryService) {
		this.zmedEmpquotaQueryService = zmedEmpquotaQueryService;
	}
	
	@Autowired
	void setZmedEmpquotaRequestBuilderUtil(ZmedEmpquotaRequestBuilderUtil zmedEmpquotaRequestBuilderUtil) {
		this.zmedEmpquotaRequestBuilderUtil = zmedEmpquotaRequestBuilderUtil;
	}
	
	
	
	
	@Autowired
	void setZmedEmpquotasmCommandService(ZmedEmpquotasmCommandService zmedEmpquotasmCommandService) {
		this.zmedEmpquotasmCommandService = zmedEmpquotasmCommandService;
	}
	
	@Autowired
	void setZmedEmpquotasmQueryService(ZmedEmpquotasmQueryService zmedEmpquotasmQueryService) {
		this.zmedEmpquotasmQueryService = zmedEmpquotasmQueryService;
	}
	
	@Autowired
	void setZmedEmpquotasmRequestBuilderUtil(ZmedEmpquotasmRequestBuilderUtil zmedEmpquotasmRequestBuilderUtil) {
		this.zmedEmpquotasmRequestBuilderUtil = zmedEmpquotasmRequestBuilderUtil;
	}
	
	
	
	
	@Autowired
	void setZmedEmpquotastfCommandService(ZmedEmpquotastfCommandService zmedEmpquotastfCommandService) {
		this.zmedEmpquotastfCommandService = zmedEmpquotastfCommandService;
	}
	
	@Autowired
	void setZmedEmpquotastfQueryService(ZmedEmpquotastfQueryService zmedEmpquotastfQueryService) {
		this.zmedEmpquotastfQueryService = zmedEmpquotastfQueryService;
	}
	
	@Autowired
	void setZmedEmpquotastfRequestBuilderUtil(ZmedEmpquotastfRequestBuilderUtil zmedEmpquotastfRequestBuilderUtil) {
		this.zmedEmpquotastfRequestBuilderUtil = zmedEmpquotastfRequestBuilderUtil;
	}
	
	
	
	
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itNoSubtypeValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	
	
	
	
	
	
	//secured({AccessRole.ADMIN_GROUP, AccessRole.SUPERIOR_GROUP})
		@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, //POST -- GET changed
			consumes = MediaType.APPLICATION_JSON_VALUE)
		public APIResponseWrapper<?> findQuotamt(@RequestBody @Validated MedDentalRequestWrapper request, BindingResult result)
				throws AuthorizationException, CannotPersistException, DataViolationException, EntityNotFoundException, Exception {
			
			
			APIResponseWrapper<?> response = null;
			
			if (result.hasErrors()) {
				response = new APIResponseWrapper<>(new ArrayData<>(
	                    ExceptionResponseWrapper.getErrors(
	                       result.getFieldErrors(), HttpMethod.PUT
	                    )
	                ), "Validation error!");
			} else {


				MedDentalResponseWrapper responseWrapper;

				response = new APIResponseWrapper<>();
				/*
				 * Getting data from it0001 by matching pernr
				 */				
				String bukrs = "";
				String persg = "";
				String persk = "";
				long stell = 0;
				
				try {
					Long numOfPernr = request.getPernr();//
					
					List<IT0001> listOfIT0001 = it0001QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
					
					if (listOfIT0001.isEmpty()) {
						response.setMessage("No Data Found in it0001");
					} else {
						int length = listOfIT0001.size();
						
						for (int i = 0; i < length; i++) {
							IT0001 it0001 = listOfIT0001.get(i);
							if(it0001.getId().getEndda().compareTo(CommonDateFunction.convertDateRequestParameterIntoDate(request.getDatbi())) >= 0) {
							//if(!CommonComparatorFunction.isDifferentDateValues(CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31"), it0001.getId().getEndda())) {
								bukrs = it0001.getT500p().getBukrs().getBukrs();
								persg = it0001.getT501t().getPersg();
								persk = it0001.getT503k().getMandt();
								stell = it0001.getT513s().getStell();
								break;
							}
						}
					}
				} catch (NumberFormatException e) { 
					throw e;
				} catch (UsernameNotFoundException e) {
					throw e;
				} catch (Exception e) {
					throw e;
				}
				
				/*
				 * 
				 * reading zmed_empquotasm
				 */
				try {
					String datab = request.getDatab();//
					String datbi = request.getDatbi();//
					Long pernr = request.getPernr();//

					Optional<ZmedEmpquotasm> listOfZmedsm = zmedEmpquotasmQueryService.findOneByCompositeKey(pernr, bukrs, persg, persk, "1", "QOUT2", 
							CommonDateFunction.convertDateRequestParameterIntoDate(datab), 
							CommonDateFunction.convertDateRequestParameterIntoDate(datbi));
					
					if (listOfZmedsm.isPresent()) {

						responseWrapper = new MedDentalResponseWrapper(listOfZmedsm.get());
						response = new APIResponseWrapper<>(responseWrapper);
					
					} else {
						/*
						 * 
						 * reading zmed_empquotastf
						 */
						Optional<ZmedEmpquotastf> listOfZmedstf = zmedEmpquotastfQueryService.findOneByCompositeKey(stell, bukrs, persg, persk, "QOUT2", 
								CommonDateFunction.convertDateRequestParameterIntoDate(datab), 
								CommonDateFunction.convertDateRequestParameterIntoDate(datbi));
						
						if (listOfZmedstf.isPresent()) {

							responseWrapper = new MedDentalResponseWrapper(listOfZmedstf.get());
							response = new APIResponseWrapper<>(responseWrapper);
						
						} else {
						
							/*
							 * 
							 * reading zmed_empquota
							 */
							Optional<ZmedEmpquota> listOfZmed = zmedEmpquotaQueryService.findOneByCompositeKey(bukrs, persg, persk, "1", "QOUT2", 
									CommonDateFunction.convertDateRequestParameterIntoDate(datab), 
									CommonDateFunction.convertDateRequestParameterIntoDate(datbi));
							
							if (listOfZmed.isPresent()) {

								responseWrapper = new MedDentalResponseWrapper(listOfZmed.get());
								response = new APIResponseWrapper<>(responseWrapper);
							
							} else {
								response.setMessage("No data found.");
							}
						}						
					}
				} catch (NumberFormatException e) { 
					throw e;
				} catch (UsernameNotFoundException e) {
					throw e;
				} catch (Exception e) {
					throw e;
				}	
								
			response.add(linkTo(methodOn(MedDentalController.class).findQuotamt(request, result)).withSelfRel());		
			
		}
		return response;
	}
	
	

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		//binder.addValidators(itValidator);
	}

}
