package com.abminvestama.hcms.rest.api.dto.response;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @since 2.0.0
 * @version 2.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@JsonInclude(Include.NON_NULL)
public class ExpenseQuotaResponseWrapper extends ResourceSupport  {

	private Date datab;
	private Date datbi;
	
	private String bukrs;
	private String persg;
	private String persk;
	private String kodequo;
	private String stell;
	private String quotamt;
	
	private ExpenseQuotaResponseWrapper() {}
	
	public ExpenseQuotaResponseWrapper(ZmedEmpquotastf zmedEmpquotastf) {
		if (zmedEmpquotastf == null) {
			new ExpenseQuotaResponseWrapper();
		} else {
			this
			//.setBukrs(zmedEmpquotastf.getBukrs() != null ? StringUtils.defaultString(zmedEmpquotastf.getBukrs().getBukrs(), StringUtils.EMPTY) : StringUtils.EMPTY)
			.setDatab(zmedEmpquotastf.getDatab())
			.setDatbi(zmedEmpquotastf.getDatbi())
			.setBukrs(StringUtils.defaultString(zmedEmpquotastf.getBukrs().getBukrs(), StringUtils.EMPTY))
			.setPersk(StringUtils.defaultString(zmedEmpquotastf.getPersk(), StringUtils.EMPTY))
			.setPersg(StringUtils.defaultString(zmedEmpquotastf.getPersg(), StringUtils.EMPTY))
			.setStell(StringUtils.defaultString(zmedEmpquotastf.getStell().toString(), StringUtils.EMPTY))
			.setKodequo(StringUtils.defaultString(zmedEmpquotastf.getKodequo(), StringUtils.EMPTY))
			.setQuotamt(StringUtils.defaultString(zmedEmpquotastf.getQuotamt().toString(), StringUtils.EMPTY));			
		}
	}
	
	

	@JsonProperty("bukrs")
	public String getBukrs() {
		return bukrs;
	}
	
	private ExpenseQuotaResponseWrapper setBukrs(String bukrs) {
		this.bukrs = bukrs;
		return this;
	}
	

	@JsonProperty("stell")
	public String getStell() {
		return stell;
	}
	
	private ExpenseQuotaResponseWrapper setStell(String stell) {
		this.stell = stell;
		return this;
	}
	
	

	
	
	@JsonProperty("expense_type")
	public String getKodequo() {
		return kodequo;
	}
	
	private ExpenseQuotaResponseWrapper setKodequo(String kodequo) {
		this.kodequo = kodequo;
		return this;
	}
	
	
	
	
	@JsonProperty("amount")
	public String getQuotamt() {
		return quotamt;
	}
	
	private ExpenseQuotaResponseWrapper setQuotamt(String quotamt) {
		this.quotamt = quotamt;
		return this;
	}
	
	
	@JsonProperty("persg")
	public String getPersg() {
		return persg;
	}
	
	private ExpenseQuotaResponseWrapper setPersg(String persg) {
		this.persg = persg;
		return this;
	}
	
	
	@JsonProperty("persk")
	public String getPersk() {
		return persk;
	}
	
	private ExpenseQuotaResponseWrapper setPersk(String persk) {
		this.persk = persk;
		return this;
	}

	
	
	@JsonProperty("valid_from_date")
	public Date getDatab() {
		return datab;
	}
	
	private ExpenseQuotaResponseWrapper setDatab(Date datab) {
		this.datab = datab;
		return this;
	}
	


	
	
	@JsonProperty("valid_to_date")
	public Date getDatbi() {
		return datbi;
	}
	
	private ExpenseQuotaResponseWrapper setDatbi(Date datbi) {
		this.datbi = datbi;
		return this;
	}

}