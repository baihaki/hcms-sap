package com.abminvestama.hcms.rest.api.dto.response;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.ResourceSupport;

import com.abminvestama.hcms.core.model.entity.PtrvHead;
import com.abminvestama.hcms.core.model.entity.PtrvSrec;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 * 
 */
@JsonInclude(Include.NON_NULL)
public class EcostTransactionResponseWrapper  extends ResourceSupport {

	private PtrvHeadResponseWrapper trxHdr;
	private ArrayData<PtrvSrecResponseWrapper> trxItm;
	
	private EcostTransactionResponseWrapper() {}
	
	
	/**
	 * @param ptrvHead
	 * @param ptrvSrec
	 */
	public EcostTransactionResponseWrapper(PtrvHead ptrvHead, PtrvSrec ptrvSrec[]) {
		if (ptrvSrec == null || ptrvHead == null) {
			new EcostTransactionResponseWrapper();
		} else {
			
			ArrayData<PtrvSrecResponseWrapper> bunchOfPtrvSrec = new ArrayData<>();
			APIResponseWrapper<ArrayData<PtrvSrecResponseWrapper>> response = new APIResponseWrapper<>();
			response.setData(bunchOfPtrvSrec);
			
			List<PtrvSrecResponseWrapper> records = new ArrayList<>();
			
			for(int i = 0; i < ptrvSrec.length; i++){
				records.add(new PtrvSrecResponseWrapper(ptrvSrec[i]));
			}
			bunchOfPtrvSrec.setItems(records.toArray(new PtrvSrecResponseWrapper[records.size()]));			
			
			trxHdr = new PtrvHeadResponseWrapper(ptrvHead);
			
			this.setTrxHdr(trxHdr)
				.setTrxItm(bunchOfPtrvSrec);
		}
	}	
	
	@JsonProperty("trx_header")
	public PtrvHeadResponseWrapper getTrxHdr() {
		return trxHdr;
	}
	
	private EcostTransactionResponseWrapper setTrxHdr(PtrvHeadResponseWrapper trxHdr) {
		this.trxHdr = trxHdr;
		return this;
	}
	
	
	@JsonProperty("trx_item")
	public ArrayData<PtrvSrecResponseWrapper> getTrxItm() {
		return trxItm;
	}
	
	private EcostTransactionResponseWrapper setTrxItm(ArrayData<PtrvSrecResponseWrapper> bunchOfTrxItm) {
		this.trxItm = bunchOfTrxItm;
		return this;
	}
	

}

