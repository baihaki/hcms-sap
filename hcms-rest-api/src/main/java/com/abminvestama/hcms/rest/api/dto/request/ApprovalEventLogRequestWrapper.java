package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class ApprovalEventLogRequestWrapper {

	private String entityId;
	private String entityObjectId;
	private String subordinateSSN;
	private String performedBySSN;
	private String performedByPositionId;
	private String operationType;
	
	public ApprovalEventLogRequestWrapper() {}
	
	@JsonProperty("entity_id")
	public String getEntityId() {
		return entityId;
	}
	
	public void setEntityId(final String entityId) {
		this.entityId = entityId;
	}
	
	@JsonProperty("entity_object_id")
	public String getEntityObjectId() {
		return entityObjectId;
	}
	
	public void setEntityObjectId(final String entityObjectId) {
		this.entityObjectId = entityObjectId;
	}
	
	@JsonProperty("subordinate_ssn")
	public String getSubordinateSSN() {
		return subordinateSSN;
	}
	
	public void setSubordinateSSN(final String subordinateSSN) {
		this.subordinateSSN = subordinateSSN;
	}	
	
	@JsonProperty("performed_by_ssn")
	public String getPerformedBySSN() {
		return performedBySSN;
	}
	
	public void setPerformedBySSN(final String performedBySSN) {
		this.performedBySSN = performedBySSN;
	}
		
	@JsonProperty("performed_by_position_id")
	public String getPerformedByPositionId() {
		return performedByPositionId;
	}
	
	public void setPerformedByPositionId(final String performedByPositionId) {
		this.performedByPositionId = performedByPositionId;
	}
	
	@JsonProperty("operation_type")
	public String getOperationType() {
		return operationType;
	}
	
	public void setOperationType(final String operationType) {
		this.operationType = operationType;
	}
}