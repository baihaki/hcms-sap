package com.abminvestama.hcms.rest.api.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.model.entity.IT0040;
import com.abminvestama.hcms.core.model.entity.IT0040;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0040CommandService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0040QueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.helper.IT0040SoapObject;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0040RequestWrapper;
import com.abminvestama.hcms.rest.api.dto.response.APIResponseWrapper;
import com.abminvestama.hcms.rest.api.dto.response.ArrayData;
import com.abminvestama.hcms.rest.api.dto.response.IT0040ResponseWrapper;
import com.abminvestama.hcms.rest.api.exception.dto.ExceptionResponseWrapper;
import com.abminvestama.hcms.rest.api.helper.IT0040RequestBuilderUtil;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@RestController
@RequestMapping("/api/v1/employee_inventory")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class IT0040Controller extends AbstractResource {

	private IT0040CommandService it0040CommandService;
	private IT0040QueryService it0040QueryService;
	

	private HCMSEntityQueryService hcmsEntityQueryService;
	private EventLogCommandService eventLogCommandService;
	
	private IT0040RequestBuilderUtil it0040RequestBuilderUtil;
	
	private UserQueryService userQueryService;
	
	private Validator itValidator;

	private MuleConsumerService muleConsumerService;
	@Autowired
	void setMuleConsumerService(MuleConsumerService muleConsumerService) {
		this.muleConsumerService = muleConsumerService;
	}
	
	@Autowired
	void setIT0040CommandService(IT0040CommandService it0040CommandService) {
		this.it0040CommandService = it0040CommandService;
	}
	
	@Autowired
	void setIT0040QueryService(IT0040QueryService it0040QueryService) {
		this.it0040QueryService = it0040QueryService;
	}
	
	

	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	
	@Autowired
	void setIT0040RequestBuilderUtil(IT0040RequestBuilderUtil it0040RequestBuilderUtil) {
		this.it0040RequestBuilderUtil = it0040RequestBuilderUtil;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	@Qualifier("itValidator")
	void setITValidator(Validator itValidator) {
		this.itValidator = itValidator;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/ssn/{pernr}", 
			headers = "Accept=application/json", produces = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<ArrayData<IT0040ResponseWrapper>> findOneByPernr(@PathVariable String pernr) throws Exception {
		ArrayData<IT0040ResponseWrapper> bunchOfIT0040 = new ArrayData<>();
		APIResponseWrapper<ArrayData<IT0040ResponseWrapper>> response = new APIResponseWrapper<>();
		response.setData(bunchOfIT0040);
		
		try {
			Long numOfPernr = Long.valueOf(pernr);
			
			List<IT0040> listOfIT0040 = it0040QueryService.findByPernr(numOfPernr).stream().collect(Collectors.toList());
			
			if (listOfIT0040.isEmpty()) {
				response.setMessage("No Data Found");
			} else {
				List<IT0040ResponseWrapper> records = new ArrayList<>();
				for (IT0040 it0040 : listOfIT0040) {
					records.add(new IT0040ResponseWrapper(it0040));
				}
				bunchOfIT0040.setItems(records.toArray(new IT0040ResponseWrapper[records.size()]));
			}
		} catch (NumberFormatException nfe) {
			throw nfe;
		} catch (Exception e) {
			throw e;
		}

		response.add(linkTo(methodOn(IT0040Controller.class).findOneByPernr(pernr)).withSelfRel());
		return response;
	}	
	
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json",
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public APIResponseWrapper<?> edit(@RequestBody @Validated IT0040RequestWrapper request, BindingResult result) throws Exception {
		APIResponseWrapper<?> response = null;
		
		if (result.hasErrors()) {
			response = new APIResponseWrapper<>(new ArrayData<>(
                           ExceptionResponseWrapper.getErrors(
                              result.getFieldErrors(), HttpMethod.PUT
                           )
                       ), "Validation error!");
		} else {
			boolean isDataChanged = false;
			
			IT0040ResponseWrapper responseWrapper = new IT0040ResponseWrapper(null);
			
			response = new APIResponseWrapper<>(responseWrapper);
			
			edit: {
				try {
					

					Optional<HCMSEntity> it0040Entity = hcmsEntityQueryService.findByEntityName(IT0040.class.getSimpleName());
					if (!it0040Entity.isPresent()) {
						throw new EntityNotFoundException("Cannot find Entity '" + IT0040.class.getSimpleName() + "' !!!");
					}	
					
					Optional<IT0040> it0040 = it0040QueryService.findOneByCompositeKey(request.getPernr(), request.getSubty(),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getEndda()),
							CommonDateFunction.convertDateRequestParameterIntoDate(request.getBegda()));
					if (!it0040.isPresent()) {
						response.setMessage("Cannot update data. No Data Found!");
						break edit;
					}
				
					RequestObjectComparatorContainer<IT0040, IT0040RequestWrapper, String> updatedContainer = it0040RequestBuilderUtil.compareAndReturnUpdatedData(request, it0040.get());

					if (updatedContainer.getRequestPayload().isPresent()) {
						isDataChanged = updatedContainer.getRequestPayload().get().isDataChanged();
					}
				
					if (!isDataChanged) {
						response.setMessage("No data changed. No need to perform the update.");
						break edit;
					}
				
					Optional<User> currentUser = userQueryService.findByUsername(currentUser());
				
					if (updatedContainer.getEntity().isPresent()) {
						updatedContainer.getEntity().get().setSeqnr(it0040.get().getSeqnr().longValue() + 1); // increment "seqnr" everytime record being updated						
						it0040 = it0040CommandService.save(updatedContainer.getEntity().get(), currentUser.isPresent() ? currentUser.get() : null);
					}
				
					if (!it0040.isPresent()) {
						throw new CannotPersistException("Cannot update IT0040 (Employee Inventory) data. Please check your data!");
					}
					
					

					StringBuilder key = new StringBuilder("pernr=" + it0040.get().getId().getPernr());
					key.append(",infty=" + it0040.get().getId().getInfty());
					key.append(",subty=" + it0040.get().getId().getSubty());
					key.append(",endda=" + it0040.get().getId().getEndda());
					key.append(",begda=" + it0040.get().getId().getBegda());
					EventLog eventLog = new EventLog(it0040Entity.get(), key.toString(), OperationType.UPDATE, 
							(updatedContainer.getEventData().isPresent() ? updatedContainer.getEventData().get() : ""));
					Optional<EventLog> savedEventLog = eventLogCommandService.save(eventLog, currentUser.get());
					

					//IT0040SoapObject it0040SoapObject = new IT0040SoapObject(savedEntity.get()); 
					//muleConsumerService.postToSAP(BAPIFunction.IT0040_CREATE.service(), "employeeInventory", it0040SoapObject);
					
				
					responseWrapper = new IT0040ResponseWrapper(it0040.get());
					response = new APIResponseWrapper<>(responseWrapper);
				
				} catch (NullPointerException nfe) { 
					throw nfe;
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
		response.add(linkTo(methodOn(IT0040Controller.class).edit(request, result)).withSelfRel());
		return response;
	}		
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(itValidator);
	}	
}