package com.abminvestama.hcms.rest.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ZmedEmpquotastfRequestWrapper extends ITRequestWrapper  {

		private static final long serialVersionUID = -2317050437190237970L;
		
		private Long stell;
		private String bukrs;
		private String persg;
		private String persk;
		private String kodequo;
		private String datbi;
		private String datab;
		private String quotamt;
		
		@JsonProperty("job")
		public Long getStell() {
			return stell;
		}
		
		
		@JsonProperty("company_code")
		public String getBukrs() {
			return bukrs;
		}
		
		
		@JsonProperty("employee_group")
		public String getPersg() {
			return persg;
		}
		
		
		@JsonProperty("employee_subgroup")
		public String getPersk() {
			return persk;
		}
		
		
		@JsonProperty("quota_type")
		public String getKodequo() {
			return kodequo;
		}
		
		
		@JsonProperty("valid_from_date")
		public String getDatab() {
			return datab;
		}
		
		
		@JsonProperty("valid_to_date")
		public String getDatbi() {
			return datbi;
		}
		
		
		@JsonProperty("quotamt")
		public String getQuotamt() {
			return quotamt;
		}
		
	}

