package com.abminvestama.hcms.rest.api.validator;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.abminvestama.hcms.core.model.entity.Workflow;
import com.abminvestama.hcms.core.service.api.business.query.WorkflowQueryService;
import com.abminvestama.hcms.rest.api.dto.request.WorkflowRequestWrapper;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Component("workflowValidator")
public class WorkflowValidator implements Validator {
	
	private WorkflowQueryService workflowQueryService;
	
	@Autowired
	void setWorkflowQueryService(WorkflowQueryService workflowQueryService) {
		this.workflowQueryService = workflowQueryService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return WorkflowRequestWrapper.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		final WorkflowRequestWrapper request = (WorkflowRequestWrapper) target;
		
		if (StringUtils.isBlank(request.getName())) {
			errors.rejectValue("name", null, "'workflow_name' is missing!");
		} else {
			Optional<Workflow> workflow = workflowQueryService.findByName(request.getName());
			if (workflow.isPresent()) {
				errors.rejectValue("name", null, "Duplicate 'workflow_name'. " + request.getName() + " already exists!");
			}
		}
	}
}