package com.abminvestama.hcms.rest.api.dto.helper;

import java.io.Serializable;
import java.util.Optional;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class RequestObjectComparatorContainer<E, R, J> implements Serializable {
	
	private E entity;
	private R requestPayload;
	private J eventData;
	
	public RequestObjectComparatorContainer(E entity, R requestPayload) {
		this.entity = entity;
		this.requestPayload = requestPayload;
	}
	
	public Optional<E> getEntity() {
		return Optional.ofNullable(entity);
	}
	
	public Optional<R> getRequestPayload() {
		return Optional.ofNullable(requestPayload);
	}
	
	public Optional<J> getEventData() {
		return Optional.ofNullable(eventData);
	}
	
	public void setEventData(J eventData) {
		this.eventData = eventData;
	}

	private static final long serialVersionUID = -1511947048269850264L;
}