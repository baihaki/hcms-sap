package com.abminvestama.hcms.rest.api.helper;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.abminvestama.hcms.common.util.CommonComparatorFunction;
import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.Attachment;
import com.abminvestama.hcms.core.model.entity.IT0242;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.business.query.AttachmentQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0242QueryService;
import com.abminvestama.hcms.core.service.util.FileAttachmentService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.IT0242RequestWrapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Modify compareAndReturnUpdatedData: Add attachment fields</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Component("it0242RequestBuilderUtil")
public class IT0242RequestBuilderUtil {
	
	private IT0242QueryService it0242QueryService;
	private AttachmentQueryService attachmentQueryService;
	private FileAttachmentService fileAttachmentService;
	
	@Autowired
	void setIT0242QueryService(IT0242QueryService it0242QueryService) {
		this.it0242QueryService = it0242QueryService;
	}
	
	@Autowired
	void setAttachmentQueryService(AttachmentQueryService attachmentQueryService) {
		this.attachmentQueryService = attachmentQueryService;
	}
	
	@Autowired
	void setFileAttachmentService(FileAttachmentService fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}


	/**
	 * Compare IT0242 requested payload with an existing IT0242 in the database.
	 * Update the existing IT0242 data with the latest data comes from the requested payload. 
	 * 
	 * @param requestPayload requested data 
	 * @param it0242DB current existing IT0242 in the database.
	 * @return updated IT0242 object to be persisted into the database.
	 */
	public RequestObjectComparatorContainer<IT0242, IT0242RequestWrapper, String> compareAndReturnUpdatedData(IT0242RequestWrapper requestPayload, IT0242 it0242DB) {
		
		RequestObjectComparatorContainer<IT0242, IT0242RequestWrapper, String> objectContainer = null;
		
		if (it0242DB == null) {
			it0242DB = new IT0242();
			try {
				Optional<IT0242> existingIT0242 = it0242QueryService.findOneByCompositeKey(
						requestPayload.getPernr(), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
						CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda()));
				
				if (existingIT0242.isPresent()) {
					throw new DataViolationException("Duplicate IT0242 Data for [ssn=" + requestPayload.getPernr() + ",end_date=" +
							requestPayload.getEndda() + ",begin_date=" + requestPayload.getBegda());
				}
				
				it0242DB.setId(
						new ITCompositeKeysNoSubtype(requestPayload.getPernr(), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getEndda()), 
								CommonDateFunction.convertDateRequestParameterIntoDate(requestPayload.getBegda())));
				it0242DB.setJamId(requestPayload.getJamId());
				it0242DB.setMarst(requestPayload.getMarst());
				it0242DB.setSeqnr(1L);
				it0242DB.setStatus(DocumentStatus.INITIAL_DRAFT);
				
				it0242DB = saveAttachment(it0242DB, requestPayload);
				
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			
			objectContainer = new RequestObjectComparatorContainer<>(it0242DB, requestPayload);
			
		} else {
			
			if (StringUtils.isNotBlank(requestPayload.getJamId())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getJamId(), it0242DB.getJamId())) {
					it0242DB.setJamId(requestPayload.getJamId());
					requestPayload.setDataChanged(true);
				}
			}
			
			if (StringUtils.isNotBlank(requestPayload.getMarst())) {
				if (CommonComparatorFunction.isDifferentStringValues(requestPayload.getMarst(), it0242DB.getMarst())) {
					it0242DB.setMarst(requestPayload.getMarst());
					requestPayload.setDataChanged(true);
				}
			}
			
			it0242DB.setSeqnr(it0242DB.getSeqnr().longValue() + 1);
			try {
				it0242DB = saveAttachment(it0242DB, requestPayload);
			} catch (Exception ignored) {
				System.err.println(ignored);
			}
			it0242DB.setSeqnr(it0242DB.getSeqnr().longValue() - 1);
			
			objectContainer = new RequestObjectComparatorContainer<>(it0242DB, requestPayload);
		}
		
		JSONObject json = new JSONObject(requestPayload);
		if (it0242DB.getStatus() != null)
		    json.put("prev_document_status", it0242DB.getStatus().name());
		objectContainer.setEventData(json.toString());
		
		return objectContainer;
	}
	
	private IT0242 saveAttachment(IT0242 it0242DB, IT0242RequestWrapper requestPayload) throws Exception {
		
		T591SKey attachmentKey = new T591SKey(SAPInfoType.BPJS.infoType(), requestPayload.getAttach1Type());
		Optional<Attachment> attachment = attachmentQueryService.findById(Optional.ofNullable(attachmentKey));
		it0242DB.setAttachment(attachment.isPresent() ? attachment.get() : null);
		String base64 = requestPayload.getAttach1Content();
		String filename = requestPayload.getAttach1Filename();
		if (StringUtils.isNotEmpty(base64) && StringUtils.isNotEmpty(filename)) {
			String ext = filename.split("\\.")[(filename.split("\\.")).length-1];
			String formatFilename = it0242DB.getId().toStringId(SAPInfoType.BPJS.infoType()).
					concat("_").concat(it0242DB.getSeqnr().toString()).concat(".").concat(ext);
		    String path = fileAttachmentService.save(formatFilename, base64);
		    it0242DB.setAttachmentPath(path);
		    requestPayload.setDataChanged(true);
		}
		
		return it0242DB;
		
	}
}