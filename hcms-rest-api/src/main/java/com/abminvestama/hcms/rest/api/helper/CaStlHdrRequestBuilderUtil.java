package com.abminvestama.hcms.rest.api.helper;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.WeakHashMap;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.CaAdvanceCode;
import com.abminvestama.hcms.core.model.constant.CashAdvanceConstant;
import com.abminvestama.hcms.core.model.constant.SettlementCASStatus;
import com.abminvestama.hcms.core.model.constant.SettlementProcess;
import com.abminvestama.hcms.core.model.constant.UserRole;
import com.abminvestama.hcms.core.model.entity.CaEmpCSKT;
import com.abminvestama.hcms.core.model.entity.CaReqHdr;
import com.abminvestama.hcms.core.model.entity.CaStlHdr;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.CaReqHdrCommandService;
import com.abminvestama.hcms.core.service.api.business.query.CaReqHdrQueryService;
import com.abminvestama.hcms.core.service.api.business.query.CaStlHdrQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.util.FileAttachmentService;
import com.abminvestama.hcms.rest.api.dto.helper.RequestObjectComparatorContainer;
import com.abminvestama.hcms.rest.api.dto.request.CaStlHdrRequestWrapper;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author wijanarko (wijanarko@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 * 
 */
@Component("CaStlHdrRequestBuilderUtil")
@Transactional(readOnly = true)
public class CaStlHdrRequestBuilderUtil {
	private T001QueryService t001QueryService;
	
	private CaReqHdrCommandService caReqHdrCommandService;
	private CaReqHdrQueryService caReqHdrQueryService;
	
	private CaStlHdrQueryService caStlHdrQueryService;
	private UserQueryService userQueryService;
	private FileAttachmentService fileAttachmentService;
	
	private CommonServiceFactory commonServiceFactory;

	@Autowired
	void setT001QueryService(T001QueryService t001QueryService) {
		this.t001QueryService = t001QueryService;
	}

	@Autowired
	void setCaStlHdrQueryService(CaStlHdrQueryService caStlHdrQueryService) {
		this.caStlHdrQueryService = caStlHdrQueryService;
	}

	@Autowired
	void setCaReqHdrCommandService(CaReqHdrCommandService caReqHdrCommandService) {
		this.caReqHdrCommandService = caReqHdrCommandService;
	}

	@Autowired
	void setCaReqHdrQueryService(CaReqHdrQueryService caReqHdrQueryService) {
		this.caReqHdrQueryService = caReqHdrQueryService;
	}

	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}

	@Autowired
	void setFileAttachmentService(FileAttachmentService fileAttachmentService) {
		this.fileAttachmentService = fileAttachmentService;
	}

	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}

	/**
	 * Compare CaStlHdr request payload with existing CaStlHdr in the database.
	 * Update existing CaStlHdr data with the latest data comes from the request payload. 
	 * 
	 * @param requestPayload request data 
	 * @param caStlHdrDB current existing CaStlHdr in the database.
	 * @return updated CaStlHdr object to be persisted into the database.
	 * @throws ParseException 
	 */
	public RequestObjectComparatorContainer<CaStlHdr, CaStlHdrRequestWrapper, String> compareAndReturnUpdatedData(
			CaStlHdrRequestWrapper requestPayload, CaStlHdr caStlHdrDB) throws DataViolationException, NoSuchMethodException, CannotPersistException, ConstraintViolationException {

		RequestObjectComparatorContainer<CaStlHdr, CaStlHdrRequestWrapper, String> objectContainer = null;
		Date thisDate = new Date();
		
		if (caStlHdrDB == null) {
			caStlHdrDB = new CaStlHdr();
			
			Optional<CaStlHdr> existingCaStlHdr = caStlHdrQueryService.findByReqno(requestPayload.getReqNo());
			// 1.Cash Advance Settlement Number - Primary Key
			if (existingCaStlHdr.isPresent()) {
				throw new DataViolationException("Duplicate Cash Advance Settlement Data : "
						+ "S.N = " + existingCaStlHdr.get().getPernr() + ", "
								+ existingCaStlHdr.get().getReqNo() + " ");
			}
			// 2.Personnel Number
			Optional<User> employee = userQueryService.findByPernr(requestPayload.getPernr());
			caStlHdrDB.setPernr(employee.get().getEmployee().getPernr());
			// 3.Cash Advance Request Number
			// ==============================
			// NOT REIMBURSEMENT, TCA No. is empty
			if (StringUtils.isNotEmpty(requestPayload.getTcaNo())) {
				// Settlement - Non Reimbursement
				Optional<CaReqHdr> tca = null;
				try {
					tca = caReqHdrQueryService.findById(Optional.ofNullable(requestPayload.getTcaNo()));
				} catch (Exception e) {
					e.printStackTrace();
				}
				caStlHdrDB.setTcaNo(tca.get());
			}
			// REIMBURSEMENT
			else if (requestPayload.getAdvanceCode().equals(CaAdvanceCode.RE.name())) {
				// Auto Create Cash Advance Request - Reimbursement & status process = 8
				CaReqHdr reqHdr = new CaReqHdr();
				reqHdr.setPernr(employee.get().getEmployee().getPernr());
				reqHdr.setBukrs(employee.get().getEmployee().getT500p().getBukrs());
				// AMOUNT REQUEST = AMOUNT SETTLEMENT
				reqHdr.setAmount(new Double(requestPayload.getTotalAmount()));
				reqHdr.setCurrency(CashAdvanceConstant.CURRENCY_IDR);
				reqHdr.setProcess(new Integer(8));
				reqHdr.setAdvanceCode(CaAdvanceCode.RE.name());
				reqHdr.setDescription(CaAdvanceCode.RE.description);
				reqHdr.setCreatedAt(thisDate);
				reqHdr.setCreatedBy(employee.get());
				reqHdr.setUpdatedAt(thisDate);
				reqHdr.setUpdatedBy(employee.get());
				reqHdr.setPaymentStatus("N");
				
				Optional<CaReqHdr> caReqHdr;
				try {
					caReqHdr = caReqHdrCommandService.save(reqHdr, employee.get());
					if (!caReqHdr.isPresent()) {
						throw new CannotPersistException("Cannot update CaReqHdr (Cash Advance Request Header) data. Please check your data!");
					}
					Optional<CaReqHdr> tcaNo = caReqHdrQueryService.findById(Optional.ofNullable(caReqHdr.get().getReqNo()));
					caStlHdrDB.setTcaNo(tcaNo.get());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			// 4.Settlement Document Number
			caStlHdrDB.setDocNo(requestPayload.getDocNo());
			// 5.Settlement Total Amount
			caStlHdrDB.setTotalAmount(StringUtils.isNotEmpty(requestPayload.getTotalAmount()) ? new Double(requestPayload.getTotalAmount()) : 0.0);
			// 6.Settlement Difference Amount
			caStlHdrDB.setDiffAmount(StringUtils.isNotEmpty(requestPayload.getDiffAmount()) ? new Double(requestPayload.getDiffAmount()) : 0.0);
			// 7.Currency
			if (!StringUtils.isNotEmpty(requestPayload.getCurrency())) {
				throw new DataViolationException("Cash Advance Settlement Currency is empty");
			}
			caStlHdrDB.setCurrency(requestPayload.getCurrency());
			// 8.Cash Advance Settlement Status (BL=Balance || OP=OverPay || UP=UnderPay)
			if (!StringUtils.isNotEmpty(requestPayload.getCasStatus())) {
				throw new DataViolationException("Cash Advance Settlement Status is empty");
			}
			caStlHdrDB.setCasStatus(requestPayload.getCasStatus());
			// 9.Settlement Notes
			caStlHdrDB.setNotes(requestPayload.getNotes());
			// 10.Company Code
			Optional<T001> bnka = t001QueryService.findByBukrs(requestPayload.getBukrs());
			caStlHdrDB.setBukrs(bnka.isPresent() ? bnka.get() : null);
			
			caStlHdrDB.setRefundAmount(StringUtils.isNotEmpty(requestPayload.getRefundAmount()) ? new Double(requestPayload.getRefundAmount()) : 0.0);
			caStlHdrDB.setClaimAmount(StringUtils.isNotEmpty(requestPayload.getClaimAmount()) ? new Double(requestPayload.getClaimAmount()) : 0.0);
		}
		// EDIT ITEM
		else {
			requestPayload.setDataChanged(true);
		}
		objectContainer = new RequestObjectComparatorContainer<>(caStlHdrDB, requestPayload);

		JSONObject json = new JSONObject(requestPayload);
		objectContainer.setEventData(json.toString());

		return objectContainer;
	}

	public CaStlHdr saveAttachment(CaStlHdr caStlHdrDB, CaStlHdrRequestWrapper requestPayload) throws Exception {
		String base64 = requestPayload.getAttach1Content();
		String filename = requestPayload.getAttach1Filename();
		if (StringUtils.isNotEmpty(base64) && StringUtils.isNotEmpty(filename)) {
			String ext = filename.split("\\.")[(filename.split("\\.")).length-1];
			String formatFilename = "CaStlHdr".concat("_").concat(caStlHdrDB.getPernr().toString()).concat("_").concat(
					caStlHdrDB.getReqNo()).concat(".").concat(ext);
		    String path = fileAttachmentService.save(formatFilename, base64);
		    caStlHdrDB.setAttachment(path);
		    requestPayload.setDataChanged(true);
		}
		return caStlHdrDB;
	}

	public WeakHashMap<String, Object> validationAppoval(int currentProcess, Optional<User> currentUser, CaStlHdr settlementHeader,
			List<CaEmpCSKT> listOfCSKTPooling, int countNumberOfApprovedPooling)
			throws Exception {
		int nextProcess = 0;
		String message = "";
		String approvalProcessNote = "";
		WeakHashMap<String, Object> result = new WeakHashMap<String, Object>();
		
		//=============================================================
		// GET ALL MASTER ROLE ON DATABASE
		//=============================================================
		Optional<Role> hrAdminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_ADMIN.roleIdent));
		Optional<Role> headRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_HEAD.roleIdent));
		Optional<Role> directorRole =commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_DIRECTOR.roleIdent));
		Optional<Role> gaRole =commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_GA.roleIdent));
		Optional<Role> poolingRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_POOLING.roleIdent));
		Optional<Role> accountingRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_ACCOUNTING.roleIdent));
		Optional<Role> treasuryRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_TREASURY.roleIdent));
		
		if (currentProcess == SettlementProcess.WAITING_HEAD_RELEASE.val) {
			// Approval Not Role Head
			if (!currentUser.get().getRoles().contains(headRole.get())) {
				message = CashAdvanceConstant.MESSAGE_INFO + " Approval must be "
						+ UserRole.ROLE_HEAD.roleName + ". Please check your role!";
				throw new Exception(message);
			}
			
			// Approval by Role Head
			result.put("TRX_APPROVAL_PROCESS", headRole.get());
			
			// Total Amount Settlement > 10 million
			if (settlementHeader.getTotalAmount() > CashAdvanceConstant.APPROVAL_DIRECTOR_LIMIT) {
				nextProcess = SettlementProcess.WAITING_DIRECTOR_RELEASE.val;
			}
			// TRAVEL ADVANCE
			else if (settlementHeader.getTcaNo().getAdvanceCode().equals(CaAdvanceCode.TA)) {
				nextProcess = SettlementProcess.WAITING_GA_RELEASE.val;
			}
			// POOLING - Cost Center
			else if (listOfCSKTPooling.size() > 0) {
				nextProcess = SettlementProcess.WAITING_POOLING_RELEASE.val;
			}
			// Nothing POOLING - Cost Center
			else {
				nextProcess = SettlementProcess.WAITING_ACCOUNTING_RELEASE.val;
			}
		}
		else if (currentProcess == SettlementProcess.WAITING_DIRECTOR_RELEASE.val) {
			if (!currentUser.get().getRoles().contains(directorRole.get())) {
				message = CashAdvanceConstant.MESSAGE_INFO + " Approval must be "
						+ UserRole.ROLE_DIRECTOR.roleName + ". Please check your role!";
				throw new Exception(message);
			}
			
			// Approval by Role DIRECTOR
			result.put("TRX_APPROVAL_PROCESS", directorRole.get());
			
			// TRAVEL ADVANCE
			if (CaAdvanceCode.TA.equals(settlementHeader.getTcaNo().getAdvanceCode())) {
				nextProcess = SettlementProcess.WAITING_GA_RELEASE.val;
			}
			// POOLING - Cost Center
			else if (listOfCSKTPooling.size() > 0) {
				nextProcess = SettlementProcess.WAITING_POOLING_RELEASE.val;
			}
			// Nothing POOLING - Cost Center
			else {
				nextProcess = SettlementProcess.WAITING_ACCOUNTING_RELEASE.val;
			}
		}
		else if (currentProcess == SettlementProcess.WAITING_GA_RELEASE.val) {
			if (!currentUser.get().getRoles().contains(gaRole.get())) {
				message = CashAdvanceConstant.MESSAGE_INFO + " Approval must be "
						+ UserRole.ROLE_GA.roleName + ". Please check your role!";
				throw new Exception(message);
			}
			
			// Approval by Role GA
			result.put("TRX_APPROVAL_PROCESS", gaRole.get());
			
			nextProcess = SettlementProcess.WAITING_HR_RELEASE.val;
		}
		else if (currentProcess == SettlementProcess.WAITING_HR_RELEASE.val) {
			if (!currentUser.get().getRoles().contains(hrAdminRole.get())) {
				message = CashAdvanceConstant.MESSAGE_INFO + " Approval must be "
						+ UserRole.ROLE_ADMIN.roleName + ". Please check your role!";
				throw new Exception(message);
			}
			
			// Approval by Role HR ADMIN
			result.put("TRX_APPROVAL_PROCESS", hrAdminRole.get());
			
			// POOLING - Cost Center
			if (listOfCSKTPooling.size() > 0) {
				nextProcess = SettlementProcess.WAITING_POOLING_RELEASE.val;
			}
			// Nothing POOLING - Cost Center
			else {
				nextProcess = SettlementProcess.WAITING_ACCOUNTING_RELEASE.val;
			}
		}
		else if (currentProcess == SettlementProcess.WAITING_POOLING_RELEASE.val) {
			if (!currentUser.get().getRoles().contains(poolingRole.get())) {
				message = CashAdvanceConstant.MESSAGE_INFO + " Approval must be "
						+ UserRole.ROLE_POOLING.roleName + ". Please check your role!";
				throw new Exception(message);
			}
			
			// Approval Pooling
			result.put("TRX_APPROVAL_PROCESS", poolingRole.get());
			
			// Add validation pooling all item
			for (CaEmpCSKT csktPooling : listOfCSKTPooling) {
				if (currentUser.get().getEmployee().getPernr().equals(csktPooling.getPernr())) {
					countNumberOfApprovedPooling++;
					if (!approvalProcessNote.isEmpty()) approvalProcessNote += ";";
					approvalProcessNote += "kostl=" + csktPooling.getKostlPool() + ",pernr=" + currentUser.get().getEmployee().getPernr();
				}
			}
			
			if (approvalProcessNote.isEmpty()) {
				message = CashAdvanceConstant.MESSAGE_INFO + " Unauthorized Approval Pooling Budget!";
				throw new Exception(message);
			}
			
			if (countNumberOfApprovedPooling == listOfCSKTPooling.size()) {
				nextProcess = SettlementProcess.WAITING_ACCOUNTING_RELEASE.val;
			}
			
			result.put("APPROVAL_PROCESS_NOTE", approvalProcessNote); // Approval Pooling
		}
		else if (currentProcess == SettlementProcess.WAITING_ACCOUNTING_RELEASE.val) {
			if (!currentUser.get().getRoles().contains(accountingRole.get())) {
				message = CashAdvanceConstant.MESSAGE_INFO + " Approval must be "
						+ UserRole.ROLE_ACCOUNTING.roleName + ". Please check your role!";
				throw new Exception(message);
			}
			
			// Approval by Role ACCOUNTING
			result.put("TRX_APPROVAL_PROCESS", accountingRole.get());
			
			// BALANCE
			if (SettlementCASStatus.BL.name().equals(settlementHeader.getCasStatus())) {
				nextProcess = SettlementProcess.READY_TO_SETTLE.val;
			}
			// GREATER THAN or LESS THAN
			else {
				nextProcess = SettlementProcess.WAITING_TREASURY_RELEASE.val;
			}
		}
		else if (currentProcess == SettlementProcess.WAITING_TREASURY_RELEASE.val) {
			if (!currentUser.get().getRoles().contains(treasuryRole.get())) {
				message = CashAdvanceConstant.MESSAGE_INFO + " Approval must be "
						+ UserRole.ROLE_TREASURY.roleName + ". Please check your role!";
				throw new Exception(message);
			}
			
			// Approval by Role TREASURY
			result.put("TRX_APPROVAL_PROCESS", treasuryRole.get());
			
			nextProcess = SettlementProcess.READY_TO_SETTLE.val;
		}
		else if (currentProcess == SettlementProcess.READY_TO_SETTLE.val) {
			if (!currentUser.get().getRoles().contains(treasuryRole.get())) {
				message = CashAdvanceConstant.MESSAGE_INFO + " Approval must be "
						+ UserRole.ROLE_TREASURY.roleName + ". Please check your role!";
				throw new Exception(message);
			}
			
			result.put("TRX_APPROVAL_PROCESS", treasuryRole.get());
			
			nextProcess = SettlementProcess.SETTLED.val;
		}
		
		result.put("PROCESS", nextProcess);
		return result;
	}

	/**
	 * 
	 * @param currentUserRequest
	 * @param caStlHdr
	 * @return IS_VALID (TRUE / FALSE), RESPONSE_MESSAGE is info message
	 */
	public WeakHashMap<String, String> validationProcessByCurrentUserRole(Optional<String> currentUserRequest, Optional<CaStlHdr> caStlHdr) {
		WeakHashMap<String, String> result = new WeakHashMap<String, String>();
		
		Optional<User> currentUser = userQueryService.findByUsername(currentUserRequest);
		Optional<Role> headRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_HEAD.roleIdent));
		Optional<Role> directorRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_DIRECTOR.roleIdent));
		Optional<Role> gaRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_GA.roleIdent));
		Optional<Role> hrAdminRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_ADMIN.roleIdent));
		Optional<Role> poolingRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_POOLING.roleIdent));
		Optional<Role> accountingRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_ACCOUNTING.roleIdent));
		Optional<Role> treasuryRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(UserRole.ROLE_TREASURY.roleIdent));
		
		// Validation Non Treasury
		if (!currentUser.get().getRoles().contains(treasuryRole.get())
				&& !currentUser.get().getEmployee().getPernr().equals(caStlHdr.get().getPernr()))  {
			
			boolean isSubOrdinate = false;
			
			// Validation Other employees
			if (!currentUser.get().getEmployee().getPernr().equals(caStlHdr.get().getPernr())) {
				// get All PERNR of Sub-Ordinates
				Long[] pernrs = null;
				List<Long> listOrPernr = userQueryService.fetchPernrByToPositionId(currentUser.get().getEmployee().getPosition().getId());
				if (!listOrPernr.isEmpty()) {
					pernrs = new Long[listOrPernr.size()];
					for (int idx=0; idx<listOrPernr.size(); idx++) {
						pernrs[idx] = new Long(""+listOrPernr.get(idx));
						// filter SSN, must be member of SubOrdinates, default isSubOrdinate = false
						if (caStlHdr.get().getPernr() != null && caStlHdr.get().getPernr().equals(pernrs[idx])) isSubOrdinate = true; 
					}
				}
			}
			
			if (SettlementProcess.WAITING_HEAD_RELEASE.val == caStlHdr.get().getProcess()) {
				// Validation Subordinate
				if (!isSubOrdinate) {
					result.put("IS_VALID", "FALSE");
					result.put("RESPONSE_MESSAGE", "Employee not your subordinate, access denied!");
					return result;
				}
				// Validation ROLE HEAD
				else if (!currentUser.get().getRoles().contains(headRole.get()))  {
					result.put("IS_VALID", "FALSE");
					result.put("RESPONSE_MESSAGE", "No Found " + UserRole.ROLE_HEAD.roleName + ", access denied!");
					return result;
				}
			}
			else if (SettlementProcess.WAITING_DIRECTOR_RELEASE.val == caStlHdr.get().getProcess()) {
				// Validation Subordinate
				if (!isSubOrdinate) {
					result.put("IS_VALID", "FALSE");
					result.put("RESPONSE_MESSAGE", "Employee not your subordinate, access denied!");
					return result;
				}
				// Validation ROLE DIRECTOR
				else if (!currentUser.get().getRoles().contains(directorRole.get()))  {
					result.put("IS_VALID", "FALSE");
					result.put("RESPONSE_MESSAGE", "No Found " + UserRole.ROLE_DIRECTOR.roleName + ", access denied!");
					return result;
				}
			}
			else if (SettlementProcess.WAITING_GA_RELEASE.val == caStlHdr.get().getProcess()) {
				if (!currentUser.get().getRoles().contains(gaRole.get()))  {
					result.put("IS_VALID", "FALSE");
					result.put("RESPONSE_MESSAGE", "No Found " + UserRole.ROLE_GA.roleName + ", access denied!");
					return result;
				}
			}
			else if (SettlementProcess.WAITING_HR_RELEASE.val == caStlHdr.get().getProcess()) {
				if (!currentUser.get().getRoles().contains(hrAdminRole.get()))  {
					result.put("IS_VALID", "FALSE");
					result.put("RESPONSE_MESSAGE", "No Found " + UserRole.ROLE_ADMIN.roleName + ", access denied!");
					return result;
				}
			}
			else if (SettlementProcess.WAITING_POOLING_RELEASE.val == caStlHdr.get().getProcess()) {
				if (!currentUser.get().getRoles().contains(poolingRole.get()))  {
					result.put("IS_VALID", "FALSE");
					result.put("RESPONSE_MESSAGE", "No Found " + UserRole.ROLE_POOLING.roleName + ", access denied!");
					return result;
				}
			}
			else if (SettlementProcess.WAITING_ACCOUNTING_RELEASE.val == caStlHdr.get().getProcess()) {
				if (!currentUser.get().getRoles().contains(accountingRole.get()))  {
					result.put("IS_VALID", "FALSE");
					result.put("RESPONSE_MESSAGE", "No Found " + UserRole.ROLE_ACCOUNTING.roleName + ", access denied!");
					return result;
				}
			}
			// Validation Subordinate
			else if (!isSubOrdinate) {
				result.put("IS_VALID", "FALSE");
				result.put("RESPONSE_MESSAGE", "Employee not your subordinate, access denied!");
				return result;
			}
		}// Validation Non Treasury
		
		// Allowed Role Treasury & self employee
		result.put("IS_VALID", "TRUE");
		result.put("RESPONSE_MESSAGE", "");
		return result;
	}
}
