package com.abminvestama.hcms;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import com.abminvestama.hcms.common.validation.aop.ValidateAspect;

/**
 * Application's main entry
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add muleConnectionTimeout, muleReadTimeout bean</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add attachmentBasePath, soapConnectionFactory, muleServiceHost, muleServicePort bean</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add mailSender, mailSession bean</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */

@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableScheduling
public class Application extends SpringBootServletInitializer {
	
	static Logger logger = LoggerFactory.getLogger(Application.class);
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	public static void main(String... args) {
		SpringApplication.run(Application.class, args);
	}
	
	/**
	 * needed to trigger JSR-303 (Bean Validation 1.0)
	 * @return
	 */
	@Bean 
	public LocalValidatorFactoryBean localValidatorFactoryBean() {
		return new LocalValidatorFactoryBean();
	}
	
	/**
	 * Needed to trigger JSR-349 (Bean Validation 1.1, i.e. Method Validation)
	 * @return
	 */
	@Bean
	public MethodValidationPostProcessor methodValidationPostProcessor() {
		return new MethodValidationPostProcessor();
	}	
	
	@Bean
	public ValidateAspect validateMethod() {
		return new ValidateAspect();
	}

    @Autowired
    Environment env;

    @Bean
    public JavaMailSenderImpl mailSender() {
    	JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    	mailSender.setHost(env.getProperty("mail.host"));
    	mailSender.setPort(env.getProperty("mail.port") != null ? Integer.parseInt(env.getProperty("mail.port")) : 25);
    	mailSender.setUsername(env.getProperty("mail.user"));
    	mailSender.setPassword(env.getProperty("mail.password"));
    	Properties props = new Properties();
    	props.put("mail.debug", StringUtils.defaultString(env.getProperty("mail.debug"), "false"));
    	props.put("mail.smtp.starttls.enable", StringUtils.defaultString(env.getProperty("mail.smtp.starttls.enable"), "false"));
    	props.put("mail.smtp.auth", StringUtils.defaultString(env.getProperty("mail.smtp.auth"), "false"));  // If you need to authenticate
    	props.put("mail.bcc", StringUtils.defaultString(env.getProperty("mail.bcc"), "baihaki.pru@gmail.com")); 
    	props.put("mail.notification.url", StringUtils.defaultString(env.getProperty("mail.notification.url"), "http://10.1.14.18:3000/"));
    	// Use the following if you need SSL
    	if (env.getProperty("mail.smtp.socketFactory.port") != null) 
    	    props.put("mail.smtp.socketFactory.port", env.getProperty("mail.smtp.socketFactory.port"));
    	if (env.getProperty("mail.smtp.socketFactory.class") != null)
    	    props.put("mail.smtp.socketFactory.class", env.getProperty("mail.smtp.socketFactory.class"));
    	if (env.getProperty("mail.smtp.socketFactory.fallback") != null)
    	    props.put("mail.smtp.socketFactory.fallback", env.getProperty("mail.smtp.socketFactory.fallback"));
		mailSender.setJavaMailProperties(props);
        return mailSender;
    }
    
    @Bean
    public Session mailSession() {
    	Session session = Session.getDefaultInstance(mailSender().getJavaMailProperties());
    	/*
    	FileOutputStream fout = null;
		try {
			fout = new FileOutputStream("/user/etc/hcms/logs/testout.txt");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
    	BufferedOutputStream os = new BufferedOutputStream(fout);
    	PrintStream ps = new PrintStream(os);
    	logger.info("JAVAMAIL debug mode is ON");
    	session.setDebugOut(ps);
    	session.setDebug(true);
    	*/
    	MimeMessage message = new MimeMessage(session);
    	try {
    		message.setFrom(new InternetAddress(StringUtils.defaultString(mailSender().getUsername(), "hcms@abm-investama.co.id")));
        	//message.setRecipients(RecipientType.TO, "devid.akmalhadi@abm-investama.co.id");
    		message.setRecipients(RecipientType.TO, mailSender().getJavaMailProperties().getProperty("mail.bcc"));
        	message.setSubject("ABM HCMS MailService is initialized");
    		message.setContent("<H1>ABM HCMS MailService</H1> is initialized and will be running.", "text/html");
			mailSender().send(message);
			//ps.flush();
		} catch (MessagingException e) {
        	System.out.println("MessagingException:" + e.getMessage());
		} catch (MailException e) {
        	System.out.println("MailException:" + e.getMessage());
		}
    	return session;
    }
    
    @Bean
    public String attachmentBasePath() {
    	return env.getProperty("attachment.basepath", "/user/etc/hcms/attachment/");
    }
    
    @Bean
    public SOAPConnectionFactory soapConnectionFactory() {
    	SOAPConnectionFactory soapConnectionFactory = null;
    	try {
    		soapConnectionFactory = SOAPConnectionFactory.newInstance();
		} catch (UnsupportedOperationException | SOAPException e) {
			e.printStackTrace();
		}
    	return soapConnectionFactory;
    }
    
    @Bean
    public String muleServiceHost() {
    	return env.getProperty("mule.host", "localhost");
    }
    
    @Bean
    public Integer muleServicePort() {
    	Integer port;
    	String portStr = env.getProperty("mule.port");
    	try {
    	     port = Integer.valueOf(portStr);
    	} catch (Exception e) {
    		port = 8100;
    	}
    	return port;
    }
    
    @Bean
    public Integer muleConnectionTimeout() {
    	Integer timeout;
    	String timeoutStr = env.getProperty("mule.connection.timeout");
    	try {
    		timeout = Integer.valueOf(timeoutStr);
    	} catch (Exception e) {
    		// set default connection timeout to 5 seconds
    		timeout = 5000;
    	}
    	return timeout;
    }
    
    @Bean
    public Integer muleReadTimeout() {
    	Integer timeout;
    	String timeoutStr = env.getProperty("mule.read.timeout");
    	try {
    		timeout = Integer.valueOf(timeoutStr);
    	} catch (Exception e) {
    		// set default read timeout to 15 seconds
    		timeout = 15000;
    	}
    	return timeout;
    }

}