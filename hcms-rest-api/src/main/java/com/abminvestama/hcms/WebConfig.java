package com.abminvestama.hcms;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.abminvestama.hcms.rest.api.interceptor.ApprovalHandlerInterceptor;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@EnableWebMvc
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {
	
	@Bean
	public ApprovalHandlerInterceptor approvalHandlerInterceptor() {
		return new ApprovalHandlerInterceptor();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(approvalHandlerInterceptor())
			.addPathPatterns("/api/v1/**/approve", "/api/v2/**/approve");
	}
}