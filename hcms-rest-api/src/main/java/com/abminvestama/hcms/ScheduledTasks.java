package com.abminvestama.hcms;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.impl.business.query.OutstandingQueryServiceImpl;
import com.abminvestama.hcms.core.service.util.MailService;

@Component
public class ScheduledTasks {
	
	private String mailHeader = "Dear Bapak/Ibu <b>EMPLOYEE_NAME</b><br><br>";
	private String mailContent = "You have OUTSTANDING_NUMBER Outstanding Task of INFOTYPE_DESCRIPTION request waiting for your approval.<br><br>";
	private String mailFooter = "Please review the Outstanding Task and perform your approval <a href=\"CLIENT_URL\">Here</a>";
	
	private UserQueryService userQueryService;
	private OutstandingQueryServiceImpl outstandingQueryService;
	private MailService mailService;
	
	@Autowired
    Environment env;
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	public void setOutstandingQueryServiceImpl(OutstandingQueryServiceImpl outstandingQueryService) {
		this.outstandingQueryService = outstandingQueryService;
	}

	@Autowired
	void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

    @Autowired
    private JavaMailSender mailSender;
	
	@Value("${scheduled.outstanding.bukrs:'3000','9900'}")
	private String outstandingBukrs;
	
    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
       
    @Scheduled(cron = "${scheduled.outstanding.cron:0 0 7 * * *}")
    public void outstandNotificationLeave() {
		String url = ((JavaMailSenderImpl) mailSender).getJavaMailProperties().getProperty("mail.notification.url", "http://10.1.14.18:3000/");
		
    	String queryIT2001 = "select (select to_char(pernr, '99999') from it0001 where m_position_id = c.to_position_id order by pernr limit 1) as bukrs, to_char(count(b.pernr), '999') " +
    			"as butxt from it2001 a inner join it0001 b on a.pernr = b.pernr " +
                "inner join position_relations c on b.m_position_id = c.from_position_id " +
    			"where a.process='1' and b.bukrs in (".concat(outstandingBukrs).concat(") ") +
    			"group by 1 order by 1";
    	outstandingQueryService.setQuery(queryIT2001);
    	Optional<Collection<T001>> listSuperior = outstandingQueryService.fetchAll();
    	if (listSuperior.isPresent()) {
    		ArrayList<T001> list = (ArrayList<T001>) listSuperior.get().stream().collect(Collectors.toList());
    		for (T001 outstanding : list) {
    			Optional<User> user = userQueryService.findByPernr(Long.valueOf(outstanding.getBukrs().trim()));
    			if (user.isPresent()) {
    				String mailBody = mailHeader.replace("EMPLOYEE_NAME", user.get().getEmployee().getSname()).concat(
    						mailContent.replace("OUTSTANDING_NUMBER", outstanding.getButxt().trim()).replace("INFOTYPE_DESCRIPTION", "Leave")).concat(
    						mailFooter.replace("CLIENT_URL", url.concat("tasks/tasks_absence_attendance/page/1?status=waiting-superior-release")));
    				String mailRecipient = /*"test." + */user.get().getEmail();
    				if (user.get().getEmployee().getT528t().getPlstx().toUpperCase().contains("DIRECTOR")) {
    					mailRecipient = env.getProperty("scheduled.outstanding.director.email.".concat(user.get().getEmployee().getT500p().getBukrs().getBukrs()), mailRecipient);
    				}
    				mailService.sendMail(mailRecipient, "Task Reminder of Leave Request", mailBody);
    			}
    		}
    	}
    }
    
    @Scheduled(cron = "${scheduled.outstanding.cron:0 0 7 * * *}")
    public void outstandNotificationExpense() {
		String url = ((JavaMailSenderImpl) mailSender).getJavaMailProperties().getProperty("mail.notification.url", "http://10.1.14.18:3000/");
		
    	String queryPtrvHeadSuperior = "select (select to_char(pernr, '99999') from it0001 where m_position_id = c.to_position_id order by pernr limit 1) as bukrs, to_char(count(b.pernr), '999') " +
    			"as butxt from ptrv_head a inner join it0001 b on a.pernr = b.pernr " +
                "inner join position_relations c on b.m_position_id = c.from_position_id " +
    			"where a.process='2' and b.bukrs in (".concat(outstandingBukrs).concat(") ") +
    			"group by 1 order by 1";
    	outstandingQueryService.setQuery(queryPtrvHeadSuperior);
    	Optional<Collection<T001>> listOutstanding = outstandingQueryService.fetchAll();
    	if (listOutstanding.isPresent()) {
    		ArrayList<T001> list = (ArrayList<T001>) listOutstanding.get().stream().collect(Collectors.toList());
    		for (T001 outstanding : list) {
    			Optional<User> user = userQueryService.findByPernr(Long.valueOf(outstanding.getBukrs().trim()));
    			if (user.isPresent()) {
    				String mailBody = mailHeader.replace("EMPLOYEE_NAME", user.get().getEmployee().getSname()).concat(
    						mailContent.replace("OUTSTANDING_NUMBER", outstanding.getButxt().trim()).replace("INFOTYPE_DESCRIPTION", "Expense Claim")).concat(
    						mailFooter.replace("CLIENT_URL", url.concat("tasks/tasks_employee_cost/page/1?status=waiting-superior-release")));
    				String mailRecipient = /*"test." + */user.get().getEmail();
    				if (user.get().getEmployee().getT528t().getPlstx().toUpperCase().contains("DIRECTOR")) {
    					mailRecipient = env.getProperty("scheduled.outstanding.director.email.".concat(user.get().getEmployee().getT500p().getBukrs().getBukrs()), mailRecipient);
    				}
    				mailService.sendMail(mailRecipient, "Task Reminder of Expense Claim Request", mailBody);
    			}
    		}
    	}
    	
    	String queryPtrvHeadAdmin = "select b.bukrs, to_char(count(b.bukrs), '999') as butxt from ptrv_head a " +
    	        "inner join it0001 b on a.pernr = b.pernr " +
                "where a.process='1' and b.bukrs in (".concat(outstandingBukrs).concat(") ") +
                "group by 1 order by 1";
    	outstandingQueryService.setQuery(queryPtrvHeadAdmin);
    	listOutstanding = outstandingQueryService.fetchAll();
    	if (listOutstanding.isPresent()) {
    		ArrayList<T001> list = (ArrayList<T001>) listOutstanding.get().stream().collect(Collectors.toList());
    		for (T001 outstanding : list) {
    			Optional<Collection<User>> users = userQueryService.fetchAdminByCompany(outstanding.getBukrs());
    			if (users.isPresent()) {
    				List<User> adminUsers = users.get().stream().collect(Collectors.toList());
    				for (User adminUser : adminUsers) {
    					String mailBody = mailHeader.replace("EMPLOYEE_NAME", "HR ADMIN").concat(
    							mailContent.replace("OUTSTANDING_NUMBER", outstanding.getButxt().trim()).replace("INFOTYPE_DESCRIPTION", "Expense Claim")).concat(
    		    						mailFooter.replace("CLIENT_URL", url.concat("tasks/tasks_employee_cost/page/1?status=waiting-admin-release")));
    		    				mailService.sendMail(/*"test." + */adminUser.getEmail(), "Task Reminder of Expense Claim Request", mailBody);
    				}
    			}
    		}
    	}
    	
    }
    
    @Scheduled(cron = "${scheduled.outstanding.cron:0 0 7 * * *}")
    public void outstandNotificationTeven() {
		String url = ((JavaMailSenderImpl) mailSender).getJavaMailProperties().getProperty("mail.notification.url", "http://10.1.14.18:3000/");
		
    	String queryTevenSuperior = "select (select to_char(pernr, '99999') from it0001 where m_position_id = c.to_position_id order by pernr limit 1) as bukrs, to_char(count(b.pernr), '999') " +
    			"as butxt from tevenvw a inner join it0001 b on a.pernr = b.pernr " +
                "inner join position_relations c on b.m_position_id = c.from_position_id " +
    			"where (a.begtrxcode LIKE '_S1' or a.endtrxcode LIKE '_S1') and b.bukrs in (".concat(outstandingBukrs).concat(") ") +
    			"group by 1 order by 1";
    	outstandingQueryService.setQuery(queryTevenSuperior);
    	Optional<Collection<T001>> listOutstanding = outstandingQueryService.fetchAll();
    	if (listOutstanding.isPresent()) {
    		ArrayList<T001> list = (ArrayList<T001>) listOutstanding.get().stream().collect(Collectors.toList());
    		for (T001 outstanding : list) {
    			Optional<User> user = userQueryService.findByPernr(Long.valueOf(outstanding.getBukrs().trim()));
    			if (user.isPresent()) {
    				String mailBody = mailHeader.replace("EMPLOYEE_NAME", user.get().getEmployee().getSname()).concat(
    						mailContent.replace("OUTSTANDING_NUMBER", outstanding.getButxt().trim()).replace("INFOTYPE_DESCRIPTION", "Attendance")).concat(
    						mailFooter.replace("CLIENT_URL", url.concat("tasks/tasks_time_events?pg=1&sts=Waiting for Superior")));
    				String mailRecipient = /*"test." + */user.get().getEmail();
    				if (user.get().getEmployee().getT528t().getPlstx().toUpperCase().contains("DIRECTOR")) {
    					mailRecipient = env.getProperty("scheduled.outstanding.director.email.".concat(user.get().getEmployee().getT500p().getBukrs().getBukrs()), mailRecipient);
    				}
    				mailService.sendMail(mailRecipient, "Task Reminder of Attendance Request", mailBody);
    			}
    		}
    	}
    	
    	String queryTevenAdmin = "select b.bukrs, to_char(count(b.bukrs), '999') as butxt from tevenvw a " +
    	        "inner join it0001 b on a.pernr = b.pernr " +
                "where (a.begtrxcode LIKE '_S2' or a.endtrxcode LIKE '_S2') and b.bukrs in (".concat(outstandingBukrs).concat(") ") +
                "group by 1 order by 1";
    	outstandingQueryService.setQuery(queryTevenAdmin);
    	listOutstanding = outstandingQueryService.fetchAll();
    	if (listOutstanding.isPresent()) {
    		ArrayList<T001> list = (ArrayList<T001>) listOutstanding.get().stream().collect(Collectors.toList());
    		for (T001 outstanding : list) {
    			Optional<Collection<User>> users = userQueryService.fetchAdminByCompany(outstanding.getBukrs());
    			if (users.isPresent()) {
    				List<User> adminUsers = users.get().stream().collect(Collectors.toList());
    				for (User adminUser : adminUsers) {
    					String mailBody = mailHeader.replace("EMPLOYEE_NAME", "HR ADMIN").concat(
    							mailContent.replace("OUTSTANDING_NUMBER", outstanding.getButxt().trim()).replace("INFOTYPE_DESCRIPTION", "Attendance")).concat(
    		    						mailFooter.replace("CLIENT_URL", url.concat("tasks/tasks_time_events?pg=1&sts=Waiting for HR Admin")));
    		    				mailService.sendMail(/*"test." + */adminUser.getEmail(), "Task Reminder of Attendance Request", mailBody);
    				}
    			}
    		}
    	}
    	
    }
    
    @Scheduled(cron = "${scheduled.outstanding.cron:0 0 7 * * *}")
    public void outstandNotificationMedical() {
		String url = ((JavaMailSenderImpl) mailSender).getJavaMailProperties().getProperty("mail.notification.url", "http://10.1.14.18:3000/");
    	
    	String queryMedicalAdmin = "select b.bukrs, to_char(count(b.bukrs), '999') as butxt from zmed_trxhdr a " +
    	        "inner join it0001 b on a.pernr = b.pernr " +
                "where a.process='1' and b.bukrs in (".concat(outstandingBukrs).concat(") ") +
                "group by 1 order by 1";
    	outstandingQueryService.setQuery(queryMedicalAdmin);
    	Optional<Collection<T001>> listOutstanding = outstandingQueryService.fetchAll();
    	if (listOutstanding.isPresent()) {
    		ArrayList<T001> list = (ArrayList<T001>) listOutstanding.get().stream().collect(Collectors.toList());
    		for (T001 outstanding : list) {
    			Optional<Collection<User>> users = userQueryService.fetchAdminByCompany(outstanding.getBukrs());
    			if (users.isPresent()) {
    				List<User> adminUsers = users.get().stream().collect(Collectors.toList());
    				for (User adminUser : adminUsers) {
    					String mailBody = mailHeader.replace("EMPLOYEE_NAME", "HR ADMIN").concat(
    							mailContent.replace("OUTSTANDING_NUMBER", outstanding.getButxt().trim()).replace("INFOTYPE_DESCRIPTION", "Medical Claim")).concat(
    		    						mailFooter.replace("CLIENT_URL", url.concat("tasks/tasks_medical/page/1?status=waiting-for-approve")));
    		    				mailService.sendMail(/*"test." + */adminUser.getEmail(), "Task Reminder of Medical Claim Request", mailBody);
    				}
    			}
    		}
    	}
    	
    }
    
    
}
