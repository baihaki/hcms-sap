package com.abminvestama.hcms.common.util;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Fix method: isDifferentNumberValues (convert to double, for precision compare)</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Fix method: isDifferentNumberValues (compare "null" value to "not null" value)</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Fix method: isDifferentDateValues (compare "null" value to "not null" value)</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public final class CommonComparatorFunction {

	public static final boolean isDifferentStringValues(String s1, String s2) {
		return (StringUtils.isNotBlank(s1) && !s1.trim().equalsIgnoreCase(s2 != null ? s2.trim() : ""));
	}
	
	public static final boolean isDifferentDateValues(Date d1, Date d2) {
		if ((d1 == null && d2 != null) || (d2 == null && d1 != null))
			return true;
		return (d1 != null && d2 != null 
				&& d1.compareTo(d2) != 0);
	}
	
	public static final boolean isDifferentCharacterValues(char c1, char c2) {
		return c1 != c2;
	}
	
	public static final boolean isDifferentFloatingPointNumberValues(Number n1, Number n2) {
		return n1.doubleValue() != n2.doubleValue();
	}
	
	public static final boolean isDifferentNumberValues(Number n1, Number n2) {
		if (n2 == null ^ n1 == null)
			return true;
		if (n1 == null && n2 == null)
			return false;
		//return n1.longValue() != n2.longValue();
		return n1.doubleValue() != n2.doubleValue();
	}
}