CREATE TABLE TRX_EVENT_LOG (
  id character varying(255) NOT NULL,
  cfg_entity_id character varying(255) NOT NULL,
  entity_object_id character varying(255),
  operation_type character varying(255) NOT NULL,
  event_data text not null,
  created_at timestamp with time zone not null default timezone('utc'::text, now()),
  created_by_id character varying(255) NOT NULL
);

COMMENT ON TABLE TRX_EVENT_LOG IS 'Transaction Event Logs';
COMMENT ON COLUMN TRX_EVENT_LOG.cfg_entity_id IS 'FK to table CFG_ENTITY';
COMMENT ON COLUMN TRX_EVENT_LOG.entity_object_id IS 'id (PK) of object. e.g. : m_positions.id';
COMMENT ON COLUMN TRX_EVENT_LOG.operation_type IS 'User operation. e.g. : Create, Update';
COMMENT ON COLUMN TRX_EVENT_LOG.event_data IS 'data performed (create/update) by user, which wrapped in JSON format.';

ALTER TABLE TRX_EVENT_LOG ADD PRIMARY KEY (id);
ALTER TABLE TRX_EVENT_LOG ADD CONSTRAINT operation_type_check CHECK (operation_type IN ('CREATE', 'UPDATE', 'DELETE'));
ALTER TABLE TRX_EVENT_LOG ADD FOREIGN KEY (cfg_entity_id) REFERENCES CFG_ENTITY (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE TRX_EVENT_LOG ADD FOREIGN KEY (created_by_id) REFERENCES m_user (id) ON UPDATE CASCADE ON DELETE RESTRICT;
