﻿CREATE TABLE ca_group_account
(
  id bigint NOT NULL,
  group_acct_text character varying(255),
  blocked_for_posting boolean,
  valuation_group character varying(20),
  CONSTRAINT ca_group_account_pkey PRIMARY KEY (id)
);
CREATE SEQUENCE group_account_serial OWNED BY ca_group_account.id;
ALTER TABLE ONLY ca_group_account ALTER COLUMN id SET DEFAULT nextval('group_account_serial'::regclass);
COMMENT ON COLUMN ca_group_account.id IS 'ID of Group Account';
COMMENT ON COLUMN ca_group_account.group_acct_text IS 'Group Account Text/Description';
COMMENT ON COLUMN ca_group_account.blocked_for_posting IS 'Blocked for Posting';
COMMENT ON COLUMN ca_group_account.valuation_group IS 'Valuation Group';

CREATE TABLE ca_gl_account
(
  id bigint NOT NULL,
  gl_acct_text character varying(255),
  group_acct_id bigint NOT NULL,
  balance_sheet_acct boolean,
  begda date,
  endda date,
  CONSTRAINT ca_gl_account_pkey PRIMARY KEY (id),
  CONSTRAINT ca_gl_account_group_acct_id_fkey FOREIGN KEY (group_acct_id)
      REFERENCES ca_group_account (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);
CREATE SEQUENCE gl_account_serial OWNED BY ca_gl_account.id;
ALTER TABLE ONLY ca_gl_account ALTER COLUMN id SET DEFAULT nextval('gl_account_serial'::regclass);
COMMENT ON COLUMN ca_gl_account.id IS 'ID of GL Account';
COMMENT ON COLUMN ca_gl_account.gl_acct_text IS 'GL Account Text/Description';
COMMENT ON COLUMN ca_gl_account.group_acct_id IS 'Group Account ID';
COMMENT ON COLUMN ca_gl_account.balance_sheet_acct IS 'Balance Sheet Account';
COMMENT ON COLUMN ca_gl_account.begda IS 'GL Account Begin Date';
COMMENT ON COLUMN ca_gl_account.endda IS 'GL Account End Date';

CREATE TABLE ca_depts_cost_center
(
  id bigint NOT NULL,
  orgeh bigint,
  endda timestamp with time zone,
  kokrs character varying(10),
  kostl character varying(10),
  CONSTRAINT ca_depts_cost_center_pkey PRIMARY KEY (id),
  CONSTRAINT ca_depts_cost_center_orgeh_fkey FOREIGN KEY (orgeh, endda)
      REFERENCES t527x (orgeh, endda) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT ca_depts_cost_center_kostl_fkey FOREIGN KEY (kostl, kokrs)
      REFERENCES cskt (kostl, kokrs) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);
CREATE SEQUENCE depts_cost_center_serial OWNED BY ca_depts_cost_center.id;
ALTER TABLE ONLY ca_depts_cost_center ALTER COLUMN id SET DEFAULT nextval('depts_cost_center_serial'::regclass);
COMMENT ON COLUMN ca_depts_cost_center.id IS 'ID of Departments Cost Center';
COMMENT ON COLUMN ca_depts_cost_center.orgeh IS 'Organizational Unit (Department)';
COMMENT ON COLUMN ca_depts_cost_center.endda IS 'Organizational Unit (Department) End Date';
COMMENT ON COLUMN ca_depts_cost_center.kokrs IS 'Controlling Area Code of Cost Center';
COMMENT ON COLUMN ca_depts_cost_center.kostl IS 'Cost Center Code';

CREATE TABLE ca_order
(
  id bigint NOT NULL, -- ID of GL Account Order
  description character varying(255), -- GL Account Order Description
  begda date, -- GL Account Order Begin Date
  endda date, -- GL Account Order End Date
  kokrs character varying(10), -- Cost Center Area
  kostl character varying(10), -- Cost Center Code
  gl_account_id bigint,
  CONSTRAINT ca_order_pkey PRIMARY KEY (id),
  CONSTRAINT ca_order_cost_center_fkey FOREIGN KEY (kokrs, kostl)
      REFERENCES cskt (kokrs, kostl) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT ca_order_gl_account_fkey FOREIGN KEY (gl_account_id)
      REFERENCES ca_gl_account (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);
CREATE SEQUENCE order_serial OWNED BY ca_order.id;
ALTER TABLE ONLY ca_order ALTER COLUMN id SET DEFAULT nextval('order_serial'::regclass);
COMMENT ON COLUMN ca_order.id IS 'ID of GL Account Order';
COMMENT ON COLUMN ca_order.description IS 'GL Account Order Description';
COMMENT ON COLUMN ca_order.begda IS 'GL Account Order Begin Date';
COMMENT ON COLUMN ca_order.endda IS 'GL Account Order End Date';
COMMENT ON COLUMN ca_order.kokrs IS 'Cost Center Area';
COMMENT ON COLUMN ca_order.kostl IS 'Cost Center Code';

CREATE TABLE ca_location_type
(
  id bigint NOT NULL,
  location_type_text character varying(255),
  CONSTRAINT ca_location_type_pkey PRIMARY KEY (id)
);
CREATE SEQUENCE location_type_serial OWNED BY ca_location_type.id;
ALTER TABLE ONLY ca_location_type ALTER COLUMN id SET DEFAULT nextval('location_type_serial'::regclass);
COMMENT ON COLUMN ca_location_type.id IS 'ID of Location Type';
COMMENT ON COLUMN ca_location_type.location_type_text IS 'Location Type Text/Description';

CREATE TABLE ca_location_identifier
(
  id character(8) NOT NULL DEFAULT left((uuid_generate_v4())::text, 8),
  location_code character varying(5) NOT NULL,
  location_text character varying(255) NOT NULL,
  country_code character varying(3) NOT NULL,
  location_type_id bigint NOT NULL,
  operate boolean DEFAULT 'true',
  CONSTRAINT ca_location_identifier_pkey PRIMARY KEY (id),
  CONSTRAINT ca_location_identifier_country_code_fkey FOREIGN KEY (country_code)
      REFERENCES t005t (land1) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT ca_location_identifier_location_type_id_fkey FOREIGN KEY (location_type_id)
      REFERENCES ca_location_type (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT ca_location_identifier_location_code UNIQUE (location_code)
);
COMMENT ON COLUMN ca_location_identifier.id IS 'ID of Location Identifier';
COMMENT ON COLUMN ca_location_identifier.location_code IS 'Location Code';
COMMENT ON COLUMN ca_location_identifier.location_text IS 'Location Text/Description';
COMMENT ON COLUMN ca_location_identifier.country_code IS 'Country Code';
COMMENT ON COLUMN ca_location_identifier.location_type_id IS 'Location Type ID';
COMMENT ON COLUMN ca_location_identifier.operate IS 'Location is Operate';

/*  TRANSACTION TABLES */

CREATE TABLE ca_req_hdr
(
  req_no character(13) NOT NULL DEFAULT left((uuid_generate_v4())::text, 13),
  pernr bigint NOT NULL,
  amount numeric(15,2),
  currency character varying(5) NOT NULL,
  description character varying(255) NOT NULL,
  attachment character varying(255),
  bank_name character varying(60),
  bank_account character varying(100),
  process integer NOT NULL DEFAULT 0,
  notes character varying(1024),
  advance_code character(2) NOT NULL DEFAULT 'PC'::text,
  bukrs character varying(10),
  created_at timestamp with time zone NOT NULL DEFAULT timezone('utc'::text, now()),
  created_by character varying(255) NOT NULL,
  updated_at timestamp with time zone NOT NULL DEFAULT timezone('utc'::text, now()),
  updated_by character varying(255) NOT NULL,
  approved1_by character varying(255),
  approved1_at timestamp with time zone,
  approved2_by character varying(255),
  approved2_at timestamp with time zone,
  approved3_by character varying(255),
  approved3_at timestamp with time zone,
  approved4_by character varying(255),
  approved4_at timestamp with time zone,
  approved_accounting_by character varying(255),
  approved_accounting_at timestamp with time zone,
  approved_treasury_by character varying(255),
  approved_treasury_at timestamp with time zone,
  received_at timestamp with time zone,
  acct_ap character varying(255),
  reason character varying(1024),
  payment_status character varying(1),
  payment_date date,
  docno character varying(10),
  last_sync timestamp with time zone,
  payment_status2 character varying(1),
  docno2 character varying(10),
  sap_message character varying(255),
  CONSTRAINT ca_req_hdr_pkey PRIMARY KEY (req_no),
  CONSTRAINT ca_req_hdr_advance_code_check CHECK (advance_code::text = ANY (ARRAY['PC', 'BT', 'TA', 'RE']))
);
COMMENT ON COLUMN ca_req_hdr.req_no IS 'Cash Advance Request Number';
COMMENT ON COLUMN ca_req_hdr.amount IS 'Total Request Amount';
COMMENT ON COLUMN ca_req_hdr.description IS 'Request Description';
COMMENT ON COLUMN ca_req_hdr.attachment IS 'Attachment Path';
COMMENT ON COLUMN ca_req_hdr.bank_name IS 'Received Bank Name';
COMMENT ON COLUMN ca_req_hdr.bank_account IS 'Received Bank Account Number';
COMMENT ON COLUMN ca_req_hdr.process IS 'Current Process of Workflow';
COMMENT ON COLUMN ca_req_hdr.notes IS 'Request Notes';
COMMENT ON COLUMN ca_req_hdr.advance_code IS 'Cash Advance code (PC=Petty Cash | BT=Bank Transfer | TA=Travel Advance | RE=Reimbursement)';

CREATE TABLE ca_req_dtl
(
  item_no smallint NOT NULL,
  req_no character(13) NOT NULL,
  description character varying(50),
  price_per_unit numeric(15,2),
  quantity integer NOT NULL DEFAULT 1,
  amount numeric(15,2),
  remarks character varying(255),
  created_at timestamp with time zone NOT NULL DEFAULT timezone('utc'::text, now()),
  created_by character varying(255) NOT NULL,
  updated_at timestamp with time zone NOT NULL DEFAULT timezone('utc'::text, now()),
  updated_by character varying(255) NOT NULL,
  CONSTRAINT ca_req_dtl_pkey PRIMARY KEY (item_no, req_no),
  CONSTRAINT ca_req_dtl_req_no_fkey FOREIGN KEY (req_no)
      REFERENCES ca_req_hdr (req_no) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);
COMMENT ON COLUMN ca_req_dtl.item_no IS 'Cash Advance Request Detail (Item Number)';
COMMENT ON COLUMN ca_req_dtl.req_no IS 'Cash Advance Request Number (Header Number)';
COMMENT ON COLUMN ca_req_dtl.description IS 'Request Detail Description';
COMMENT ON COLUMN ca_req_dtl.price_per_unit IS 'Price per Unit (Currency on Request Header)';
COMMENT ON COLUMN ca_req_dtl.quantity IS 'Quantity of Request Detail / Item';
COMMENT ON COLUMN ca_req_dtl.amount IS 'Total Amount of Request Detail / Item (Currency on Request Header)';
COMMENT ON COLUMN ca_req_dtl.remarks IS 'Remarks of Request Detail / Item';

CREATE TABLE ca_travel_arg_dtl
(
  item_no smallint NOT NULL,
  req_no character(13) NOT NULL,
  arg_date date,
  departure_location_id character(8),
  arrival_location_id character(8),
  duration integer,
  remarks character varying(255),
  arg_type integer,
  created_at timestamp with time zone NOT NULL DEFAULT timezone('utc'::text, now()),
  created_by character varying(255) NOT NULL,
  updated_at timestamp with time zone NOT NULL DEFAULT timezone('utc'::text, now()),
  updated_by character varying(255) NOT NULL,
  CONSTRAINT ca_travel_arg_dtl_pkey PRIMARY KEY (item_no, req_no),
  CONSTRAINT ca_travel_arg_dtl_req_no_fkey FOREIGN KEY (req_no)
      REFERENCES ca_req_hdr (req_no) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT ca_travel_arg_dtl_departure_fkey FOREIGN KEY (departure_location_id)
      REFERENCES ca_location_identifier (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT ca_travel_arg_dtl_arrival_fkey FOREIGN KEY (arrival_location_id)
      REFERENCES ca_location_identifier (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);
COMMENT ON COLUMN ca_travel_arg_dtl.item_no IS 'Travel Arrangement Detail (Item Number)';
COMMENT ON COLUMN ca_travel_arg_dtl.req_no IS 'Cash Advance Request Number (Header Number)';
COMMENT ON COLUMN ca_travel_arg_dtl.arg_date IS 'Travel Arrangement Date';
COMMENT ON COLUMN ca_travel_arg_dtl.departure_location_id IS 'Departure Location Identifier ID';
COMMENT ON COLUMN ca_travel_arg_dtl.arrival_location_id IS 'Arrival Location Identifier ID';
COMMENT ON COLUMN ca_travel_arg_dtl.duration IS 'Duration of Travel Arrangement Detail';
COMMENT ON COLUMN ca_travel_arg_dtl.remarks IS 'Remarks of Travel Arrangement Detail';
COMMENT ON COLUMN ca_travel_arg_dtl.arg_type IS 'Travel Arrangement Type';
