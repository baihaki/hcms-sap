CREATE TABLE IT0019 (
  pernr bigint not null,
  subty character varying(4) not null,
  endda date not null, 
  begda date not null,
  seqnr bigint default 0,
  aedtm timestamp with time zone not null default timezone('utc'::text, now()),
  uname character varying(255),
  itxex character varying(5),
  tmart character varying(5) not null,
  termn date,
  mndat date,
  bvmrk character varying(5),
  tmjhr integer,
  tmmon smallint,
  tmtag smallint,
  mnjhr integer,
  mnmon smallint,
  mntag smallint,
  infty character varying(10)
);

COMMENT ON TABLE IT0019 IS 'Master data transaction for Monitoring of Tasks';

COMMENT ON COLUMN IT0019.pernr IS 'Employee ID/SSN';

COMMENT ON COLUMN IT0019.subty IS 'Subtype';

COMMENT ON COLUMN IT0019.endda IS 'End Date';

COMMENT ON COLUMN IT0019.begda IS 'Begin Date';

COMMENT ON COLUMN IT0019.seqnr IS 'Infotype Record No';

COMMENT ON COLUMN IT0019.aedtm IS 'Changed On';

COMMENT ON COLUMN IT0019.uname IS 'Changed By';

COMMENT ON COLUMN IT0019.itxex IS 'Text Exists';

COMMENT ON COLUMN IT0019.tmart IS 'Task Type';

COMMENT ON COLUMN IT0019.termn IS 'Date of Task';

COMMENT ON COLUMN IT0019.mndat IS 'Reminder Date';

COMMENT ON COLUMN IT0019.bvmrk IS 'Processing Indicator';

COMMENT ON COLUMN IT0019.tmjhr IS 'Year of Date';

COMMENT ON COLUMN IT0019.TMMON IS 'Month of Date';

COMMENT ON COLUMN IT0019.TMTAG IS 'Day of Date';

COMMENT ON COLUMN IT0019.MNJHR IS 'Year of Reminder';

COMMENT ON COLUMN IT0019.MNMON IS 'Month of Reminder';

COMMENT ON COLUMN IT0019.MNTAG IS 'Day of Reminder';

COMMENT ON COLUMN IT0019.infty IS 'Infotype';
