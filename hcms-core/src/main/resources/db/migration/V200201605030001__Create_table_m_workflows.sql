CREATE TABLE m_workflows (
id character varying(255) NOT NULL DEFAULT uuid_generate_v1mc(),
opt_lock bigint default 0,
name character varying(255) NOT NULL,
description text,
created_at timestamp with time zone not null default timezone('utc'::text, now()),
created_by_id character varying(255),
updated_at timestamp with time zone not null default timezone('utc'::text, now()),
updated_by_id character varying(255),
deleted_at timestamp with time zone,
deleted_by_id character varying(255)
);

COMMENT ON TABLE m_workflows IS 'Master table for Workflows';

COMMENT ON COLUMN m_workflows.name IS 'Workflow Name';

COMMENT ON COLUMN m_workflows.description IS 'Workflow Description';

COMMENT ON COLUMN m_workflows.created_at IS 'Date of created';

COMMENT ON COLUMN m_workflows.created_by_id IS 'Created By';

COMMENT ON COLUMN m_workflows.updated_at IS 'Last modified';

COMMENT ON COLUMN m_workflows.updated_by_id IS 'Updated By';

COMMENT ON COLUMN m_workflows.deleted_at IS 'Deleted date';

COMMENT ON COLUMN m_workflows.deleted_by_id IS 'Deleted By';
