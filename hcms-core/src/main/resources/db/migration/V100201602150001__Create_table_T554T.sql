CREATE TABLE T554T (
  AWART character varying(10) not null,
  ATEXT character varying(50) not null
);

ALTER TABLE T554T ADD PRIMARY KEY (AWART);

COMMENT ON TABLE T554T IS 'Master data Absence Type';

COMMENT ON COLUMN T554T.AWART IS 'Absence Type Code';

COMMENT ON COLUMN T554T.ATEXT IS 'Absence Type Name/Text';
