DROP VIEW public.tevenvw;

CREATE OR REPLACE VIEW public.tevenvw AS 
	SELECT
		id,
		pernr, 		-- Employee Personnel Number
		ename, 		-- Employee name - IT0001
		ldate, 		-- Logical date
		begagtime,	-- Begin time actual(machine)
		endagtime,	-- End time actual(machine)
		begltime,	-- Begin logical time
		endltime,	-- End logical time
		wdhour,		-- Working Delay Hour
		(CASE WHEN whour < 0 THEN 0 ELSE whour END) AS whour, -- Working Hour [without lunch break time] 
		begprocess, -- status process check in
		endprocess, -- status process check out
		begtrxcode, -- transaction code status check in
		endtrxcode, -- transaction code status check out
		begreason,	-- begin reason
		endreason,	-- end reason
		begaprvnote,-- begin approval note
		endaprvnote -- end approval note
	FROM
	(
		SELECT 
			pernr || '.' || ldate AS id,
			pernr, 		-- Employee Personnel Number
			ename, 		-- Employee name - IT0001
			ldate, 		-- Logical date
			begagtime,	-- Begin time actual(machine)
			begltime,	-- Begin logical time
			endagtime,	-- End time actual(machine)
			endltime,	-- End logical time
			-- Working Delay Hour
			TRUNC( ( (DATE_PART('hour'::text, (ldate + begltime) - (ldate + TO_CHAR(time '08:20', 'HH24:MI')::time)))
						+ (DATE_PART('minute'::text, (ldate + begltime) - (ldate + TO_CHAR(time '08:20', 'HH24:MI')::time)) / 60)
					)::numeric, 2) AS wdhour,  
			-- Working Hour [without lunch break time] 
			TRUNC( ( (DATE_PART('hour'::text, (ldate + endltime) - (ldate + begltime)))
						+ (DATE_PART('minute'::text, (ldate + endltime) - (ldate + begltime)) / 60)
						)::numeric, 2) - 1 AS whour,
			begprocess, -- status process check in
			endprocess, -- status process check out
			begtrxcode, -- transaction code status check in
			endtrxcode, -- transaction code status check out
			begreason,	-- begin reason
			endreason,	-- end reason
			begaprvnote,-- begin approval note
			endaprvnote -- end approval note
		FROM
		(
			-- TEVEN CHECK IN
			(SELECT 
					emp.pernr, 
					emp.ename,
					tevenin.ldate, 
					tevenin.ltime AS begltime, 
					tevenout.ltime AS endltime,
					tevenin.process AS begprocess,
					tevenout.process AS endprocess,
					tevenin.trxcode AS begtrxcode,
					tevenout.trxcode AS endtrxcode,
					tevenin.reason AS begreason,
					tevenout.reason AS endreason,
					tevenin.approval_note AS begaprvnote,
					tevenout.approval_note AS endaprvnote,
					tevenin.agtime AS begagtime,
					tevenout.agtime AS endagtime
				FROM (SELECT DISTINCT pernr, ename FROM IT0001) emp 
					LEFT JOIN (SELECT * FROM teven WHERE satza = 'P10') AS tevenin ON emp.pernr = tevenin.pernr
					LEFT JOIN (SELECT * FROM teven WHERE satza = 'P20') AS tevenout ON tevenout.pernr = tevenin.pernr AND tevenout.ldate = tevenin.ldate
				) 
			UNION
			-- TEVEN CHECK OUT
			(SELECT 
					emp.pernr, 
					emp.ename,
					tevenout.ldate, 
					tevenin.ltime AS begltime, 
					tevenout.ltime AS endltime,
					tevenin.process AS begprocess,
					tevenout.process AS endprocess,
					tevenin.trxcode AS begtrxcode,
					tevenout.trxcode AS endtrxcode,
					tevenin.reason AS begreason,
					tevenout.reason AS endreason,
					tevenin.approval_note AS begaprvnote,
					tevenout.approval_note AS endaprvnote,
					tevenin.agtime AS begagtime,
					tevenout.agtime AS endagtime
				FROM (SELECT DISTINCT pernr, ename FROM IT0001) emp 
					LEFT JOIN (SELECT * FROM teven WHERE satza = 'P20') AS tevenout ON emp.pernr = tevenout.pernr
					LEFT JOIN (SELECT * FROM teven WHERE satza = 'P10') AS tevenin ON tevenin.pernr = tevenout.pernr AND tevenin.ldate = tevenout.ldate
				) 
		) AS tevenvw
		WHERE ldate IS NOT NULL
	) AS tvw;

ALTER TABLE public.tevenvw OWNER TO hcms;
