ALTER TABLE IT0006 ADD COLUMN document_status character varying(25) NOT NULL DEFAULT 'DRAFT';

ALTER TABLE IT0006 ADD CONSTRAINT it0006_check_document_status CHECK (document_status IN ('DRAFT', 'PUBLISHED'));
