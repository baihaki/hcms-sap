DROP TABLE IF EXISTS public.tevenlate CASCADE;

CREATE TABLE public.tevenlate
(
  bukrs character varying(4) NOT NULL,
  latecomming time without time zone,
  begda date NOT NULL,
  endda date NOT NULL,
  CONSTRAINT tevenlate_fkey PRIMARY KEY (bukrs, begda, endda)
);

COMMENT ON TABLE public.tevenlate  IS 'setup multi tolerance late (group by bukrs)';

ALTER TABLE public.tevenlate OWNER TO hcms;

INSERT INTO tevenlate VALUES('9900', '08:20:00', '2017-01-01', '9999-12-31');
INSERT INTO tevenlate VALUES('3000', '08:15:00', '2018-01-01', '9999-12-31');
INSERT INTO tevenlate VALUES('4000', '08:15:00', '2018-01-01', '9999-12-31');