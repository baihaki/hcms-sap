ALTER TABLE trx_event_log DROP CONSTRAINT operation_type_check;

ALTER TABLE trx_event_log ADD CONSTRAINT operation_type_check CHECK (operation_type IN ('CREATE', 'UPDATE', 'APPROVAL', 'DOCUMENT_REJECT', 'DELETE'));
