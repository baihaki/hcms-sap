ALTER TABLE public.abmgroupattendance
   ALTER COLUMN entrydatetime TYPE timestamp without time zone;
   
ALTER TABLE public.abmgroupattendance
   ALTER COLUMN exitdatetime TYPE timestamp without time zone;
