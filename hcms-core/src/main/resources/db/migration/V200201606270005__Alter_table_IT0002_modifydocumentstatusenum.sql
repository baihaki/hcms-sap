ALTER TABLE IT0002 DROP CONSTRAINT it0002_document_status_check;

UPDATE IT0002 SET document_status = 'INITIAL_DRAFT';

ALTER TABLE IT0002 ADD CONSTRAINT it0002_document_status_check CHECK (document_status IN ('INITIAL_DRAFT', 'DRAFT_REJECTED', 'UPDATED_DRAFT', 'UPDATE_IN_PROGRESS', 'UPDATE_REJECTED', 'PUBLISHED'));

ALTER TABLE IT0002 ALTER COLUMN document_status SET DEFAULT 'INITIAL_DRAFT';
