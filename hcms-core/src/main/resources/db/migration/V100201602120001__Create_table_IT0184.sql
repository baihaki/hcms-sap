CREATE TABLE IT0184 (
  pernr bigint not null,
  subty character varying(4) not null,
  endda date not null,
  begda date not null,
  seqnr bigint default 0,
  aedtm timestamp with time zone not null default timezone('utc'::text, now()),
  uname character varying(255),
  itxex character varying(2),
  pftyp character varying(4) not null,
  pftyp_infty character varying(10) not null
);

COMMENT ON TABLE IT0184 IS 'Master data transaction table for Resume Texts';

COMMENT ON COLUMN IT0184.pernr IS 'Employee SSN/ID';

COMMENT ON COLUMN IT0184.subty IS 'Subtype';

COMMENT ON COLUMN IT0184.endda IS 'End Date';

COMMENT ON COLUMN IT0184.begda IS 'Begin Date';

COMMENT ON COLUMN IT0184.seqnr IS 'Infotype Record No.';

COMMENT ON COLUMN IT0184.aedtm IS 'Changed On.';

COMMENT ON COLUMN IT0184.uname IS 'Changed By';

COMMENT ON COLUMN IT0184.itxex IS 'Text Exists';

COMMENT ON COLUMN IT0184.pftyp IS 'Profile Type';

COMMENT ON COLUMN IT0184.pftyp_infty IS 'Infotype';
