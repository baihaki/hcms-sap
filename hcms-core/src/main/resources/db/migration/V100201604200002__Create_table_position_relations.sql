CREATE TABLE position_relations (
  id character varying(255) NOT NULL default uuid_generate_v1mc(),
  opt_lock bigint default 0,
  position_relation_type_id character varying(255) not null,
  from_position_id bigint not null,
  to_position_id bigint not null,
  valid_from timestamp with time zone,
  valid_thru timestamp with time zone,
  created_at timestamp with time zone not null default timezone('utc'::text, now()),
  created_by_id character varying(255) not null,
  updated_at timestamp with time zone not null default timezone('utc'::text, now()),
  updated_by_id character varying(255) not null,
  deleted_at timestamp with time zone,
  deleted_by_id character varying(255)  
);
