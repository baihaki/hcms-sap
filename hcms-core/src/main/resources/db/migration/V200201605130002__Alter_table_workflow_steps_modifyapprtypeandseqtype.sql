ALTER TABLE workflow_steps ALTER COLUMN approval_type TYPE character varying(50);

DROP TYPE approvalType CASCADE;

ALTER TABLE workflow_steps ALTER COLUMN sequence_type TYPE character varying(50);

DROP TYPE sequenceType CASCADE;

ALTER TABLE workflow_steps ADD CONSTRAINT approvalType CHECK (approval_type IN ('ALL_SUPERIOR', 'ONE_SUPERIOR'));

ALTER TABLE workflow_steps ADD CONSTRAINT sequenceType CHECK (sequence_type IN ('SERIAL', 'PARALEL'));

