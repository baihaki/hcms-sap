CREATE TABLE IT2001 (
  pernr bigint not null,
  subty character varying(4) not null,
  endda date not null,
  begda date not null,
  seqnr bigint default 0,
  aedtm timestamp with time zone not null default timezone('utc'::text, now()),
  uname character varying(225),
  awart character varying(10) not null,
  abwtg numeric(10,2),
  stdaz numeric(10,2),
  kaltg numeric(10,2)
);

COMMENT ON TABLE IT2001 IS 'Master data Transaction for Absences';

COMMENT ON COLUMN IT2001.pernr IS 'Employee SSN/ID';

COMMENT ON COLUMN IT2001.subty IS 'Subtype';

COMMENT ON COLUMN IT2001.endda IS 'End Date';

COMMENT ON COLUMN IT2001.begda IS 'Begin Date';

COMMENT ON COLUMN IT2001.seqnr IS 'Infotype Record No.';

COMMENT ON COLUMN IT2001.aedtm IS 'Changed On.';

COMMENT ON COLUMN IT2001.uname IS 'Changed By';

COMMENT ON COLUMN IT2001.awart IS 'Att./Absence Type';

COMMENT ON COLUMN IT2001.abwtg IS 'Att./Absence Days';

COMMENT ON COLUMN IT2001.stdaz IS 'Absence Hours';

COMMENT ON COLUMN IT2001.kaltg IS 'Calendar Days';
