CREATE TABLE public.abmgroupattendance
(
   first_name character varying(20), 
   last_name character varying(40), 
   cardno character varying(32), 
   sn character varying(10), 
   company character varying(40), 
   entrydatetime timestamp with time zone, 
   exitdatetime timestamp with time zone, 
   days date, 
   entry_time time without time zone, 
   exit_time time without time zone, 
   first_door character varying(80), 
   last_door character varying(80),
   PRIMARY KEY (cardno, days)
);
ALTER TABLE public.abmgroupattendance OWNER TO hcms;
COMMENT ON TABLE public.abmgroupattendance IS 'temporary data from finger machine';