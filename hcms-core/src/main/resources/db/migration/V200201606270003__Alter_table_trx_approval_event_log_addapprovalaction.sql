ALTER TABLE trx_approval_event_log ADD approval_action character varying(25) NOT NULL DEFAULT 'APPROVED';

ALTER TABLE trx_approval_event_log ADD CONSTRAINT approval_action_check CHECK (approval_action IN ('APPROVED', 'REJECTED'));
