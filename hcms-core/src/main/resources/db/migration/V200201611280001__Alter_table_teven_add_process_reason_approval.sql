﻿ALTER TABLE public.teven ADD COLUMN process character varying(1);
ALTER TABLE public.teven ADD COLUMN reason character varying(255);
COMMENT ON COLUMN public.teven.process IS '0: new create; 1: release; 2: approved 1; 3: approved 2';

ALTER TABLE public.teven ADD COLUMN approved1_by character varying(255);
ALTER TABLE public.teven ADD COLUMN approved1_at timestamp with time zone;
ALTER TABLE public.teven ADD COLUMN approved2_by character varying(255);
ALTER TABLE public.teven ADD COLUMN approved2_at timestamp with time zone;