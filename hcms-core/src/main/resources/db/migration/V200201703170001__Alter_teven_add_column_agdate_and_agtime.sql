ALTER TABLE public.teven ADD COLUMN agdate date;
ALTER TABLE public.teven ADD COLUMN agtime time without time zone;
COMMENT ON COLUMN public.teven.agdate IS 'ABM Group Attendance - Days';
COMMENT ON COLUMN public.teven.agtime IS 'ABM Group Attendance - EntryTime Or ExitTime';
