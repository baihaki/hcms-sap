ALTER TABLE IT0009 ADD COLUMN document_status character varying(25) NOT NULL DEFAULT 'DRAFT';

ALTER TABLE IT0009 ADD CONSTRAINT it0009_document_status_check CHECK (document_status IN ('DRAFT', 'PUBLISHED'));

COMMENT ON COLUMN IT0009.document_status IS 'Status of Document (i.e. DRAFT or PUBLISHED)';
