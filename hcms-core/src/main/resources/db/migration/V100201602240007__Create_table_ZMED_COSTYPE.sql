CREATE TABLE ZMED_COSTYPE (
  BUKRS character varying(10) NOT NULL,
  KODEMED character varying(10) NOT NULL,
  KODECOS character varying(10) NOT NULL,
  DATAB date not null,
  DATBI date not null,
  DESCR character varying(255)
);

COMMENT ON TABLE ZMED_COSTYPE IS 'Master data for Medical Cost Type';

COMMENT ON COLUMN ZMED_COSTYPE.BUKRS IS 'Company Code';

COMMENT ON COLUMN ZMED_COSTYPE.KODEMED IS 'Medical Type (Data Element)';

COMMENT ON COLUMN ZMED_COSTYPE.KODECOS IS 'Cost Type (Data Element)';

COMMENT ON COLUMN ZMED_COSTYPE.DATAB IS 'Valid From Date';

COMMENT ON COLUMN ZMED_COSTYPE.DATBI IS 'Valid To Date';

COMMENT ON COLUMN ZMED_COSTYPE.DESCR IS 'Description'
