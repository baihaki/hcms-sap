CREATE TABLE ZMED_EMPQUOTA (
  BUKRS character varying(10) NOT NULL,
  PERSG character varying(10) NOT NULL,
  PERSK character varying(10) NOT NULL,
  FATXT character(1) NOT NULL,
  KODEQUO character varying(10) NOT NULL,
  DATAB date not null,
  DATBI date not null,
  EXTEN smallint default 0,
  QUOTAMT numeric(15, 2) default 0.0
);

COMMENT ON TABLE ZMED_EMPQUOTA IS 'Master data table for Employee Quota';

COMMENT ON COLUMN ZMED_EMPQUOTA.BUKRS IS 'Company Code';

COMMENT ON COLUMN ZMED_EMPQUOTA.PERSG IS 'Employee Group';

COMMENT ON COLUMN ZMED_EMPQUOTA.PERSK IS 'Employee Sub-Group';

COMMENT ON COLUMN ZMED_EMPQUOTA.FATXT IS 'Marrital Status Key';

COMMENT ON COLUMN ZMED_EMPQUOTA.KODEQUO IS 'Quota Type (Data Element)';

COMMENT ON COLUMN ZMED_EMPQUOTA.DATAB IS 'Valid From Date';

COMMENT ON COLUMN ZMED_EMPQUOTA.DATBI IS 'Valid To Date';

COMMENT ON COLUMN ZMED_EMPQUOTA.EXTEN IS 'Number of Additional Exemptions';

COMMENT ON COLUMN ZMED_EMPQUOTA.QUOTAMT IS 'Wage Type Amount for Payments';
