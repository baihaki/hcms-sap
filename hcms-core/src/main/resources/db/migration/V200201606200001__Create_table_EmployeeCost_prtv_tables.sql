﻿CREATE TABLE ptrv_head
(
  hdvrs integer NOT NULL,
  pernr bigint NOT NULL,
  reinr bigint NOT NULL,
  molga character varying(2),
  morei character varying(2),
  schem character varying(2),
  zland character varying(3),
  datv1 date,
  uhrv1 time without time zone,
  datb1 date,
  uhrb1 time without time zone,
  dath1 date,
  uhrh1 time without time zone,
  datr1 date,
  uhrr1 time without time zone,
  endrg date,
  dates date,
  times time without time zone,
  uname character varying(12),
  repid character varying(40),
  dantn bigint,
  fintn bigint,
  request character(1),
  travel_plan character(1),
  expenses character(1),
  st_trgtg date,
  st_trgall date,
  dat_reduc1 date,
  dat_reduc2 date,
  datv1_dienst date,
  uhrv1_dienst time without time zone,
  datb1_dienst date,
  uhrb1_dienst time without time zone,
  abordnung bigint,
  exchange_date date,
  zort1 character varying(59),
  CONSTRAINT ptrv_head_pkey PRIMARY KEY (hdvrs, pernr, reinr)
);

CREATE TABLE ptrv_perio
(
  mandt character varying(3) NOT NULL,
  perio numeric(3,0) NOT NULL,
  pernr numeric(8,0) NOT NULL,
  reinr numeric(10,0) NOT NULL,
  pdvrs numeric(2,0) NOT NULL,
  hdvrs numeric(2,0),
  pdatv date,
  puhrv time without time zone,
  pdatb date,
  puhrb time without time zone,
  antrg character(1),
  abrec character(1),
  uebrf character(1),
  uebdt character(1),
  tlock character(1),
  waers character varying(5),
  abrj1 numeric(4,0),
  abrp1 numeric(2,0),
  perm1 character(2),
  abkr1 character(2),
  begp1 date,
  endp1 date,
  abrj2 numeric(4,0),
  abrp2 numeric(2,0),
  perm2 character(2),
  abkr2 character(2),
  begp2 date,
  endp2 date,
  accdt date,
  acctm time without time zone,
  runid numeric(10,0),
  verpa character(1),
  uebkz character(1),
  anuep numeric(2,0),
  no_miles character(1),
  lstay character(1),
  CONSTRAINT ptrv_perio_pkey PRIMARY KEY (mandt, pernr, reinr, perio, pdvrs)
);

CREATE TABLE ptrv_srec
(
  mandt character varying(3) NOT NULL,
  pernr bigint NOT NULL,
  reinr bigint NOT NULL,
  perio integer NOT NULL,
  receiptno character varying(3) NOT NULL,
  exp_type character varying(4),
  rec_amount numeric(13,2),
  rec_curr character varying(5),
  rec_rate numeric(9,5),
  loc_amount numeric(13,2),
  loc_curr character varying(5),
  rec_date date,
  shorttxt character varying(10),
  paper_receipt character(1),
  CONSTRAINT ptrv_srec_pkey PRIMARY KEY (mandt, pernr, reinr, perio, receiptno)
);

CREATE TABLE sh_t706b1
(
  morei character varying(2) NOT NULL,
  spkzl character varying(4) NOT NULL,
  sptxt character varying(30) NOT NULL,
  CONSTRAINT sh_t706b1_pkey PRIMARY KEY (morei, spkzl)
);

CREATE TABLE t706b1
(
  morei character varying(2) NOT NULL,
  spkzl character varying(4) NOT NULL,
  endda date NOT NULL,
  begda date NOT NULL,
  CONSTRAINT t706b1_pkey PRIMARY KEY (morei, spkzl, endda, begda)
);

CREATE TABLE t706s
(
  morei character varying(2) NOT NULL,
  schem character varying(2) NOT NULL,
  stext character varying(25) NOT NULL,
  CONSTRAINT t706s_pkey PRIMARY KEY (morei, schem)
);

CREATE TABLE t702n
(
  morei character varying(2) NOT NULL,
  text25 character varying(20) NOT NULL,
  CONSTRAINT t702n_pkey PRIMARY KEY (morei)
);

CREATE TABLE t702o
(
  morei character varying(2) NOT NULL,
  land1 character varying(2) NOT NULL,
  text25 character varying(20) NOT NULL,
  CONSTRAINT t702o_pkey PRIMARY KEY (morei, land1)
);