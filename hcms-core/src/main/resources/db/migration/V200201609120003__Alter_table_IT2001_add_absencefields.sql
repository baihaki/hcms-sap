﻿ALTER TABLE IT2001 ADD COLUMN address character varying(255);
ALTER TABLE IT2001 ADD COLUMN phone character varying(15);
ALTER TABLE IT2001 ADD COLUMN mobile character varying(15);
ALTER TABLE IT2001 ADD COLUMN notes character varying(255);
ALTER TABLE IT2001 ADD COLUMN reason character varying(255);