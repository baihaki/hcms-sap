﻿ALTER TABLE it0019 ADD COLUMN text1 character varying(255);
ALTER TABLE it0019 ADD COLUMN text2 character varying(255);
ALTER TABLE it0019 ADD COLUMN text3 character varying(255);
ALTER TABLE it0019 ADD COLUMN attach1_subty character varying(4) NOT NULL DEFAULT 'SP'::character varying;
ALTER TABLE it0019 ADD COLUMN attach1_path character varying(255);
ALTER TABLE it0019 ADD CONSTRAINT it0019_attach1_fkey FOREIGN KEY (infty, attach1_subty)
   REFERENCES cfg_attachment (infty, subty) ON UPDATE CASCADE ON DELETE RESTRICT;