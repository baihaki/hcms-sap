﻿CREATE TABLE cfg_attachment
(
  subty character varying(4) NOT NULL, -- Subtype
  stext character varying(40) NOT NULL, -- Name
  infty character varying(10) NOT NULL, -- Infotype
  CONSTRAINT cfg_attachment_pkey PRIMARY KEY (infty, subty)
);
COMMENT ON TABLE cfg_attachment
  IS 'Table master for Attachment Document Type (Custom Table)';
COMMENT ON COLUMN cfg_attachment.subty IS 'Subtype';
COMMENT ON COLUMN cfg_attachment.stext IS 'Name / Description';
COMMENT ON COLUMN cfg_attachment.infty IS 'Infotype';

INSERT INTO cfg_attachment VALUES('KTP', 'Kartu Tanda Penduduk', '0002');
INSERT INTO cfg_attachment VALUES('KK', 'Kartu Keluarga', '0002');
INSERT INTO cfg_attachment VALUES('AN', 'Akte Nikah', '0002');
INSERT INTO cfg_attachment VALUES('KTP', 'Kartu Tanda Penduduk', '0006');
INSERT INTO cfg_attachment VALUES('KK', 'Kartu Keluarga', '0006');
INSERT INTO cfg_attachment VALUES('ID', 'Personal ID', '0185');
INSERT INTO cfg_attachment VALUES('KK', 'Kartu Keluarga', '0021');
INSERT INTO cfg_attachment VALUES('IJA', 'Ijazah / Sertifikat', '0022');
INSERT INTO cfg_attachment VALUES('SKK', 'Surat Keterangan Kerja', '0023');
INSERT INTO cfg_attachment VALUES('REK', 'Buku Rekening Bank', '0009');
INSERT INTO cfg_attachment VALUES('SP', 'Surat Peringatan', '0019');
/* INSERT INTO cfg_attachment VALUES('FOTO', 'Foto Profile', 'user'); */
INSERT INTO cfg_attachment VALUES('ALL', 'KTP, KK, Akte Nikah', '0002');
INSERT INTO cfg_attachment VALUES('ALL', 'KTP, KK', '0006');
