﻿ALTER TABLE it0021 ADD COLUMN address character varying(255);
ALTER TABLE it0021 ADD COLUMN phone character varying(20);

COMMENT ON COLUMN it0021.address IS 'Emergency Contact Address';
COMMENT ON COLUMN it0021.phone IS 'Emergency Contact Phone Number';