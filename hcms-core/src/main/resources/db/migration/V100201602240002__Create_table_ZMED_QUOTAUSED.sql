CREATE TABLE ZMED_QUOTAUSED (
  GJAHR integer not null,
  BUKRS character varying(10) not null,
  PERNR bigint not null,
  KODEQUO character varying(10) not null,
  QUOTAMT numeric(15,2) default 0.0 
);

COMMENT ON TABLE ZMED_QUOTAUSED IS 'Master Data for Quota Used';

COMMENT ON COLUMN ZMED_QUOTAUSED.GJAHR IS 'Fiscal Year';

COMMENT ON COLUMN ZMED_QUOTAUSED.BUKRS IS 'Company Code';

COMMENT ON COLUMN ZMED_QUOTAUSED.PERNR IS 'Employee SSN/Personnel Number';

COMMENT ON COLUMN ZMED_QUOTAUSED.KODEQUO IS 'Quota Type (Data Element)';

COMMENT ON COLUMN ZMED_QUOTAUSED.QUOTAMT IS 'Wage Type Amount for Payments';
