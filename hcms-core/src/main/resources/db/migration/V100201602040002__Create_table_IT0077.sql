CREATE TABLE IT0077 (
  pernr bigint not null,
  endda date not null,
  begda date not null,
  seqnr bigint,
  aedtm timestamp with time zone not null default timezone('utc'::text, now()),
  uname character varying(255),
  molga character varying(5),
  racky character varying(5)
);

COMMENT ON TABLE IT0077 IS 'Master data transaction for Additional Personal Data';

COMMENT ON COLUMN IT0077.pernr IS 'Employee SSN/ID';

COMMENT ON COLUMN IT0077.endda IS 'End Date';

COMMENT ON COLUMN IT0077.begda IS 'Begin Date';

COMMENT ON COLUMN IT0077.seqnr IS 'Infotype Record No.';

COMMENT ON COLUMN IT0077.aedtm IS 'Changed On';

COMMENT ON COLUMN IT0077.uname IS 'Changed By';

COMMENT ON COLUMN IT0077.molga IS 'Country Grouping Code';

COMMENT ON COLUMN IT0077.racky IS 'Ethnic Origin Code';
