﻿INSERT INTO m_role (role_name, role_description, is_system_role, created_by_id, updated_by_id) VALUES ('ROLE_HEAD', 'Role for Department Head', 'TRUE', 'ABM_SUPERADMIN', 'ABM_SUPERADMIN');
INSERT INTO m_role (role_name, role_description, is_system_role, created_by_id, updated_by_id) VALUES ('ROLE_DIRECTOR', 'Role for Director', 'TRUE', 'ABM_SUPERADMIN', 'ABM_SUPERADMIN');
INSERT INTO m_role (role_name, role_description, is_system_role, created_by_id, updated_by_id) VALUES ('ROLE_GA', 'Role for GA', 'TRUE', 'ABM_SUPERADMIN', 'ABM_SUPERADMIN');
INSERT INTO m_role (role_name, role_description, is_system_role, created_by_id, updated_by_id) VALUES ('ROLE_ACCOUNTING', 'Role for Accounting', 'TRUE', 'ABM_SUPERADMIN', 'ABM_SUPERADMIN');
INSERT INTO m_role (role_name, role_description, is_system_role, created_by_id, updated_by_id) VALUES ('ROLE_TREASURY', 'Role for Treasury', 'TRUE', 'ABM_SUPERADMIN', 'ABM_SUPERADMIN');

INSERT INTO cfg_entity (entity_name, created_by_id, updated_by_id, deleted_at, deleted_by_id, opt_lock, entity_desc) VALUES ('CaReqHdr', 'ABM_SUPERADMIN', 'ABM_SUPERADMIN', NULL, NULL, 0, 'Cash Advance Request Header');
INSERT INTO cfg_entity (entity_name, created_by_id, updated_by_id, deleted_at, deleted_by_id, opt_lock, entity_desc) VALUES ('CaReqDtl', 'ABM_SUPERADMIN', 'ABM_SUPERADMIN', NULL, NULL, 0, 'Cash Advance Request Details');
INSERT INTO cfg_entity (entity_name, created_by_id, updated_by_id, deleted_at, deleted_by_id, opt_lock, entity_desc) VALUES ('CaTravelArgDtl', 'ABM_SUPERADMIN', 'ABM_SUPERADMIN', NULL, NULL, 0, 'Travel Arrangement Details');
INSERT INTO cfg_entity (entity_name, created_by_id, updated_by_id, deleted_at, deleted_by_id, opt_lock, entity_desc) VALUES ('CaStlHdr', 'ABM_SUPERADMIN', 'ABM_SUPERADMIN', NULL, NULL, 0, 'Cash Advance Settlement Header');
INSERT INTO cfg_entity (entity_name, created_by_id, updated_by_id, deleted_at, deleted_by_id, opt_lock, entity_desc) VALUES ('CaStlDtl', 'ABM_SUPERADMIN', 'ABM_SUPERADMIN', NULL, NULL, 0, 'Cash Advance Settlement Details');
