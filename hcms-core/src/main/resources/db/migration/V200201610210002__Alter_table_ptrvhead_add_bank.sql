﻿ALTER TABLE ptrv_head ADD COLUMN banka character varying(60);
ALTER TABLE ptrv_head ADD COLUMN bankn character varying(100);

COMMENT ON COLUMN ptrv_head.banka IS 'Current Employee Bank Name';
COMMENT ON COLUMN ptrv_head.bankn IS 'Current Employee Bank Account Number';