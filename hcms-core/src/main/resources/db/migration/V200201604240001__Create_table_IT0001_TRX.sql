CREATE TABLE IT0001_TRX (
  id character varying(255) NOT NULL DEFAULT uuid_generate_v1mc(),
  pernr bigint NOT NULL,
  endda timestamp with time zone NOT NULL,
  begda timestamp with time zone NOT NULL,
  seqnr bigint default 0,
  aedtm timestamp with time zone,
  uname character varying(100),
  werks character varying(10),
  bukrs character varying(10),
  persg character varying(10),
  persk character varying(10),
  vdsk1 character varying(14),
  gsber character varying(10),
  werks2 character varying(10),
  btrtl character varying(10),
  abkrs character varying(10),
  kostl character varying(10),
  kokrs character varying(10),
  orgeh bigint,
  endda_orgunit timestamp with time zone,
  plans bigint,
  endda_emposition timestamp with time zone,
  stell bigint,
  endda_empjob timestamp with time zone,
  mstbr character varying(8),
  sacha character varying(3),
  sachp character varying(3),
  sachz character varying(3),
  sname character varying(30),
  ename character varying(40),
  otype character varying(2),
  sbmod character varying(4),
  m_job_id character varying(255),
  m_position_id character varying(255)
);

ALTER TABLE IT0001_TRX ADD PRIMARY KEY (id);

COMMENT ON TABLE IT0001_TRX IS 'Transaction Table for IT0001';

COMMENT ON COLUMN IT0001_TRX.pernr IS 'Employee Personnel Number';

COMMENT ON COLUMN IT0001_TRX.endda IS 'End Date';

COMMENT ON COLUMN IT0001_TRX.begda IS 'Begin Date';

COMMENT ON COLUMN IT0001_TRX.seqnr IS 'Infotype Record No.';

COMMENT ON COLUMN IT0001_TRX.aedtm IS 'Changed On.';

COMMENT ON COLUMN IT0001_TRX.uname IS 'Changed By';

COMMENT ON COLUMN IT0001_TRX.werks IS 'Personnel Area';

COMMENT ON COLUMN IT0001_TRX.bukrs IS 'Company Code';

COMMENT ON COLUMN IT0001_TRX.persg IS 'Employee Group';

COMMENT ON COLUMN IT0001_TRX.persk IS 'Employee Sub Group';

COMMENT ON COLUMN IT0001_TRX.vdsk1 IS 'Organizational Key';

COMMENT ON COLUMN IT0001_TRX.gsber IS 'Business Area';

COMMENT ON COLUMN IT0001_TRX.werks2 IS 'Code of Personnel Area SubArea';

COMMENT ON COLUMN IT0001_TRX.btrtl IS 'Personnel SubArea';

COMMENT ON COLUMN IT0001_TRX.abkrs IS 'Payroll Area';

COMMENT ON COLUMN IT0001_TRX.kostl IS 'Cost Center Code (Controlling Area Code)';

COMMENT ON COLUMN IT0001_TRX.kokrs IS 'Cost Center Code';

COMMENT ON COLUMN IT0001_TRX.orgeh IS 'Organizational Unit';

COMMENT ON COLUMN IT0001_TRX.endda_orgunit IS 'Organizational Unit (End Date)';

COMMENT ON COLUMN IT0001_TRX.plans IS 'Position';

COMMENT ON COLUMN IT0001_TRX.endda_emposition IS 'Position (End Date)';

COMMENT ON COLUMN IT0001_TRX.stell IS 'JOB KEY';

COMMENT ON COLUMN IT0001_TRX.endda_empjob IS 'JOB KEY (End Date)';

COMMENT ON COLUMN IT0001_TRX.mstbr IS 'Supervisor Area';

COMMENT ON COLUMN IT0001_TRX.sacha IS 'Payroll Administrator';

COMMENT ON COLUMN IT0001_TRX.sachp IS 'Pers.Administrator';

COMMENT ON COLUMN IT0001_TRX.sname IS 'Last Name First Name';

COMMENT ON COLUMN IT0001_TRX.ename IS 'Employee/Appl.Name';

COMMENT ON COLUMN IT0001_TRX.otype IS 'Object Type';

COMMENT ON COLUMN IT0001_TRX.sbmod IS 'Administrator Group';

