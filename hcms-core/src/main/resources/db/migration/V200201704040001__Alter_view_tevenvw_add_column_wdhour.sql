DROP VIEW public.tevenvw;

CREATE OR REPLACE VIEW public.tevenvw AS 
	SELECT 
		tevenvw.pernr || '.' || tevenvw.ldate AS id,
		pernr, 		-- Employee Personnel Number
		ename, 		-- Employee name - IT0001
		ldate, 		-- Logical date
		begltime,	-- Begin logical time
		endltime,	-- End logical time
		-- Working Actual Hour [with lunch break]
		TRUNC( ( (DATE_PART('hour'::text, (ldate + endltime) - (ldate + begltime)))  
					+ (DATE_PART('minute'::text, (ldate + endltime) - (ldate + begltime) ) / 60)
				)::numeric, 2) AS wahour,
		-- Working Delay Hour
		TRUNC( ( (DATE_PART('hour'::text, (ldate + begltime) - (ldate + TO_CHAR(time '08:20', 'HH24:MI')::time)))
					+ (DATE_PART('minute'::text, (ldate + begltime) - (ldate + TO_CHAR(time '08:20', 'HH24:MI')::time)) / 60)
				)::numeric, 2) AS wdhour,  
		-- Working Hour [without lunch break] 
		TRUNC( ( (DATE_PART('hour'::text, (ldate + endltime) - (ldate + begltime)))
					+ (DATE_PART('minute'::text, (ldate + endltime) - (ldate + begltime)) / 60)
				)::numeric, 2) - 1 AS whour,
		-- Productivity Hour
		TRUNC( ( (DATE_PART('hour'::text, (ldate + endltime) - (ldate + begltime)))
					+ ( (DATE_PART('minute'::text, (ldate + endltime) - (ldate + begltime)) / 60) / 8.75)
				)::numeric, 2) AS phour,
		begprocess, -- status process check in
		endprocess, -- status process check out
		begtrxcode, -- transaction code status check in
		endtrxcode, -- transaction code status check out
		begreason,	-- begin reason
		endreason,	-- end reason
		begaprvnote,-- begin approval note
		endaprvnote -- end approval note
	FROM
	(
		-- TEVEN CHECK IN
		(SELECT 
				emp.pernr, 
				emp.ename,
				teven.ldate,
				teven.ltime AS begltime,
				(SELECT tev.ltime FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P20') AS endltime,
				teven.process AS begprocess,
				(SELECT tev.process FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P20') AS endprocess,
				teven.trxcode AS begtrxcode,
				(SELECT tev.trxcode FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P20') AS endtrxcode,
				teven.reason AS begreason,
				(SELECT tev.reason FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P20') AS endreason,
				teven.approval_note as begaprvnote,
				(SELECT tev.approval_note FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P20') AS endaprvnote
			FROM (SELECT DISTINCT pernr, ename FROM IT0001) emp LEFT JOIN teven ON emp.pernr = teven.pernr AND satza = 'P10') 
		UNION
		-- TEVEN CHECK OUT
		(SELECT 
				emp.pernr, 
				emp.ename,
				teven.ldate,
				(SELECT tev.ltime FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P10') AS begltime,
				teven.ltime AS endltime,
				(SELECT tev.process FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P10') AS begprocess,
				teven.process AS endprocess,
				(SELECT tev.trxcode FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P10') AS begtrxcode,
				teven.trxcode AS endtrxcode,
				(SELECT tev.reason FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P10') AS begreason,
				teven.reason AS endreason,
				(SELECT tev.approval_note FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P10') AS begaprvnote,
				teven.approval_note AS endaprvnote
			FROM (SELECT DISTINCT pernr, ename FROM IT0001) emp LEFT JOIN teven ON emp.pernr = teven.pernr AND satza = 'P20') 
	) tevenvw
	WHERE ldate IS NOT NULL;

ALTER TABLE public.tevenvw OWNER TO hcms;
