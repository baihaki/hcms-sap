CREATE TABLE teven
(
   PDSNR bigint, 
   PERNR bigint, 
   LDATE date, 
   LTIME time without time zone, 
   ERDAT date, 
   ERTIM time without time zone, 
   SATZA character varying(3), 
   TERID character varying(4),
   ABWGR character varying(4), 
   EXLGA character varying(4), 
   ZEINH character varying(3), 
   HRAZL numeric(7,2), 
   HRBET numeric(9,2), 
   ORIGF character varying(1), 
   DALLF character varying(1), 
   PDC_USRUP character varying(20), 
   UNAME character varying(12), 
   AEDTM date, 
   ZAUSW bigint,
   PRIMARY KEY (PDSNR),
   FOREIGN KEY (SATZA) REFERENCES T705P (SATZA) ON DELETE RESTRICT,
   FOREIGN KEY (ABWGR) REFERENCES T705H (PINCO)
);

COMMENT ON COLUMN teven.PDSNR IS 'Number';
COMMENT ON COLUMN teven.PERNR IS 'Personnel Number';
COMMENT ON COLUMN teven.LDATE IS 'Logical date';
COMMENT ON COLUMN teven.LTIME IS 'Logical time';
COMMENT ON COLUMN teven.ERDAT IS 'Created on';
COMMENT ON COLUMN teven.ERTIM IS 'Created at';
COMMENT ON COLUMN teven.SATZA IS 'Time event type';
COMMENT ON COLUMN teven.TERID IS 'Terminal ID';
COMMENT ON COLUMN teven.ABWGR IS 'Att./absence reason';
COMMENT ON COLUMN teven.EXLGA IS 'Employee expenditure';
COMMENT ON COLUMN teven.ZEINH IS 'Unit of time/meas.';
COMMENT ON COLUMN teven.HRAZL IS 'Number';
COMMENT ON COLUMN teven.HRBET IS 'Amount';
COMMENT ON COLUMN teven.ORIGF IS 'Origin';
COMMENT ON COLUMN teven.DALLF IS 'Day assignment';
COMMENT ON COLUMN teven.PDC_USRUP IS 'Cust.Field';
COMMENT ON COLUMN teven.UNAME IS 'User Name';
COMMENT ON COLUMN teven.AEDTM IS 'Changed on';
COMMENT ON COLUMN teven.ZAUSW IS 'Time ID number';
COMMENT ON TABLE teven IS 'Time Event Type';

INSERT INTO teven(PDSNR, PERNR, LDATE, LTIME, ERDAT, ERTIM, SATZA, TERID, ABWGR, EXLGA, ZEINH, HRAZL, HRBET, ORIGF, DALLF, PDC_USRUP, UNAME, AEDTM, ZAUSW) VALUES (8731313, 21555, '2015-01-01', '08:00:00', '2015-12-07', '10:46:00', 'P10', 'SSS1', null, null, null, 0.00, 0.00, 'M', null, null, 'ABMC_ANDANG', '2015-12-07', 0);