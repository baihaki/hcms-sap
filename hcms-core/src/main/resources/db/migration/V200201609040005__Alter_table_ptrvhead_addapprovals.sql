﻿ALTER TABLE ptrv_head
  ADD COLUMN created_by character varying(255);
ALTER TABLE ptrv_head
  ADD COLUMN created_at timestamp with time zone;
ALTER TABLE ptrv_head
  ADD COLUMN approved1_by character varying(255);
ALTER TABLE ptrv_head
  ADD COLUMN approved1_at timestamp with time zone;
ALTER TABLE ptrv_head
  ADD COLUMN approved2_by character varying(255);
ALTER TABLE ptrv_head
  ADD COLUMN approved2_at timestamp with time zone;