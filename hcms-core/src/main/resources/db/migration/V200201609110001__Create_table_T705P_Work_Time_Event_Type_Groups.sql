CREATE TABLE T705P
(
   SATZA character varying(3), 
   DDTEXT character varying(60),
   PRIMARY KEY (SATZA)
);

COMMENT ON COLUMN T705P.SATZA IS 'Time event type';
COMMENT ON COLUMN T705P.DDTEXT IS 'Description';
COMMENT ON TABLE T705P  IS 'Attendance/Absence Reason Texts for Subsystem';

INSERT INTO T705P VALUES('P01', 'Clock-in/-out');
INSERT INTO T705P VALUES('P02', 'Start or end of break');
INSERT INTO T705P VALUES('P03', 'Start or end of off-site work');
INSERT INTO T705P VALUES('P04', 'Start or end of off-site work at home');
INSERT INTO T705P VALUES('P05', 'Interim entry');
INSERT INTO T705P VALUES('P10', 'Clock-in');
INSERT INTO T705P VALUES('P11', 'Change');
INSERT INTO T705P VALUES('P15', 'Start of break');
INSERT INTO T705P VALUES('P20', 'Clock-out');
INSERT INTO T705P VALUES('P25', 'End of break');
INSERT INTO T705P VALUES('P30', 'Start of off-site work');
INSERT INTO T705P VALUES('P35', 'Start of off-site work at home');
INSERT INTO T705P VALUES('P40', 'End of off-site work');
INSERT INTO T705P VALUES('P45', 'End of off-site work at home');
INSERT INTO T705P VALUES('P60', 'Inforation entry');
