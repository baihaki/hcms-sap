CREATE TABLE ZMED_TRXHDR (
  BUKRS character varying(10) NOT NULL,
  ZCLMNO character varying(10) NOT NULL,
  PERNR bigint,
  ORGTX character varying(25),
  ZDIVISION character varying(40),
  ENAME character varying(40),
  CLMST character varying(4),
  CLMAMT numeric(15, 2),
  REJAMT numeric(15, 2),
  APPAMT numeric(15, 2),
  PAGU1 numeric(15, 2),
  PAGU2 numeric(15, 2),
  PAGUDENT numeric(15, 2),
  PAGU2BYEMP numeric(15, 2),
  TOTAMT numeric(15, 2),
  PASIEN character varying(50),
  PENYAKIT character varying(30),
  TGLMASUK date,
  TGLKELUAR date,
  CLMDT date,
  CLMBY character varying(10),
  APRDT date,
  APRBY character varying(10),
  PAYDT date,
  PAYBY character varying(10),
  PAYMETHOD character varying(5),
  BELNR character varying(10),
  JAMIN character varying(10),
  NOTEX character varying(250),
  BILLDT date,
  KODEMED character varying(5),
  MEDTYDESC character varying(30),
  REVDT date,
  REVBY character varying(10)
);

COMMENT ON TABLE ZMED_TRXHDR IS 'Transaction Heade table for Medical';

COMMENT ON COLUMN ZMED_TRXHDR.BUKRS IS 'Company Code';

COMMENT ON COLUMN ZMED_TRXHDR.ZCLMNO IS 'Claim Number';

COMMENT ON COLUMN ZMED_TRXHDR.PERNR IS 'Personnel Number / Employee SSN';

COMMENT ON COLUMN ZMED_TRXHDR.ORGTX IS 'Short Text of Organizational Unit';

COMMENT ON COLUMN ZMED_TRXHDR.ZDIVISION IS 'Name of Organizational Element';

COMMENT ON COLUMN ZMED_TRXHDR.ENAME IS 'Formatted Name of Employee or Applicant';

COMMENT ON COLUMN ZMED_TRXHDR.CLMST IS 'Data Type 4 Characters';

COMMENT ON COLUMN ZMED_TRXHDR.CLMAMT IS 'Wage Type Amount for Payments';

COMMENT ON COLUMN ZMED_TRXHDR.REJAMT IS 'Wage Type Amount for Payments';

COMMENT ON COLUMN ZMED_TRXHDR.APPAMT IS 'Wage Type Amount for Payments';

COMMENT ON COLUMN ZMED_TRXHDR.PAGU1 IS 'Wage Type Amount for Payments';

COMMENT ON COLUMN ZMED_TRXHDR.PAGU2 IS 'Wage Type Amount for Payments';

COMMENT ON COLUMN ZMED_TRXHDR.PAGUDENT IS 'Wage Type Amount for Payments';

COMMENT ON COLUMN ZMED_TRXHDR.PAGU2BYEMP IS 'Wage Type Amount for Payments';

COMMENT ON COLUMN ZMED_TRXHDR.TOTAMT IS 'Wage Type Amount for Payments';

COMMENT ON COLUMN ZMED_TRXHDR.PASIEN IS 'Additional Partner Name';

COMMENT ON COLUMN ZMED_TRXHDR.PENYAKIT IS 'Desease (30 characters)';

COMMENT ON COLUMN ZMED_TRXHDR.TGLMASUK IS 'Date on Which Record Was Created (Patient In)';

COMMENT ON COLUMN ZMED_TRXHDR.TGLKELUAR IS 'Date on Which Record Was Created (Patient Out)';

COMMENT ON COLUMN ZMED_TRXHDR.CLMDT IS 'Date on Which Record Was Created';

COMMENT ON COLUMN ZMED_TRXHDR.CLMBY IS 'Character Field Length = 10';

COMMENT ON COLUMN ZMED_TRXHDR.APRDT IS 'Date on Which Record Was Created (Approval)';

COMMENT ON COLUMN ZMED_TRXHDR.APRBY IS 'Character Field Length = 10';

COMMENT ON COLUMN ZMED_TRXHDR.PAYDT IS 'Date on Which Record Was Created (Payment)';

COMMENT ON COLUMN ZMED_TRXHDR.PAYBY IS 'Character Field Length = 10';

COMMENT ON COLUMN ZMED_TRXHDR.PAYMETHOD IS 'Character Field Length = 5';

COMMENT ON COLUMN ZMED_TRXHDR.BELNR IS 'Accounting Document Number';

COMMENT ON COLUMN ZMED_TRXHDR.JAMIN IS 'Character Field Length = 10';

COMMENT ON COLUMN ZMED_TRXHDR.NOTEX IS 'Character 250 long';

COMMENT ON COLUMN ZMED_TRXHDR.BILLDT IS 'Date on Which Record Was Created (Date of Bill)';

COMMENT ON COLUMN ZMED_TRXHDR.KODEMED IS 'Character Field Length =5';

COMMENT ON COLUMN ZMED_TRXHDR.MEDTYDESC IS '30 Characters';

COMMENT ON COLUMN ZMED_TRXHDR.REVDT IS 'Date on Which Record Was Created (Date of Revised)';

COMMENT ON COLUMN ZMED_TRXHDR.REVBY IS 'Character Field Length = 10';
