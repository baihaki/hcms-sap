CREATE TABLE T548T (
  datar character varying(4) not null,
  dtext character varying(255) not null
);

ALTER TABLE T548T ADD PRIMARY KEY (datar);

COMMENT ON TABLE T548T IS 'Master data for Date Type';

COMMENT ON COLUMN T548T.datar IS 'Date Type Code';

COMMENT ON COLUMN T548T.dtext IS 'Date Type Text/Name';
