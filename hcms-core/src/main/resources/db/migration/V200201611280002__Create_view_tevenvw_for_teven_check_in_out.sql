﻿-- View: public.tevenvw

-- DROP VIEW public.tevenvw;

CREATE OR REPLACE VIEW public.tevenvw AS 
 SELECT
        CASE
            WHEN begtev.pernr IS NOT NULL THEN 
            	begtev.pernr
            ELSE 
            	endtev.pernr
        END AS pernr,
        CASE
            WHEN begtev.pernr IS NOT NULL THEN 
            	( SELECT it0001.ename FROM it0001 WHERE it0001.pernr = begtev.pernr ORDER BY it0001.endda DESC LIMIT 1)
            ELSE 
            	( SELECT it0001.ename FROM it0001 WHERE it0001.pernr = endtev.pernr ORDER BY it0001.endda DESC LIMIT 1)
        END AS ename,
        CASE
            WHEN begtev.ldate IS NOT NULL THEN 
            	begtev.ldate
            ELSE 
            	endtev.ldate
        END AS ldate,
	    begtev.ltime AS begltime,
	    endtev.ltime AS endltime,
	    ( SELECT trunc((date_part('hour'::text, endtev.ldate + endtev.ltime - (begtev.ldate + begtev.ltime)) + date_part('minute'::text, endtev.ldate + endtev.ltime - (begtev.ldate + begtev.ltime)) / 60::double precision)::numeric, 2) AS trunc) AS whour,
	    ( SELECT trunc(((date_part('hour'::text, endtev.ldate + endtev.ltime - (begtev.ldate + begtev.ltime)) + date_part('minute'::text, endtev.ldate + endtev.ltime - (begtev.ldate + begtev.ltime)) / 60::double precision) / 8.75::double precision)::numeric, 2) AS trunc) AS phour,
	    begtev.process AS begprocess,
	    endtev.process AS endprocess,
	    begtev.reason AS begreason,
	    endtev.reason AS endreason
   FROM ( SELECT teven.pdsnr,
		            teven.pernr,
		            teven.ldate,
		            teven.ltime,
		            teven.erdat,
		            teven.ertim,
		            teven.satza,
		            teven.terid,
		            teven.abwgr,
		            teven.exlga,
		            teven.zeinh,
		            teven.hrazl,
		            teven.hrbet,
		            teven.origf,
		            teven.dallf,
		            teven.pdc_usrup,
		            teven.uname,
		            teven.aedtm,
		            teven.zausw,
		            teven.pmbde,
		            teven.tevenid,
		            teven.process,
		            teven.reason
	           FROM teven
	          WHERE teven.satza::text = 'P10'::text) begtev
     LEFT JOIN ( SELECT teven.pdsnr,
			            teven.pernr,
			            teven.ldate,
			            teven.ltime,
			            teven.erdat,
			            teven.ertim,
			            teven.satza,
			            teven.terid,
			            teven.abwgr,
			            teven.exlga,
			            teven.zeinh,
			            teven.hrazl,
			            teven.hrbet,
			            teven.origf,
			            teven.dallf,
			            teven.pdc_usrup,
			            teven.uname,
			            teven.aedtm,
			            teven.zausw,
			            teven.pmbde,
			            teven.tevenid,
			            teven.process,
			            teven.reason
			           FROM teven
			          WHERE teven.satza::text = 'P20'::text) endtev 
     ON endtev.pernr = begtev.pernr AND endtev.ldate = begtev.ldate;

ALTER TABLE public.tevenvw OWNER TO hcms;