ALTER TABLE position_relations ALTER COLUMN from_position_id TYPE character varying(255);

ALTER TABLE position_relations ALTER COLUMN to_position_id TYPE character varying(255);

ALTER TABLE position_relations add primary key (id);

ALTER TABLE position_relations add FOREIGN KEY (position_relation_type_id) REFERENCES m_position_relation_types(id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE position_relations add FOREIGN KEY (from_position_id) REFERENCES m_positions(id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE position_relations add FOREIGN KEY (to_position_id) REFERENCES m_positions(id) ON UPDATE CASCADE ON DELETE RESTRICT;
