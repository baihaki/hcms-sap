﻿ALTER TABLE ptrv_srec
  ADD COLUMN start_volume numeric(13,2);
ALTER TABLE ptrv_srec
  ADD COLUMN end_volume numeric(13,2);
ALTER TABLE ptrv_srec
  ADD COLUMN sum_volume numeric(13,2);