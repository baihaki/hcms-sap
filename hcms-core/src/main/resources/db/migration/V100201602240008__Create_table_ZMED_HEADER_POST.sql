CREATE TABLE ZMED_HEADER_POST (
  OBJ_TYPE character varying(10) NOT NULL,
  OBJ_KEY character varying(50) NOT NULL,
  OBJ_SYS character varying(10) NOT NULL,
  BUS_ACT character varying(10),
  USERNAME character varying(255),
  HEADER_TXT character varying(255),
  COMP_CODE character varying(10),
  DOC_DATE DATE,
  PSTNG_DATE DATE,
  TRANS_DATE DATE,
  FISC_YEAR smallint,
  FIS_PERIOD smallint,
  DOC_TYPE character varying(5),
  REF_DOC_NO character varying(25),
  AC_DOC_NO character varying(25),
  OBJ_KEY_R character varying(25),
  REASON_REV character varying(10),
  COMPO_ACC character varying(10),
  REF_DOC_NO_LONG character varying(50),
  ACC_PRINCIPLE character varying(10),
  NEG_POSTING character varying(10),
  OBJ_KEY_INV character varying(25),
  BILL_CATEGORY character varying(10),
  VATDATE DATE,
  LOGSYS character varying(10)
);

COMMENT ON TABLE ZMED_HEADER_POST IS 'Posting for Medical Header';

COMMENT ON COLUMN ZMED_HEADER_POST.OBJ_TYPE IS 'Reference Transaction';

COMMENT ON COLUMN ZMED_HEADER_POST.OBJ_KEY IS 'Reference Key';

COMMENT ON COLUMN ZMED_HEADER_POST.OBJ_SYS IS 'Logical System of Source Document';

COMMENT ON COLUMN ZMED_HEADER_POST.BUS_ACT IS 'Business Transaction';

COMMENT ON COLUMN ZMED_HEADER_POST.USERNAME IS 'User name';

COMMENT ON COLUMN ZMED_HEADER_POST.HEADER_TXT IS 'Document Header Text';

COMMENT ON COLUMN ZMED_HEADER_POST.COMP_CODE IS 'Company Code';

COMMENT ON COLUMN ZMED_HEADER_POST.DOC_DATE IS 'Document Date in Document';

COMMENT ON COLUMN ZMED_HEADER_POST.PSTNG_DATE IS 'Posting Date in the Document';

COMMENT ON COLUMN ZMED_HEADER_POST.TRANS_DATE IS 'Translation Date';

COMMENT ON COLUMN ZMED_HEADER_POST.FISC_YEAR IS 'Fiscal Year';

COMMENT ON COLUMN ZMED_HEADER_POST.FIS_PERIOD IS 'Fiscal Period';

COMMENT ON COLUMN ZMED_HEADER_POST.DOC_TYPE IS 'Document Type';

COMMENT ON COLUMN ZMED_HEADER_POST.REF_DOC_NO IS 'Reference Document Number';

COMMENT ON COLUMN ZMED_HEADER_POST.AC_DOC_NO IS 'Accounting Document Number';

COMMENT ON COLUMN ZMED_HEADER_POST.OBJ_KEY_R IS 'Cancel: object key(AWREF_REV and AWORG_REV)';

COMMENT ON COLUMN ZMED_HEADER_POST.REASON_REV IS 'Reason for reversal';

COMMENT ON COLUMN ZMED_HEADER_POST.COMPO_ACC IS 'Component in ACC Interface';

COMMENT ON COLUMN ZMED_HEADER_POST.REF_DOC_NO_LONG IS 'Reference Document Number (for Dependencies see Long Text)';

COMMENT ON COLUMN ZMED_HEADER_POST.ACC_PRINCIPLE IS 'Accounting Principle';

COMMENT ON COLUMN ZMED_HEADER_POST.NEG_POSTING IS 'Indicator: Negative Posting';

COMMENT ON COLUMN ZMED_HEADER_POST.OBJ_KEY_INV IS 'Invoice Ref.: Object Key (AWREF_REB and AWORG_REB)';

COMMENT ON COLUMN ZMED_HEADER_POST.BILL_CATEGORY IS 'Billing Category';

COMMENT ON COLUMN ZMED_HEADER_POST.VATDATE IS 'Tax Reporting Date';

COMMENT ON COLUMN ZMED_HEADER_POST.LOGSYS IS 'Logical System';
