-- Jika DROP VIEW tevenvw, harus jalankan "V200201703010002__Alter_view_tevenvw_add_column_first_door_last_door.sql"
DROP VIEW public.tevenvw;

ALTER TABLE public.teven ALTER COLUMN trxcode TYPE character varying(3);

ALTER TABLE public.teven  ADD COLUMN approved3_by character varying(255);
ALTER TABLE public.teven  ADD COLUMN approved3_at timestamp with time zone;

ALTER TABLE public.teven  ADD COLUMN approved4_by character varying(255);
ALTER TABLE public.teven  ADD COLUMN approved4_at timestamp with time zone;

ALTER TABLE public.teven  ADD COLUMN approved5_by character varying(255);
ALTER TABLE public.teven  ADD COLUMN approved5_at timestamp with time zone;

ALTER TABLE public.teven  ADD COLUMN approved6_by character varying(255);
ALTER TABLE public.teven  ADD COLUMN approved6_at timestamp with time zone;

ALTER TABLE public.teven  ADD COLUMN approval_note character varying(255);

COMMENT ON COLUMN public.teven.trxcode IS 'transaction code:
AC=Auto-Create; AE=Auto-Editable; AS1=Auto-Submitted to Superior; AS2=Auto-Submitted to HRAdmin; AF=Auto-Finished;
MC=Manual-Create; ME=Manual-Editable; MS1=Manual-Submitted to Superior; MS2=Manual-Submitted to HRAdmin; MF=Manual-Finished';
