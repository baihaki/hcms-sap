﻿ALTER TABLE it0184
    ADD COLUMN text1 character varying(10);
COMMENT ON COLUMN it0184.text1 IS 'Text Value (NEW field to store Blood Type)';
ALTER TABLE it0184
    ADD COLUMN document_status character varying(25) NOT NULL DEFAULT 'DRAFT'::character varying;
ALTER TABLE it0184
    ADD CONSTRAINT it0184_document_status_check CHECK (document_status::text = ANY
        (ARRAY['DRAFT'::character varying, 'PUBLISHED'::character varying]::text[]));
COMMENT ON COLUMN it0184.document_status IS 'Status of Document (i.e. DRAFT || PUBLISHED)';

ALTER TABLE it0185
    ADD COLUMN document_status character varying(25) NOT NULL DEFAULT 'DRAFT'::character varying;
ALTER TABLE it0185
    ADD CONSTRAINT it0185_document_status_check CHECK (document_status::text = ANY
        (ARRAY['DRAFT'::character varying, 'PUBLISHED'::character varying]::text[]));
COMMENT ON COLUMN it0185.document_status IS 'Status of Document (i.e. DRAFT || PUBLISHED)';

ALTER TABLE it0241
    ADD COLUMN document_status character varying(25) NOT NULL DEFAULT 'DRAFT'::character varying;
ALTER TABLE it0241
    ADD CONSTRAINT it0241_document_status_check CHECK (document_status::text = ANY
        (ARRAY['DRAFT'::character varying, 'PUBLISHED'::character varying]::text[]));
COMMENT ON COLUMN it0241.document_status IS 'Status of Document (i.e. DRAFT || PUBLISHED)';

