CREATE TABLE m_jobs (
  id character varying(255) NOT NULL DEFAULT uuid_generate_v1mc(),
  opt_lock bigint default 0,
  code character varying(30) not null,
  name character varying(255) not null,
  description text,
  valid_from timestamp with time zone,
  valid_thru timestamp with time zone,
  created_at timestamp with time zone not null default timezone('utc'::text, now()),
  created_by_id character varying(255),
  updated_at timestamp with time zone not null default timezone('utc'::text, now()),
  updated_by_id character varying(255),
  deleted_at timestamp with time zone,
  deleted_by_id character varying(255)
);

ALTER TABLE m_jobs add primary key (id);
CREATE UNIQUE INDEX idx_code on m_jobs (code);
