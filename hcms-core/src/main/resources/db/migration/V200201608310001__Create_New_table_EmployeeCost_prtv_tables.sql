﻿CREATE TABLE t500l
(
  molga character varying(2) NOT NULL,
  ltext character varying(40) NOT NULL,
  CONSTRAINT t500l_pkey PRIMARY KEY (molga)
);

ALTER TABLE t702n
  ALTER COLUMN text25 TYPE character varying(40);

ALTER TABLE t706s
  ALTER COLUMN stext TYPE character varying(40);

ALTER TABLE t702o
  ALTER COLUMN text25 TYPE character varying(40);

CREATE TABLE sub_t706b1
(
  morei character varying(2) NOT NULL,
  spkzl character varying(4) NOT NULL,
  sub_spkzl character varying(4) NOT NULL,
  sub_sptxt character varying(30) NOT NULL,
  sub_form smallint NOT NULL,
  CONSTRAINT sub_t706b1_pkey PRIMARY KEY (morei, spkzl, sub_spkzl)
);


INSERT INTO sh_t706b1 VALUES ('AB', 'NECP', 'Pulse Cell Phones');
INSERT INTO sh_t706b1 VALUES ('AB', 'NEGA', 'Gasoline');
INSERT INTO sh_t706b1 VALUES ('AB', 'NEOT', 'Other (Car Running Cost)');
INSERT INTO sh_t706b1 VALUES ('AB', 'NEPR', 'Parking');
INSERT INTO sh_t706b1 VALUES ('AB', 'NETL', 'Toll');

INSERT INTO t706b1 VALUES ('AB', 'NECP', '9999-12-31', '2016-01-01');
INSERT INTO t706b1 VALUES ('AB', 'NEGA', '9999-12-31', '2016-01-01');
INSERT INTO t706b1 VALUES ('AB', 'NEOT', '9999-12-31', '2016-01-01');
INSERT INTO t706b1 VALUES ('AB', 'NEPR', '9999-12-31', '2016-01-01');
INSERT INTO t706b1 VALUES ('AB', 'NETL', '9999-12-31', '2016-01-01');

INSERT INTO sub_t706b1 VALUES ('AB', 'NECP', 'CP01', 'Pulse', 1);
INSERT INTO sub_t706b1 VALUES ('AB', 'NEGA', 'GA01', 'Fuel', 2);
INSERT INTO sub_t706b1 VALUES ('AB', 'NEOT', 'OT01', 'Oil', 2);
INSERT INTO sub_t706b1 VALUES ('AB', 'NEOT', 'OT02', 'Tyres', 2);
INSERT INTO sub_t706b1 VALUES ('AB', 'NEOT', 'OT03', 'Repairs', 2);
INSERT INTO sub_t706b1 VALUES ('AB', 'NEOT', 'OT04', 'Misc', 2);
INSERT INTO sub_t706b1 VALUES ('AB', 'NEPR', 'PR01', 'Parking', 2);
INSERT INTO sub_t706b1 VALUES ('AB', 'NETL', 'TL01', 'Toll Fee', 2);

INSERT INTO t706s VALUES ('AB', '01', 'Domestic Trip');
INSERT INTO t706s VALUES ('AB', '02', 'International Trip');
INSERT INTO t706s VALUES ('AB', '99', 'Travel Calendar');
INSERT INTO t706s VALUES ('AB', 'EC', 'Emp Cost - Executive');
INSERT INTO t706s VALUES ('AB', 'NE', 'Emp Cost - Non Executive');
INSERT INTO t706s VALUES ('AB', 'SP', 'Weekly Report');
INSERT INTO t706s VALUES ('AB', 'VS', 'Trip Advances');
INSERT INTO t706s VALUES ('AB', 'YY', 'Domestic - Executive');
INSERT INTO t706s VALUES ('AB', 'ZZ', 'International - Executive');

INSERT INTO t500l VALUES ('34', 'Indonesia');

INSERT INTO t702n VALUES ('AB', 'ABM Accounting ID');

INSERT INTO t702o VALUES ('AB', 'ID', 'Indonesia');