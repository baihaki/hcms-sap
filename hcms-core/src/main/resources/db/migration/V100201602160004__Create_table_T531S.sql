CREATE TABLE T531S (
  tmart character varying(5) not null,
  tmtxt character varying(255) not null
);

ALTER TABLE T531S ADD PRIMARY KEY (tmart);

COMMENT ON TABLE T531S IS 'Master data for Task Type';

COMMENT ON COLUMN T531S.tmart IS 'Task Type';

COMMENT ON COLUMN T531S.tmtxt IS 'Task Type Name/Text';
