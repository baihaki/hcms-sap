CREATE TABLE IT0041 (
  pernr bigint not null,
  endda date not null,
  begda date not null, 
  seqnr bigint default 0,
  aedtm timestamp with time zone not null default timezone('utc'::text, now()),
  uname character varying(255),
  dar01 character varying(4) not null,
  dat01 date not null,
  dar02 character varying(4),
  dat02 date,
  dar03 character varying(4),
  dat03 date,
  dar04 character varying(4),
  dat04 date,
  dar05 character varying(4),
  dat05 date,
  dar06 character varying(4),
  dat06 date,
  dar07 character varying(4),
  dat07 date,
  dar08 character varying(4),
  dat08 date,
  dar09 character varying(4),
  dat09 date,
  dar10 character varying(4),
  dat10 date,
  dar11 character varying(4),
  dat11 date,
  dar12 character varying(4),
  dat12 date
);

COMMENT ON TABLE IT0041 IS 'Master data transaction for Date Specifications';

COMMENT ON COLUMN IT0041.pernr IS 'Employee ID/SSN';

COMMENT ON COLUMN IT0041.endda IS 'End Date';

COMMENT ON COLUMN IT0041.begda IS 'Begin Date';

COMMENT ON COLUMN IT0041.seqnr IS 'Infotype Record No.';

COMMENT ON COLUMN IT0041.aedtm IS 'Changed On';

COMMENT ON COLUMN IT0041.uname IS 'Changed By';

COMMENT ON COLUMN IT0041.dar01 IS 'Date Type';

COMMENT ON COLUMN IT0041.dat01 IS 'Date for Date Type';
