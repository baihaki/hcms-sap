CREATE TABLE CFG_ENTITY_APPROVAL (
  id character varying(255) NOT NULL DEFAULT uuid_generate_v1mc(),
  cfg_entity_id character varying(255) NOT NULL,
  cfg_approval_category_id character varying(255) NOT NULL,
  created_at timestamp with time zone not null default timezone('utc'::text, now()),
  created_by_id character varying(255) NOT NULL,
  updated_at timestamp with time zone not null default timezone('utc'::text, now()),
  updated_by_id character varying(255) NOT NULL,
  deleted_at timestamp with time zone,
  deleted_by_id character varying(255)
);

ALTER TABLE CFG_ENTITY_APPROVAL ADD PRIMARY KEY (id);
ALTER TABLE CFG_ENTITY_APPROVAL ADD FOREIGN KEY (cfg_entity_id) REFERENCES CFG_ENTITY (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE CFG_ENTITY_APPROVAL ADD FOREIGN KEY (cfg_approval_category_id) REFERENCES CFG_APPROVAL_CATEGORY (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE CFG_ENTITY_APPROVAL ADD CONSTRAINT idx_cfg_entity_approval UNIQUE (cfg_entity_id, cfg_approval_category_id);
ALTER TABLE CFG_ENTITY_APPROVAL ADD FOREIGN KEY (created_by_id) REFERENCES m_user (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE CFG_ENTITY_APPROVAL ADD FOREIGN KEY (updated_by_id) REFERENCES m_user (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE CFG_ENTITY_APPROVAL ADD FOREIGN KEY (deleted_by_id) REFERENCES m_user (id) ON UPDATE CASCADE ON DELETE RESTRICT;

COMMENT ON TABLE CFG_ENTITY_APPROVAL IS 'List of Entity Approval Category';

COMMENT ON COLUMN CFG_ENTITY_APPROVAL.cfg_entity_id IS 'FK to table CFG_ENTITY';

COMMENT ON COLUMN CFG_ENTITY_APPROVAL.cfg_approval_category_id IS 'FK to table CFG_APPROVAL_CATEGORY';
