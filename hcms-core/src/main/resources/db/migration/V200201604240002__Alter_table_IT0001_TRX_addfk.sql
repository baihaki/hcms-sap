ALTER TABLE IT0001_TRX ADD FOREIGN KEY (abkrs) REFERENCES T549T (abkrs) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE IT0001_TRX ADD FOREIGN KEY (kostl, kokrs) REFERENCES cskt (kostl, kokrs) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE IT0001_TRX ADD FOREIGN KEY (m_job_id) REFERENCES m_jobs (id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE IT0001_TRX ADD FOREIGN KEY (m_position_id) REFERENCES m_positions (id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE IT0001_TRX ADD FOREIGN KEY (orgeh, endda_orgunit) REFERENCES T527X (orgeh, endda) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE IT0001_TRX ADD FOREIGN KEY (persg) REFERENCES T501T (persg) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE IT0001_TRX ADD FOREIGN KEY (persk) REFERENCES T503K (mandt) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE IT0001_TRX ADD FOREIGN KEY (plans, endda_emposition) REFERENCES T528T (plans, endda) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE IT0001_TRX ADD FOREIGN KEY (stell, endda_empjob) REFERENCES T513S (stell, endda) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE IT0001_TRX ADD FOREIGN KEY (werks2, btrtl) REFERENCES V_001P_ALL (werks, btrtl) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE IT0001_TRX ADD FOREIGN KEY (werks, bukrs) REFERENCES T500P (persa, bukrs) ON UPDATE CASCADE ON DELETE RESTRICT;

