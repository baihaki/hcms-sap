ALTER TABLE teven DROP CONSTRAINT teven_pkey;
ALTER TABLE teven ALTER COLUMN pdsnr DROP NOT NULL;
ALTER TABLE teven ADD COLUMN tevenid character varying(255) NOT NULL DEFAULT uuid_generate_v1mc();
ALTER TABLE teven ADD PRIMARY KEY (tevenid);
COMMENT ON COLUMN teven.tevenid IS 'PRIMARY KEY - TEVEN ID';

-- Delete item from file "V200201609110003__Create_table_TEVEN_time_event_type.sql"
DELETE FROM teven WHERE pdsnr = 8731313