CREATE TABLE ca_stl_hdr
(
  req_no character(13) NOT NULL DEFAULT LEFT((uuid_generate_v4())::text, 13), -- Cash Advance Settlement Number
  tca_no character varying(255), -- Cash Advance Request Number
  doc_no character varying(255), -- Settlement Document Number
  total_amount numeric(15,2), -- Settlement Total Amount
  diff_amount numeric(15,2), -- Settlement Differency Amount
  currency character varying(5) NOT NULL,
  cas_status character(2), -- Cash Advance Settlement Status (OP=Overpay | UP=Underpay | BL=Balance)
  created_at timestamp with time zone NOT NULL DEFAULT timezone('utc'::text, now()),
  created_by character varying(255) NOT NULL,
  updated_at timestamp with time zone,
  updated_by character varying(255),
  process integer NOT NULL DEFAULT 0, -- Current Process of Workflow
  notes character varying(1024), -- Settlement Notes
  bukrs character varying(10),
  refund_amount numeric(15,2) NOT NULL DEFAULT 0,
  claim_amount numeric(15,2) NOT NULL DEFAULT 0,
  pernr bigint NOT NULL,
  reason character varying(1024), -- Settlement Notes By Approval
  payment_status character varying(1),
  payment_date date,
  docno character varying(10),
  last_sync timestamp with time zone,
  payment_status2 character varying(1),
  docno2 character varying(10),
  attachment character varying(255), -- Attachment Path
  sap_message character varying(255),
  CONSTRAINT ca_stl_hdr_pkey PRIMARY KEY (req_no),
  CONSTRAINT ca_stl_hdr_tca_no_fkey FOREIGN KEY (tca_no)
      REFERENCES ca_req_hdr (req_no) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT ca_stl_hdr_tca_no_ukey UNIQUE (tca_no),
  CONSTRAINT ca_stl_hdr_cas_status_check CHECK (cas_status::text = ANY (ARRAY['OP'::text, 'UP'::text, 'BL'::text]))
);
COMMENT ON COLUMN ca_stl_hdr.req_no IS 'Cash Advance Settlement Number';
COMMENT ON COLUMN ca_stl_hdr.tca_no IS 'Cash Advance Request Number';
COMMENT ON COLUMN ca_stl_hdr.doc_no IS 'Settlement Document Number';
COMMENT ON COLUMN ca_stl_hdr.total_amount IS 'Settlement Total Amount';
COMMENT ON COLUMN ca_stl_hdr.diff_amount IS 'Settlement Differency Amount';
COMMENT ON COLUMN ca_stl_hdr.cas_status IS 'Cash Advance Settlement Status (OP=Overpay | UP=Underpay)';
COMMENT ON COLUMN ca_stl_hdr.process IS 'Current Process of Workflow';
COMMENT ON COLUMN ca_stl_hdr.notes IS 'Settlement Notes';
COMMENT ON COLUMN ca_stl_hdr.reason IS 'Settlement Notes By Approval';
COMMENT ON COLUMN ca_stl_hdr.attachment IS 'Attachment Path';


CREATE TABLE ca_stl_dtl
(
  id character(13) NOT NULL DEFAULT LEFT((uuid_generate_v4())::text, 13), -- Cash Advance Settlement Detail ID
  item_no smallint NOT NULL DEFAULT 1, -- Cash Advance Settlement Detail (Sequence / Item Number)
  req_no character varying(13) NOT NULL, -- Cash Advance Settlement Number (Header Number)
  stl_date date, -- Settlement Detail Date
  description character varying(50), -- Settlement Detail Description
  amount numeric(15,2), -- Total Amount of Settlement Detail
  currency character varying(5) NOT NULL,
  order_id bigint, -- GL Account Order ID
  created_at timestamp with time zone NOT NULL DEFAULT timezone('utc'::text, now()),
  created_by character varying(255) NOT NULL,
  updated_at timestamp with time zone NOT NULL DEFAULT timezone('utc'::text, now()),
  updated_by character varying(255) NOT NULL,
  pool_budget smallint NOT NULL DEFAULT 0,
  CONSTRAINT ca_stl_dtl_pkey PRIMARY KEY (id),
  CONSTRAINT ca_stl_dtl_order_id_fkey FOREIGN KEY (order_id)
      REFERENCES ca_order (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT ca_stl_dtl_req_no_fkey FOREIGN KEY (req_no)
      REFERENCES ca_stl_hdr (req_no) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);
COMMENT ON COLUMN ca_stl_dtl.id IS 'Cash Advance Settlement Detail ID';
COMMENT ON COLUMN ca_stl_dtl.item_no IS 'Cash Advance Settlement Detail (Sequence / Item Number)';
COMMENT ON COLUMN ca_stl_dtl.req_no IS 'Cash Advance Settlement Number (Header Number)';
COMMENT ON COLUMN ca_stl_dtl.stl_date IS 'Settlement Detail Date';
COMMENT ON COLUMN ca_stl_dtl.description IS 'Settlement Detail Description';
COMMENT ON COLUMN ca_stl_dtl.amount IS 'Total Amount of Settlement Detail';
COMMENT ON COLUMN ca_stl_dtl.order_id IS 'GL Account Order ID';


CREATE TABLE ca_emp_cskt
(
  emp_id bigint NOT NULL, -- Employee ID
  pernr bigint,
  kokrs character varying(10), -- Controlling Area Code of Cost Center
  kostl_default character varying(10) NOT NULL, -- Cost Center Code Default
  kostl_level smallint, -- Cost Center Level
  kostl_pool character varying(10),  -- Cost Center Pooling Assignment
  pooling_code character varying(10), -- Pooling Code of Cost Center, BU[BUTTOM_UP] & TD[TOP_DOWN]
  CONSTRAINT ca_emp_cskt_pkey PRIMARY KEY (emp_id),
  CONSTRAINT ca_emp_cskt_fkey FOREIGN KEY (kokrs, kostl_default)
      REFERENCES cskt (kokrs, kostl) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT ca_emp_cskt_ukey UNIQUE (pernr, kokrs, kostl_default, kostl_pool),
  CONSTRAINT ca_emp_csk_pooling_code_check CHECK (pooling_code::text = ANY (ARRAY['BU'::text, 'TD'::text]))
);
CREATE SEQUENCE emp_cskt_serial OWNED BY ca_emp_cskt.emp_id;
ALTER TABLE ONLY ca_emp_cskt ALTER COLUMN emp_id SET DEFAULT nextval('emp_cskt_serial'::regclass);
COMMENT ON TABLE ca_emp_cskt IS 'Employee Assign to Cost Center';
COMMENT ON COLUMN ca_emp_cskt.emp_id IS 'Employee ID';
COMMENT ON COLUMN ca_emp_cskt.kokrs IS 'Controlling Area Code of Cost Center';
COMMENT ON COLUMN ca_emp_cskt.kostl_default IS 'Cost Center Code Default';
COMMENT ON COLUMN ca_emp_cskt.kostl_level IS 'Cost Center Level';
COMMENT ON COLUMN ca_emp_cskt.kostl_pool IS 'Cost Center Pooling Assignment';
COMMENT ON COLUMN ca_emp_cskt.pooling_code IS 'Pooling Code of Cost Center, BU[BUTTOM_UP] & TD[TOP_DOWN]';

CREATE TABLE trx_approval_process
(
  id character varying(255) NOT NULL DEFAULT uuid_generate_v1mc(),
  cfg_entity_id character varying(255),
  entity_object_id character varying(255), -- key transaction
  seq integer, -- sequences
  process integer,
  m_role_id character varying(255),
  approval_at timestamp with time zone,
  approval_by character varying,
  approval_action character varying(25),
  approval_note character varying(255),
  CONSTRAINT trx_approval_process_pkey PRIMARY KEY (id),
  CONSTRAINT approval_by_fkey FOREIGN KEY (approval_by)
      REFERENCES m_user (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT trx_approval_process_cfg_entity_id_fkey FOREIGN KEY (cfg_entity_id)
      REFERENCES cfg_entity (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT user_roles_m_role_id_fkey FOREIGN KEY (m_role_id)
      REFERENCES m_role (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);
COMMENT ON COLUMN trx_approval_process.entity_object_id IS 'key transaction';
COMMENT ON COLUMN trx_approval_process.seq IS 'sequences';

ALTER TABLE cskt ADD COLUMN level_code smallint;
COMMENT ON COLUMN cskt.level_code IS '1: Root, 0: Non Root';
UPDATE cskt SET level_code = 1 WHERE kostl = '991001';
UPDATE cskt SET level_code = 0 WHERE kostl <> '991001' AND kostl LIKE '99%';
