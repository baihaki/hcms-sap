﻿ALTER TABLE it0022 ADD COLUMN attach1_subty character varying(4) NOT NULL DEFAULT 'IJA'::character varying;
ALTER TABLE it0022 ADD COLUMN attach1_path character varying(255);
ALTER TABLE it0022 ADD CONSTRAINT it0022_attach1_fkey FOREIGN KEY (infty, attach1_subty)
   REFERENCES cfg_attachment (infty, subty) ON UPDATE CASCADE ON DELETE RESTRICT;