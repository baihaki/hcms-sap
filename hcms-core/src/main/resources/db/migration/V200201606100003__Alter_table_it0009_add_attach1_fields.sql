ALTER TABLE it0009 ADD COLUMN attach1_subty character varying(4) NOT NULL DEFAULT 'REK'::character varying;
ALTER TABLE it0009 ADD COLUMN attach1_path character varying(255);
ALTER TABLE it0009 ADD CONSTRAINT it0009_attach1_fkey FOREIGN KEY (infty, attach1_subty)
   REFERENCES cfg_attachment (infty, subty) ON UPDATE CASCADE ON DELETE RESTRICT;