﻿ALTER TABLE it0185 ADD COLUMN attach1_subty character varying(4) NOT NULL DEFAULT 'ID'::character varying;
ALTER TABLE it0185 ADD COLUMN attach1_path character varying(255);
ALTER TABLE it0185 ADD CONSTRAINT it0185_attach1_fkey FOREIGN KEY (infty, attach1_subty)
   REFERENCES cfg_attachment (infty, subty) ON UPDATE CASCADE ON DELETE RESTRICT;