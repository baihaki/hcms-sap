CREATE TABLE IT0241 (
  pernr bigint not null,
  endda date not null,
  begda date not null,
  seqnr bigint default 0,
  aedtm timestamp with time zone not null default timezone('utc'::text, now()),
  uname character varying(255),
  taxid character varying(50) not null,
  marrd char(1),
  spben char(1),
  refno character varying(50),
  depnd char(2),
  rdate date
);

COMMENT ON TABLE IT0241 IS 'Master data transaction for Tax Data Indonesia';

COMMENT ON COLUMN IT0241.PERNR IS 'Employee SSN/ID';

COMMENT ON COLUMN IT0241.endda IS 'End Date';

COMMENT ON COLUMN IT0241.begda IS 'Begin Date';

COMMENT ON COLUMN IT0241.seqnr IS 'Infotype Record No.';

COMMENT ON COLUMN IT0241.aedtm IS 'Changed On';

COMMENT ON COLUMN IT0241.uname IS 'Changed By';

COMMENT ON COLUMN IT0241.taxid IS 'Tax ID';

COMMENT ON COLUMN IT0241.marrd IS 'Married for Tax Purposes';

COMMENT ON COLUMN IT0241.spben IS 'Spouse Benefit';

COMMENT ON COLUMN IT0241.refno IS 'Unemployment Reference Number';

COMMENT ON COLUMN IT0241.depnd IS 'Number of Dependents for Tax Purposes';

COMMENT ON COLUMN IT0241.rdate IS 'NPWP Registration Date';
