﻿ALTER TABLE trx_event_log ADD COLUMN status character varying(25) DEFAULT 'INITIAL_DRAFT';

COMMENT ON COLUMN trx_event_log.status IS 'Updated Document Status of IT object';