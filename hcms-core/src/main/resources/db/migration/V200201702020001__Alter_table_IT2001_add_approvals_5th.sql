﻿ALTER TABLE IT2001
  ADD COLUMN approved4_by character varying(255);
ALTER TABLE IT2001
  ADD COLUMN approved4_at timestamp with time zone;
ALTER TABLE IT2001
  ADD COLUMN approved5_by character varying(255);
ALTER TABLE IT2001
  ADD COLUMN approved5_at timestamp with time zone;
