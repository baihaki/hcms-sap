﻿ALTER TABLE ptrv_head ADD COLUMN total_amount numeric(15,2);
ALTER TABLE ptrv_head ADD COLUMN reject_amounts character varying(255);

COMMENT ON COLUMN ptrv_head.total_amount IS 'Expense Total Amount';
COMMENT ON COLUMN ptrv_head.reject_amounts IS 'Reject Amounts for each Expense Type';