ALTER TABLE IT0021 ADD COLUMN document_status character varying(25) NOT NULL DEFAULT 'DRAFT';

ALTER TABLE IT0021 ADD CONSTRAINT it0021_check_document_status CHECK (document_status IN ('DRAFT', 'PUBLISHED'));
