ALTER TABLE it0021 ADD COLUMN attach1_subty character varying(4) NOT NULL DEFAULT 'KK'::character varying;
ALTER TABLE it0021 ADD COLUMN attach1_path character varying(255);
ALTER TABLE it0021 ADD CONSTRAINT it0021_attach1_fkey FOREIGN KEY (infty, attach1_subty)
   REFERENCES cfg_attachment (infty, subty) ON UPDATE CASCADE ON DELETE RESTRICT;