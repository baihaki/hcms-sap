CREATE TABLE CFG_ENTITY (
  id character varying(255) NOT NULL,
  entity_name character varying(255) NOT NULL,
  cfg_approval_category_id character varying(255) NOT NULL,
  created_at timestamp with time zone not null default timezone('utc'::text, now()),
  created_by_id character varying(255) NOT NULL,
  updated_at timestamp with time zone not null default timezone('utc'::text, now()),
  updated_by_id character varying(255) NOT NULL,
  deleted_at timestamp with time zone,
  deleted_by_id character varying(255)
);

COMMENT ON TABLE CFG_ENTITY IS 'List of Entities';
COMMENT ON COLUMN CFG_ENTITY.entity_name IS 'Name of Entity (table|class)';
COMMENT ON COLUMN CFG_ENTITY.cfg_approval_category_id IS 'FK to cfg_approval_category table';

ALTER TABLE CFG_ENTITY add PRIMARY KEY (id);
ALTER TABLE CFG_ENTITY ADD FOREIGN KEY (cfg_approval_category_id) REFERENCES CFG_APPROVAL_CATEGORY (id) ON UPDATE CASCADE ON DELETE RESTRICT;

