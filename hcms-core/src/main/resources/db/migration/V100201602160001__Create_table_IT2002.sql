CREATE TABLE IT2002 (
  pernr bigint not null,
  subty character varying(4) not null,
  endda date not null,
  begda date not null,
  seqnr bigint default 0,
  aedtm timestamp with time zone not null default timezone('utc'::text, now()),
  uname character varying(255),
  awart character varying(10) not null,
  abwtg numeric(10, 2),
  kaltg numeric(10, 2),
  stdaz numeric(10, 2)
);

COMMENT ON TABLE IT2002 IS 'Master data transaction for Attendances';

COMMENT ON COLUMN IT2002.pernr IS 'Employee ID/SSN';

COMMENT ON COLUMN IT2002.subty IS 'Subtype';

COMMENT ON COLUMN IT2002.endda IS 'End Date';

COMMENT ON COLUMN IT2002.begda IS 'Begin Date';

COMMENT ON COLUMN IT2002.seqnr IS 'Infotype Record No.';

COMMENT ON COLUMN IT2002.aedtm IS 'Changed On';

COMMENT ON COLUMN IT2002.uname IS 'Changed By';

COMMENT ON COLUMN IT2002.awart IS 'Attendance Type';

COMMENT ON COLUMN IT2002.abwtg IS 'Attendance Days';

COMMENT ON COLUMN IT2002.kaltg IS 'Calendar Days';

COMMENT ON COLUMN IT2002.stdaz IS 'Attendance Hours';
