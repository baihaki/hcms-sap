CREATE TABLE phdat
(
   pinco character varying(4), 
   phdtxt character varying(40), 
   erdat date, 
   phtxt character varying(40), 
   PRIMARY KEY (erdat)
);
COMMENT ON COLUMN phdat.pinco IS 'public holiday type';
COMMENT ON COLUMN phdat.phdtxt IS 'public holiday description';
COMMENT ON COLUMN phdat.erdat IS 'public holiday date';
COMMENT ON COLUMN phdat.phtxt IS 'public holiday';
COMMENT ON TABLE phdat IS 'calender hari libur nasional dan cuti bersama';

INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('9981', 'HARI LIBUR NASIONAL', '2016-01-01', 'TAHUN BARU 2016');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('9982', 'HARI LIBUR NASIONAL', '2016-02-08', 'TAHUN BARU IMLEK 2567 KONGZILI');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('9983', 'HARI LIBUR NASIONAL', '2016-03-09', 'HARI RAYA NYEPI TAHUN BARU SAKA 1938');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('9984', 'HARI LIBUR NASIONAL', '2016-03-25', 'WAFAT ISA AL-MASIH');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('9985', 'HARI LIBUR NASIONAL', '2016-05-01', 'HARI BURUH INTERNASIONAL');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('9986', 'HARI LIBUR NASIONAL', '2016-05-05', 'KENAIKAN YESUS KRISTUS');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('9987', 'HARI LIBUR NASIONAL', '2016-05-06', 'ISRA MIR`AJ NABI MUHAMMAD SAW');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('9988', 'HARI LIBUR NASIONAL', '2016-05-22', 'HARI RAYA WAISAK 2560');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('9989', 'HARI LIBUR NASIONAL', '2016-07-06', 'HARI RAYA IDUL FITRI 1437 HIJRIYAH');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('9990', 'HARI LIBUR NASIONAL', '2016-07-07', 'HARI RAYA IDUL FITRI 1437 HIJRIYAH');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('9991', 'HARI LIBUR NASIONAL', '2016-08-17', 'HARI KEMERDAKAAN REPUBLIK INDONESIA');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('9992', 'HARI LIBUR NASIONAL', '2016-09-12', 'HARI RAYA IDUL ADHA 1437 HIJRIYAH');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('9993', 'HARI LIBUR NASIONAL', '2016-10-02', 'TAHUN BARU ISLAM 1438 HIJRIYAH');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('9994', 'HARI LIBUR NASIONAL', '2016-12-12', 'MAULID NABI MUHAMMAD SAW');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('9995', 'HARI LIBUR NASIONAL', '2016-12-25', 'HARI RAYA NATAL');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('111', 'CUTI BERSAMA', '2016-07-04', 'HARI RAYA IDUL FITRI 1437 HIJRIYAH');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('111', 'CUTI BERSAMA', '2016-07-05', 'HARI RAYA IDUL FITRI 1437 HIJRIYAH');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('111', 'CUTI BERSAMA', '2016-07-08', 'HARI RAYA IDUL FITRI 1437 HIJRIYAH');
INSERT INTO phdat(pinco, phdtxt, erdat, phtxt) VALUES('111', 'CUTI BERSAMA', '2016-12-26', 'HARI RAYA NATAL');

