DROP VIEW public.tevenvw;

CREATE OR REPLACE VIEW public.tevenvw AS 
	SELECT 
		tevenvw.pernr || '.' || tevenvw.ldate AS id,
		pernr, 
		ename, 
		ldate, 
		begltime,
		endltime,
		( SELECT trunc((date_part('hour'::text, ldate + endltime - (ldate + begltime)) + date_part('minute'::text, ldate + endltime - (ldate + begltime)) / 60::double precision)::numeric, 2) AS trunc) AS whour,
		( SELECT trunc(((date_part('hour'::text, ldate + endltime - (ldate + begltime)) + date_part('minute'::text, ldate + endltime - (ldate + begltime)) / 60::double precision) / 8.75::double precision)::numeric, 2) AS trunc) AS phour,
		begprocess, -- status process check in
		endprocess, -- status process check out
		begtrxcode, -- transaction code status check in
		endtrxcode, -- transaction code status check out
		begreason,	-- begin reason
		endreason	-- end reason
	FROM
	(
		-- TEVEN CHECK IN
		(SELECT 
				emp.pernr, 
				emp.ename,
				teven.ldate,
				teven.ltime AS begltime,
				(SELECT tev.ltime FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P20') AS endltime,
				teven.process AS begprocess,
				(SELECT tev.process FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P20') AS endprocess,
				teven.trxcode AS begtrxcode,
				(SELECT tev.trxcode FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P20') AS endtrxcode,
				teven.reason AS begreason,
				(SELECT reason FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P20') AS endreason
			FROM (SELECT DISTINCT pernr, ename FROM IT0001) emp LEFT JOIN teven ON emp.pernr = teven.pernr AND satza = 'P10') 
		UNION
		-- TEVEN CHECK OUT
		(SELECT 
				emp.pernr, 
				emp.ename,
				teven.ldate,
				(SELECT tev.ltime FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P10') AS begltime,
				teven.ltime AS endltime,
				(SELECT tev.process FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P10') AS begprocess,
				teven.process AS endprocess,
				(SELECT tev.trxcode FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P10') AS begtrxcode,
				teven.trxcode AS endtrxcode,
				(SELECT tev.reason FROM teven tev WHERE tev.pernr = teven.pernr AND tev.ldate = teven.ldate AND tev.satza = 'P10') AS begreason,
				teven.reason AS endreason
			FROM (SELECT DISTINCT pernr, ename FROM IT0001) emp LEFT JOIN teven ON emp.pernr = teven.pernr AND satza = 'P20') 
	) tevenvw
	WHERE ldate IS NOT NULL;

ALTER TABLE public.tevenvw OWNER TO hcms;