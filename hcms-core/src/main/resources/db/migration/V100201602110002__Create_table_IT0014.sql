CREATE TABLE IT0014 (
  pernr bigint not null,
  subty character varying(4) not null,
  endda date not null,
  begda date not null,
  seqnr bigint default 0,
  aedtm timestamp with time zone not null default timezone('utc'::text, now()),
  uname character varying(255),
  lgart character varying(15) not null,
  opken character(1),
  betrg numeric(15,2),
  waers character varying(5)
);

COMMENT ON TABLE IT0014 IS 'Master data transaction table for Recurring Payment/Deductions';

COMMENT ON COLUMN IT0014.pernr IS 'Employee SSN/ID';

COMMENT ON COLUMN IT0014.subty IS 'Subtype';

COMMENT ON COLUMN IT0014.endda IS 'End Date';

COMMENT ON COLUMN IT0014.begda IS 'Begin Date';

COMMENT ON COLUMN IT0014.seqnr IS 'Infotype Record No.';

COMMENT ON COLUMN IT0014.aedtm IS 'Changed On';

COMMENT ON COLUMN IT0014.uname IS 'Changed By';

COMMENT ON COLUMN IT0014.lgart IS 'Wage Type';

COMMENT ON COLUMN IT0014.opken IS 'Operation Indicator';

COMMENT ON COLUMN IT0014.betrg IS 'Amount';

COMMENT ON COLUMN IT0014.waers IS 'Currency';
