ALTER TABLE trx_approval_event_log ADD COLUMN cfg_entity_id character varying(255) NOT NULL;

ALTER TABLE trx_approval_event_log ADD COLUMN entity_object_id character varying(255) NOT NULL;

ALTER TABLE trx_approval_event_log ADD FOREIGN KEY (cfg_entity_id) REFERENCES cfg_entity (id) ON UPDATE CASCADE ON DELETE RESTRICT;
