CREATE TABLE ZMED_TRXITM (
  ITMNO character varying(10) NOT NULL,
  BUKRS character varying(10),
  CLMNO character varying(10),
  BILLDT date,
  KODEMED character varying(5),
  KODECOS character varying(5),
  ZCOSTYP character varying(40),
  CLMAMT numeric(15, 2),
  REJAMT numeric(15, 2),
  APPAMT numeric(15, 2),
  RELAT character varying(4),
  UMUR character varying(2),
  PASIEN character varying(50)
);

COMMENT ON TABLE ZMED_TRXITM IS 'Medical Transaction';

COMMENT ON COLUMN ZMED_TRXITM.ITMNO IS 'Character Field Length = 10';

COMMENT ON COLUMN ZMED_TRXITM.BUKRS IS 'Company Code';

COMMENT ON COLUMN ZMED_TRXITM.CLMNO IS 'Character Field Length = 10';

COMMENT ON COLUMN ZMED_TRXITM.BILLDT IS 'Date on Which Record Was Created (Date of Bill)';

COMMENT ON COLUMN ZMED_TRXITM.KODEMED IS 'R/2 table';

COMMENT ON COLUMN ZMED_TRXITM.KODECOS IS 'R/2 table';

COMMENT ON COLUMN ZMED_TRXITM.ZCOSTYP IS 'Cost Types';

COMMENT ON COLUMN ZMED_TRXITM.CLMAMT IS 'Wage Type Amount for Payments';

COMMENT ON COLUMN ZMED_TRXITM.REJAMT IS 'Wage Type Amount for Payments';

COMMENT ON COLUMN ZMED_TRXITM.APPAMT IS 'Wage Type Amount for Payments';

COMMENT ON COLUMN ZMED_TRXITM.RELAT IS 'Type of Family Record';

COMMENT ON COLUMN ZMED_TRXITM.UMUR IS 'Version Number Component';

COMMENT ON COLUMN ZMED_TRXITM.PASIEN IS 'Additional Partner Name';
