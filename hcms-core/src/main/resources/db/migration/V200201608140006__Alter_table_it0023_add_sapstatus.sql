﻿ALTER TABLE it0023 ADD COLUMN sap_status character(1);
ALTER TABLE it0023 ADD COLUMN sap_message character varying(255);
COMMENT ON COLUMN it0023.sap_status IS 'Status of SAP Synchronization for PUBLISHED document (S=Synchronized | E=Error returned by BAPI | F=Failed connnection to BAPI)';
COMMENT ON COLUMN it0023.sap_message IS 'Any Error/Failure Message returned by SAP Synchronization for PUBLISHED document';
