ALTER TABLE workflow_steps drop constraint workflow_steps_m_workflow_id_seq_name_m_position_id_key;

ALTER TABLE workflow_steps drop column name;

ALTER TABLE workflow_steps ADD CONSTRAINT workflow_steps_m_workflow_id_seq_name_m_position_id_key UNIQUE (m_workflow_id, seq, m_position_id);
