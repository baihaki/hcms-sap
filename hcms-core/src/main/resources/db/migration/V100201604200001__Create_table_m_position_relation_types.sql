create table m_position_relation_types (
  id character varying(255) not null default uuid_generate_v1mc(),
  opt_lock bigint default 0,
  name character varying(255) not null,
  description text,
  system boolean not null default false,
  created_at timestamp with time zone not null default timezone('utc'::text, now()),
  created_by_id character varying(255) not null,
  updated_at timestamp with time zone not null default timezone('utc'::text, now()),
  updated_by_id character varying(255) not null,
  deleted_at timestamp with time zone,
  deleted_by_id character varying(255)
);
  
