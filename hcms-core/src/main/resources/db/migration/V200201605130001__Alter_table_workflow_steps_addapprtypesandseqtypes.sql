CREATE TYPE sequenceType AS ENUM ('SERIAL', 'PARALEL');

CREATE TYPE approvalType AS ENUM ('ALL_SUPERIOR', 'ONE_SUPERIOR');

ALTER TABLE workflow_steps ADD COLUMN sequence_type sequenceType NOT NULL DEFAULT 'SERIAL';

ALTER TABLE workflow_steps ADD COLUMN approval_type approvalType NOT NULL DEFAULT 'ALL_SUPERIOR';
