CREATE TABLE workflow_steps (
  id character varying(255) NOT NULL DEFAULT uuid_generate_v1mc(),
  opt_lock bigint default 0,
  m_workflow_id character varying(255) NOT NULL,
  seq integer not null default 1,
  name character varying(255) NOT NULL,
  description text,
  m_position_id character varying(255) NOT NULL,
  created_at timestamp with time zone not null default timezone('utc'::text, now()),
  created_by_id character varying(255),
  updated_at timestamp with time zone not null default timezone('utc'::text, now()),
  updated_by_id character varying(255),
  deleted_at timestamp with time zone,
  deleted_by_id character varying(255)
);

COMMENT ON TABLE workflow_steps IS 'Workflow Steps';

COMMENT ON COLUMN workflow_steps.m_workflow_id IS 'Workflow ID';

COMMENT ON COLUMN workflow_steps.seq IS 'Workflow Sequence No';

COMMENT ON COLUMN workflow_steps.name IS 'Workflow Step Name';

COMMENT ON COLUMN workflow_steps.description IS 'Workflow Description';

COMMENT ON COLUMN workflow_steps.m_position_id IS 'Employee Position ID';

COMMENT ON COLUMN workflow_steps.created_at IS 'Created On';

COMMENT ON COLUMN workflow_steps.created_by_id IS 'Created By';

COMMENT ON COLUMN workflow_steps.updated_at IS 'Updated On';

COMMENT ON COLUMN workflow_steps.updated_by_id IS 'Updated By';

COMMENT ON COLUMN workflow_steps.deleted_at IS 'Deleted on';

COMMENT ON COLUMN workflow_steps.deleted_by_id IS 'Deleted By';
