﻿ALTER TABLE zmed_trxhdr ADD COLUMN banka character varying(60);
ALTER TABLE zmed_trxhdr ADD COLUMN bankn character varying(100);

COMMENT ON COLUMN zmed_trxhdr.banka IS 'Current Employee Bank Name';
COMMENT ON COLUMN zmed_trxhdr.bankn IS 'Current Employee Bank Account Number';