CREATE TABLE CFG_APPROVAL_CATEGORY (
   id character varying(255) NOT NULL DEFAULT uuid_generate_v1mc(),
   ent_category character varying(255) NOT NULL DEFAULT 'HR_MASTER_DATA',
   m_workflow_id character varying(255) not null,
   created_at timestamp with time zone not null default timezone('utc'::text, now()),
   created_by_id character varying(255) not null,
   updated_at timestamp with time zone not null default timezone('utc'::text, now()),
   updated_by_id character varying(255) not null,
   deleted_at timestamp with time zone,
   deleted_by_id character varying(255)
);

COMMENT ON TABLE CFG_APPROVAL_CATEGORY IS 'approval config based on entity category';
COMMENT ON COLUMN CFG_APPROVAL_CATEGORY.id IS 'approval category id (key)';
COMMENT ON COLUMN CFG_APPROVAL_CATEGORY.ent_category IS 'category of entity';
COMMENT ON COLUMN CFG_APPROVAL_CATEGORY.m_workflow_id IS 'Workflow ID';

ALTER TABLE CFG_APPROVAL_CATEGORY ADD PRIMARY KEY (id);
ALTER TABLE CFG_APPROVAL_CATEGORY ADD CONSTRAINT entity_category_check CHECK (ent_category IN ('HR_MASTER_DATA', 'MEDICAL', 'TRAVEL', 'TIME'));
ALTER TABLE CFG_APPROVAL_CATEGORY ADD FOREIGN KEY (m_workflow_id) REFERENCES m_workflows (id) ON UPDATE CASCADE ON DELETE RESTRICT;
