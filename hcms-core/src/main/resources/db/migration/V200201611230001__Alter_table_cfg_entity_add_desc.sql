﻿ALTER TABLE cfg_entity ADD COLUMN entity_desc character varying(255);

COMMENT ON COLUMN cfg_entity.entity_desc IS 'Description of Entity';