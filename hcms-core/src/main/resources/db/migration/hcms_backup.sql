--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-08-28 22:47:06 WIB

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12623)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3325 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 2 (class 3079 OID 49847)
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- TOC entry 3326 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 184 (class 1259 OID 55886)
-- Name: bnka; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE bnka (
    bankl character varying(255) NOT NULL,
    adrnr character varying(255),
    banka character varying(255) NOT NULL,
    brnch character varying(255),
    ort01 character varying(255),
    stras character varying(255)
);


--
-- TOC entry 185 (class 1259 OID 55894)
-- Name: cfg_approval_category; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cfg_approval_category (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id character varying(255),
    updated_at timestamp without time zone NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    ent_category character varying(255) NOT NULL,
    m_workflow_id character varying(255) NOT NULL
);


--
-- TOC entry 186 (class 1259 OID 55902)
-- Name: cfg_attachment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cfg_attachment (
    infty character varying(255) NOT NULL,
    subty character varying(255) NOT NULL,
    stext character varying(40) NOT NULL
);


--
-- TOC entry 187 (class 1259 OID 55910)
-- Name: cfg_entity; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cfg_entity (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id character varying(255),
    updated_at timestamp without time zone NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    entity_name character varying(255) NOT NULL
);


--
-- TOC entry 188 (class 1259 OID 55918)
-- Name: cfg_entity_approval; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cfg_entity_approval (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id character varying(255),
    updated_at timestamp without time zone NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    operation_type character varying(255) NOT NULL,
    cfg_approval_category_id character varying(255) NOT NULL,
    cfg_entity_id character varying(255) NOT NULL
);


--
-- TOC entry 183 (class 1259 OID 51521)
-- Name: cfg_region_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cfg_region_types (
    id character varying(255) DEFAULT uuid_generate_v1mc() NOT NULL,
    opt_lock bigint DEFAULT 0 NOT NULL,
    ident_name character varying(100) NOT NULL,
    name character varying(100) NOT NULL,
    description text,
    system boolean DEFAULT true,
    created_at timestamp with time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    created_by_id character varying(255) NOT NULL,
    updated_at timestamp with time zone DEFAULT timezone('utc'::text, now()) NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    deleted_at timestamp with time zone,
    deleted_by_id character varying(255)
);


--
-- TOC entry 3327 (class 0 OID 0)
-- Dependencies: 183
-- Name: TABLE cfg_region_types; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE cfg_region_types IS 'Configuration Table for Region Types';


--
-- TOC entry 3328 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN cfg_region_types.ident_name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN cfg_region_types.ident_name IS 'Region Type unique name (used as system identifier)';


--
-- TOC entry 3329 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN cfg_region_types.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN cfg_region_types.name IS 'Region Type name (human friendly name)';


--
-- TOC entry 3330 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN cfg_region_types.system; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN cfg_region_types.system IS 'flag to indicate whether this record was generated by the system or not';


--
-- TOC entry 189 (class 1259 OID 55926)
-- Name: cfg_vehicle_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cfg_vehicle_types (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id character varying(255),
    updated_at timestamp without time zone NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    description character varying(255),
    ident_name character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


--
-- TOC entry 190 (class 1259 OID 55934)
-- Name: cskt; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cskt (
    kokrs character varying(10) NOT NULL,
    kostl character varying(10) NOT NULL,
    kltxt character varying(255),
    ktext character varying(150) NOT NULL,
    mcds3 character varying(255) NOT NULL
);


--
-- TOC entry 191 (class 1259 OID 55942)
-- Name: it0001; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0001 (
    begda date NOT NULL,
    endda date NOT NULL,
    pernr bigint NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    ename character varying(40),
    gsber character varying(10),
    mstbr character varying(8),
    otype character varying(2),
    sacha character varying(3),
    sachp character varying(3),
    sachz character varying(3),
    sbmod character varying(4),
    seqnr bigint,
    sname character varying(30),
    vdsk1 character varying(14),
    kokrs character varying(10),
    kostl character varying(10),
    m_job_id character varying(255),
    m_position_id character varying(255),
    bukrs character varying(255),
    werks character varying(10),
    persg character varying(10),
    persk character varying(10),
    endda_empjob date,
    stell bigint,
    endda_orgunit date,
    orgeh bigint,
    endda_emposition date,
    plans bigint,
    abkrs character varying(10),
    btrtl character varying(10),
    werks2 character varying(10)
);


--
-- TOC entry 192 (class 1259 OID 55950)
-- Name: it0001_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0001_log (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id character varying(255),
    updated_at timestamp without time zone NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    aedtm timestamp without time zone,
    begda date NOT NULL,
    ename character varying(40),
    endda date NOT NULL,
    gsber character varying(10),
    mstbr character varying(8),
    otype character varying(2),
    pernr bigint NOT NULL,
    sacha character varying(3),
    sachp character varying(3),
    sachz character varying(3),
    sbmod character varying(4),
    seqnr bigint,
    sname character varying(30),
    uname character varying(255),
    vdsk1 character varying(14),
    kokrs character varying(10),
    kostl character varying(10),
    m_job_id character varying(255),
    m_position_id character varying(255),
    bukrs character varying(255),
    werks character varying(10),
    persg character varying(10),
    persk character varying(10),
    endda_empjob date,
    stell bigint,
    endda_orgunit date,
    orgeh bigint,
    endda_emposition date,
    plans bigint,
    abkrs character varying(10),
    trx_id character varying(255),
    btrtl character varying(10),
    werks2 character varying(10)
);


--
-- TOC entry 193 (class 1259 OID 55958)
-- Name: it0001_trx; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0001_trx (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id character varying(255),
    updated_at timestamp without time zone NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    aedtm timestamp without time zone,
    begda date NOT NULL,
    ename character varying(40),
    endda date NOT NULL,
    gsber character varying(10),
    mstbr character varying(8),
    otype character varying(2),
    pernr bigint NOT NULL,
    sacha character varying(3),
    sachp character varying(3),
    sachz character varying(3),
    sbmod character varying(4),
    seqnr bigint,
    sname character varying(30),
    uname character varying(255),
    vdsk1 character varying(14),
    kokrs character varying(10),
    kostl character varying(10),
    m_job_id character varying(255),
    m_position_id character varying(255),
    bukrs character varying(255),
    werks character varying(10),
    persg character varying(10),
    persk character varying(10),
    endda_empjob date,
    stell bigint,
    endda_orgunit date,
    orgeh bigint,
    endda_emposition date,
    plans bigint,
    abkrs character varying(10),
    btrtl character varying(10),
    werks2 character varying(10)
);


--
-- TOC entry 194 (class 1259 OID 55966)
-- Name: it0002; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0002 (
    begda date NOT NULL,
    endda date NOT NULL,
    pernr bigint NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    sap_message character varying(255),
    sap_status character varying(1),
    anzkd smallint,
    attach1_path character varying(255),
    cname character varying(80) NOT NULL,
    famdt date,
    gbdat date,
    gbort character varying(40),
    gesch character varying(255),
    knznm character varying(255),
    nachn character varying(40),
    rufnm character varying(40),
    seqnr bigint NOT NULL,
    document_status character varying(40) NOT NULL,
    anred character varying(1),
    infty character varying(255),
    attach1_subty character varying(255),
    famst character varying(1),
    gblnd character varying(3),
    konfe character varying(2),
    art3 character varying(1),
    title3 character varying(15),
    natio character varying(3),
    sprsl character varying(3),
    art2 character varying(1),
    title2 character varying(15),
    art character varying(1),
    title character varying(15)
);


--
-- TOC entry 195 (class 1259 OID 55974)
-- Name: it0006; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0006 (
    begda date NOT NULL,
    endda date NOT NULL,
    infty character varying(255) NOT NULL,
    pernr bigint NOT NULL,
    subty character varying(255) NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    sap_message character varying(255),
    sap_status character varying(1),
    anssa character varying(4),
    attach1_path character varying(255),
    com01 character varying(4),
    entk2 double precision,
    entkm double precision,
    locat character varying(40),
    name2 character varying(40),
    num01 character varying(20),
    ort01 character varying(40),
    ort02 character varying(40),
    pstlz character varying(10),
    seqnr bigint,
    document_status character varying(255) NOT NULL,
    stras character varying(60),
    telnr character varying(14),
    attach1_subty character varying(255),
    land1 character varying(3),
    state character varying(3)
);


--
-- TOC entry 196 (class 1259 OID 55982)
-- Name: it0009; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0009 (
    begda date NOT NULL,
    endda date NOT NULL,
    infty character varying(255) NOT NULL,
    pernr bigint NOT NULL,
    subty character varying(255) NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    sap_message character varying(255),
    sap_status character varying(1),
    anzhl double precision,
    attach1_path character varying(255),
    bankn character varying(255),
    betrg double precision,
    bkort character varying(255),
    bkplz character varying(255),
    emftx character varying(255),
    seqnr bigint,
    document_status character varying(255) NOT NULL,
    waers character varying(255),
    zweck character varying(255),
    attach1_subty character varying(255),
    bankl character varying(255),
    banks character varying(3),
    bnksa character varying(255),
    zlsch character varying(255)
);


--
-- TOC entry 197 (class 1259 OID 55990)
-- Name: it0014; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0014 (
    begda date NOT NULL,
    endda date NOT NULL,
    infty character varying(255) NOT NULL,
    pernr bigint NOT NULL,
    subty character varying(255) NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    betrg double precision,
    opken character varying(255),
    seqnr bigint,
    waers character varying(255),
    lgart character varying(255)
);


--
-- TOC entry 198 (class 1259 OID 55998)
-- Name: it0019; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0019 (
    begda date NOT NULL,
    endda date NOT NULL,
    infty character varying(255) NOT NULL,
    pernr bigint NOT NULL,
    subty character varying(255) NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    attach1_path character varying(255),
    bvmrk character varying(255),
    itxex character varying(255),
    mndat date,
    mnjhr integer,
    mnmon integer,
    mntag integer,
    seqnr bigint,
    termn date,
    text1 character varying(255),
    text2 character varying(255),
    text3 character varying(255),
    tmjhr integer,
    tmmon integer,
    tmtag integer,
    attach1_subty character varying(255),
    tmart character varying(255)
);


--
-- TOC entry 199 (class 1259 OID 56006)
-- Name: it0021; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0021 (
    begda date NOT NULL,
    endda date NOT NULL,
    infty character varying(255) NOT NULL,
    pernr bigint NOT NULL,
    subty character varying(255) NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    sap_message character varying(255),
    sap_status character varying(1),
    attach1_path character varying(255),
    fanam character varying(255),
    fasex character varying(255),
    favor character varying(255),
    fcnam character varying(255),
    fgbdt date,
    fgbot character varying(255),
    fknzn integer,
    seqnr bigint,
    document_status character varying(255) NOT NULL,
    attach1_subty character varying(255),
    infty2 character varying(255),
    famsa character varying(255),
    fanat character varying(3),
    fgbld character varying(3),
    fnmzu character varying(1),
    title character varying(15)
);


--
-- TOC entry 200 (class 1259 OID 56014)
-- Name: it0022; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0022 (
    begda date NOT NULL,
    endda date NOT NULL,
    infty character varying(255) NOT NULL,
    pernr bigint NOT NULL,
    subty character varying(255) NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    sap_message character varying(255),
    sap_status character varying(1),
    anzkl double precision,
    attach1_path character varying(255),
    emark character varying(255),
    insti character varying(255),
    seqnr bigint,
    document_status character varying(255) NOT NULL,
    anzeh character varying(255),
    attach1_subty character varying(255),
    ausbi bigint,
    slabs character varying(255),
    sland character varying(3),
    slart character varying(255),
    sltp1 character varying(255),
    sltp2 character varying(255)
);


--
-- TOC entry 201 (class 1259 OID 56022)
-- Name: it0023; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0023 (
    begda date NOT NULL,
    endda date NOT NULL,
    pernr bigint NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    sap_message character varying(255),
    sap_status character varying(1),
    ansvx character varying(20),
    arbgb character varying(255),
    attach1_path character varying(255),
    ort01 character varying(100),
    seqnr bigint,
    document_status character varying(255) NOT NULL,
    infty character varying(255),
    attach1_subty character varying(255),
    branc character varying(10),
    land1 character varying(3),
    taete bigint
);


--
-- TOC entry 202 (class 1259 OID 56030)
-- Name: it0040; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0040 (
    begda date NOT NULL,
    endda date NOT NULL,
    infty character varying(255) NOT NULL,
    pernr bigint NOT NULL,
    subty character varying(255) NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    anzkl double precision,
    itxex character varying(255),
    lobnr character varying(255),
    seqnr bigint,
    leihg character varying(255),
    zeinh character varying(255)
);


--
-- TOC entry 203 (class 1259 OID 56038)
-- Name: it0041; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0041 (
    begda date NOT NULL,
    endda date NOT NULL,
    pernr bigint NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    dat01 date,
    dat02 date,
    dat03 date,
    dat04 date,
    dat05 date,
    dat06 date,
    dat07 date,
    dat08 date,
    dat09 date,
    dat10 date,
    dat11 date,
    dat12 date,
    seqnr bigint,
    dar01 character varying(255),
    dar02 character varying(255),
    dar03 character varying(255),
    dar04 character varying(255),
    dar05 character varying(255),
    dar06 character varying(255),
    dar07 character varying(255),
    dar08 character varying(255),
    dar09 character varying(255),
    dar10 character varying(255),
    dar11 character varying(255),
    dar12 character varying(255)
);


--
-- TOC entry 204 (class 1259 OID 56046)
-- Name: it0077; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0077 (
    begda date NOT NULL,
    endda date NOT NULL,
    pernr bigint NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    seqnr bigint,
    molga character varying(5),
    racky character varying(5)
);


--
-- TOC entry 205 (class 1259 OID 56051)
-- Name: it0105; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0105 (
    begda date NOT NULL,
    endda date NOT NULL,
    infty character varying(255) NOT NULL,
    pernr bigint NOT NULL,
    subty character varying(255) NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    sap_message character varying(255),
    sap_status character varying(1),
    seqnr bigint,
    document_status character varying(255) NOT NULL,
    usrid_long character varying(255),
    comminfty character varying(255),
    usrty character varying(255)
);


--
-- TOC entry 206 (class 1259 OID 56059)
-- Name: it0184; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0184 (
    begda date NOT NULL,
    endda date NOT NULL,
    infty character varying(255) NOT NULL,
    pernr bigint NOT NULL,
    subty character varying(255) NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    sap_message character varying(255),
    sap_status character varying(1),
    itxex character varying(255),
    seqnr bigint,
    document_status character varying(255) NOT NULL,
    text1 character varying(255)
);


--
-- TOC entry 207 (class 1259 OID 56067)
-- Name: it0185; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0185 (
    begda date NOT NULL,
    endda date NOT NULL,
    infty character varying(255) NOT NULL,
    pernr bigint NOT NULL,
    subty character varying(255) NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    sap_message character varying(255),
    sap_status character varying(1),
    attach1_path character varying(255),
    auth1 character varying(30),
    expid date,
    fpdat date,
    icnum character varying(30),
    isspl character varying(30),
    seqnr bigint,
    document_status character varying(255) NOT NULL,
    attach1_subty character varying(255),
    ictyp character varying(5),
    iscot character varying(3)
);


--
-- TOC entry 208 (class 1259 OID 56075)
-- Name: it0241; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0241 (
    begda date NOT NULL,
    endda date NOT NULL,
    pernr bigint NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    attach1_path character varying(255),
    depnd character varying(2),
    marrd character varying(1),
    rdate date,
    refno character varying(255),
    seqnr bigint,
    spben character varying(1),
    document_status character varying(255) NOT NULL,
    taxid character varying(255) NOT NULL,
    infty character varying(255),
    attach1_subty character varying(255)
);


--
-- TOC entry 209 (class 1259 OID 56083)
-- Name: it0242; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it0242 (
    begda date NOT NULL,
    endda date NOT NULL,
    pernr bigint NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    attach1_path character varying(255),
    jamid character varying(255) NOT NULL,
    marst character varying(1),
    seqnr bigint,
    document_status character varying(255) NOT NULL,
    infty character varying(255),
    attach1_subty character varying(255)
);


--
-- TOC entry 210 (class 1259 OID 56091)
-- Name: it2001; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it2001 (
    begda date NOT NULL,
    endda date NOT NULL,
    infty character varying(255) NOT NULL,
    pernr bigint NOT NULL,
    subty character varying(255) NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    abwtg double precision,
    kaltg double precision,
    seqnr bigint,
    stdaz double precision,
    awart character varying(255)
);


--
-- TOC entry 211 (class 1259 OID 56099)
-- Name: it2002; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it2002 (
    begda date NOT NULL,
    endda date NOT NULL,
    infty character varying(255) NOT NULL,
    pernr bigint NOT NULL,
    subty character varying(255) NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    abwtg double precision,
    kaltg double precision,
    seqnr bigint,
    stdaz double precision,
    awart character varying(255)
);


--
-- TOC entry 212 (class 1259 OID 56107)
-- Name: it2006; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE it2006 (
    begda date NOT NULL,
    endda date NOT NULL,
    infty character varying(255) NOT NULL,
    pernr bigint NOT NULL,
    subty character varying(255) NOT NULL,
    aedtm timestamp without time zone,
    uname character varying(255),
    anzhl double precision,
    deend date,
    desta date,
    kverb double precision,
    seqnr bigint,
    ktart integer
);


--
-- TOC entry 213 (class 1259 OID 56115)
-- Name: m_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE m_jobs (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id character varying(255),
    updated_at timestamp without time zone NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    code character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    name character varying(255) NOT NULL
);


--
-- TOC entry 214 (class 1259 OID 56123)
-- Name: m_permission; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE m_permission (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id character varying(255),
    updated_at timestamp without time zone NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    permission_description character varying(255),
    permission_name character varying(255) NOT NULL,
    permission_object character varying(255) NOT NULL,
    is_system_permission boolean
);


--
-- TOC entry 215 (class 1259 OID 56131)
-- Name: m_position_relation_types; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE m_position_relation_types (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id character varying(255),
    updated_at timestamp without time zone NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    description character varying(255),
    name character varying(255) NOT NULL,
    system boolean
);


--
-- TOC entry 216 (class 1259 OID 56139)
-- Name: m_positions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE m_positions (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id character varying(255),
    updated_at timestamp without time zone NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    code character varying(255) NOT NULL,
    description character varying(255),
    name character varying(255) NOT NULL
);


--
-- TOC entry 217 (class 1259 OID 56147)
-- Name: m_role; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE m_role (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id character varying(255),
    updated_at timestamp without time zone NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    role_description character varying(255),
    role_name character varying(255) NOT NULL,
    is_system_role boolean
);


--
-- TOC entry 218 (class 1259 OID 56155)
-- Name: m_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE m_user (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id character varying(255),
    updated_at timestamp without time zone NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    user_auth_token character varying(255),
    user_auth_token_valid_thru timestamp without time zone,
    user_email character varying(255) NOT NULL,
    user_password character varying(255) NOT NULL,
    photo_link character varying(255),
    is_system_user boolean NOT NULL,
    user_name character varying(255) NOT NULL,
    user_valid_from timestamp without time zone,
    user_valid_thru timestamp without time zone,
    begda date,
    endda date,
    pernr bigint
);


--
-- TOC entry 219 (class 1259 OID 56163)
-- Name: m_workflows; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE m_workflows (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id character varying(255),
    updated_at timestamp without time zone NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    description character varying(255),
    name character varying(255) NOT NULL
);


--
-- TOC entry 220 (class 1259 OID 56171)
-- Name: permission_accesstypes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE permission_accesstypes (
    permission_id character varying(255) NOT NULL,
    access_permission_type_id integer
);


--
-- TOC entry 221 (class 1259 OID 56174)
-- Name: position_relations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE position_relations (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id character varying(255),
    updated_at timestamp without time zone NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    valid_from timestamp without time zone,
    valid_thru timestamp without time zone,
    from_position_id character varying(255),
    position_relation_type_id character varying(255),
    to_position_id character varying(255)
);


--
-- TOC entry 222 (class 1259 OID 56182)
-- Name: ptrv_head; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ptrv_head (
    hdvrs integer NOT NULL,
    pernr bigint NOT NULL,
    reinr bigint NOT NULL,
    abordnung bigint,
    dantn bigint,
    dat_reduc1 date,
    dat_reduc2 date,
    datb1 date,
    datb1_dienst date,
    dates date,
    dath1 date,
    datr1 date,
    datv1 date,
    datv1_dienst date,
    endrg date,
    exchange_date date,
    expenses character varying(255),
    fintn bigint,
    kunde character varying(255),
    repid character varying(255),
    request character varying(255),
    schem character varying(255),
    st_trgall date,
    st_trgtg date,
    times time without time zone,
    travel_plan character varying(255),
    uhrb1 time without time zone,
    uhrb1_dienst time without time zone,
    uhrh1 time without time zone,
    uhrr1 time without time zone,
    uhrv1 time without time zone,
    uname character varying(255),
    uhrv1_dienst time without time zone,
    zland character varying(255),
    zort1 character varying(255),
    molga character varying(2),
    morei character varying(2)
);


--
-- TOC entry 223 (class 1259 OID 56190)
-- Name: ptrv_perio; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ptrv_perio (
    mandt character varying(255) NOT NULL,
    pdvrs bigint NOT NULL,
    perio bigint NOT NULL,
    pernr bigint NOT NULL,
    reinr bigint NOT NULL,
    abkr1 character varying(255),
    abkr2 character varying(255),
    abrec character varying(255),
    abrj1 bigint,
    abrj2 bigint,
    abrp1 bigint,
    abrp2 bigint,
    accdt date,
    acctm time without time zone,
    antrg character varying(255),
    anuep bigint,
    begp1 date,
    begp2 date,
    endp1 date,
    endp2 date,
    hdvrs character varying(255),
    lstay character varying(255),
    no_miles character varying(255),
    pdatb date,
    pdatv date,
    perm1 character varying(255),
    perm2 character varying(255),
    puhrb time without time zone,
    puhrv time without time zone,
    runid bigint,
    tlock character varying(255),
    uebdt character varying(255),
    uebkz character varying(255),
    uebrf character varying(255),
    verpa character varying(255),
    waers character varying(255)
);


--
-- TOC entry 224 (class 1259 OID 56198)
-- Name: ptrv_srec; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ptrv_srec (
    mandt character varying(255) NOT NULL,
    perio integer NOT NULL,
    pernr bigint NOT NULL,
    receiptno character varying(255) NOT NULL,
    reinr bigint NOT NULL,
    exp_type character varying(255),
    loc_amount double precision,
    loc_curr character varying(255),
    paper_receipt character varying(255),
    rec_amount double precision,
    rec_curr character varying(255),
    rec_date date,
    rec_rate double precision,
    shorttxt character varying(255)
);


--
-- TOC entry 225 (class 1259 OID 56206)
-- Name: role_permissions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE role_permissions (
    m_role_id character varying(255) NOT NULL,
    m_permission_id character varying(255) NOT NULL
);


--
-- TOC entry 182 (class 1259 OID 50176)
-- Name: schema_version; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_version (
    version_rank integer NOT NULL,
    installed_rank integer NOT NULL,
    version character varying(50) NOT NULL,
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


--
-- TOC entry 226 (class 1259 OID 56214)
-- Name: sh_t706b1; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE sh_t706b1 (
    morei character varying(255) NOT NULL,
    spkzl character varying(255) NOT NULL,
    sptxt character varying(255) NOT NULL
);


--
-- TOC entry 227 (class 1259 OID 56222)
-- Name: t001; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t001 (
    bukrs character varying(10) NOT NULL,
    butxt character varying(255) NOT NULL
);


--
-- TOC entry 228 (class 1259 OID 56227)
-- Name: t002t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t002t (
    sprsl character varying(3) NOT NULL,
    langname character varying(255) NOT NULL
);


--
-- TOC entry 229 (class 1259 OID 56232)
-- Name: t005t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t005t (
    land1 character varying(3) NOT NULL,
    landx character varying(15) NOT NULL,
    natio character varying(15) NOT NULL
);


--
-- TOC entry 230 (class 1259 OID 56237)
-- Name: t005u; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t005u (
    bland character varying(3) NOT NULL,
    bezei character varying(20) NOT NULL
);


--
-- TOC entry 231 (class 1259 OID 56242)
-- Name: t016t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t016t (
    brsch character varying(10) NOT NULL,
    brtxt character varying(255) NOT NULL
);


--
-- TOC entry 232 (class 1259 OID 56247)
-- Name: t042z; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t042z (
    zlsch character varying(255) NOT NULL,
    text1 character varying(255) NOT NULL
);


--
-- TOC entry 233 (class 1259 OID 56255)
-- Name: t500l; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t500l (
    molga character varying(2) NOT NULL,
    ltext character varying(40) NOT NULL
);


--
-- TOC entry 234 (class 1259 OID 56260)
-- Name: t500p; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t500p (
    bukrs character varying(255) NOT NULL,
    persa character varying(10) NOT NULL,
    name2 character varying(255),
    pbtxt character varying(255) NOT NULL
);


--
-- TOC entry 235 (class 1259 OID 56268)
-- Name: t501t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t501t (
    persg character varying(10) NOT NULL,
    pgtxt character varying(255) NOT NULL
);


--
-- TOC entry 236 (class 1259 OID 56273)
-- Name: t502t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t502t (
    famst character varying(1) NOT NULL,
    ftext character varying(40) NOT NULL
);


--
-- TOC entry 237 (class 1259 OID 56278)
-- Name: t503k; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t503k (
    mandt character varying(10) NOT NULL,
    persk character varying(255) NOT NULL
);


--
-- TOC entry 238 (class 1259 OID 56283)
-- Name: t505s; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t505s (
    molga character varying(5) NOT NULL,
    racky character varying(5) NOT NULL,
    ltext character varying(255) NOT NULL
);


--
-- TOC entry 239 (class 1259 OID 56288)
-- Name: t512t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t512t (
    lgart character varying(255) NOT NULL,
    lgtxt character varying(255) NOT NULL
);


--
-- TOC entry 240 (class 1259 OID 56296)
-- Name: t513c; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t513c (
    taete bigint NOT NULL,
    ltext character varying(255) NOT NULL
);


--
-- TOC entry 241 (class 1259 OID 56301)
-- Name: t513s; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t513s (
    endda date NOT NULL,
    stell bigint NOT NULL,
    begda date,
    stltx character varying(100) NOT NULL
);


--
-- TOC entry 242 (class 1259 OID 56306)
-- Name: t516t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t516t (
    konfe character varying(2) NOT NULL,
    kitxt character varying(4) NOT NULL,
    ktext character varying(25) NOT NULL
);


--
-- TOC entry 243 (class 1259 OID 56311)
-- Name: t517t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t517t (
    slart character varying(255) NOT NULL,
    stext character varying(50) NOT NULL
);


--
-- TOC entry 244 (class 1259 OID 56316)
-- Name: t517x; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t517x (
    faart character varying(255) NOT NULL,
    ftext character varying(255) NOT NULL
);


--
-- TOC entry 245 (class 1259 OID 56324)
-- Name: t518b; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t518b (
    ausbi bigint NOT NULL,
    atext character varying(255) NOT NULL
);


--
-- TOC entry 246 (class 1259 OID 56329)
-- Name: t519t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t519t (
    slabs character varying(255) NOT NULL,
    stext character varying(255) NOT NULL
);


--
-- TOC entry 247 (class 1259 OID 56337)
-- Name: t522g; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t522g (
    anred character varying(1) NOT NULL,
    anrlt character varying(15) NOT NULL,
    atext character varying(5) NOT NULL,
    gesch character varying(1) NOT NULL
);


--
-- TOC entry 248 (class 1259 OID 56342)
-- Name: t527x; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t527x (
    endda date NOT NULL,
    orgeh bigint NOT NULL,
    begda date,
    orgtx character varying(255) NOT NULL
);


--
-- TOC entry 249 (class 1259 OID 56347)
-- Name: t528t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t528t (
    endda date NOT NULL,
    plans bigint NOT NULL,
    begda date,
    plstx character varying(150) NOT NULL
);


--
-- TOC entry 250 (class 1259 OID 56352)
-- Name: t531s; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t531s (
    tmart character varying(255) NOT NULL,
    tmtxt character varying(255) NOT NULL
);


--
-- TOC entry 251 (class 1259 OID 56360)
-- Name: t535n; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t535n (
    art character varying(1) NOT NULL,
    title character varying(15) NOT NULL,
    duevo character varying(1),
    ttout character varying(15) NOT NULL
);


--
-- TOC entry 252 (class 1259 OID 56365)
-- Name: t538t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t538t (
    zeinh character varying(255) NOT NULL,
    etext character varying(255) NOT NULL
);


--
-- TOC entry 253 (class 1259 OID 56373)
-- Name: t548t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t548t (
    datar character varying(255) NOT NULL,
    dtext character varying(255) NOT NULL
);


--
-- TOC entry 254 (class 1259 OID 56381)
-- Name: t549t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t549t (
    abkrs character varying(10) NOT NULL,
    abktx character varying(255) NOT NULL
);


--
-- TOC entry 255 (class 1259 OID 56386)
-- Name: t554t; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t554t (
    awart character varying(255) NOT NULL,
    atext character varying(255) NOT NULL
);


--
-- TOC entry 256 (class 1259 OID 56394)
-- Name: t556b; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t556b (
    ktart integer NOT NULL,
    ktext character varying(50) NOT NULL
);


--
-- TOC entry 257 (class 1259 OID 56399)
-- Name: t591s; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t591s (
    infty character varying(255) NOT NULL,
    subty character varying(255) NOT NULL,
    stext character varying(40) NOT NULL
);


--
-- TOC entry 258 (class 1259 OID 56407)
-- Name: t5r06; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t5r06 (
    ictyp character varying(5) NOT NULL,
    ictxt character varying(255) NOT NULL
);


--
-- TOC entry 259 (class 1259 OID 56412)
-- Name: t702n; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t702n (
    morei character varying(2) NOT NULL,
    text25 character varying(40) NOT NULL
);


--
-- TOC entry 260 (class 1259 OID 56417)
-- Name: t702o; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t702o (
    land1 character varying(255) NOT NULL,
    morei character varying(255) NOT NULL,
    text25 character varying(255) NOT NULL
);


--
-- TOC entry 261 (class 1259 OID 56425)
-- Name: t705h; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t705h (
    grawg character varying(255) NOT NULL,
    moabw bigint NOT NULL,
    pinco character varying(255) NOT NULL,
    spras character varying(255) NOT NULL,
    zeity character varying(255) NOT NULL,
    gtext character varying(255)
);


--
-- TOC entry 262 (class 1259 OID 56433)
-- Name: t705p; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t705p (
    pmbde character varying(255) NOT NULL,
    satza character varying(255) NOT NULL,
    ddtext character varying(255) NOT NULL
);


--
-- TOC entry 263 (class 1259 OID 56441)
-- Name: t706b1; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t706b1 (
    begda timestamp without time zone NOT NULL,
    endda timestamp without time zone NOT NULL,
    morei character varying(255) NOT NULL,
    spkzl character varying(255) NOT NULL
);


--
-- TOC entry 264 (class 1259 OID 56449)
-- Name: t706s; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE t706s (
    morei character varying(255) NOT NULL,
    schem character varying(255) NOT NULL,
    stext character varying(255) NOT NULL
);


--
-- TOC entry 265 (class 1259 OID 56457)
-- Name: teven; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE teven (
    pdsnr bigint NOT NULL,
    aedtm date,
    dallf character varying(255),
    erdat date,
    ertim character varying(255),
    exlga character varying(255),
    hrazl double precision,
    hrbet double precision,
    ldate date,
    ltime character varying(255),
    origf character varying(255),
    pdc_usrup character varying(255),
    pernr bigint NOT NULL,
    terid character varying(255),
    uname character varying(255),
    zausw bigint,
    zeinh character varying(255),
    abwgr character varying(255),
    satza character varying(3)
);


--
-- TOC entry 266 (class 1259 OID 56465)
-- Name: trx_approval_event_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE trx_approval_event_log (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    approval_action character varying(255) NOT NULL,
    entity_object_id character varying(255) NOT NULL,
    performed_by_ssn bigint NOT NULL,
    cfg_entity_id character varying(255) NOT NULL,
    performed_by_position_id character varying(255) NOT NULL
);


--
-- TOC entry 267 (class 1259 OID 56473)
-- Name: trx_event_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE trx_event_log (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    entity_object_id character varying(255) NOT NULL,
    event_data character varying(255) NOT NULL,
    operation_type character varying(255) NOT NULL,
    cfg_entity_id character varying(255) NOT NULL
);


--
-- TOC entry 268 (class 1259 OID 56481)
-- Name: user_roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_roles (
    m_user_id character varying(255) NOT NULL,
    m_role_id character varying(255) NOT NULL
);


--
-- TOC entry 269 (class 1259 OID 56489)
-- Name: v_001p_all; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE v_001p_all (
    btrtl character varying(10) NOT NULL,
    werks character varying(10) NOT NULL,
    btext character varying(255) NOT NULL,
    name1 character varying(255)
);


--
-- TOC entry 270 (class 1259 OID 56497)
-- Name: workflow_steps; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE workflow_steps (
    id character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by_id character varying(255) NOT NULL,
    deleted_at timestamp without time zone,
    deleted_by_id character varying(255),
    updated_at timestamp without time zone NOT NULL,
    updated_by_id character varying(255) NOT NULL,
    opt_lock bigint NOT NULL,
    approval_type character varying(255) NOT NULL,
    description character varying(255),
    seq integer NOT NULL,
    sequence_type character varying(255) NOT NULL,
    m_position_id character varying(255) NOT NULL,
    m_workflow_id character varying(255) NOT NULL
);


--
-- TOC entry 271 (class 1259 OID 56505)
-- Name: zmed_costype; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE zmed_costype (
    bukrs character varying(255) NOT NULL,
    datab date NOT NULL,
    datbi date NOT NULL,
    kodecos character varying(255) NOT NULL,
    kodemed character varying(255) NOT NULL,
    descr character varying(255)
);


--
-- TOC entry 272 (class 1259 OID 56513)
-- Name: zmed_empquota; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE zmed_empquota (
    bukrs character varying(255) NOT NULL,
    datab date NOT NULL,
    datbi date NOT NULL,
    fatxt character varying(255) NOT NULL,
    kodequo character varying(255) NOT NULL,
    persg character varying(255) NOT NULL,
    persk character varying(255) NOT NULL,
    exten smallint,
    quotamt double precision
);


--
-- TOC entry 273 (class 1259 OID 56521)
-- Name: zmed_empquotasm; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE zmed_empquotasm (
    bukrs character varying(255) NOT NULL,
    datab date NOT NULL,
    datbi date NOT NULL,
    fatxt character varying(255) NOT NULL,
    kodequo character varying(255) NOT NULL,
    pernr bigint NOT NULL,
    persg character varying(255) NOT NULL,
    persk character varying(255) NOT NULL,
    exten smallint,
    quotamt double precision
);


--
-- TOC entry 274 (class 1259 OID 56529)
-- Name: zmed_empquotastf; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE zmed_empquotastf (
    bukrs character varying(255) NOT NULL,
    datab date NOT NULL,
    datbi date NOT NULL,
    kodequo character varying(255) NOT NULL,
    persg character varying(255) NOT NULL,
    persk character varying(255) NOT NULL,
    stell bigint NOT NULL,
    quotamt double precision
);


--
-- TOC entry 275 (class 1259 OID 56537)
-- Name: zmed_header_post; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE zmed_header_post (
    obj_key character varying(255) NOT NULL,
    obj_sys character varying(255) NOT NULL,
    obj_type character varying(255) NOT NULL,
    ac_doc_no character varying(255),
    acc_principle character varying(255),
    bill_category character varying(255),
    bus_act character varying(255),
    compo_acc character varying(255),
    doc_date date,
    doc_type character varying(255),
    fis_period smallint,
    fisc_year smallint,
    header_txt character varying(255),
    logsys character varying(255),
    neg_posting character varying(255),
    obj_key_inv character varying(255),
    obj_key_r character varying(255),
    pstng_date date,
    reason_rev character varying(255),
    ref_doc_no character varying(255),
    ref_doc_no_long character varying(255),
    trans_date date,
    username character varying(255),
    vatdate date,
    compcode character varying(10) NOT NULL
);


--
-- TOC entry 276 (class 1259 OID 56545)
-- Name: zmed_medtype; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE zmed_medtype (
    bukrs character varying(255) NOT NULL,
    datab date NOT NULL,
    datbi date NOT NULL,
    kodemed character varying(255) NOT NULL,
    descr character varying(255)
);


--
-- TOC entry 277 (class 1259 OID 56553)
-- Name: zmed_quotaused; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE zmed_quotaused (
    bukrs character varying(255) NOT NULL,
    gjahr integer NOT NULL,
    kodequo character varying(255) NOT NULL,
    pernr bigint NOT NULL,
    quotamt double precision
);


--
-- TOC entry 278 (class 1259 OID 56561)
-- Name: zmed_quotype; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE zmed_quotype (
    bukrs character varying(255) NOT NULL,
    datab date NOT NULL,
    datbi date NOT NULL,
    kodequo character varying(255) NOT NULL,
    descr character varying(255),
    persen double precision
);


--
-- TOC entry 279 (class 1259 OID 56569)
-- Name: zmed_trxhdr; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE zmed_trxhdr (
    bukrs character varying(255) NOT NULL,
    zclmno character varying(255) NOT NULL,
    appamt double precision,
    aprby character varying(255),
    aprdt date,
    belnr character varying(255),
    billdt date,
    clmamt double precision,
    clmby character varying(255),
    clmdt date,
    clmst character varying(255),
    docno character varying(255),
    docno2 character varying(255),
    ename character varying(255),
    jamin character varying(255),
    kodemed character varying(255),
    last_sync timestamp without time zone,
    medtydesc character varying(255),
    notex character varying(255),
    orgtx character varying(255),
    pagu1 double precision,
    pagu2 double precision,
    pagu2byemp double precision,
    pagudent double precision,
    pasien character varying(255),
    payby character varying(255),
    paydt date,
    payment_date date,
    payment_status character varying(255),
    payment_status2 character varying(255),
    paymethod character varying(255),
    penyakit character varying(255),
    pernr bigint,
    process character varying(255),
    rejamt double precision,
    revby character varying(255),
    revdt date,
    tglkeluar date,
    tglmasuk date,
    totamt double precision,
    zdivision character varying(255)
);


--
-- TOC entry 280 (class 1259 OID 56577)
-- Name: zmed_trxitm; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE zmed_trxitm (
    clmno character varying(255) NOT NULL,
    itmno character varying(255) NOT NULL,
    appamt double precision,
    billdt date,
    clmamt double precision,
    kodecos character varying(255),
    kodemed character varying(255),
    pasien character varying(255),
    rejamt double precision,
    relat character varying(255),
    umur character varying(255),
    zcostyp character varying(255),
    bukrs character varying(10) NOT NULL
);


--
-- TOC entry 3221 (class 0 OID 55886)
-- Dependencies: 184
-- Data for Name: bnka; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3222 (class 0 OID 55894)
-- Dependencies: 185
-- Data for Name: cfg_approval_category; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3223 (class 0 OID 55902)
-- Dependencies: 186
-- Data for Name: cfg_attachment; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3224 (class 0 OID 55910)
-- Dependencies: 187
-- Data for Name: cfg_entity; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3225 (class 0 OID 55918)
-- Dependencies: 188
-- Data for Name: cfg_entity_approval; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3220 (class 0 OID 51521)
-- Dependencies: 183
-- Data for Name: cfg_region_types; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO cfg_region_types (id, opt_lock, ident_name, name, description, system, created_at, created_by_id, updated_at, updated_by_id, deleted_at, deleted_by_id) VALUES ('4028e48956d1d4350156d1d446260000', 0, 'COUNTRY', 'Country', NULL, false, '2016-08-28 22:46:04.966+07', 'superadmin', '2016-08-28 22:46:04.966+07', 'superadmin', NULL, NULL);
INSERT INTO cfg_region_types (id, opt_lock, ident_name, name, description, system, created_at, created_by_id, updated_at, updated_by_id, deleted_at, deleted_by_id) VALUES ('4028e48956d1d4350156d1d446540001', 0, 'PROVINCE', 'Province', NULL, false, '2016-08-28 22:46:05.012+07', 'superadmin', '2016-08-28 22:46:05.012+07', 'superadmin', NULL, NULL);
INSERT INTO cfg_region_types (id, opt_lock, ident_name, name, description, system, created_at, created_by_id, updated_at, updated_by_id, deleted_at, deleted_by_id) VALUES ('4028e48956d1d4350156d1d446670002', 0, 'DISTRICT', 'District', NULL, false, '2016-08-28 22:46:05.031+07', 'superadmin', '2016-08-28 22:46:05.031+07', 'superadmin', NULL, NULL);
INSERT INTO cfg_region_types (id, opt_lock, ident_name, name, description, system, created_at, created_by_id, updated_at, updated_by_id, deleted_at, deleted_by_id) VALUES ('4028e48956d1d4350156d1d446800003', 0, 'SUBDISTRICT', 'Subdistrict', NULL, false, '2016-08-28 22:46:05.056+07', 'superadmin', '2016-08-28 22:46:05.056+07', 'superadmin', NULL, NULL);
INSERT INTO cfg_region_types (id, opt_lock, ident_name, name, description, system, created_at, created_by_id, updated_at, updated_by_id, deleted_at, deleted_by_id) VALUES ('4028e48956d1d4350156d1d446880004', 0, 'VILLAGE', 'Village', NULL, false, '2016-08-28 22:46:05.064+07', 'superadmin', '2016-08-28 22:46:05.064+07', 'superadmin', NULL, NULL);


--
-- TOC entry 3226 (class 0 OID 55926)
-- Dependencies: 189
-- Data for Name: cfg_vehicle_types; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO cfg_vehicle_types (id, created_at, created_by_id, deleted_at, deleted_by_id, updated_at, updated_by_id, opt_lock, description, ident_name, name) VALUES ('4028e48956d1c7d30156d1c7e42c0000', '2016-08-28 22:32:33.452', 'superadmin', NULL, NULL, '2016-08-28 22:32:33.452', 'superadmin', 0, NULL, 'TRAIN', 'Train');
INSERT INTO cfg_vehicle_types (id, created_at, created_by_id, deleted_at, deleted_by_id, updated_at, updated_by_id, opt_lock, description, ident_name, name) VALUES ('4028e48956d1c7d30156d1c7e4680001', '2016-08-28 22:32:33.512', 'superadmin', NULL, NULL, '2016-08-28 22:32:33.512', 'superadmin', 0, NULL, 'AIRPLANE', 'Air Plane');
INSERT INTO cfg_vehicle_types (id, created_at, created_by_id, deleted_at, deleted_by_id, updated_at, updated_by_id, opt_lock, description, ident_name, name) VALUES ('4028e48956d1c7d30156d1c7e46e0002', '2016-08-28 22:32:33.518', 'superadmin', NULL, NULL, '2016-08-28 22:32:33.518', 'superadmin', 0, NULL, 'SHIP', 'Ship');
INSERT INTO cfg_vehicle_types (id, created_at, created_by_id, deleted_at, deleted_by_id, updated_at, updated_by_id, opt_lock, description, ident_name, name) VALUES ('4028e48956d1c7d30156d1c7e4740003', '2016-08-28 22:32:33.523', 'superadmin', NULL, NULL, '2016-08-28 22:32:33.523', 'superadmin', 0, NULL, 'BUS', 'Bus');
INSERT INTO cfg_vehicle_types (id, created_at, created_by_id, deleted_at, deleted_by_id, updated_at, updated_by_id, opt_lock, description, ident_name, name) VALUES ('4028e48956d1c7d30156d1c7e47b0004', '2016-08-28 22:32:33.531', 'superadmin', NULL, NULL, '2016-08-28 22:32:33.531', 'superadmin', 0, NULL, 'CAR_RENTAL', 'Car Rental');


--
-- TOC entry 3227 (class 0 OID 55934)
-- Dependencies: 190
-- Data for Name: cskt; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3228 (class 0 OID 55942)
-- Dependencies: 191
-- Data for Name: it0001; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3229 (class 0 OID 55950)
-- Dependencies: 192
-- Data for Name: it0001_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3230 (class 0 OID 55958)
-- Dependencies: 193
-- Data for Name: it0001_trx; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3231 (class 0 OID 55966)
-- Dependencies: 194
-- Data for Name: it0002; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3232 (class 0 OID 55974)
-- Dependencies: 195
-- Data for Name: it0006; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3233 (class 0 OID 55982)
-- Dependencies: 196
-- Data for Name: it0009; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3234 (class 0 OID 55990)
-- Dependencies: 197
-- Data for Name: it0014; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3235 (class 0 OID 55998)
-- Dependencies: 198
-- Data for Name: it0019; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3236 (class 0 OID 56006)
-- Dependencies: 199
-- Data for Name: it0021; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3237 (class 0 OID 56014)
-- Dependencies: 200
-- Data for Name: it0022; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3238 (class 0 OID 56022)
-- Dependencies: 201
-- Data for Name: it0023; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3239 (class 0 OID 56030)
-- Dependencies: 202
-- Data for Name: it0040; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3240 (class 0 OID 56038)
-- Dependencies: 203
-- Data for Name: it0041; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3241 (class 0 OID 56046)
-- Dependencies: 204
-- Data for Name: it0077; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3242 (class 0 OID 56051)
-- Dependencies: 205
-- Data for Name: it0105; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3243 (class 0 OID 56059)
-- Dependencies: 206
-- Data for Name: it0184; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3244 (class 0 OID 56067)
-- Dependencies: 207
-- Data for Name: it0185; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3245 (class 0 OID 56075)
-- Dependencies: 208
-- Data for Name: it0241; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3246 (class 0 OID 56083)
-- Dependencies: 209
-- Data for Name: it0242; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3247 (class 0 OID 56091)
-- Dependencies: 210
-- Data for Name: it2001; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3248 (class 0 OID 56099)
-- Dependencies: 211
-- Data for Name: it2002; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3249 (class 0 OID 56107)
-- Dependencies: 212
-- Data for Name: it2006; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3250 (class 0 OID 56115)
-- Dependencies: 213
-- Data for Name: m_jobs; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3251 (class 0 OID 56123)
-- Dependencies: 214
-- Data for Name: m_permission; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3252 (class 0 OID 56131)
-- Dependencies: 215
-- Data for Name: m_position_relation_types; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3253 (class 0 OID 56139)
-- Dependencies: 216
-- Data for Name: m_positions; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3254 (class 0 OID 56147)
-- Dependencies: 217
-- Data for Name: m_role; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3255 (class 0 OID 56155)
-- Dependencies: 218
-- Data for Name: m_user; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3256 (class 0 OID 56163)
-- Dependencies: 219
-- Data for Name: m_workflows; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3257 (class 0 OID 56171)
-- Dependencies: 220
-- Data for Name: permission_accesstypes; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3258 (class 0 OID 56174)
-- Dependencies: 221
-- Data for Name: position_relations; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3259 (class 0 OID 56182)
-- Dependencies: 222
-- Data for Name: ptrv_head; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3260 (class 0 OID 56190)
-- Dependencies: 223
-- Data for Name: ptrv_perio; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3261 (class 0 OID 56198)
-- Dependencies: 224
-- Data for Name: ptrv_srec; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3262 (class 0 OID 56206)
-- Dependencies: 225
-- Data for Name: role_permissions; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3219 (class 0 OID 50176)
-- Dependencies: 182
-- Data for Name: schema_version; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (1, 1, '10020151208001', 'Create table m permission', 'SQL', 'V10020151208001__Create_table_m_permission.sql', -377850373, 'hcms', '2016-06-27 18:32:14.367019', 18, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (2, 2, '10020151208002', 'Create table m role', 'SQL', 'V10020151208002__Create_table_m_role.sql', -845964572, 'hcms', '2016-06-27 18:32:14.428301', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (3, 3, '10020151208003', 'Create table m user', 'SQL', 'V10020151208003__Create_table_m_user.sql', -1730726312, 'hcms', '2016-06-27 18:32:14.455322', 15, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (4, 4, '10020151208004', 'Create table permission accesstypes', 'SQL', 'V10020151208004__Create_table_permission_accesstypes.sql', 1585768800, 'hcms', '2016-06-27 18:32:14.486182', 7, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (5, 5, '10020151208005', 'Create table role permissions', 'SQL', 'V10020151208005__Create_table_role_permissions.sql', 1009348164, 'hcms', '2016-06-27 18:32:14.510277', 11, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (6, 6, '10020151208006', 'Create table user roles', 'SQL', 'V10020151208006__Create_table_user_roles.sql', 66559782, 'hcms', '2016-06-27 18:32:14.542401', 10, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (7, 7, '100201512100001', 'Create table T591S address type', 'SQL', 'V100201512100001__Create_table_T591S_address_type.sql', 989567472, 'hcms', '2016-06-27 18:32:14.573696', 7, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (8, 8, '100201512100002', 'Create table T005T country', 'SQL', 'V100201512100002__Create_table_T005T_country.sql', -1267025279, 'hcms', '2016-06-27 18:32:14.60449', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (9, 9, '100201512100003', 'Create table T005U region', 'SQL', 'V100201512100003__Create_table_T005U_region.sql', 1964160917, 'hcms', '2016-06-27 18:32:14.628178', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (10, 10, '100201512100004', 'Create table T042Z payment method', 'SQL', 'V100201512100004__Create_table_T042Z_payment_method.sql', -1534825459, 'hcms', '2016-06-27 18:32:14.652858', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (11, 11, '100201512100005', 'Create table BNKA bank key', 'SQL', 'V100201512100005__Create_table_BNKA_bank_key.sql', 1201351889, 'hcms', '2016-06-27 18:32:14.675053', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (12, 12, '100201512100006', 'Create table T001 company', 'SQL', 'V100201512100006__Create_table_T001_company.sql', -1877975243, 'hcms', '2016-06-27 18:32:14.696955', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (13, 13, '100201512100007', 'Create table T500P personnel area', 'SQL', 'V100201512100007__Create_table_T500P_personnel_area.sql', -290087181, 'hcms', '2016-06-27 18:32:14.720747', 9, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (14, 14, '100201512100008', 'Create table T501 employee.group', 'SQL', 'V100201512100008__Create_table_T501_employee.group.sql', -2086749033, 'hcms', '2016-06-27 18:32:14.74572', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (15, 15, '100201512100009', 'Create table T503K employee subgroup', 'SQL', 'V100201512100009__Create_table_T503K_employee_subgroup.sql', 1712057330, 'hcms', '2016-06-27 18:32:14.764483', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (16, 16, '100201512100010', 'Create table V 001P ALL personnel subarea', 'SQL', 'V100201512100010__Create_table_V_001P_ALL_personnel_subarea.sql', -798053900, 'hcms', '2016-06-27 18:32:14.778631', 9, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (17, 17, '100201512100011', 'Create table T549T payroll area', 'SQL', 'V100201512100011__Create_table_T549T_payroll_area.sql', 776469826, 'hcms', '2016-06-27 18:32:14.804329', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (18, 18, '100201512100012', 'Create table T527X organizational unit', 'SQL', 'V100201512100012__Create_table_T527X_organizational_unit.sql', -1253462123, 'hcms', '2016-06-27 18:32:14.8314', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (19, 19, '100201512100013', 'Create table CSKT cost center', 'SQL', 'V100201512100013__Create_table_CSKT_cost_center.sql', -1639424485, 'hcms', '2016-06-27 18:32:14.852661', 11, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (20, 20, '100201512100014', 'Create table T528T position', 'SQL', 'V100201512100014__Create_table_T528T_position.sql', 581639052, 'hcms', '2016-06-27 18:32:14.88285', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (21, 21, '100201512100015', 'Create table T513S job', 'SQL', 'V100201512100015__Create_table_T513S_job.sql', 2074013961, 'hcms', '2016-06-27 18:32:14.902747', 7, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (22, 22, '100201512100016', 'Create table IT0001 personal main data', 'SQL', 'V100201512100016__Create_table_IT0001_personal_main_data.sql', -1808949240, 'hcms', '2016-06-27 18:32:14.931509', 24, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (23, 23, '100201512100017', 'Alter table cskt cost center altertimestamptype', 'SQL', 'V100201512100017__Alter_table_cskt_cost_center_altertimestamptype.sql', 1011815420, 'hcms', '2016-06-27 18:32:14.974531', 9, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (24, 24, '100201512100018', 'Alter table T513S job altertimestamptype', 'SQL', 'V100201512100018__Alter_table_T513S_job_altertimestamptype.sql', -2099454935, 'hcms', '2016-06-27 18:32:15.006377', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (25, 25, '100201512110001', 'Alter table T501 employee group renameto T501T', 'SQL', 'V100201512110001__Alter_table_T501_employee_group_renameto_T501T.sql', -424494349, 'hcms', '2016-06-27 18:32:15.03449', 1, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (26, 26, '100201512110002', 'Alter table IT0001 personal main data changetypeint', 'SQL', 'V100201512110002__Alter_table_IT0001_personal_main_data_changetypeint.sql', -1992970487, 'hcms', '2016-06-27 18:32:15.049955', 26, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (27, 27, '100201512110003', 'Create table IT0006 address', 'SQL', 'V100201512110003__Create_table_IT0006_address.sql', -519650316, 'hcms', '2016-06-27 18:32:15.091137', 20, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (28, 28, '100201512140001', 'Alter table IT0006 address setdefaultchangedon', 'SQL', 'V100201512140001__Alter_table_IT0006_address_setdefaultchangedon.sql', -2102200163, 'hcms', '2016-06-27 18:32:15.12614', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (29, 29, '100201512140002', 'Create table IT0009 bank details', 'SQL', 'V100201512140002__Create_table_IT0009_bank_details.sql', 1203988912, 'hcms', '2016-06-27 18:32:15.149513', 21, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (30, 30, '100201512150001', 'Alter table m user changeuseridlength', 'SQL', 'V100201512150001__Alter_table_m_user_changeuseridlength.sql', -2130434290, 'hcms', '2016-06-27 18:32:15.187621', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (31, 31, '100201512170001', 'Alter table IT0001 changeforeignkeybukrs', 'SQL', 'V100201512170001__Alter_table_IT0001_changeforeignkeybukrs.sql', 1123427277, 'hcms', '2016-06-27 18:32:15.212183', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (32, 32, '100201512170002', 'Alter table IT0001 changeforeignkeybukrs', 'SQL', 'V100201512170002__Alter_table_IT0001_changeforeignkeybukrs.sql', 809778564, 'hcms', '2016-06-27 18:32:15.229059', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (33, 33, '100201512170003', 'Alter table IT0001 addforeignkeywerksbtrtl', 'SQL', 'V100201512170003__Alter_table_IT0001_addforeignkeywerksbtrtl.sql', 350457989, 'hcms', '2016-06-27 18:32:15.245035', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (34, 34, '100201512170004', 'Alter table CSKT dropcolumndatbi', 'SQL', 'V100201512170004__Alter_table_CSKT_dropcolumndatbi.sql', -1070499034, 'hcms', '2016-06-27 18:32:15.266103', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (35, 35, '100201512170005', 'Alter table CSKT addprimarykey', 'SQL', 'V100201512170005__Alter_table_CSKT_addprimarykey.sql', 2134705956, 'hcms', '2016-06-27 18:32:15.285267', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (36, 36, '100201512170006', 'Alter table IT0001 addreferencestocskt', 'SQL', 'V100201512170006__Alter_table_IT0001_addreferencestocskt.sql', 1560826441, 'hcms', '2016-06-27 18:32:15.297486', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (37, 37, '100201512170007', 'Alter table IT0001 changecolorgehtypetonum', 'SQL', 'V100201512170007__Alter_table_IT0001_changecolorgehtypetonum.sql', -565875069, 'hcms', '2016-06-27 18:32:15.30969', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (38, 38, '100201512170008', 'Alter table IT0001 addreferencestoT527X', 'SQL', 'V100201512170008__Alter_table_IT0001_addreferencestoT527X.sql', 511581804, 'hcms', '2016-06-27 18:32:15.327553', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (39, 39, '100201512230001', 'Create table T502T marritalstatus', 'SQL', 'V100201512230001__Create_table_T502T_marritalstatus.sql', 64608877, 'hcms', '2016-06-27 18:32:15.365838', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (40, 40, '100201512230002', 'Create table T516T religion', 'SQL', 'V100201512230002__Create_table_T516T_religion.sql', 310268555, 'hcms', '2016-06-27 18:32:15.380853', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (41, 41, '100201512230003', 'Create table T522G gender', 'SQL', 'V100201512230003__Create_table_T522G_gender.sql', 925850148, 'hcms', '2016-06-27 18:32:15.398561', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (42, 42, '100201512280001', 'Create table T535N title', 'SQL', 'V100201512280001__Create_table_T535N_title.sql', 1025136037, 'hcms', '2016-06-27 18:32:15.422458', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (43, 43, '100201512280002', 'Alter table T522G gender changeprimarykey', 'SQL', 'V100201512280002__Alter_table_T522G_gender_changeprimarykey.sql', -1251254298, 'hcms', '2016-06-27 18:32:15.438071', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (44, 44, '100201512280003', 'Create table T002T language', 'SQL', 'V100201512280003__Create_table_T002T_language.sql', -149359233, 'hcms', '2016-06-27 18:32:15.454948', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (45, 45, '100201512280004', 'Alter table T002T language altersprsllength', 'SQL', 'V100201512280004__Alter_table_T002T_language_altersprsllength.sql', 326912103, 'hcms', '2016-06-27 18:32:15.467856', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (46, 46, '100201512280005', 'Create table IT0002 personnelinformation', 'SQL', 'V100201512280005__Create_table_IT0002_personnelinformation.sql', -1764444979, 'hcms', '2016-06-27 18:32:15.488876', 17, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (47, 47, '100201512290001', 'Alter table User addemployeerefs', 'SQL', 'V100201512290001__Alter_table_User_addemployeerefs.sql', -66985866, 'hcms', '2016-06-27 18:32:15.518162', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (48, 48, '100201512290002', 'Alter table User addemployeefk', 'SQL', 'V100201512290002__Alter_table_User_addemployeefk.sql', -1773656466, 'hcms', '2016-06-27 18:32:15.532767', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (49, 49, '100201512290003', 'Alter table user addphotolinkcolumn', 'SQL', 'V100201512290003__Alter_table_user_addphotolinkcolumn.sql', 949966416, 'hcms', '2016-06-27 18:32:15.554465', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (50, 50, '100201512290004', 'Drop table IT0001 personal main data', 'SQL', 'V100201512290004__Drop_table_IT0001_personal_main_data.sql', -571935420, 'hcms', '2016-06-27 18:32:15.565834', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (51, 51, '100201512290005', 'Create table IT0001 personal main data', 'SQL', 'V100201512290005__Create_table_IT0001_personal_main_data.sql', -2121119419, 'hcms', '2016-06-27 18:32:15.580848', 33, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (52, 52, '100201512290006', 'Alter table IT0001 personalmaindata dropfk', 'SQL', 'V100201512290006__Alter_table_IT0001_personalmaindata_dropfk.sql', -1827591332, 'hcms', '2016-06-27 18:32:15.626246', 7, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (53, 53, '100201512290007', 'Alter table IT0001 personalmaindata switchcostcentercolumns', 'SQL', 'V100201512290007__Alter_table_IT0001_personalmaindata_switchcostcentercolumns.sql', -760254668, 'hcms', '2016-06-27 18:32:15.643101', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (54, 54, '100201512290008', 'Alter table t513s jobkey alterdatecolumntype', 'SQL', 'V100201512290008__Alter_table_t513s_jobkey_alterdatecolumntype.sql', 706835650, 'hcms', '2016-06-27 18:32:15.65912', 13, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (55, 55, '100201512290009', 'Alter table it0001 personalmaindata addfk', 'SQL', 'V100201512290009__Alter_table_it0001_personalmaindata_addfk.sql', 1488503759, 'hcms', '2016-06-27 18:32:15.686621', 15, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (56, 56, '100201512290010', 'Alter table IT0002 personalmaindata adddescription', 'SQL', 'V100201512290010__Alter_table_IT0002_personalmaindata_adddescription.sql', 840731912, 'hcms', '2016-06-27 18:32:15.713124', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (57, 57, '100201512290011', 'Alter table m user changepernrreftoit0001', 'SQL', 'V100201512290011__Alter_table_m_user_changepernrreftoit0001.sql', 1496053505, 'hcms', '2016-06-27 18:32:15.730527', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (58, 58, '100201512300001', 'Alter table IT0009 bank dropfkaddrtype', 'SQL', 'V100201512300001__Alter_table_IT0009_bank_dropfkaddrtype.sql', 1607042113, 'hcms', '2016-06-27 18:32:15.749166', 1, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (59, 59, '100201512300002', 'Alter table IT0006 address dropfkaddrtype', 'SQL', 'V100201512300002__Alter_table_IT0006_address_dropfkaddrtype.sql', -506858474, 'hcms', '2016-06-27 18:32:15.760982', 1, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (60, 60, '100201512300003', 'Alter table T591S droppk', 'SQL', 'V100201512300003__Alter_table_T591S_droppk.sql', 1934324992, 'hcms', '2016-06-27 18:32:15.772904', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (61, 61, '100201512300004', 'Alter table T591S addpk', 'SQL', 'V100201512300004__Alter_table_T591S_addpk.sql', -1910759774, 'hcms', '2016-06-27 18:32:15.788495', 1, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (62, 62, '100201512300005', 'Alter table IT0006 dropinftycol', 'SQL', 'V100201512300005__Alter_table_IT0006_dropinftycol.sql', -528567851, 'hcms', '2016-06-27 18:32:15.801378', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (63, 63, '100201512300006', 'Alter table IT0006 addreftoT591s', 'SQL', 'V100201512300006__Alter_table_IT0006_addreftoT591s.sql', 1355199942, 'hcms', '2016-06-27 18:32:15.820392', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (64, 64, '100201512300007', 'Alter table IT0006 dropstatefkey', 'SQL', 'V100201512300007__Alter_table_IT0006_dropstatefkey.sql', -2106109223, 'hcms', '2016-06-27 18:32:15.833986', 1, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (65, 65, '100201512300008', 'Alter table IT0006 dropland1fkey', 'SQL', 'V100201512300008__Alter_table_IT0006_dropland1fkey.sql', -1342329827, 'hcms', '2016-06-27 18:32:15.846079', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (66, 66, '100201512300009', 'Alter table IT0006 addstatefk', 'SQL', 'V100201512300009__Alter_table_IT0006_addstatefk.sql', -1746022166, 'hcms', '2016-06-27 18:32:15.860035', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (67, 67, '100201512300010', 'Alter table IT0006 addland1fk', 'SQL', 'V100201512300010__Alter_table_IT0006_addland1fk.sql', -1006940715, 'hcms', '2016-06-27 18:32:15.876281', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (68, 68, '100201601080001', 'Alter table IT0006 alterenddabegdatypetodate', 'SQL', 'V100201601080001__Alter_table_IT0006_alterenddabegdatypetodate.sql', -1006715067, 'hcms', '2016-06-27 18:32:15.891476', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (69, 69, '100201601110001', 'Alter table IT0006 changeunamelength', 'SQL', 'V100201601110001__Alter_table_IT0006_changeunamelength.sql', 1472535857, 'hcms', '2016-06-27 18:32:15.911588', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (70, 70, '100201601120001', 'Alter table IT0009 dropinfotype', 'SQL', 'V100201601120001__Alter_table_IT0009_dropinfotype.sql', -1402257669, 'hcms', '2016-06-27 18:32:15.932137', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (71, 71, '100201601120002', 'Alter table IT0009 expandbanknlength', 'SQL', 'V100201601120002__Alter_table_IT0009_expandbanknlength.sql', -1045733935, 'hcms', '2016-06-27 18:32:15.952352', 1, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (72, 72, '100201601120003', 'Alter table IT0009 dropfkbnka', 'SQL', 'V100201601120003__Alter_table_IT0009_dropfkbnka.sql', -380229998, 'hcms', '2016-06-27 18:32:15.966959', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (73, 73, '100201601120004', 'Alter table IT0009 dropfkt042z', 'SQL', 'V100201601120004__Alter_table_IT0009_dropfkt042z.sql', -988522963, 'hcms', '2016-06-27 18:32:15.98729', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (74, 74, '100201601120005', 'Alter table IT0009 dropfkt005t', 'SQL', 'V100201601120005__Alter_table_IT0009_dropfkt005t.sql', 1288504597, 'hcms', '2016-06-27 18:32:16.00369', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (75, 75, '100201601120006', 'Alter table IT0009 addfkbnka', 'SQL', 'V100201601120006__Alter_table_IT0009_addfkbnka.sql', 99852626, 'hcms', '2016-06-27 18:32:16.021967', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (76, 76, '100201601120007', 'Alter table IT0009 addfkt005t', 'SQL', 'V100201601120007__Alter_table_IT0009_addfkt005t.sql', 1286557711, 'hcms', '2016-06-27 18:32:16.03693', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (77, 77, '100201601120008', 'Alter table IT0009 addfkt042z', 'SQL', 'V100201601120008__Alter_table_IT0009_addfkt042z.sql', 999002422, 'hcms', '2016-06-27 18:32:16.054996', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (78, 78, '100201601120009', 'Alter table IT0009 addfkt591s', 'SQL', 'V100201601120009__Alter_table_IT0009_addfkt591s.sql', -616771130, 'hcms', '2016-06-27 18:32:16.071696', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (79, 79, '100201601180001', 'Alter table IT0009 changeenddabegdatodate', 'SQL', 'V100201601180001__Alter_table_IT0009_changeenddabegdatodate.sql', 1479272528, 'hcms', '2016-06-27 18:32:16.091691', 7, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (80, 80, '100201601180002', 'Alter table IT0009 changeunamelength', 'SQL', 'V100201601180002__Alter_table_IT0009_changeunamelength.sql', 893278842, 'hcms', '2016-06-27 18:32:16.109921', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (81, 81, '100201601180003', 'Create table T5R06', 'SQL', 'V100201601180003__Create_table_T5R06.sql', 1091314871, 'hcms', '2016-06-27 18:32:16.131386', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (82, 82, '100201601180004', 'Create table IT0185', 'SQL', 'V100201601180004__Create_table_IT0185.sql', -1924558557, 'hcms', '2016-06-27 18:32:16.157396', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (83, 83, '100201601190001', 'Alter table IT0185 addictypfk', 'SQL', 'V100201601190001__Alter_table_IT0185_addictypfk.sql', -1747614482, 'hcms', '2016-06-27 18:32:16.186109', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (84, 84, '100201601190002', 'Alter table IT0185 addiscotfk', 'SQL', 'V100201601190002__Alter_table_IT0185_addiscotfk.sql', -1102530877, 'hcms', '2016-06-27 18:32:16.203976', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (85, 85, '100201601190003', 'Create table IT0021', 'SQL', 'V100201601190003__Create_table_IT0021.sql', 910074951, 'hcms', '2016-06-27 18:32:16.224513', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (86, 86, '100201601190004', 'Alter table IT0021 addfk', 'SQL', 'V100201601190004__Alter_table_IT0021_addfk.sql', -1644050256, 'hcms', '2016-06-27 18:32:16.242348', 11, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (87, 87, '100201601210001', 'Create table T517T', 'SQL', 'V100201601210001__Create_table_T517T.sql', 1828645256, 'hcms', '2016-06-27 18:32:16.266898', 9, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (88, 88, '100201601210002', 'Create table T518B', 'SQL', 'V100201601210002__Create_table_T518B.sql', 775818349, 'hcms', '2016-06-27 18:32:16.28943', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (89, 89, '100201601210003', 'Create table T519T', 'SQL', 'V100201601210003__Create_table_T519T.sql', 2006131208, 'hcms', '2016-06-27 18:32:16.305753', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (90, 90, '100201601210004', 'Create table T538T', 'SQL', 'V100201601210004__Create_table_T538T.sql', -1657135672, 'hcms', '2016-06-27 18:32:16.326961', 7, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (91, 91, '100201601210005', 'Create table T517X', 'SQL', 'V100201601210005__Create_table_T517X.sql', -1507033547, 'hcms', '2016-06-27 18:32:16.34928', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (92, 92, '100201601210006', 'Create table IT0022', 'SQL', 'V100201601210006__Create_table_IT0022.sql', -420878993, 'hcms', '2016-06-27 18:32:16.365612', 9, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (93, 93, '100201601210007', 'Alter table IT0022 addconstraints', 'SQL', 'V100201601210007__Alter_table_IT0022_addconstraints.sql', 638001478, 'hcms', '2016-06-27 18:32:16.38897', 21, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (94, 94, '100201601220001', 'Alter table IT0022 addsubtyfk', 'SQL', 'V100201601220001__Alter_table_IT0022_addsubtyfk.sql', -841496732, 'hcms', '2016-06-27 18:32:16.422078', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (95, 95, '100201601260001', 'Create table T016T', 'SQL', 'V100201601260001__Create_table_T016T.sql', 1485075205, 'hcms', '2016-06-27 18:32:16.435427', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (96, 96, '100201601260002', 'Create table T513C', 'SQL', 'V100201601260002__Create_table_T513C.sql', -1727378414, 'hcms', '2016-06-27 18:32:16.460149', 9, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (97, 97, '100201601260003', 'Create table IT0023', 'SQL', 'V100201601260003__Create_table_IT0023.sql', -1171714912, 'hcms', '2016-06-27 18:32:16.485579', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (98, 98, '100201601260004', 'Alter table IT0023 addpkfk', 'SQL', 'V100201601260004__Alter_table_IT0023_addpkfk.sql', 1171329295, 'hcms', '2016-06-27 18:32:16.503765', 12, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (99, 99, '100201601270001', 'Create table T556B', 'SQL', 'V100201601270001__Create_table_T556B.sql', -1833226922, 'hcms', '2016-06-27 18:32:16.532173', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (100, 100, '100201601270002', 'Create table IT2006', 'SQL', 'V100201601270002__Create_table_IT2006.sql', 291056874, 'hcms', '2016-06-27 18:32:16.556863', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (101, 101, '100201601270003', 'Alter table T556B removesmopgkandmozko', 'SQL', 'V100201601270003__Alter_table_T556B_removesmopgkandmozko.sql', 391227721, 'hcms', '2016-06-27 18:32:16.574327', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (102, 102, '100201601270004', 'Alter table IT2006 dropcolumnmopgkandmozko', 'SQL', 'V100201601270004__Alter_table_IT2006_dropcolumnmopgkandmozko.sql', -982963224, 'hcms', '2016-06-27 18:32:16.596084', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (103, 103, '100201601270005', 'Alter table IT2006 addfk', 'SQL', 'V100201601270005__Alter_table_IT2006_addfk.sql', -290276940, 'hcms', '2016-06-27 18:32:16.618203', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (104, 104, '100201601280001', 'Alter table IT0002 changeunamelength', 'SQL', 'V100201601280001__Alter_table_IT0002_changeunamelength.sql', -1733120986, 'hcms', '2016-06-27 18:32:16.633606', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (105, 105, '100201602010001', 'Alter table T591S addcolumninfotype', 'SQL', 'V100201602010001__Alter_table_T591S_addcolumninfotype.sql', 516570889, 'hcms', '2016-06-27 18:32:16.648328', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (106, 106, '100201602010002', 'Alter table T591S removespk', 'SQL', 'V100201602010002__Alter_table_T591S_removespk.sql', -391095952, 'hcms', '2016-06-27 18:32:16.667763', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (107, 107, '100201602030001', 'Alter table IT0006 addinftycolumn', 'SQL', 'V100201602030001__Alter_table_IT0006_addinftycolumn.sql', 777465827, 'hcms', '2016-06-27 18:32:16.691764', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (108, 108, '100201602030002', 'Alter table IT0006 addpk', 'SQL', 'V100201602030002__Alter_table_IT0006_addpk.sql', 1425605178, 'hcms', '2016-06-27 18:32:16.708906', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (109, 109, '100201602030003', 'Alter table IT0009 addinftycolumn', 'SQL', 'V100201602030003__Alter_table_IT0009_addinftycolumn.sql', 315557850, 'hcms', '2016-06-27 18:32:16.728672', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (110, 110, '100201602030004', 'Alter table IT0009 addpk', 'SQL', 'V100201602030004__Alter_table_IT0009_addpk.sql', 831101282, 'hcms', '2016-06-27 18:32:16.746912', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (111, 111, '100201602030005', 'Alter table IT0185 addinftycolumn', 'SQL', 'V100201602030005__Alter_table_IT0185_addinftycolumn.sql', 1974380021, 'hcms', '2016-06-27 18:32:16.764671', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (112, 112, '100201602030006', 'Alter table IT0185 addpk', 'SQL', 'V100201602030006__Alter_table_IT0185_addpk.sql', -1392311771, 'hcms', '2016-06-27 18:32:16.781949', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (113, 113, '100201602030007', 'Alter table IT0021 addinftycolumn', 'SQL', 'V100201602030007__Alter_table_IT0021_addinftycolumn.sql', -804242149, 'hcms', '2016-06-27 18:32:16.800637', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (114, 114, '100201602030008', 'Alter table IT0021 addpk', 'SQL', 'V100201602030008__Alter_table_IT0021_addpk.sql', 1400419071, 'hcms', '2016-06-27 18:32:16.825013', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (115, 115, '100201602030009', 'Alter table IT0022 addinftycolumn', 'SQL', 'V100201602030009__Alter_table_IT0022_addinftycolumn.sql', 123586876, 'hcms', '2016-06-27 18:32:16.840051', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (116, 116, '100201602030010', 'Alter table IT0022 addpk', 'SQL', 'V100201602030010__Alter_table_IT0022_addpk.sql', -1761778835, 'hcms', '2016-06-27 18:32:16.857719', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (117, 117, '100201602030011', 'Alter table IT2006 addinftycolumn', 'SQL', 'V100201602030011__Alter_table_IT2006_addinftycolumn.sql', 1467833895, 'hcms', '2016-06-27 18:32:16.884517', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (118, 118, '100201602030012', 'Alter table IT2006 addpk', 'SQL', 'V100201602030012__Alter_table_IT2006_addpk.sql', -113096458, 'hcms', '2016-06-27 18:32:16.9018', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (119, 119, '100201602030013', 'Create table IT0040', 'SQL', 'V100201602030013__Create_table_IT0040.sql', 1615643961, 'hcms', '2016-06-27 18:32:16.917102', 7, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (120, 120, '100201602030014', 'Alter table T591S addpk', 'SQL', 'V100201602030014__Alter_table_T591S_addpk.sql', -1267477534, 'hcms', '2016-06-27 18:32:16.942685', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (121, 121, '100201602030015', 'Alter table IT0040 addpkandfk', 'SQL', 'V100201602030015__Alter_table_IT0040_addpkandfk.sql', 872728225, 'hcms', '2016-06-27 18:32:16.961453', 9, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (122, 122, '100201602030016', 'Alter table IT0021 addfk', 'SQL', 'V100201602030016__Alter_table_IT0021_addfk.sql', -117701164, 'hcms', '2016-06-27 18:32:16.989344', 7, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (123, 123, '100201602030017', 'Alter table IT0006 addfk', 'SQL', 'V100201602030017__Alter_table_IT0006_addfk.sql', -1256228915, 'hcms', '2016-06-27 18:32:17.016608', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (124, 124, '100201602030018', 'Alter table IT0009 addfk', 'SQL', 'V100201602030018__Alter_table_IT0009_addfk.sql', -19869237, 'hcms', '2016-06-27 18:32:17.034516', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (125, 125, '100201602030019', 'Alter table IT0185 addfk', 'SQL', 'V100201602030019__Alter_table_IT0185_addfk.sql', 389164797, 'hcms', '2016-06-27 18:32:17.054096', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (126, 126, '100201602030020', 'Alter table IT0022 addfk', 'SQL', 'V100201602030020__Alter_table_IT0022_addfk.sql', -1894932165, 'hcms', '2016-06-27 18:32:17.088769', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (127, 127, '100201602030021', 'Alter table IT2006 addfk', 'SQL', 'V100201602030021__Alter_table_IT2006_addfk.sql', 1417146515, 'hcms', '2016-06-27 18:32:17.110301', 7, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (128, 128, '100201602040001', 'Create table T505S', 'SQL', 'V100201602040001__Create_table_T505S.sql', 4289934, 'hcms', '2016-06-27 18:32:17.159659', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (129, 129, '100201602040002', 'Create table IT0077', 'SQL', 'V100201602040002__Create_table_IT0077.sql', -379531968, 'hcms', '2016-06-27 18:32:17.196278', 9, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (130, 130, '100201602040003', 'Alter table IT0077 addconstraints', 'SQL', 'V100201602040003__Alter_table_IT0077_addconstraints.sql', 1810837925, 'hcms', '2016-06-27 18:32:17.22709', 9, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (131, 131, '100201602040004', 'Create table IT0105', 'SQL', 'V100201602040004__Create_table_IT0105.sql', 1895331050, 'hcms', '2016-06-27 18:32:17.253675', 9, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (132, 132, '100201602040005', 'Alter table IT0105 addpkandfks', 'SQL', 'V100201602040005__Alter_table_IT0105_addpkandfks.sql', -1151011033, 'hcms', '2016-06-27 18:32:17.279491', 18, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (133, 133, '100201602050001', 'Alter table IT0105 changeusrtyfk', 'SQL', 'V100201602050001__Alter_table_IT0105_changeusrtyfk.sql', -1214435692, 'hcms', '2016-06-27 18:32:17.314091', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (134, 134, '100201602100001', 'Alter table IT0002 addcolumnnatio', 'SQL', 'V100201602100001__Alter_table_IT0002_addcolumnnatio.sql', -665468606, 'hcms', '2016-06-27 18:32:17.335259', 1, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (135, 135, '100201602100002', 'Alter table IT0002 adddescriptionandfkonnatio', 'SQL', 'V100201602100002__Alter_table_IT0002_adddescriptionandfkonnatio.sql', 1673732753, 'hcms', '2016-06-27 18:32:17.351449', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (136, 136, '100201602100003', 'Create table IT0241', 'SQL', 'V100201602100003__Create_table_IT0241.sql', 300739221, 'hcms', '2016-06-27 18:32:17.3744', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (137, 137, '100201602100004', 'Alter table IT0241 addpk', 'SQL', 'V100201602100004__Alter_table_IT0241_addpk.sql', 1462097778, 'hcms', '2016-06-27 18:32:17.388964', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (138, 138, '100201602110001', 'Create table T512T', 'SQL', 'V100201602110001__Create_table_T512T.sql', -2073288172, 'hcms', '2016-06-27 18:32:17.404579', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (139, 139, '100201602110002', 'Create table IT0014', 'SQL', 'V100201602110002__Create_table_IT0014.sql', -96032878, 'hcms', '2016-06-27 18:32:17.422938', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (140, 140, '100201602110003', 'Alter table IT0014 addpkandfk', 'SQL', 'V100201602110003__Alter_table_IT0014_addpkandfk.sql', 1562836817, 'hcms', '2016-06-27 18:32:17.446645', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (141, 141, '100201602120001', 'Create table IT0184', 'SQL', 'V100201602120001__Create_table_IT0184.sql', -626403474, 'hcms', '2016-06-27 18:32:17.463089', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (142, 142, '100201602120002', 'Alter table IT0184 addpkandfk', 'SQL', 'V100201602120002__Alter_table_IT0184_addpkandfk.sql', -1173209228, 'hcms', '2016-06-27 18:32:17.483984', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (143, 143, '100201602120003', 'Alter table IT0184 removesduplicatecolumn', 'SQL', 'V100201602120003__Alter_table_IT0184_removesduplicatecolumn.sql', -177112774, 'hcms', '2016-06-27 18:32:17.514981', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (144, 144, '100201602120004', 'Alter table IT0184 addinftyaspk', 'SQL', 'V100201602120004__Alter_table_IT0184_addinftyaspk.sql', 259196353, 'hcms', '2016-06-27 18:32:17.536187', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (145, 145, '100201602120005', 'Alter table IT0184 removepftypcol', 'SQL', 'V100201602120005__Alter_table_IT0184_removepftypcol.sql', -223697315, 'hcms', '2016-06-27 18:32:17.551757', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (146, 146, '100201602130001', 'Alter table IT0184 resizeitxexcolumn', 'SQL', 'V100201602130001__Alter_table_IT0184_resizeitxexcolumn.sql', -1814584659, 'hcms', '2016-06-27 18:32:17.57753', 1, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (147, 147, '100201602140001', 'Alter table IT0014 addinfty', 'SQL', 'V100201602140001__Alter_table_IT0014_addinfty.sql', 155729298, 'hcms', '2016-06-27 18:32:17.590807', 1, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (148, 148, '100201602140002', 'Alter table IT0014 manipulateinfty', 'SQL', 'V100201602140002__Alter_table_IT0014_manipulateinfty.sql', 425376594, 'hcms', '2016-06-27 18:32:17.606853', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (149, 149, '100201602140003', 'Alter table IT0014 addinftysubtypefk', 'SQL', 'V100201602140003__Alter_table_IT0014_addinftysubtypefk.sql', -449515588, 'hcms', '2016-06-27 18:32:17.625851', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (150, 150, '100201602150001', 'Create table T554T', 'SQL', 'V100201602150001__Create_table_T554T.sql', 2020321009, 'hcms', '2016-06-27 18:32:17.644218', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (151, 151, '100201602150002', 'Create table IT2001', 'SQL', 'V100201602150002__Create_table_IT2001.sql', 1868327052, 'hcms', '2016-06-27 18:32:17.66149', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (152, 152, '100201602150003', 'Alter table IT2001 addinftycolumn', 'SQL', 'V100201602150003__Alter_table_IT2001_addinftycolumn.sql', -1706426331, 'hcms', '2016-06-27 18:32:17.685147', 1, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (153, 153, '100201602150004', 'Alter table IT2001 addpkandfk', 'SQL', 'V100201602150004__Alter_table_IT2001_addpkandfk.sql', -1399835463, 'hcms', '2016-06-27 18:32:17.703384', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (154, 154, '100201602160001', 'Create table IT2002', 'SQL', 'V100201602160001__Create_table_IT2002.sql', 56443418, 'hcms', '2016-06-27 18:32:17.719926', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (155, 155, '100201602160002', 'Alter table IT2002 addinfty', 'SQL', 'V100201602160002__Alter_table_IT2002_addinfty.sql', -1569617066, 'hcms', '2016-06-27 18:32:17.738225', 1, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (156, 156, '100201602160003', 'Alter table IT2002 addpkfk', 'SQL', 'V100201602160003__Alter_table_IT2002_addpkfk.sql', -953487053, 'hcms', '2016-06-27 18:32:17.749987', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (157, 157, '100201602160004', 'Create table T531S', 'SQL', 'V100201602160004__Create_table_T531S.sql', 248547222, 'hcms', '2016-06-27 18:32:17.773774', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (158, 158, '100201602160005', 'Create table IT0019', 'SQL', 'V100201602160005__Create_table_IT0019.sql', -334277811, 'hcms', '2016-06-27 18:32:17.787622', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (159, 159, '100201602160006', 'Alter table IT0019 addpkfk', 'SQL', 'V100201602160006__Alter_table_IT0019_addpkfk.sql', -1568749610, 'hcms', '2016-06-27 18:32:17.808072', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (160, 160, '100201602170001', 'Create table T548T', 'SQL', 'V100201602170001__Create_table_T548T.sql', 2098874737, 'hcms', '2016-06-27 18:32:17.831647', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (161, 161, '100201602170002', 'Create table IT0041', 'SQL', 'V100201602170002__Create_table_IT0041.sql', 1258839517, 'hcms', '2016-06-27 18:32:17.847482', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (162, 162, '100201602170003', 'Alter table IT0041 addpkfk', 'SQL', 'V100201602170003__Alter_table_IT0041_addpkfk.sql', -860087365, 'hcms', '2016-06-27 18:32:17.86261', 25, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (163, 163, '100201602240001', 'Create table ZMED QUOTYPE', 'SQL', 'V100201602240001__Create_table_ZMED_QUOTYPE.sql', -148113016, 'hcms', '2016-06-27 18:32:17.90155', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (164, 164, '100201602240002', 'Create table ZMED QUOTAUSED', 'SQL', 'V100201602240002__Create_table_ZMED_QUOTAUSED.sql', 1446695060, 'hcms', '2016-06-27 18:32:17.916827', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (165, 165, '100201602240003', 'Create table ZMED MEDTYPE', 'SQL', 'V100201602240003__Create_table_ZMED_MEDTYPE.sql', 1608815516, 'hcms', '2016-06-27 18:32:17.93221', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (166, 166, '100201602240004', 'Create table ZMED EMPQUOTASTF', 'SQL', 'V100201602240004__Create_table_ZMED_EMPQUOTASTF.sql', 999806299, 'hcms', '2016-06-27 18:32:17.947406', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (167, 167, '100201602240005', 'Create table ZMED EMPQUOTASM', 'SQL', 'V100201602240005__Create_table_ZMED_EMPQUOTASM.sql', -573932162, 'hcms', '2016-06-27 18:32:17.970398', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (168, 168, '100201602240006', 'Create table ZMED EMPQUOTA', 'SQL', 'V100201602240006__Create_table_ZMED_EMPQUOTA.sql', 742292841, 'hcms', '2016-06-27 18:32:17.985483', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (169, 169, '100201602240007', 'Create table ZMED COSTYPE', 'SQL', 'V100201602240007__Create_table_ZMED_COSTYPE.sql', -2089142577, 'hcms', '2016-06-27 18:32:18.005166', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (170, 170, '100201602240008', 'Create table ZMED HEADER POST', 'SQL', 'V100201602240008__Create_table_ZMED_HEADER_POST.sql', 1308256724, 'hcms', '2016-06-27 18:32:18.022394', 9, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (171, 171, '100201603010001', 'Create table ZMED TRXHDR', 'SQL', 'V100201603010001__Create_table_ZMED_TRXHDR.sql', -2023147083, 'hcms', '2016-06-27 18:32:18.047706', 10, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (172, 172, '100201603010002', 'Create table ZMED TRXITM', 'SQL', 'V100201603010002__Create_table_ZMED_TRXITM.sql', 1204134722, 'hcms', '2016-06-27 18:32:18.071985', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (173, 173, '100201604150001', 'Alter table m user setdefaultid', 'SQL', 'V100201604150001__Alter_table_m_user_setdefaultid.sql', -483782932, 'hcms', '2016-06-27 18:32:18.089379', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (174, 174, '100201604160001', 'Alter table m role adddefaultid', 'SQL', 'V100201604160001__Alter_table_m_role_adddefaultid.sql', -833391448, 'hcms', '2016-06-27 18:32:18.108772', 1, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (175, 175, '100201604200001', 'Create table m position relation types', 'SQL', 'V100201604200001__Create_table_m_position_relation_types.sql', -1644060894, 'hcms', '2016-06-27 18:32:18.122437', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (176, 176, '100201604200002', 'Create table position relations', 'SQL', 'V100201604200002__Create_table_position_relations.sql', 24499962, 'hcms', '2016-06-27 18:32:18.137109', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (177, 177, '100201604210001', 'Alter table m position relation types addconstraints', 'SQL', 'V100201604210001__Alter_table_m_position_relation_types_addconstraints.sql', -1254333947, 'hcms', '2016-06-27 18:32:18.155249', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (178, 178, '100201604210002', 'Create table m jobs', 'SQL', 'V100201604210002__Create_table_m_jobs.sql', -1277794602, 'hcms', '2016-06-27 18:32:18.177146', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (179, 179, '100201604210003', 'Create table m positions', 'SQL', 'V100201604210003__Create_table_m_positions.sql', 413799840, 'hcms', '2016-06-27 18:32:18.193661', 7, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (180, 180, '100201604210004', 'Alter table position relations addpkfk', 'SQL', 'V100201604210004__Alter_table_position_relations_addpkfk.sql', -2065014074, 'hcms', '2016-06-27 18:32:18.212046', 22, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (181, 181, '100201604210005', 'Alter table it0001 addcolumnjobidpositionid', 'SQL', 'V100201604210005__Alter_table_it0001_addcolumnjobidpositionid.sql', 2033277318, 'hcms', '2016-06-27 18:32:18.248109', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (182, 182, '100201604210006', 'Alter table it0001 addfkjobposition', 'SQL', 'V100201604210006__Alter_table_it0001_addfkjobposition.sql', -42533802, 'hcms', '2016-06-27 18:32:18.262159', 7, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (183, 183, '200201604240001', 'Create table IT0001 TRX', 'SQL', 'V200201604240001__Create_table_IT0001_TRX.sql', -1920792884, 'hcms', '2016-06-27 18:32:18.283635', 13, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (184, 184, '200201604240002', 'Alter table IT0001 TRX addfk', 'SQL', 'V200201604240002__Alter_table_IT0001_TRX_addfk.sql', -1920939186, 'hcms', '2016-06-27 18:32:18.309917', 17, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (185, 185, '200201604240003', 'Create table IT0001 LOG', 'SQL', 'V200201604240003__Create_table_IT0001_LOG.sql', 1943172414, 'hcms', '2016-06-27 18:32:18.339304', 10, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (186, 186, '200201604240004', 'Alter table IT0001 LOG addfk', 'SQL', 'V200201604240004__Alter_table_IT0001_LOG_addfk.sql', -550476504, 'hcms', '2016-06-27 18:32:18.363104', 18, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (187, 187, '200201605030001', 'Create table m workflows', 'SQL', 'V200201605030001__Create_table_m_workflows.sql', 245972515, 'hcms', '2016-06-27 18:32:18.399127', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (188, 188, '200201605030002', 'Alter table m workflows addpk', 'SQL', 'V200201605030002__Alter_table_m_workflows_addpk.sql', 1993483993, 'hcms', '2016-06-27 18:32:18.415714', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (189, 189, '200201605030003', 'Create table workflow steps', 'SQL', 'V200201605030003__Create_table_workflow_steps.sql', -235045635, 'hcms', '2016-06-27 18:32:18.432222', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (190, 190, '200201605030004', 'Alter table workflow steps addconstraints', 'SQL', 'V200201605030004__Alter_table_workflow_steps_addconstraints.sql', -249646125, 'hcms', '2016-06-27 18:32:18.45217', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (191, 191, '200201605030005', 'Alter table m workflows adduniqueindexforindex', 'SQL', 'V200201605030005__Alter_table_m_workflows_adduniqueindexforindex.sql', 1454834304, 'hcms', '2016-06-27 18:32:18.46903', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (192, 192, '200201605030006', 'Alter table workflow steps adduniqueconstraints', 'SQL', 'V200201605030006__Alter_table_workflow_steps_adduniqueconstraints.sql', 1083361603, 'hcms', '2016-06-27 18:32:18.485695', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (193, 193, '200201605130001', 'Alter table workflow steps addapprtypesandseqtypes', 'SQL', 'V200201605130001__Alter_table_workflow_steps_addapprtypesandseqtypes.sql', -590084917, 'hcms', '2016-06-27 18:32:18.505825', 13, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (194, 194, '200201605130002', 'Alter table workflow steps modifyapprtypeandseqtype', 'SQL', 'V200201605130002__Alter_table_workflow_steps_modifyapprtypeandseqtype.sql', 1647679699, 'hcms', '2016-06-27 18:32:18.532572', 19, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (195, 195, '200201605190001', 'Create table cfg approval category', 'SQL', 'V200201605190001__Create_table_cfg_approval_category.sql', 233328689, 'hcms', '2016-06-27 18:32:18.570934', 10, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (196, 196, '200201605190002', 'Create table cfg entity', 'SQL', 'V200201605190002__Create_table_cfg_entity.sql', -274210065, 'hcms', '2016-06-27 18:32:18.597187', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (197, 197, '200201605190003', 'Create table trx event log', 'SQL', 'V200201605190003__Create_table_trx_event_log.sql', 463368910, 'hcms', '2016-06-27 18:32:18.617671', 15, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (198, 198, '200201605190004', 'Create table trx approval event log', 'SQL', 'V200201605190004__Create_table_trx_approval_event_log.sql', -1014511130, 'hcms', '2016-06-27 18:32:18.648621', 7, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (199, 199, '200201605190005', 'Alter table cfg approval category addunique', 'SQL', 'V200201605190005__Alter_table_cfg_approval_category_addunique.sql', -798301891, 'hcms', '2016-06-27 18:32:18.668539', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (200, 200, '200201605190006', 'Alter table cfg entity addunique', 'SQL', 'V200201605190006__Alter_table_cfg_entity_addunique.sql', -398829007, 'hcms', '2016-06-27 18:32:18.687319', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (201, 201, '200201605190007', 'Alter table cfg entity setdefaultid', 'SQL', 'V200201605190007__Alter_table_cfg_entity_setdefaultid.sql', -1254673545, 'hcms', '2016-06-27 18:32:18.705412', 1, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (202, 202, '200201605190008', 'Alter table IT0009 adddocumentstatus', 'SQL', 'V200201605190008__Alter_table_IT0009_adddocumentstatus.sql', 180902897, 'hcms', '2016-06-27 18:32:18.720703', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (203, 203, '200201605190009', 'Alter table IT0006 adddocumentstatus', 'SQL', 'V200201605190009__Alter_table_IT0006_adddocumentstatus.sql', 1136798728, 'hcms', '2016-06-27 18:32:18.738451', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (204, 204, '200201605190010', 'Alter table IT0021 adddocumentstatus', 'SQL', 'V200201605190010__Alter_table_IT0021_adddocumentstatus.sql', -519742132, 'hcms', '2016-06-27 18:32:18.756153', 14, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (205, 205, '200201605190011', 'Alter table IT0022 adddocumentstatus', 'SQL', 'V200201605190011__Alter_table_IT0022_adddocumentstatus.sql', 1274833434, 'hcms', '2016-06-27 18:32:18.784411', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (206, 206, '200201605190012', 'Alter table IT0023 adddocumentstatus', 'SQL', 'V200201605190012__Alter_table_IT0023_adddocumentstatus.sql', -1686403817, 'hcms', '2016-06-27 18:32:18.802', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (207, 207, '200201605190013', 'Alter table IT0040 adddocumentstatus', 'SQL', 'V200201605190013__Alter_table_IT0040_adddocumentstatus.sql', -273174548, 'hcms', '2016-06-27 18:32:18.827155', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (208, 208, '200201605190014', 'Alter table cfg entity dropcolumnfkapprvcategory', 'SQL', 'V200201605190014__Alter_table_cfg_entity_dropcolumnfkapprvcategory.sql', -1329029615, 'hcms', '2016-06-27 18:32:18.850235', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (209, 209, '200201605190015', 'Alter table cfg entity adduniqueconstraint', 'SQL', 'V200201605190015__Alter_table_cfg_entity_adduniqueconstraint.sql', 917637703, 'hcms', '2016-06-27 18:32:18.863584', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (210, 210, '200201605190016', 'Create table cfg entity approval', 'SQL', 'V200201605190016__Create_table_cfg_entity_approval.sql', 516738037, 'hcms', '2016-06-27 18:32:18.88434', 18, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (211, 211, '200201605190017', 'Alter table cfg entity addcolumnoptlock', 'SQL', 'V200201605190017__Alter_table_cfg_entity_addcolumnoptlock.sql', 60404807, 'hcms', '2016-06-27 18:32:18.913193', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (212, 212, '200201605190018', 'Alter table cfg entity approval addcolumnoptlock', 'SQL', 'V200201605190018__Alter_table_cfg_entity_approval_addcolumnoptlock.sql', -1934622813, 'hcms', '2016-06-27 18:32:18.935511', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (213, 213, '200201605190019', 'Alter table trx event log addcolumnoptlock', 'SQL', 'V200201605190019__Alter_table_trx_event_log_addcolumnoptlock.sql', -811331395, 'hcms', '2016-06-27 18:32:18.957588', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (214, 214, '200201605190020', 'Alter table trx approval event log addcolumnoptlock', 'SQL', 'V200201605190020__Alter_table_trx_approval_event_log_addcolumnoptlock.sql', -1861267119, 'hcms', '2016-06-27 18:32:18.985137', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (215, 215, '200201605190021', 'Alter table trx approval event log dropfktrxeventlog', 'SQL', 'V200201605190021__Alter_table_trx_approval_event_log_dropfktrxeventlog.sql', 1921141786, 'hcms', '2016-06-27 18:32:19.002106', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (216, 216, '200201605190022', 'Alter table trx approval event log addentitymetadatacolumns', 'SQL', 'V200201605190022__Alter_table_trx_approval_event_log_addentitymetadatacolumns.sql', -136711822, 'hcms', '2016-06-27 18:32:19.02011', 5, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (217, 217, '200201605200001', 'Alter table trx approval event log adddefaultid', 'SQL', 'V200201605200001__Alter_table_trx_approval_event_log_adddefaultid.sql', -584931639, 'hcms', '2016-06-27 18:32:19.045515', 1, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (218, 218, '200201605200002', 'Alter table cfg entity approval addcoloperationtype', 'SQL', 'V200201605200002__Alter_table_cfg_entity_approval_addcoloperationtype.sql', -1612219686, 'hcms', '2016-06-27 18:32:19.057488', 11, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (219, 219, '200201605200003', 'Alter table cfg entity approval changeuniqueconstraint', 'SQL', 'V200201605200003__Alter_table_cfg_entity_approval_changeuniqueconstraint.sql', 646163156, 'hcms', '2016-06-27 18:32:19.081372', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (220, 220, '200201605270001', 'Alter table cfg approval category addoptlock', 'SQL', 'V200201605270001__Alter_table_cfg_approval_category_addoptlock.sql', -793400724, 'hcms', '2016-06-27 18:32:19.095861', 7, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (221, 221, '200201606030001', 'Alter table trx event log adddefaultuuid', 'SQL', 'V200201606030001__Alter_table_trx_event_log_adddefaultuuid.sql', 572839942, 'hcms', '2016-06-27 18:32:19.121361', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (222, 222, '200201606030002', 'Alter table trx event log alteroperationtype', 'SQL', 'V200201606030002__Alter_table_trx_event_log_alteroperationtype.sql', 1514822095, 'hcms', '2016-06-27 18:32:19.138133', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (223, 223, '200201606100001', 'Alter table it0184-it0185-it0241 add document status', 'SQL', 'V200201606100001__Alter_table_it0184-it0185-it0241_add_document_status.sql', -1490194568, 'hcms', '2016-06-27 18:32:19.153257', 24, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (225, 225, '200201606100003', 'Alter table it0009 add attach1 fields', 'SQL', 'V200201606100003__Alter_table_it0009_add_attach1_fields.sql', -1243161055, 'hcms', '2016-06-27 18:32:19.240986', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (226, 226, '200201606100004', 'Alter table it0021 add attach1 fields', 'SQL', 'V200201606100004__Alter_table_it0021_add_attach1_fields.sql', 783186376, 'hcms', '2016-06-27 18:32:19.261243', 12, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (227, 227, '200201606100005', 'Create table IT0242 BPJS', 'SQL', 'V200201606100005__Create_table_IT0242_BPJS.sql', -1821523740, 'hcms', '2016-06-27 18:32:19.28787', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (228, 228, '200201606130001', 'Alter table it0002-it0105 add document status', 'SQL', 'V200201606130001__Alter_table_it0002-it0105_add_document_status.sql', -90250938, 'hcms', '2016-06-27 18:32:19.312662', 13, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (230, 230, '200201606140001', 'Alter table Medical zmed tables', 'SQL', 'V200201606140001__Alter_table_Medical_zmed_tables.sql', -1894635284, 'hcms', '2016-06-27 18:32:19.361932', 30, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (232, 232, '200201606240001', 'Alter table IT0009 alterdocumentstatusenum', 'SQL', 'V200201606240001__Alter_table_IT0009_alterdocumentstatusenum.sql', 242993188, 'hcms', '2016-06-27 18:32:19.459552', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (233, 233, '200201606270001', 'Alter table IT0009 modifydocumentstatus', 'SQL', 'V200201606270001__Alter_table_IT0009_modifydocumentstatus.sql', 614168687, 'hcms', '2016-06-27 18:32:19.479773', 3, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (234, 234, '200201606270002', 'Alter table trx event log modifyoperationtypeenum', 'SQL', 'V200201606270002__Alter_table_trx_event_log_modifyoperationtypeenum.sql', -673094615, 'hcms', '2016-06-27 18:32:19.499561', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (235, 235, '200201606270003', 'Alter table trx approval event log addapprovalaction', 'SQL', 'V200201606270003__Alter_table_trx_approval_event_log_addapprovalaction.sql', -1411632689, 'hcms', '2016-06-27 18:32:19.518432', 7, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (236, 236, '200201606270004', 'Alter table trx approval event log renamecolumns', 'SQL', 'V200201606270004__Alter_table_trx_approval_event_log_renamecolumns.sql', -345306398, 'hcms', '2016-06-27 18:32:19.54266', 1, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (237, 237, '200201606270005', 'Alter table IT0002 modifydocumentstatusenum', 'SQL', 'V200201606270005__Alter_table_IT0002_modifydocumentstatusenum.sql', -1764949669, 'hcms', '2016-06-27 23:48:33.86469', 32, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (238, 238, '200201606270006', 'Alter table IT0006 modifydocumentstatusenum', 'SQL', 'V200201606270006__Alter_table_IT0006_modifydocumentstatusenum.sql', -1785793314, 'hcms', '2016-06-27 23:52:17.653833', 682, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (239, 239, '200201606270007', 'Alter table IT0021 modifydocumentstatusenum', 'SQL', 'V200201606270007__Alter_table_IT0021_modifydocumentstatusenum.sql', 1815822010, 'hcms', '2016-06-27 23:54:26.545351', 27, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (240, 240, '200201606270008', 'Alter table IT0022 modifydocumentstatusenum', 'SQL', 'V200201606270008__Alter_table_IT0022_modifydocumentstatusenum.sql', 892873491, 'hcms', '2016-06-27 23:56:16.348697', 16, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (241, 241, '200201606270009', 'Alter table IT0023 modifydocumentstatusenum', 'SQL', 'V200201606270009__Alter_table_IT0023_modifydocumentstatusenum.sql', -1259742645, 'hcms', '2016-06-27 23:58:07.256747', 16, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (242, 242, '200201606270010', 'Alter table IT0040 modifydocumentstatusenum', 'SQL', 'V200201606270010__Alter_table_IT0040_modifydocumentstatusenum.sql', 1748891921, 'hcms', '2016-06-28 00:00:03.542816', 36, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (243, 243, '200201606270011', 'Alter table IT0105 modifydocumentstatusenum', 'SQL', 'V200201606270011__Alter_table_IT0105_modifydocumentstatusenum.sql', 499273558, 'hcms', '2016-06-28 00:02:42.520209', 705, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (244, 244, '200201606270012', 'Alter table IT0184 modifydocumentstatusenum', 'SQL', 'V200201606270012__Alter_table_IT0184_modifydocumentstatusenum.sql', 58323764, 'hcms', '2016-06-28 00:07:52.578856', 17, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (245, 245, '200201606270013', 'Alter table IT0185 modifydocumentstatusenum', 'SQL', 'V200201606270013__Alter_table_IT0185_modifydocumentstatusenum.sql', -2102918548, 'hcms', '2016-06-28 00:09:52.196434', 21, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (246, 246, '200201606270014', 'Alter table IT0241 modifydocumentstatusenum', 'SQL', 'V200201606270014__Alter_table_IT0241_modifydocumentstatusenum.sql', 716905504, 'hcms', '2016-06-28 00:11:56.560422', 22, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (247, 247, '200201606270015', 'Alter table IT0242 modifydocumentstatusenum', 'SQL', 'V200201606270015__Alter_table_IT0242_modifydocumentstatusenum.sql', 1941440905, 'hcms', '2016-06-28 00:13:37.833176', 17, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (248, 248, '200201607080001', 'Alter table workflow steps dropcolumnnamenchangeuniquecons', 'SQL', 'V200201607080001__Alter_table_workflow_steps_dropcolumnnamenchangeuniquecons.sql', 321541391, 'hcms', '2016-07-08 23:33:56.799781', 59, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (249, 249, '200201607090001', 'Alter table zmedtrxhrd addprocess', 'SQL', 'V200201607090001__Alter_table_zmedtrxhrd_addprocess.sql', -1322162532, 'hcms', '2016-07-26 07:12:04.91157', 26, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (250, 250, '200201607170001', 'Alter table it0022 add attach1 fields', 'SQL', 'V200201607170001__Alter_table_it0022_add_attach1_fields.sql', -1515622067, 'hcms', '2016-07-26 07:12:04.973538', 33, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (251, 251, '200201607170002', 'Alter table it0023 add attach1 fields', 'SQL', 'V200201607170002__Alter_table_it0023_add_attach1_fields.sql', -50074499, 'hcms', '2016-07-26 07:12:05.04794', 21, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (252, 252, '200201607170003', 'Alter table it0185 add attach1 fields', 'SQL', 'V200201607170003__Alter_table_it0185_add_attach1_fields.sql', -2086500107, 'hcms', '2016-07-26 07:12:05.10051', 15, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (253, 253, '200201607170004', 'Alter table it0241 add attach1 fields', 'SQL', 'V200201607170004__Alter_table_it0241_add_attach1_fields.sql', 1767367693, 'hcms', '2016-07-26 07:12:05.142354', 29, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (254, 254, '200201607170005', 'Alter table it0242 add attach1 fields', 'SQL', 'V200201607170005__Alter_table_it0242_add_attach1_fields.sql', 2139260648, 'hcms', '2016-07-26 07:12:05.200364', 23, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (224, 224, '200201606100002', 'Create table cfg attachment', 'SQL', 'V200201606100002__Create_table_cfg_attachment.sql', -1095060197, 'hcms', '2016-06-27 18:32:19.19216', 14, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (229, 229, '200201606130002', 'Development Insert table cfg entity MUST REMOVED', 'SQL', 'V200201606130002__Development_Insert_table_cfg_entity_MUST_REMOVED.sql', 976011710, 'hcms', '2016-06-27 18:32:19.341715', 6, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (231, 231, '200201606200001', 'Create table EmployeeCost prtv tables', 'SQL', 'V200201606200001__Create_table_EmployeeCost_prtv_tables.sql', -1833228256, 'hcms', '2016-06-27 18:32:19.411429', 33, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (255, 255, '200201607170006', 'Alter table it0002 add attach1 fields', 'SQL', 'V200201607170006__Alter_table_it0002_add_attach1_fields.sql', 1645921875, 'hcms', '2016-08-27 12:35:26.501359', 58, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (256, 256, '200201607170007', 'Alter table it0006 add attach1 fields', 'SQL', 'V200201607170007__Alter_table_it0006_add_attach1_fields.sql', 903039571, 'hcms', '2016-08-27 12:36:14.765519', 188, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (257, 257, '200201607170008', 'Alter table it0019 add attach1 fields', 'SQL', 'V200201607170008__Alter_table_it0019_add_attach1_fields.sql', 792056449, 'hcms', '2016-08-27 12:36:15.022054', 24, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (258, 258, '200201607240001', 'Alter table zmedtrxitm primarykey', 'SQL', 'V200201607240001__Alter_table_zmedtrxitm_primarykey.sql', 2027243863, 'hcms', '2016-08-27 12:36:15.076362', 14, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (259, 259, '200201608140001', 'Alter table it0002 add sapstatus', 'SQL', 'V200201608140001__Alter_table_it0002_add_sapstatus.sql', -741814814, 'hcms', '2016-08-27 12:36:15.11963', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (260, 260, '200201608140002', 'Alter table it0006 add sapstatus', 'SQL', 'V200201608140002__Alter_table_it0006_add_sapstatus.sql', 1331907836, 'hcms', '2016-08-27 12:36:15.154766', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (261, 261, '200201608140003', 'Alter table it0009 add sapstatus', 'SQL', 'V200201608140003__Alter_table_it0009_add_sapstatus.sql', -1255576969, 'hcms', '2016-08-27 12:36:15.189464', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (262, 262, '200201608140004', 'Alter table it0021 add sapstatus', 'SQL', 'V200201608140004__Alter_table_it0021_add_sapstatus.sql', 709272955, 'hcms', '2016-08-27 12:36:15.220776', 8, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (263, 263, '200201608140005', 'Alter table it0022 add sapstatus', 'SQL', 'V200201608140005__Alter_table_it0022_add_sapstatus.sql', -1367275885, 'hcms', '2016-08-27 12:36:15.26027', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (264, 264, '200201608140006', 'Alter table it0023 add sapstatus', 'SQL', 'V200201608140006__Alter_table_it0023_add_sapstatus.sql', 1678992884, 'hcms', '2016-08-27 12:36:15.29019', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (265, 265, '200201608140007', 'Alter table it0105 add sapstatus', 'SQL', 'V200201608140007__Alter_table_it0105_add_sapstatus.sql', 899934212, 'hcms', '2016-08-27 12:36:15.324833', 4, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (266, 266, '200201608140008', 'Alter table it0184 add sapstatus', 'SQL', 'V200201608140008__Alter_table_it0184_add_sapstatus.sql', -781737754, 'hcms', '2016-08-27 12:36:15.351894', 7, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (267, 267, '200201608140009', 'Alter table it0185 add sapstatus', 'SQL', 'V200201608140009__Alter_table_it0185_add_sapstatus.sql', 469091201, 'hcms', '2016-08-27 12:36:15.381884', 2, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (268, 268, '200201608140010', 'Alter table zmedtrxhdr adddocstatus', 'SQL', 'V200201608140010__Alter_table_zmedtrxhdr_adddocstatus.sql', -489556779, 'hcms', '2016-08-27 12:36:15.405976', 35, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (269, 269, '200201608210001', 'Alter table it0021 add infty2', 'SQL', 'V200201608210001__Alter_table_it0021_add_infty2.sql', 1194701421, 'hcms', '2016-08-27 12:36:15.464639', 19, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (270, 270, '200201608240001', 'Create table cfg region types', 'SQL', 'V200201608240001__Create_table_cfg_region_types.sql', 1991620206, 'hcms', '2016-08-28 13:48:40.046479', 34, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (271, 271, '200201608250001', 'Create table cfg vehicle types', 'SQL', 'V200201608250001__Create_table_cfg_vehicle_types.sql', -316675981, 'hcms', '2016-08-28 13:48:40.114664', 21, true);
INSERT INTO schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (272, 272, '200201608260001', 'Alter table zmedtrxhdr adddocstatus2', 'SQL', 'V200201608260001__Alter_table_zmedtrxhdr_adddocstatus2.sql', 813480663, 'hcms', '2016-08-28 13:48:40.163694', 6, true);


--
-- TOC entry 3263 (class 0 OID 56214)
-- Dependencies: 226
-- Data for Name: sh_t706b1; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3264 (class 0 OID 56222)
-- Dependencies: 227
-- Data for Name: t001; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3265 (class 0 OID 56227)
-- Dependencies: 228
-- Data for Name: t002t; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3266 (class 0 OID 56232)
-- Dependencies: 229
-- Data for Name: t005t; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3267 (class 0 OID 56237)
-- Dependencies: 230
-- Data for Name: t005u; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3268 (class 0 OID 56242)
-- Dependencies: 231
-- Data for Name: t016t; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3269 (class 0 OID 56247)
-- Dependencies: 232
-- Data for Name: t042z; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3270 (class 0 OID 56255)
-- Dependencies: 233
-- Data for Name: t500l; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3271 (class 0 OID 56260)
-- Dependencies: 234
-- Data for Name: t500p; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3272 (class 0 OID 56268)
-- Dependencies: 235
-- Data for Name: t501t; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3273 (class 0 OID 56273)
-- Dependencies: 236
-- Data for Name: t502t; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3274 (class 0 OID 56278)
-- Dependencies: 237
-- Data for Name: t503k; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3275 (class 0 OID 56283)
-- Dependencies: 238
-- Data for Name: t505s; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3276 (class 0 OID 56288)
-- Dependencies: 239
-- Data for Name: t512t; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3277 (class 0 OID 56296)
-- Dependencies: 240
-- Data for Name: t513c; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3278 (class 0 OID 56301)
-- Dependencies: 241
-- Data for Name: t513s; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3279 (class 0 OID 56306)
-- Dependencies: 242
-- Data for Name: t516t; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3280 (class 0 OID 56311)
-- Dependencies: 243
-- Data for Name: t517t; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3281 (class 0 OID 56316)
-- Dependencies: 244
-- Data for Name: t517x; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3282 (class 0 OID 56324)
-- Dependencies: 245
-- Data for Name: t518b; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3283 (class 0 OID 56329)
-- Dependencies: 246
-- Data for Name: t519t; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3284 (class 0 OID 56337)
-- Dependencies: 247
-- Data for Name: t522g; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3285 (class 0 OID 56342)
-- Dependencies: 248
-- Data for Name: t527x; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3286 (class 0 OID 56347)
-- Dependencies: 249
-- Data for Name: t528t; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3287 (class 0 OID 56352)
-- Dependencies: 250
-- Data for Name: t531s; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3288 (class 0 OID 56360)
-- Dependencies: 251
-- Data for Name: t535n; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3289 (class 0 OID 56365)
-- Dependencies: 252
-- Data for Name: t538t; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3290 (class 0 OID 56373)
-- Dependencies: 253
-- Data for Name: t548t; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3291 (class 0 OID 56381)
-- Dependencies: 254
-- Data for Name: t549t; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3292 (class 0 OID 56386)
-- Dependencies: 255
-- Data for Name: t554t; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3293 (class 0 OID 56394)
-- Dependencies: 256
-- Data for Name: t556b; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3294 (class 0 OID 56399)
-- Dependencies: 257
-- Data for Name: t591s; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3295 (class 0 OID 56407)
-- Dependencies: 258
-- Data for Name: t5r06; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3296 (class 0 OID 56412)
-- Dependencies: 259
-- Data for Name: t702n; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3297 (class 0 OID 56417)
-- Dependencies: 260
-- Data for Name: t702o; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3298 (class 0 OID 56425)
-- Dependencies: 261
-- Data for Name: t705h; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3299 (class 0 OID 56433)
-- Dependencies: 262
-- Data for Name: t705p; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3300 (class 0 OID 56441)
-- Dependencies: 263
-- Data for Name: t706b1; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3301 (class 0 OID 56449)
-- Dependencies: 264
-- Data for Name: t706s; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3302 (class 0 OID 56457)
-- Dependencies: 265
-- Data for Name: teven; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3303 (class 0 OID 56465)
-- Dependencies: 266
-- Data for Name: trx_approval_event_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3304 (class 0 OID 56473)
-- Dependencies: 267
-- Data for Name: trx_event_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3305 (class 0 OID 56481)
-- Dependencies: 268
-- Data for Name: user_roles; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3306 (class 0 OID 56489)
-- Dependencies: 269
-- Data for Name: v_001p_all; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3307 (class 0 OID 56497)
-- Dependencies: 270
-- Data for Name: workflow_steps; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3308 (class 0 OID 56505)
-- Dependencies: 271
-- Data for Name: zmed_costype; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3309 (class 0 OID 56513)
-- Dependencies: 272
-- Data for Name: zmed_empquota; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3310 (class 0 OID 56521)
-- Dependencies: 273
-- Data for Name: zmed_empquotasm; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3311 (class 0 OID 56529)
-- Dependencies: 274
-- Data for Name: zmed_empquotastf; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3312 (class 0 OID 56537)
-- Dependencies: 275
-- Data for Name: zmed_header_post; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3313 (class 0 OID 56545)
-- Dependencies: 276
-- Data for Name: zmed_medtype; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3314 (class 0 OID 56553)
-- Dependencies: 277
-- Data for Name: zmed_quotaused; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3315 (class 0 OID 56561)
-- Dependencies: 278
-- Data for Name: zmed_quotype; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3316 (class 0 OID 56569)
-- Dependencies: 279
-- Data for Name: zmed_trxhdr; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3317 (class 0 OID 56577)
-- Dependencies: 280
-- Data for Name: zmed_trxitm; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2738 (class 2606 OID 55893)
-- Name: bnka_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY bnka
    ADD CONSTRAINT bnka_pkey PRIMARY KEY (bankl);


--
-- TOC entry 2740 (class 2606 OID 55901)
-- Name: cfg_approval_category_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cfg_approval_category
    ADD CONSTRAINT cfg_approval_category_pkey PRIMARY KEY (id);


--
-- TOC entry 2744 (class 2606 OID 55909)
-- Name: cfg_attachment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cfg_attachment
    ADD CONSTRAINT cfg_attachment_pkey PRIMARY KEY (infty, subty);


--
-- TOC entry 2750 (class 2606 OID 55925)
-- Name: cfg_entity_approval_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cfg_entity_approval
    ADD CONSTRAINT cfg_entity_approval_pkey PRIMARY KEY (id);


--
-- TOC entry 2746 (class 2606 OID 55917)
-- Name: cfg_entity_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cfg_entity
    ADD CONSTRAINT cfg_entity_pkey PRIMARY KEY (id);


--
-- TOC entry 2734 (class 2606 OID 51535)
-- Name: cfg_region_types_ident_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cfg_region_types
    ADD CONSTRAINT cfg_region_types_ident_name_key UNIQUE (ident_name);


--
-- TOC entry 2736 (class 2606 OID 51533)
-- Name: cfg_region_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cfg_region_types
    ADD CONSTRAINT cfg_region_types_pkey PRIMARY KEY (id);


--
-- TOC entry 2754 (class 2606 OID 55933)
-- Name: cfg_vehicle_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cfg_vehicle_types
    ADD CONSTRAINT cfg_vehicle_types_pkey PRIMARY KEY (id);


--
-- TOC entry 2758 (class 2606 OID 55941)
-- Name: cskt_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cskt
    ADD CONSTRAINT cskt_pkey PRIMARY KEY (kokrs, kostl);


--
-- TOC entry 2762 (class 2606 OID 55957)
-- Name: it0001_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_log
    ADD CONSTRAINT it0001_log_pkey PRIMARY KEY (id);


--
-- TOC entry 2760 (class 2606 OID 55949)
-- Name: it0001_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001
    ADD CONSTRAINT it0001_pkey PRIMARY KEY (begda, endda, pernr);


--
-- TOC entry 2764 (class 2606 OID 55965)
-- Name: it0001_trx_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_trx
    ADD CONSTRAINT it0001_trx_pkey PRIMARY KEY (id);


--
-- TOC entry 2766 (class 2606 OID 55973)
-- Name: it0002_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0002
    ADD CONSTRAINT it0002_pkey PRIMARY KEY (begda, endda, pernr);


--
-- TOC entry 2768 (class 2606 OID 55981)
-- Name: it0006_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0006
    ADD CONSTRAINT it0006_pkey PRIMARY KEY (begda, endda, infty, pernr, subty);


--
-- TOC entry 2770 (class 2606 OID 55989)
-- Name: it0009_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0009
    ADD CONSTRAINT it0009_pkey PRIMARY KEY (begda, endda, infty, pernr, subty);


--
-- TOC entry 2772 (class 2606 OID 55997)
-- Name: it0014_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0014
    ADD CONSTRAINT it0014_pkey PRIMARY KEY (begda, endda, infty, pernr, subty);


--
-- TOC entry 2774 (class 2606 OID 56005)
-- Name: it0019_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0019
    ADD CONSTRAINT it0019_pkey PRIMARY KEY (begda, endda, infty, pernr, subty);


--
-- TOC entry 2776 (class 2606 OID 56013)
-- Name: it0021_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0021
    ADD CONSTRAINT it0021_pkey PRIMARY KEY (begda, endda, infty, pernr, subty);


--
-- TOC entry 2778 (class 2606 OID 56021)
-- Name: it0022_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0022
    ADD CONSTRAINT it0022_pkey PRIMARY KEY (begda, endda, infty, pernr, subty);


--
-- TOC entry 2780 (class 2606 OID 56029)
-- Name: it0023_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0023
    ADD CONSTRAINT it0023_pkey PRIMARY KEY (begda, endda, pernr);


--
-- TOC entry 2782 (class 2606 OID 56037)
-- Name: it0040_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0040
    ADD CONSTRAINT it0040_pkey PRIMARY KEY (begda, endda, infty, pernr, subty);


--
-- TOC entry 2784 (class 2606 OID 56045)
-- Name: it0041_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0041
    ADD CONSTRAINT it0041_pkey PRIMARY KEY (begda, endda, pernr);


--
-- TOC entry 2786 (class 2606 OID 56050)
-- Name: it0077_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0077
    ADD CONSTRAINT it0077_pkey PRIMARY KEY (begda, endda, pernr);


--
-- TOC entry 2788 (class 2606 OID 56058)
-- Name: it0105_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0105
    ADD CONSTRAINT it0105_pkey PRIMARY KEY (begda, endda, infty, pernr, subty);


--
-- TOC entry 2790 (class 2606 OID 56066)
-- Name: it0184_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0184
    ADD CONSTRAINT it0184_pkey PRIMARY KEY (begda, endda, infty, pernr, subty);


--
-- TOC entry 2792 (class 2606 OID 56074)
-- Name: it0185_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0185
    ADD CONSTRAINT it0185_pkey PRIMARY KEY (begda, endda, infty, pernr, subty);


--
-- TOC entry 2794 (class 2606 OID 56082)
-- Name: it0241_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0241
    ADD CONSTRAINT it0241_pkey PRIMARY KEY (begda, endda, pernr);


--
-- TOC entry 2796 (class 2606 OID 56090)
-- Name: it0242_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0242
    ADD CONSTRAINT it0242_pkey PRIMARY KEY (begda, endda, pernr);


--
-- TOC entry 2798 (class 2606 OID 56098)
-- Name: it2001_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it2001
    ADD CONSTRAINT it2001_pkey PRIMARY KEY (begda, endda, infty, pernr, subty);


--
-- TOC entry 2800 (class 2606 OID 56106)
-- Name: it2002_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it2002
    ADD CONSTRAINT it2002_pkey PRIMARY KEY (begda, endda, infty, pernr, subty);


--
-- TOC entry 2802 (class 2606 OID 56114)
-- Name: it2006_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it2006
    ADD CONSTRAINT it2006_pkey PRIMARY KEY (begda, endda, infty, pernr, subty);


--
-- TOC entry 2804 (class 2606 OID 56122)
-- Name: m_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_jobs
    ADD CONSTRAINT m_jobs_pkey PRIMARY KEY (id);


--
-- TOC entry 2808 (class 2606 OID 56130)
-- Name: m_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_permission
    ADD CONSTRAINT m_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 2814 (class 2606 OID 56138)
-- Name: m_position_relation_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_position_relation_types
    ADD CONSTRAINT m_position_relation_types_pkey PRIMARY KEY (id);


--
-- TOC entry 2818 (class 2606 OID 56146)
-- Name: m_positions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_positions
    ADD CONSTRAINT m_positions_pkey PRIMARY KEY (id);


--
-- TOC entry 2822 (class 2606 OID 56154)
-- Name: m_role_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_role
    ADD CONSTRAINT m_role_pkey PRIMARY KEY (id);


--
-- TOC entry 2826 (class 2606 OID 56162)
-- Name: m_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_user
    ADD CONSTRAINT m_user_pkey PRIMARY KEY (id);


--
-- TOC entry 2832 (class 2606 OID 56170)
-- Name: m_workflows_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_workflows
    ADD CONSTRAINT m_workflows_pkey PRIMARY KEY (id);


--
-- TOC entry 2836 (class 2606 OID 56181)
-- Name: position_relations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY position_relations
    ADD CONSTRAINT position_relations_pkey PRIMARY KEY (id);


--
-- TOC entry 2838 (class 2606 OID 56189)
-- Name: ptrv_head_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ptrv_head
    ADD CONSTRAINT ptrv_head_pkey PRIMARY KEY (hdvrs, pernr, reinr);


--
-- TOC entry 2840 (class 2606 OID 56197)
-- Name: ptrv_perio_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ptrv_perio
    ADD CONSTRAINT ptrv_perio_pkey PRIMARY KEY (mandt, pdvrs, perio, pernr, reinr);


--
-- TOC entry 2842 (class 2606 OID 56205)
-- Name: ptrv_srec_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ptrv_srec
    ADD CONSTRAINT ptrv_srec_pkey PRIMARY KEY (mandt, perio, pernr, receiptno, reinr);


--
-- TOC entry 2844 (class 2606 OID 56213)
-- Name: role_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY role_permissions
    ADD CONSTRAINT role_permissions_pkey PRIMARY KEY (m_role_id, m_permission_id);


--
-- TOC entry 2730 (class 2606 OID 50497)
-- Name: schema_version_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_version
    ADD CONSTRAINT schema_version_pk PRIMARY KEY (version);


--
-- TOC entry 2846 (class 2606 OID 56221)
-- Name: sh_t706b1_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sh_t706b1
    ADD CONSTRAINT sh_t706b1_pkey PRIMARY KEY (morei, spkzl);


--
-- TOC entry 2850 (class 2606 OID 56226)
-- Name: t001_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t001
    ADD CONSTRAINT t001_pkey PRIMARY KEY (bukrs);


--
-- TOC entry 2852 (class 2606 OID 56231)
-- Name: t002t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t002t
    ADD CONSTRAINT t002t_pkey PRIMARY KEY (sprsl);


--
-- TOC entry 2854 (class 2606 OID 56236)
-- Name: t005t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t005t
    ADD CONSTRAINT t005t_pkey PRIMARY KEY (land1);


--
-- TOC entry 2856 (class 2606 OID 56241)
-- Name: t005u_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t005u
    ADD CONSTRAINT t005u_pkey PRIMARY KEY (bland);


--
-- TOC entry 2858 (class 2606 OID 56246)
-- Name: t016t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t016t
    ADD CONSTRAINT t016t_pkey PRIMARY KEY (brsch);


--
-- TOC entry 2860 (class 2606 OID 56254)
-- Name: t042z_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t042z
    ADD CONSTRAINT t042z_pkey PRIMARY KEY (zlsch);


--
-- TOC entry 2862 (class 2606 OID 56259)
-- Name: t500l_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t500l
    ADD CONSTRAINT t500l_pkey PRIMARY KEY (molga);


--
-- TOC entry 2864 (class 2606 OID 56267)
-- Name: t500p_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t500p
    ADD CONSTRAINT t500p_pkey PRIMARY KEY (bukrs, persa);


--
-- TOC entry 2866 (class 2606 OID 56272)
-- Name: t501t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t501t
    ADD CONSTRAINT t501t_pkey PRIMARY KEY (persg);


--
-- TOC entry 2868 (class 2606 OID 56277)
-- Name: t502t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t502t
    ADD CONSTRAINT t502t_pkey PRIMARY KEY (famst);


--
-- TOC entry 2870 (class 2606 OID 56282)
-- Name: t503k_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t503k
    ADD CONSTRAINT t503k_pkey PRIMARY KEY (mandt);


--
-- TOC entry 2872 (class 2606 OID 56287)
-- Name: t505s_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t505s
    ADD CONSTRAINT t505s_pkey PRIMARY KEY (molga, racky);


--
-- TOC entry 2874 (class 2606 OID 56295)
-- Name: t512t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t512t
    ADD CONSTRAINT t512t_pkey PRIMARY KEY (lgart);


--
-- TOC entry 2876 (class 2606 OID 56300)
-- Name: t513c_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t513c
    ADD CONSTRAINT t513c_pkey PRIMARY KEY (taete);


--
-- TOC entry 2878 (class 2606 OID 56305)
-- Name: t513s_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t513s
    ADD CONSTRAINT t513s_pkey PRIMARY KEY (endda, stell);


--
-- TOC entry 2880 (class 2606 OID 56310)
-- Name: t516t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t516t
    ADD CONSTRAINT t516t_pkey PRIMARY KEY (konfe);


--
-- TOC entry 2882 (class 2606 OID 56315)
-- Name: t517t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t517t
    ADD CONSTRAINT t517t_pkey PRIMARY KEY (slart);


--
-- TOC entry 2884 (class 2606 OID 56323)
-- Name: t517x_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t517x
    ADD CONSTRAINT t517x_pkey PRIMARY KEY (faart);


--
-- TOC entry 2886 (class 2606 OID 56328)
-- Name: t518b_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t518b
    ADD CONSTRAINT t518b_pkey PRIMARY KEY (ausbi);


--
-- TOC entry 2888 (class 2606 OID 56336)
-- Name: t519t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t519t
    ADD CONSTRAINT t519t_pkey PRIMARY KEY (slabs);


--
-- TOC entry 2890 (class 2606 OID 56341)
-- Name: t522g_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t522g
    ADD CONSTRAINT t522g_pkey PRIMARY KEY (anred);


--
-- TOC entry 2892 (class 2606 OID 56346)
-- Name: t527x_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t527x
    ADD CONSTRAINT t527x_pkey PRIMARY KEY (endda, orgeh);


--
-- TOC entry 2894 (class 2606 OID 56351)
-- Name: t528t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t528t
    ADD CONSTRAINT t528t_pkey PRIMARY KEY (endda, plans);


--
-- TOC entry 2896 (class 2606 OID 56359)
-- Name: t531s_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t531s
    ADD CONSTRAINT t531s_pkey PRIMARY KEY (tmart);


--
-- TOC entry 2898 (class 2606 OID 56364)
-- Name: t535n_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t535n
    ADD CONSTRAINT t535n_pkey PRIMARY KEY (art, title);


--
-- TOC entry 2900 (class 2606 OID 56372)
-- Name: t538t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t538t
    ADD CONSTRAINT t538t_pkey PRIMARY KEY (zeinh);


--
-- TOC entry 2902 (class 2606 OID 56380)
-- Name: t548t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t548t
    ADD CONSTRAINT t548t_pkey PRIMARY KEY (datar);


--
-- TOC entry 2904 (class 2606 OID 56385)
-- Name: t549t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t549t
    ADD CONSTRAINT t549t_pkey PRIMARY KEY (abkrs);


--
-- TOC entry 2906 (class 2606 OID 56393)
-- Name: t554t_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t554t
    ADD CONSTRAINT t554t_pkey PRIMARY KEY (awart);


--
-- TOC entry 2908 (class 2606 OID 56398)
-- Name: t556b_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t556b
    ADD CONSTRAINT t556b_pkey PRIMARY KEY (ktart);


--
-- TOC entry 2910 (class 2606 OID 56406)
-- Name: t591s_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t591s
    ADD CONSTRAINT t591s_pkey PRIMARY KEY (infty, subty);


--
-- TOC entry 2912 (class 2606 OID 56411)
-- Name: t5r06_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t5r06
    ADD CONSTRAINT t5r06_pkey PRIMARY KEY (ictyp);


--
-- TOC entry 2914 (class 2606 OID 56416)
-- Name: t702n_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t702n
    ADD CONSTRAINT t702n_pkey PRIMARY KEY (morei);


--
-- TOC entry 2916 (class 2606 OID 56424)
-- Name: t702o_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t702o
    ADD CONSTRAINT t702o_pkey PRIMARY KEY (land1, morei);


--
-- TOC entry 2918 (class 2606 OID 56432)
-- Name: t705h_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t705h
    ADD CONSTRAINT t705h_pkey PRIMARY KEY (grawg, moabw, pinco, spras, zeity);


--
-- TOC entry 2922 (class 2606 OID 56440)
-- Name: t705p_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t705p
    ADD CONSTRAINT t705p_pkey PRIMARY KEY (pmbde, satza);


--
-- TOC entry 2926 (class 2606 OID 56448)
-- Name: t706b1_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t706b1
    ADD CONSTRAINT t706b1_pkey PRIMARY KEY (begda, endda, morei, spkzl);


--
-- TOC entry 2928 (class 2606 OID 56456)
-- Name: t706s_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t706s
    ADD CONSTRAINT t706s_pkey PRIMARY KEY (morei, schem);


--
-- TOC entry 2930 (class 2606 OID 56464)
-- Name: teven_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY teven
    ADD CONSTRAINT teven_pkey PRIMARY KEY (pdsnr);


--
-- TOC entry 2932 (class 2606 OID 56472)
-- Name: trx_approval_event_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY trx_approval_event_log
    ADD CONSTRAINT trx_approval_event_log_pkey PRIMARY KEY (id);


--
-- TOC entry 2934 (class 2606 OID 56480)
-- Name: trx_event_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY trx_event_log
    ADD CONSTRAINT trx_event_log_pkey PRIMARY KEY (id);


--
-- TOC entry 2924 (class 2606 OID 56616)
-- Name: uk_5svyqphdniwahlgodju771j7t; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t705p
    ADD CONSTRAINT uk_5svyqphdniwahlgodju771j7t UNIQUE (satza);


--
-- TOC entry 2806 (class 2606 OID 56594)
-- Name: uk_6ea6r2o67ugfbke3jg35vwxy5; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_jobs
    ADD CONSTRAINT uk_6ea6r2o67ugfbke3jg35vwxy5 UNIQUE (code);


--
-- TOC entry 2810 (class 2606 OID 56598)
-- Name: uk_7cyo7sywtv54yt5q8m5rsbuix; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_permission
    ADD CONSTRAINT uk_7cyo7sywtv54yt5q8m5rsbuix UNIQUE (permission_object);


--
-- TOC entry 2828 (class 2606 OID 56608)
-- Name: uk_8rlf98f21n1evst8l1x9cw1lh; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_user
    ADD CONSTRAINT uk_8rlf98f21n1evst8l1x9cw1lh UNIQUE (user_name);


--
-- TOC entry 2830 (class 2606 OID 56606)
-- Name: uk_ar5uxhkuuh5c5cdo5gplggd14; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_user
    ADD CONSTRAINT uk_ar5uxhkuuh5c5cdo5gplggd14 UNIQUE (user_email);


--
-- TOC entry 2756 (class 2606 OID 56592)
-- Name: uk_epvcsuur4gpbq1fccm4xdys7n; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cfg_vehicle_types
    ADD CONSTRAINT uk_epvcsuur4gpbq1fccm4xdys7n UNIQUE (ident_name);


--
-- TOC entry 2748 (class 2606 OID 56588)
-- Name: uk_fuqlnoq7gut46s4osbktbtmpl; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cfg_entity
    ADD CONSTRAINT uk_fuqlnoq7gut46s4osbktbtmpl UNIQUE (entity_name);


--
-- TOC entry 2816 (class 2606 OID 56600)
-- Name: uk_g6ow2dcuqlf1keahjqrvwv1gj; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_position_relation_types
    ADD CONSTRAINT uk_g6ow2dcuqlf1keahjqrvwv1gj UNIQUE (name);


--
-- TOC entry 2742 (class 2606 OID 56586)
-- Name: uk_gnasjwovg5fi4x0y5kco5am8q; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cfg_approval_category
    ADD CONSTRAINT uk_gnasjwovg5fi4x0y5kco5am8q UNIQUE (ent_category, m_workflow_id);


--
-- TOC entry 2920 (class 2606 OID 56614)
-- Name: uk_grxkpf14rhoxl3jc9w6gwb546; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t705h
    ADD CONSTRAINT uk_grxkpf14rhoxl3jc9w6gwb546 UNIQUE (pinco);


--
-- TOC entry 2812 (class 2606 OID 56596)
-- Name: uk_kkqjx6o9vkd84dvfs8ect43u7; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_permission
    ADD CONSTRAINT uk_kkqjx6o9vkd84dvfs8ect43u7 UNIQUE (permission_name);


--
-- TOC entry 2820 (class 2606 OID 56602)
-- Name: uk_lmhuv51r03bdh1n6r4g68l30x; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_positions
    ADD CONSTRAINT uk_lmhuv51r03bdh1n6r4g68l30x UNIQUE (code);


--
-- TOC entry 2940 (class 2606 OID 56618)
-- Name: uk_m0jxj2sn6qkuksa4qxurwc0qw; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY workflow_steps
    ADD CONSTRAINT uk_m0jxj2sn6qkuksa4qxurwc0qw UNIQUE (m_workflow_id, seq, m_position_id);


--
-- TOC entry 2848 (class 2606 OID 56612)
-- Name: uk_mlkejlt11uardo9ip8111i5eh; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sh_t706b1
    ADD CONSTRAINT uk_mlkejlt11uardo9ip8111i5eh UNIQUE (spkzl);


--
-- TOC entry 2752 (class 2606 OID 56590)
-- Name: uk_opvhi6xj6lem43mmay8sej9w3; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cfg_entity_approval
    ADD CONSTRAINT uk_opvhi6xj6lem43mmay8sej9w3 UNIQUE (cfg_entity_id, cfg_approval_category_id);


--
-- TOC entry 2824 (class 2606 OID 56604)
-- Name: uk_ph2nbijpnob1l3gu2ipkgnqoe; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_role
    ADD CONSTRAINT uk_ph2nbijpnob1l3gu2ipkgnqoe UNIQUE (role_name);


--
-- TOC entry 2834 (class 2606 OID 56610)
-- Name: uk_q5ju8j3r5jcc23oh4q9tq7r1x; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_workflows
    ADD CONSTRAINT uk_q5ju8j3r5jcc23oh4q9tq7r1x UNIQUE (name);


--
-- TOC entry 2936 (class 2606 OID 56488)
-- Name: user_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT user_roles_pkey PRIMARY KEY (m_user_id, m_role_id);


--
-- TOC entry 2938 (class 2606 OID 56496)
-- Name: v_001p_all_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY v_001p_all
    ADD CONSTRAINT v_001p_all_pkey PRIMARY KEY (btrtl, werks);


--
-- TOC entry 2942 (class 2606 OID 56504)
-- Name: workflow_steps_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY workflow_steps
    ADD CONSTRAINT workflow_steps_pkey PRIMARY KEY (id);


--
-- TOC entry 2944 (class 2606 OID 56512)
-- Name: zmed_costype_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_costype
    ADD CONSTRAINT zmed_costype_pkey PRIMARY KEY (bukrs, datab, datbi, kodecos, kodemed);


--
-- TOC entry 2946 (class 2606 OID 56520)
-- Name: zmed_empquota_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_empquota
    ADD CONSTRAINT zmed_empquota_pkey PRIMARY KEY (bukrs, datab, datbi, fatxt, kodequo, persg, persk);


--
-- TOC entry 2948 (class 2606 OID 56528)
-- Name: zmed_empquotasm_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_empquotasm
    ADD CONSTRAINT zmed_empquotasm_pkey PRIMARY KEY (bukrs, datab, datbi, fatxt, kodequo, pernr, persg, persk);


--
-- TOC entry 2950 (class 2606 OID 56536)
-- Name: zmed_empquotastf_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_empquotastf
    ADD CONSTRAINT zmed_empquotastf_pkey PRIMARY KEY (bukrs, datab, datbi, kodequo, persg, persk, stell);


--
-- TOC entry 2952 (class 2606 OID 56544)
-- Name: zmed_header_post_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_header_post
    ADD CONSTRAINT zmed_header_post_pkey PRIMARY KEY (obj_key, obj_sys, obj_type);


--
-- TOC entry 2954 (class 2606 OID 56552)
-- Name: zmed_medtype_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_medtype
    ADD CONSTRAINT zmed_medtype_pkey PRIMARY KEY (bukrs, datab, datbi, kodemed);


--
-- TOC entry 2956 (class 2606 OID 56560)
-- Name: zmed_quotaused_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_quotaused
    ADD CONSTRAINT zmed_quotaused_pkey PRIMARY KEY (bukrs, gjahr, kodequo, pernr);


--
-- TOC entry 2958 (class 2606 OID 56568)
-- Name: zmed_quotype_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_quotype
    ADD CONSTRAINT zmed_quotype_pkey PRIMARY KEY (bukrs, datab, datbi, kodequo);


--
-- TOC entry 2960 (class 2606 OID 56576)
-- Name: zmed_trxhdr_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_trxhdr
    ADD CONSTRAINT zmed_trxhdr_pkey PRIMARY KEY (bukrs, zclmno);


--
-- TOC entry 2962 (class 2606 OID 56584)
-- Name: zmed_trxitm_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_trxitm
    ADD CONSTRAINT zmed_trxitm_pkey PRIMARY KEY (clmno, itmno);


--
-- TOC entry 2728 (class 1259 OID 50605)
-- Name: schema_version_ir_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX schema_version_ir_idx ON schema_version USING btree (installed_rank);


--
-- TOC entry 2731 (class 1259 OID 50606)
-- Name: schema_version_s_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX schema_version_s_idx ON schema_version USING btree (success);


--
-- TOC entry 2732 (class 1259 OID 50607)
-- Name: schema_version_vr_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX schema_version_vr_idx ON schema_version USING btree (version_rank);


--
-- TOC entry 3094 (class 2606 OID 57274)
-- Name: fk_12w6wpoi1sxtrs7wf78hnthmm; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY workflow_steps
    ADD CONSTRAINT fk_12w6wpoi1sxtrs7wf78hnthmm FOREIGN KEY (m_workflow_id) REFERENCES m_workflows(id);


--
-- TOC entry 3062 (class 2606 OID 57114)
-- Name: fk_1mypwvijf4gxl63jfvbotmqjb; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0184
    ADD CONSTRAINT fk_1mypwvijf4gxl63jfvbotmqjb FOREIGN KEY (infty, subty) REFERENCES t591s(infty, subty);


--
-- TOC entry 3101 (class 2606 OID 57309)
-- Name: fk_1t4sn1xaa6x0me1c2j764i3eu; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_quotaused
    ADD CONSTRAINT fk_1t4sn1xaa6x0me1c2j764i3eu FOREIGN KEY (bukrs) REFERENCES t001(bukrs);


--
-- TOC entry 3072 (class 2606 OID 57164)
-- Name: fk_2rgggsd6dtuyplcy60dkf8w2f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it2002
    ADD CONSTRAINT fk_2rgggsd6dtuyplcy60dkf8w2f FOREIGN KEY (infty, subty) REFERENCES t591s(infty, subty);


--
-- TOC entry 3016 (class 2606 OID 56884)
-- Name: fk_2t24kp0g7q92s8r1k2g88ur4e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0009
    ADD CONSTRAINT fk_2t24kp0g7q92s8r1k2g88ur4e FOREIGN KEY (banks) REFERENCES t005t(land1);


--
-- TOC entry 3058 (class 2606 OID 57094)
-- Name: fk_2u30302p4akhdiq12chgsv9c3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0041
    ADD CONSTRAINT fk_2u30302p4akhdiq12chgsv9c3 FOREIGN KEY (dar12) REFERENCES t548t(datar);


--
-- TOC entry 3027 (class 2606 OID 56939)
-- Name: fk_2vgj6pbshh06txwb8plj7u9rq; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0021
    ADD CONSTRAINT fk_2vgj6pbshh06txwb8plj7u9rq FOREIGN KEY (fanat) REFERENCES t005t(land1);


--
-- TOC entry 2980 (class 2606 OID 56704)
-- Name: fk_2wjj6s684w7pjlhvmj38wgrg5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_log
    ADD CONSTRAINT fk_2wjj6s684w7pjlhvmj38wgrg5 FOREIGN KEY (bukrs, werks) REFERENCES t500p(bukrs, persa);


--
-- TOC entry 3063 (class 2606 OID 57119)
-- Name: fk_309yhbyx0xfpl170pj412o61; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0185
    ADD CONSTRAINT fk_309yhbyx0xfpl170pj412o61 FOREIGN KEY (infty, attach1_subty) REFERENCES cfg_attachment(infty, subty);


--
-- TOC entry 2991 (class 2606 OID 56759)
-- Name: fk_354po19xpt6y13grsw9u4bgmq; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_trx
    ADD CONSTRAINT fk_354po19xpt6y13grsw9u4bgmq FOREIGN KEY (m_position_id) REFERENCES m_positions(id);


--
-- TOC entry 3059 (class 2606 OID 57099)
-- Name: fk_3m9ignxluiicdytq6t7v63e8r; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0077
    ADD CONSTRAINT fk_3m9ignxluiicdytq6t7v63e8r FOREIGN KEY (molga, racky) REFERENCES t505s(molga, racky);


--
-- TOC entry 3000 (class 2606 OID 56804)
-- Name: fk_3m9ljs651qt26ibpv2jao6rp6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0002
    ADD CONSTRAINT fk_3m9ljs651qt26ibpv2jao6rp6 FOREIGN KEY (anred) REFERENCES t522g(anred);


--
-- TOC entry 3102 (class 2606 OID 57314)
-- Name: fk_3nsr8ooj5cb0h5sv0sx900de; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_quotype
    ADD CONSTRAINT fk_3nsr8ooj5cb0h5sv0sx900de FOREIGN KEY (bukrs) REFERENCES t001(bukrs);


--
-- TOC entry 2992 (class 2606 OID 56764)
-- Name: fk_3ueklimsyf7leh3o3ii6qklyn; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_trx
    ADD CONSTRAINT fk_3ueklimsyf7leh3o3ii6qklyn FOREIGN KEY (bukrs, werks) REFERENCES t500p(bukrs, persa);


--
-- TOC entry 3089 (class 2606 OID 57249)
-- Name: fk_4e8711xcyrjuy9u0ihsgcckjw; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY trx_approval_event_log
    ADD CONSTRAINT fk_4e8711xcyrjuy9u0ihsgcckjw FOREIGN KEY (performed_by_position_id) REFERENCES m_positions(id);


--
-- TOC entry 3041 (class 2606 OID 57009)
-- Name: fk_5n9j8o62ho7nmjn2a8hoyavlo; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0023
    ADD CONSTRAINT fk_5n9j8o62ho7nmjn2a8hoyavlo FOREIGN KEY (branc) REFERENCES t016t(brsch);


--
-- TOC entry 3082 (class 2606 OID 57214)
-- Name: fk_5o6j7vc1mxuy30yxw61pdwawn; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY role_permissions
    ADD CONSTRAINT fk_5o6j7vc1mxuy30yxw61pdwawn FOREIGN KEY (m_permission_id) REFERENCES m_permission(id);


--
-- TOC entry 2986 (class 2606 OID 56734)
-- Name: fk_5ss1f2eu7knyf2ecue09ugfa0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_log
    ADD CONSTRAINT fk_5ss1f2eu7knyf2ecue09ugfa0 FOREIGN KEY (abkrs) REFERENCES t549t(abkrs);


--
-- TOC entry 3104 (class 2606 OID 57324)
-- Name: fk_66e3u2k8iykpsec9nu9erbc2v; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_trxitm
    ADD CONSTRAINT fk_66e3u2k8iykpsec9nu9erbc2v FOREIGN KEY (bukrs) REFERENCES t001(bukrs);


--
-- TOC entry 3097 (class 2606 OID 57289)
-- Name: fk_6mdku4yw2vslvbb4kk1vgvfko; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_empquotasm
    ADD CONSTRAINT fk_6mdku4yw2vslvbb4kk1vgvfko FOREIGN KEY (bukrs) REFERENCES t001(bukrs);


--
-- TOC entry 3068 (class 2606 OID 57144)
-- Name: fk_6ojroah9w5fk8twxev6sr10ia; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0242
    ADD CONSTRAINT fk_6ojroah9w5fk8twxev6sr10ia FOREIGN KEY (infty, attach1_subty) REFERENCES cfg_attachment(infty, subty);


--
-- TOC entry 3087 (class 2606 OID 57239)
-- Name: fk_6qfuv9yqusdy3ody0n5sko2hl; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY teven
    ADD CONSTRAINT fk_6qfuv9yqusdy3ody0n5sko2hl FOREIGN KEY (satza) REFERENCES t705p(satza);


--
-- TOC entry 2990 (class 2606 OID 56754)
-- Name: fk_6rbi5ekhoetlbth9f2t4htvo0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_trx
    ADD CONSTRAINT fk_6rbi5ekhoetlbth9f2t4htvo0 FOREIGN KEY (m_job_id) REFERENCES m_jobs(id);


--
-- TOC entry 2987 (class 2606 OID 56739)
-- Name: fk_6tx3paxc1cgbbm3mba2vwhpbj; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_log
    ADD CONSTRAINT fk_6tx3paxc1cgbbm3mba2vwhpbj FOREIGN KEY (trx_id) REFERENCES it0001_trx(id);


--
-- TOC entry 2964 (class 2606 OID 56624)
-- Name: fk_7ajeoh1g598m0uvo8101vaej5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cfg_entity_approval
    ADD CONSTRAINT fk_7ajeoh1g598m0uvo8101vaej5 FOREIGN KEY (cfg_approval_category_id) REFERENCES cfg_approval_category(id);


--
-- TOC entry 3084 (class 2606 OID 57224)
-- Name: fk_7fwvk60t3nlnv2wvd4ohioaeq; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t500p
    ADD CONSTRAINT fk_7fwvk60t3nlnv2wvd4ohioaeq FOREIGN KEY (bukrs) REFERENCES t001(bukrs);


--
-- TOC entry 3012 (class 2606 OID 56864)
-- Name: fk_7pny53sub4e1mkvehfqw56fuc; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0006
    ADD CONSTRAINT fk_7pny53sub4e1mkvehfqw56fuc FOREIGN KEY (land1) REFERENCES t005t(land1);


--
-- TOC entry 3038 (class 2606 OID 56994)
-- Name: fk_7upk3nkyxtk31c3svtr6phxk3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0022
    ADD CONSTRAINT fk_7upk3nkyxtk31c3svtr6phxk3 FOREIGN KEY (sltp2) REFERENCES t517x(faart);


--
-- TOC entry 3040 (class 2606 OID 57004)
-- Name: fk_7yldi3xbokh4iu9mfgwrid010; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0023
    ADD CONSTRAINT fk_7yldi3xbokh4iu9mfgwrid010 FOREIGN KEY (infty, attach1_subty) REFERENCES cfg_attachment(infty, subty);


--
-- TOC entry 3091 (class 2606 OID 57259)
-- Name: fk_80p2i2m1qdmfmgqh4w7qx03oc; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT fk_80p2i2m1qdmfmgqh4w7qx03oc FOREIGN KEY (m_role_id) REFERENCES m_role(id);


--
-- TOC entry 2969 (class 2606 OID 56649)
-- Name: fk_87g5vmi1e6sph5vt4yyngd6f5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001
    ADD CONSTRAINT fk_87g5vmi1e6sph5vt4yyngd6f5 FOREIGN KEY (bukrs, werks) REFERENCES t500p(bukrs, persa);


--
-- TOC entry 2979 (class 2606 OID 56699)
-- Name: fk_8bk5cu32oavd8nfhiik73jodm; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_log
    ADD CONSTRAINT fk_8bk5cu32oavd8nfhiik73jodm FOREIGN KEY (m_position_id) REFERENCES m_positions(id);


--
-- TOC entry 3086 (class 2606 OID 57234)
-- Name: fk_8fqcrsoyi7phn0gdanibmqqi2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY teven
    ADD CONSTRAINT fk_8fqcrsoyi7phn0gdanibmqqi2 FOREIGN KEY (abwgr) REFERENCES t705h(pinco);


--
-- TOC entry 2968 (class 2606 OID 56644)
-- Name: fk_8gqoh1h4y4fuykwgc3t9q895q; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001
    ADD CONSTRAINT fk_8gqoh1h4y4fuykwgc3t9q895q FOREIGN KEY (m_position_id) REFERENCES m_positions(id);


--
-- TOC entry 2997 (class 2606 OID 56789)
-- Name: fk_8kvxubuiikuwxwt3cbmwc7242; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_trx
    ADD CONSTRAINT fk_8kvxubuiikuwxwt3cbmwc7242 FOREIGN KEY (endda_emposition, plans) REFERENCES t528t(endda, plans);


--
-- TOC entry 2973 (class 2606 OID 56669)
-- Name: fk_8mujmykc8l3my989icj47wcnf; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001
    ADD CONSTRAINT fk_8mujmykc8l3my989icj47wcnf FOREIGN KEY (endda_orgunit, orgeh) REFERENCES t527x(endda, orgeh);


--
-- TOC entry 2999 (class 2606 OID 56799)
-- Name: fk_8xctuu745eu7ucfs6gujiecm2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_trx
    ADD CONSTRAINT fk_8xctuu745eu7ucfs6gujiecm2 FOREIGN KEY (btrtl, werks2) REFERENCES v_001p_all(btrtl, werks);


--
-- TOC entry 3034 (class 2606 OID 56974)
-- Name: fk_9132d6ryq7likyc4qsq4v0r0t; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0022
    ADD CONSTRAINT fk_9132d6ryq7likyc4qsq4v0r0t FOREIGN KEY (slabs) REFERENCES t519t(slabs);


--
-- TOC entry 3048 (class 2606 OID 57044)
-- Name: fk_93e1ux1sxbt5e2rm0om6d4gxt; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0041
    ADD CONSTRAINT fk_93e1ux1sxbt5e2rm0om6d4gxt FOREIGN KEY (dar02) REFERENCES t548t(datar);


--
-- TOC entry 2981 (class 2606 OID 56709)
-- Name: fk_99ggumymo969nk6c99ghsxjn7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_log
    ADD CONSTRAINT fk_99ggumymo969nk6c99ghsxjn7 FOREIGN KEY (persg) REFERENCES t501t(persg);


--
-- TOC entry 3053 (class 2606 OID 57069)
-- Name: fk_99v3gured86johyg8juhnqx5y; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0041
    ADD CONSTRAINT fk_99v3gured86johyg8juhnqx5y FOREIGN KEY (dar07) REFERENCES t548t(datar);


--
-- TOC entry 3047 (class 2606 OID 57039)
-- Name: fk_9ejjfoa8xnw1q2upta3j26346; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0041
    ADD CONSTRAINT fk_9ejjfoa8xnw1q2upta3j26346 FOREIGN KEY (dar01) REFERENCES t548t(datar);


--
-- TOC entry 3069 (class 2606 OID 57149)
-- Name: fk_9hd6qfavlb9q1ekrlivpjg899; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it2001
    ADD CONSTRAINT fk_9hd6qfavlb9q1ekrlivpjg899 FOREIGN KEY (awart) REFERENCES t554t(awart);


--
-- TOC entry 3103 (class 2606 OID 57319)
-- Name: fk_a1fhctnvdowm0uf6ijdyflg9c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_trxhdr
    ADD CONSTRAINT fk_a1fhctnvdowm0uf6ijdyflg9c FOREIGN KEY (bukrs) REFERENCES t001(bukrs);


--
-- TOC entry 3005 (class 2606 OID 56829)
-- Name: fk_ae1agdaijaatxmy2adsxhty7w; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0002
    ADD CONSTRAINT fk_ae1agdaijaatxmy2adsxhty7w FOREIGN KEY (art3, title3) REFERENCES t535n(art, title);


--
-- TOC entry 3078 (class 2606 OID 57194)
-- Name: fk_aihgtejrqrqoh5lrgob4vhf2o; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY position_relations
    ADD CONSTRAINT fk_aihgtejrqrqoh5lrgob4vhf2o FOREIGN KEY (position_relation_type_id) REFERENCES m_position_relation_types(id);


--
-- TOC entry 2963 (class 2606 OID 56619)
-- Name: fk_alocfeavvgxf1v26q4n9b83o8; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cfg_approval_category
    ADD CONSTRAINT fk_alocfeavvgxf1v26q4n9b83o8 FOREIGN KEY (m_workflow_id) REFERENCES m_workflows(id);


--
-- TOC entry 3033 (class 2606 OID 56969)
-- Name: fk_ap6ut6xqlitlmcor0c9orbmpa; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0022
    ADD CONSTRAINT fk_ap6ut6xqlitlmcor0c9orbmpa FOREIGN KEY (ausbi) REFERENCES t518b(ausbi);


--
-- TOC entry 3055 (class 2606 OID 57079)
-- Name: fk_aumlti1loju32fk3w2tutf3is; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0041
    ADD CONSTRAINT fk_aumlti1loju32fk3w2tutf3is FOREIGN KEY (dar09) REFERENCES t548t(datar);


--
-- TOC entry 3100 (class 2606 OID 57304)
-- Name: fk_bn8p6xld01wlsuve17mjxh6hi; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_medtype
    ADD CONSTRAINT fk_bn8p6xld01wlsuve17mjxh6hi FOREIGN KEY (bukrs) REFERENCES t001(bukrs);


--
-- TOC entry 3093 (class 2606 OID 57269)
-- Name: fk_bov4tel9hwnsgfg6ih100587m; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY workflow_steps
    ADD CONSTRAINT fk_bov4tel9hwnsgfg6ih100587m FOREIGN KEY (m_position_id) REFERENCES m_positions(id);


--
-- TOC entry 3044 (class 2606 OID 57024)
-- Name: fk_bsi6em32e49pcaxe8o326uvk0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0040
    ADD CONSTRAINT fk_bsi6em32e49pcaxe8o326uvk0 FOREIGN KEY (infty, leihg) REFERENCES t591s(infty, subty);


--
-- TOC entry 3073 (class 2606 OID 57169)
-- Name: fk_cdcqo9vmhoevljyk3krxhsbnf; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it2006
    ADD CONSTRAINT fk_cdcqo9vmhoevljyk3krxhsbnf FOREIGN KEY (ktart) REFERENCES t556b(ktart);


--
-- TOC entry 3003 (class 2606 OID 56819)
-- Name: fk_cp1tepmub79yqben2iykmgy22; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0002
    ADD CONSTRAINT fk_cp1tepmub79yqben2iykmgy22 FOREIGN KEY (gblnd) REFERENCES t005t(land1);


--
-- TOC entry 3025 (class 2606 OID 56929)
-- Name: fk_cp7v25k28rruv90xutqovj4rs; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0021
    ADD CONSTRAINT fk_cp7v25k28rruv90xutqovj4rs FOREIGN KEY (infty, attach1_subty) REFERENCES cfg_attachment(infty, subty);


--
-- TOC entry 3028 (class 2606 OID 56944)
-- Name: fk_cs5s01r6fq9mhfoxjso4o80ic; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0021
    ADD CONSTRAINT fk_cs5s01r6fq9mhfoxjso4o80ic FOREIGN KEY (fgbld) REFERENCES t005t(land1);


--
-- TOC entry 3099 (class 2606 OID 57299)
-- Name: fk_cv6tw8e7q9d7rh82ehchk1n8g; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_header_post
    ADD CONSTRAINT fk_cv6tw8e7q9d7rh82ehchk1n8g FOREIGN KEY (compcode) REFERENCES t001(bukrs);


--
-- TOC entry 3039 (class 2606 OID 56999)
-- Name: fk_cy3exv6yri3887bbnenh08x8a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0022
    ADD CONSTRAINT fk_cy3exv6yri3887bbnenh08x8a FOREIGN KEY (infty, subty) REFERENCES t591s(infty, subty);


--
-- TOC entry 3070 (class 2606 OID 57154)
-- Name: fk_dhn172jxrrxyrjb72f9ircy2q; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it2001
    ADD CONSTRAINT fk_dhn172jxrrxyrjb72f9ircy2q FOREIGN KEY (infty, subty) REFERENCES t591s(infty, subty);


--
-- TOC entry 3018 (class 2606 OID 56894)
-- Name: fk_e4lhohv8cpgt61ju8lymfppwx; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0009
    ADD CONSTRAINT fk_e4lhohv8cpgt61ju8lymfppwx FOREIGN KEY (infty, subty) REFERENCES t591s(infty, subty);


--
-- TOC entry 3092 (class 2606 OID 57264)
-- Name: fk_e6abt06oia318pc02drs30ocd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY user_roles
    ADD CONSTRAINT fk_e6abt06oia318pc02drs30ocd FOREIGN KEY (m_user_id) REFERENCES m_user(id);


--
-- TOC entry 3075 (class 2606 OID 57179)
-- Name: fk_eaixadvsxc6wed5a0rwh4p2tl; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY m_user
    ADD CONSTRAINT fk_eaixadvsxc6wed5a0rwh4p2tl FOREIGN KEY (begda, endda, pernr) REFERENCES it0001(begda, endda, pernr);


--
-- TOC entry 3037 (class 2606 OID 56989)
-- Name: fk_edskxuv0st3nls4mi36e390iy; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0022
    ADD CONSTRAINT fk_edskxuv0st3nls4mi36e390iy FOREIGN KEY (sltp1) REFERENCES t517x(faart);


--
-- TOC entry 2983 (class 2606 OID 56719)
-- Name: fk_ei6l3kkgbyy0dduni9wwo88ph; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_log
    ADD CONSTRAINT fk_ei6l3kkgbyy0dduni9wwo88ph FOREIGN KEY (endda_empjob, stell) REFERENCES t513s(endda, stell);


--
-- TOC entry 3019 (class 2606 OID 56899)
-- Name: fk_em0kml45x5l139we8s9mdotfr; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0009
    ADD CONSTRAINT fk_em0kml45x5l139we8s9mdotfr FOREIGN KEY (zlsch) REFERENCES t042z(zlsch);


--
-- TOC entry 3076 (class 2606 OID 57184)
-- Name: fk_eubjsyraqxxkhuwdj0blnmbhx; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY permission_accesstypes
    ADD CONSTRAINT fk_eubjsyraqxxkhuwdj0blnmbhx FOREIGN KEY (permission_id) REFERENCES m_permission(id);


--
-- TOC entry 3023 (class 2606 OID 56919)
-- Name: fk_f1bvxs57vchowbtddibbiybd6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0019
    ADD CONSTRAINT fk_f1bvxs57vchowbtddibbiybd6 FOREIGN KEY (infty, subty) REFERENCES t591s(infty, subty);


--
-- TOC entry 3001 (class 2606 OID 56809)
-- Name: fk_ffb9fy1e0253933n5od82ul70; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0002
    ADD CONSTRAINT fk_ffb9fy1e0253933n5od82ul70 FOREIGN KEY (infty, attach1_subty) REFERENCES cfg_attachment(infty, subty);


--
-- TOC entry 3030 (class 2606 OID 56954)
-- Name: fk_ffj7wp0xr1432gu05d01m5dil; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0021
    ADD CONSTRAINT fk_ffj7wp0xr1432gu05d01m5dil FOREIGN KEY (fnmzu, title) REFERENCES t535n(art, title);


--
-- TOC entry 3057 (class 2606 OID 57089)
-- Name: fk_fmaavu2yg4ggsknx73ylpomeb; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0041
    ADD CONSTRAINT fk_fmaavu2yg4ggsknx73ylpomeb FOREIGN KEY (dar11) REFERENCES t548t(datar);


--
-- TOC entry 3002 (class 2606 OID 56814)
-- Name: fk_fmbvoo9ubx6gv0jrs8niunfqf; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0002
    ADD CONSTRAINT fk_fmbvoo9ubx6gv0jrs8niunfqf FOREIGN KEY (famst) REFERENCES t502t(famst);


--
-- TOC entry 3051 (class 2606 OID 57059)
-- Name: fk_g18rbsotusgrmnh2e3n31798d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0041
    ADD CONSTRAINT fk_g18rbsotusgrmnh2e3n31798d FOREIGN KEY (dar05) REFERENCES t548t(datar);


--
-- TOC entry 2972 (class 2606 OID 56664)
-- Name: fk_g8cq2ed17mfjgi6l86tbtvpos; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001
    ADD CONSTRAINT fk_g8cq2ed17mfjgi6l86tbtvpos FOREIGN KEY (endda_empjob, stell) REFERENCES t513s(endda, stell);


--
-- TOC entry 2974 (class 2606 OID 56674)
-- Name: fk_gky0yv6u6jr7939q6jxrjshse; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001
    ADD CONSTRAINT fk_gky0yv6u6jr7939q6jxrjshse FOREIGN KEY (endda_emposition, plans) REFERENCES t528t(endda, plans);


--
-- TOC entry 2998 (class 2606 OID 56794)
-- Name: fk_gv1x4o1mb2fyf7ruc5c5t4qde; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_trx
    ADD CONSTRAINT fk_gv1x4o1mb2fyf7ruc5c5t4qde FOREIGN KEY (abkrs) REFERENCES t549t(abkrs);


--
-- TOC entry 3022 (class 2606 OID 56914)
-- Name: fk_gwkvqwyglw217cimkawktc7t6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0019
    ADD CONSTRAINT fk_gwkvqwyglw217cimkawktc7t6 FOREIGN KEY (infty, attach1_subty) REFERENCES cfg_attachment(infty, subty);


--
-- TOC entry 3054 (class 2606 OID 57074)
-- Name: fk_gxt621o666ajv114wff6ft2ij; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0041
    ADD CONSTRAINT fk_gxt621o666ajv114wff6ft2ij FOREIGN KEY (dar08) REFERENCES t548t(datar);


--
-- TOC entry 3046 (class 2606 OID 57034)
-- Name: fk_h50yla2i7rxvgq3kk0saiwuyq; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0040
    ADD CONSTRAINT fk_h50yla2i7rxvgq3kk0saiwuyq FOREIGN KEY (zeinh) REFERENCES t538t(zeinh);


--
-- TOC entry 2994 (class 2606 OID 56774)
-- Name: fk_hhbtpxpta6u97rpvx0ginkx7k; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_trx
    ADD CONSTRAINT fk_hhbtpxpta6u97rpvx0ginkx7k FOREIGN KEY (persk) REFERENCES t503k(mandt);


--
-- TOC entry 3004 (class 2606 OID 56824)
-- Name: fk_hlkmohh5f9sbk02yx32s7ocx5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0002
    ADD CONSTRAINT fk_hlkmohh5f9sbk02yx32s7ocx5 FOREIGN KEY (konfe) REFERENCES t516t(konfe);


--
-- TOC entry 2988 (class 2606 OID 56744)
-- Name: fk_i16i3fvltw9ag3nh14k82f7cr; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_log
    ADD CONSTRAINT fk_i16i3fvltw9ag3nh14k82f7cr FOREIGN KEY (btrtl, werks2) REFERENCES v_001p_all(btrtl, werks);


--
-- TOC entry 3043 (class 2606 OID 57019)
-- Name: fk_i189xm3yp1ybpqyhlor2g48t2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0023
    ADD CONSTRAINT fk_i189xm3yp1ybpqyhlor2g48t2 FOREIGN KEY (taete) REFERENCES t513c(taete);


--
-- TOC entry 3060 (class 2606 OID 57104)
-- Name: fk_i1olh7uckvf54yniyfup1mvnr; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0105
    ADD CONSTRAINT fk_i1olh7uckvf54yniyfup1mvnr FOREIGN KEY (infty, subty) REFERENCES t591s(infty, subty);


--
-- TOC entry 3011 (class 2606 OID 56859)
-- Name: fk_i5o3fgktah72qdukxauu9dh0w; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0006
    ADD CONSTRAINT fk_i5o3fgktah72qdukxauu9dh0w FOREIGN KEY (infty, subty) REFERENCES t591s(infty, subty);


--
-- TOC entry 2989 (class 2606 OID 56749)
-- Name: fk_idss8d8smxwjam711iu4am52j; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_trx
    ADD CONSTRAINT fk_idss8d8smxwjam711iu4am52j FOREIGN KEY (kokrs, kostl) REFERENCES cskt(kokrs, kostl);


--
-- TOC entry 2984 (class 2606 OID 56724)
-- Name: fk_ihmwhd5relmig5ao0701yrnek; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_log
    ADD CONSTRAINT fk_ihmwhd5relmig5ao0701yrnek FOREIGN KEY (endda_orgunit, orgeh) REFERENCES t527x(endda, orgeh);


--
-- TOC entry 3074 (class 2606 OID 57174)
-- Name: fk_ivpskquqnbnkq3hobq3wrqa13; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it2006
    ADD CONSTRAINT fk_ivpskquqnbnkq3hobq3wrqa13 FOREIGN KEY (infty, subty) REFERENCES t591s(infty, subty);


--
-- TOC entry 3081 (class 2606 OID 57209)
-- Name: fk_j05kh53gilmk76t5su2skt38v; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ptrv_head
    ADD CONSTRAINT fk_j05kh53gilmk76t5su2skt38v FOREIGN KEY (morei) REFERENCES t702n(morei);


--
-- TOC entry 3020 (class 2606 OID 56904)
-- Name: fk_j2f7sylvtf34uhit81soklgku; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0014
    ADD CONSTRAINT fk_j2f7sylvtf34uhit81soklgku FOREIGN KEY (lgart) REFERENCES t512t(lgart);


--
-- TOC entry 3085 (class 2606 OID 57229)
-- Name: fk_jr0sjfymsrwed727ytxr7rpc1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY t706b1
    ADD CONSTRAINT fk_jr0sjfymsrwed727ytxr7rpc1 FOREIGN KEY (spkzl) REFERENCES sh_t706b1(spkzl);


--
-- TOC entry 3006 (class 2606 OID 56834)
-- Name: fk_jx5qmhe9dhp3xd9khv8r8whue; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0002
    ADD CONSTRAINT fk_jx5qmhe9dhp3xd9khv8r8whue FOREIGN KEY (natio) REFERENCES t005t(land1);


--
-- TOC entry 3036 (class 2606 OID 56984)
-- Name: fk_kg1sp9va1ao5ci4juv97tujnl; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0022
    ADD CONSTRAINT fk_kg1sp9va1ao5ci4juv97tujnl FOREIGN KEY (slart) REFERENCES t517t(slart);


--
-- TOC entry 3015 (class 2606 OID 56879)
-- Name: fk_kjn6th3elp4tbtvpw9we8cowb; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0009
    ADD CONSTRAINT fk_kjn6th3elp4tbtvpw9we8cowb FOREIGN KEY (bankl) REFERENCES bnka(bankl);


--
-- TOC entry 2996 (class 2606 OID 56784)
-- Name: fk_krew0lhn1voib7htsqtdbsp22; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_trx
    ADD CONSTRAINT fk_krew0lhn1voib7htsqtdbsp22 FOREIGN KEY (endda_orgunit, orgeh) REFERENCES t527x(endda, orgeh);


--
-- TOC entry 3079 (class 2606 OID 57199)
-- Name: fk_l64uk8c0y6vgta8tgb046mhfm; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY position_relations
    ADD CONSTRAINT fk_l64uk8c0y6vgta8tgb046mhfm FOREIGN KEY (to_position_id) REFERENCES m_positions(id);


--
-- TOC entry 3061 (class 2606 OID 57109)
-- Name: fk_lcvewe47cy8681tdybakrjxk0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0105
    ADD CONSTRAINT fk_lcvewe47cy8681tdybakrjxk0 FOREIGN KEY (comminfty, usrty) REFERENCES t591s(infty, subty);


--
-- TOC entry 3021 (class 2606 OID 56909)
-- Name: fk_le500mkuxgjrrxnvp1tyydxly; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0014
    ADD CONSTRAINT fk_le500mkuxgjrrxnvp1tyydxly FOREIGN KEY (infty, subty) REFERENCES t591s(infty, subty);


--
-- TOC entry 3088 (class 2606 OID 57244)
-- Name: fk_le8qlorkagy7wx8l39jix4l8r; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY trx_approval_event_log
    ADD CONSTRAINT fk_le8qlorkagy7wx8l39jix4l8r FOREIGN KEY (cfg_entity_id) REFERENCES cfg_entity(id);


--
-- TOC entry 3026 (class 2606 OID 56934)
-- Name: fk_lfir8crfhdahwrbxen4fytyyf; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0021
    ADD CONSTRAINT fk_lfir8crfhdahwrbxen4fytyyf FOREIGN KEY (infty2, famsa) REFERENCES t591s(infty, subty);


--
-- TOC entry 2982 (class 2606 OID 56714)
-- Name: fk_lqos0onuu65oudxbyqo50m708; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_log
    ADD CONSTRAINT fk_lqos0onuu65oudxbyqo50m708 FOREIGN KEY (persk) REFERENCES t503k(mandt);


--
-- TOC entry 3029 (class 2606 OID 56949)
-- Name: fk_m5o0hjvet4d68pjabiosqv52t; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0021
    ADD CONSTRAINT fk_m5o0hjvet4d68pjabiosqv52t FOREIGN KEY (infty, subty) REFERENCES t591s(infty, subty);


--
-- TOC entry 2985 (class 2606 OID 56729)
-- Name: fk_m6don54gh465irvf55c64oi3w; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_log
    ADD CONSTRAINT fk_m6don54gh465irvf55c64oi3w FOREIGN KEY (endda_emposition, plans) REFERENCES t528t(endda, plans);


--
-- TOC entry 3009 (class 2606 OID 56849)
-- Name: fk_momsppuyubem3bj0654g4jeh7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0002
    ADD CONSTRAINT fk_momsppuyubem3bj0654g4jeh7 FOREIGN KEY (art, title) REFERENCES t535n(art, title);


--
-- TOC entry 2976 (class 2606 OID 56684)
-- Name: fk_mrl9xlyudi2g19nfu3oagh2bg; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001
    ADD CONSTRAINT fk_mrl9xlyudi2g19nfu3oagh2bg FOREIGN KEY (btrtl, werks2) REFERENCES v_001p_all(btrtl, werks);


--
-- TOC entry 3080 (class 2606 OID 57204)
-- Name: fk_mw45bjpcabeegsuhlpgg5efuq; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ptrv_head
    ADD CONSTRAINT fk_mw45bjpcabeegsuhlpgg5efuq FOREIGN KEY (molga) REFERENCES t500l(molga);


--
-- TOC entry 3064 (class 2606 OID 57124)
-- Name: fk_n1d5o01sbypyftrvnn2ljenlg; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0185
    ADD CONSTRAINT fk_n1d5o01sbypyftrvnn2ljenlg FOREIGN KEY (ictyp) REFERENCES t5r06(ictyp);


--
-- TOC entry 2971 (class 2606 OID 56659)
-- Name: fk_n3fft8hpbclrt5mhyjdvvh9w; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001
    ADD CONSTRAINT fk_n3fft8hpbclrt5mhyjdvvh9w FOREIGN KEY (persk) REFERENCES t503k(mandt);


--
-- TOC entry 3050 (class 2606 OID 57054)
-- Name: fk_n4v9t1j9qlk4ms2ru1k93rgq7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0041
    ADD CONSTRAINT fk_n4v9t1j9qlk4ms2ru1k93rgq7 FOREIGN KEY (dar04) REFERENCES t548t(datar);


--
-- TOC entry 2970 (class 2606 OID 56654)
-- Name: fk_n68tfevsond7ci8jyevgqf1gp; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001
    ADD CONSTRAINT fk_n68tfevsond7ci8jyevgqf1gp FOREIGN KEY (persg) REFERENCES t501t(persg);


--
-- TOC entry 3035 (class 2606 OID 56979)
-- Name: fk_n8dob7mkdxsf7g7fo4l75xqjw; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0022
    ADD CONSTRAINT fk_n8dob7mkdxsf7g7fo4l75xqjw FOREIGN KEY (sland) REFERENCES t005t(land1);


--
-- TOC entry 2965 (class 2606 OID 56629)
-- Name: fk_ncyfdbtcn7eby00yrvvra2k5c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cfg_entity_approval
    ADD CONSTRAINT fk_ncyfdbtcn7eby00yrvvra2k5c FOREIGN KEY (cfg_entity_id) REFERENCES cfg_entity(id);


--
-- TOC entry 3049 (class 2606 OID 57049)
-- Name: fk_nuhelkeohw9uvx1ifda1ag8tb; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0041
    ADD CONSTRAINT fk_nuhelkeohw9uvx1ifda1ag8tb FOREIGN KEY (dar03) REFERENCES t548t(datar);


--
-- TOC entry 3031 (class 2606 OID 56959)
-- Name: fk_nwml27qgr606x6pg8j24bktdj; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0022
    ADD CONSTRAINT fk_nwml27qgr606x6pg8j24bktdj FOREIGN KEY (anzeh) REFERENCES t538t(zeinh);


--
-- TOC entry 3071 (class 2606 OID 57159)
-- Name: fk_ny8n21n0vd0ufr24i4h7xk5i; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it2002
    ADD CONSTRAINT fk_ny8n21n0vd0ufr24i4h7xk5i FOREIGN KEY (awart) REFERENCES t554t(awart);


--
-- TOC entry 3095 (class 2606 OID 57279)
-- Name: fk_o3y5ns8blkt0r0bpk94rktu1r; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_costype
    ADD CONSTRAINT fk_o3y5ns8blkt0r0bpk94rktu1r FOREIGN KEY (bukrs) REFERENCES t001(bukrs);


--
-- TOC entry 3077 (class 2606 OID 57189)
-- Name: fk_og43buykwoccbpbm4ewk33b2v; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY position_relations
    ADD CONSTRAINT fk_og43buykwoccbpbm4ewk33b2v FOREIGN KEY (from_position_id) REFERENCES m_positions(id);


--
-- TOC entry 2995 (class 2606 OID 56779)
-- Name: fk_ogikk9dcvufnn9joxqbl0tmlj; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_trx
    ADD CONSTRAINT fk_ogikk9dcvufnn9joxqbl0tmlj FOREIGN KEY (endda_empjob, stell) REFERENCES t513s(endda, stell);


--
-- TOC entry 2966 (class 2606 OID 56634)
-- Name: fk_oo6d0u9owaegbduwh99sblxg4; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001
    ADD CONSTRAINT fk_oo6d0u9owaegbduwh99sblxg4 FOREIGN KEY (kokrs, kostl) REFERENCES cskt(kokrs, kostl);


--
-- TOC entry 2977 (class 2606 OID 56689)
-- Name: fk_oq1hcsup880kw9fftgf2v9p7f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_log
    ADD CONSTRAINT fk_oq1hcsup880kw9fftgf2v9p7f FOREIGN KEY (kokrs, kostl) REFERENCES cskt(kokrs, kostl);


--
-- TOC entry 3067 (class 2606 OID 57139)
-- Name: fk_oyjbh232cflvosd47yby3agda; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0241
    ADD CONSTRAINT fk_oyjbh232cflvosd47yby3agda FOREIGN KEY (infty, attach1_subty) REFERENCES cfg_attachment(infty, subty);


--
-- TOC entry 2978 (class 2606 OID 56694)
-- Name: fk_p1cm3csb173lgax0ia5f6kbp0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_log
    ADD CONSTRAINT fk_p1cm3csb173lgax0ia5f6kbp0 FOREIGN KEY (m_job_id) REFERENCES m_jobs(id);


--
-- TOC entry 3042 (class 2606 OID 57014)
-- Name: fk_pev3urae2o8v93c2dsa7391b9; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0023
    ADD CONSTRAINT fk_pev3urae2o8v93c2dsa7391b9 FOREIGN KEY (land1) REFERENCES t005t(land1);


--
-- TOC entry 3083 (class 2606 OID 57219)
-- Name: fk_ph05cktawujnrqy8qhiq0erdh; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY role_permissions
    ADD CONSTRAINT fk_ph05cktawujnrqy8qhiq0erdh FOREIGN KEY (m_role_id) REFERENCES m_role(id);


--
-- TOC entry 3007 (class 2606 OID 56839)
-- Name: fk_pl8651fj354ta3hx0vi7ll6dj; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0002
    ADD CONSTRAINT fk_pl8651fj354ta3hx0vi7ll6dj FOREIGN KEY (sprsl) REFERENCES t002t(sprsl);


--
-- TOC entry 3098 (class 2606 OID 57294)
-- Name: fk_q1d58aq9bo04wqpo1ejh4yp9p; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_empquotastf
    ADD CONSTRAINT fk_q1d58aq9bo04wqpo1ejh4yp9p FOREIGN KEY (bukrs) REFERENCES t001(bukrs);


--
-- TOC entry 3024 (class 2606 OID 56924)
-- Name: fk_q52gf7d49mwait5yuywjx50a8; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0019
    ADD CONSTRAINT fk_q52gf7d49mwait5yuywjx50a8 FOREIGN KEY (tmart) REFERENCES t531s(tmart);


--
-- TOC entry 2967 (class 2606 OID 56639)
-- Name: fk_q79guohp8lbloq6oqjivs0idt; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001
    ADD CONSTRAINT fk_q79guohp8lbloq6oqjivs0idt FOREIGN KEY (m_job_id) REFERENCES m_jobs(id);


--
-- TOC entry 3008 (class 2606 OID 56844)
-- Name: fk_qj74t93iv0ui5hsyw11qsxcn3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0002
    ADD CONSTRAINT fk_qj74t93iv0ui5hsyw11qsxcn3 FOREIGN KEY (art2, title2) REFERENCES t535n(art, title);


--
-- TOC entry 3010 (class 2606 OID 56854)
-- Name: fk_qsvjgbv1k56w7javkxnt7lgyj; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0006
    ADD CONSTRAINT fk_qsvjgbv1k56w7javkxnt7lgyj FOREIGN KEY (infty, attach1_subty) REFERENCES cfg_attachment(infty, subty);


--
-- TOC entry 3090 (class 2606 OID 57254)
-- Name: fk_r0q17j6rhxb60twtrygb92clu; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY trx_event_log
    ADD CONSTRAINT fk_r0q17j6rhxb60twtrygb92clu FOREIGN KEY (cfg_entity_id) REFERENCES cfg_entity(id);


--
-- TOC entry 2993 (class 2606 OID 56769)
-- Name: fk_r1e1eqgig0xjwxk7wtq4rg09f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001_trx
    ADD CONSTRAINT fk_r1e1eqgig0xjwxk7wtq4rg09f FOREIGN KEY (persg) REFERENCES t501t(persg);


--
-- TOC entry 2975 (class 2606 OID 56679)
-- Name: fk_r6h1y4iu2kg6dk5sc9u94h2pg; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0001
    ADD CONSTRAINT fk_r6h1y4iu2kg6dk5sc9u94h2pg FOREIGN KEY (abkrs) REFERENCES t549t(abkrs);


--
-- TOC entry 3096 (class 2606 OID 57284)
-- Name: fk_rjkkxvwuiaovumdsedyxq2wrf; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY zmed_empquota
    ADD CONSTRAINT fk_rjkkxvwuiaovumdsedyxq2wrf FOREIGN KEY (bukrs) REFERENCES t001(bukrs);


--
-- TOC entry 3013 (class 2606 OID 56869)
-- Name: fk_rpc2c50j3048ohxtxmprugl1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0006
    ADD CONSTRAINT fk_rpc2c50j3048ohxtxmprugl1 FOREIGN KEY (state) REFERENCES t005u(bland);


--
-- TOC entry 3052 (class 2606 OID 57064)
-- Name: fk_rpg75ey48qsy6fohymyl97b53; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0041
    ADD CONSTRAINT fk_rpg75ey48qsy6fohymyl97b53 FOREIGN KEY (dar06) REFERENCES t548t(datar);


--
-- TOC entry 3017 (class 2606 OID 56889)
-- Name: fk_sbjdo5i2e05ovlc6wcgaj8nah; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0009
    ADD CONSTRAINT fk_sbjdo5i2e05ovlc6wcgaj8nah FOREIGN KEY (infty, bnksa) REFERENCES t591s(infty, subty);


--
-- TOC entry 3056 (class 2606 OID 57084)
-- Name: fk_shtyqtsuacchaxn7e57likd2d; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0041
    ADD CONSTRAINT fk_shtyqtsuacchaxn7e57likd2d FOREIGN KEY (dar10) REFERENCES t548t(datar);


--
-- TOC entry 3045 (class 2606 OID 57029)
-- Name: fk_snihq68pgnaxrexp09yg02m6k; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0040
    ADD CONSTRAINT fk_snihq68pgnaxrexp09yg02m6k FOREIGN KEY (infty, subty) REFERENCES t591s(infty, subty);


--
-- TOC entry 3032 (class 2606 OID 56964)
-- Name: fk_ssc5v5goejm9je7dgns5rnrc7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0022
    ADD CONSTRAINT fk_ssc5v5goejm9je7dgns5rnrc7 FOREIGN KEY (infty, attach1_subty) REFERENCES cfg_attachment(infty, subty);


--
-- TOC entry 3066 (class 2606 OID 57134)
-- Name: fk_ssn5v5rtolchsg4s3k6gh8x70; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0185
    ADD CONSTRAINT fk_ssn5v5rtolchsg4s3k6gh8x70 FOREIGN KEY (infty, subty) REFERENCES t591s(infty, subty);


--
-- TOC entry 3065 (class 2606 OID 57129)
-- Name: fk_thlgpfi1shup2ywgubmnh9d8a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0185
    ADD CONSTRAINT fk_thlgpfi1shup2ywgubmnh9d8a FOREIGN KEY (iscot) REFERENCES t005t(land1);


--
-- TOC entry 3014 (class 2606 OID 56874)
-- Name: fk_u6nfq1dtijiy0nxq2hm0vnpa; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY it0009
    ADD CONSTRAINT fk_u6nfq1dtijiy0nxq2hm0vnpa FOREIGN KEY (infty, attach1_subty) REFERENCES cfg_attachment(infty, subty);


--
-- TOC entry 3324 (class 0 OID 0)
-- Dependencies: 8
-- Name: public; Type: ACL; Schema: -; Owner: -
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-08-28 22:47:07 WIB

--
-- PostgreSQL database dump complete
--

