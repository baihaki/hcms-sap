ALTER TABLE cfg_entity_approval DROP CONSTRAINT idx_cfg_entity_approval;

ALTER TABLE cfg_entity_approval ADD CONSTRAINT idx_cfg_entity_approval UNIQUE (cfg_entity_id, operation_type);
