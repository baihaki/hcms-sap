CREATE TABLE IT0105 (
  pernr bigint not null,
  infty character varying(10) not null,
  subty character varying(4) not null,
  endda date not null,
  begda date not null,
  seqnr bigint,
  aedtm timestamp with time zone not null default timezone('utc'::text, now()),
  uname character varying(255),
  usrty character varying(4),
  usrid_long character varying(255)
);

COMMENT ON TABLE IT0105 IS 'Master data transaction for Communication';

COMMENT ON COLUMN IT0105.pernr IS 'Employee SSN/ID';

COMMENT ON COLUMN IT0105.infty IS 'Infotype';

COMMENT ON COLUMN IT0105.subty IS 'Subtype';

COMMENT ON COLUMN IT0105.endda IS 'End Date';

COMMENT ON COLUMN IT0105.begda IS 'Begin Date';

COMMENT ON COLUMN IT0105.seqnr IS 'Infotype Record No.';

COMMENT ON COLUMN IT0105.aedtm IS 'Changed On';

COMMENT ON COLUMN IT0105.uname IS 'Changed By';

COMMENT ON COLUMN IT0105.usrty IS 'Communication Type';

COMMENT ON COLUMN IT0105.usrid_long IS 'Long ID/Number';
