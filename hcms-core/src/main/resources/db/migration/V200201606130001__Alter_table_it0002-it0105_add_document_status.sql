﻿ALTER TABLE it0002
    ADD COLUMN document_status character varying(25) NOT NULL DEFAULT 'DRAFT'::character varying;
ALTER TABLE it0002
    ADD CONSTRAINT it0002_document_status_check CHECK (document_status::text = ANY
        (ARRAY['DRAFT'::character varying, 'PUBLISHED'::character varying]::text[]));
COMMENT ON COLUMN it0002.document_status IS 'Status of Document (i.e. DRAFT || PUBLISHED)';

ALTER TABLE it0105
    ADD COLUMN document_status character varying(25) NOT NULL DEFAULT 'DRAFT'::character varying;
ALTER TABLE it0105
    ADD CONSTRAINT it0105_document_status_check CHECK (document_status::text = ANY
        (ARRAY['DRAFT'::character varying, 'PUBLISHED'::character varying]::text[]));
COMMENT ON COLUMN it0105.document_status IS 'Status of Document (i.e. DRAFT || PUBLISHED)';