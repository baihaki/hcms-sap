﻿ALTER TABLE it0002 ADD COLUMN infty character varying(10) NOT NULL DEFAULT '0002'::character varying;
ALTER TABLE it0002 ADD COLUMN attach1_subty character varying(4) NOT NULL DEFAULT 'ALL'::character varying;
ALTER TABLE it0002 ADD COLUMN attach1_path character varying(255);
ALTER TABLE it0002 ADD CONSTRAINT it0002_attach1_fkey FOREIGN KEY (infty, attach1_subty)
   REFERENCES cfg_attachment (infty, subty) ON UPDATE CASCADE ON DELETE RESTRICT;