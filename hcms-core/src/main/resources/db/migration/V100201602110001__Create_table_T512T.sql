CREATE TABLE T512T (
  LGART character varying(15) not null,
  LGTXT character varying(255) not null
);

COMMENT ON TABLE T512T IS 'Master data table for Wage Type';

COMMENT ON COLUMN T512T.LGART IS 'Wage Type Code';

COMMENT ON COLUMN T512T.LGTXT IS 'Wage Type Long Text / Description';

ALTER TABLE T512T ADD PRIMARY KEY (LGART);
