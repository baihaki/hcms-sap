﻿ALTER TABLE ptrv_head
  ADD COLUMN last_prev_month numeric(13,2);
ALTER TABLE ptrv_head
  ADD COLUMN last_this_month numeric(13,2);