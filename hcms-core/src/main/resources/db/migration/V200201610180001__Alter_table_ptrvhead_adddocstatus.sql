﻿ALTER TABLE ptrv_head
  ADD COLUMN payment_status character varying(1);
ALTER TABLE ptrv_head
  ADD COLUMN payment_date date;
ALTER TABLE ptrv_head
  ADD COLUMN docno character varying(10);
ALTER TABLE ptrv_head
  ADD COLUMN last_sync timestamp with time zone;
  
ALTER TABLE ptrv_head
  ADD COLUMN payment_status2 character varying(1);
ALTER TABLE ptrv_head
  ADD COLUMN docno2 character varying(10);