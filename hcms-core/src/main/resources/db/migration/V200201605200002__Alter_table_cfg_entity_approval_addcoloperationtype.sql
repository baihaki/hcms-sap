ALTER TABLE cfg_entity_approval ADD COLUMN operation_type character varying(25) NOT NULL DEFAULT 'CREATE';

ALTER TABLE cfg_entity_approval ADD CONSTRAINT cfg_entity_approval_optype_check CHECK (operation_type IN ('CREATE', 'UPDATE', 'DELETE'));
