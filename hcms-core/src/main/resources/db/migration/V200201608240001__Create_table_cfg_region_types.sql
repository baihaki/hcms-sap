CREATE TABLE cfg_region_types (
  id character varying(255) not null default uuid_generate_V1mc(),
  opt_lock bigint not null default 0,
  ident_name character varying(100) NOT NULL,
  name character varying(100) NOT NULL,
  description text,
  system boolean default true,
  created_at timestamp with time zone not null default timezone('utc'::text, now()),
  created_by_id character varying(255) not null,
  updated_at timestamp with time zone not null default timezone('utc'::text, now()),
  updated_by_id character varying(255) not null,
  deleted_at timestamp with time zone,
  deleted_by_id character varying(255)
);

COMMENT ON TABLE cfg_region_types IS 'Configuration Table for Region Types';

COMMENT ON COLUMN cfg_region_types.ident_name IS 'Region Type unique name (used as system identifier)';

COMMENT ON COLUMN cfg_region_types.name IS 'Region Type name (human friendly name)';

COMMENT ON COLUMN cfg_region_types.system IS 'flag to indicate whether this record was generated by the system or not';

ALTER TABLE cfg_region_types ADD PRIMARY KEY(id);

ALTER TABLE cfg_region_types ADD UNIQUE(ident_name);
