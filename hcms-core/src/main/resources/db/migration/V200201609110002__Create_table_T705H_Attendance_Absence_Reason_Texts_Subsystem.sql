CREATE TABLE T705H
(
   BUKRS character varying(4),
   PINCO character varying(4), 
   GTEXT character varying(30),
   PRIMARY KEY (PINCO)
);

COMMENT ON COLUMN T705H.BUKRS IS 'Company Code';
COMMENT ON COLUMN T705H.PINCO IS 'Att./absence reason';
COMMENT ON COLUMN T705H.GTEXT IS 'Att./abs.reason text';
COMMENT ON TABLE T705H IS 'Attendance/Absence Reason Texts for Subsystem';

INSERT INTO T705H VALUES('9900', 'H1', 'Cuti Haid');
INSERT INTO T705H VALUES('9900', 'H2', 'Cuti Hamil');
INSERT INTO T705H VALUES('9900', 'L1', 'Terlambat Masuk Kerja');
INSERT INTO T705H VALUES('9900', 'M1', 'Tdk Msk kerja tanpa Keterangan');
INSERT INTO T705H VALUES('9900', 'M2', 'Tdk Msk kerja tanpa Keterangan');
INSERT INTO T705H VALUES('9900', 'P1', 'Izin Atasan - 1/2 Hari Kerja');
INSERT INTO T705H VALUES('9900', 'P2', 'Izin Atasan - 1 Hari Kerja');
INSERT INTO T705H VALUES('9900', 'S0', 'Surat Keterangan Dokter 1/2 HK');
INSERT INTO T705H VALUES('9900', 'S1', 'Surat Keterangan Dokter');
INSERT INTO T705H VALUES('9900', 'S1_2', 'Surat Keterangan Dokter > 2 HK');
INSERT INTO T705H VALUES('9900', 'S2', 'Surat Keterangan Dokter u/.Acc');
INSERT INTO T705H VALUES('9900', 'S3', 'Surat Ket. Dokter Opname');

