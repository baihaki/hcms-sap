CREATE TABLE TRX_APPROVAL_EVENT_LOG (
  id character varying(255) NOT NULL,
  trx_event_log_id character varying(255) NOT NULL,
  approved_by_ssn bigint not null,
  approved_by_position_id character varying(255) NOT NULL,
  created_at timestamp with time zone not null default timezone('utc'::text, now()),
  created_by_id character varying(255) NOT NULL
);

COMMENT ON TABLE TRX_APPROVAL_EVENT_LOG IS 'Approval Event Log Transactions';
COMMENT ON COLUMN TRX_APPROVAL_EVENT_LOG.trx_event_log_id IS 'FK to table TRX_EVENT_LOG';
COMMENT ON COLUMN TRX_APPROVAL_EVENT_LOG.approved_by_ssn IS 'Employee SSN (pernr)';
COMMENT ON COLUMN TRX_APPROVAL_EVENT_LOG.approved_by_position_id IS 'FK to table m_positions';

ALTER TABLE TRX_APPROVAL_EVENT_LOG ADD PRIMARY KEY (id);
ALTER TABLE TRX_APPROVAL_EVENT_LOG ADD FOREIGN KEY (trx_event_log_id) REFERENCES TRX_EVENT_LOG (id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE TRX_APPROVAL_EVENT_LOG ADD FOREIGN KEY (approved_by_position_id) REFERENCES m_positions (id) ON UPDATE CASCADE ON DELETE RESTRICT;
