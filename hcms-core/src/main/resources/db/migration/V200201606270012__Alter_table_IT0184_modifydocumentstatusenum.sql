ALTER TABLE IT0184 DROP CONSTRAINT it0184_document_status_check;

UPDATE IT0184 SET document_status = 'INITIAL_DRAFT';

ALTER TABLE IT0184 ADD CONSTRAINT it0184_document_status_check CHECK (document_status IN ('INITIAL_DRAFT', 'DRAFT_REJECTED', 'UPDATED_DRAFT', 'UPDATE_IN_PROGRESS', 'UPDATE_REJECTED', 'PUBLISHED'));

ALTER TABLE IT0184 ALTER COLUMN document_status SET DEFAULT 'INITIAL_DRAFT';
