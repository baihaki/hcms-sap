ALTER TABLE trx_approval_event_log RENAME COLUMN  approved_by_ssn TO performed_by_ssn;

ALTER TABLE trx_approval_event_log RENAME COLUMN approved_by_position_id TO performed_by_position_id;
