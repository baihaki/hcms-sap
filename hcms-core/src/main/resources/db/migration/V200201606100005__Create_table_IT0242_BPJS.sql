﻿CREATE TABLE it0242
(
  pernr bigint NOT NULL,
  endda date NOT NULL,
  begda date NOT NULL,
  seqnr bigint DEFAULT 0,
  aedtm timestamp with time zone NOT NULL DEFAULT timezone('utc'::text, now()),
  uname character varying(255),
  jamid character varying(50),
  marst character(1),
  document_status character varying(25) NOT NULL DEFAULT 'DRAFT'::character varying,
  CONSTRAINT it0242_pkey PRIMARY KEY (pernr, endda, begda),
  CONSTRAINT it0242_document_status_check CHECK (document_status::text = ANY (ARRAY['DRAFT'::character varying::text, 'REJECTED'::character varying::text, 'APPROVED'::character varying::text, 'PUBLISHED'::character varying::text]))
);

COMMENT ON TABLE it0242
  IS 'Master data for BPJS';
COMMENT ON COLUMN it0242.pernr IS 'Employee SSN/ID';
COMMENT ON COLUMN it0242.endda IS 'End Date';
COMMENT ON COLUMN it0242.begda IS 'Begin Date';
COMMENT ON COLUMN it0242.seqnr IS 'Infotype Record No.';
COMMENT ON COLUMN it0242.aedtm IS 'Changed On';
COMMENT ON COLUMN it0242.uname IS 'Changed By';
COMMENT ON COLUMN it0242.jamid IS 'Jamsostek Identification Number';
COMMENT ON COLUMN it0242.marst IS 'Marital Status';
COMMENT ON COLUMN it0242.document_status IS 'Status of Document (i.e. DRAFT || REJECTED || APPROVED || PUBLISHED)';

INSERT INTO cfg_attachment VALUES('NPWP', 'Kartu NPWP', '0241');
INSERT INTO cfg_attachment VALUES('BPJS', 'Kartu BPJS', '0242');