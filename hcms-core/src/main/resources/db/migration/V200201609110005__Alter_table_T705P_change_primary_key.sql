ALTER TABLE teven DROP CONSTRAINT teven_satza_fkey;

-- TABLE T705P 
-- ADD COLUMN and set initial value '01'
ALTER TABLE t705p ADD COLUMN pmbde character varying(2);
COMMENT ON COLUMN t705p.pmbde IS 'Time Event Group';
UPDATE t705p SET pmbde = '01';
ALTER TABLE t705p ALTER COLUMN pmbde SET NOT NULL;

-- CHANGE primary key(pmbde, satza)
ALTER TABLE t705p DROP CONSTRAINT t705p_pkey; -- Drop old Primary Key
-- CHANGE new primary key
ALTER TABLE t705p ADD PRIMARY KEY (pmbde, satza);


-- TABLE TEVEN - update Foreign Key After Change Primary Key
-- ADD NEW COLUMN pmbde
ALTER TABLE teven ADD COLUMN pmbde character varying(2);
COMMENT ON COLUMN teven.pmbde IS 'Time Event Group';
ALTER TABLE teven ADD FOREIGN KEY (pmbde, satza) REFERENCES t705p (pmbde, satza) ON UPDATE NO ACTION ON DELETE RESTRICT;

UPDATE teven SET pmbde = temp.pmbde FROM (SELECT * FROM t705p) temp WHERE teven.satza = temp.satza;