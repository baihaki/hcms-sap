﻿ALTER TABLE zmed_trxhdr
  ADD COLUMN payment_status character varying(1);
ALTER TABLE zmed_trxhdr
  ADD COLUMN payment_date date;
ALTER TABLE zmed_trxhdr
  ADD COLUMN docno character varying(10);
ALTER TABLE zmed_trxhdr
  ADD COLUMN last_sync timestamp with time zone;