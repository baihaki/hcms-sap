ALTER TABLE ZMED_COSTYPE
  ADD CONSTRAINT zmed_costype_pkey PRIMARY KEY (bukrs, kodemed, kodecos, datab, datbi);
ALTER TABLE ZMED_COSTYPE
  ADD CONSTRAINT zmed_costype_bukrs_fkey FOREIGN KEY (bukrs)
      REFERENCES public.t001 (bukrs) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ZMED_EMPQUOTA
  ADD CONSTRAINT zmed_empquota_pkey PRIMARY KEY (bukrs, persg, persk, fatxt, kodequo, datab, datbi);
ALTER TABLE ZMED_EMPQUOTA
  ADD CONSTRAINT zmed_empquota_bukrs_fkey FOREIGN KEY (bukrs)
      REFERENCES public.t001 (bukrs) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;
	  
ALTER TABLE ZMED_EMPQUOTASM
  ADD CONSTRAINT zmed_empquotasm_pkey PRIMARY KEY (pernr, bukrs, persg, persk, fatxt, kodequo, datab, datbi);
ALTER TABLE ZMED_EMPQUOTASM
  ADD CONSTRAINT zmed_empquotasm_bukrs_fkey FOREIGN KEY (bukrs)
      REFERENCES public.t001 (bukrs) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ZMED_EMPQUOTASTF
  ADD CONSTRAINT zmed_empquotastf_pkey PRIMARY KEY (stell, bukrs, persg, persk, kodequo, datab, datbi);
ALTER TABLE ZMED_EMPQUOTASTF
  ADD CONSTRAINT zmed_empquotastf_bukrs_fkey FOREIGN KEY (bukrs)
      REFERENCES public.t001 (bukrs) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;
	  
ALTER TABLE ZMED_HEADER_POST
  ADD CONSTRAINT zmed_header_post_pkey PRIMARY KEY (obj_key, obj_sys, obj_type);
 ALTER TABLE ZMED_HEADER_POST
  ADD CONSTRAINT zmed_header_post_bukrs_fkey FOREIGN KEY (comp_code)
      REFERENCES public.t001 (bukrs) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;
	  
ALTER TABLE ZMED_MEDTYPE
  ADD CONSTRAINT zmed_medtype_pkey PRIMARY KEY (bukrs, kodemed, datab, datbi);
ALTER TABLE ZMED_MEDTYPE
  ADD CONSTRAINT zmed_medtype_bukrs_fkey FOREIGN KEY (bukrs)
      REFERENCES public.t001 (bukrs) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;
	  
ALTER TABLE ZMED_QUOTAUSED
  ADD CONSTRAINT zmed_quotaused_pkey PRIMARY KEY (gjahr, bukrs, pernr, kodequo);
  
ALTER TABLE ZMED_QUOTYPE
  ADD CONSTRAINT zmed_quotype_pkey PRIMARY KEY (datbi, bukrs, datab, kodequo);
ALTER TABLE ZMED_QUOTYPE
  ADD CONSTRAINT zmed_quotype_bukrs_fkey FOREIGN KEY (bukrs)
      REFERENCES public.t001 (bukrs) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ZMED_TRXHDR
  ADD CONSTRAINT zmed_trxhdr_pkey PRIMARY KEY (bukrs, zclmno);
ALTER TABLE ZMED_TRXHDR
  ADD CONSTRAINT zmed_trxhdr_bukrs_fkey FOREIGN KEY (bukrs)
      REFERENCES public.t001 (bukrs) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;
	  
ALTER TABLE ZMED_TRXITM
  ADD CONSTRAINT zmed_trxitm_pkey PRIMARY KEY (itmno);
ALTER TABLE ZMED_TRXITM
  ADD CONSTRAINT zmed_trxitm_bukrs_fkey FOREIGN KEY (bukrs)
      REFERENCES public.t001 (bukrs) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT;