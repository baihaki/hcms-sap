﻿ALTER TABLE IT2001
  ADD COLUMN created_by character varying(255);
ALTER TABLE IT2001
  ADD COLUMN created_at timestamp with time zone;
ALTER TABLE IT2001
  ADD COLUMN approved1_by character varying(255);
ALTER TABLE IT2001
  ADD COLUMN approved1_at timestamp with time zone;
ALTER TABLE IT2001
  ADD COLUMN approved2_by character varying(255);
ALTER TABLE IT2001
  ADD COLUMN approved2_at timestamp with time zone;
ALTER TABLE IT2001
  ADD COLUMN approved3_by character varying(255);
ALTER TABLE IT2001
  ADD COLUMN approved3_at timestamp with time zone;
