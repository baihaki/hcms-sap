CREATE TABLE cfg_vehicle_types(
  id character varying(255) NOT NULL DEFAULT uuid_generate_v1mc(),
  opt_lock bigint NOT NULL default 0,
  ident_name character varying(50) NOT NULL,
  name character varying(100) NOT NULL,
  description text,
  created_at timestamp with time zone not null default timezone('utc'::text, now()),
  created_by_id character varying(255) NOT NULL,
  updated_at timestamp with time zone not null default timezone('utc'::text, now()),
  updated_by_id character varying(255) NOT NULL,
  deleted_at timestamp with time zone,
  deleted_by_id character varying(255)
);

COMMENT ON TABLE cfg_vehicle_types IS 'Configuration table for Vehicle Types';

COMMENT ON COLUMN cfg_vehicle_types.ident_name IS 'Unique name for Vehicle Type (used as system identifier)';

COMMENT ON COLUMN cfg_vehicle_types.name IS 'Vehicle Type Name (human friendly name)';

ALTER TABLE cfg_vehicle_types ADD PRIMARY KEY(id);

ALTER TABLE cfg_vehicle_types ADD UNIQUE(ident_name);
