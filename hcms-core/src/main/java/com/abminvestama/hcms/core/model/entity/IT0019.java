package com.abminvestama.hcms.core.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * Class that represents master data transaction for "<strong>Monitoring of Tasks</strong> (i.e. IT0019 in SAP).
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add attachment, attachmentPath field</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add fields: text1, text2, text3</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 * 
 */
@Entity
@Table(name = "it0019")
public class IT0019 extends SAPAbstractEntity<ITCompositeKeys> {

	private static final long serialVersionUID = -6458810502850127103L;

	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "infty", referencedColumnName = "infty", insertable = false, updatable = false),
		@JoinColumn(name = "subty", referencedColumnName = "subty", insertable = false, updatable = false)
	})
	private T591S subty;
	
	public IT0019() {}
	
	public IT0019(ITCompositeKeys id) {
		this();
		this.id = id;
	}
	
	@Column(name = "seqnr")
	private Long seqnr;
	
	@Column(name = "itxex")
	private String itxex;
	
	@ManyToOne
	@JoinColumn(name = "tmart", referencedColumnName = "tmart")
	private T531S tmart;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "termn")
	private Date termn;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "mndat")
	private Date mndat;
	
	@Column(name = "bvmrk")
	private String bvmrk;
	
	@Column(name = "tmjhr")
	private Integer tmjhr;
	
	@Column(name = "tmmon")
	private Integer tmmon;
	
	@Column(name = "tmtag")
	private Integer tmtag;
	
	@Column(name = "mnjhr")
	private Integer mnjhr;
	
	@Column(name = "mnmon")
	private Integer mnmon;
	
	@Column(name = "mntag")
	private Integer mntag;
	
	@Column(name = "text1")
	private String text1;
	
	@Column(name = "text2")
	private String text2;
	
	@Column(name = "text3")
	private String text3;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "infty", referencedColumnName = "infty", insertable = false, updatable = false),
		@JoinColumn(name = "attach1_subty", referencedColumnName = "subty", insertable = false, updatable = false)
	})
	private Attachment attachment;
	
	@Column(name = "attach1_path")
	private String attachmentPath;
	
	/**
	 * GET Employee ID/SSN.
	 * 
	 * @return
	 */
	public Long getPernr() {
		return pernr;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	public T591S getSubty() {
		return subty;
	}
	
	/**
	 * GET Infotype Record No.
	 * 
	 * @return
	 */
	public Long getSeqnr() {
		return seqnr;
	}
	
	public void setSeqnr(Long seqnr) {
		this.seqnr = seqnr;
	}
	
	/**
	 * GET Text Exists.
	 * 
	 * @return
	 */
	public String getItxex() {
		return itxex;
	}
	
	public void setItxex(String itxex) {
		this.itxex = itxex;
	}
	
	/**
	 * GET Task Type.
	 * 
	 * @return
	 */
	public T531S getTmart() {
		return tmart;
	}
	
	public void setTmart(T531S tmart) {
		this.tmart = tmart;
	}
	
	/**
	 * GET Date of Task.
	 * 
	 * @return
	 */
	public Date getTermn() {
		return termn;
	}
	
	public void setTermn(Date termn) {
		this.termn = termn;
	}
	
	/**
	 * GET Reminder Date.
	 * 
	 * @return
	 */
	public Date getMndat() {
		return mndat;
	}
	
	public void setMndat(Date mndat) {
		this.mndat = mndat;
	}
	
	/**
	 * GET Processing Indicator.
	 * 
	 * @return
	 */
	public String getBvmrk() {
		return bvmrk;
	}
	
	public void setBvmrk(String bvmrk) {
		this.bvmrk = bvmrk;
	}
	
	/**
	 * GET Year of Date.
	 * 
	 * @return
	 */
	public Integer getTmjhr() {
		return tmjhr;
	}
	
	public void setTmjhr(Integer tmjhr) {
		this.tmjhr = tmjhr;
	}
	
	/**
	 * GET Month of Date.
	 * 
	 * @return
	 */
	public Integer getTmmon() {
		return tmmon;
	}
	
	public void setTmmon(Integer tmmon) {
		this.tmmon = tmmon;
	}
	
	/**
	 * GET Day of Date.
	 * 
	 * @return
	 */
	public Integer getTmtag() {
		return tmtag;
	}
	
	public void setTmtag(Integer tmtag) {
		this.tmtag = tmtag;
	}
	
	/**
	 * GET Year of Reminder.
	 * 
	 * @return
	 */
	public Integer getMnjhr() {
		return mnjhr;
	}
	
	public void setMnjhr(Integer mnjhr) {
		this.mnjhr = mnjhr;
	}
	
	/**
	 * GET Month of Reminder.
	 * 
	 * @return
	 */
	public Integer getMnmon() {
		return mnmon;
	}
	
	public void setMnmon(Integer mnmon) {
		this.mnmon = mnmon;
	}
	
	/**
	 * GET Day of Reminder.
	 * 
	 * @return
	 */
	public Integer getMntag() {
		return mntag;
	}
	
	public void setMntag(Integer mntag) {
		this.mntag = mntag;
	}
	
	/**
	 * GET Text1 Value.
	 * 
	 * @return
	 */
	public String getText1() {
		return text1;
	}
	
	public void setText1(String text1) {
		this.text1 = text1;
	}
	
	/**
	 * GET Text2 Value.
	 * 
	 * @return
	 */
	public String getText2() {
		return text2;
	}
	
	public void setText2(String text2) {
		this.text2 = text2;
	}
	
	/**
	 * GET Text3 Value.
	 * 
	 * @return
	 */
	public String getText3() {
		return text3;
	}
	
	public void setText3(String text3) {
		this.text3 = text3;
	}

	/**
	 * GET Attachment Document Type.
	 * 
	 * @return
	 */
	public Attachment getAttachment() {
		return attachment;
	}
	
	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	/**
	 * GET Attachment Document file path.
	 * 
	 * @return
	 */
	public String getAttachmentPath() {
		return attachmentPath;
	}
	
	public void  setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}
}