package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.T706S;
import com.abminvestama.hcms.core.model.entity.T706SKey;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

public interface T706SQueryService extends DatabaseQueryService<T706S, T706SKey>, DatabasePaginationQueryService {
	

	@NotNull
	Collection<T706S> findByMoreiNE(String morei/*, String schem*/);

	Optional<T706S> findById(String schem);	
}