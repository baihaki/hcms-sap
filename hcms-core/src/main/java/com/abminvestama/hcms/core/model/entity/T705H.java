package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "t705h")
public class T705H extends SAPAbstractEntityZmed<T705HKey>{

	private static final long serialVersionUID = 4747706564427397557L;


	public T705H() {}


	public T705H(T705HKey id) {
		this();
		this.id = id;
	}
	
//	@Column(name = "spras", nullable = false, insertable = false, updatable = false)
//	private String spras;
//	
//	@Column(name = "grawg", nullable = false, insertable = false, updatable = false)
//	private String grawg;
//
//	@Column(name = "zeity", nullable = false, insertable = false, updatable = false)
//	private String zeity;
//
//	@Column(name = "moabw", nullable = false, insertable = false, updatable = false)
//	private Long moabw;

	@Column(name = "bukrs", nullable = false, insertable = false, updatable = false)
	private String bukrs;

	@Column(name = "pinco", nullable = false, insertable = false, updatable = false)
	private String pinco;
	
	@Column(name = "gtext")
	private String gtext;


//	public String getSpras() {
//		return spras;
//	}
//
//
//	public void setSpras(String spras) {
//		this.spras = spras;
//	}
//
//
//	public String getGrawg() {
//		return grawg;
//	}
//
//
//	public void setGrawg(String grawg) {
//		this.grawg = grawg;
//	}
//
//
//	public String getZeity() {
//		return zeity;
//	}
//
//
//	public void setZeity(String zeity) {
//		this.zeity = zeity;
//	}
//
//
//	public Long getMoabw() {
//		return moabw;
//	}
//
//
//	public void setMoabw(Long moabw) {
//		this.moabw = moabw;
//	}

	public String getBukrs() {
		return bukrs;
	}
	
	
	public void setBukrs(String bukrs) {
		this.bukrs = bukrs;
	}
	

	public String getPinco() {
		return pinco;
	}


	public void setPinco(String pinco) {
		this.pinco = pinco;
	}


	public String getGtext() {
		return gtext;
	}


	public void setGtext(String gtext) {
		this.gtext = gtext;
	}
}