package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyHeaderPost;
import com.abminvestama.hcms.core.model.entity.ZmedHeaderPost;
import com.abminvestama.hcms.core.repository.ZmedHeaderPostRepository;
import com.abminvestama.hcms.core.service.api.business.query.ZmedHeaderPostQueryService;

@Service("zmedHeaderPostQueryService")
@Transactional(readOnly = true)
public class ZmedHeaderPostQueryServiceImpl implements ZmedHeaderPostQueryService {

	private ZmedHeaderPostRepository zmedHeaderPostRepository;
	
	@Autowired
	ZmedHeaderPostQueryServiceImpl(ZmedHeaderPostRepository zmedHeaderPostRepository) {
		this.zmedHeaderPostRepository = zmedHeaderPostRepository;
	}
	
	//@Override
	//public Optional<ZmedHeaderPost> findById(Optional<String> id) throws Exception {
	//	return id.map(pk -> zmedHeaderPostRepository.findOne(pk));
	//}

	@Override
	public Optional<Collection<ZmedHeaderPost>> fetchAll() {
		List<ZmedHeaderPost> entities = new ArrayList<>();
		Optional<Iterable<ZmedHeaderPost>> entityIterable = Optional.ofNullable(zmedHeaderPostRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}

	@Override
	public Optional<ZmedHeaderPost> findById(
			Optional<ZmedCompositeKeyHeaderPost> id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<ZmedHeaderPost> findOneByCompositeKey(String objType,
			String objKey, String objSys) {
		if (objType == null || objKey == null || objSys == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(zmedHeaderPostRepository.findOneByCompositeKey(objType, objKey, objSys));
	}

	/*@Override
	public Optional<ZmedHeaderPost> findByEntityName(final String entityName) {
		return Optional.ofNullable(zmedHeaderPostRepository.findByObjKey(entityName));
	}*/
}
