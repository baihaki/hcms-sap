package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;

@Embeddable
@Immutable
public class T705PKey implements Serializable {

	private static final long serialVersionUID = 2987013141063448484L;
	
	private String pmbde;
	private String satza;
	
	public T705PKey() {}
	
	public T705PKey(String pmbde, String satza) {
		this.pmbde = pmbde;
		this.satza = satza;
	}
	
	public String getPmbde() {
		return pmbde;
	}
	
	public String getSatza() {
		return satza;
	}
	
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final T705PKey key = (T705PKey) o;
		
		if (key.getPmbde() != null ? !key.getPmbde().equalsIgnoreCase(pmbde) : pmbde != null) {
			return false;
		}
		
		if (key.getSatza() != null ? !key.getSatza().equalsIgnoreCase(satza) : satza != null) {
			return false;
		}
		
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.pmbde != null ? this.pmbde.hashCode() : 0;
		result = result * 31 + (this.satza != null ? this.satza.hashCode() : 0);
		return result;
	}
}