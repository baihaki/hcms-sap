package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.IT0002;
import com.abminvestama.hcms.core.model.entity.IT0006;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add methods: countByGenderAndBukrs, countByAgeAndBukrs</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0002QueryService extends DatabaseQueryService<IT0002, ITCompositeKeysNoSubtype>, DatabasePaginationQueryService {
	
	@NotNull
	Optional<IT0002> findOneByCompositeKey(Long pernr, Date endda, Date begda);
	
	@NotNull
	Collection<IT0002> findByPernr(Long pernr);		
	

	@NotNull
	Page<IT0002> findByPernrAndStatus(long pernr, String status, int pageNumber);

	@NotNull
	Long countByGenderAndBukrs(String gesch, String bukrs);

	@NotNull
	Long countByAgeAndBukrs(String dat1, String dat2, String bukrs);
}