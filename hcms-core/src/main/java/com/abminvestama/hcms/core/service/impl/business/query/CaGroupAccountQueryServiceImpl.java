package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.CaGroupAccount;
import com.abminvestama.hcms.core.repository.CaGroupAccountRepository;
import com.abminvestama.hcms.core.service.api.business.query.CaGroupAccountQueryService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("caGroupAccountQueryService")
@Transactional(readOnly = true)
public class CaGroupAccountQueryServiceImpl implements CaGroupAccountQueryService {
	
	private CaGroupAccountRepository caGroupAccountRepository;
	
	@Autowired
	CaGroupAccountQueryServiceImpl(CaGroupAccountRepository caGroupAccountRepository) {
		this.caGroupAccountRepository = caGroupAccountRepository;
	}

	@Override
	public Optional<CaGroupAccount> findById(Optional<Long> id) throws Exception {
		return id.map(pk -> caGroupAccountRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<CaGroupAccount>> fetchAll() {
		List<CaGroupAccount> listOfCaGroupAccount = new ArrayList<>();
		Optional<Iterable<CaGroupAccount>> bunchOfCaGroupAccount = Optional.ofNullable(caGroupAccountRepository.findAll());
		return bunchOfCaGroupAccount.map(iter -> {
			iter.forEach(caGroupAccount -> {
				listOfCaGroupAccount.add(caGroupAccount);
			});
			return listOfCaGroupAccount;
		});
	}
}