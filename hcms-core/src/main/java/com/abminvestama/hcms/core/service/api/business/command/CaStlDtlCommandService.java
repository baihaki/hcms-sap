package com.abminvestama.hcms.core.service.api.business.command;

import java.util.List;

import com.abminvestama.hcms.core.model.entity.CaStlDtl;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author wijanarko (wijanarko@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaStlDtlCommandService extends DatabaseCommandService<CaStlDtl> {
	boolean deleteList(List<CaStlDtl> entities);
}
