package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Optional;

import com.abminvestama.hcms.core.model.constant.EntityCategory;
import com.abminvestama.hcms.core.model.entity.ApprovalCategory;
import com.abminvestama.hcms.core.model.entity.Workflow;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface ApprovalCategoryQueryService extends DatabaseQueryService<ApprovalCategory, String> {
	
	Optional<ApprovalCategory> findByWorkflowAndEntityCategory(final Workflow workflow,
			final EntityCategory entityCategory);
}