package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * Class that represents <strong>Location Type</strong> object.
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 3.0.0 (Cash Advance)
 * @since 3.0.0
 *
 */
@Entity
@Table(name = "ca_location_type")
public class CaLocationType implements Serializable {
	
	private static final long serialVersionUID = -1127608090184228975L;
	
	@Id
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "location_type_text")
	private String locationTypeText; 
	
	public void setId(Long id){
		this.id = id;
	}
 
	public String getLocationTypeText(){
		return locationTypeText;
	}
	
	public void setLocationTypeText(String locationTypeText){
		this.locationTypeText = locationTypeText;
	}
	
	public CaLocationType(){
	}

	public CaLocationType( Long id,  String locationTypeText ){
	 	this.id = id;
	 	this.locationTypeText = locationTypeText;
	}
	
	public Long getId() {
		return  id;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		CaLocationType caLocationType = (CaLocationType) o;
		 
		if (id != null ? !id.equals(caLocationType.id) : caLocationType.id != null) return false; 
		if (locationTypeText != null ? !locationTypeText.equals(caLocationType.locationTypeText) : caLocationType.locationTypeText != null) return false;
		return true; 
	}
	
	@Override
	public int hashCode() {
	    int result = (id != null ?  id.hashCode() : 0); 
		result = 31 * (locationTypeText != null ?  locationTypeText.hashCode() : 0); 
	 
		return result; 
	}
	
	@Override
	public String toString() {
		return "caLocationType(id="+id+",locationTypeText="+locationTypeText+')'; 
	}
 
}

	