package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;



@Embeddable
@Immutable
public class ZmedCompositeKeyEmpquotasm implements Serializable {
	private static final long serialVersionUID = 5385850040856914239L;

	private Long pernr;
	private String bukrs;
	private String kodequo;
	private String persg;
	private String persk;
	private String fatxt;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datab", nullable = false)
	private Date datab;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datbi", nullable = false)
	private Date datbi;
	
	public ZmedCompositeKeyEmpquotasm() {}
	
	public ZmedCompositeKeyEmpquotasm(Long pernr, String bukrs, String persg, String persk, String fatxt, String kodequo, Date datab, Date datbi) {
		this.pernr = pernr;
		this.bukrs = bukrs;
		this.kodequo = kodequo;
		this.persg = persg;
		this.persk = persk;
		this.fatxt = fatxt;
		this.datab = datab;
		this.datbi = datbi;
	}
	
	public String getFatxt() {
		return fatxt;
	}
	
	public String getBukrs() {
		return bukrs;
	}

	public Long getPernr() {
		return pernr;
	}

	public String getKodequo() {
		return kodequo;
	}
	
	public String getPersg() {
		return persg;
	}

	public String getPersk() {
		return persk;
	}

	public Date getDatab() {
		return datab;
	}

	public Date getDatbi() {
		return datbi;
	}

	
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final ZmedCompositeKeyEmpquotasm key = (ZmedCompositeKeyEmpquotasm) o;
		
		if (key.getBukrs() != null ? key.getBukrs() != (bukrs != null ? bukrs: "") : bukrs != null) {
			return false;
		}
		
		if (key.getPernr() != null ? key.getPernr() != (pernr != null ? pernr: "") : pernr != null) {
			return false;
		}
		
		if (key.getKodequo() != null ? key.getKodequo() != (kodequo != null ? kodequo: "") : kodequo != null) {
			return false;
		}
		
		if (key.getFatxt() != null ? key.getFatxt() != (fatxt != null ? fatxt: "") : fatxt != null) {
			return false;
		}
		
		if (key.getPersg() != null ? key.getPersg() != (persg != null ? persg: "") : persg != null) {
			return false;
		}
		
		if (key.getPersk() != null ? key.getPersk() != (persk != null ? persk: "") : persk != null) {
			return false;
		}
		
		if (key.getDatab() != null ? (key.getDatab().compareTo(datab != null ? datab : new Date()) != 0) : datab != null) {
			return false;
		}
		
		if (key.getDatbi() != null ? (key.getDatbi().compareTo(datbi != null ? datbi : new Date()) != 0) : datbi != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.bukrs != null ? this.bukrs.hashCode() : 0;
		result = result * 31 + (this.fatxt != null ? this.fatxt.hashCode() : 0);
		result = result * 31 + (this.pernr != null ? this.pernr.hashCode() : 0);
		result = result * 31 + (this.persk != null ? this.persk.hashCode() : 0);
		result = result * 31 + (this.kodequo != null ? this.kodequo.hashCode() : 0);
		result = result * 31 + (this.datab != null ? this.datab.hashCode() : 0);
		result = result * 31 + (this.datbi != null ? this.datbi.hashCode() : 0);
		return result;
	}
}
