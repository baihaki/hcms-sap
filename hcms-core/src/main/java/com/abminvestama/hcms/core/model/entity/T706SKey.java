package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;

@Embeddable
@Immutable
public class T706SKey implements Serializable {

	private static final long serialVersionUID = 2987013141063448484L;
	
	private String morei;
	private String schem;
	
	public T706SKey() {}
	
	public T706SKey(String morei, String schem) {
		this.morei = morei;
		this.schem = schem;
	}
	
	public String getMorei() {
		return morei;
	}
	
	public String getSchem() {
		return schem;
	}
	
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final T706SKey key = (T706SKey) o;
		
		if (key.getMorei() != null ? !key.getMorei().equalsIgnoreCase(morei) : morei != null) {
			return false;
		}
		
		if (key.getSchem() != null ? !key.getSchem().equalsIgnoreCase(schem) : schem != null) {
			return false;
		}
		
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.morei != null ? this.morei.hashCode() : 0;
		result = result * 31 + (this.schem != null ? this.schem.hashCode() : 0);
		return result;
	}
}