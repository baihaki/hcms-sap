package com.abminvestama.hcms.core.repository;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.CaGlAccount;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaGlAccountRepository extends CrudRepository<CaGlAccount, Long> {

	@Query("FROM CaGlAccount caGlAccount ORDER BY caGlAccount.glAcctText")
	Page<CaGlAccount> findAll(Pageable pageRequest);
	
	@Query("FROM CaGlAccount caGlAccount WHERE caGlAccount.endda = '9999-12-31' ORDER BY caGlAccount.glAcctText")
	Page<CaGlAccount> findValidAll(Pageable pageRequest);
	
	@Query(
			"FROM CaGlAccount caGlAccount WHERE caGlAccount.groupAccount.id = :groupAccountId ORDER BY " + 
			"caGlAccount.glAcctText"
	)
	Collection<CaGlAccount> findByGroupAccountId(@Param("groupAccountId") Long groupAccountId);
	
	@Query(
			"FROM CaGlAccount caGlAccount WHERE caGlAccount.groupAccount.id = :groupAccountId AND caGlAccount.endda = '9999-12-31' ORDER BY " + 
			"caGlAccount.glAcctText"
	)	
	Collection<CaGlAccount> findValidByGroupAccountId(@Param("groupAccountId") Long groupAccountId);
	
	@Query(
		"FROM CaGlAccount caGlAccount WHERE caGlAccount.groupAccount.id = :groupAccountId ORDER BY " + 
			"caGlAccount.glAcctText"
	)
	Page<CaGlAccount> findByGroupAccountId(@Param("groupAccountId") Long groupAccountId, Pageable pageRequest);
	
	@Query(
			"FROM CaGlAccount caGlAccount WHERE caGlAccount.groupAccount.id = :groupAccountId AND caGlAccount.endda = '9999-12-31' ORDER BY " + 
			"caGlAccount.glAcctText"
	)
	Page<CaGlAccount> findValidByGroupAccountId(@Param("groupAccountId") Long groupAccountId, Pageable pageRequest);
	
}