package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;


public interface ZmedEmpquotasmCommandService extends DatabaseCommandService<ZmedEmpquotasm> {
}