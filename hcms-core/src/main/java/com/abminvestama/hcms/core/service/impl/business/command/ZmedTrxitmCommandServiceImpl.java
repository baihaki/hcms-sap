package com.abminvestama.hcms.core.service.impl.business.command;


import java.util.Date;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.ZmedTrxitmRepository;
import com.abminvestama.hcms.core.service.api.business.command.ZmedTrxitmCommandService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add updateClmno method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("ZmedTrxitmCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ZmedTrxitmCommandServiceImpl implements ZmedTrxitmCommandService {
	
	private ZmedTrxitmRepository zmedTrxitmRepository;
		
		@Autowired
		ZmedTrxitmCommandServiceImpl(ZmedTrxitmRepository zmedTrxitmRepository) {
			this.zmedTrxitmRepository = zmedTrxitmRepository;
		}
		
		@Override
		public Optional<ZmedTrxitm> save(ZmedTrxitm entity, User user)
				throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
			//if (user != null) {
			//	entity.setUname(user.getId());
			//}
			
			try {
				return Optional.ofNullable(zmedTrxitmRepository.save(entity));
			} catch (Exception e) {
				throw new CannotPersistException("Cannot persist ZmedTrxitm (Personal Information).Reason=" + e.getMessage());
			}				
		}

		@Override
		public boolean delete(ZmedTrxitm entity, User user) throws NoSuchMethodException, ConstraintViolationException {
			// no delete at this version.
			throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
		}

		@Override
		public boolean restore(ZmedTrxitm entity, User user) throws NoSuchMethodException, ConstraintViolationException {
			// since we don't have delete operation, then we don't need the 'restore' as well
			throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
		}
		
		@Override
		public Optional<ZmedTrxitm> updateClmno(String newClmno, ZmedTrxitm entity, User user)
				/*throws CannotPersistException, NoSuchMethodException, ConstraintViolationException*/ {
			
			try {
				int row = zmedTrxitmRepository.updateClmno(newClmno, entity.getClmno(), entity.getItmno());
				if (row > 0) {
				    entity.setClmno(newClmno);
				}
				return Optional.ofNullable(entity);
				//return Optional.ofNullable(zmedTrxitmRepository.save(entity));
			} catch (Exception e) {
				entity.setClmno(newClmno);
				System.out.println("updateClmno Exception: " + e.getMessage());
				return Optional.ofNullable(entity);
			}				
		}
	}