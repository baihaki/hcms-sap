package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 * 
 * Class that represents master data for <strong>Sub Expense Types</strong> (sub_t706b1).
 *
 */
@Entity
@Table(name = "sub_t706b1")
public class SubT706B1 implements Serializable {
	
	private static final long serialVersionUID = 2142159354846517718L;
	
	@EmbeddedId
	private SubT706B1Key id;
	
	@Column(name = "sub_sptxt", nullable = false)
	private String subSptxt;
	
	@Column(name = "sub_form", nullable = false)
	private short subForm;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "morei", referencedColumnName = "morei", insertable = false, updatable = false),
		@JoinColumn(name = "spkzl", referencedColumnName = "spkzl", insertable = false, updatable = false)
	})
	private SHT706B1 sht706b1;

	public SubT706B1() {}
	
	public SubT706B1(SubT706B1Key id) {
		this();
		this.id = id;
	}

	public String getSubSptxt() {
		return subSptxt;
	}

	public short getSubForm() {
		return subForm;
	}

	public SHT706B1 getSht706b1() {
		return sht706b1;
	}

	public SubT706B1Key getId() {
		return id;
	}
	
}