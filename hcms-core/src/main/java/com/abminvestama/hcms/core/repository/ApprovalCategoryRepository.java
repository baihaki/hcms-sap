package com.abminvestama.hcms.core.repository;

import org.springframework.data.repository.CrudRepository;

import com.abminvestama.hcms.core.model.constant.EntityCategory;
import com.abminvestama.hcms.core.model.entity.ApprovalCategory;
import com.abminvestama.hcms.core.model.entity.Workflow;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface ApprovalCategoryRepository extends CrudRepository<ApprovalCategory, String> {
	
	ApprovalCategory findByWorkflowAndEntityCategory(Workflow workflow, EntityCategory entityCategory);
}