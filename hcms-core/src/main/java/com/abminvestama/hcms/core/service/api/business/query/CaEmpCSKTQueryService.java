package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.CaEmpCSKT;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaEmpCSKTQueryService extends DatabaseQueryService<CaEmpCSKT, Long>, DatabasePaginationQueryService {
	
	Collection<CaEmpCSKT> findCostCenterActiveByPernr(Long pernr);

	Collection<CaEmpCSKT> findAllCostCenterActiveByBukrs(String bukrs);
	
	Collection<CaEmpCSKT> findEmpCSKTByKostlPoolAndPoolingCode(String kostlPool, String poolingCode);
	
	Collection<CaEmpCSKT> findEmpCSKTByKostlPoolAndKostlDefault(String kostlPool, String kostlDefault);

	Page<CaEmpCSKT> fetchAllWithPaging(int pageNumber);
}
