package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0014;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0014SoapObject {
	
	private String employeenumber;
	private String validitybegin;
	private String validityend;
	private String infty;
	private String subty;
	private String seqnr;
	private String lgart;
	private String opken;
	private String betrg;
	private String waers;
	


	private IT0014SoapObject() {}
	
	public IT0014SoapObject(IT0014 object) {
		if (object == null) {
			new IT0014SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			infty = object.getSubty().getId().getInfty();
			subty = object.getSubty().getId().getSubty();
			seqnr = object.getSeqnr().toString();
			lgart = object.getLgart().getLgart();
			opken = object.getOpken();
			betrg = object.getBetrg().toString();
			waers = object.getWaers();
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}

	public String getInfty() {
		return infty;
	}

	public String getSubty() {
		return subty;
	}

	public String getSeqnr() {
		return seqnr;
	}

	public String getLgart() {
		return lgart;
	}

	public String getOpken() {
		return opken;
	}

	public String getBetrg() {
		return betrg;
	}

	public String getWaers() {
		return waers;
	}

}
