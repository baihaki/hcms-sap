package com.abminvestama.hcms.core.model.constant;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public enum FamilySubtype {

	SPOUSE("1"), DIVORCED_SPOUSE("10"), FATHER("11"), MOTHER("12"), DOMESTIC_PARTNER("13"),
	CHILD_OF_DOMESTIC_PARTNER("14"), REGISTERED_PARTNER("15"), CHILD("2"), GUARDIAN("5"),
	EMERGENCY_CONTACT("7");
	
	private String subty;
	
	private FamilySubtype(String subty) {
		this.subty = subty;
	}
	
	public String subtype() {
		return subty;
	}
}