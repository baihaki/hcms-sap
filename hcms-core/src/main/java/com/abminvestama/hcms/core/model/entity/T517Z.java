package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 * 
 * Class that represents master data for <strong>Education Est. (School Types) / Branches of Study</strong> (i.e. T517Z in SAP).
 *
 */
@Entity
@Table(name = "t517z")
@IdClass(T517ZKey.class)
public class T517Z implements Serializable {
	
	private static final long serialVersionUID = -3229985840278831454L;

	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "slart", referencedColumnName = "slart", insertable = false, updatable = false, nullable = false)
	private T517T slart;
	
	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "faart", referencedColumnName = "faart", insertable = false, updatable = false, nullable = false)	
	private T517X faart;
	
	/**
	 * GET Education Est. (School Type)
	 * 
	 * @return
	 */
	public T517T getSlart() {
		return slart;
	}
	
	/**
	 * GET Education Branch of Study.
	 * 
	 * @return
	 */
	public T517X getFaart() {
		return faart;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final T517Z obj = (T517Z) o;
		
		if (obj.getSlart() != null ? !obj.getSlart().equals(slart) : slart != null) {
			return false;
		}
		
		if (obj.getFaart() != null ? !obj.getFaart().equals(faart) : faart != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.slart != null ? this.slart.hashCode() : 0;
		result = result * 31 + (this.faart != null ? this.faart.hashCode() : 0);
		return result;
	}
}