package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.repository.EventLogRepository;
import com.abminvestama.hcms.core.service.api.business.query.EventLogQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("eventLogQueryService")
@Transactional(readOnly = true)
public class EventLogQueryServiceImpl implements EventLogQueryService {
	
	private EventLogRepository eventLogRepository;
	
	@Autowired
	EventLogQueryServiceImpl(EventLogRepository eventLogRepository) {
		this.eventLogRepository = eventLogRepository;
	}

	@Override
	public Optional<EventLog> findById(Optional<String> id) throws Exception {
		return id.map(pk -> eventLogRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<EventLog>> fetchAll() {
		Optional<Iterable<EventLog>> eventLogRecords = Optional.ofNullable(eventLogRepository.findAll());
		List<EventLog> eventLogCollections = new ArrayList<>();
		return eventLogRecords.map(eventLogs -> { 
			eventLogs.forEach(eventLog -> {
				eventLogCollections.add(eventLog);
			});
			return eventLogCollections;
		});
	}

	@Override
	public Collection<EventLog> findByEntityIdAndObjectId(String id, String objectId) throws DataViolationException {
		if (StringUtils.isEmpty(id) || StringUtils.isEmpty(objectId)) {
			throw new DataViolationException("Insufficient Parameters! 'entity_id' and 'entity_object_id' is required.");
		}
		
		return eventLogRepository.findByEntityIdAndObjectId(id, objectId);
	}

	@Override
	public Collection<EventLog> findByEntityIdAndObjectIdAndOperationType(String id, String objectId,
			OperationType operationType) {
		if (StringUtils.isEmpty(id) || StringUtils.isEmpty(objectId) || operationType == null) {
			return new ArrayList<>();
		}
		
		return eventLogRepository.findBYEntityIdAndObjectIdAndOperationType(id, objectId, operationType.name());
	}
}