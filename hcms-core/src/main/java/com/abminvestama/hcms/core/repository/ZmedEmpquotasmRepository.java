package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;

public interface ZmedEmpquotasmRepository extends CrudRepository<ZmedEmpquotasm, String> {
	
	//ZmedEmpquotasm findByEntityName(String Name);	

	/*@Query("FROM ZmedEmpquotasm zmed_empquotasm WHERE zmed_empquotasm.pernr = :pernr")
	Collection<ZmedEmpquotasm> findByPernr(@Param("pernr") Long pernr);	*/
	
	@Query("FROM ZmedEmpquotasm zmed_empquotasm WHERE zmed_empquotasm.bukrs.bukrs = :bukrs AND zmed_empquotasm.fatxt = :fatxt AND zmed_empquotasm.pernr = :pernr AND zmed_empquotasm.persg = :persg AND zmed_empquotasm.persk = :persk AND zmed_empquotasm.kodequo = :kodequo AND zmed_empquotasm.id.datab = :datab AND zmed_empquotasm.id.datbi = :datbi")
	ZmedEmpquotasm findOneByCompositeKey(
			@Param("pernr") long pernr,
			@Param("bukrs") String bukrs,
			@Param("persg") String persg, 
			@Param("persk") String persk,
			@Param("fatxt") String fatxt,
			@Param("kodequo") String kodequo,
			@Param("datab") @Temporal(TemporalType.DATE) Date datab,
			@Param("datbi") @Temporal(TemporalType.DATE) Date datbi);
	
	

	@Query("FROM ZmedEmpquotasm zmed_empquotasm WHERE zmed_empquotasm.bukrs.bukrs = :bukrs AND zmed_empquotasm.pernr = :pernr AND zmed_empquotasm.persg = :persg AND zmed_empquotasm.persk = :persk AND zmed_empquotasm.id.datab = :datab AND zmed_empquotasm.id.datbi = :datbi")
	Collection<ZmedEmpquotasm> findQuotas(
			@Param("pernr") long pernr,
			@Param("bukrs") String bukrs,
			@Param("persg") String persg, 
			@Param("persk") String persk,
			@Param("datab") @Temporal(TemporalType.DATE) Date datab,
			@Param("datbi") @Temporal(TemporalType.DATE) Date datbi);
	
	Collection<ZmedEmpquotasm> findByPernrAndBukrsAndPerskAndPersgAndDatabAndDatbi(long pernr, T001 bukrs, String persk, String persg, Date datab, Date datbi);
	Collection<ZmedEmpquotasm> findByPernrAndBukrsAndPerskAndPersg(long pernr, T001 bukrs, String persk, String persg);
	
	
	//@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.id.endda = :endda AND it0002.id.begda = :begda")
	//IT0002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
	//		@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
}