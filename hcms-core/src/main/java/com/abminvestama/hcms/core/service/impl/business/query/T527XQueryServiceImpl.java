package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T527X;
import com.abminvestama.hcms.core.model.entity.T527XKey;
import com.abminvestama.hcms.core.repository.T527XRepository;
import com.abminvestama.hcms.core.service.api.business.query.T527XQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("t527xQueryService")
@Transactional(readOnly = true)
public class T527XQueryServiceImpl implements T527XQueryService {
	
	private T527XRepository t527xRepository;
	
	@Autowired
	T527XQueryServiceImpl(T527XRepository t527xRepository) {
		this.t527xRepository = t527xRepository;
	}

	@Override
	public Optional<T527X> findById(Optional<T527XKey> id) throws Exception {
		return id.map(pk -> t527xRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<T527X>> fetchAll() {
		List<T527X> listOfT527X = new ArrayList<>();
		Optional<Iterable<T527X>> bunchOfT527X = Optional.ofNullable(t527xRepository.findAll());
		return bunchOfT527X.map(iter -> {
			iter.forEach(t527x -> {
				listOfT527X.add(t527x);
			});
			return listOfT527X;
		});
	}
}