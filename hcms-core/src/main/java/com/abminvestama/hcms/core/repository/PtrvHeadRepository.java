package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.PtrvHead;
import com.abminvestama.hcms.core.model.entity.PtrvHeadKey;

/**
 * 
 * @since 1.0.0
 * @version 1.1.0
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.1.0</td><td>Baihaki</td><td>Add findByPernrAndProcessesAndDatv1 method</td></tr>
 *     <tr><td>1.0.9</td><td>Baihaki</td><td>Add parameter @Param("lastSync") to update last_sync on updateReinr method</td></tr>
 *     <tr><td>1.0.8</td><td>Baihaki</td><td>Add parameter @Param("reason") to update reason on updateReinr method</td></tr>
 *     <tr><td>1.0.7</td><td>Baihaki</td><td>Add parameter @Param("notStatus") to filter by paymentStatus or not (optional)</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessAndSuperiorPositionCount, findByBukrsAndProcessAndSuperiorPositionWithPaging</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add forms parameter</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessesWithPaging, findByPernrAndProcessesWithPaging</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: findByPernrWithPaging, findByPernrAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add methods: findByBukrsWithPaging, findByBukrsAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add updateReinr method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface PtrvHeadRepository extends CrudRepository<PtrvHead, PtrvHeadKey> {


	@Query("FROM PtrvHead ptrv_head WHERE ptrv_head.hdvrs = :hdvrs AND ptrv_head.pernr = :pernr AND ptrv_head.reinr = :reinr")
	PtrvHead findOneByCompositeKey(@Param("hdvrs") Integer hdvrs, 
			@Param("pernr") Long pernr,
			@Param("reinr") Long reinr);
	
	@Modifying(clearAutomatically = true)
	@Query(nativeQuery=true, value="UPDATE ptrv_head SET reinr = :newReinr, process = :process, approved1_by = :approved_by," +
	    " approved1_at = :approved_at, reason = :reason, last_sync = :last_sync WHERE reinr = :oldReinr AND pernr = :pernr")
	int updateReinr(@Param("newReinr") Long newReinr, @Param("process") String process,
			@Param("approved_by") String approvedBy, @Param("approved_at") Date approvedAt,
			@Param("reason") String reason, @Param("last_sync") Date lastSync,
			@Param("oldReinr") Long oldReinr, @Param("pernr") Long pernr);
	
	/*
	@Query("FROM PtrvHead ptrv_head WHERE ptrv_head.bukrs.bukrs = :bukrs AND (ptrv_head.paymentStatus != :notStatus)"
			+ " ORDER BY ptrv_head.dates DESC, ptrv_head.times DESC")
	Page<PtrvHead> findByBukrsWithPaging(@Param("bukrs") String bukrs, @Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM PtrvHead ptrv_head WHERE ptrv_head.bukrs.bukrs = :bukrs AND ptrv_head.process = :process AND (ptrv_head.paymentStatus != :notStatus)"
			+ " ORDER BY ptrv_head.dates DESC, ptrv_head.times DESC")
	Page<PtrvHead> findByBukrsAndProcessWithPaging(@Param("bukrs") String bukrs, @Param("process") String process, @Param("notStatus") String notStatus, Pageable pageRequest);

	@Query("FROM PtrvHead ptrv_head WHERE ptrv_head.pernr = :pernr AND (ptrv_head.paymentStatus != :notStatus)"
			+ " ORDER BY ptrv_head.dates DESC, ptrv_head.times DESC")
	Page<PtrvHead> findByPernrWithPaging(@Param("pernr") Long pernr, @Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM PtrvHead ptrv_head WHERE ptrv_head.pernr = :pernr AND ptrv_head.process = :process AND (ptrv_head.paymentStatus != :notStatus)"
			+ " ORDER BY ptrv_head.dates DESC, ptrv_head.times DESC")
	Page<PtrvHead> findByPernrAndProcessWithPaging(@Param("pernr") Long pernr, @Param("process") String process, @Param("notStatus") String notStatus, Pageable pageRequest);

	//@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.id.endda = :endda AND it0002.id.begda = :begda")
	//IT0002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
	//		@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM PtrvHead ptrv_head WHERE ptrv_head.bukrs.bukrs = :bukrs AND ptrv_head.process in :processes AND (ptrv_head.paymentStatus != :notStatus)"
			+ " ORDER BY ptrv_head.dates DESC, ptrv_head.times DESC")
	Page<PtrvHead> findByBukrsAndProcessesWithPaging(@Param("bukrs") String bukrs, @Param("processes") String[] processes, @Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM PtrvHead ptrv_head WHERE ptrv_head.pernr = :pernr AND ptrv_head.process in :processes AND (ptrv_head.paymentStatus != :notStatus)"
			+ " ORDER BY ptrv_head.dates DESC, ptrv_head.times DESC")
	Page<PtrvHead> findByPernrAndProcessesWithPaging(@Param("pernr") Long pernr, @Param("processes") String[] processes, @Param("notStatus") String notStatus, Pageable pageRequest);
	*/
	
	@Query("FROM PtrvHead ptrv_head WHERE ptrv_head.bukrs.bukrs = :bukrs AND ptrv_head.form in :forms"
			+ " AND (ptrv_head.paymentStatus != :notStatus) ORDER BY ptrv_head.dates DESC, ptrv_head.times DESC")
	Page<PtrvHead> findByBukrsWithPaging(@Param("bukrs") String bukrs, @Param("forms") short[] forms, @Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM PtrvHead ptrv_head WHERE ptrv_head.bukrs.bukrs = :bukrs AND ptrv_head.process = :process AND ptrv_head.form in :forms"
			+ " AND (ptrv_head.paymentStatus != :notStatus) ORDER BY ptrv_head.dates DESC, ptrv_head.times DESC")
	Page<PtrvHead> findByBukrsAndProcessWithPaging(@Param("bukrs") String bukrs, @Param("process") String process
			, @Param("forms") short[] forms, @Param("notStatus") String notStatus, Pageable pageRequest);

	String FIND_BY_SUPERIOR_COUNT = "SELECT COUNT(*) FROM ptrv_head INNER JOIN it0001 a ON ptrv_head.pernr = a.pernr" +
	        " INNER JOIN position_relations b ON a.m_position_id = b.from_position_id " +
			" WHERE ptrv_head.bukrs = :bukrs AND ptrv_head.process = :process AND ptrv_head.form_type in :forms " +
	        " AND (ptrv_head.payment_status != :notStatus) AND a.endda = '9999-12-31' AND b.to_position_id = :superiorPosition";
	
	String FIND_BY_SUPERIOR_QUERY = "SELECT ptrv_head.* FROM ptrv_head INNER JOIN it0001 a ON ptrv_head.pernr = a.pernr" +
	        " INNER JOIN position_relations b ON a.m_position_id = b.from_position_id " +
			" WHERE ptrv_head.bukrs = :bukrs AND ptrv_head.process = :process AND ptrv_head.form_type in :forms " +
	        " AND (ptrv_head.payment_status != :notStatus) AND a.endda = '9999-12-31' AND b.to_position_id = :superiorPosition " +
			" ORDER BY ptrv_head.dates DESC, ptrv_head.times DESC LIMIT :limit OFFSET :offset";
	
	@Query(nativeQuery=true, value=FIND_BY_SUPERIOR_COUNT)
	int findByBukrsAndProcessAndSuperiorPositionCount(@Param("bukrs") String bukrs, @Param("process") String process
			, @Param("forms") short[] forms, @Param("notStatus") String notStatus, @Param("superiorPosition") String superiorPosition);
	
	@Query(nativeQuery=true, value=FIND_BY_SUPERIOR_QUERY)
	Collection<PtrvHead> findByBukrsAndProcessAndSuperiorPositionWithPaging(@Param("bukrs") String bukrs, @Param("process") String process
			, @Param("forms") short[] forms, @Param("notStatus") String notStatus, @Param("superiorPosition") String superiorPosition
			, @Param("limit") int limit, @Param("offset") int offset);
	
	@Query("FROM PtrvHead ptrv_head WHERE ptrv_head.pernr = :pernr AND ptrv_head.form in :forms"
			+ " AND (ptrv_head.paymentStatus != :notStatus) ORDER BY ptrv_head.dates DESC, ptrv_head.times DESC")
	Page<PtrvHead> findByPernrWithPaging(@Param("pernr") Long pernr, @Param("forms") short[] forms, @Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM PtrvHead ptrv_head WHERE ptrv_head.pernr = :pernr AND ptrv_head.process = :process AND ptrv_head.form in :forms"
			+ " AND (ptrv_head.paymentStatus != :notStatus) ORDER BY ptrv_head.dates DESC, ptrv_head.times DESC")
	Page<PtrvHead> findByPernrAndProcessWithPaging(@Param("pernr") Long pernr, @Param("process") String process
			, @Param("forms") short[] forms, @Param("notStatus") String notStatus, Pageable pageRequest);

	//@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.id.endda = :endda AND it0002.id.begda = :begda")
	//IT0002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
	//		@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM PtrvHead ptrv_head WHERE ptrv_head.bukrs.bukrs = :bukrs AND ptrv_head.process in :processes AND ptrv_head.form in :forms"
			+ " AND (ptrv_head.paymentStatus != :notStatus) ORDER BY ptrv_head.dates DESC, ptrv_head.times DESC")
	Page<PtrvHead> findByBukrsAndProcessesWithPaging(@Param("bukrs") String bukrs, @Param("processes") String[] processes
			, @Param("forms") short[] forms, @Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM PtrvHead ptrv_head WHERE ptrv_head.pernr = :pernr AND ptrv_head.process in :processes AND ptrv_head.form in :forms"
			+ " AND (ptrv_head.paymentStatus != :notStatus) ORDER BY ptrv_head.dates DESC, ptrv_head.times DESC")
	Page<PtrvHead> findByPernrAndProcessesWithPaging(@Param("pernr") Long pernr, @Param("processes") String[] processes
			, @Param("forms") short[] forms, @Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM PtrvHead ptrv_head WHERE ptrv_head.pernr = :pernr AND ptrv_head.process in :processes AND ptrv_head.datv1 = :datv1"
			+ " ORDER BY ptrv_head.dates DESC, ptrv_head.times DESC")
	Collection<PtrvHead> findByPernrAndProcessesAndDatv1(@Param("pernr") Long pernr, @Param("processes") String[] processes, @Param("datv1") Date datv1);

	
}