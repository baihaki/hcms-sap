package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.CaStlHdr;
import com.abminvestama.hcms.core.repository.CaStlHdrRepository;
import com.abminvestama.hcms.core.service.api.business.query.CaStlHdrQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("caStlHdrQueryService")
@Transactional(readOnly = true)
public class CaStlHdrQueryServiceImpl implements CaStlHdrQueryService {
	
	private CaStlHdrRepository caStlHdrRepository;

	@Autowired
	CaStlHdrQueryServiceImpl(CaStlHdrRepository caStlHdrRepository) {
		this.caStlHdrRepository = caStlHdrRepository;
	}

	@Override
	public Optional<CaStlHdr> findById(Optional<String> id) {
		return id.map(data -> Optional.ofNullable(caStlHdrRepository.findByReqno(data))).orElse(Optional.empty());
	}

	@Override
	public Optional<Collection<CaStlHdr>> fetchAll() {
		List<CaStlHdr> listOfCaStlHdr = new ArrayList<>();
		Optional<Iterable<CaStlHdr>> bunchOfCaStlHdr = Optional.ofNullable(caStlHdrRepository.findAll());
		return bunchOfCaStlHdr.map(iter -> {
			iter.forEach(caStlHdr -> {
				listOfCaStlHdr.add(caStlHdr);
			});
			return listOfCaStlHdr;
		});
	}

	@Override
	public Optional<CaStlHdr> findByReqno(String reqNo) {
		if (reqNo == null ) return Optional.empty();
		return Optional.ofNullable(caStlHdrRepository.findByReqno(reqNo));
	}

	@Override
	public Optional<CaStlHdr> findByTcano(String tcaNo){
		if (tcaNo == null || tcaNo.isEmpty()) return Optional.empty();
		return Optional.ofNullable(caStlHdrRepository.findByTcano(tcaNo));
	}

	@Override
	public Page<CaStlHdr> findByPernr(Long pernr, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		return caStlHdrRepository.findByPernr(pernr, pageRequest);
	}

	@Override
	public Page<CaStlHdr> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		return caStlHdrRepository.findAll(pageRequest);		
	}

	@Override
	public Page<CaStlHdr> findAllByCriteriaWithPernrs(Long[] pernrs, Integer process, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		Page<CaStlHdr> pages = null;
		if (pernrs != null && pernrs.length > 0 && process != null) {
			pages = caStlHdrRepository.findAllByCriteriaWithPernrs(pernrs, process, pageRequest);
		}
		// Nothing filter process
		else if (pernrs != null && pernrs.length > 0) {
			pages = caStlHdrRepository.findAllByCriteriaWithPernrs(pernrs, pageRequest);
		}
		return pages;
	}

	@Override
	public Page<CaStlHdr> findAllByCriteriaWithProcesses(Long pernr, Integer[] processes, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		Page<CaStlHdr> pages = null;
		if (pernr != null && processes != null && processes.length > 0) {
			pages = caStlHdrRepository.findAllByCriteriaWithProcesses(pernr, processes, pageRequest);
		}
		else if (processes != null && processes.length > 0) {
			pages = caStlHdrRepository.findAllByCriteriaWithProcesses(processes, pageRequest);
		}
		return pages;
	}

	@Override
	public Page<CaStlHdr> findAllByCriteria(Long pernr, Integer process, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		return pernr == null ? caStlHdrRepository.findAllByCriteria(process, pageRequest)
				: caStlHdrRepository.findAllByCriteria(pernr, process, pageRequest);		
	}

	@Override
	public Page<CaStlHdr> findSubOrdinatesByCriteria(Long pernr, Integer process, String toPositionId, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		List<CaStlHdr> listOfCaStlHdr = new ArrayList<CaStlHdr>();
		
		// Find by PERNR, Process, To Position Id
		if (pernr != null && process != null && !StringUtils.isEmpty(toPositionId)) {
			int total = caStlHdrRepository.findSubOrdinatesByCriteriaCount(pernr, process, toPositionId);
			listOfCaStlHdr = caStlHdrRepository.findSubOrdinatesByCriteriaQuery(pernr, process,
					toPositionId, pageRequest.getPageSize(), pageRequest.getOffset()).stream().collect(Collectors.toList());
			return new PageImpl<CaStlHdr>(listOfCaStlHdr, pageRequest, total);
		}
		// Find by Process, To Position Id
		else if (process != null && !StringUtils.isEmpty(toPositionId)) {
			int total = caStlHdrRepository.findSubOrdinatesByCriteriaCount(process, toPositionId);
			listOfCaStlHdr = caStlHdrRepository.findSubOrdinatesByCriteriaQuery(process,
					toPositionId, pageRequest.getPageSize(), pageRequest.getOffset()).stream().collect(Collectors.toList());
			return new PageImpl<CaStlHdr>(listOfCaStlHdr, pageRequest, total);
		}
		// Find by To Position Id
		else if (!StringUtils.isEmpty(toPositionId)) {
			int total = caStlHdrRepository.findSubOrdinatesByCriteriaCount(toPositionId);
			listOfCaStlHdr = caStlHdrRepository
					.findSubOrdinatesByCriteriaQuery(toPositionId, pageRequest.getPageSize(), pageRequest.getOffset())
					.stream().collect(Collectors.toList());
			return new PageImpl<CaStlHdr>(listOfCaStlHdr, pageRequest, total);
		}
		return new PageImpl<CaStlHdr>(new ArrayList<CaStlHdr>());
	}
}
