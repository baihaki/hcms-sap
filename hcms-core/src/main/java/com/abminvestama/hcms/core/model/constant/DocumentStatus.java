package com.abminvestama.hcms.core.model.constant;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public enum DocumentStatus {

	INITIAL_DRAFT, // First time document is created 
	UPDATED_DRAFT, // Document which has status as 'INITIAL_DRAFT' (not Approved yet) and being updated recently 
	UPDATE_IN_PROGRESS, //  'PUBLISHED' document that is being updated
	DRAFT_REJECTED, // 'DRAFT' (either 'INITIAL_DRAFT' or 'UPDATED_DRAFT') document which has been rejected
	UPDATE_REJECTED, // 'UPDATE_IN_PROGRESS' document which has been rejected
	PUBLISHED; // Document that is already approved by the 'Supervisor/Admin'
}