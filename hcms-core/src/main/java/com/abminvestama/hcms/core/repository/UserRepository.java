package com.abminvestama.hcms.core.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.User;

/**
 * 
 * This class is responsible to handle all database operations againts the User object.
 * 
 * @see User
 * 
 * @since 1.0.0
 * @version 1.0.5
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add findByBukrsAndRole method</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add findSubordinateByEmployeeName method</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add findByEmployeeName, findAllByEmployeeName method</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add findAdminByBukrs method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findByPernr method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface UserRepository extends CrudRepository<User, String> {
	
	String QUERY_BY_TO_POSITION_ID = 
		"WITH RECURSIVE nodes(from_position_id, to_position_id) AS (" +
		    "SELECT s1.from_position_id, s1.to_position_id " +
		    	"FROM position_relations s1 " +
		    	"WHERE s1.to_position_id = :toPositionId " +
		    "UNION " +
		    "SELECT s2.from_position_id, s2.to_position_id " + 
		    	"FROM position_relations s2, nodes s1 " +  
		    	"WHERE s2.to_position_id = s1.from_position_id " + 
		") " + 
		"SELECT emp.pernr FROM nodes " +  
		"INNER JOIN it0001 AS spv ON spv.m_position_id = nodes.to_position_id AND spv.endda = '9999-12-31' " + 
		"INNER JOIN it0001 AS emp ON emp.m_position_id = nodes.from_position_id AND emp.endda = '9999-12-31' AND emp.bukrs = spv.bukrs";

	String QUERY_ADMIN_BY_BUKRS = "select A.* from m_user A " +
	        "inner join user_roles B on A.id = B.m_user_id " +
			"inner join m_role C on B.m_role_id = C.id " +
	        "inner join it0001 D on A.pernr = D.pernr " +
			"where C.role_name = 'ROLE_ADMIN' and A.endda = '9999-12-31' and D.bukrs = :bukrs";
	
	String QUERY_BY_SUPERIOR = "SELECT U.* FROM m_user U INNER JOIN it0001 A ON U.pernr = A.pernr " + 
			"INNER JOIN position_relations B ON A.m_position_id = B.from_position_id " +
			"WHERE A.bukrs = :bukrs and A.endda = '9999-12-31' and A.sname like :name and B.to_position_id = :superiorPosition " +
			"ORDER BY A.sname";
	
	String QUERY_BY_BUKRS_AND_ROLE = "select A.* from m_user A " +
	        "inner join user_roles B on A.id = B.m_user_id " +
			"inner join m_role C on B.m_role_id = C.id " +
	        "inner join it0001 D on A.pernr = D.pernr " +
			"where C.role_name = :roleName and A.endda = '9999-12-31' and D.bukrs = :bukrs";
	
	@Query("FROM User u")
	List<User> fetchAllUsers();
	
	User findByUsername(String username);
	
	User findByEmail(String email);
	
	@Query("FROM User u WHERE u.employee.pernr = :pernr ORDER BY u.employee.id.endda DESC, u.employee.id.begda DESC")
	List<User> findByPernr(@Param("pernr") Long pernr);

	@Query(nativeQuery=true, value=QUERY_BY_TO_POSITION_ID)
	List<Long> findPernrByToPositionId(@Param("toPositionId") String toPositionId);

	@Query(nativeQuery=true, value=QUERY_ADMIN_BY_BUKRS)
	List<User> findAdminByBukrs(@Param("bukrs") String bukrs);
	
	@Query(nativeQuery=true, value=QUERY_BY_BUKRS_AND_ROLE)
	List<User> findByBukrsAndRole(@Param("bukrs") String bukrs, @Param("roleName") String roleName);
	
	@Query("FROM User u WHERE u.employee.t500p.bukrs.bukrs = :bukrs AND u.employee.sname like :name ORDER BY u.employee.sname")
	List<User> findByEmployeeName(@Param("bukrs") String bukrs, @Param("name") String employeeName);
	
	@Query("FROM User u WHERE u.employee.sname like :name ORDER BY u.employee.sname")
	List<User> findAllByEmployeeName(@Param("name") String employeeName);
	
	@Query(nativeQuery=true, value=QUERY_BY_SUPERIOR)
	List<User> findSubordinateByEmployeeName(@Param("bukrs") String bukrs, @Param("name") String employeeName, @Param("superiorPosition") String superiorPosition);
}