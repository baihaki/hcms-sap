package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add methods: countByPostAndBukrs, countByJobAndBukrs, countByEmpAndBukrs</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0001QueryService extends DatabaseQueryService<IT0001, ITCompositeKeysNoSubtype>, DatabasePaginationQueryService {

	@NotNull
	Optional<IT0001> findOneByCompositeKey(Long pernr, Date endda, Date begda);
	
	@NotNull
	Collection<IT0001> findByPernr(Long pernr);		
	
	@NotNull
	Collection<IT0001> findByPositionId(String positionId);

	@NotNull
	Long countByPostAndBukrs(String persa, String bukrs);
	
	@NotNull
	Long countByJobAndBukrs(String stell, String bukrs);

	@NotNull
	Long countByEmpAndBukrs(String persg, String bukrs);
}