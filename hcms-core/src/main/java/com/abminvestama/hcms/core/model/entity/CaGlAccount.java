package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * Class that represents <strong>GL Account</strong> object.
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 3.0.0 (Cash Advance)
 * @since 3.0.0
 *
 */
@Entity
@Table(name = "ca_gl_account")
public class CaGlAccount implements Serializable {
	
	private static final long serialVersionUID = -2128967398350368185L;
	
	@Id
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "gl_acct_text")
	private String glAcctText;
	
	@ManyToOne
	@JoinColumn(name = "group_acct_id", referencedColumnName = "id")
	private CaGroupAccount groupAccount;
	
	@Column(name = "balance_sheet_acct")
	private boolean balanceSheetAcct;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "begda")
	private Date begda;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "endda")
	private Date endda; 
	
	public void setId(Long id){
		this.id = id;
	}
 
	public String getGlAcctText(){
		return glAcctText;
	}
	
	public void setGlAcctText(String glAcctText){
		this.glAcctText = glAcctText;
	}
 
	/**
	 * GET Group Account.
	 * 
	 * @return {@link CaGroupAccount} object
	 */
	public CaGroupAccount getGroupAccount(){
		return groupAccount;
	}
	
	/**
	 * SET Group Account.
	 * 
	 * @param groupAccount the Group Account object
	 */
	public void setGroupAccount(CaGroupAccount groupAccount){
		this.groupAccount = groupAccount;
	}
 
	public boolean getBalanceSheetAcct(){
		return balanceSheetAcct;
	}
	
	public void setBalanceSheetAcct(boolean balanceSheetAcct){
		this.balanceSheetAcct = balanceSheetAcct;
	}
 
	public Date getBegda(){
		return begda;
	}
	
	public void setBegda(Date begda){
		this.begda = begda;
	}
 
	public Date getEndda(){
		return endda;
	}
	
	public void setEndda(Date endda){
		this.endda = endda;
	}
	
	public CaGlAccount(){
	}

	public CaGlAccount( Long id,  String glAcctText,  CaGroupAccount groupAccount,  boolean balanceSheetAcct,  Date begda,  Date endda ){
	 	this.id = id;
	 	this.glAcctText = glAcctText;
	 	this.groupAccount = groupAccount;
	 	this.balanceSheetAcct = balanceSheetAcct;
	 	this.begda = begda;
	 	this.endda = endda;
	}
	
	public Long getId() {
		return  id;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		CaGlAccount caGlAccount = (CaGlAccount) o;
		 
		if (id != null ? !id.equals(caGlAccount.id) : caGlAccount.id != null) return false; 
		if (glAcctText != null ? !glAcctText.equals(caGlAccount.glAcctText) : caGlAccount.glAcctText != null) return false; 
		if (groupAccount != null ? !groupAccount.equals(caGlAccount.groupAccount) : caGlAccount.groupAccount != null) return false; 
		if (balanceSheetAcct != caGlAccount.balanceSheetAcct) return false;  
		if (begda != null ? !begda.equals(caGlAccount.begda) : caGlAccount.begda != null) return false; 
		if (endda != null ? !endda.equals(caGlAccount.endda) : caGlAccount.endda != null) return false;
		return true; 
	}
	
	@Override
	public int hashCode() {
	    int result = (id != null ?  id.hashCode() : 0); 
		result = 31 * (glAcctText != null ?  glAcctText.hashCode() : 0); 
		result = 31 * (groupAccount != null ?  groupAccount.hashCode() : 0); 
		result = 31 * result + (balanceSheetAcct ? 1 : 0); 	result = 31 * (begda != null ?  begda.hashCode() : 0); 
		result = 31 * (endda != null ?  endda.hashCode() : 0); 
	 
		return result; 
	}
	
	@Override
	public String toString() {
		return "caGlAccount(id="+id+",glAcctText="+glAcctText+",groupAccount="+groupAccount+",balanceSheetAcct="+balanceSheetAcct+",begda="+begda+",endda="+endda+')'; 
	}

}

	