package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;


public interface ZmedCostypeCommandService extends DatabaseCommandService<ZmedCostype> {
}