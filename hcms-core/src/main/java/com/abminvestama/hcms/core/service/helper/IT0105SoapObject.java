package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.IT0105;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0105SoapObject {

	private String employeenumber;
	private String validitybegin;
	private String validityend;
	
	private String subty;
	private String usridLong;
	private String recordnumber;
	
	private IT0105SoapObject() {}
	
	public IT0105SoapObject(IT0105 object) {
		if (object == null) {
			new IT0105SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			subty = object.getId().getSubty();
			usridLong = object.getUsridLong();
			recordnumber = object.getSeqnr().toString();
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}

	public String getSubty() {
		return subty;
	}

	public String getUsridLong() {
		return usridLong;
	}

	public String getRecordnumber() {
		return recordnumber;
	}

}
