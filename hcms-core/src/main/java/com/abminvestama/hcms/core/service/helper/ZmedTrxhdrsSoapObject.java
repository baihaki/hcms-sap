package com.abminvestama.hcms.core.service.helper;

import java.util.List;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;

/**
 * 
 * @since 2.0.0
 * @version 2.0.1
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add field: year</td></tr>
 *     <tr><td>2.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public class ZmedTrxhdrsSoapObject {
	
	private String bukrs;
	public String year;
	private List<ZmedTrxhdrSoapObject> listTrxHeader;
	
	private ZmedTrxhdrsSoapObject() {}
	
	public ZmedTrxhdrsSoapObject(String bukrs) {
		new ZmedTrxhdrsSoapObject();
		this.bukrs = bukrs;
	}
	
	public ZmedTrxhdrsSoapObject(String bukrs, String year) {
		new ZmedTrxhdrsSoapObject();
		this.bukrs = bukrs;
		this.year = year;
	}

	public String getBukrs() {
		return bukrs;
	}

	public String getYear() {
		return year;
	}

	public List<ZmedTrxhdrSoapObject> getListTrxHeader() {
		return listTrxHeader;
	}

	public void setListTrxHeader(List<ZmedTrxhdrSoapObject> listTrxHeader) {
		this.listTrxHeader = listTrxHeader;
	}
	
}
