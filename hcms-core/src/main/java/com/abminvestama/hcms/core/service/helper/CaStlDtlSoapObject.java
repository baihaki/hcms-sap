package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.core.model.entity.CaStlDtl;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 3.0.0
 * @since 3.0.0
 *
 */
public class CaStlDtlSoapObject {
	
	private String glaccount;
	private String kostl = "0000000000"; // 10 Character
	private String amount;
	private String order;
	private String description;
	
	private CaStlDtlSoapObject() {}
	
	public CaStlDtlSoapObject(String glaccount, String kostl, String amount, String order, String description) {
		this.glaccount = glaccount;
		this.kostl = kostl;
		this.amount = amount;
		this.order = order;
		this.description = description;
	}
	
	public CaStlDtlSoapObject(String glaccount, String kostl, String amount) {
		this.glaccount = glaccount;
		this.kostl = kostl;
		this.amount = amount;
	}
	public CaStlDtlSoapObject(CaStlDtl object) {
		if (object == null) {
			new CaStlDtlSoapObject();
		} else {
			//mandt = object.getMandt();
			//pernr = object.getPernr().toString();
			//reinr = object.getReinr().toString();
			//perio = object.getPerio().toString();
			glaccount = object.getOrderId().getGlAccount().getId().toString();
			// KOSTL [10 Character]
			kostl = kostl.concat(object.getOrderId().getCskt().getKostl());
			kostl = kostl.substring((kostl.length() - 10), kostl.length());
			amount = object.getAmount() != null && object.getAmount() > 0 ? String.valueOf(object.getAmount()) : "0";
			order = object.getOrderId().getId() + "";
			description = object.getDescription();
		}		
	}

	public String getGlaccount() {
		return glaccount;
	}

	public String getKostl() {
		return kostl;
	}

	public String getAmount() {
		return amount;
	}

	public String getOrder() {
		return order == null ? "" : order;
	}

	public String getDescription() {
		return description == null ? "" : description;
	}

}
