package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0023;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0023SoapObject {
	
	private String employeenumber;
	private String validitybegin;
	private String validityend;
	private String seqnr;
	private String arbgb;
	private String ort01;
	private String land1;
	private String branc;
	private String taete;
	private String ansvx;
	
	private IT0023SoapObject() {}
	
	public IT0023SoapObject(IT0023 object) {
		if (object == null) {
			new IT0023SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			//seqnr = object.getSeqnr().toString();
			arbgb = object.getArbgb();
			ort01 = object.getOrt01();
			land1 = object.getLand1().getLand1();
			branc = object.getBranc().getBrsch();
			taete = object.getTaete().getTaete().toString();
			//ansvx = object.getAnsvx(); Require SAP table t542c (Work Contract codes)
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}


	public String getSeqnr() {
		return seqnr;
	}

	public String getArbgb() {
		return arbgb;
	}

	public String getOrt01() {
		return ort01;
	}

	public String getLand1() {
		return land1;
	}

	public String getBranc() {
		return branc;
	}

	public String getTaete() {
		return taete;
	}

	public String getAnsvx() {
		return ansvx;
	}

}
