package com.abminvestama.hcms.core.model.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "zmed_quotaused")
public class ZmedQuotaused extends SAPAbstractEntityZmed<ZmedCompositeKeyQuotaused> {
	
	private static final long serialVersionUID = 2496232738510047249L;

	public ZmedQuotaused() {}
	

	public ZmedQuotaused(ZmedCompositeKeyQuotaused id) {
		this();
		this.id = id;
	}

	@Column(name = "gjahr", nullable = false, insertable = false, updatable = false)
	private Integer gjahr;

	@ManyToOne
	@JoinColumn(name = "bukrs", referencedColumnName = "bukrs", nullable = false, insertable = false, updatable = false)
	private T001 bukrs;
	//@Column(name = "bukrs", nullable = false, insertable = false, updatable = false)
	//private String bukrs;
	
	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;

	@Column(name = "kodequo", nullable = false, insertable = false, updatable = false)
	private String kodequo;

	@Column(name = "quotamt")
	private Double quotamt;

	public Integer getGjahr() {
		return gjahr;
	}

	public void setGjahr(Integer gjahr) {
		this.gjahr = gjahr;
	}

	public T001 getBukrs() {
		return bukrs;
	}

	public void setBukrs(T001 bukrs) {
		this.bukrs = bukrs;
	}

	public Long getPernr() {
		return pernr;
	}

	public void setPernr(Long pernr) {
		this.pernr = pernr;
	}

	public String getKodequo() {
		return kodequo;
	}

	public void setKodequo(String kodequo) {
		this.kodequo = kodequo;
	}

	public Double getQuotamt() {
		return quotamt;
	}

	public void setQuotamt(Double quotamt) {
		this.quotamt = quotamt;
	}
	
}
