package com.abminvestama.hcms.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.abminvestama.hcms.core.model.entity.T005U;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface T005URepository extends CrudRepository<T005U, String> {

	Page<T005U> findAll(Pageable pageRequest);
}