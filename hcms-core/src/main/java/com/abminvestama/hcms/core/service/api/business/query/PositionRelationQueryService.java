package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.PositionRelation;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface PositionRelationQueryService extends DatabaseQueryService<PositionRelation, String>, DatabasePaginationQueryService {

	@NotNull
	Page<PositionRelation> fetchAllWithPaging(int pageNumber);
	
	@NotNull
	Collection<PositionRelation> findByFromPositionid(String fromPositionId);
	
	@NotNull
	Collection<PositionRelation> findByToPositionId(String toPositionId);
	
	@NotNull
	Page<PositionRelation> findByFromPositionId(String fromPositionId, int pageNumber);
	
	@NotNull
	Page<PositionRelation> findByToPositionId(String toPositionId, int pageNumber);
	
	@NotNull
	Collection<PositionRelation> findByFromPositionAndToPosition(final String fromPositionId, final String toPositionId);	
	
	@NotNull
	Collection<PositionRelation> findByFromPositionAndToPositionAndRelationType(final String fromPositionId,
			final String toPoisitionId, final String relationType);
}