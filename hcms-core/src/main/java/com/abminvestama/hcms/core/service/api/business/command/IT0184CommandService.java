package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.IT0184;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 * 
 * This class is a representation of 'C' in <strong>CQRS</strong> pattern (<i>Command Query Responsibility Segregation</i>). 
 * This class contains all command operations for object <code>IT0184</code> in the system.
 *
 */
public interface IT0184CommandService extends DatabaseCommandService<IT0184> {
}