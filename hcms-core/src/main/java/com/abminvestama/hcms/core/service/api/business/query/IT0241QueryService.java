package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.IT0241;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add findByStatus with employee parameter</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add findByPernrAndStatus method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findByStatus method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0241QueryService extends DatabaseQueryService<IT0241, ITCompositeKeysNoSubtype>, DatabasePaginationQueryService {
	
	@NotNull
	Optional<IT0241> findOneByCompositeKey(Long pernr, Date endda, Date begda);
	
	@NotNull
	Collection<IT0241> findByPernr(Long pernr);		
	
	@NotNull
	Page<IT0241> findByStatus(String status, int pageNumber);
	
	@NotNull
	Page<IT0241> findByPernrAndStatus(long pernr, String status, int pageNumber);

	Page<IT0241> findByStatus(String status, int pageNumber, IT0001 employee);
}