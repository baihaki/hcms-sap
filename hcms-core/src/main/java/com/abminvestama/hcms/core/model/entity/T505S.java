package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 * 
 * Class that represents master data for <strong>Ethnicity</strong> (i.e. T505S in SAP). 
 *
 */
@Entity
@Table(name = "t505s")
public class T505S implements Serializable {

	private static final long serialVersionUID = 7647045490019818540L;
	
	@EmbeddedId
	private T505SKey id;
	
	@Column(name = "ltext", nullable = false)
	private String ltext;
	
	public T505S() {}
	
	public T505S(T505SKey id) {
		this();
		this.id = id;
	}
	
	public T505SKey getId() {
		return id;
	}
	
	public String getLtext() {
		return ltext;
	}	
}