package com.abminvestama.hcms.core.repository;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.CaStlHdr;

/**
 * 
 * @author wijanarko (wijanarko@gmx.com)
 * @version 1.0.0
 * @since 1.0.0
 * 
 * Cash Advance Settlement Header Repository (DAO) Interface.
 *
 */
public interface CaStlHdrRepository extends CrudRepository<CaStlHdr, String> {
	
	String FIND_BY_TO_POSITION_ID_COUNT = "SELECT COUNT(*) " +
			"FROM position_relations A " +
			"INNER JOIN it0001 B ON B.m_position_id = A.from_position_id " +
			"INNER JOIN m_user C ON C.pernr = B.pernr " +
			"INNER JOIN ca_stl_hdr D ON D.pernr = C.pernr " +
			"WHERE B.endda = '9999-12-31' AND A.to_position_id = :toPositionId ";
	
	String FIND_BY_TO_POSITION_ID_QUERY = "SELECT D.* " +
			"FROM position_relations A " +
			"INNER JOIN it0001 B ON B.m_position_id = A.from_position_id " +
			"INNER JOIN m_user C ON C.pernr = B.pernr " +
			"INNER JOIN ca_stl_hdr D ON D.pernr = C.pernr " +
			"WHERE B.endda = '9999-12-31' AND A.to_position_id = :toPositionId " +
				"ORDER BY D.created_at DESC LIMIT :limit OFFSET :offset";

	String FIND_BY_PROCESS_AND_TO_POSITION_ID_COUNT = "SELECT COUNT(*) " +
			"FROM position_relations A " +
			"INNER JOIN it0001 B ON B.m_position_id = A.from_position_id " +
			"INNER JOIN m_user C ON C.pernr = B.pernr " +
			"INNER JOIN ca_stl_hdr D ON D.pernr = C.pernr " +
			"WHERE B.endda = '9999-12-31' AND D.process = :process AND A.to_position_id = :toPositionId ";
	
	String FIND_BY_PROCESS_AND_TO_POSITION_ID_QUERY = "SELECT D.* " +
			"FROM position_relations A " +
			"INNER JOIN it0001 B ON B.m_position_id = A.from_position_id " +
			"INNER JOIN m_user C ON C.pernr = B.pernr " +
			"INNER JOIN ca_stl_hdr D ON D.pernr = C.pernr " +
			"WHERE B.endda = '9999-12-31' AND D.process = :process AND A.to_position_id = :toPositionId " +
				"ORDER BY D.created_at DESC LIMIT :limit OFFSET :offset";
	
	String FIND_BY_PERNR_AND_PROCESS_AND_TO_POSITION_ID_COUNT = "SELECT COUNT(*) " +
			"FROM position_relations A " +
			"INNER JOIN it0001 B ON B.m_position_id = A.from_position_id " +
			"INNER JOIN m_user C ON C.pernr = B.pernr " +
			"INNER JOIN ca_stl_hdr D ON D.pernr = C.pernr " +
			"WHERE B.endda = '9999-12-31' " +
				"AND (C.pernr = :pernr OR :pernr IS NULL) " + 
				"AND (D.process = :process OR :process IS NULL) " + 
				"AND A.to_position_id = :toPositionId ";
	
	String FIND_BY_PERNR_AND_PROCESS_AND_TO_POSITION_ID_QUERY = "SELECT D.* " +
			"FROM position_relations A " +
			"INNER JOIN it0001 B ON B.m_position_id = A.from_position_id " +
			"INNER JOIN m_user C ON C.pernr = B.pernr " +
			"INNER JOIN ca_stl_hdr D ON D.pernr = C.pernr " +
			"WHERE B.endda = '9999-12-31' " +
				"AND (C.pernr = :pernr OR :pernr IS NULL) " + 
				"AND (D.process = :process OR :process IS NULL) " + 
				"AND A.to_position_id = :toPositionId " +
			"ORDER BY D.created_at DESC LIMIT :limit OFFSET :offset";
	
	@Query("FROM CaStlHdr caStlHdr WHERE caStlHdr.reqNo = :reqNo")
	CaStlHdr findByReqno(@Param("reqNo") String reqNo);

	@Query("FROM CaStlHdr caStlHdr WHERE caStlHdr.tcaNo.reqNo = :tcaNo")
	CaStlHdr findByTcano(@Param("tcaNo") String tcaNo);

	Page<CaStlHdr> findAll(Pageable pageRequest);

	@Query("FROM CaStlHdr caStlHdr WHERE caStlHdr.pernr = :pernr")
	Page<CaStlHdr> findByPernr(@Param("pernr") Long pernr, Pageable pageRequest);
	
	@Query("FROM CaStlHdr caStlHdr WHERE caStlHdr.pernr IN :pernrs")
	Page<CaStlHdr> findAllByCriteriaWithPernrs(@Param("pernrs") Long[] pernrs, Pageable pageRequest);

	@Query("FROM CaStlHdr caStlHdr WHERE caStlHdr.pernr IN :pernrs AND caStlHdr.process = :process")
	Page<CaStlHdr> findAllByCriteriaWithPernrs(@Param("pernrs") Long[] pernrs, @Param("process") Integer process, Pageable pageRequest);

	@Query("FROM CaStlHdr caStlHdr WHERE caStlHdr.process IN :processes")
	Page<CaStlHdr> findAllByCriteriaWithProcesses(@Param("processes") Integer[] processes, Pageable pageRequest);

	@Query("FROM CaStlHdr caStlHdr WHERE caStlHdr.pernr = :pernr AND caStlHdr.process IN :processes")
	Page<CaStlHdr> findAllByCriteriaWithProcesses(@Param("pernr") Long pernr, @Param("processes") Integer[] processes, Pageable pageRequest);

	@Query("FROM CaStlHdr caStlHdr WHERE caStlHdr.process = :process")
	Page<CaStlHdr> findAllByCriteria(@Param("process") Integer process, Pageable pageRequest);
	
	@Query("FROM CaStlHdr caStlHdr WHERE caStlHdr.pernr = :pernr AND caStlHdr.process = :process")
	Page<CaStlHdr> findAllByCriteria(@Param("pernr") Long pernr, @Param("process") Integer process, Pageable pageRequest);
	
	@Query(nativeQuery=true, value=FIND_BY_TO_POSITION_ID_COUNT)
	int findSubOrdinatesByCriteriaCount(@Param("toPositionId") String toPositionId);

	@Query(nativeQuery=true, value=FIND_BY_TO_POSITION_ID_QUERY)
	Collection<CaStlHdr> findSubOrdinatesByCriteriaQuery(@Param("toPositionId") String toPositionId,
			@Param("limit") int limit, @Param("offset") int offset);

	@Query(nativeQuery=true, value=FIND_BY_PROCESS_AND_TO_POSITION_ID_COUNT)
	int findSubOrdinatesByCriteriaCount(@Param("process") Integer process, @Param("toPositionId") String toPositionId);

	@Query(nativeQuery=true, value=FIND_BY_PROCESS_AND_TO_POSITION_ID_QUERY)
	Collection<CaStlHdr> findSubOrdinatesByCriteriaQuery(@Param("process") Integer process,
			@Param("toPositionId") String toPositionId, @Param("limit") int limit, @Param("offset") int offset);

	@Query(nativeQuery=true, value=FIND_BY_PERNR_AND_PROCESS_AND_TO_POSITION_ID_COUNT)
	int findSubOrdinatesByCriteriaCount(@Param("pernr") Long pernr, @Param("process") Integer process,
			@Param("toPositionId") String toPositionId);

	@Query(nativeQuery=true, value=FIND_BY_PERNR_AND_PROCESS_AND_TO_POSITION_ID_QUERY)
	Collection<CaStlHdr> findSubOrdinatesByCriteriaQuery(@Param("pernr") Long pernr,
			@Param("process") Integer process, @Param("toPositionId") String toPositionId, @Param("limit") int limit,
			@Param("offset") int offset);

}
