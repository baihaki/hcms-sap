package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0041;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0041SoapObject {
	
	private String employeenumber;
	private String validitybegin;
	private String validityend;
	private String seqnr;
	private String dar01;
	private String dat01;
	private String dar02;
	private String dat02;
	private String dar03;
	private String dat03;
	private String dar04;
	private String dat04;
	private String dar05;
	private String dat05;
	private String dar06;
	private String dat06;
	private String dar07;
	private String dat07;
	private String dar08;
	private String dat08;
	private String dar09;
	private String dat09;
	private String dar10;
	private String dat10;
	private String dar11;
	private String dat11;
	private String dar12;
	private String dat12;

	private IT0041SoapObject() {}
	
	public IT0041SoapObject(IT0041 object) {
		if (object == null) {
			new IT0041SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			seqnr = object.getSeqnr().toString();
			dat01 = CommonDateFunction.convertDateToStringYMD(object.getDat01());
			dar01 = object.getDar01().getDatar();
			dat02 = CommonDateFunction.convertDateToStringYMD(object.getDat02());
			dar02 = object.getDar02().getDatar();
			dat03 = CommonDateFunction.convertDateToStringYMD(object.getDat03());
			dar03 = object.getDar03().getDatar();
			dat04 = CommonDateFunction.convertDateToStringYMD(object.getDat04());
			dar04 = object.getDar04().getDatar();
			dat05 = CommonDateFunction.convertDateToStringYMD(object.getDat05());
			dar05 = object.getDar05().getDatar();
			dat06 = CommonDateFunction.convertDateToStringYMD(object.getDat06());
			dar06 = object.getDar06().getDatar();
			dat07 = CommonDateFunction.convertDateToStringYMD(object.getDat07());
			dar07 = object.getDar07().getDatar();
			dat08 = CommonDateFunction.convertDateToStringYMD(object.getDat08());
			dar08 = object.getDar08().getDatar();
			dat09 = CommonDateFunction.convertDateToStringYMD(object.getDat09());
			dar09 = object.getDar09().getDatar();
			dat10 = CommonDateFunction.convertDateToStringYMD(object.getDat10());
			dar10 = object.getDar10().getDatar();
			dat11 = CommonDateFunction.convertDateToStringYMD(object.getDat11());
			dar11 = object.getDar11().getDatar();
			dat12 = CommonDateFunction.convertDateToStringYMD(object.getDat12());
			dar12 = object.getDar12().getDatar();
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}

	
	public String getSeqnr() {
		return seqnr;
	}

	public String getDar01() {
		return dar01;
	}

	public String getDat01() {
		return dat01;
	}

	public String getDar02() {
		return dar02;
	}

	public String getDat02() {
		return dat02;
	}

	public String getDar03() {
		return dar03;
	}

	public String getDat03() {
		return dat03;
	}

	public String getDar04() {
		return dar04;
	}

	public String getDat04() {
		return dat04;
	}

	public String getDar05() {
		return dar05;
	}

	public String getDat05() {
		return dat05;
	}

	public String getDar06() {
		return dar06;
	}

	public String getDat06() {
		return dat06;
	}

	public String getDar07() {
		return dar07;
	}

	public String getDat07() {
		return dat07;
	}

	public String getDar08() {
		return dar08;
	}

	public String getDat08() {
		return dat08;
	}

	public String getDar09() {
		return dar09;
	}

	public String getDat09() {
		return dat09;
	}

	public String getDar10() {
		return dar10;
	}

	public String getDat10() {
		return dat10;
	}

	public String getDar11() {
		return dar11;
	}

	public String getDat11() {
		return dat11;
	}

	public String getDar12() {
		return dar12;
	}

	public String getDat12() {
		return dat12;
	}


}
