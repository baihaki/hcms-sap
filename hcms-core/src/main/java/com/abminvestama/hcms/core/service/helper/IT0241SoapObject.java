package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0241;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0241SoapObject {
	
	private String employeenumber;
	private String validitybegin;
	private String validityend;
	private String taxId;
	private String marrd;
	private String spben;
	private String refno;
	private String depnd;
	private String rdate;
	
	
	private IT0241SoapObject() {}
	
	public IT0241SoapObject(IT0241 object) {
		if (object == null) {
			new IT0241SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			taxId = object.getTaxId();
			marrd = object.getMarrd();
			rdate = CommonDateFunction.convertDateToStringYMD(object.getRdate());
			spben = object.getSpben();
			refno = object.getRefno();
			depnd = object.getDepnd();
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}

	public String getTaxId() {
		return taxId;
	}

	public String getMarrd() {
		return marrd;
	}

	public String getSpben() {
		return spben;
	}

	public String getRefno() {
		return refno;
	}

	public String getDepnd() {
		return depnd;
	}

	public String getRdate() {
		return rdate;
	}


}
