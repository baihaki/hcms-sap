package com.abminvestama.hcms.core.model.entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Class that represents master data for <strong>Employee Cost Header</strong> (i.e. ptrv_head in SAP).
 * 
 * @since 1.0.0
 * @version 1.0.9
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.9</td><td>Baihaki</td><td>Add fields: totalAmount, rejectAmounts</td></tr>
 *     <tr><td>1.0.8</td><td>Baihaki</td><td>Add fields: banka, bankn</td></tr>
 *     <tr><td>1.0.7</td><td>Baihaki</td><td>Add fields for synchronization of document status: paymentStatus2, docno2</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Add fields for synchronization of document status: paymentStatus, paymentDate, docno, lastSync</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add fields: reason</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add fields: lastPrevMonth, lastThisMonth</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add fields: createdBy, createdAt, superiorApprovedBy, superiorApprovedAt, adminApprovedBy, adminApprovedAt</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add fields: bukrs, process, form</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add field: kunde</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 * 
 */
@Entity
@Table(name = "ptrv_head")
public class PtrvHead extends SAPAbstractEntityZmed<PtrvHeadKey> {

	private static final long serialVersionUID = 2496232738510047249L;

	public PtrvHead() {}

	public PtrvHead(PtrvHeadKey id) {
		this();
		this.id = id;
	}
	
	
	@Column(name = "hdvrs", nullable = false, insertable = false, updatable = false)
	private Integer hdvrs;
	
	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;
	
	@Column(name = "reinr", nullable = false, insertable = false, updatable = false)
	private Long reinr;
	
	

	@ManyToOne
	@JoinColumn(name = "molga", referencedColumnName = "molga")
	private T500L molga;
	//@Column(name = "molga")
	//private String molga;

	@ManyToOne
	@JoinColumn(name = "morei", referencedColumnName = "morei")
	private T702N morei;
	//@Column(name = "morei")
	//private String morei;

	/*@ManyToOne
	@JoinColumn(name = "schem", referencedColumnName = "schem")
	private T706S schem;*/
	@Column(name = "schem")
	private String schem;

	/*@ManyToOne
	@JoinColumn(name = "zland", referencedColumnName = "land1")
	private T702O zland;*/
	@Column(name = "zland")
	private String zland;

	@Column(name = "zort1")
	private String zort1;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datv1")
	private Date datv1;

	@Temporal(TemporalType.TIME)
	@Column(name = "uhrv1")
	private Date uhrv1;

	@Temporal(TemporalType.DATE)
	@Column(name = "datb1")
	private Date datb1;

	@Temporal(TemporalType.TIME)
	@Column(name = "uhrb1")
	private Date uhrb1;

	@Temporal(TemporalType.DATE)
	@Column(name = "dath1")
	private Date dath1;

	@Temporal(TemporalType.TIME)
	@Column(name = "uhrh1")
	private Date uhrh1;

	@Temporal(TemporalType.DATE)
	@Column(name = "datr1")
	private Date datr1;

	@Temporal(TemporalType.TIME)
	@Column(name = "uhrr1")
	private Date uhrr1;
	

	@Temporal(TemporalType.DATE)
	@Column(name = "endrg")
	private Date endrg;

	@Temporal(TemporalType.DATE)
	@Column(name = "dates")
	private Date dates;

	@Temporal(TemporalType.TIME)
	@Column(name = "times")
	private Date times;
	
	@Column(name = "uname")
	private String uname;

	@Column(name = "repid")
	private String repid;

	@Column(name = "dantn")
	private Long dantn;	

	@Column(name = "fintn")
	private Long fintn;

	@Column(name = "request")
	private String request;

	@Column(name = "travel_plan")
	private String travelPlan;

	@Column(name = "expenses")
	private String expenses;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "st_trgtg")
	private Date stTrgtg;

	@Temporal(TemporalType.DATE)
	@Column(name = "st_trgall")
	private Date stTrgall;

	@Temporal(TemporalType.DATE)
	@Column(name = "dat_reduc1")
	private Date datReduc1;

	@Temporal(TemporalType.DATE)
	@Column(name = "dat_reduc2")
	private Date datReduc2;

	@Temporal(TemporalType.DATE)
	@Column(name = "datv1_dienst")
	private Date datv1Dienst;

	@Temporal(TemporalType.TIME)
	@Column(name = "uhrv1_dienst")
	private Date urhv1Dienst;

	@Temporal(TemporalType.DATE)
	@Column(name = "datb1_dienst")
	private Date datb1Dienst;

	@Temporal(TemporalType.TIME)
	@Column(name = "uhrb1_dienst")
	private Date uhrb1Dienst;

	@Column(name = "abordnung")
	private Long abordnung;
	
	
	@Temporal(TemporalType.DATE)
	@Column(name = "exchange_date")
	private Date exchangeDate;
	
	@Column(name = "kunde")
	private String kunde;
	
	@ManyToOne
	@JoinColumn(name = "bukrs", referencedColumnName = "bukrs", nullable = false)
	private T001 bukrs;

	@Column(name = "process")
	private String process;
	
	@Column(name = "form_type")
	private short form;
	
	//@Column(name = "created_by")
	//private String createdBy;
	@ManyToOne
	@JoinColumn(name = "created_by", referencedColumnName = "id")
	private User createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	private Date createdAt;
	
	//@Column(name = "approved1_by")
	//private String superiorApprovedBy;
	@ManyToOne
	@JoinColumn(name = "approved1_by", referencedColumnName = "id")
	private User superiorApprovedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved1_at")
	private Date superiorApprovedAt;
	
	//@Column(name = "approved2_by")
	//private String adminApprovedBy;
	@ManyToOne
	@JoinColumn(name = "approved2_by", referencedColumnName = "id")
	private User adminApprovedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved2_at")
	private Date adminApprovedAt;
	
	@Column(name = "last_prev_month")
	private Double lastPrevMonth;

	@Column(name = "last_this_month")
	private Double lastThisMonth;

	@Column(name = "reason", length = 1024)
	private String reason;
	
	@Column(name = "payment_status")
	private String paymentStatus;

	@Temporal(TemporalType.DATE)
	@Column(name = "payment_date")
	private Date paymentDate;

	@Column(name = "docno")
	private String docno;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_sync")
	private Date lastSync;
	
	@Column(name = "payment_status2")
	private String paymentStatus2;
	
	@Column(name = "docno2")
	private String docno2;
	
	@Column(name = "banka", length = 60)
	private String banka;
	
	@Column(name = "bankn", length = 100)
	private String bankn;
	
	@Column(name = "total_amount")
	private Double totalAmount;
	
	@Column(name = "reject_amounts")
	private String rejectAmounts;
	
	public String getZort1() {
		return zort1;
	}

	public void setZort1(String zort1) {
		this.zort1 = zort1;
	}

	public Integer getHdvrs() {
		return hdvrs;
	}

	public void setHdvrs(Integer hdvrs) {
		this.hdvrs = hdvrs;
	}

	public Long getPernr() {
		return pernr;
	}

	public void setPernr(Long pernr) {
		this.pernr = pernr;
	}

	public Long getReinr() {
		return reinr;
	}

	public void setReinr(Long reinr) {
		this.reinr = reinr;
	}

	public T500L getMolga() {
		return molga;
	}

	public void setMolga(T500L molga) {
		this.molga = molga;
	}

	public T702N getMorei() {
		return morei;
	}

	public void setMorei(T702N morei) {
		this.morei = morei;
	}

	public String getSchem() {
		return schem;
	}

	public void setSchem(String schem) {
		this.schem = schem;
	}

	public String getZland() {
		return zland;
	}

	public void setZland(String zland) {
		this.zland = zland;
	}

	public Date getDatv1() {
		return datv1;
	}

	public void setDatv1(Date datv1) {
		this.datv1 = datv1;
	}

	public Date getUhrv1() {
		return uhrv1;
	}

	public void setUhrv1(Date uhrv1) {
		this.uhrv1 = uhrv1;
	}

	public Date getDatb1() {
		return datb1;
	}

	public void setDatb1(Date datb1) {
		this.datb1 = datb1;
	}

	public Date getUhrb1() {
		return uhrb1;
	}

	public void setUhrb1(Date uhrb1) {
		this.uhrb1 = uhrb1;
	}

	public Date getDath1() {
		return dath1;
	}

	public void setDath1(Date dath1) {
		this.dath1 = dath1;
	}

	public Date getUhrh1() {
		return uhrh1;
	}

	public void setUhrh1(Date uhrh1) {
		this.uhrh1 = uhrh1;
	}

	public Date getDatr1() {
		return datr1;
	}

	public void setDatr1(Date datr1) {
		this.datr1 = datr1;
	}

	public Date getUhrr1() {
		return uhrr1;
	}

	public void setUhrr1(Date uhrr1) {
		this.uhrr1 = uhrr1;
	}

	public Date getEndrg() {
		return endrg;
	}

	public void setEndrg(Date endrg) {
		this.endrg = endrg;
	}

	public Date getDates() {
		return dates;
	}

	public void setDates(Date dates) {
		this.dates = dates;
	}

	public Date getTimes() {
		return times;
	}

	public void setTimes(Date times) {
		this.times = times;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getRepid() {
		return repid;
	}

	public void setRepid(String repid) {
		this.repid = repid;
	}

	public Long getDantn() {
		return dantn;
	}

	public void setDantn(Long dantn) {
		this.dantn = dantn;
	}

	public Long getFintn() {
		return fintn;
	}

	public void setFintn(Long fintn) {
		this.fintn = fintn;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getTravelPlan() {
		return travelPlan;
	}

	public void setTravelPlan(String travelPlan) {
		this.travelPlan = travelPlan;
	}

	public String getExpenses() {
		return expenses;
	}

	public void setExpenses(String expenses) {
		this.expenses = expenses;
	}

	public Date getStTrgtg() {
		return stTrgtg;
	}

	public void setStTrgtg(Date stTrgtg) {
		this.stTrgtg = stTrgtg;
	}

	public Date getStTrgall() {
		return stTrgall;
	}

	public void setStTrgall(Date stTrgall) {
		this.stTrgall = stTrgall;
	}

	public Date getDatReduc1() {
		return datReduc1;
	}

	public void setDatReduc1(Date datReduc1) {
		this.datReduc1 = datReduc1;
	}

	public Date getDatReduc2() {
		return datReduc2;
	}

	public void setDatReduc2(Date datReduc2) {
		this.datReduc2 = datReduc2;
	}

	public Date getDatv1Dienst() {
		return datv1Dienst;
	}

	public void setDatv1Dienst(Date datv1Dienst) {
		this.datv1Dienst = datv1Dienst;
	}

	public Date getUrhv1Dienst() {
		return urhv1Dienst;
	}

	public void setUrhv1Dienst(Date urhv1Dienst) {
		this.urhv1Dienst = urhv1Dienst;
	}

	public Date getDatb1Dienst() {
		return datb1Dienst;
	}

	public void setDatb1Dienst(Date datb1Dienst) {
		this.datb1Dienst = datb1Dienst;
	}

	public Date getUhrb1Dienst() {
		return uhrb1Dienst;
	}

	public void setUhrb1Dienst(Date uhrb1Dienst) {
		this.uhrb1Dienst = uhrb1Dienst;
	}

	public Long getAbordnung() {
		return abordnung;
	}

	public void setAbordnung(Long abordnung) {
		this.abordnung = abordnung;
	}

	public Date getExchangeDate() {
		return exchangeDate;
	}

	public void setExchangeDate(Date exchangeDate) {
		this.exchangeDate = exchangeDate;
	}

	public String getKunde() {
		return kunde;
	}

	public void setKunde(String kunde) {
		this.kunde = kunde;
	}
	
	public T001 getBukrs() {
		return bukrs;
	}

	public void setBukrs(T001 bukrs) {
		this.bukrs = bukrs;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public short getForm() {
		return form;
	}

	public void setForm(short form) {
		this.form = form;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public User getSuperiorApprovedBy() {
		return superiorApprovedBy;
	}

	public void setSuperiorApprovedBy(User superiorApprovedBy) {
		this.superiorApprovedBy = superiorApprovedBy;
	}

	public Date getSuperiorApprovedAt() {
		return superiorApprovedAt;
	}

	public void setSuperiorApprovedAt(Date superiorApprovedAt) {
		this.superiorApprovedAt = superiorApprovedAt;
	}

	public User getAdminApprovedBy() {
		return adminApprovedBy;
	}

	public void setAdminApprovedBy(User adminApprovedBy) {
		this.adminApprovedBy = adminApprovedBy;
	}

	public Date getAdminApprovedAt() {
		return adminApprovedAt;
	}

	public void setAdminApprovedAt(Date adminApprovedAt) {
		this.adminApprovedAt = adminApprovedAt;
	}

	public Double getLastPrevMonth() {
		return lastPrevMonth;
	}

	public void setLastPrevMonth(Double lastPrevMonth) {
		this.lastPrevMonth = lastPrevMonth;
	}

	public Double getLastThisMonth() {
		return lastThisMonth;
	}

	public void setLastThisMonth(Double lastThisMonth) {
		this.lastThisMonth = lastThisMonth;
	}
	
	/**
	 * GET Reason for Revision.
	 * 
	 * @return
	 */
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getDocno() {
		return docno;
	}

	public void setDocno(String docno) {
		this.docno = docno;
	}

	public Date getLastSync() {
		return lastSync;
	}

	public void setLastSync(Date lastSync) {
		this.lastSync = lastSync;
	}

	public String getPaymentStatus2() {
		return paymentStatus2;
	}

	public void setPaymentStatus2(String paymentStatus2) {
		this.paymentStatus2 = paymentStatus2;
	}

	public String getDocno2() {
		return docno2;
	}

	public void setDocno2(String docno2) {
		this.docno2 = docno2;
	}

	/**
	 * GET Employee's Bank Name.
	 * 
	 * @return
	 */
	public String getBanka() {
		return banka;
	}

	public void setBanka(String banka) {
		this.banka = banka;
	}

	/**
	 * GET Employee's Bank Account Number.
	 * 
	 * @return
	 */
	public String getBankn() {
		return bankn;
	}

	public void setBankn(String bankn) {
		this.bankn = bankn;
	}

	/**
	 * GET Expense Total Amount.
	 * 
	 * @return
	 */
	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	/**
	 * GET Reject Amounts for each Expense Sub-Type.
	 * 
	 * @return
	 */
	public String getRejectAmounts() {
		return rejectAmounts;
	}

	public void setRejectAmounts(String rejectAmounts) {
		this.rejectAmounts = rejectAmounts;
	}
	
}

