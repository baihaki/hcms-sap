package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.abminvestama.hcms.core.model.entity.PtrvSrecKey;
import com.abminvestama.hcms.core.model.entity.PtrvSrec;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add method: findByTripnumber</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface PtrvSrecQueryService extends DatabaseQueryService<PtrvSrec, PtrvSrecKey>, DatabasePaginationQueryService{
	//Optional<PtrvSrec> findByEntityName(final String entityName);
	
	@NotNull
	Optional<PtrvSrec> findOneByCompositeKey(String mandt, Long pernr, Long reinr, Integer perio, String receiptno);
/*
	@NotNull
	Collection<PtrvSrec> findByKodemed(String bukrs, String kodemed, Date datbi);
	*/

	Collection<PtrvSrec> findByTripnumber(Long tripnumber);
}

