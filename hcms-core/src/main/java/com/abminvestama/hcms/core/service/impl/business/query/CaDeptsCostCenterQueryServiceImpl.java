package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.abminvestama.hcms.core.model.entity.CaDeptsCostCenter;
import com.abminvestama.hcms.core.repository.CaDeptsCostCenterRepository;
import com.abminvestama.hcms.core.service.api.business.query.CaDeptsCostCenterQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("caDeptsCostCenterQueryService")
public class CaDeptsCostCenterQueryServiceImpl implements CaDeptsCostCenterQueryService {
	CaDeptsCostCenterRepository caDeptsCostCenterRepository;

	@Autowired
	CaDeptsCostCenterQueryServiceImpl(CaDeptsCostCenterRepository caDeptsCostCenterRepository) {
		this.caDeptsCostCenterRepository = caDeptsCostCenterRepository;
	}

	@Override
	public Optional<CaDeptsCostCenter> findById(Optional<Long> id) throws Exception {
		return id.map(pk -> caDeptsCostCenterRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<CaDeptsCostCenter>> fetchAll() {
		List<CaDeptsCostCenter> listOfCaDeptsCostCenter = new ArrayList<>();
		Optional<Iterable<CaDeptsCostCenter>> bunchOfCaDeptsCostCenter = Optional.ofNullable(caDeptsCostCenterRepository.findAll());
		return bunchOfCaDeptsCostCenter.map(iter -> {
			iter.forEach(caDeptsCostCenter -> {
				listOfCaDeptsCostCenter.add(caDeptsCostCenter);
			});
			return listOfCaDeptsCostCenter;
		});
	}

	@Override
	public Page<CaDeptsCostCenter> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return caDeptsCostCenterRepository.findAll(pageRequest);
	}
}
