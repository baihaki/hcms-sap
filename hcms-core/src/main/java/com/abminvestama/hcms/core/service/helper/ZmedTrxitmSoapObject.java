package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class ZmedTrxitmSoapObject {
	
	private String itmno;
	private String bukrs;
	private String clmno;
	private String billdt;
	private String kodemed;
	private String kodecos;
	private String zcostyp;
	private String clmamt;
	private String rejamt;
	private String appamt;
	private String relat;
	private String umur;
	private String pasien;
	
	private ZmedTrxitmSoapObject() {}
	
	public ZmedTrxitmSoapObject(ZmedTrxitm object) {
		if (object == null) {
			new ZmedTrxitmSoapObject();
		} else {
			kodemed = object.getKodemed();
			kodecos = object.getKodecos();
			zcostyp = object.getZcostyp();
			clmamt = object.getClmamt() != null && object.getClmamt() > 0 ? String.valueOf(object.getClmamt().intValue()) : null;
			rejamt = object.getRejamt() != null && object.getRejamt() > 0 ? String.valueOf(object.getRejamt().intValue()) : null;
			appamt = object.getAppamt() != null && object.getAppamt() > 0 ? String.valueOf(object.getAppamt().intValue()) : null;
		}		
	}
	
	public String getKodemed() {
		return kodemed;
	}

	public String getKodecos() {
		return kodecos;
	}

	public String getClmamt() {
		return clmamt;
	}

	public String getRejamt() {
		return rejamt;
	}

	public String getAppamt() {
		return appamt;
	}

}
