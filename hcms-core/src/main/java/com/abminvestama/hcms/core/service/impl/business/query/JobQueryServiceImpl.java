package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.Job;
import com.abminvestama.hcms.core.repository.JobRepository;
import com.abminvestama.hcms.core.service.api.business.query.JobQueryService;

/**
 * 
 * @since 2.0.0
 * @version 2.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add findByCode method</td></tr>
 *     <tr><td>2.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("jobQueryService")
@Transactional(readOnly = true)
public class JobQueryServiceImpl implements JobQueryService {

	private JobRepository jobRepository;
	
	@Autowired
	JobQueryServiceImpl(JobRepository jobRepository) {
		this.jobRepository = jobRepository;
	}
	
	@Override
	public Optional<Job> findById(Optional<String> id) throws Exception {
		return id.map(data -> Optional.ofNullable(jobRepository.findOne(data))).orElse(Optional.empty());
	}

	@Override
	public Optional<Collection<Job>> fetchAll() {
		List<Job> jobList = new ArrayList<>();
		Optional<Iterable<Job>> jobIterable = Optional.ofNullable(jobRepository.findAll());
		return jobIterable.map(iter -> {
			iter.forEach(job -> jobList.add(job));
			return jobList;
		});
	}

	@Override
	public Page<Job> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return jobRepository.findAll(pageRequest);				
	}
	
	@Override
	public Optional<Job> findByCode(Optional<String> code) throws Exception {
		return code.map(data -> Optional.ofNullable(jobRepository.findByCode(data))).orElse(Optional.empty());
	}

}
