package com.abminvestama.hcms.core.service.impl.business.command;


import java.util.Date;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyTrxhdr;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.ZmedTrxhdrRepository;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.ZmedTrxhdrCommandService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Modify threadUpdate method: Add parameters (eventLogCommandService, eventLog, user)</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add UpdateThread, threadUpdate method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add updateZclmno method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("ZmedTrxhdrCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ZmedTrxhdrCommandServiceImpl implements ZmedTrxhdrCommandService {
	
	private ZmedTrxhdrRepository zmedTrxhdrRepository;
    
    class UpdateThread implements Runnable {
    	
    	ZmedTrxhdr zmedTrxhdr;
    	EventLogCommandService eventLogCommandService;
    	EventLog eventLog;
    	User user;
    	
    	UpdateThread(ZmedTrxhdr zmedTrxhdr, EventLogCommandService eventLogCommandService, EventLog eventLog, User user) {
    		this.zmedTrxhdr = zmedTrxhdr;
    		this.eventLogCommandService = eventLogCommandService;
    		this.eventLog = eventLog;
    		this.user = user;
    	}
    	
		@Override
		public void run() {
			try {
				Thread.sleep(2000);
				//ZmedCompositeKeyTrxhdr zmedTrxhdrKey = zmedTrxhdr.getId();
				//zmedTrxhdr.setId(new ZmedCompositeKeyTrxhdr(zmedTrxhdrKey.getBukrs(), zmedTrxhdr.getZclmno()));
				zmedTrxhdr = zmedTrxhdrRepository.save(zmedTrxhdr);
				eventLog.setStatus("newZclmno=" + zmedTrxhdr.getZclmno());
				eventLogCommandService.save(eventLog, user);
				System.out.println("UpdateThread succesfully saved " + zmedTrxhdr.toString());
			} catch (Exception e) {
	        	System.out.println("UpdateThread Exception:" + e.getMessage());
			}
		}
		
    }
    
	@Autowired
	ZmedTrxhdrCommandServiceImpl(ZmedTrxhdrRepository zmedTrxhdrRepository) {
		this.zmedTrxhdrRepository = zmedTrxhdrRepository;
	}
	
	@Override
	public Optional<ZmedTrxhdr> save(ZmedTrxhdr entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		//if (user != null) {
			//entity.setUname(user.getId());
		//}
		
		try {
			return Optional.ofNullable(zmedTrxhdrRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist ZmedTrxhdr (Personal Information).Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(ZmedTrxhdr entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(ZmedTrxhdr entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}
	
	@Override
	public Optional<ZmedTrxhdr> updateZclmno(String newZclmno, ZmedTrxhdr entity, User user)
			/*throws CannotPersistException, NoSuchMethodException, ConstraintViolationException*/ {
		
		try {
			int row = zmedTrxhdrRepository.updateZclmno(newZclmno, entity.getProcess(), entity.getReason(), entity.getZclmno(), entity.getBukrs().getBukrs());
			if (row > 0) {
			    entity.setZclmno(newZclmno);
			}
			return Optional.ofNullable(entity);
			//return Optional.ofNullable(zmedTrxhdrRepository.save(entity));
		} catch (Exception e) {
			entity.setZclmno(newZclmno);
			System.out.println("updateZclmno Exception: " + e.getMessage());
			return Optional.ofNullable(entity);
		}				
	}
	
	@Override
	public void threadUpdate(ZmedTrxhdr entity, EventLogCommandService eventLogCommandService, EventLog eventLog, User user) {
		new Thread(new UpdateThread(entity, eventLogCommandService, eventLog, user)).start();
	}
		
}