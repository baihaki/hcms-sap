package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;


public interface ZmedEmpquotaQueryService extends DatabaseQueryService<ZmedEmpquota, ZmedCompositeKeyEmpquota>, DatabasePaginationQueryService{
	//Optional<ZmedEmpquota> findByEntityName(final String entityName);
	

	@NotNull
	Optional<ZmedEmpquota> findOneByCompositeKey(String bukrs, String persg, String persk, String fatxt, String kodequo, Date datab, Date datbi);
	

	@NotNull
	List<ZmedEmpquota> findQuotas(String bukrs, String persg, String persk, Date datab, Date datbi);
	
}

