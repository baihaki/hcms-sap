package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.T705H;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.T705HKey;
import com.abminvestama.hcms.core.repository.T705HRepository;
import com.abminvestama.hcms.core.service.api.business.query.T705HQueryService;


@Service("t705hQueryService")
@Transactional(readOnly = true)
public class T705HQueryServiceImpl implements T705HQueryService {

	private T705HRepository t705hRepository;
	
	@Autowired
	T705HQueryServiceImpl(T705HRepository t705hRepository) {
		this.t705hRepository = t705hRepository;
	}
	
	@Override
	public Optional<Collection<T705H>> fetchAll() {
		Optional<Iterable<T705H>> t705hIterates = Optional.ofNullable(t705hRepository.findAll());
		if (!t705hIterates.isPresent()) {
			return Optional.empty();
		}
		
		List<T705H> listOfT705H = new ArrayList<>();
		t705hIterates.map(t705hIterable -> {			
			t705hIterable.forEach(t705h -> {
				listOfT705H.add(t705h);
			});
			return listOfT705H;
		});
		
		return Optional.of(listOfT705H);
	}

	/*@Override
	public Optional<T705H> findOneByCompositeKey(Long pdsnr) {
		if (pdsnr == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(t705hRepository.findOneByCompositeKey(pdsnr));
	}
*/
	
	@Override
	public Optional<T705H> findById(Optional<T705HKey> id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<T705H> findByPinco(String pinco) {
		
		if (pinco == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(t705hRepository.findByPinco(pinco));
	}

}