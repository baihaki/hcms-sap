package com.abminvestama.hcms.core.exception;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class AuthorizationException extends RuntimeException {

	private static final long serialVersionUID = -8538409664576600880L;
	
	public AuthorizationException(final String message) {
		super(message);
	}
	
	public AuthorizationException(final String message, final Throwable throwable) {
		super(message, throwable);
	}
}