package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "t705p")
public class T705P extends SAPAbstractEntityZmed<T705PKey> {

	private static final long serialVersionUID = 3669795826077668803L;
	

	public T705P() {}
	
	public T705P(T705PKey id) {
		this();
		this.id = id;
	}
	
	@Column(name = "pmbde", nullable = false, insertable = false, updatable = false, length = 2)
	private String pmbde;
	
	@Column(name = "satza", nullable = false, insertable = false, updatable = false, length = 3)
	private String satza;

	@Column(name = "ddtext", nullable = false, insertable = false, updatable = false)
	private String ddtext;
	
	public void setPmbde(String pmbde) {
		this.pmbde = pmbde;
	}

	public void setSatza(String satza) {
		this.satza = satza;
	}

	public void setDdtext(String ddtext) {
		this.ddtext = ddtext;
	}

	public String getPmbde() {
		return pmbde;
	}

	public String getSatza() {
		return satza;
	}

	public String getDdtext() {
		return ddtext;
	}

	
}