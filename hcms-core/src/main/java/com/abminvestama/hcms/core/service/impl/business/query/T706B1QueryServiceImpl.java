package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T706B1;
import com.abminvestama.hcms.core.repository.T706B1Repository;
import com.abminvestama.hcms.core.service.api.business.query.T706B1QueryService;

@Service("t706b1QueryService")
@Transactional(readOnly = true)
public class T706B1QueryServiceImpl implements T706B1QueryService {

	private T706B1Repository t706b1Repository;
	
	@Autowired
	T706B1QueryServiceImpl(T706B1Repository t706b1Repository) {
		this.t706b1Repository = t706b1Repository;
	}
	
	@Override
	public Optional<T706B1> findById(Optional<String> id) throws Exception {
		return id.map(pk -> t706b1Repository.findOne(pk));
	}

	@Override
	public Optional<Collection<T706B1>> fetchAll() {
		Optional<Iterable<T706B1>> t706b1Iterable = Optional.ofNullable(t706b1Repository.findAll());
		if (!t706b1Iterable.isPresent()) {
			return Optional.empty();
		}
		
		List<T706B1> listOfT706B1 = new ArrayList<>();
		return t706b1Iterable.map(iteratesT706B1 -> {
			iteratesT706B1.forEach(t706b1 -> {
				listOfT706B1.add(t706b1);
			});
			
			return listOfT706B1;
		});				
	}
}