package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T706S;
import com.abminvestama.hcms.core.model.entity.T706SKey;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.repository.T706SRepository;
import com.abminvestama.hcms.core.service.api.business.query.T706SQueryService;

@Service("t706sQueryService")
@Transactional(readOnly = true)
public class T706SQueryServiceImpl implements T706SQueryService {

	private T706SRepository t706sRepository;
	
	@Autowired
	T706SQueryServiceImpl(T706SRepository t706sRepository) {
		this.t706sRepository = t706sRepository;
	}
	
	/*@Override
	public Optional<T706S> findById(Optional<String> id) throws Exception {
		return id.map(pk -> t706sRepository.findOne(pk));
	}*/

	@Override
	public Optional<Collection<T706S>> fetchAll() {
		Optional<Iterable<T706S>> t706sIterable = Optional.ofNullable(t706sRepository.findAll());
		if (!t706sIterable.isPresent()) {
			return Optional.empty();
		}
		
		List<T706S> listOfT706S = new ArrayList<>();
		return t706sIterable.map(iteratesT706S -> {
			iteratesT706S.forEach(t706s -> {
				listOfT706S.add(t706s);
			});
			
			return listOfT706S;
		});				
	}

	@Override
	public Optional<T706S> findById(Optional<T706SKey> id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<T706S> findByMoreiNE(String morei) {
		if (morei == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<T706S>> empCost
			= Optional.ofNullable(t706sRepository.findByMoreiNE(morei));
		
		return (empCost.isPresent()
				? empCost.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

	@Override
	public Optional<T706S> findById(String schem) {
		if (schem == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(t706sRepository.findBySchem(schem));
	}

}