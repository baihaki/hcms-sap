package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;

import com.abminvestama.hcms.core.model.entity.SubT706B1;
import com.abminvestama.hcms.core.model.entity.SubT706B1Key;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface SubT706B1QueryService extends DatabaseQueryService<SubT706B1, SubT706B1Key>, DatabasePaginationQueryService {
	
	Collection<SubT706B1> findByMoreiAndSpkzlPrefixAndForm(String morei, String spkzlPrefix, Short form);
}