package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;


public interface ZmedEmpquotastfCommandService extends DatabaseCommandService<ZmedEmpquotastf> {
}