package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.IT0022;
import com.abminvestama.hcms.core.model.entity.IT0105;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface IT0105QueryService extends DatabaseQueryService<IT0105, ITCompositeKeys>, DatabasePaginationQueryService {
	
	@NotNull
	Optional<IT0105> findOneByCompositeKey(Long pernr, String subty, Date endda, Date begda);
	
	@NotNull
	Collection<IT0105> findByPernr(Long pernr);
	
	@NotNull
	Collection<IT0105> findByPernrAndSubty(Long pernr, String subty);

	
	@NotNull
	Page<IT0105> findByStatus(String status, int pageNumber);
	

	@NotNull
	Page<IT0105> findByPernrAndStatus(long pernr, String status, int pageNumber);
}