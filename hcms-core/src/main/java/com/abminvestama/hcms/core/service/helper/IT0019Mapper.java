package com.abminvestama.hcms.core.service.helper;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.CSKT;
import com.abminvestama.hcms.core.model.entity.CSKTKey;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.Job;
import com.abminvestama.hcms.core.model.entity.Position;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.IT0019;
import com.abminvestama.hcms.core.model.entity.T500P;
import com.abminvestama.hcms.core.model.entity.T501T;
import com.abminvestama.hcms.core.model.entity.T503K;
import com.abminvestama.hcms.core.model.entity.T513S;
import com.abminvestama.hcms.core.model.entity.T513SKey;
import com.abminvestama.hcms.core.model.entity.T527X;
import com.abminvestama.hcms.core.model.entity.T527XKey;
import com.abminvestama.hcms.core.model.entity.T528T;
import com.abminvestama.hcms.core.model.entity.T528TKey;
import com.abminvestama.hcms.core.model.entity.T531S;
import com.abminvestama.hcms.core.model.entity.T549T;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.V001PAll;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0019Mapper {
	
	private IT0019 it0019;
	
	private IT0019Mapper() {}
	
	public IT0019Mapper(Map<String, String> map, CommonServiceFactory commonServiceFactory, User user) {
		if (map == null) {
			new IT0019Mapper();
			it0019 = new IT0019();
		} else {
			ITCompositeKeys key = new ITCompositeKeys(Long.valueOf(map.get("sn")),
					SAPInfoType.TASK_MONITORING.infoType(), "03",
					CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31"),
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("startdt")));
			it0019 = new IT0019(key);
			Optional<T531S> tmart;
			try {
				tmart = commonServiceFactory.getT531SQueryService().findById(Optional.ofNullable(map.get("tmart")));
				if (tmart.isPresent()) {
					it0019.setTmart(tmart.get());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			it0019.setTermn(map.get("termn") != null && !map.get("termn").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("termn")) : null);
			it0019.setMndat(map.get("mndat") != null && !map.get("mndat").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("mndat")) : null);
			it0019.setBvmrk(map.get("bvmrk"));
			it0019.setTmjhr(map.get("tmjhr") != null ? Integer.parseInt(map.get("tmjhr")) : 0);
			it0019.setTmmon(map.get("tmmon") != null ? Integer.parseInt(map.get("tmmon")) : 0);
			it0019.setTmtag(map.get("tmtag") != null ? Integer.parseInt(map.get("tmtag")) : 0);
			it0019.setMnjhr(map.get("mnjhr") != null ? Integer.parseInt(map.get("mnjhr")) : 0);
			it0019.setMnmon(map.get("mnmon") != null ? Integer.parseInt(map.get("mnmon")) : 0);
			it0019.setMntag(map.get("mntag") != null ? Integer.parseInt(map.get("mntag")) : 0);
		}		
	}

	public IT0019 getIT0019() {
		return it0019;
	}
	
}
