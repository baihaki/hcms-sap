package com.abminvestama.hcms.core.service.helper;

import org.apache.commons.lang3.StringUtils;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.CaReqHdr;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public class CaReqHdrSoapObject {

	private String reqno;
	private String pernr;
	private String amount;
	private String bukrs;
	private String budat;
	private String hkont;
	private String docno;
	private String text;
	
	private CaReqHdrSoapObject() {}
	
	public CaReqHdrSoapObject(CaReqHdr object) {
		if (object == null) {
			new CaReqHdrSoapObject();
		} else {
			reqno = object.getReqNo();
			pernr = object.getPernr().toString();
			amount = object.getAmount() != null && object.getAmount() > 0 ? String.valueOf(object.getAmount().intValue()) : null;
			bukrs = object.getBukrs().getBukrs();
			budat = object.getApprovedTreasuryAt() != null ? CommonDateFunction.convertDateToStringYMD(object.getApprovedTreasuryAt()) : null;
			hkont = null;
			text = object.getDescription();
		}		
	}
	
	public CaReqHdrSoapObject(String reqno, String docno) {
		this.reqno = StringUtils.defaultString(reqno);
		this.docno = StringUtils.defaultString(docno); 
	}
	
	public CaReqHdrSoapObject(String bukrs, String budat, String reqno) {
		this.bukrs = StringUtils.defaultString(bukrs);
		this.budat = StringUtils.defaultString(budat);
		this.reqno = StringUtils.defaultString(reqno);
	}

	public String getReqno() {
		return reqno;
	}

	public String getPernr() {
		return pernr;
	}

	public String getAmount() {
		return amount;
	}

	public String getBukrs() {
		return bukrs;
	}

	public String getBudat() {
		return budat;
	}

	public String getHkont() {
		return hkont;
	}

	public String getDocno() {
		return docno;
	}

	public String getText() {
		return text;
	}
	
}
