package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.CaGroupAccount;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.CaGroupAccountRepository;
import com.abminvestama.hcms.core.service.api.business.command.CaGroupAccountCommandService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("caGroupAccountCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CaGroupAccountCommandServiceImpl implements CaGroupAccountCommandService {

	private CaGroupAccountRepository caGroupAccountRepository;
	
	@Autowired
	CaGroupAccountCommandServiceImpl(CaGroupAccountRepository caGroupAccountRepository) {
		this.caGroupAccountRepository = caGroupAccountRepository;
	}
	
	@Override
	public Optional<CaGroupAccount> save(CaGroupAccount entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		try {
			return Optional.ofNullable(caGroupAccountRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist CaGroupAccount. Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(CaGroupAccount entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(CaGroupAccount entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}
}