package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * @since 2.0.0
 * @version 2.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add field: entityDesc</td></tr>
 *     <tr><td>2.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Entity
@Table(name = "cfg_entity", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"entity_name"} )
})
public class HCMSEntity extends AbstractEntity {

	private static final long serialVersionUID = 8502579691253652991L;

	@Column(name = "entity_name", nullable = false)
	private String entityName;
	
	@Column(name = "entity_desc")
	private String entityDesc;
	
	HCMSEntity() {}
	
	public HCMSEntity(String entityName) {
		this.entityName = entityName;
	}
	
	public String getEntityName() {
		return entityName;
	}
	
	public String getEntityDesc() {
		return entityDesc;
	}	
}