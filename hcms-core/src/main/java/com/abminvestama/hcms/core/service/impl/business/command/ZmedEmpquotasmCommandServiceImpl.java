package com.abminvestama.hcms.core.service.impl.business.command;


import java.util.Date;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.ZmedEmpquotasmRepository;
import com.abminvestama.hcms.core.service.api.business.command.ZmedEmpquotasmCommandService;


@Service("ZmedEmpquotasmCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ZmedEmpquotasmCommandServiceImpl implements ZmedEmpquotasmCommandService {
	
	private ZmedEmpquotasmRepository zmedEmpquotasmRepository;
		
		@Autowired
		ZmedEmpquotasmCommandServiceImpl(ZmedEmpquotasmRepository zmedEmpquotasmRepository) {
			this.zmedEmpquotasmRepository = zmedEmpquotasmRepository;
		}
		
		@Override
		public Optional<ZmedEmpquotasm> save(ZmedEmpquotasm entity, User user)
				throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
			//if (user != null) {
				//entity.setUname(user.getId());
			//}
			
			try {
				return Optional.ofNullable(zmedEmpquotasmRepository.save(entity));
			} catch (Exception e) {
				throw new CannotPersistException("Cannot persist ZmedEmpquotasm (Personal Information).Reason=" + e.getMessage());
			}				
		}

		@Override
		public boolean delete(ZmedEmpquotasm entity, User user) throws NoSuchMethodException, ConstraintViolationException {
			// no delete at this version.
			throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
		}

		@Override
		public boolean restore(ZmedEmpquotasm entity, User user) throws NoSuchMethodException, ConstraintViolationException {
			// since we don't have delete operation, then we don't need the 'restore' as well
			throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
		}
	}