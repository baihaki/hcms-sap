package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Extends {@link SAPSynchronizeEntity} for Synchronization fields</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add text1 and status (DocumentStatus) field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 * Class that represents master data transaction for <Strong>Resume Texts</strong> (i.e. Infotype IT0184 in SAP).
 */
@Entity
@Table(name = "it0184")
public class IT0184 extends SAPSynchronizeEntity<ITCompositeKeys> {

	private static final long serialVersionUID = 329556734544102682L;
	
	@Column(name = "pernr", insertable = false, updatable = false, nullable = false)
	private Long pernr;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "subty", referencedColumnName = "subty", insertable = false, updatable = false),
		@JoinColumn(name = "infty", referencedColumnName = "infty", insertable = false, updatable = false)
	})
	private T591S subty;
	
	public IT0184() {}
	
	public IT0184(ITCompositeKeys id) {
		this.id = id;
	}
	
	@Column(name = "seqnr")
	private Long seqnr;
	
	@Column(name = "itxex")
	private String itxex;
	
	@Column(name = "text1")
	private String text1;
	
	@Column(name = "document_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private DocumentStatus status;
	
	/**
	 * GET Employee SSN/ID.
	 * 
	 * @return
	 */
	public Long getPernr() {
		return pernr;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	public T591S getSubty() {
		return subty;
	}
	
	/**
	 * GET Infotype record no.
	 * 
	 * @return
	 */
	public Long getSeqnr() {
		return seqnr;
	}
	
	public void setSeqnr(Long seqnr) {
		this.seqnr = seqnr;
	}
	
	/**
	 * GET Text Exists.
	 * 
	 * @return
	 */
	public String getItxex() {
		return itxex;
	}
	
	public void setItxex(String itxex) {
		this.itxex = itxex;
	}
	
	/**
	 * GET Text Value.
	 * 
	 * @return
	 */
	public String getText1() {
		return text1;
	}
	
	public void setText1(String text1) {
		this.text1 = text1;
	}
	
	/**
	 * GET Document Status.
	 * 
	 * @return
	 */
	public DocumentStatus getStatus() {
		return status;
	}
	
	public void setStatus(DocumentStatus status) {
		this.status = status;
	}
}