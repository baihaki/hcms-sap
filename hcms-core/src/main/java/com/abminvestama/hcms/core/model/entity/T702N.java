package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t702n")
public class T702N implements Serializable {

	private static final long serialVersionUID = 4747706564427397557L;

	@Id
	@Column(name = "morei", nullable = false, unique = true, length = 2)
	private String morei;
	
	@Column(name = "text25", nullable = false, length = 40)
	private String text25;
	
	/**
	 * Get Marrital Status Code
	 * 
	 * @return
	 */
	public String getMorei() {
		return morei;
	}
	
	/**
	 * Get Marrital Status Text (Description)
	 * 
	 * @return
	 */
	public String getText25() {
		return text25;
	}
}