package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;

import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.SHT706B1;
import com.abminvestama.hcms.core.model.entity.SHT706B1Key;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

public interface SHT706B1QueryService extends DatabaseQueryService<SHT706B1, SHT706B1Key>, DatabasePaginationQueryService {
	

	@NotNull
	Collection<SHT706B1> findByMoreiNE(String morei/*, String schem*/);	
}