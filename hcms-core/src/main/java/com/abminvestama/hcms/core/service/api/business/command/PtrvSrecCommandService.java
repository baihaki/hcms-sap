package com.abminvestama.hcms.core.service.api.business.command;

import java.util.List;
import java.util.Optional;

import com.abminvestama.hcms.core.model.entity.PtrvSrec;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add deleteList method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add updateTripnumber method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface PtrvSrecCommandService extends DatabaseCommandService<PtrvSrec> {

	Optional<PtrvSrec> updateTripnumber(Long newReinr, PtrvSrec entity, User user);

	boolean deleteList(List<PtrvSrec> entities);
}