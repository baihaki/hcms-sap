package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;

@Embeddable
@Immutable
public class SHT706B1Key implements Serializable {

	private static final long serialVersionUID = 2987013141063448484L;
	
	private String morei;
	private String spkzl;
	
	public SHT706B1Key() {}
	
	public SHT706B1Key(String morei, String spkzl) {
		this.morei = morei;
		this.spkzl = spkzl;
	}
	
	public String getMorei() {
		return morei;
	}
	
	public String getSpkzl() {
		return spkzl;
	}
	
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final SHT706B1Key key = (SHT706B1Key) o;
		
		if (key.getMorei() != null ? !key.getMorei().equalsIgnoreCase(morei) : morei != null) {
			return false;
		}
		
		if (key.getSpkzl() != null ? !key.getSpkzl().equalsIgnoreCase(spkzl) : spkzl != null) {
			return false;
		}
		
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.morei != null ? this.morei.hashCode() : 0;
		result = result * 31 + (this.spkzl != null ? this.spkzl.hashCode() : 0);
		return result;
	}
}