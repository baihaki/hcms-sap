package com.abminvestama.hcms.core.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;

/**
 * 
 * Class that represents master data transaction for <strong>Tax Data Indonesia</strong> (i.e. IT0241 infotype in SAP).
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add attachment, attachmentPath field</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add status (DocumentStatus) field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Entity
@Table(name = "it0241")
public class IT0241 extends SAPAbstractEntity<ITCompositeKeysNoSubtype> {

	private static final long serialVersionUID = -4557085307211692369L;

	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;
	
	public IT0241() {}
	
	public IT0241(ITCompositeKeysNoSubtype id) {
		this();
		this.id = id;
	}
	
	@Column(name = "seqnr")
	private Long seqnr;	
	
	@Column(name = "taxid", nullable = false)
	private String taxId;
	
	@Column(name = "marrd", length = 1)
	private String marrd;
	
	@Column(name = "spben", length = 1)
	private String spben;
	
	@Column(name = "refno")
	private String refno;
	
	@Column(name = "depnd", length = 2)
	private String depnd;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "rdate")
	private Date rdate;
	
	@Column(name = "document_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private DocumentStatus status;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "infty", referencedColumnName = "infty", insertable = false, updatable = false),
		@JoinColumn(name = "attach1_subty", referencedColumnName = "subty", insertable = false, updatable = false)
	})
	private Attachment attachment;
	
	@Column(name = "attach1_path")
	private String attachmentPath;
	
	/**
	 * GET Employee SSN.
	 * 
	 * @return
	 */
	public Long getPernr() {
		return pernr;
	}
	
	public void setPernr(Long pernr) {
		this.pernr = pernr;
	}
	
	/**
	 * GET Infotype Record No.
	 * 
	 * @return
	 */
	public Long getSeqnr() {
		return seqnr;
	}
	
	public void setSeqnr(Long seqnr) {
		this.seqnr = seqnr;
	}
	
	/**
	 * GET Tax ID.
	 *  
	 * @return
	 */
	public String getTaxId() {
		return taxId;
	}
	
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	
	/**
	 * GET Married for Tax Purposes.
	 * 
	 * @return
	 */
	public String getMarrd() {
		return marrd;
	}
	
	public void setMarrd(String marrd) {
		this.marrd = marrd;
	}
	
	/**
	 * GET Spouse Benefit.
	 * 
	 * @return
	 */
	public String getSpben() {
		return spben;
	}
	
	public void setSpben(String spben) {
		this.spben = spben;
	}
	
	/**
	 * GET Unemployment Reference Number.
	 * 
	 * @return
	 */
	public String getRefno() {
		return refno;
	}
	
	public void setRefno(String refno) {
		this.refno = refno;
	}
	
	/**
	 * Get Number of Dependents for Tax Purposes.
	 * 
	 * @return
	 */
	public String getDepnd() {
		return depnd;
	}
	
	public void setDepnd(String depnd) {
		this.depnd = depnd;
	}
	
	/**
	 * GET NPWP Registration Date.
	 * 
	 * @return
	 */
	public Date getRdate() {
		return rdate;
	}
	
	public void setRdate(Date rdate) {
		this.rdate = rdate;
	}
	
	/**
	 * GET Document Status.
	 * 
	 * @return
	 */
	public DocumentStatus getStatus() {
		return status;
	}
	
	public void setStatus(DocumentStatus status) {
		this.status = status;
	}

	/**
	 * GET Attachment Document Type.
	 * 
	 * @return
	 */
	public Attachment getAttachment() {
		return attachment;
	}
	
	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	/**
	 * GET Attachment Document file path.
	 * 
	 * @return
	 */
	public String getAttachmentPath() {
		return attachmentPath;
	}
	
	public void  setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}
}