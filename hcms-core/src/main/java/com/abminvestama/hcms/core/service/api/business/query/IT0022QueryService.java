package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.IT0006;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.IT0022;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add methods: countByEduAndBukrs</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0022QueryService extends DatabaseQueryService<IT0022, ITCompositeKeys>, DatabasePaginationQueryService {

	@NotNull
	Optional<IT0022> findOneByCompositeKey(Long pernr, String subty, Date endda, Date begda);
	
	@NotNull
	Collection<IT0022> findByPernr(Long pernr);
	
	@NotNull
	Collection<IT0022> findByPernrAndSubty(Long pernr, String subty);	

	@NotNull
	Page<IT0022> findByStatus(String status, int pageNumber);
	

	@NotNull
	Page<IT0022> findByPernrAndStatus(long pernr, String status, int pageNumber);

	@NotNull
	Long countByEduAndBukrs(String slart, String bukrs);
}