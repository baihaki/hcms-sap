package com.abminvestama.hcms.core.service.helper;

import org.apache.commons.lang3.StringUtils;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0002;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0002SoapObject {

	private String employeenumber;
	private String validitybegin;
	private String validityend;
	
	private String nachn;
	private String cname;
	private String art;
	private String title;
	private String art2;
	private String title2;
	private String art3;
	private String title3;
	private String rufnm;
	private String knznm;
	private String anred;
	private String gesch;
	private String gbdat;
	private String gblnd;
	private String natio;
	private String gbort;
	private String sprsl;
	private String konfe;
	private String famst;
	private String famdt;
	private String anzkd;
	
	private IT0002SoapObject() {}
	
	public IT0002SoapObject(IT0002 object) {
		if (object == null) {
			new IT0002SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			nachn = object.getNachn();
			cname = object.getCname();
			art = object.getTitle() != null ? object.getTitle().getId().getArt() : null;
			title = object.getTitle() != null ? object.getTitle().getId().getTitle() : null;
			art2 = object.getTitl2() != null ? object.getTitl2().getId().getArt() : null;
			title2 = object.getTitl2() != null ? object.getTitl2().getId().getTitle() : null;
			/*
			art2 = object.getTitl2().getId().getArt();
			title2 = object.getTitl2().getId().getTitle();
			art3 = object.getNamzu().getId().getArt();
			title3 = object.getNamzu().getId().getTitle();
			*/
			rufnm = object.getRufnm();
			knznm = object.getKnznm();
			anred = object.getAnred() != null ? object.getAnred().getAnred() : null;
			gesch = object.getGesch();
			gbdat = object.getGbdat() != null ? CommonDateFunction.convertDateToStringYMD(object.getGbdat()) : null;
			gblnd = object.getGblnd() != null ? object.getGblnd().getLand1() : null;
			natio = object.getNatio() != null ? object.getNatio().getLand1() : null;
			gbort = object.getGbort();
			sprsl = object.getSprsl() != null ? object.getSprsl().getSprsl() : null;
			konfe = object.getKonfe() != null ? object.getKonfe().getKonfe() : null;
			famst = object.getFamst() != null ? object.getFamst().getFamst() : null;
			famdt = object.getFamdt() != null ? CommonDateFunction.convertDateToStringYMD(object.getFamdt()) : null;
			anzkd = object.getAnzkd() != null ? object.getAnzkd().toString() : null;
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}

	public String getNachn() {
		return nachn;
	}

	public String getCname() {
		return cname;
	}

	public String getArt() {
		return art;
	}

	public String getTitle() {
		return title;
	}

	public String getArt2() {
		return art2;
	}

	public String getTitle2() {
		return title2;
	}

	public String getArt3() {
		return art3;
	}

	public String getTitle3() {
		return title3;
	}

	public String getRufnm() {
		return rufnm;
	}

	public String getKnznm() {
		return knznm;
	}

	public String getAnred() {
		return anred;
	}

	public String getGesch() {
		return gesch;
	}

	public String getGbdat() {
		return gbdat;
	}

	public String getGblnd() {
		return gblnd;
	}

	public String getNatio() {
		return natio;
	}

	public String getGbort() {
		return gbort;
	}

	public String getSprsl() {
		return sprsl;
	}

	public String getKonfe() {
		return konfe;
	}

	public String getFamst() {
		return famst;
	}

	public String getFamdt() {
		return famdt;
	}

	public String getAnzkd() {
		return anzkd;
	}
}
