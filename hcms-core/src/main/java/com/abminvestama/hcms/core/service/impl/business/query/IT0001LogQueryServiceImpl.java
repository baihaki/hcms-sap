package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.IT0001Log;
import com.abminvestama.hcms.core.repository.IT0001LogRepository;
import com.abminvestama.hcms.core.service.api.business.query.IT0001LogQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("it0001LogQueryService")
@Transactional(readOnly = true)
public class IT0001LogQueryServiceImpl implements IT0001LogQueryService {

	private IT0001LogRepository it0001LogRepository;
	
	@Autowired
	IT0001LogQueryServiceImpl(IT0001LogRepository it0001LogRepository) {
		this.it0001LogRepository = it0001LogRepository;
	}
	
	@Override
	public Optional<IT0001Log> findById(Optional<String> id) throws Exception {
		return id.map(pk -> it0001LogRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<IT0001Log>> fetchAll() {
		List<IT0001Log> it0001LogList = new ArrayList<>();
		it0001LogRepository.findAll().forEach(it0001Log -> {
			it0001LogList.add(it0001Log);
		});
		
		return Optional.of(it0001LogList);
	}

	@Override
	public Page<IT0001Log> findByPernrAndEnddaAndBegda(Long pernr, Date endda, Date begda, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return it0001LogRepository.findByPernrAndEnddaAndBegda(pernr, endda, begda, pageRequest);
	}

	@Override
	public Page<IT0001Log> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return it0001LogRepository.findAll(pageRequest);						
	}
}