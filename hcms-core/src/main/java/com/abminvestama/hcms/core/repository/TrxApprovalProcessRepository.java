package com.abminvestama.hcms.core.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.TrxApprovalProcess;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface TrxApprovalProcessRepository extends CrudRepository<TrxApprovalProcess, String> {
	
	@Query(nativeQuery=true, value="SELECT * FROM trx_approval_process WHERE entity_object_id = :entityObjectId ORDER BY :orderBy")
	Collection<TrxApprovalProcess> fetchByEntityObjectId(@Param("entityObjectId") String entityObjectId, @Param("orderBy") String orderBy);

}
