package com.abminvestama.hcms.core.repository;

import org.springframework.data.repository.CrudRepository;

import com.abminvestama.hcms.core.model.entity.PositionRelationType;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface PositionRelationTypeRepository extends CrudRepository<PositionRelationType, String> {
	
	PositionRelationType findByName(String name);
}