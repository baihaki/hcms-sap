package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.PtrvSrecKey;
import com.abminvestama.hcms.core.model.entity.PtrvSrec;
import com.abminvestama.hcms.core.repository.PtrvSrecRepository;
import com.abminvestama.hcms.core.service.api.business.query.PtrvSrecQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add method: findByTripnumber</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("ptrvSrecQueryService")
@Transactional(readOnly = true)
public class PtrvSrecQueryServiceImpl implements PtrvSrecQueryService {

	private PtrvSrecRepository ptrvSrecRepository;
	
	@Autowired
	PtrvSrecQueryServiceImpl(PtrvSrecRepository ptrvSrecRepository) {
		this.ptrvSrecRepository = ptrvSrecRepository;
	}
	
	@Override
	public Optional<Collection<PtrvSrec>> fetchAll() {
		List<PtrvSrec> entities = new ArrayList<>();
		Optional<Iterable<PtrvSrec>> entityIterable = Optional.ofNullable(ptrvSrecRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}
	
	
	
	@Override
	public Optional<PtrvSrec> findById(Optional<PtrvSrecKey> id)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
	@Override
	public Optional<PtrvSrec> findOneByCompositeKey(String mandt, Long pernr,
			Long reinr, Integer perio, String receiptno) {
		if (mandt == null || pernr == null || reinr == null || perio == null || receiptno == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(ptrvSrecRepository.findOneByCompositeKey(mandt, pernr, reinr, perio, receiptno));
	}

	/*@Override
	public Collection<PtrvSrec> findByKodemed(String bukrs, String kodemed,
			Date datbi) {
		if (bukrs == null || kodemed == null || datbi == null) {
			return Collections.emptyList();
		}
		

		Optional<Collection<PtrvSrec>> bunchOfZmed
			= Optional.ofNullable(ptrvSrecRepository.findByKodemed(bukrs, kodemed, datbi));
		
		return (bunchOfZmed.isPresent()
				? bunchOfZmed.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}*/
	
	@Override
	public Collection<PtrvSrec> findByTripnumber(Long tripnumber) {
		if (tripnumber == null) {
			return Collections.emptyList();
		}
		Optional<Collection<PtrvSrec>> bunchOfPtrvSrec
			= Optional.ofNullable(ptrvSrecRepository.findByTripnumber(tripnumber));
		
		return (bunchOfPtrvSrec.isPresent()
				? bunchOfPtrvSrec.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}
		
}
