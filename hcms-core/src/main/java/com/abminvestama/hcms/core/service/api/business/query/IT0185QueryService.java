package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.IT0185;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add findByPernrAndStatus method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findByStatus method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0185QueryService extends DatabaseQueryService<IT0185, ITCompositeKeys>, DatabasePaginationQueryService {
	
	@NotNull
	Optional<IT0185> findOneByCompositeKey(Long pernr, String subty, Date endda, Date begda);
	
	@NotNull
	Collection<IT0185> findByPernr(Long pernr);
	
	@NotNull
	Collection<IT0185> findByPernrAndSubty(Long pernr, String subty);	
	
	@NotNull
	Page<IT0185> findByStatus(String status, int pageNumber);
	
	@NotNull
	Page<IT0185> findByPernrAndStatus(long pernr, String status, int pageNumber);
}