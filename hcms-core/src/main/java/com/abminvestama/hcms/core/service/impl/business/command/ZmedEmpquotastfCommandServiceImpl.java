package com.abminvestama.hcms.core.service.impl.business.command;


import java.util.Date;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.ZmedEmpquotastfRepository;
import com.abminvestama.hcms.core.service.api.business.command.ZmedEmpquotastfCommandService;


@Service("ZmedEmpquotastfCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ZmedEmpquotastfCommandServiceImpl implements ZmedEmpquotastfCommandService {
	
	private ZmedEmpquotastfRepository zmedEmpquotastfRepository;
		
		@Autowired
		ZmedEmpquotastfCommandServiceImpl(ZmedEmpquotastfRepository zmedEmpquotastfRepository) {
			this.zmedEmpquotastfRepository = zmedEmpquotastfRepository;
		}
		
		@Override
		public Optional<ZmedEmpquotastf> save(ZmedEmpquotastf entity, User user)
				throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
			//if (user != null) {
				//entity.setUname(user.getId());
			//}
			
			try {
				return Optional.ofNullable(zmedEmpquotastfRepository.save(entity));
			} catch (Exception e) {
				throw new CannotPersistException("Cannot persist ZmedEmpquotastf (Personal Information).Reason=" + e.getMessage());
			}				
		}

		@Override
		public boolean delete(ZmedEmpquotastf entity, User user) throws NoSuchMethodException, ConstraintViolationException {
			// no delete at this version.
			throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
		}

		@Override
		public boolean restore(ZmedEmpquotastf entity, User user) throws NoSuchMethodException, ConstraintViolationException {
			// since we don't have delete operation, then we don't need the 'restore' as well
			throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
		}
	}