package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0184;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0184SoapObject {
	
	private String employeenumber;
	private String validitybegin;
	private String validityend;
	private String subty;
	private String infty;
	private String seqnr;
	private String itxex;
	private String text1;
	

	private IT0184SoapObject() {}
	
	public IT0184SoapObject(IT0184 object) {
		if (object == null) {
			new IT0184SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			subty = object.getId().getSubty();
			//infty = object.getId().getInfty();
			//seqnr = object.getSeqnr().toString();
			itxex = object.getItxex();
			text1 = object.getText1();
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}
	public String getSubty() {
		return subty;
	}

	public String getInfty() {
		return infty;
	}

	public String getSeqnr() {
		return seqnr;
	}

	public String getItxex() {
		return itxex;
	}

	public String getText1() {
		return text1;
	}

}
