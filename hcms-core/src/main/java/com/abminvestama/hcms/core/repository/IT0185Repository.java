package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0185;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;

/**
 * 
 * @since 1.0.0
 * @version 1.0.4
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add order by begda DESC on all queries</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: findByPernrForPublishedOrUpdated</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add findByPernrAndStatus method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findByStatus method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0185Repository extends CrudRepository<IT0185, ITCompositeKeys> {
	
	@Query("FROM IT0185 it0185 WHERE it0185.pernr = :pernr AND it0185.id.infty = :infty AND it0185.id.subty = :subty AND it0185.id.endda = :endda AND it0185.id.begda = :begda")
	IT0185 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty,
			@Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM IT0185 it0185 WHERE it0185.pernr = :pernr AND it0185.id.infty = :infty ORDER BY it0185.id.subty ASC, it0185.id.endda DESC, it0185.id.begda DESC")
	Collection<IT0185> findByPernr(@Param("pernr") Long pernr, @Param("infty") String infty);
	
	@Query("FROM IT0185 it0185 WHERE it0185.pernr = :pernr AND it0185.id.infty = :infty AND it0185.id.subty = :subty ORDER BY it0185.id.endda DESC, it0185.id.begda DESC")
	Collection<IT0185> findByPernrAndSubty(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty);

	@Query("FROM IT0185 it0185 WHERE it0185.status = :status ORDER BY it0185.id.endda DESC, it0185.id.begda DESC")
	Page<IT0185> findByStatus(@Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0185 it0185 WHERE it0185.pernr = :pernr AND it0185.status = :status ORDER BY it0185.id.endda DESC, it0185.id.begda DESC")
	Page<IT0185> findByPernrAndStatus(@Param("pernr") Long pernr, @Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0185 it0185 WHERE it0185.pernr = :pernr AND " +
	"(it0185.status = 'PUBLISHED' OR it0185.status = 'UPDATE_IN_PROGRESS' OR it0185.status = 'UPDATE_REJECTED') " +
	" ORDER BY it0185.id.endda DESC, it0185.id.begda DESC")
	Page<IT0185> findByPernrForPublishedOrUpdated(@Param("pernr") Long pernr, Pageable pageRequest);
}