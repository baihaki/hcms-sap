package com.abminvestama.hcms.core.service.helper;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.CSKT;
import com.abminvestama.hcms.core.model.entity.CSKTKey;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.Job;
import com.abminvestama.hcms.core.model.entity.Position;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.T500P;
import com.abminvestama.hcms.core.model.entity.T501T;
import com.abminvestama.hcms.core.model.entity.T503K;
import com.abminvestama.hcms.core.model.entity.T513S;
import com.abminvestama.hcms.core.model.entity.T513SKey;
import com.abminvestama.hcms.core.model.entity.T527X;
import com.abminvestama.hcms.core.model.entity.T527XKey;
import com.abminvestama.hcms.core.model.entity.T528T;
import com.abminvestama.hcms.core.model.entity.T528TKey;
import com.abminvestama.hcms.core.model.entity.T549T;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.V001PAll;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0001Mapper {
	
	private IT0001 it0001;
	
	private IT0001Mapper() {}
	
	public IT0001Mapper(Map<String, String> map, CommonServiceFactory commonServiceFactory, User user) {
		if (map == null) {
			new IT0001Mapper();
			it0001 = new IT0001();
		} else {
			ITCompositeKeysNoSubtype key = new ITCompositeKeysNoSubtype(Long.valueOf(map.get("sn")),
					CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31"),
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("startdt")));
			it0001 = new IT0001(key);
			Optional<T001> bukrs = commonServiceFactory.getT001QueryService().findByBukrs(map.get("cocd"));
			if (bukrs.isPresent()) {
				T500P t500p = new T500P();
				t500p.setPersa(map.get("branc"));
				t500p.setBukrs(bukrs.get());
				it0001.setT500p(t500p);
			}
			T501T t501t = new T501T();
			t501t.setPersg(map.get("empgroupcd"));
			it0001.setT501t(t501t);
			T503K t503k = new T503K();
			t503k.setMandt(map.get("persk"));
			it0001.setT503k(t503k);
			it0001.setVdsk1(map.get("divnm"));
			it0001.setGsber(map.get("cocd"));
			V001PAll v001pall = new V001PAll();
			v001pall.setWerks(map.get("branc"));
			v001pall.setBtrtl(map.get("btrtl"));
			it0001.setV001pall(v001pall);
			T549T t549t = new T549T();
			t549t.setAbkrs(map.get("payrollarea"));
			it0001.setT549t(t549t);
			if (map.get("costcenter") != null) {
				String kostl = "";
				try {
					kostl = Long.valueOf(map.get("costcenter")).toString();
				} catch (Exception e) {
					kostl = map.get("costcenter");
				}
				Optional<CSKTKey> csktKey = Optional.ofNullable(new CSKTKey("1000", kostl));
				Optional<CSKT> csktObject;
				try {
					csktObject = commonServiceFactory.getCSKTQueryService().findById(csktKey);
				} catch (Exception e) {
					csktObject = Optional.empty();
				}
				if (!csktObject.isPresent()) {
					CSKT cskt = new CSKT();
					cskt.setKokrs("1000");
					cskt.setKostl(kostl);
					cskt.setKltxt(StringUtils.defaultString(map.get("notes"), "-"));
					cskt.setKtext(StringUtils.defaultString(map.get("notes"), "-"));
					cskt.setMcds3(StringUtils.defaultString(map.get("notes"), "-"));
					try {
						csktObject = commonServiceFactory.getCSKTCommandService().save(cskt, null);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				it0001.setCskt(csktObject.isPresent() ? csktObject.get() : null);
			}
			if (map.get("jobcd") != null) {
				Optional<T513SKey> t513sKey = Optional.ofNullable(new T513SKey(Long.valueOf(map.get("jobcd")),
						CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31")));
				Optional<T513S> t513sObject;
				try {
					t513sObject = commonServiceFactory.getT513SQueryService().findById(t513sKey);
				} catch (Exception e) {
					t513sObject = Optional.empty();
				}
				if (!t513sObject.isPresent()) {
					T513S t513s = new T513S();
					t513s.setStell(Long.valueOf(map.get("jobcd")));
					t513s.setEndda(CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31"));
					t513s.setBegda(new Date());
					t513s.setStltx(StringUtils.defaultString(map.get("jobnm"), "-"));
					try {
						t513sObject = commonServiceFactory.getT513SCommandService().save(t513s, null);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				it0001.setT513s(t513sObject.isPresent() ? t513sObject.get() : null);
				
				Optional<Job> jobObject;
				try {
					jobObject = commonServiceFactory.getJobQueryService().findByCode(Optional.ofNullable(map.get("jobcd")));
				} catch (Exception e) {
					jobObject = Optional.empty();
				}
				if (!jobObject.isPresent()) {
					Job job = new Job();
					job.setCode(map.get("jobcd"));
					job.setName(StringUtils.defaultString(map.get("jobnm"), "-"));
					job.setCreatedById(user.getId());
					try {
						jobObject = commonServiceFactory.getJobCommandService().save(job, user);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				it0001.setJob(jobObject.isPresent() ? jobObject.get() : null);
			}
			if (map.get("deptcd") != null) {
				Optional<T527XKey> t527xKey = Optional.ofNullable(new T527XKey(Long.valueOf(map.get("deptcd")),
						CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31")));
				Optional<T527X> t527xObject;
				try {
					t527xObject = commonServiceFactory.getT527XQueryService().findById(t527xKey);
				} catch (Exception e) {
					t527xObject = Optional.empty();
				}
				if (!t527xObject.isPresent() && Long.valueOf(map.get("deptcd")).longValue() != 0 && map.get("deptnm") != null) {
					T527X t527x = new T527X();
					t527x.setOrgeh(Long.valueOf(map.get("deptcd")));
					t527x.setEndda(CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31"));
					t527x.setBegda(new Date());
					t527x.setOrgtx(StringUtils.defaultString(map.get("deptnm"), "-"));
					try {
						t527xObject = commonServiceFactory.getT527XCommandService().save(t527x, null);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				it0001.setT527x(t527xObject.isPresent() ? t527xObject.get() : null);
			}
			if (map.get("positioncd") != null) {
				Optional<T528TKey> t528tKey = Optional.ofNullable(new T528TKey(Long.valueOf(map.get("positioncd")),
						CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31")));
				Optional<T528T> t528tObject;
				Optional<Position> positionObject;
				try {
					t528tObject = commonServiceFactory.getT528TQueryService().findById(t528tKey);
				} catch (Exception e) {
					t528tObject = Optional.empty();
				}
				try {
					positionObject = commonServiceFactory.getPositionQueryService().findByCode(Optional.ofNullable(map.get("positioncd")));
				} catch (Exception e) {
					positionObject = Optional.empty();
				}
				if (!t528tObject.isPresent()) {
					T528T t528t = new T528T();
					t528t.setPlans(Long.valueOf(map.get("positioncd")));
					t528t.setEndda(CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31"));
					t528t.setBegda(new Date());
					t528t.setPlstx(StringUtils.defaultString(map.get("positiontxt"), "-"));
					try {
						t528tObject = commonServiceFactory.getT528TCommandService().save(t528t, null);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if (!positionObject.isPresent()) {
					Position position = new Position();
					position.setCode(map.get("positioncd"));
					position.setName(StringUtils.defaultString(map.get("positiontxt"), "-"));
					position.setCreatedById(user.getId());
					try {
						positionObject = commonServiceFactory.getPositionCommandService().save(position, user);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				it0001.setT528t(t528tObject.isPresent() ? t528tObject.get() : null);
				it0001.setPosition(positionObject.isPresent() ? positionObject.get() : null);
			}
			
			/*
			if (map.get("deptcd") != null) {
				T527X t527x = new T527X();
				t527x.setOrgeh(Long.valueOf(map.get("deptcd")));
				t527x.setEndda(CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31"));
				it0001.setT527x(t527x);
			}
			if (map.get("positioncd") != null) {
				T528T t528t = new T528T();
				t528t.setPlans(Long.valueOf(map.get("positioncd")));
				t528t.setEndda(CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31"));
				it0001.setT528t(t528t);
			}*/
			it0001.setSname(map.get("fullname"));
			it0001.setEname(map.get("fullname"));
			
			/*
			it0001.setAppamt(map.get("appamt") != null ? Double.valueOf(map.get("appamt")) : 0.00);
			it0001.setAprby(map.get("aprby"));
			it0001.setAprdt(map.get("aprdt") != null && !map.get("aprdt").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("aprdt")) : null);
			it0001.setBelnr(map.get("belnr"));
			it0001.setBilldt(map.get("billdt") != null ? CommonDateFunction.convertDateRequestParameterIntoDate(map.get("billdt")) : null);
			//Optional<T001> bukrs = t001QueryService.findByBukrs(map.get("bukrs"));				
			//it0001.setBukrs(bukrs.isPresent() ? bukrs.get() : null);
			it0001.setClmamt(map.get("clmamt") != null ? Double.valueOf(map.get("clmamt")) : 0.00);
			it0001.setClmby(map.get("clmby"));
			it0001.setClmdt(map.get("clmdt") != null && !map.get("clmdt").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("clmdt")) : null);
			it0001.setClmst(map.get("clmst"));
			it0001.setEname(map.get("ename"));
			it0001.setId(new ZmedCompositeKeyTrxhdr(map.get("bukrs"), map.get("zclmno")));
			it0001.setJamin(map.get("jamin"));
			it0001.setKodemed(map.get("kodemed"));
			it0001.setMedtydesc(map.get("medtydesc"));
			it0001.setNotex(map.get("notex"));
			it0001.setOrgtx(map.get("orgtx"));
			it0001.setPagu1(map.get("pagu1") != null ? Double.valueOf(map.get("pagu1")) : 0.00);
			it0001.setPagu2(map.get("pagu2") != null ? Double.valueOf(map.get("pagu1")) : 0.00);
			it0001.setPagu2byemp(map.get("pagu2byemp") != null ? Double.valueOf(map.get("pagu2byemp")) : 0.00);
			it0001.setPagudent(map.get("pagudent") != null ? Double.valueOf(map.get("pagudent")) : 0.00);
			it0001.setPasien(map.get("pasien"));
			it0001.setPayby(map.get("payby"));
			it0001.setPaydt(map.get("paydt") != null && !map.get("paydt").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("paydt")) : null);
			it0001.setPaymethod(map.get("paymethod"));
			it0001.setPenyakit(map.get("penyakit"));
			Long pernr = map.get("employeenumber") != null ? Long.valueOf(map.get("employeenumber")) : null;
			it0001.setPernr(pernr);
			it0001.setRejamt(map.get("rejamt") != null ? Double.valueOf(map.get("rejamt")) : 0.00);
			it0001.setRevby(map.get("revby"));
			it0001.setRevdt(map.get("revdt") != null && !map.get("revdt").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("revdt")) : null);
			it0001.setTglkeluar(map.get("tglkeluar") != null && !map.get("tglkeluar").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("tglkeluar")) : null);
			it0001.setTglmasuk(map.get("tglmasuk") != null && !map.get("tglmasuk").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("tglmasuk")) : null);
			it0001.setTotamt(map.get("totamt") != null ? Double.valueOf(map.get("totamt")) : 0.00);
			it0001.setZclmno(map.get("zclmno"));
			it0001.setZdivision(map.get("zdivision"));
			
			it0001.setPaymentStatus(map.get("paymentstatus"));
			it0001.setPaymentDate(map.get("paymentdate") != null && !map.get("paymentdate").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("paymentdate")) : null);
			it0001.setDocno(map.get("sapdocno"));
			*/
		}		
	}

	public IT0001 getIT0001() {
		return it0001;
	}
	
}
