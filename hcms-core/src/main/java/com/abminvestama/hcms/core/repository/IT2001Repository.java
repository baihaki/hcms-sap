package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.IT2001;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;

/**
 * 
 * @since 1.0.0
 * @version 1.1.0
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.1.0</td><td>Baihaki</td><td>Changed from CrudRepository to JpaRepository to use JPA specific method i.e. saveAndFlush</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessesAndSuperiorPositionCount, findByBukrsAndProcessesAndSuperiorPositionWithPaging</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessesWithPaging, findByPernrAndProcessesWithPaging</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: findByPernrWithPaging, findByPernrAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add methods: findByBukrsWithPaging, findByBukrsAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add method: findByPernrAllInfotype</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT2001Repository extends JpaRepository<IT2001, ITCompositeKeys> {

	@Query("FROM IT2001 it2001 WHERE it2001.pernr = :pernr AND it2001.id.infty = :infty AND it2001.id.subty = :subty AND it2001.id.endda = :endda AND it2001.id.begda = :begda")
	IT2001 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty,
			@Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM IT2001 it2001 WHERE it2001.pernr = :pernr AND it2001.id.infty = :infty ORDER BY it2001.id.subty ASC, it2001.id.endda DESC")
	Collection<IT2001> findByPernr(@Param("pernr") Long pernr, @Param("infty") String infty);
	
	@Query("FROM IT2001 it2001 WHERE it2001.pernr = :pernr AND it2001.id.infty = :infty AND it2001.id.subty = :subty ORDER BY it2001.id.endda DESC")
	Collection<IT2001> findByPernrAndSubty(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty);
	
	@Query("FROM IT2001 it2001 WHERE it2001.pernr = :pernr ORDER BY it2001.id.infty ASC, it2001.id.subty ASC, it2001.id.endda DESC")
	Collection<IT2001> findByPernrAllInfotype(@Param("pernr") Long pernr);
	
	@Query("FROM IT2001 it2001 WHERE it2001.bukrs.bukrs = :bukrs ORDER BY it2001.createdAt DESC, it2001.id.endda DESC, it2001.id.begda DESC")
	Page<IT2001> findByBukrsWithPaging(@Param("bukrs") String bukrs, Pageable pageRequest);
	
	@Query("FROM IT2001 it2001 WHERE it2001.bukrs.bukrs = :bukrs AND it2001.process = :process"
			+ " ORDER BY it2001.createdAt DESC, it2001.id.endda DESC, it2001.id.begda DESC")
	Page<IT2001> findByBukrsAndProcessWithPaging(@Param("bukrs") String bukrs, @Param("process") String process, Pageable pageRequest);
	
	@Query("FROM IT2001 it2001 WHERE it2001.pernr = :pernr"
			+ " ORDER BY it2001.createdAt DESC, it2001.id.endda DESC, it2001.id.begda DESC")
	Page<IT2001> findByPernrWithPaging(@Param("pernr") Long pernr, Pageable pageRequest);
	
	@Query("FROM IT2001 it2001 WHERE it2001.pernr = :pernr AND it2001.process = :process"
			+ " ORDER BY it2001.createdAt DESC, it2001.id.endda DESC, it2001.id.begda DESC")
	Page<IT2001> findByPernrAndProcessWithPaging(@Param("pernr") Long pernr, @Param("process") String process, Pageable pageRequest);
	
	@Query("FROM IT2001 it2001 WHERE it2001.bukrs.bukrs = :bukrs AND it2001.process in :processes"
			+ " ORDER BY it2001.createdAt DESC, it2001.id.endda DESC, it2001.id.begda DESC")
	Page<IT2001> findByBukrsAndProcessesWithPaging(@Param("bukrs") String bukrs, @Param("processes") String[] processes, Pageable pageRequest);
	
	@Query("FROM IT2001 it2001 WHERE it2001.pernr = :pernr AND it2001.process in :processes"
			+ " ORDER BY it2001.createdAt DESC, it2001.id.endda DESC, it2001.id.begda DESC")
	Page<IT2001> findByPernrAndProcessesWithPaging(@Param("pernr") Long pernr, @Param("processes") String[] processes, Pageable pageRequest);

	String FIND_BY_SUPERIOR_COUNT = "SELECT COUNT(*) FROM it2001 INNER JOIN it0001 a ON it2001.pernr = a.pernr" +
	        " INNER JOIN position_relations b ON a.m_position_id = b.from_position_id " +
			" WHERE it2001.bukrs = :bukrs AND it2001.process in :processes " +
	        " AND a.endda = '9999-12-31' AND b.to_position_id = :superiorPosition";
	
	String FIND_BY_SUPERIOR_QUERY = "SELECT it2001.* FROM it2001 INNER JOIN it0001 a ON it2001.pernr = a.pernr" +
	        " INNER JOIN position_relations b ON a.m_position_id = b.from_position_id " +
			" WHERE it2001.bukrs = :bukrs AND it2001.process in :processes " +
	        " AND a.endda = '9999-12-31' AND b.to_position_id = :superiorPosition " +
			" ORDER BY it2001.created_at DESC, it2001.endda DESC, it2001.begda DESC LIMIT :limit OFFSET :offset";
	
	@Query(nativeQuery=true, value=FIND_BY_SUPERIOR_COUNT)
	int findByBukrsAndProcessesAndSuperiorPositionCount(@Param("bukrs") String bukrs, @Param("processes") String[] processes
			, @Param("superiorPosition") String superiorPosition);
	
	@Query(nativeQuery=true, value=FIND_BY_SUPERIOR_QUERY)
	Collection<IT2001> findByBukrsAndProcessesAndSuperiorPositionWithPaging(@Param("bukrs") String bukrs, @Param("processes") String[] processes
			, @Param("superiorPosition") String superiorPosition, @Param("limit") int limit, @Param("offset") int offset);
	
}