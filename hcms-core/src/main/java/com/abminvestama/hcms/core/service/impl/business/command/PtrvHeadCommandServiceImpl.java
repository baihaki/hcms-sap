package com.abminvestama.hcms.core.service.impl.business.command;


import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.PtrvHead;
import com.abminvestama.hcms.core.model.entity.PtrvHeadKey;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.PtrvHeadRepository;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.PtrvHeadCommandService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Modify threadUpdate method: Add parameters (eventLogCommandService, eventLog, user)</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add UpdateThread, threadUpdate method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add updateZclmno method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("PtrvHeadCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class PtrvHeadCommandServiceImpl implements PtrvHeadCommandService {
	
	private PtrvHeadRepository ptrvHeadRepository;
	
    class UpdateThread implements Runnable {
    	
    	PtrvHead ptrvHead;
    	EventLogCommandService eventLogCommandService;
    	EventLog eventLog;
    	User user;
    	
    	UpdateThread(PtrvHead ptrvHead, EventLogCommandService eventLogCommandService, EventLog eventLog, User user) {
    		this.ptrvHead = ptrvHead;
    		this.eventLogCommandService = eventLogCommandService;
    		this.eventLog = eventLog;
    		this.user = user;
    	}
    	
		@Override
		public void run() {
			try {
				Thread.sleep(10000);
				PtrvHeadKey ptrvHeadKey = ptrvHead.getId();
				ptrvHead.setId(new PtrvHeadKey(ptrvHeadKey.getPernr(), ptrvHead.getReinr(), ptrvHeadKey.getHdvrs()));
				ptrvHead = ptrvHeadRepository.save(ptrvHead);
				eventLog.setStatus("newReinr=" + ptrvHead.getReinr());
				eventLogCommandService.save(eventLog, user);
				System.out.println("UpdateThread succesfully saved " + ptrvHead.toString());
			} catch (Exception e) {
	        	System.out.println("UpdateThread Exception:" + e.getMessage());
			}
		}
		
    }
    
    @Autowired
	PtrvHeadCommandServiceImpl(PtrvHeadRepository ptrvHeadRepository) {
		this.ptrvHeadRepository = ptrvHeadRepository;
	}
	
	@Override
	public Optional<PtrvHead> save(PtrvHead entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		//if (user != null) {
		//	entity.setUname(user.getId());
		//}
		
		try {
			return Optional.ofNullable(ptrvHeadRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist PtrvHead (Personal Information).Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(PtrvHead entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(PtrvHead entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}
	
	@Override
	public Optional<PtrvHead> updateReinr(Long newReinr, PtrvHead entity, User user)
			/*throws CannotPersistException, NoSuchMethodException, ConstraintViolationException*/ {
		
		try {
			int row = ptrvHeadRepository.updateReinr(newReinr, entity.getProcess(), entity.getSuperiorApprovedBy().getId(),
					entity.getSuperiorApprovedAt(), entity.getReason(), entity.getLastSync(), entity.getId().getReinr(), entity.getId().getPernr());
			if (row > 0) {
			    entity.setReinr(newReinr);
			}
			return Optional.ofNullable(entity);
			//return Optional.ofNullable(ptrvHeadRepository.save(entity));
		} catch (Exception e) {
			entity.setReinr(newReinr);
			System.out.println("updateReinr Exception: " + e.getMessage());
			return Optional.ofNullable(entity);
		}				
	}
	
	@Override
	public void threadUpdate(PtrvHead entity, EventLogCommandService eventLogCommandService, EventLog eventLog, User user) {
		new Thread(new UpdateThread(entity, eventLogCommandService, eventLog, user)).start();
	}
    
}