package com.abminvestama.hcms.core.service.api.business.command;

import java.util.List;

import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add threadUpdate method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface ZmedQuotausedCommandService extends DatabaseCommandService<ZmedQuotaused> {

	void threadUpdate(List<ZmedQuotaused> listZmedQuotaused);
}