package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;

import org.hibernate.annotations.Immutable;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Immutable
public class T517AKey implements Serializable {
	
	private static final long serialVersionUID = 6641245159652243461L;

	private String slart;
	
	private String slabs;
	
	public T517AKey() {}
	
	public T517AKey(String slart, String slabs) {
		this.slart = slart;
		this.slabs = slabs;
	}
	
	public String getSlart() {
		return slart;
	}
	
	public String getSlabs() {
		return slabs;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final T517AKey key = (T517AKey) o;
		
		if (slart != null ? !slart.equalsIgnoreCase(key.getSlart()) : key.slart != null) {
			return false;
		}
		
		if (slabs != null ? !slabs.equalsIgnoreCase(key.getSlabs()) : key.slabs != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = slart != null ? slart.hashCode() : 0;
		result = result * 31 + (slabs != null ? slabs.hashCode() : 0);
		return result;
	}
}