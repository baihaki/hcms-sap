package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.PositionRelationType;
import com.abminvestama.hcms.core.repository.PositionRelationTypeRepository;
import com.abminvestama.hcms.core.service.api.business.query.PositionRelationTypeQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("positionRelationTypeQueryService")
@Transactional(readOnly = true)
public class PositionRelationTypeQueryServiceImpl implements PositionRelationTypeQueryService {

	private PositionRelationTypeRepository positionRelationTypeRepository;
	
	@Autowired
	PositionRelationTypeQueryServiceImpl(PositionRelationTypeRepository positionRelationTypeRepository) {
		this.positionRelationTypeRepository = positionRelationTypeRepository;
	}
	
	@Override
	public Optional<PositionRelationType> findById(Optional<String> id) throws Exception {
		return id.map(pk -> positionRelationTypeRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<PositionRelationType>> fetchAll() {
		List<PositionRelationType> positionRelationTypes = new ArrayList<>();
		positionRelationTypeRepository.findAll().forEach(prt -> {
			positionRelationTypes.add(prt);
		});
		
		return Optional.ofNullable(positionRelationTypes);
	}

	@Override
	public Optional<PositionRelationType> findByName(final String name) {
		if (StringUtils.isEmpty(name)) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(positionRelationTypeRepository.findByName(name));
	}
}