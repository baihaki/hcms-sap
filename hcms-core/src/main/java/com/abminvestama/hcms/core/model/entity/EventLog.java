package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.abminvestama.hcms.core.model.constant.OperationType;

/**
 * 
 * @since 2.0.0
 * @version 2.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.2</td><td>Baihaki</td><td>Add field: status, to save current Document Status</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add field: mailNotification, for send notification email / not</td></tr>
 *     <tr><td>2.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Entity
@Table(name = "trx_event_log")
public class EventLog extends EventAbstractEntity {

	private static final long serialVersionUID = 2110934015547117168L;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cfg_entity_id", referencedColumnName = "id", nullable = false)
	private HCMSEntity hcmsEntity;
	
	@Column(name = "entity_object_id", nullable = false)
	private String entityObjectId;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "operation_type", nullable = false)
	private OperationType operationType;
	
	@Column(name = "event_data", nullable = false)
	private String eventData;
	
	@Column(name = "status")
	private String status;
	
	@Transient
	private boolean mailNotification;
	
	protected EventLog() {}
	
	public EventLog(HCMSEntity hcmsEntity, String entityObjectId, OperationType operationType, 
			String eventData) {
		this.hcmsEntity = hcmsEntity;
		this.entityObjectId = entityObjectId;
		this.operationType = operationType;
		this.eventData = eventData;
		this.mailNotification = true;
	}
	
	public HCMSEntity getHcmsEntity() {
		return hcmsEntity;
	}
	
	public String getEntityObjectId() {
		return entityObjectId;
	}
	
	public OperationType getOperationType() {
		return operationType;
	}
	
	public String getEventData() {
		return eventData;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isMailNotification() {
		return mailNotification;
	}

	public void setMailNotification(boolean mailNotification) {
		this.mailNotification = mailNotification;
	}
}