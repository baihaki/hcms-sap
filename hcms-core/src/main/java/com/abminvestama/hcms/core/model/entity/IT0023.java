package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;

/**
 * Class that represents master data transaction for <strong>Employment History</strong>.
 * 
 * @since 1.0.0
 * @version 2.0.0
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.0</td><td>Baihaki</td><td>Add posName, posRoles field</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Extends {@link SAPSynchronizeEntity} for Synchronization fields</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add attachment, attachmentPath field</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add status (DocumentStatus) field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 * 
 */
@Entity
@Table(name = "it0023")
public class IT0023 extends SAPSynchronizeEntity<ITCompositeKeysNoSubtype> {

	private static final long serialVersionUID = 7939907011267678419L;
	
	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;
	
	public IT0023() {}
	
	public IT0023(ITCompositeKeysNoSubtype id) {
		this();
		this.id = id;
	}
	
	@Column(name = "seqnr")
	private Long seqnr;
	
	@Column(name = "arbgb")
	private String arbgb;
	
	@Column(name = "ort01", length = 100)
	private String ort01;
	
	@ManyToOne
	@JoinColumn(name = "land1", referencedColumnName = "land1")
	private T005T land1;
	
	@ManyToOne
	@JoinColumn(name = "branc", referencedColumnName = "brsch")
	private T016T branc;
	
	@ManyToOne
	@JoinColumn(name = "taete", referencedColumnName = "taete")
	private T513C taete;
	
	@Column(name = "ansvx", length = 20)
	private String ansvx;
	
	@Column(name = "document_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private DocumentStatus status;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "infty", referencedColumnName = "infty", insertable = false, updatable = false),
		@JoinColumn(name = "attach1_subty", referencedColumnName = "subty", insertable = false, updatable = false)
	})
	private Attachment attachment;
	
	@Column(name = "attach1_path")
	private String attachmentPath;
	
	@Column(name = "posname", length = 128)
	private String posName;
	
	@Column(name = "posroles", length = 1024)
	private String posRoles;
	
	/**
	 * GET Employee SSN.
	 * 
	 * @return
	 */
	public Long getSeqnr() {
		return seqnr;
	}
	
	public void setSeqnr(Long seqnr) {
		this.seqnr = seqnr;
	}
	
	/**
	 * GET Employer.
	 * 
	 * @return
	 */
	public String getArbgb() {
		return arbgb;
	}
	
	public void setArbgb(String arbgb) {
		this.arbgb = arbgb;
	}
	
	/**
	 * GET City.
	 * 
	 * @return
	 */
	public String getOrt01() {
		return ort01;
	}
	
	public void setOrt01(String ort01) {
		this.ort01 = ort01;
	}
	
	/**
	 * GET Country Key.
	 * 
	 * @return
	 */
	public T005T getLand1() {
		return land1;
	}
	
	public void setLand1(T005T land1) {
		this.land1 = land1;
	}
	
	/**
	 * GET Industry.
	 * 
	 * @return
	 */
	public T016T getBranc() {
		return branc;
	}
	
	public void setBranc(T016T branc) {
		this.branc = branc;
	}
	
	/**
	 * GET Job.
	 * 
	 * @return
	 */
	public T513C getTaete() {
		return taete;
	}
	
	public void setTaete(T513C taete) {
		this.taete = taete;
	}
	
	/**
	 * GET Work Contract.
	 * 
	 * @return
	 */
	public String getAnsvx() {
		return ansvx;
	}
	
	public void setAnsvx(String ansvx) {
		this.ansvx = ansvx;
	}
	
	/**
	 * GET Document Status.
	 * 
	 * @return
	 */
	public DocumentStatus getStatus() {
		return status;
	}
	
	public void setStatus(DocumentStatus status) {
		this.status = status;
	}

	/**
	 * GET Attachment Document Type.
	 * 
	 * @return
	 */
	public Attachment getAttachment() {
		return attachment;
	}
	
	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	/**
	 * GET Attachment Document file path.
	 * 
	 * @return
	 */
	public String getAttachmentPath() {
		return attachmentPath;
	}
	
	public void  setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}

	/**
	 * GET Position Name.
	 * 
	 * @return
	 */
	public String getPosName() {
		return posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

	/**
	 * GET Position Roles / Job Descriptions.
	 * 
	 * @return
	 */
	public String getPosRoles() {
		return posRoles;
	}

	public void setPosRoles(String posRoles) {
		this.posRoles = posRoles;
	}
	
}