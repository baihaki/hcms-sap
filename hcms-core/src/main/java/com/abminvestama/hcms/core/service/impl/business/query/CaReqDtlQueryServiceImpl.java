package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.CaReqDtlKey;
import com.abminvestama.hcms.core.model.entity.CaReqDtl;
import com.abminvestama.hcms.core.repository.CaReqDtlRepository;
import com.abminvestama.hcms.core.service.api.business.query.CaReqDtlQueryService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("caReqDtlQueryService")
@Transactional(readOnly = true)
public class CaReqDtlQueryServiceImpl implements CaReqDtlQueryService {

	private CaReqDtlRepository caReqDtlRepository;
	
	@Autowired
	CaReqDtlQueryServiceImpl(CaReqDtlRepository caReqDtlRepository) {
		this.caReqDtlRepository = caReqDtlRepository;
	}
	
	@Override
	public Optional<Collection<CaReqDtl>> fetchAll() {
		List<CaReqDtl> entities = new ArrayList<>();
		Optional<Iterable<CaReqDtl>> entityIterable = Optional.ofNullable(caReqDtlRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}
	
	@Override
	public Optional<CaReqDtl> findById(Optional<CaReqDtlKey> id)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}	
	
	@Override
	public Optional<CaReqDtl> findOneByCompositeKey(String reqNo, Short itemNo) {
		if (reqNo == null || itemNo == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(caReqDtlRepository.findOneByCompositeKey(reqNo, itemNo));
	}
	
	@Override
	public Collection<CaReqDtl> findByReqNo(String reqNo) {
		if (reqNo == null) {
			return Collections.emptyList();
		}
		Optional<Collection<CaReqDtl>> bunchOfCaReqDtl
			= Optional.ofNullable(caReqDtlRepository.findByReqNo(reqNo));
		
		return (bunchOfCaReqDtl.isPresent()
				? bunchOfCaReqDtl.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}
		
}
