package com.abminvestama.hcms.core.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @since 1.0.0
 * @version 1.0.5
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add fields: banka, bankn</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add fields: reason</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add fields for synchronization of document status: paymentStatus2, docno2</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add fields for synchronization of document status: paymentStatus, paymentDate, docno, lastSync</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add fields: process</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Entity
@Table(name = "zmed_trxhdr")
public class ZmedTrxhdr extends SAPAbstractEntityZmed<ZmedCompositeKeyTrxhdr>{


	private static final long serialVersionUID = 2496232738510047249L;

	public ZmedTrxhdr() {}

	public ZmedTrxhdr(ZmedCompositeKeyTrxhdr id) {
		this();
		this.id = id;
	}
	
	

	@ManyToOne
	@JoinColumn(name = "bukrs", referencedColumnName = "bukrs", nullable = false, insertable = false, updatable = false)
	private T001 bukrs;
	//@Column(name = "bukrs", nullable = false, insertable = false, updatable = false)
	//private String bukrs;
	
	@Column(name = "zclmno", nullable = false, insertable = false, updatable = false)
	private String zclmno;
	
	@Column(name = "pernr")
	private Long pernr;
	
	@Column(name = "orgtx")
	private String orgtx;
	
	@Column(name = "zdivision")
	private String zdivision;
	
	@Column(name = "ename")
	private String ename;
	
	@Column(name = "clmst")
	private String clmst;
	
	@Column(name = "clmamt")
	private Double clmamt;
	
	@Column(name = "rejamt")
	private Double rejamt;
	
	@Column(name = "appamt")
	private Double appamt;
	
	@Column(name = "pagu1")
	private Double pagu1;
	
	@Column(name = "pagu2")
	private Double pagu2;
	
	@Column(name = "pagudent")
	private Double pagudent;
	
	@Column(name = "pagu2byemp")
	private Double pagu2byemp;
	
	@Column(name = "totamt")
	private Double totamt;

	@Column(name = "pasien")
	private String pasien;
	
	@Column(name = "penyakit")
	private String penyakit;

	@Temporal(TemporalType.DATE)
	@Column(name = "tglmasuk")
	private Date tglmasuk;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "tglkeluar")
	private Date tglkeluar;

	@Temporal(TemporalType.DATE)
	@Column(name = "clmdt")
	private Date clmdt;

	@Column(name = "clmby")
	private String clmby;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "aprdt")
	private Date aprdt;

	@Column(name = "aprby")
	private String aprby;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "paydt")
	private Date paydt;

	@Column(name = "payby")
	private String payby;
	
	@Column(name = "paymethod")
	private String paymethod;
	
	@Column(name = "belnr")
	private String belnr;
	
	@Column(name = "jamin")
	private String jamin;
	
	@Column(name = "notex")
	private String notex;

	@Temporal(TemporalType.DATE)
	@Column(name = "billdt")
	private Date billdt;

	@Column(name = "kodemed")
	private String kodemed;

	@Column(name = "medtydesc")
	private String medtydesc;

	@Temporal(TemporalType.DATE)
	@Column(name = "revdt")
	private Date revdt;

	@Column(name = "revby")
	private String revby;

	@Column(name = "process")
	private String process;
	
	@Column(name = "payment_status")
	private String paymentStatus;

	@Temporal(TemporalType.DATE)
	@Column(name = "payment_date")
	private Date paymentDate;

	@Column(name = "docno")
	private String docno;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_sync")
	private Date lastSync;
	
	@Column(name = "payment_status2")
	private String paymentStatus2;
	
	@Column(name = "docno2")
	private String docno2;
	
	@Column(name = "reason", length = 255)
	private String reason;
	
	@Column(name = "banka", length = 60)
	private String banka;
	
	@Column(name = "bankn", length = 100)
	private String bankn;

	
	public T001 getBukrs() {
		return bukrs;
	}

	public void setBukrs(T001 bukrs) {
		this.bukrs = bukrs;
	}

	public String getZclmno() {
		return zclmno;
	}

	public void setZclmno(String zclmno) {
		this.zclmno = zclmno;
	}

	public Long getPernr() {
		return pernr;
	}

	public void setPernr(Long pernr) {
		this.pernr = pernr;
	}

	public String getOrgtx() {
		return orgtx;
	}

	public void setOrgtx(String orgtx) {
		this.orgtx = orgtx;
	}

	public String getZdivision() {
		return zdivision;
	}

	public void setZdivision(String zdivision) {
		this.zdivision = zdivision;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getClmst() {
		return clmst;
	}

	public void setClmst(String clmst) {
		this.clmst = clmst;
	}

	public Double getClmamt() {
		return clmamt;
	}

	public void setClmamt(Double clmamt) {
		this.clmamt = clmamt;
	}

	public Double getRejamt() {
		return rejamt;
	}

	public void setRejamt(Double rejamt) {
		this.rejamt = rejamt;
	}

	public Double getAppamt() {
		return appamt;
	}

	public void setAppamt(Double appamt) {
		this.appamt = appamt;
	}

	public Double getPagu1() {
		return pagu1;
	}

	public void setPagu1(Double pagu1) {
		this.pagu1 = pagu1;
	}

	public Double getPagu2() {
		return pagu2;
	}

	public void setPagu2(Double pagu2) {
		this.pagu2 = pagu2;
	}

	public Double getPagudent() {
		return pagudent;
	}

	public void setPagudent(Double pagudent) {
		this.pagudent = pagudent;
	}

	public Double getPagu2byemp() {
		return pagu2byemp;
	}

	public void setPagu2byemp(Double pagu2byemp) {
		this.pagu2byemp = pagu2byemp;
	}

	public Double getTotamt() {
		return totamt;
	}

	public void setTotamt(Double totamt) {
		this.totamt = totamt;
	}

	public String getPasien() {
		return pasien;
	}

	public void setPasien(String pasien) {
		this.pasien = pasien;
	}

	public String getPenyakit() {
		return penyakit;
	}

	public void setPenyakit(String penyakit) {
		this.penyakit = penyakit;
	}

	public Date getTglmasuk() {
		return tglmasuk;
	}

	public void setTglmasuk(Date tglmasuk) {
		this.tglmasuk = tglmasuk;
	}

	public Date getTglkeluar() {
		return tglkeluar;
	}

	public void setTglkeluar(Date tglkeluar) {
		this.tglkeluar = tglkeluar;
	}

	public Date getClmdt() {
		return clmdt;
	}

	public void setClmdt(Date clmdt) {
		this.clmdt = clmdt;
	}

	public String getClmby() {
		return clmby;
	}

	public void setClmby(String clmby) {
		this.clmby = clmby;
	}

	public Date getAprdt() {
		return aprdt;
	}

	public void setAprdt(Date aprdt) {
		this.aprdt = aprdt;
	}

	public String getAprby() {
		return aprby;
	}

	public void setAprby(String aprby) {
		this.aprby = aprby;
	}

	public Date getPaydt() {
		return paydt;
	}

	public void setPaydt(Date paydt) {
		this.paydt = paydt;
	}

	public String getPayby() {
		return payby;
	}

	public void setPayby(String payby) {
		this.payby = payby;
	}

	public String getPaymethod() {
		return paymethod;
	}

	public void setPaymethod(String paymethod) {
		this.paymethod = paymethod;
	}

	public String getBelnr() {
		return belnr;
	}

	public void setBelnr(String belnr) {
		this.belnr = belnr;
	}

	public String getJamin() {
		return jamin;
	}

	public void setJamin(String jamin) {
		this.jamin = jamin;
	}

	public String getNotex() {
		return notex;
	}

	public void setNotex(String notex) {
		this.notex = notex;
	}

	public Date getBilldt() {
		return billdt;
	}

	public void setBilldt(Date billdt) {
		this.billdt = billdt;
	}

	public String getKodemed() {
		return kodemed;
	}

	public void setKodemed(String kodemed) {
		this.kodemed = kodemed;
	}

	public String getMedtydesc() {
		return medtydesc;
	}

	public void setMedtydesc(String medtydesc) {
		this.medtydesc = medtydesc;
	}

	public Date getRevdt() {
		return revdt;
	}

	public void setRevdt(Date revdt) {
		this.revdt = revdt;
	}

	public String getRevby() {
		return revby;
	}

	public void setRevby(String revby) {
		this.revby = revby;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getDocno() {
		return docno;
	}

	public void setDocno(String docno) {
		this.docno = docno;
	}

	public String getPaymentStatus2() {
		return paymentStatus2;
	}

	public void setPaymentStatus2(String paymentStatus2) {
		this.paymentStatus2 = paymentStatus2;
	}

	public String getDocno2() {
		return docno2;
	}

	public void setDocno2(String docno2) {
		this.docno2 = docno2;
	}

	public Date getLastSync() {
		return lastSync;
	}

	public void setLastSync(Date lastSync) {
		this.lastSync = lastSync;
	}
	
	/**
	 * GET Reason of Rejection.
	 * 
	 * @return
	 */
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * GET Employee's Bank Name.
	 * 
	 * @return
	 */
	public String getBanka() {
		return banka;
	}

	public void setBanka(String banka) {
		this.banka = banka;
	}

	/**
	 * GET Employee's Bank Account Number.
	 * 
	 * @return
	 */
	public String getBankn() {
		return bankn;
	}

	public void setBankn(String bankn) {
		this.bankn = bankn;
	}
	
}

