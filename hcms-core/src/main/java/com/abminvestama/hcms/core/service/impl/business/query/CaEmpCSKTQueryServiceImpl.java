package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.abminvestama.hcms.core.model.entity.CaEmpCSKT;
import com.abminvestama.hcms.core.repository.CaEmpCSKTRepository;
import com.abminvestama.hcms.core.service.api.business.query.CaEmpCSKTQueryService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("caEmpCSKTQueryService")
public class CaEmpCSKTQueryServiceImpl implements CaEmpCSKTQueryService {
	CaEmpCSKTRepository caEmpCSKTRepository;

	@Autowired
	CaEmpCSKTQueryServiceImpl(CaEmpCSKTRepository caEmpCSKTRepository) {
		this.caEmpCSKTRepository = caEmpCSKTRepository;
	}

	@Override
	public Optional<CaEmpCSKT> findById(Optional<Long> id) throws Exception {
		return id.map(pk -> caEmpCSKTRepository.findOne(pk));
	}

	@Override
	public Collection<CaEmpCSKT> findCostCenterActiveByPernr(Long pernr) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<CaEmpCSKT>> bunchOfCaEmpCSKT;
		bunchOfCaEmpCSKT = Optional.ofNullable(caEmpCSKTRepository.findCostCenterActiveByPernr(pernr));
		
		return (bunchOfCaEmpCSKT.isPresent()
				? bunchOfCaEmpCSKT.get().stream().collect(Collectors.toList())
						: Collections.emptyList());	
	}

	@Override
	public Collection<CaEmpCSKT> findAllCostCenterActiveByBukrs(String bukrs) {
		Optional<Collection<CaEmpCSKT>> bunchOfCaEmpCSKT;
		
		if (bukrs != null && !bukrs.isEmpty()) {
			// format cost center of company.  example: bukrs = "9900", kostl LIKE "99%"
			bukrs = bukrs.substring(0, 2) + "%";
			bunchOfCaEmpCSKT = Optional.ofNullable(caEmpCSKTRepository.findAllCostCenterActiveByBukrs(bukrs));
			
			return (bunchOfCaEmpCSKT.isPresent()
					? bunchOfCaEmpCSKT.get().stream().collect(Collectors.toList())
							: Collections.emptyList());	
		}
		else {
			return Collections.emptyList();
		}
	}

	@Override
	public Optional<Collection<CaEmpCSKT>> fetchAll() {
		List<CaEmpCSKT> listOfCaEmpCSKT = new ArrayList<>();
		Optional<Iterable<CaEmpCSKT>> bunchOfCaEmpCSKT = Optional.ofNullable(caEmpCSKTRepository.findAll());
		return bunchOfCaEmpCSKT.map(iter -> {
			iter.forEach(caEmpCSKT -> {
				listOfCaEmpCSKT.add(caEmpCSKT);
			});
			return listOfCaEmpCSKT;
		});
	}

	@Override
	public Collection<CaEmpCSKT> findEmpCSKTByKostlPoolAndPoolingCode(String kostlPool, String poolingCode) {
		kostlPool = kostlPool == null ? "" : kostlPool;
		poolingCode = poolingCode == null ? "" : poolingCode;
		
		Optional<Collection<CaEmpCSKT>> bunchOfCaEmpCSKT;
		bunchOfCaEmpCSKT = Optional.ofNullable(caEmpCSKTRepository.findEmpCSKTByKostlPoolAndPoolingCode(kostlPool, poolingCode));
		
		return (bunchOfCaEmpCSKT.isPresent()
				? bunchOfCaEmpCSKT.get().stream().collect(Collectors.toList())
						: Collections.emptyList());	
	}

	@Override
	public Collection<CaEmpCSKT> findEmpCSKTByKostlPoolAndKostlDefault(String kostlPool, String kostlDefault) {
		kostlPool = kostlPool == null ? "" : kostlPool;
		kostlDefault = kostlDefault == null ? "" : kostlDefault;
		
		Optional<Collection<CaEmpCSKT>> bunchOfCaEmpCSKT;
		bunchOfCaEmpCSKT = Optional.ofNullable(caEmpCSKTRepository.findEmpCSKTByKostlPoolAndKostlDefault(kostlPool, kostlDefault));
		
		return (bunchOfCaEmpCSKT.isPresent()
				? bunchOfCaEmpCSKT.get().stream().collect(Collectors.toList())
						: Collections.emptyList());
	}

	@Override
	public Page<CaEmpCSKT> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return caEmpCSKTRepository.findAll(pageRequest);
	}
}
