package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.T528T;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.T528TRepository;
import com.abminvestama.hcms.core.service.api.business.command.T528TCommandService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("t528tCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class T528TCommandServiceImpl implements T528TCommandService {

	private T528TRepository t528tRepository;
	
	@Autowired
	T528TCommandServiceImpl(T528TRepository t528tRepository) {
		this.t528tRepository = t528tRepository;
	}
	
	@Override
	public Optional<T528T> save(T528T entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		try {
			return Optional.ofNullable(t528tRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist T528T (Employee Position).Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(T528T entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(T528T entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}
}