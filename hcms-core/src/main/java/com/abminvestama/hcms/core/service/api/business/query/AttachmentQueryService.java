package com.abminvestama.hcms.core.service.api.business.query;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.Attachment;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 *
 */
public interface AttachmentQueryService extends DatabaseQueryService<Attachment, T591SKey>, DatabasePaginationQueryService {
	
	Page<Attachment> fetchAllWithPaging(int pageNumber);
	
	Page<Attachment> fetchAllByInfotype(String infty, int pageNumber);
}