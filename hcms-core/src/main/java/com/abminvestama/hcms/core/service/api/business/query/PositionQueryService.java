package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Optional;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.Position;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 *
 * This <i>Query Service Interface</i> is used along with class <strong>Position</strong> and 
 * represents 'Query' Layer in term of <strong>CQRS</strong> pattern.
 * Latter on, this <i>Query Service Interface</i> should be replacing its predecessor (i.e. T528TQueryService)
 * which exists on version 1.0.0.
 * 
 * @see Position
 * 
 * @since 2.0.0
 * @version 2.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add findByCode method</td></tr>
 *     <tr><td>2.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 * 
 */
public interface PositionQueryService extends DatabaseQueryService<Position, String>, DatabasePaginationQueryService {

	Page<Position> fetchAllWithPaging(int pageNumber);

	Optional<Position> findByCode(Optional<String> code) throws Exception;
}