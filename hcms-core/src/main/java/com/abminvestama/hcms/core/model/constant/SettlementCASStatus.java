package com.abminvestama.hcms.core.model.constant;

/**
 * 
 * @author wijanarko (wijanarko777@gmx.com)
 * @version 1.0.0
 * @since 1.0.0
 * 
 * Cash Advance Settlement Status
 *
 */
public enum SettlementCASStatus {
	
	BL(CashAdvanceConstant.SETTLEMENT_CAS_STATUS_BL), // Cash Advance Settlement Status - Balance
	OP(CashAdvanceConstant.SETTLEMENT_CAS_STATUS_OP), // Cash Advance Settlement Status - OverPay
	UP(CashAdvanceConstant.SETTLEMENT_CAS_STATUS_UP); // Cash Advance Settlement Status - UnderPay
	
	public String description;

	SettlementCASStatus(String description) {
		this.description = description;
	}
}
