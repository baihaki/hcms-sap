package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 * 
 * Master table for <strong>Position</strong>. Latter on, this table should 
 * replace table <strong>T528T</strong> on version 1.0.0.
 *
 */
@Entity
@Table(name = "m_positions", uniqueConstraints = { 
		@UniqueConstraint(columnNames = "code")
})
public class Position extends AbstractEntity {

	private static final long serialVersionUID = -3083291577132640290L;

	@Column(name = "code", nullable = false, unique = true)
	private String code;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "description", nullable = true)
	private String description;
	
	public Position() {}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
}