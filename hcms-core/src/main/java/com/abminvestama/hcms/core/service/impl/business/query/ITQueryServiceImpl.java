package com.abminvestama.hcms.core.service.impl.business.query;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.SAPAbstractEntity;
import com.abminvestama.hcms.core.service.api.business.query.ITQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.4
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Modify method: fetchDistinctByCriteriaWithPaging (add employee)</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Modify method: fetchDistinctByBukrsWithPaging (sort by Employee's sname)</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add method: fetchDistinctByBukrsWithPaging</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add Order By aedtm (modified time)</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("itQueryService")
@Transactional(readOnly = true)
public class ITQueryServiceImpl implements ITQueryService {

	@PersistenceContext(type = PersistenceContextType.TRANSACTION)	
	private EntityManager em;
	
	@Autowired
    Environment env;

	@Override
	public Optional<SAPAbstractEntity<?>> findById(Optional<ITCompositeKeys> id) throws Exception {
		throw new NoSuchMethodException("Method not implemented. Please use method findOneByCompositeKeys of each Infotype.");
	}

	@Override
	public Optional<Collection<SAPAbstractEntity<?>>> fetchAll() {
		return Optional.empty();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<?> fetchDistinctByCriteriaWithPaging(int pageNumber, String bukrs, Long pernr, String[] excludeStatus, Object itObject, IT0001 employee) {
		
		String queryByBukrsCount = "SELECT COUNT(*) FROM ( " +
				"SELECT DISTINCT ON (A.aedtm, A.pernr, A.subty, A.endda, A.begda, B.pernr) A.*, B.bukrs, B.endda, B.begda " +
				"FROM ITtableName A INNER JOIN it0001 B ON A.pernr = B.pernr " +
				"WHERE B.bukrs = :bukrs and B.endda >= A.endda and document_status not in :excludeStatus " +
				"ORDER BY A.aedtm desc, A.pernr, A.subty, B.pernr, A.endda desc, A.begda desc, B.endda desc, B.begda desc " +
				") AS rows";
		
		String queryByBukrs = "SELECT DISTINCT ON (A.aedtm, A.pernr, A.subty, A.endda, A.begda, B.pernr) A.*, B.bukrs, B.endda, B.begda " +
				"FROM ITtableName A INNER JOIN it0001 B ON A.pernr = B.pernr " +
				"WHERE B.bukrs = :bukrs and B.endda >= A.endda and document_status not in :excludeStatus " +
				"ORDER BY A.aedtm desc, A.pernr, A.subty, B.pernr, A.endda desc, A.begda desc, B.endda desc, B.begda desc " +
				"LIMIT :limit OFFSET :offset";
		
		String queryByPernrCount = "SELECT COUNT(*) FROM ( " +
		        "SELECT DISTINCT ON (aedtm, pernr, subty, endda, begda) * FROM ITtableName WHERE pernr = :pernr and document_status not in :excludeStatus " +
				"ORDER BY aedtm desc, pernr, subty, endda desc, begda desc ) AS rows";
		
		String queryByPernr = "SELECT DISTINCT ON (aedtm, pernr, subty, endda, begda) * " +
	            "FROM ITtableName WHERE pernr = :pernr and document_status not in :excludeStatus " +
				"ORDER BY aedtm desc, pernr, subty, endda desc, begda desc " +
				"LIMIT :limit OFFSET :offset";
		
	    try {
	    	itObject.getClass().getMethod("getSubty");
		} catch (NoSuchMethodException | SecurityException e) {
			// No Subtype field
			queryByBukrsCount = queryByBukrsCount.replace(", A.subty", StringUtils.EMPTY);
			queryByBukrs = queryByBukrs.replace(", A.subty", StringUtils.EMPTY);
			queryByPernrCount = queryByPernrCount.replace(", subty", StringUtils.EMPTY);
			queryByPernr = queryByPernr.replace(", subty", StringUtils.EMPTY);
		}
		
		Pageable pageRequest = createPageRequest(pageNumber);
		BigInteger total = BigInteger.ZERO;
		
		List<Object> istOfITObject = new ArrayList<Object>();
		List<String> listExcludes = new ArrayList<String>();
		for (int i = 0; i < excludeStatus.length; i++) {
			listExcludes.add(excludeStatus[i]);
		}
		
		if (!StringUtils.isEmpty(bukrs)) {
			
			// check if there is customize workflow admin task
			if (env.getProperty("workflow.filter.admin.".concat(bukrs)) != null) {
				String filter = env.getProperty("workflow.filter.admin.".concat(bukrs));
				String[] params = filter.split(",");
				String whereClause = "WHERE B.bukrs = :bukrs ";
				for (int i = 0; i < params.length; i++) {
					String[] subParams = params[i].split("\\.");
					Object object = employee;
					for (int j=0; j < subParams.length; j++) {
						try {
							Method getMethod = object.getClass().getMethod(
									"get".concat(subParams[j]));
							object = getMethod.invoke(object);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					String paramValue = (String) object;
					whereClause = whereClause.concat("and B.").concat(subParams[subParams.length-1].toLowerCase()).
							concat("='").concat(paramValue).concat("' ");
				}
				queryByBukrsCount = queryByBukrsCount.replace("WHERE B.bukrs = :bukrs ", whereClause);
				queryByBukrs = queryByBukrs.replace("WHERE B.bukrs = :bukrs ", whereClause);
			}
			
			total =  (BigInteger) em.createNativeQuery(queryByBukrsCount.replaceAll("ITtableName", itObject.getClass().getSimpleName().toLowerCase())).
					setParameter("bukrs", bukrs).setParameter("excludeStatus", listExcludes).getSingleResult();
			istOfITObject = em.createNativeQuery(queryByBukrs.replaceAll("ITtableName", itObject.getClass().getSimpleName().toLowerCase()), itObject.getClass()).
					setParameter("bukrs", bukrs).setParameter("excludeStatus", listExcludes).
					setParameter("limit", pageRequest.getPageSize()).setParameter("offset", pageRequest.getOffset()).getResultList();
		} else if (pernr != null) {
			total =  (BigInteger) em.createNativeQuery(queryByPernrCount.replaceAll("ITtableName", itObject.getClass().getSimpleName().toLowerCase())).
					setParameter("pernr", pernr).setParameter("excludeStatus", listExcludes).getSingleResult();
			istOfITObject = em.createNativeQuery(queryByPernr.replaceAll("ITtableName", itObject.getClass().getSimpleName().toLowerCase()), itObject.getClass()).
					setParameter("pernr", pernr).setParameter("excludeStatus", listExcludes).
					setParameter("limit", pageRequest.getPageSize()).setParameter("offset", pageRequest.getOffset()).getResultList();
		}
		
		Page<?> page = new PageImpl<Object>(istOfITObject, pageRequest, total.longValue());
		
		return page;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Page<?> fetchDistinctByBukrsWithPaging(int pageNumber, String bukrs, String superiorPosition) {
		String queryByBukrsCount = "SELECT COUNT(*) FROM ( " +
                                   "SELECT DISTINCT ON (A.sname, A.pernr) A.* FROM it0001 A " +
				                   "WHERE A.bukrs = :bukrs) as rows";
		
		String queryByBukrs = "SELECT DISTINCT ON (A.sname, A.pernr) A.* FROM it0001 A " +
		                      "WHERE A.bukrs = :bukrs " +
                              "ORDER BY A.sname, A.pernr, A.endda desc, A.begda desc LIMIT :limit OFFSET :offset";
		
		Pageable pageRequest = pageNumber > 0 ? createPageRequest(pageNumber) : createPageRequest(1, 100000);
		BigInteger total = BigInteger.ZERO;
		
		List<Object> listOfITObject = new ArrayList<Object>();
		
		if (StringUtils.isNotEmpty(superiorPosition)) {
			queryByBukrsCount = queryByBukrsCount.replace("FROM it0001 A", "FROM it0001 A INNER JOIN position_relations B ON A.m_position_id = B.from_position_id")
					.replace("WHERE A.bukrs = :bukrs", "WHERE A.bukrs = :bukrs and B.to_position_id = :superiorPosition");
			queryByBukrs = queryByBukrs.replace("FROM it0001 A", "FROM it0001 A INNER JOIN position_relations B ON A.m_position_id = B.from_position_id")
					.replace("WHERE A.bukrs = :bukrs", "WHERE A.bukrs = :bukrs and B.to_position_id = :superiorPosition");
			total = (BigInteger) em.createNativeQuery(queryByBukrsCount).setParameter("bukrs", bukrs).
					setParameter("superiorPosition", superiorPosition).getSingleResult();
			listOfITObject = em.createNativeQuery(queryByBukrs, IT0001.class).setParameter("bukrs", bukrs).
					setParameter("superiorPosition", superiorPosition).
					setParameter("limit", pageRequest.getPageSize()).setParameter("offset", pageRequest.getOffset()).getResultList();
		} else {

			if (StringUtils.isEmpty(bukrs)) {
				bukrs = "NO_BUKRS";
				queryByBukrsCount = queryByBukrsCount.replaceFirst("WHERE A.bukrs = :bukrs", "WHERE A.bukrs != :bukrs or A.bukrs is null");
				queryByBukrs = queryByBukrs.replaceFirst("WHERE A.bukrs = :bukrs", "WHERE A.bukrs != :bukrs or A.bukrs is null");
			}
			
			total = (BigInteger) em.createNativeQuery(queryByBukrsCount).setParameter("bukrs", bukrs).getSingleResult();
			listOfITObject = em.createNativeQuery(queryByBukrs, IT0001.class).setParameter("bukrs", bukrs).
					setParameter("limit", pageRequest.getPageSize()).setParameter("offset", pageRequest.getOffset()).getResultList();
		}
		
		Page<?> page = new PageImpl<Object>(listOfITObject, pageRequest, total.longValue());
		
		return page;

	}
}