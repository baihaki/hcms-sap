package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T505S;
import com.abminvestama.hcms.core.model.entity.T505SKey;
import com.abminvestama.hcms.core.repository.T505SRepository;
import com.abminvestama.hcms.core.service.api.business.query.T505SQueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("t505sQueryService")
@Transactional(readOnly = true)
public class T505SQueryServiceImpl implements T505SQueryService {

	private T505SRepository t505sRepository;
	
	@Autowired
	T505SQueryServiceImpl(T505SRepository t505sRepository) {
		this.t505sRepository = t505sRepository;
	}
	
	@Override
	public Optional<T505S> findById(Optional<T505SKey> id) throws Exception {
		return id.map(pk -> t505sRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<T505S>> fetchAll() {
		List<T505S> listOfT505S = new ArrayList<>();
		Optional<Iterable<T505S>> bunchOfT505S = Optional.ofNullable(t505sRepository.findAll());
		return bunchOfT505S.map(t505sIter -> {
			t505sIter.forEach(t505s -> {
				listOfT505S.add(t505s);
			});
			return listOfT505S;
		});
	}
}