package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.TEVEN;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;

public interface TEVENCommandService extends DatabaseCommandService<TEVEN> {
}