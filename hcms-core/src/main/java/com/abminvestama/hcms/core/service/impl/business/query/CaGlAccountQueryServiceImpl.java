package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.CaGlAccount;
import com.abminvestama.hcms.core.repository.CaGlAccountRepository;
import com.abminvestama.hcms.core.service.api.business.query.CaGlAccountQueryService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("caGlAccountQueryService")
@Transactional(readOnly = true)
public class CaGlAccountQueryServiceImpl implements CaGlAccountQueryService {

	private CaGlAccountRepository caGlAccountRepository;

	@Autowired
	CaGlAccountQueryServiceImpl(CaGlAccountRepository caGlAccountRepository) {
		this.caGlAccountRepository = caGlAccountRepository;
	}
	
	@Override
	public Optional<CaGlAccount> findById(Optional<Long> id) throws Exception {
		return id.map(pk -> caGlAccountRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<CaGlAccount>> fetchAll() {
		List<CaGlAccount> caGlAccounts = new ArrayList<>();
		caGlAccountRepository.findAll().forEach(pr -> {
			caGlAccounts.add(pr);
		});
		
		return Optional.of(caGlAccounts);
	}

	@Override
	public Page<CaGlAccount> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return caGlAccountRepository.findAll(pageRequest);
	}

	@Override
	public Page<CaGlAccount> fetchValidAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return caGlAccountRepository.findValidAll(pageRequest);
	}

	@Override
	public Collection<CaGlAccount> findByGroupAccountId(Long groupAccountId) {
		return caGlAccountRepository.findByGroupAccountId(groupAccountId);
	}

	@Override
	public Collection<CaGlAccount> findValidByGroupAccountId(Long groupAccountId) {
		return caGlAccountRepository.findValidByGroupAccountId(groupAccountId);
	}

	@Override
	public Page<CaGlAccount> findByGroupAccountId(Long groupAccountId, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return caGlAccountRepository.findByGroupAccountId(groupAccountId, pageRequest);
	}

	@Override
	public Page<CaGlAccount> findValidByGroupAccountId(Long groupAccountId, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return caGlAccountRepository.findByGroupAccountId(groupAccountId, pageRequest);
	}
		
}