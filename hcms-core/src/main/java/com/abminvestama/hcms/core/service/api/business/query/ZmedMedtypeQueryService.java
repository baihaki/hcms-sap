package com.abminvestama.hcms.core.service.api.business.query;


import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyMedtype;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;


public interface ZmedMedtypeQueryService extends DatabaseQueryService<ZmedMedtype, ZmedCompositeKeyMedtype>, DatabasePaginationQueryService{
	//Optional<ZmedMedtype> findByEntityName(final String entityName);
	

	@NotNull
	Optional<ZmedMedtype> findOneByCompositeKey(String bukrs, String kodemed, Date datab, Date datbi);
	
	
	@NotNull
	Collection<ZmedMedtype> findByBukrs(String bukrs, Date datbi);
	
}

