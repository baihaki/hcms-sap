package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyTrxitm;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;
import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;
import com.abminvestama.hcms.core.repository.ZmedTrxitmRepository;
import com.abminvestama.hcms.core.service.api.business.query.ZmedTrxitmQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Modify findOneByCompositeKey method: Add clmno</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("zmedTrxitmQueryService")
@Transactional(readOnly = true)
public class ZmedTrxitmQueryServiceImpl implements ZmedTrxitmQueryService {

	private ZmedTrxitmRepository zmedTrxitmRepository;
	
	@Autowired
	ZmedTrxitmQueryServiceImpl(ZmedTrxitmRepository zmedTrxitmRepository) {
		this.zmedTrxitmRepository = zmedTrxitmRepository;
	}
	
	//@Override
	//public Optional<ZmedTrxitm> findById(Optional<String> id) throws Exception {
	//	return id.map(pk -> zmedTrxitmRepository.findOne(pk));
	//}

	@Override
	public Optional<Collection<ZmedTrxitm>> fetchAll() {
		List<ZmedTrxitm> entities = new ArrayList<>();
		Optional<Iterable<ZmedTrxitm>> entityIterable = Optional.ofNullable(zmedTrxitmRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}

	@Override
	public Optional<ZmedTrxitm> findById(Optional<ZmedCompositeKeyTrxitm> id)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<ZmedTrxitm> findOneByCompositeKey(String itmno, String clmno) {
		if (itmno == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(zmedTrxitmRepository.findOneByCompositeKey(itmno, clmno));
	}

	@Override
	public String findLastId() {
		String itmno = zmedTrxitmRepository.findLastId();
		return itmno; 
	}

	/*@Override
	public Optional<ZmedTrxitm> findByEntityName(final String entityName) {
		return Optional.ofNullable(zmedTrxitmRepository.findByKodemed(entityName));
	}*/
	
	

	@Override
	public Collection<ZmedTrxitm> findByClmno(String clmno) {
		if (clmno == null) {
			return Collections.emptyList();
		}
		Optional<Collection<ZmedTrxitm>> bunchOfZmed
			= Optional.ofNullable(zmedTrxitmRepository.findByClmno(clmno));
		
		return (bunchOfZmed.isPresent()
				? bunchOfZmed.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}
}
