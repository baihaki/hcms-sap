package com.abminvestama.hcms.core.service.api.business.query;

import com.abminvestama.hcms.core.model.entity.T505S;
import com.abminvestama.hcms.core.model.entity.T505SKey;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface T505SQueryService extends DatabaseQueryService<T505S, T505SKey>, DatabasePaginationQueryService {
}