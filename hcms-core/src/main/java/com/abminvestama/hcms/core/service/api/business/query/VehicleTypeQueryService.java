package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Optional;

import com.abminvestama.hcms.core.model.entity.VehicleType;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface VehicleTypeQueryService extends DatabaseQueryService<VehicleType, String> {

	Optional<VehicleType> findByIdentName(final String identName);
}