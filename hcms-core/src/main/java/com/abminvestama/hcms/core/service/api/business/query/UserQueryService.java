package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.1.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)<br>wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.1.1</td><td>Wijanarko</td><td>Add methods: fetchPernrByToPositionId</td></tr>
 *     <tr><td>1.1.0</td><td>Baihaki</td><td>Add methods: fetchByCompanyAndRole, findByPernrAndRelationRole</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add findByEmployeeName method with superiorPosition parameter</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add findByEmployeeName method</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add fetchAdminByCompany method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findByPernr method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 * 
 * This class is a representation of 'Q' in <strong>CQRS</strong> pattern (<i>Command Query Responsibility Segregation</i>). 
 * This class contains all query operations for object <code>User</code> in the system.
 *
 */
public interface UserQueryService extends DatabaseQueryService<User, String>, UserDetailsService {

	Optional<User> findByUsername(final Optional<String> username);

	Optional<User> findByPernr(Long pernr);

	Optional<Collection<User>> fetchAdminByCompany(String bukrs);
	
	Optional<Collection<User>> findByEmployeeName(String bukrs, String employeeName);

	Optional<Collection<User>> findByEmployeeName(String bukrs,
			String employeeName, String superiorPosition);

	List<Long> fetchPernrByToPositionId(String toPositionId);
	
	Optional<Collection<User>> fetchByCompanyAndRole(String bukrs,
			String roleName);

	Optional<User> findByPernrAndRelationRole(Long pernr, String roleName);
}