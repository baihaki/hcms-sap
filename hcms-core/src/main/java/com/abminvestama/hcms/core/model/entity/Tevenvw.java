package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "tevenvw")
public class Tevenvw implements Serializable {

	private static final long serialVersionUID = -8917178156473475169L;

	/*
	 * PERNR.LDATE
	 */
	@Id
	@Column(name = "id", nullable = false, unique = true, insertable = false, updatable = false)
	private String id;
	
	/*
	 * Personal Number
	 */
	@Column(name = "pernr", columnDefinition = "pernr")
	private Long pernr;

	/*
	 * Employee Name
	 */
	@Column(name = "ename", columnDefinition = "ename")
	private String ename;

	@Temporal(TemporalType.DATE)
	@Column(name = "ldate", columnDefinition = "ldate")
	private Date ldate;

	/*
	 * Begin time actual(machine) - clock in
	 */
	@Column(name = "begagtime", columnDefinition = "begagtime")
	private Date begagtime;

	/*
	 * Begin Logic Time(modify) - clock in
	 */
	@Column(name = "begltime", columnDefinition = "begltime")
	private Date begltime;

	/*
	 * End time actual(machine) - clock out
	 */
	@Column(name = "endagtime", columnDefinition = "endagtime")
	private Date endagtime;

	/*
	 * End Login Time(modify) - clock out
	 */
	@Column(name = "endltime", columnDefinition = "endltime")
	private Date endltime;

	/*
	 * working actual hour
	 */
//	@Column(name = "wahour", columnDefinition = "wahour")
//	private Double wahour;

	/*
	 * working delay hour
	 */
	@Column(name = "wdhour", columnDefinition = "wdhour")
	private Double wdhour;

	/*
	 * working hour
	 */
	@Column(name = "whour", columnDefinition = "whour")
	private Double whour;

	/*
	 * Productivity Hour
	 */
//	@Column(name = "phour", columnDefinition = "phour")
//	private Double phour;

	/*
	 * Status teven clock in
	 */
	@Column(name = "begprocess", columnDefinition = "begprocess")
	private String begprocess;

	/*
	 * Status teven clock out
	 */
	@Column(name = "endprocess", columnDefinition = "endprocess")
	private String endprocess;

	/*
	 * Begin Transaction Code - clock in
	 */
	@Column(name = "begtrxcode", columnDefinition = "begtrxcode")
	private String begTrxCode;

	/*
	 * End Transaction Code - clock out
	 */
	@Column(name = "endtrxcode", columnDefinition = "endtrxcode")
	private String endTrxCode;

	/**
	 * Begin Reason
	 */
	@Column(name = "begreason", columnDefinition = "begreason")
	private String begreason;

	/**
	 * End Reason
	 */
	@Column(name = "endreason", columnDefinition = "endreason")
	private String endreason;

	/*
	 * First Door
	@Column(name = "firstdoor", columnDefinition = "firstdoor")
	private String firstdoor;
	 */
	
	/*
	 * Last Door
	@Column(name = "lastdoor", columnDefinition = "lastdoor")
	private String lastdoor;
	 */

	/*
	 * Division Name
	 */
	@Transient
	private String divisionName;

	/*
	 * Department Name
	 */
	@Transient
	private String departmentName;

	/**
	 * Begin Approval Note
	 */
	@Column(name = "begaprvnote", columnDefinition = "begaprvnote")
	private String begaprvnote;

	/**
	 * End Approval Note
	 */
	@Column(name = "endaprvnote", columnDefinition = "endaprvnote")
	private String endaprvnote;

	public Tevenvw() {}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getPernr() {
		return pernr;
	}

	public void setPernr(Long pernr) {
		this.pernr = pernr;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public Date getLdate() {
		return ldate;
	}

	public void setLdate(Date ldate) {
		this.ldate = ldate;
	}

	public Date getBegagtime() {
		return begagtime;
	}

	public void setBegagtime(Date begagtime) {
		this.begagtime = begagtime;
	}

	public Date getBegltime() {
		return begltime;
	}

	public void setBegltime(Date begltime) {
		this.begltime = begltime;
	}

	public Date getEndagtime() {
		return endagtime;
	}

	public void setEndagtime(Date endagtime) {
		this.endagtime = endagtime;
	}

	public Date getEndltime() {
		return endltime;
	}

	public void setEndltime(Date endltime) {
		this.endltime = endltime;
	}

//	public Double getWahour() {
//		return wahour;
//	}
//
//	public void setWahour(Double wahour) {
//		this.wahour = wahour;
//	}

	public Double getWdhour() {
		return wdhour;
	}

	public void setWdhour(Double wdhour) {
		this.wdhour = wdhour;
	}

	public Double getWhour() {
		return whour;
	}

	public void setWhour(Double whour) {
		this.whour = whour;
	}

//	public Double getPhour() {
//		return phour;
//	}
//
//	public void setPhour(Double phour) {
//		this.phour = phour;
//	}

	public String getBegprocess() {
		return begprocess;
	}

	public void setBegprocess(String begprocess) {
		this.begprocess = begprocess;
	}

	public String getEndprocess() {
		return endprocess;
	}

	public void setEndprocess(String endprocess) {
		this.endprocess = endprocess;
	}

	public String getBegTrxCode() {
		return begTrxCode;
	}

	public void setBegTrxCode(String begTrxCode) {
		this.begTrxCode = begTrxCode;
	}

	public String getEndTrxCode() {
		return endTrxCode;
	}

	public void setEndTrxCode(String endTrxCode) {
		this.endTrxCode = endTrxCode;
	}

	/*
	public String getFirstdoor() {
		return firstdoor;
	}

	public void setFirstdoor(String firstdoor) {
		this.firstdoor = firstdoor;
	}

	public String getLastdoor() {
		return lastdoor;
	}

	public void setLastdoor(String lastdoor) {
		this.lastdoor = lastdoor;
	}
	*/

	public String getBegreason() {
		return begreason;
	}
	
	public void setBegreason(String begreason) {
		this.begreason = begreason;
	}
	
	public String getEndreason() {
		return endreason;
	}
	
	public void setEndreason(String endreason) {
		this.endreason = endreason;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getBegaprvnote() {
		return begaprvnote;
	}

	public void setBegaprvnote(String begaprvnote) {
		this.begaprvnote = begaprvnote;
	}

	public String getEndaprvnote() {
		return endaprvnote;
	}

	public void setEndaprvnote(String endaprvnote) {
		this.endaprvnote = endaprvnote;
	}

}
