package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;

import com.abminvestama.hcms.core.model.entity.IT0006;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;
/**

 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@PreAuthorize("isFullyAuthenticated()")
public interface IT0006QueryService extends DatabaseQueryService<IT0006, ITCompositeKeys>, DatabasePaginationQueryService {

	@NotNull
	Optional<IT0006> findOneByCompositeKey(Long pernr, String subty, Date endda, Date begda);
	
	@NotNull
	Collection<IT0006> findByPernr(Long pernr);
	
	@NotNull
	Page<IT0006> findByPernrAndSubty(Long pernr, String status, String page, String subty);

	@NotNull
	Page<IT0006> findByStatus(String status, int pageNumber);
	

	@NotNull
	Page<IT0006> findByPernrAndStatus(long pernr, String status, int pageNumber);
}