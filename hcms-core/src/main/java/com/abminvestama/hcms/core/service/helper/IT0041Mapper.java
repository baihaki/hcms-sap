package com.abminvestama.hcms.core.service.helper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0041;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.T548T;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 3.0.0
 * @since 3.0.0
 *
 */
public class IT0041Mapper {
	
	private IT0041 it0041;
	
	private IT0041Mapper() {}
	
	public IT0041Mapper(Map<String, String> map, CommonServiceFactory commonServiceFactory) {
		if (map == null) {
			new IT0041Mapper();
			it0041 = new IT0041();
		} else {
			ITCompositeKeysNoSubtype key = new ITCompositeKeysNoSubtype(Long.valueOf(map.get("field1")),
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("field2")),
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("field3")));
			it0041 = new IT0041(key);
			it0041.setSeqnr(map.get("field4") != null ? Long.valueOf(map.get("field4")) : 0);
			
			// set Dar01 to Dar09, and Da01 to Dat09
			for (int i=0; i<9; i++) {
				String darField = String.valueOf(i*2+7);
				String datField = String.valueOf(i*2+8);
				if (map.get("field".concat(darField)) != null && map.get("field".concat(datField)) != null &&
						!map.get("field".concat(datField)).equals("0000-00-00")) {
					Optional<T548T> darObject;
					try {
						darObject = commonServiceFactory.getT548TQueryService().findById(Optional.ofNullable(map.get("field".concat(darField))));
					} catch (Exception e) {
						darObject = Optional.empty();
					}
					try {
						Method setDar = it0041.getClass().getMethod(
								"setDar0".concat(String.valueOf(i+1)), T548T.class);
						Method setDat = it0041.getClass().getMethod(
								"setDat0".concat(String.valueOf(i+1)), Date.class);
						if (darObject.isPresent()) {
							setDar.invoke(it0041, darObject.get());
							setDat.invoke(it0041, CommonDateFunction.convertDateRequestParameterIntoDate(map.get("field".concat(datField))));
						}
					} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}
			
			
		}		
	}

	public IT0041 getIT0041() {
		return it0041;
	}
	
}
