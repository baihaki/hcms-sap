package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.PtrvHeadKey;
import com.abminvestama.hcms.core.model.entity.PtrvHead;
import com.abminvestama.hcms.core.repository.PtrvHeadRepository;
import com.abminvestama.hcms.core.service.api.business.query.PtrvHeadQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.1.0
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.1.0</td><td>Baihaki</td><td>Add methods: findByPernrAndProcessesAndDatv1</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Add parameter "notStatus" to filter by paymentStatus or not (optional)</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add superiorPosition parameter on methods: findByBukrsAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add forms parameter on findByPernrAndProcessesWithPaging</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessesWithPaging, findByPernrAndProcessesWithPaging</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add methods: findByPernrAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("ptrvHeadQueryService")
@Transactional(readOnly = true)
public class PtrvHeadQueryServiceImpl implements PtrvHeadQueryService {

	private PtrvHeadRepository ptrvHeadRepository;
	
	@Autowired
	PtrvHeadQueryServiceImpl(PtrvHeadRepository ptrvHeadRepository) {
		this.ptrvHeadRepository = ptrvHeadRepository;
	}
	
	@Override
	public Optional<Collection<PtrvHead>> fetchAll() {
		List<PtrvHead> entities = new ArrayList<>();
		Optional<Iterable<PtrvHead>> entityIterable = Optional.ofNullable(ptrvHeadRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}
	
	
	

	@Override
	public Optional<PtrvHead> findOneByCompositeKey(Integer hdvrs, Long pernr,	Long reinr) {
		if (hdvrs == null || pernr == null || reinr == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(ptrvHeadRepository.findOneByCompositeKey(hdvrs, pernr, reinr));
	}


	@Override
	public Optional<PtrvHead> findById(Optional<PtrvHeadKey> id)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/*@Override
	public Collection<PtrvHead> findByKodemed(String bukrs, String kodemed,
			Date datbi) {
		if (bukrs == null || kodemed == null || datbi == null) {
			return Collections.emptyList();
		}
		

		Optional<Collection<PtrvHead>> bunchOfZmed
			= Optional.ofNullable(ptrvHeadRepository.findByKodemed(bukrs, kodemed, datbi));
		
		return (bunchOfZmed.isPresent()
				? bunchOfZmed.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}*/
	

	/*
	@Override
	public Page<PtrvHead> findByBukrsAndProcessWithPaging(String bukrs, String process, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (StringUtils.isEmpty(process)) ? ptrvHeadRepository.findByBukrsWithPaging(bukrs, notStatus, pageRequest) 
				: ptrvHeadRepository.findByBukrsAndProcessWithPaging(bukrs, process, notStatus, pageRequest);
	}

	@Override
	public Page<PtrvHead> findByPernrAndProcessWithPaging(long pernr, String process, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (StringUtils.isEmpty(process)) ? ptrvHeadRepository.findByPernrWithPaging(pernr, notStatus, pageRequest) 
				: ptrvHeadRepository.findByPernrAndProcessWithPaging(pernr, process, notStatus, pageRequest);
	}

	@Override
	public Page<PtrvHead> findByBukrsAndProcessesWithPaging(String bukrs, String[] processes, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (processes == null) ? ptrvHeadRepository.findByBukrsWithPaging(bukrs, notStatus, pageRequest) 
				: ptrvHeadRepository.findByBukrsAndProcessesWithPaging(bukrs, processes, notStatus, pageRequest);
	}

	@Override
	public Page<PtrvHead> findByPernrAndProcessesWithPaging(long pernr, String[] processes, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (processes == null) ? ptrvHeadRepository.findByPernrWithPaging(pernr, notStatus, pageRequest) 
				: ptrvHeadRepository.findByPernrAndProcessesWithPaging(pernr, processes, notStatus, pageRequest);
	}
	*/
	

	@Override
	public Page<PtrvHead> findByBukrsAndProcessWithPaging(String bukrs, String process, short[] forms, String notStatus, String superiorPosition, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		if (!StringUtils.isEmpty(superiorPosition)) {
			int total = ptrvHeadRepository.findByBukrsAndProcessAndSuperiorPositionCount(bukrs, process, forms, notStatus, superiorPosition);
			List<PtrvHead> listOfPtrvHead = ptrvHeadRepository.findByBukrsAndProcessAndSuperiorPositionWithPaging(
					bukrs, process, forms, notStatus, superiorPosition, pageRequest.getPageSize(), pageRequest.getOffset()).
					stream().collect(Collectors.toList());
			Page<PtrvHead> page = new PageImpl<PtrvHead>(listOfPtrvHead, pageRequest, total);
			return page;
		}
		
		return (StringUtils.isEmpty(process)) ? ptrvHeadRepository.findByBukrsWithPaging(bukrs, forms, notStatus, pageRequest) 
				: ptrvHeadRepository.findByBukrsAndProcessWithPaging(bukrs, process, forms, notStatus, pageRequest);
	}

	@Override
	public Page<PtrvHead> findByPernrAndProcessWithPaging(long pernr, String process, short[] forms, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (StringUtils.isEmpty(process)) ? ptrvHeadRepository.findByPernrWithPaging(pernr, forms, notStatus, pageRequest) 
				: ptrvHeadRepository.findByPernrAndProcessWithPaging(pernr, process, forms, notStatus, pageRequest);
	}

	@Override
	public Page<PtrvHead> findByBukrsAndProcessesWithPaging(String bukrs, String[] processes, short[] forms, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (processes == null) ? ptrvHeadRepository.findByBukrsWithPaging(bukrs, forms, notStatus, pageRequest) 
				: ptrvHeadRepository.findByBukrsAndProcessesWithPaging(bukrs, processes, forms, notStatus, pageRequest);
	}

	@Override
	public Page<PtrvHead> findByPernrAndProcessesWithPaging(long pernr, String[] processes, short[] forms, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (processes == null) ? ptrvHeadRepository.findByPernrWithPaging(pernr, forms, notStatus, pageRequest) 
				: ptrvHeadRepository.findByPernrAndProcessesWithPaging(pernr, processes, forms, notStatus, pageRequest);
	}

	@Override
	public Optional<Collection<PtrvHead>> findByPernrAndProcessesAndDatv1(Long pernr, String[] processes, Date datv1) {
		if (pernr == null || processes == null || datv1 == null) {
			return Optional.empty();
		}
		
		Optional<Collection<PtrvHead>> bunchOfPtrvHead
			= Optional.ofNullable(ptrvHeadRepository.findByPernrAndProcessesAndDatv1(pernr, processes, datv1));
		
		return bunchOfPtrvHead;
	}
	
}
