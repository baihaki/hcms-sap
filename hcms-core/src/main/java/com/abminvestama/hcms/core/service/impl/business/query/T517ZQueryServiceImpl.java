package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T517T;
import com.abminvestama.hcms.core.model.entity.T517Z;
import com.abminvestama.hcms.core.model.entity.T517ZKey;
import com.abminvestama.hcms.core.repository.T517ZRepository;
import com.abminvestama.hcms.core.service.api.business.query.T517ZQueryService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("t517zQueryService")
@Transactional(readOnly = true)
public class T517ZQueryServiceImpl implements T517ZQueryService {

	private T517ZRepository t517zRepository;
	
	@Autowired
	T517ZQueryServiceImpl(T517ZRepository t517zRepository) {
		this.t517zRepository = t517zRepository;
	}
	
	@Override
	public Optional<T517Z> findById(Optional<T517ZKey> id) throws Exception {
		return id.map(pk -> t517zRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<T517Z>> fetchAll() {
		List<T517Z> listOfT517Z = new ArrayList<>();
		Optional<Iterable<T517Z>> iterableT517Z = Optional.ofNullable(t517zRepository.findAll());
		return iterableT517Z.map(iter -> {
			iter.forEach(t517z -> {
				listOfT517Z.add(t517z);
			});
			return listOfT517Z;
		});
	}

	@Override
	public Page<T517Z> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return t517zRepository.findAll(pageRequest);
	}

	@Override
	public Collection<T517Z> findBySlart(T517T slart) {
		Optional<Collection<T517Z>> bunchOfT517Z = Optional.ofNullable(t517zRepository.findBySlart(slart));
		return (bunchOfT517Z.isPresent() ?
				bunchOfT517Z.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}
}