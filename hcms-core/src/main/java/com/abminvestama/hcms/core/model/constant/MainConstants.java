package com.abminvestama.hcms.core.model.constant;

/**
 * 
 * @author wijanarko (wijanarko777@gmx.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class MainConstants {
	//public static String dnsHostName = "abmjkt01p1.abmm.tmt.co.id";
	//public static String domainLdap = "abmm.tmt.co.id";
	//public static String portLdap = "389";
	public static boolean isLoginLdap = false;
	public static final String passInitLdap = "ATREUS_GLOBAL_DEFAULT_PASS_LDAP";
	public static final String passLdap = "$2a$10$J7IzOOC4kN3xsd/D0VhrP.MNsUuPSjSQpJWOZsGmleydufEuo0EZ2";
	
	public static final String TRANSACTION_APPROVAL_PROCESS = "trx_approval_process";
}
