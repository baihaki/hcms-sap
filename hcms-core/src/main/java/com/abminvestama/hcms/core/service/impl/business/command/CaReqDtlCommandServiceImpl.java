package com.abminvestama.hcms.core.service.impl.business.command;


import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.CaReqDtl;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.CaReqDtlRepository;
import com.abminvestama.hcms.core.service.api.business.command.CaReqDtlCommandService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("caReqDtlCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CaReqDtlCommandServiceImpl implements CaReqDtlCommandService {
	
	private CaReqDtlRepository caReqDtlRepository;
		
		@Autowired
		CaReqDtlCommandServiceImpl(CaReqDtlRepository caReqDtlRepository) {
			this.caReqDtlRepository = caReqDtlRepository;
		}
		
		@Override
		public Optional<CaReqDtl> save(CaReqDtl entity, User user)
				throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
			if (user != null) {
				if (entity.getCreatedBy() == null) {
					entity.setCreatedBy(user);
				}
				entity.setUpdatedBy(user);
			}
			
			try {
				return Optional.ofNullable(caReqDtlRepository.save(entity));
			} catch (Exception e) {
				throw new CannotPersistException("Cannot persist CaReqDtl (Cash Advance Request Detail). Reason=" + e.getMessage());
			}				
		}

		@Override
		public boolean delete(CaReqDtl entity, User user) throws NoSuchMethodException, ConstraintViolationException {
			// no delete at this version.
			throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
		}

		@Override
		public boolean restore(CaReqDtl entity, User user) throws NoSuchMethodException, ConstraintViolationException {
			// since we don't have delete operation, then we don't need the 'restore' as well
			throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
		}
		/*
		@Override
		public Optional<CaReqDtl> updateTripnumber(Long newReinr, CaReqDtl entity, User user)
				throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
			
			try {
				int row = caReqDtlRepository.updateTripnumber(newReinr, entity.getId().getReinr(), entity.getId().getReceiptno(), entity.getId().getPernr()); 
				if (row > 0) {
				    entity.setReinr(newReinr);
				}
				return Optional.ofNullable(entity);
				//return Optional.ofNullable(caReqDtlRepository.save(entity));
			} catch (Exception e) {
				entity.setReinr(newReinr);
				System.out.println("updateTripnumber Exception: " + e.getMessage());
				return Optional.ofNullable(entity);
			}				
		}
*/
		@Override
		public boolean deleteList(List<CaReqDtl> entities) {
			try {
			    caReqDtlRepository.delete(entities);
			} catch (Exception e) {
				return false;
			}
			return true;
		}
	}