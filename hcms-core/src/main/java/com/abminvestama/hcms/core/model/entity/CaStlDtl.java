package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.abminvestama.hcms.core.model.constant.CashAdvanceConstant;

/**
 * 
 * Class that represents <strong>Cash Advance Request Header</strong>
 * transaction.
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko@gmx.com) <br>
 *         <br>
 *         <table>
 *         <tr>
 *         <td><b>Version</td>
 *         <td><b>Author</td>
 *         <td><b>Description</td>
 *         </tr>
 *         <tr>
 *         <td>1.0.0</td>
 *         <td>Wijanarko</td>
 *         <td>Development release</td>
 *         </tr>
 *         </table>
 *
 */
@Entity
@Table(name = CashAdvanceConstant.TABEL_CASHADVANCE_SETTLEMENT_DETAIL)
public class CaStlDtl implements Serializable {

	private static final long serialVersionUID = -1372796417404610281L;

	@Id
	@Column(name = "id", nullable = false, insertable = false, updatable = false, length = 13)
	@GeneratedValue(
        strategy = GenerationType.SEQUENCE, 
        generator = "assigned-sequence"
    )
    @GenericGenerator(
        name = "assigned-sequence", 
        strategy = "com.abminvestama.hcms.core.model.entity.PostgreUUIDGenerationStrategy",
        parameters = @Parameter(
            name = "sequence_name",
            value = "hibernate_sequence"
        )
    )
	private String detailId;

	@Column(name = "req_no")
	private String reqNo;

	@Column(name = "stl_date")
	private Date stlDate;

	@Column(name = "description")
	private String description;
	
	@Column(name = "amount")
	private Double amount;
	
	@Column(name = "currency")
	private String currency;

	@ManyToOne
	@JoinColumn(name = "order_id", referencedColumnName = "id")
	private CaOrder orderId;
	
	@Column(name = "created_at")
	private Date createdAt;
	
	@ManyToOne
	@JoinColumn(name = "created_by", referencedColumnName = "id")
	private User createdBy;

	@Column(name = "updated_at")
	private Date updatedAt;

	@ManyToOne
	@JoinColumn(name = "updated_by", referencedColumnName = "id")
	private User updatedBy;

	@Column(name = "item_no")
	private Integer itemNo;
	
	@Column(name = "pool_budget")
	private Integer poolBudget;

	public String getDetailId() {
		return detailId;
	}

	public void setDetailId(String detailId) {
		this.detailId = detailId;
	}

	public String getReqNo() {
		return reqNo;
	}

	public void setReqNo(String reqNo) {
		this.reqNo = reqNo;
	}

	public Date getStlDate() {
		return stlDate;
	}

	public void setStlDate(Date stlDate) {
		this.stlDate = stlDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public CaOrder getOrderId() {
		return orderId;
	}

	public void setOrderId(CaOrder orderId) {
		this.orderId = orderId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getItemNo() {
		return itemNo;
	}

	public void setItemNo(Integer itemNo) {
		this.itemNo = itemNo;
	}

	public Integer getPoolBudget() {
		return poolBudget;
	}

	public void setPoolBudget(Integer poolBudget) {
		this.poolBudget = poolBudget;
	}

}
