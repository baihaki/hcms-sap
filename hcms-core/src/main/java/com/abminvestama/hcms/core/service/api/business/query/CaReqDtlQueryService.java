package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.abminvestama.hcms.core.model.entity.CaReqDtlKey;
import com.abminvestama.hcms.core.model.entity.CaReqDtl;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaReqDtlQueryService extends DatabaseQueryService<CaReqDtl, CaReqDtlKey>, DatabasePaginationQueryService{
	
	@NotNull
	Optional<CaReqDtl> findOneByCompositeKey(String reqNo, Short itemNo);

	Collection<CaReqDtl> findByReqNo(String reqNo);
}

