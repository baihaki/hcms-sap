package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.IT0184;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.repository.IT0184Repository;
import com.abminvestama.hcms.core.service.api.business.query.IT0184QueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.4
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add findByPernrAndSubtyAndStatus method, Set findByPernrAndSubtyForPublishedOrUpdated for PUBLISHED status</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Modify findByPernrAndStatus method: Set findByPernrForPublishedOrUpdated for PUBLISHED status</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add findByPernrAndStatus method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findByStatus method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("it0184QueryService")
@Transactional(readOnly = true)
public class IT0184QueryServiceImpl implements IT0184QueryService {

	private IT0184Repository it0184Repository;
	
	@Autowired
	IT0184QueryServiceImpl(IT0184Repository it0184Repository) {
		this.it0184Repository = it0184Repository;
	}
	
	@Override
	public Optional<IT0184> findById(Optional<ITCompositeKeys> id) throws Exception {
		throw new NoSuchMethodException("Method not implemented. Please use method findOneByCompositeKeys instead.");		
	}

	@Override
	public Optional<Collection<IT0184>> fetchAll() {
		Optional<Iterable<IT0184>> it0184Iterates = Optional.ofNullable(it0184Repository.findAll());
		if (!it0184Iterates.isPresent()) {
			return Optional.empty();
		}
		
		List<IT0184> listOfIT0184 = new ArrayList<>();
		it0184Iterates.map(it0184Iterable -> {			
			it0184Iterable.forEach(it0184 -> {
				listOfIT0184.add(it0184);
			});
			return listOfIT0184;
		});
		
		return Optional.of(listOfIT0184);
	}

	@Override
	public Optional<IT0184> findOneByCompositeKey(Long pernr, String subty, Date endda, Date begda) {
		if (pernr == null || StringUtils.isBlank(subty) || endda == null || begda == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(it0184Repository.findOneByCompositeKey(pernr, SAPInfoType.RESUME_TEXTS.infoType(), subty, endda, begda));
	}

	@Override
	public Collection<IT0184> findByPernr(Long pernr) {
		
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT0184>> bunchOfIT0184 
			= Optional.ofNullable(it0184Repository.findByPernr(pernr, SAPInfoType.RESUME_TEXTS.infoType()));
		
		return (bunchOfIT0184.isPresent()
				? bunchOfIT0184.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

	@Override
	public Collection<IT0184> findByPernrAndSubty(Long pernr, String subty) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		if (StringUtils.isBlank(subty)) {
			subty = "01"; // default to subtype '01' (i.e. Profile)
		}
		
		Optional<Collection<IT0184>> bunchOfIT0184 
			= Optional.ofNullable(it0184Repository.findByPernrAndSubty(pernr, SAPInfoType.RESUME_TEXTS.infoType(), subty));

		return (bunchOfIT0184.isPresent() 
					? bunchOfIT0184.get().stream().collect(Collectors.toList())
					: Collections.emptyList());		
	}

	@Override
	public Page<IT0184> findByStatus(String status, int pageNumber) {
		if (StringUtils.isEmpty(status)) {
			status = DocumentStatus.PUBLISHED.name();
		}
		
		Pageable pageRequest = createPageRequest(pageNumber);
		
		DocumentStatus documentStatus = DocumentStatus.valueOf(status);
		
		return it0184Repository.findByStatus(documentStatus, pageRequest);
	}

	@Override
	public Page<IT0184> findByPernrAndStatus(long pernr, String status, int pageNumber) {
		if (StringUtils.isEmpty(status)) {
			status = DocumentStatus.PUBLISHED.name();
		}
		
		Pageable pageRequest = createPageRequest(pageNumber);
		
		DocumentStatus documentStatus = DocumentStatus.valueOf(status);
		
		return (documentStatus == DocumentStatus.PUBLISHED) ? it0184Repository.findByPernrForPublishedOrUpdated(pernr, pageRequest)
				: it0184Repository.findByPernrAndStatus(pernr, documentStatus, pageRequest);
	}

	@Override
	public Collection<IT0184> findByPernrAndSubtyAndStatus(Long pernr, String subty, String status) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		if (StringUtils.isBlank(subty)) {
			subty = "01"; // default to subtype '01' (i.e. Profile)
		}
		
		DocumentStatus documentStatus = DocumentStatus.valueOf(status);
		
		Optional<Collection<IT0184>> bunchOfIT0184 
			= Optional.ofNullable(documentStatus == DocumentStatus.PUBLISHED ?
					it0184Repository.findByPernrAndSubtyForPublishedOrUpdated(pernr, SAPInfoType.RESUME_TEXTS.infoType(), subty)
					: it0184Repository.findByPernrAndSubtyAndStatus(pernr, SAPInfoType.RESUME_TEXTS.infoType(), subty, documentStatus));

		return (bunchOfIT0184.isPresent() 
					? bunchOfIT0184.get().stream().collect(Collectors.toList())
					: Collections.emptyList());		
	}

}