package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.IT0002;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;

public interface ZmedCostypeRepository extends CrudRepository<ZmedCostype, String> {

//	ZmedCostype findByKodemed(String kodemed);

	@Query("FROM ZmedCostype zmed_costype WHERE zmed_costype.bukrs.bukrs = :bukrs AND zmed_costype.kodemed = :kodemed AND zmed_costype.kodecos = :kodecos AND zmed_costype.id.datab = :datab AND zmed_costype.id.datbi = :datbi")
	ZmedCostype findOneByCompositeKey(@Param("bukrs") String bukrs, 
			@Param("kodemed") String kodemed,
			@Param("kodecos") String kodecos,
			@Param("datab") @Temporal(TemporalType.DATE) Date datab,
			@Param("datbi") @Temporal(TemporalType.DATE) Date datbi);
	
	@Query("FROM ZmedCostype zmed_costype WHERE zmed_costype.bukrs.bukrs = :bukrs AND zmed_costype.kodemed = :kodemed AND zmed_costype.id.datbi = :datbi")
	Collection<ZmedCostype> findByKodemed(@Param("bukrs") String bukrs, @Param("kodemed") String kodemed,
			@Param("datbi")  @Temporal(TemporalType.DATE) Date datbi);
	

	@Query("FROM ZmedCostype zmed_costype WHERE zmed_costype.bukrs.bukrs = :bukrs AND zmed_costype.id.datbi = :datbi")
	Collection<ZmedCostype> findByKodemed(@Param("bukrs") String bukrs,
			@Param("datbi")  @Temporal(TemporalType.DATE) Date datbi);
	

	//@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.id.endda = :endda AND it0002.id.begda = :begda")
	//IT0002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
	//		@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	
//	@Query("FROM IT0077 it0077 WHERE it0077.pernr = :pernr ORDER BY it0077.id.endda DESC")
//	Collection<IT0077> findByPernr(@Param("pernr") Long pernr);	
}
