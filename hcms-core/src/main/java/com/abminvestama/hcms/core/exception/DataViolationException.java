package com.abminvestama.hcms.core.exception;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class DataViolationException extends RuntimeException {

	private static final long serialVersionUID = -4149301241559927283L;

	public DataViolationException(final String message) {
		super(message);
	}
	
	public DataViolationException(final String message, Throwable throwable) {
		super(message, throwable);
	}
}