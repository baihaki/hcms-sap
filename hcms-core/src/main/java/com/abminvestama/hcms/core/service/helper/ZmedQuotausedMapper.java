package com.abminvestama.hcms.core.service.helper;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyQuotaused;
import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class ZmedQuotausedMapper {
	
	private ZmedQuotaused zmedQuotaused;
	
	private ZmedQuotausedMapper() {}
	
	public ZmedQuotausedMapper(Map<String, String> map) {
		if (map == null) {
			new ZmedQuotausedMapper();
			zmedQuotaused = new ZmedQuotaused();
		} else {
			zmedQuotaused = new ZmedQuotaused();
			ZmedCompositeKeyQuotaused id = new ZmedCompositeKeyQuotaused(
					map.get("gjahr") != null ? Integer.valueOf(map.get("gjahr")) : 0,
					StringUtils.defaultString(map.get("bukrs"), ""),
					map.get("pernr") != null ? Long.valueOf(map.get("pernr")) : null,
					StringUtils.defaultString(map.get("kodequo"), ""));
			zmedQuotaused.setId(id);
			zmedQuotaused.setQuotamt(map.get("quotamt") != null ? Double.valueOf(map.get("quotamt")) : 0.00);
		}		
	}

	public ZmedQuotaused getZmedQuotaused() {
		return zmedQuotaused;
	}
	
}
