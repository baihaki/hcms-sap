package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T500L;
import com.abminvestama.hcms.core.repository.T500LRepository;
import com.abminvestama.hcms.core.service.api.business.query.T500LQueryService;

@Service("t500lQueryService")
@Transactional(readOnly = true)
public class T500LQueryServiceImpl implements T500LQueryService {

	private T500LRepository t500lRepository;
	
	@Autowired
	T500LQueryServiceImpl(T500LRepository t500lRepository) {
		this.t500lRepository = t500lRepository;
	}
	
	@Override
	public Optional<T500L> findById(Optional<String> id) throws Exception {
		return id.map(pk -> t500lRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<T500L>> fetchAll() {
		List<T500L> listOfT500L = new ArrayList<>();
		Optional<Iterable<T500L>> iterT500L = Optional.ofNullable(t500lRepository.findAll());
		return iterT500L.map(iter -> {
			iter.forEach(t500l -> {
				listOfT500L.add(t500l);
			});
			return listOfT500L;
		});
	}

	@Override
	public Page<T500L> findAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return t500lRepository.findAll(pageRequest);
	}
}