package com.abminvestama.hcms.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.abminvestama.hcms.core.model.entity.T500L;

public interface T500LRepository extends CrudRepository<T500L, String> {
	
	Page<T500L> findAll(Pageable pageRequest);
}