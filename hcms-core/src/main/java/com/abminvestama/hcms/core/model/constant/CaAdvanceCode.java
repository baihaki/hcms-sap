package com.abminvestama.hcms.core.model.constant;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 3.0.0 (Cash Advance)
 * @since 3.0.0
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.1</td><td>Wijanarko</td><td>Add Description of CaAdvanceCode & Text of Code</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public enum CaAdvanceCode {

	PC(TextOfCode.CASH_ADVANCE_CODE_PC), // Petty Cash Advance Request 
	BT(TextOfCode.CASH_ADVANCE_CODE_BT), // Bank Transfer Advance Request  
	TA(TextOfCode.CASH_ADVANCE_CODE_TA), // Travel Arrangement Request
	RE(TextOfCode.CASH_ADVANCE_CODE_RE); // Reimbursement

	public String description;

	CaAdvanceCode(String description) {
		this.description = description;
	}

	public static class TextOfCode {
		public static final String CASH_ADVANCE_CODE_PC = "Petty Cash";
		public static final String CASH_ADVANCE_CODE_BT = "Bank Transfer";
		public static final String CASH_ADVANCE_CODE_TA = "Travel Arrangement";
		public static final String CASH_ADVANCE_CODE_RE = "Reimbursement";
	}
}