package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.CaGlAccount;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaGlAccountQueryService extends DatabaseQueryService<CaGlAccount, Long>, DatabasePaginationQueryService {

	@NotNull
	Page<CaGlAccount> fetchAllWithPaging(int pageNumber);
	
	@NotNull
	Page<CaGlAccount> fetchValidAllWithPaging(int pageNumber);
	
	@NotNull
	Collection<CaGlAccount> findByGroupAccountId(Long groupAccountId);
	
	@NotNull
	Collection<CaGlAccount> findValidByGroupAccountId(Long groupAccountId);
	
	@NotNull
	Page<CaGlAccount> findByGroupAccountId(Long groupAccountId, int pageNumber);
	
	@NotNull
	Page<CaGlAccount> findValidByGroupAccountId(Long groupAccountId, int pageNumber);
	
}