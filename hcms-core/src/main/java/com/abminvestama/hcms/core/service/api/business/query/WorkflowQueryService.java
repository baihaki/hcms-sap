package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Optional;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.Workflow;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface WorkflowQueryService extends DatabaseQueryService<Workflow, String>, DatabasePaginationQueryService {

	Optional<Workflow> findByName(String name);
	Page<Workflow> fetchAllWithPaging(int pageNumber);
}