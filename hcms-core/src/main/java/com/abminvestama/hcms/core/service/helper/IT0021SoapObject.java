package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0021;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0021SoapObject {
	
	private String employeenumber;
	private String validitybegin;
	private String validityend;
	private String subty;
    private String famsa;
	private String fgbdt;
	private String fgbld;
	private String fanat;
	private String fasex;
	private String favor;
	private String fanam;
	private String fgbot;
	private String fcnam;
	private String fknzn;
	private String fnmzu;
	private String title;
	
	private IT0021SoapObject() {}
	
	public IT0021SoapObject(IT0021 object) {
		if (object == null) {
			new IT0021SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			subty = object.getId().getSubty();
			famsa = object.getFamsa() != null ? object.getFamsa().getId().getSubty() : null;
			fgbdt = CommonDateFunction.convertDateToStringYMD(object.getFgbdt());
			fgbld = object.getFgbld().getLand1();
			fanat = object.getFanat().getLand1();
			fasex = object.getFasex();
			favor = object.getFavor();
			fanam = object.getFanam();
			fgbot = object.getFgbot();
			fcnam = object.getFcnam();
			fknzn = object.getFknzn().toString();
			if (object.getT535n() != null && object.getT535n().getId() != null) {
			    fnmzu = object.getT535n().getId().getArt();
			    title = object.getT535n().getId().getTitle();
			}
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}

	public String getSubty() {
		return subty;
	}

	public String getFamsa() {
		return famsa;
	}

	public String getFgbdt() {
		return fgbdt;
	}

	public String getFgbld() {
		return fgbld;
	}

	public String getFanat() {
		return fanat;
	}

	public String getFasex() {
		return fasex;
	}

	public String getFavor() {
		return favor;
	}

	public String getFanam() {
		return fanam;
	}

	public String getFgbot() {
		return fgbot;
	}

	public String getFcnam() {
		return fcnam;
	}

	public String getFknzn() {
		return fknzn;
	}

	public String getFnmzu() {
		return fnmzu;
	}

	public String getTitle() {
		return title;
	}

}
