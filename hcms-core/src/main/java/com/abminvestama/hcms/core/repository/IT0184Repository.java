package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0184;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;

/**
 * 
 * @since 1.0.0
 * @version 1.0.4
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: findByPernrAndSubtyAndStatus, findByPernrAndSubtyForPublishedOrUpdated</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: findByPernrForPublishedOrUpdated</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add findByPernrAndStatus method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findByStatus method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0184Repository extends CrudRepository<IT0184, ITCompositeKeys> {

	@Query("FROM IT0184 it0184 WHERE it0184.pernr = :pernr AND it0184.id.infty = :infty AND it0184.id.subty = :subty AND it0184.id.endda = :endda AND it0184.id.begda = :begda")
	IT0184 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty,
			@Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);		

	@Query("FROM IT0184 it0184 WHERE it0184.status = :status ORDER BY it0184.id.endda DESC, it0184.id.begda DESC")
	Page<IT0184> findByStatus(@Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0184 it0184 WHERE it0184.pernr = :pernr AND it0184.id.infty = :infty ORDER BY it0184.id.subty ASC, it0184.id.endda DESC, it0184.id.begda DESC")
	Collection<IT0184> findByPernr(@Param("pernr") Long pernr, @Param("infty") String infty);
	
	@Query("FROM IT0184 it0184 WHERE it0184.pernr = :pernr AND it0184.status = :status ORDER BY it0184.id.endda DESC, it0184.id.begda DESC")
	Page<IT0184> findByPernrAndStatus(@Param("pernr") Long pernr, @Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0184 it0184 WHERE it0184.pernr = :pernr AND " +
	"(it0184.status = 'PUBLISHED' OR it0184.status = 'UPDATE_IN_PROGRESS' OR it0184.status = 'UPDATE_REJECTED') " +
	" ORDER BY it0184.id.endda DESC, it0184.id.begda DESC")
	Page<IT0184> findByPernrForPublishedOrUpdated(@Param("pernr") Long pernr, Pageable pageRequest);
	
	@Query("FROM IT0184 it0184 WHERE it0184.pernr = :pernr AND it0184.id.infty = :infty AND it0184.id.subty = :subty ORDER BY it0184.id.endda DESC, it0184.id.begda DESC")
	Collection<IT0184> findByPernrAndSubty(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty);
	
	@Query("FROM IT0184 it0184 WHERE it0184.pernr = :pernr AND it0184.id.infty = :infty AND it0184.id.subty = :subty AND it0184.status = :status ORDER BY it0184.id.endda DESC, it0184.id.begda DESC")
	Collection<IT0184> findByPernrAndSubtyAndStatus(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty, @Param("status") DocumentStatus status);
	
	@Query("FROM IT0184 it0184 WHERE it0184.pernr = :pernr AND " +
			"(it0184.status = 'PUBLISHED' OR it0184.status = 'UPDATE_IN_PROGRESS' OR it0184.status = 'UPDATE_REJECTED') " +
			" AND it0184.id.infty = :infty AND it0184.id.subty = :subty ORDER BY it0184.id.endda DESC, it0184.id.begda DESC")
	Collection<IT0184> findByPernrAndSubtyForPublishedOrUpdated(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty);
	
}