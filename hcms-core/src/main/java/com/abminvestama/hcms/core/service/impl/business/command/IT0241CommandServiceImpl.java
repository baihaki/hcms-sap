package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.IT0241;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.IT0241Repository;
import com.abminvestama.hcms.core.service.api.business.command.IT0241CommandService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("it0241CommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class IT0241CommandServiceImpl implements IT0241CommandService {

	private IT0241Repository it0241Repository;
	
	@Autowired
	IT0241CommandServiceImpl(IT0241Repository it0241Repository) {
		this.it0241Repository = it0241Repository;
	}
	
	@Override
	public Optional<IT0241> save(IT0241 entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setUname(user.getId());
		}
		
		try {
			return Optional.ofNullable(it0241Repository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist IT0241 (Tax Data Indonesia).Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(IT0241 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete on this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(IT0241 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}
}