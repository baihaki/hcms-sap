package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0021;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;

/**
 * 
 * @since 1.0.0
 * @version 1.0.5
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add findMedPatientByPernr method</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: findByPernrForPublishedOrUpdated, findByPernrAndSubtyForPublishedOrUpdated</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add findBySubtyAndStatus method</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add findByPernrAndStatus method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findByStatus method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0021Repository extends CrudRepository<IT0021, ITCompositeKeys> {
	
	@Query("FROM IT0021 it0021 WHERE it0021.pernr = :pernr AND it0021.id.infty = '0021' AND it0021.id.subty = :subty AND it0021.id.endda = :endda AND it0021.id.begda = :begda")
	IT0021 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("subty") String subty,
			@Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM IT0021 it0021 WHERE it0021.pernr = :pernr AND it0021.id.infty = '0021' ORDER BY it0021.id.subty ASC, it0021.id.endda DESC, it0021.id.begda DESC")
	Collection<IT0021> findByPernr(@Param("pernr") Long pernr);
	
	@Query("FROM IT0021 it0021 WHERE it0021.pernr = :pernr AND it0021.id.infty = '0021' " +
	"AND it0021.id.endda='9999-12-31' AND it0021.id.subty in ('1','2') ORDER BY it0021.id.subty, it0021.id.begda")
	Collection<IT0021> findMedPatientByPernr(@Param("pernr") Long pernr);
	
	@Query("FROM IT0021 it0021 WHERE it0021.pernr = :pernr AND it0021.id.infty = '0021' AND it0021.id.subty = :subty ORDER BY it0021.id.endda DESC, it0021.id.begda DESC")
	Collection<IT0021> findByPernrAndSubty(@Param("pernr") Long pernr, @Param("subty") String subty);

	@Query("FROM IT0021 it0021 WHERE it0021.status = :status ORDER BY it0021.id.endda DESC, it0021.id.begda DESC")
	Page<IT0021> findByStatus(@Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0021 it0021 WHERE it0021.pernr = :pernr AND it0021.status = :status ORDER BY it0021.id.endda DESC, it0021.id.begda DESC")
	Page<IT0021> findByPernrAndStatus(@Param("pernr") Long pernr, @Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0021 it0021 WHERE it0021.pernr = :pernr AND " +
	"(it0021.status = 'PUBLISHED' OR it0021.status = 'UPDATE_IN_PROGRESS' OR it0021.status = 'UPDATE_REJECTED') " + 
	" ORDER BY it0021.id.endda DESC, it0021.id.begda DESC")
	Page<IT0021> findByPernrForPublishedOrUpdated(@Param("pernr") Long pernr, Pageable pageRequest);
	
	@Query("FROM IT0021 it0021 WHERE it0021.pernr = :pernr AND it0021.id.infty = '0021' AND it0021.id.subty = :subty AND it0021.status = :status ORDER BY it0021.id.endda DESC, it0021.id.begda DESC")
	Page<IT0021> findByPernrAndSubtyAndStatus(@Param("pernr") Long pernr, @Param("subty") String subty, @Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0021 it0021 WHERE it0021.pernr = :pernr AND it0021.id.infty = '0021' AND it0021.id.subty = :subty AND " +
	"(it0021.status = 'PUBLISHED' OR it0021.status = 'UPDATE_IN_PROGRESS' OR it0021.status = 'UPDATE_REJECTED') " + 
	" ORDER BY it0021.id.endda DESC, it0021.id.begda DESC")
	Page<IT0021> findByPernrAndSubtyForPublishedOrUpdated(@Param("pernr") Long pernr, @Param("subty") String subty, Pageable pageRequest);

	@Query("FROM IT0021 it0021 WHERE it0021.id.infty = '0021' AND it0021.id.subty = :subty AND it0021.status = :status ORDER BY it0021.id.endda DESC, it0021.id.begda DESC")
	Page<IT0021> findBySubtyAndStatus(@Param("subty") String subty, @Param("status") DocumentStatus status, Pageable pageRequest);
}