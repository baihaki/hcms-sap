package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T517T;
import com.abminvestama.hcms.core.model.entity.T517A;
import com.abminvestama.hcms.core.model.entity.T517AKey;
import com.abminvestama.hcms.core.repository.T517ARepository;
import com.abminvestama.hcms.core.service.api.business.query.T517AQueryService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("t517aQueryService")
@Transactional(readOnly = true)
public class T517AQueryServiceImpl implements T517AQueryService {

	private T517ARepository t517aRepository;
	
	@Autowired
	T517AQueryServiceImpl(T517ARepository t517aRepository) {
		this.t517aRepository = t517aRepository;
	}
	
	@Override
	public Optional<T517A> findById(Optional<T517AKey> id) throws Exception {
		return id.map(pk -> t517aRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<T517A>> fetchAll() {
		List<T517A> listOfT517A = new ArrayList<>();
		Optional<Iterable<T517A>> iterableT517A = Optional.ofNullable(t517aRepository.findAll());
		return iterableT517A.map(iter -> {
			iter.forEach(t517a -> {
				listOfT517A.add(t517a);
			});
			return listOfT517A;
		});
	}

	@Override
	public Page<T517A> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return t517aRepository.findAll(pageRequest);
	}

	@Override
	public Collection<T517A> findBySlart(T517T slart) {
		Optional<Collection<T517A>> bunchOfT517A = Optional.ofNullable(t517aRepository.findBySlart(slart));
		return (bunchOfT517A.isPresent() ?
				bunchOfT517A.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}
}