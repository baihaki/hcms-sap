package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.Position;
import com.abminvestama.hcms.core.repository.PositionRepository;
import com.abminvestama.hcms.core.service.api.business.query.PositionQueryService;

/**
 * 
 * @since 2.0.0
 * @version 2.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add findByCode method</td></tr>
 *     <tr><td>2.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("positionQueryService")
@Transactional(readOnly = true)
public class PositionQueryServiceImpl implements PositionQueryService {

	private PositionRepository positionRepository;
	
	@Autowired
	PositionQueryServiceImpl(PositionRepository positionRepository) {
		this.positionRepository = positionRepository;
	}
	
	@Override
	public Optional<Position> findById(Optional<String> id) throws Exception {
		return id.map(data -> Optional.ofNullable(positionRepository.findOne(data))).orElse(Optional.empty());
	}

	@Override
	public Optional<Collection<Position>> fetchAll() {
		List<Position> positionList = new ArrayList<>();
		Optional<Iterable<Position>> positionIterable = Optional.ofNullable(positionRepository.findAll());
		return positionIterable.map(iter -> {
			iter.forEach(position -> positionList.add(position));
			return positionList;
		});
	}

	@Override
	public Page<Position> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return positionRepository.findAll(pageRequest);				
	}
	
	@Override
	public Optional<Position> findByCode(Optional<String> code) throws Exception {
		return code.map(data -> Optional.ofNullable(positionRepository.findByCode(data))).orElse(Optional.empty());
	}
}