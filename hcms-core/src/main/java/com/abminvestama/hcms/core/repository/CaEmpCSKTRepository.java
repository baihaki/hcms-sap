package com.abminvestama.hcms.core.repository;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.CaEmpCSKT;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaEmpCSKTRepository extends CrudRepository<CaEmpCSKT, Long> {
	
	@Query("FROM CaEmpCSKT caEmpCSKT WHERE caEmpCSKT.cskt.kokrs = '1000' AND caEmpCSKT.pernr = :pernr")
	Collection<CaEmpCSKT> findCostCenterActiveByPernr(@Param("pernr") Long pernr);

	@Query("FROM CaEmpCSKT caEmpCSKT WHERE caEmpCSKT.cskt.kokrs = '1000' AND caEmpCSKT.cskt.kostl LIKE :bukrs")
	Collection<CaEmpCSKT> findAllCostCenterActiveByBukrs(@Param("bukrs") String bukrs);
	
	@Query("FROM CaEmpCSKT E WHERE E.cskt.kokrs = '1000' AND E.kostlPool = :kostlPool AND E.poolingCode = :poolingCode")
	Collection<CaEmpCSKT> findEmpCSKTByKostlPoolAndPoolingCode(@Param("kostlPool") String kostlPool, @Param("poolingCode") String poolingCode);

	@Query("FROM CaEmpCSKT E WHERE E.cskt.kokrs = '1000' AND E.kostlPool = :kostlPool AND E.cskt.kostl = :kostlDefault")
	Collection<CaEmpCSKT> findEmpCSKTByKostlPoolAndKostlDefault(@Param("kostlPool") String kostlPool, @Param("kostlDefault") String kostlDefault);
	
	Page<CaEmpCSKT> findAll(Pageable pageRequest);
}
