package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T554T;
import com.abminvestama.hcms.core.repository.T554TRepository;
import com.abminvestama.hcms.core.service.api.business.query.T554TQueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("t554tQueryService")
@Transactional(readOnly = true)
public class T554TQueryServiceImpl implements T554TQueryService {

	private T554TRepository t554tRepository;
	
	@Autowired
	T554TQueryServiceImpl(T554TRepository t554tRepository) {
		this.t554tRepository = t554tRepository;
	}
	
	@Override
	public Optional<T554T> findById(Optional<String> id) throws Exception {
		return id.map(pk -> t554tRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<T554T>> fetchAll() {
		Optional<Iterable<T554T>> t554tIterable = Optional.ofNullable(t554tRepository.findAll());
		if (!t554tIterable.isPresent()) {
			return Optional.empty();
		}
		
		List<T554T> listOfT554T = new ArrayList<>();
		return t554tIterable.map(iteratesT554T -> {
			iteratesT554T.forEach(t554t -> {
				listOfT554T.add(t554t);
			});
			
			return listOfT554T;
		});				
	}
}