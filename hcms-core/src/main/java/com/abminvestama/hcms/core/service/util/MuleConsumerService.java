package com.abminvestama.hcms.core.service.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.entity.User;

/**
 * Service Utility for making connection to MuleSoft WebService (for SAP Integration)
 * 
 * @since 2.0.0
 * @version 2.0.5
 * @author baihaki.pru@gmail.com
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.5</td><td>Baihaki</td><td>Set recordnumber returned from BAPI to seqnr on postToSAP (MasterData Create/Update)</td></tr>
 *     <tr><td>2.0.4</td><td>Baihaki</td><td>Add muleConnectionTimeout and muleReadTimeout on postToSAP, postToSAPAsync, getFromSAP</td></tr>
 *     <tr><td>2.0.3</td><td>Baihaki</td><td>Add method: postToSAPAsync to run MuleConsumerThread for A-synchronize Post to SAP</td></tr>
 *     <tr><td>2.0.2</td><td>Baihaki</td><td>Rename method: postToSAP to postToSAPSync for Synchronize Post to SAP</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add method: getFromSAP, Add fields: muleServiceHost, muleServicePort</td></tr>
 *     <tr><td>2.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("muleConsumerService")
public class MuleConsumerService {
	
	static Logger logger = LoggerFactory.getLogger(MuleConsumerService.class);
    
    @Autowired
    private SOAPConnectionFactory soapConnectionFactory;
    
    @Autowired
    String muleServiceHost;

    @Autowired
    Integer muleServicePort;
    
    @Autowired
    Integer muleConnectionTimeout;
    
    @Autowired
    Integer muleReadTimeout;
    
    class MuleConsumerThread implements Runnable {
    	
    	String muleService;
    	String uri;
    	Object soapObject;
    	Object commandService;
    	Object entity;
    	
    	MuleConsumerThread(String muleService, String uri, Object soapObject, Object commandService, Object entity) {
    		this.muleService = muleService;
    		this.uri = uri;
    		this.soapObject = soapObject;
    		this.commandService = commandService;
    		this.entity = entity;
    	}
    	
		@Override
		public void run() {
			try {
				Thread.sleep(3000);
				Map<String, String> resultMap = postToSAP(muleService, uri, soapObject);
				String message = resultMap.get("errorMessage");
				if (StringUtils.isEmpty(message)) {
					message = resultMap.get("message");
				}
				Method setSapMessage = entity.getClass().getMethod("setSapMessage", String.class);
				setSapMessage.invoke(entity, message);
				Method setSapStatus = entity.getClass().getMethod("setSapStatus", String.class);
				setSapStatus.invoke(entity, resultMap.get("messageType"));
				// set recordnumber, if exist
				if (resultMap.get("recordnumber") != null) {
					Method setRecordNumber = entity.getClass().getMethod("setSeqnr", Long.class);
					long recordNumber = 0;
					try {
					    recordNumber = Long.parseLong(resultMap.get("recordnumber"));
					} catch (Exception e) {
					}
					setRecordNumber.invoke(entity, recordNumber);
				}
				Method save = commandService.getClass().getMethod(
						"save", entity.getClass(), User.class);
				save.invoke(commandService, entity, null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
    }
    
	public void postToSAPAsync(String muleService, String uri, Object soapObject, Object commandService, Object entity) {
		new Thread(new MuleConsumerThread(muleService, uri, soapObject, commandService, entity)).start();
		
	}
    
	private Map<String, String> postToSAP(String muleService, String uri, Object soapObject) {
		
		Map<String, String> resultMap = new HashMap<String, String>();
		
		try {
			// Create SOAP Connection
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			
			// Send SOAP Message to SOAP Server
			//String url = "http://10.30.109.36:8100/".concat(uri);
			URL url = new URL("http", muleServiceHost, muleServicePort.intValue(), "/" + uri);
			URL timeoutUrl = new URL(url, "/" + uri, new URLStreamHandler() {
	            @Override
	            protected URLConnection openConnection(URL url) throws IOException {
	              URL target = new URL(url.toString());
	              URLConnection connection = target.openConnection();
	              // Connection settings
	              connection.setConnectTimeout(muleConnectionTimeout);
	              connection.setReadTimeout(muleReadTimeout);
	              return(connection);
	            }
	        });
			
			//SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(muleService, uri, soapObject), url);
			SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(muleService, uri, soapObject), timeoutUrl);
			
			// Process the SOAP Response
			//Source source = printSOAPResponse(soapResponse);
			printSOAPResponse(soapResponse);
			
			String message = "";
			String statusText = "";
			String errorCode = "";
			String errorMessage = "";
			
			SOAPBody body = soapResponse.getSOAPPart().getEnvelope().getBody();
			if (body.getFirstChild().getLocalName().equalsIgnoreCase("fault")) {
				statusText = "FAULT";
				resultMap.put("messageType", "F");
				errorCode = body.getFirstChild().getFirstChild().getTextContent();
				errorMessage = body.getFirstChild().getFirstChild().getNextSibling().getTextContent();
			} else {
			    NodeList nodes = body.getFirstChild().getFirstChild().getChildNodes();
			    Map<String, Node> nodeMap = new HashMap<String, Node>();
			    for (int i=0; i < nodes.getLength(); i ++) {
				    nodeMap.put(nodes.item(i).getLocalName(), nodes.item(i));
			    }
			    if (nodeMap.get("type") != null && nodeMap.get("type").getTextContent() != null && nodeMap.get("type").getTextContent().equalsIgnoreCase("E")) {
			    	resultMap.put("messageType", "E");
			    	if (nodeMap.get("message") != null)
			    		errorMessage = nodeMap.get("message").getTextContent();
			    } else {
			    	resultMap.put("messageType", "S");
			    	if (nodeMap.get("message") != null)
			    		message = nodeMap.get("message").getTextContent();
			    }			    	
			    if (nodeMap.get("statustext") != null)
			    	statusText = nodeMap.get("statustext").getTextContent();
			    // if status = 0
			    if (nodeMap.get("status") != null && nodeMap.get("status").getTextContent().equals("0")) {
			    	resultMap.put("messageType", "L");
			    	errorMessage = statusText;
			    }
			    // set recordnumber, if exists
			    if ( nodeMap.get("recordnumber") != null && !StringUtils.isEmpty(nodeMap.get("recordnumber").getTextContent()) ) {
			    	resultMap.put("recordnumber", nodeMap.get("recordnumber").getTextContent());
			    }
			}
			
			resultMap.put("message", message);
			resultMap.put("statusText", statusText);
			resultMap.put("errorCode", errorCode);
			resultMap.put("errorMessage", errorMessage);
			
		} catch (Exception e) {
	    	resultMap.put("messageType", "F");
			resultMap.put("errorMessage", e.getMessage());
			//e.printStackTrace();
			//throw e;
		}
		
		return resultMap;
	}
    
	public Map<String, String> postToSAPSync(String muleService, String uri, Object soapObject) {
		
		Map<String, String> resultMap = new HashMap<String, String>();
		
		try {
			// Create SOAP Connection
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			
			// Send SOAP Message to SOAP Server
			//String url = "http://10.30.109.36:8100/".concat(uri);
			URL url = new URL("http", muleServiceHost, muleServicePort.intValue(), "/" + uri);
			URL timeoutUrl = new URL(url, "/" + uri, new URLStreamHandler() {
	            @Override
	            protected URLConnection openConnection(URL url) throws IOException {
	              URL target = new URL(url.toString());
	              URLConnection connection = target.openConnection();
	              // Connection settings
	              connection.setConnectTimeout(muleConnectionTimeout);
	              connection.setReadTimeout(muleReadTimeout);
	              return(connection);
	            }
	        });
			
			//SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(muleService, uri, soapObject), url);
			SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(muleService, uri, soapObject), timeoutUrl);
			
			// Process the SOAP Response
			//Source source = printSOAPResponse(soapResponse);
			printSOAPResponse(soapResponse);
			
			String message = "";
			String statusText = "";
			String errorCode = "";
			String errorMessage = "";
			
			SOAPBody body = soapResponse.getSOAPPart().getEnvelope().getBody();
			if (body.getFirstChild().getLocalName().equalsIgnoreCase("fault")) {
				statusText = "FAULT";
				errorCode = body.getFirstChild().getFirstChild().getTextContent();
				errorMessage = body.getFirstChild().getFirstChild().getNextSibling().getTextContent();
			} else {
			    NodeList nodes = body.getFirstChild().getFirstChild().getChildNodes();
			    Map<String, Node> nodeMap = new HashMap<String, Node>();
			    String firstMsg = null;
			    for (int i=0; i < nodes.getLength(); i ++) {
				    nodeMap.put(nodes.item(i).getLocalName(), nodes.item(i));
				    // handle returned message in multiple rows, only get the first row's message
				    if (nodes.item(i).getChildNodes().getLength() > 1 && firstMsg == null) {
				    	firstMsg = nodes.item(i).getLocalName();
				    	NodeList msgNodes = nodes.item(i).getChildNodes();
				    	for (int j=0; j < msgNodes.getLength(); j++) {
				    		resultMap.put(msgNodes.item(j).getLocalName(), msgNodes.item(j).getTextContent());
				    	}
				    }
			    }
			    if (nodeMap.get("type") != null && nodeMap.get("type").getTextContent() != null && nodeMap.get("type").getTextContent().equalsIgnoreCase("E")) {
			    	if (nodeMap.get("message") != null)
			    		errorMessage = nodeMap.get("message").getTextContent();
			    } else {
			    	if (nodeMap.get("message") != null)
			    		message = nodeMap.get("message").getTextContent();
			    	if (nodeMap.get("belnr") != null)
			    		statusText = nodeMap.get("belnr").getTextContent();
			    }			    	
			    if (nodeMap.get("statustext") != null)
			    	statusText = nodeMap.get("statustext").getTextContent();
			}
			
			resultMap.put("message", message);
			resultMap.put("statusText", statusText);
			resultMap.put("errorCode", errorCode);
			resultMap.put("errorMessage", errorMessage);
			resultMap.put("belnr", statusText);
			
			
		} catch (Exception e) {
			resultMap.put("errorMessage", e.getMessage());
			e.printStackTrace();
			//throw e;
		}
		
		return resultMap;
	}
    
	public List<Map<String, String>> getFromSAP(String muleService, String uri, Object soapObject) {		
		Map<String, String> resultMap = new HashMap<String, String>();
		List<Map<String, String>> resultList =  new ArrayList<Map<String, String>>();
		
		try {
			// Create SOAP Connection
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			//String url = "http://10.30.109.36:8100/".concat(uri);
			URL url = new URL("http", muleServiceHost, muleServicePort.intValue(), "/" + uri);
			URL timeoutUrl = new URL(url, "/" + uri, new URLStreamHandler() {
	            @Override
	            protected URLConnection openConnection(URL url) throws IOException {
	              URL target = new URL(url.toString());
	              URLConnection connection = target.openConnection();
	              // Connection settings
	              connection.setConnectTimeout(muleConnectionTimeout);
	              connection.setReadTimeout(muleReadTimeout);
	              return(connection);
	            }
	        });
			
			//SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(muleService, uri, soapObject), url);
			SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(muleService, uri, soapObject), timeoutUrl);
			
			// Process the SOAP Response
			//Source source = printSOAPResponse(soapResponse);
			printSOAPResponse(soapResponse);
			
			String message = "";
			String statusText = "";
			String errorCode = "";
			String errorMessage = "";
		    int rowCount = 0;
			
			SOAPBody body = soapResponse.getSOAPPart().getEnvelope().getBody();
			if (body.getFirstChild().getLocalName().equalsIgnoreCase("fault")) {
				statusText = "FAULT";
				errorCode = body.getFirstChild().getFirstChild().getTextContent();
				errorMessage = body.getFirstChild().getFirstChild().getNextSibling().getTextContent();
			} else {
				// get header nodes from BAPI result (message, statustext, etc)
				NodeList nodes = body.getFirstChild().getFirstChild().getChildNodes();
			    Map<String, Node> nodeMap = new HashMap<String, Node>();
			    for (int i=0; i < nodes.getLength(); i ++) {
				    nodeMap.put(nodes.item(i).getLocalName(), nodes.item(i));
				    resultMap.put(nodes.item(i).getLocalName(), nodes.item(i).getTextContent());
			    }
			    // get row nodes from BAPI result (if there is at least one row returned)
			    if (body.getFirstChild().getFirstChild().getFirstChild() != null) {
			        nodes = body.getFirstChild().getFirstChild().getFirstChild().getChildNodes();
			        for (int i=0; i < nodes.getLength(); i ++) {
				        nodeMap.put(nodes.item(i).getLocalName(), nodes.item(i));
				        resultMap.put(nodes.item(i).getLocalName(), nodes.item(i).getTextContent());
			        }
			        rowCount++;
			    }
			    if (nodeMap.get("type") != null && nodeMap.get("type").getTextContent() != null && nodeMap.get("type").getTextContent().equalsIgnoreCase("E")) {
			    	if (nodeMap.get("message") != null)
			    		errorMessage = nodeMap.get("message").getTextContent();
			    } else {
			    	if (nodeMap.get("message") != null)
			    		message = nodeMap.get("message").getTextContent();
			    }			    	
			    if (nodeMap.get("statustext") != null)
			    	statusText = nodeMap.get("statustext").getTextContent();
			}
			
			resultMap.put("message", message);
			resultMap.put("statusText", statusText);
			resultMap.put("errorCode", errorCode);
			resultMap.put("errorMessage", errorMessage);
	    	resultMap.put("rowCount", String.valueOf(rowCount));
			
			resultList.add(resultMap);
			
			// Only for multiple rows returned from SAP
			Node node = body.getFirstChild().getFirstChild().getFirstChild();			
			while (node != null && node.getNextSibling() != null) {
				node = node.getNextSibling();
			    NodeList nodes = node.getChildNodes();
				Map<String, String> nextResultMap = new HashMap<String, String>();
			    for (int i=0; i < nodes.getLength(); i ++) {
				    nextResultMap.put(nodes.item(i).getLocalName(), nodes.item(i).getTextContent());
			    }
			    resultList.add(nextResultMap);
			}
			if (resultList.size() > 1) {
				resultList.get(0).put("rowCount", String.valueOf(resultList.size()));
			}
			
		} catch (Exception e) {
			resultMap.put("errorMessage", e.getMessage());
	    	resultMap.put("rowCount", "0");
	    	resultList.add(resultMap);
			e.printStackTrace();
			//throw e;
		}
		
		return resultList;
	}

	
	private SOAPMessage createSOAPRequest(String muleService, String uri, Object soapObject) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		
		String namespace = uri;
		if (BAPIFunction.IT0001_FETCH_URI.service().equals(uri)) {
			namespace = BAPIFunction.MASTERDATA_FETCH_URI.service();
		} else if (BAPIFunction.ZMEDDOCTRXHDR_FETCH_ABM_URI.service().equals(uri)) {
			namespace = BAPIFunction.ZMEDDOCTRXHDR_FETCH_URI.service();
		} else if (BAPIFunction.IT0105_CHANGE_URI.service().equals(uri)) {
			namespace = BAPIFunction.IT0105_CREATE_URI.service();
		} else if (BAPIFunction.TIME_ABSATD_DELETE_URI.service().equals(uri)) {
			namespace = BAPIFunction.TIME_ABSATD_CREATE_URI.service();
		} else if (uri.contains(BAPIFunction.INFOTYPE_FETCH_NAMESPACE.service())) {
			namespace = BAPIFunction.INFOTYPE_FETCH_NAMESPACE.service();
		}
		String serverURI = "http://".concat(namespace).concat(".hcms.com/");
		String prefix = namespace.substring(0, 3);
		
		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(prefix, serverURI);
		
		JSONObject json = new JSONObject(soapObject);
		
		// SOAP Body for main bank
		SOAPBody soapBody = envelope.getBody();
		//SOAPElement soapBodyElem = soapBody.addChildElement("BAPI_BANKDETAIL_CREATE", prefix);
		SOAPElement soapBodyElem = soapBody.addChildElement(muleService, prefix);
		SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("import");
		
		Iterator<String> jsonKeys = json.keys();
		while (jsonKeys.hasNext()) {
			String key = jsonKeys.next();
			//if (key.equals("trxItems")) {
			if (json.get(key) instanceof JSONArray) {
				//SOAPElement soapBodyElem2 = soapBodyElem1.addChildElement(key);
				JSONArray trxItems = json.getJSONArray(key);
				for (int i=0; i < trxItems.length(); i ++) {
					SOAPElement soapBodyElement = soapBodyElem1.addChildElement(key);
					JSONObject trxItem = trxItems.getJSONObject(i);
					Iterator<String> keys = trxItem.keys();
					while (keys.hasNext()) {
						String key1 = keys.next();
						//Name name = envelope.createName(key1);
					    //soapBodyElement.addAttribute(name, trxItem.getString(key1));
					    soapBodyElement.addChildElement(key1).addTextNode(trxItem.getString(key1));
					}
				}
			} else {
			    soapBodyElem1.addChildElement(key).addTextNode(json.getString(key));
			}
		}
		
		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", "");
		soapMessage.saveChanges();
		
		// Print the request message
		//System.out.println("Request SOAP Messages = ");
		//soapMessage.writeTo(System.out);
		//System.out.println();
		
		logger.info("Request SOAP Message = ");
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		soapMessage.writeTo(stream);
		logger.info(stream.toString());
		
		return soapMessage;
	}
	
	private Source printSOAPResponse(SOAPMessage soapResponse) throws Exception {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Source sourceContent = soapResponse.getSOAPPart().getContent();
		//System.out.print("\nResponse SOAP Message = ");
		//StreamResult result = new StreamResult(System.out);
		logger.info("Response SOAP Message = ");
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		StreamResult result = new StreamResult(stream);
		transformer.transform(sourceContent, result);
		logger.info(stream.toString());
		return sourceContent;
	}
}