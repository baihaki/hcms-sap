package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.abminvestama.hcms.core.model.constant.ApprovalAction;

/**
 * 
 * @since 2.0.0
 * @version 2.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.2</td><td>Baihaki</td><td>Add field: eventLog, submitUser to save/get submit details on approval email notification</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add field: mailNotification, for send notification email / not</td></tr>
 *     <tr><td>2.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Entity
@Table(name = "trx_approval_event_log")
public class ApprovalEventLog extends EventAbstractEntity {

	private static final long serialVersionUID = -6151335391192804267L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cfg_entity_id", referencedColumnName = "id", nullable = false)
	private HCMSEntity hcmsEntity;
	
	@Column(name = "entity_object_id", nullable = false)
	private String entityObjectId;
	
	@Column(name = "performed_by_ssn", nullable = false)
	private long performedBySSN;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "performed_by_position_id", referencedColumnName = "id", nullable = false)
	private Position performedByPosition;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "approval_action", nullable = false)
	private ApprovalAction approvalAction;
	
	@Transient
	private boolean mailNotification;
	
	@Transient
	private EventLog eventLog;
	
	@Transient
	private User submitUser;
	
	protected ApprovalEventLog() {}
	
	public ApprovalEventLog(HCMSEntity hcmsEntity, String entityObjectId, long performedBySSN, 
			Position performedByPosition, ApprovalAction approvalAction) {
		this.hcmsEntity = hcmsEntity;
		this.entityObjectId = entityObjectId;
		this.performedBySSN = performedBySSN;
		this.performedByPosition = performedByPosition;
		this.approvalAction = approvalAction;
		this.mailNotification = true;
	}

	public HCMSEntity getHcmsEntity() {
		return hcmsEntity;
	}
	
	public String getEntityObjectId() {
		return entityObjectId;
	}
	
	public long getPerformedBySSN() {
		return performedBySSN;
	}
	
	public Position getPerformedByPosition() {
		return performedByPosition;
	}
	
	public ApprovalAction getApprovalAction() {
		return approvalAction;
	}

	public boolean isMailNotification() {
		return mailNotification;
	}

	public void setMailNotification(boolean mailNotification) {
		this.mailNotification = mailNotification;
	}

	public EventLog getEventLog() {
		return eventLog;
	}

	public void setEventLog(EventLog eventLog) {
		this.eventLog = eventLog;
	}

	public User getSubmitUser() {
		return submitUser;
	}

	public void setSubmitUser(User submitUser) {
		this.submitUser = submitUser;
	}
}