package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Optional;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.CaOrder;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaOrderQueryService extends DatabaseQueryService<CaOrder, Long>, DatabasePaginationQueryService {
	
	Optional<CaOrder> findByOrderId(Long orderId);
	
	Page<CaOrder> findByOrgeh(Long orgeh, int pageNumber);
	
	Page<CaOrder> findByCriteria(Long orgeh, Long glAccountId, int pageNumber);

	Page<CaOrder> fetchAllWithPaging(int pageNumber);
}
