package com.abminvestama.hcms.core.service.impl.business.query;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.constant.TimeManagementConstants;
import com.abminvestama.hcms.core.model.entity.TEVEN;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.T705P;
import com.abminvestama.hcms.core.model.entity.TEVENKey;
import com.abminvestama.hcms.core.model.entity.Tevenvw;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.TEVENRepository;
import com.abminvestama.hcms.core.service.api.business.query.TEVENQueryService;


@Service("tevenQueryService")
@Transactional(readOnly = true)
public class TEVENQueryServiceImpl implements TEVENQueryService {

	private TEVENRepository tevenRepository;
	
	@PersistenceContext(type = PersistenceContextType.TRANSACTION)	
	private EntityManager em;
	
	@Autowired
	TEVENQueryServiceImpl(TEVENRepository tevenRepository) {
		this.tevenRepository = tevenRepository;
	}
	
	@Override
	public Optional<Collection<TEVEN>> fetchAll() {
		Optional<Iterable<TEVEN>> tevenIterates = Optional.ofNullable(tevenRepository.findAll());
		if (!tevenIterates.isPresent()) {
			return Optional.empty();
		}
		
		List<TEVEN> listOfTEVEN = new ArrayList<>();
		tevenIterates.map(tevenIterable -> {			
			tevenIterable.forEach(teven -> {
				listOfTEVEN.add(teven);
			});
			return listOfTEVEN;
		});
		
		return Optional.of(listOfTEVEN);
	}

	@Override
	public Optional<TEVEN> findOneByCompositeKey(Long pdsnr) {
		if (pdsnr == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(tevenRepository.findOneByCompositeKey(pdsnr));
	}

	@Override
	public Optional<TEVEN> findOneByCompositeKeys(Long pernr, Date ldate, T705P satza) {
		return Optional.ofNullable(tevenRepository.findOneByCompositeKeys(pernr, ldate, satza.getSatza()));
	}	

	@Override
	public Optional<TEVEN> findById(Optional<TEVENKey> id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<TEVEN> findByPernr(Long pernr, int pageNumber) {
		if (pernr == null) {
			return null;
		}		
		Pageable pageRequest = createPageRequest(pageNumber);
		//DocumentStatus documentStatus = DocumentStatus.valueOf(status);
		
		return tevenRepository.findByPernr(pernr, pageRequest);
	}

	@Override
	public Page<TEVEN> findByPernr(Long pernr, Date startDate, Date endDate,
			int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		//DocumentStatus documentStatus = DocumentStatus.valueOf(status);
		return tevenRepository.findByPernr(pernr, startDate, endDate, pageRequest);
	}

	/**
	 * get All Employee(List TevenView)
	 * 	1)	"RoleName":
	 * 		a. ROLE_ADMIN_WITH_HR_ADMIN, get All Employee by CompanyCode
	 * 		b. ROLE_ADMIN_WITHOUT_HR_ADMIN, get All Employee by CompanyCode(Without PERNR - HR ADMIN)
	 * 		c. ROLE_SUPERIOR_WITH_SUPERIOR, get All Employee by Superior
	 * 		d. ROLE_SUPERIOR_WITHOUT_SUPERIOR, get All Employee by Superior(Without PERNR - SUPERIOR)
	 * 		e. (ROLE_USER, ROLE_ADMIN, ROLE_SUPERIOR), get Employee by SSN
	 * 	2)	"BegLDate", Begin Logic Date
	 * 	3)	"EndLDate", End Logic Date
	 * 	4)	"Process", 0: Create / Editable State; 1: Submitted; 2: Approved 1,...etc
	 * 	5)	"TrxCode(Transaction Code)", 	
	 * 			AC: Auto-Create; AE:Auto-Editable; AS1: Auto-Submitted/Waiting Superior; AS2: Auto-Waiting HRAdmin; AF: Auto-Finished/Released
	 * 			MC: Manual-Create; ME: Manual-Editable; MS1: Manual-Submitted/Waiting Superior; MS2: Auto-Waiting HRAdmin; MF: Manual-Finished/Released
	 *  6)	"showLate", default "FALSE"
	 * 	7)	"OrderBy", default "LDATE DESC, PERNR".
	 * 	8)	"PageNumber", PageNumber to show by LIMIT(default 10) - OFFSET
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Page<Tevenvw> fetchAllEmpByCriteria(
			String roleName,
			User currentUser, 
			User ssn,
			Date begldate, 
			Date endldate, 
			String process, 
			String trxCode,
			String showLate,
			String orderBy,
			int pageNumber) {
		
		DateFormat formatDate = new SimpleDateFormat(TimeManagementConstants.FORMAT_YYYY_MM_DD);
		Pageable pageRequest = null;
		if (pageNumber != -1L) {
			pageRequest = createPageRequest(pageNumber);
		}
		
		String sql = "";
		String sqlCOUNT = "SELECT COUNT(*) FROM tevenvw ";
		String sqlSELECT = "SELECT * FROM tevenvw ";
		String sqlWHERE = "";
		String sqlOrderBy = orderBy.isEmpty() ? "" : "ORDER BY " + orderBy + " ";
		String sqlLIMIT = "";
		
		//HR ADMIN
		if (roleName.contains(TimeManagementConstants.ROLE_ADMIN)) {
			if (roleName.equals(TimeManagementConstants.ROLE_ADMIN_WITH_HR_ADMIN)) {
				//get All Employee(With HR ADMIN) by company code HR ADMIN
				sqlWHERE = "pernr IN (SELECT pernr FROM it0001 WHERE bukrs = '" + currentUser.getEmployee().getT500p().getBukrs().getBukrs() + "') ";
			}
			else if (roleName.equals(TimeManagementConstants.ROLE_ADMIN_WITHOUT_HR_ADMIN)) {
				//get All Employee(Without HR ADMIN) by company code HR ADMIN
				sqlWHERE = "pernr IN (SELECT pernr FROM it0001 WHERE bukrs = '" + currentUser.getEmployee().getT500p().getBukrs().getBukrs() + "')";
				sqlWHERE = "(" + sqlWHERE + " AND pernr <> " + currentUser.getEmployee().getPernr() + ") ";
			}
			else {
				//get single employee
				sqlWHERE = "pernr = " + ssn.getEmployee().getPernr() + " ";
			}
		}
		else if(roleName.contains(TimeManagementConstants.ROLE_SUPERIOR)) {
			if(roleName.equals(TimeManagementConstants.ROLE_SUPERIOR_WITH_SUPERIOR)) {
				//get All Employee(With Superior) by Position Relation Superior
				sqlWHERE = "pernr IN (SELECT pernr "
										+ "FROM it0001 "
										+ "WHERE m_position_id IN (SELECT from_position_id "
										+ "FROM position_relations "
										+ "WHERE to_position_id = '" + currentUser.getEmployee().getPosition().getId() + "')"
										+ ") ";
				sqlWHERE = "(" + sqlWHERE + " OR pernr = " + currentUser.getEmployee().getPernr() + ") ";
			}
			else if(roleName.equals(TimeManagementConstants.ROLE_SUPERIOR_WITHOUT_SUPERIOR)) {
				//get All Employee(Without Superior) by Position Relation Superior
				sqlWHERE = "pernr IN (SELECT pernr "
										+ "FROM it0001 "
										+ "WHERE m_position_id IN (SELECT from_position_id "
										+ "FROM position_relations "
										+ "WHERE to_position_id = '" + currentUser.getEmployee().getPosition().getId() + "')"
										+ ") ";
				sqlWHERE = "(" + sqlWHERE + " AND pernr <> " + currentUser.getEmployee().getPernr() + ") ";
			}
			//get single employee
			else {
				sqlWHERE += "pernr = " + ssn.getEmployee().getPernr() + " ";
			}
		}
		else if(roleName.equals(TimeManagementConstants.ROLE_USER)) {
			sqlWHERE += "pernr = " + currentUser.getEmployee().getPernr() + " ";
		}
		
		if(begldate != null && endldate != null) {
			sqlWHERE += !sqlWHERE.isEmpty() ? "AND " : sqlWHERE;
			sqlWHERE += "ldate BETWEEN '" + formatDate.format(begldate) + "' AND '" + formatDate.format(endldate) + "' ";
		}
		if(process != null && !process.isEmpty()) {
			sqlWHERE += !sqlWHERE.isEmpty() ? "AND " : sqlWHERE;
			sqlWHERE += "(begprocess = '" + process + "' OR endprocess = '" + process +"') ";
		}
		if(trxCode != null && !trxCode.isEmpty()) {
			sqlWHERE += !sqlWHERE.isEmpty() ? "AND " : sqlWHERE;
			sqlWHERE += "(begtrxcode LIKE '%" + trxCode + "%' OR endtrxcode LIKE '%" + trxCode +"%') ";
		}
		
		// Show DELAY / LATE Items
		if(showLate.equals("1")) {
			sqlWHERE += !sqlWHERE.isEmpty() ? "AND " : sqlWHERE;
			sqlWHERE += "(";
			sqlWHERE += 	"begtrxcode LIKE '%F2' "; 		// REJECT - SPV
			sqlWHERE += 	"OR begtrxcode LIKE '%F4' "; 	// REJECT - HR ADMIN
			// NOT APPROVE
			sqlWHERE += 	"OR begtrxcode LIKE '%S%' "; 	// WAITING APPROVE
			sqlWHERE += 	"OR begtrxcode LIKE '%E%' "; 	// EDITABLE
			sqlWHERE += 	"OR begtrxcode LIKE 'MC' "; 	// MANUAL CREATE
			sqlWHERE += 	"OR (begtrxcode IN ('AC') AND wdhour > 0) "; 	// AUTO - CREATE (LATE)
			sqlWHERE += ")";
		}
		
		if (pageNumber != -1L)
			sqlLIMIT = "LIMIT " + pageRequest.getPageSize() + " OFFSET " + pageRequest.getOffset();
		
		
		//GET COUNT
		sql = sqlCOUNT + (sqlWHERE.isEmpty() ? sqlWHERE : "WHERE " + sqlWHERE);
		BigInteger total = (BigInteger) em.createNativeQuery(sql).getSingleResult();
		
		//GET ITEMS
		sql = sqlSELECT + (sqlWHERE.isEmpty() ? sqlWHERE : "WHERE " + sqlWHERE) + sqlOrderBy + sqlLIMIT;
		List<Tevenvw> resultSet = em.createNativeQuery(sql, Tevenvw.class).getResultList();
		
		Page<Tevenvw> page = null;
		if (pageNumber != -1L && total.intValue() > 0)
			page = new PageImpl<Tevenvw>(resultSet, pageRequest, total.longValue());
		else if (total.intValue() > 0)
			page = new PageImpl<Tevenvw>(resultSet, new PageRequest(0, total.intValue()), total.longValue());
		else
			page = new PageImpl<Tevenvw>(new ArrayList<Tevenvw>());
		return page;
	}

	@Override
	public int deleteItem(TEVEN teven) {
		//Default, delete item not yet submitted status only & data MST(Auto Generate) can't delete.
		String sql = "WHERE process = '0' AND trxcode NOT LIKE 'A%' ";
		
		if (teven.getPernr() != null) {
			sql += "AND pernr = " + teven.getPernr() + " ";
		}
		if (teven.getLdate() != null) {
			sql += "AND ldate = '" + new SimpleDateFormat(TimeManagementConstants.FORMAT_YYYY_MM_DD).format(teven.getLdate()) + "' ";
		}
		if (teven.getSatza() != null) {
			sql += "AND satza = '" + teven.getSatza().getSatza() + "' ";
		}
		sql = "DELETE FROM TEVEN " + sql;
		return em.createNativeQuery(sql).executeUpdate();
	}

	@Override
	public Page<TEVEN> findByPernr(Long pernr, Date startDate, Date endDate, String satza,
			int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		return tevenRepository.findByPernr(pernr, startDate, endDate, pageRequest);
	}
}