package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyTrxhdr;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.5
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add parameter "notStatus" to filter by paymentStatus or not (optional)</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessesWithPaging, findByPernrAndProcessesWithPaging</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: findByPernrAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add methods: updateZclmno</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface ZmedTrxhdrQueryService extends DatabaseQueryService<ZmedTrxhdr, ZmedCompositeKeyTrxhdr>, DatabasePaginationQueryService{
	//Optional<ZmedTrxhdr> findByEntityName(final String entityName);
	
	//Optional<Collection<ZmedTrxhdr>> findByEntityName(final Long entityName);
	

	@NotNull
	Optional<ZmedTrxhdr> findOneByCompositeKey(String bukrs, String zclmno);
	

	@NotNull
	Collection<ZmedTrxhdr> findByPernr(Long pernr);

	@NotNull
	Collection<ZmedTrxhdr> findByBukrs(String bukrs);

	@NotNull
	Collection<ZmedTrxhdr> findByClmno(String clmno);

	Page<ZmedTrxhdr> findByBukrsAndProcessWithPaging(String bukrs, String process, String notStatus, int pageNumber);
	
	Page<ZmedTrxhdr> findByPernrAndProcessWithPaging(long pernr, String process, String notStatus, int pageNumber);

	Page<ZmedTrxhdr> findByBukrsAndProcessesWithPaging(String bukrs, String[] processes, String notStatus, int pageNumber);
	
	Page<ZmedTrxhdr> findByPernrAndProcessesWithPaging(long pernr, String[] processes, String notStatus, int pageNumber);
	
}

