package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyCostype;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;
import com.abminvestama.hcms.core.repository.ZmedEmpquotaRepository;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotaQueryService;

@Service("zmedEmpquotaQueryService")
@Transactional(readOnly = true)
public class ZmedEmpquotaQueryServiceImpl implements ZmedEmpquotaQueryService {

	private ZmedEmpquotaRepository zmedEmpquotaRepository;
	
	@Autowired
	ZmedEmpquotaQueryServiceImpl(ZmedEmpquotaRepository zmedEmpquotaRepository) {
		this.zmedEmpquotaRepository = zmedEmpquotaRepository;
	}
	
	//@Override
	//public Optional<ZmedEmpquota> findById(Optional<String> id) throws Exception {
	//	return id.map(pk -> zmedEmpquotaRepository.findOne(pk));
	//}
	
	
	
	@Override
	public Optional<ZmedEmpquota> findOneByCompositeKey(String bukrs, String persg, String persk, String fatxt, String kodequo, Date datab, Date datbi) {
		if (bukrs == null || persg == null || persk == null || fatxt == null || kodequo == null || datab == null || datbi == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(zmedEmpquotaRepository.findOneByCompositeKey(bukrs, persg, persk, fatxt, kodequo, datab, datbi));
	}

	@Override
	public Optional<Collection<ZmedEmpquota>> fetchAll() {
		List<ZmedEmpquota> entities = new ArrayList<>();
		Optional<Iterable<ZmedEmpquota>> entityIterable = Optional.ofNullable(zmedEmpquotaRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}

	/*@Override
	public Optional<ZmedEmpquota> findByEntityName(final String entityName) {
		return Optional.ofNullable(zmedEmpquotaRepository.findByPersk(entityName));
	}*/
	
	@Override
	public Optional<ZmedEmpquota> findById(Optional<ZmedCompositeKeyEmpquota> id)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ZmedEmpquota> findQuotas(String bukrs, String persg,
			String persk, Date datab, Date datbi) {
		if (bukrs == null || persg == null || persk == null || datab == null || datbi == null) {
			return Collections.emptyList();
		}
		Optional<Collection<ZmedEmpquota>> bunchOfZmed
			= Optional.ofNullable(zmedEmpquotaRepository.findQuotas(bukrs, persg, persk, datab, datbi));
		
		return (bunchOfZmed.isPresent()
				? bunchOfZmed.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

}
