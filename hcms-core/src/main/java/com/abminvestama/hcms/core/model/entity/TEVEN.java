package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.sql.Timestamp;
//import java.time.String;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "teven")
//public class TEVEN extends SAPAbstractEntityZmed<TEVENKey>{
public class TEVEN implements Serializable {

	private static final long serialVersionUID = 4747706564427397557L;


//	public TEVEN() {}


//	public TEVEN(TEVENKey id) {
//		this();
//		this.id = id;
//	}
	
	@Id
	@GeneratedValue(generator = "system-uuid")
	@Column(name = "tevenid", nullable = false, insertable = false, updatable = false, length = 255)
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	protected String tevenid;

	@Column(name = "pdsnr", nullable = true, length = 12)
	private Long pdsnr;
	
	@Column(name = "pernr", nullable = false, length = 40)
	private Long pernr;
//	@ManyToOne
//	@JoinColumn(name = "pernr", referencedColumnName = "pernr")
//	private IT0001 pernr;
	
	@Column(name = "zdivision")
	transient private String zdivision;

	@Temporal(TemporalType.DATE)
	@Column(name = "ldate")
	private Date ldate;
	

	//@Temporal(TemporalType.TIME)
	@Column(name = "ltime")
	private Date ltime;
	

	@Temporal(TemporalType.DATE)
	@Column(name = "erdat")
	private Date erdat;
	

	//@Temporal(TemporalType.TIME)
	@Column(name = "ertim")
	private Date ertim;

	@Column(name = "pmbde")
	private String pmbde;
	
	@ManyToOne
	@JoinColumn(name = "satza", referencedColumnName = "satza")
	private T705P satza;
	
	@Column(name = "terid")
	private String terid;

	
	
	@ManyToOne
	@JoinColumn(name = "abwgr", referencedColumnName = "pinco")
	private T705H abwgr;
	

	@Column(name = "exlga")
	private String exlga;
	
	@Column(name = "zeinh")
	private String zeinh;
	
	@Column(name = "hrazl")
	private Double hrazl;
	

	@Column(name = "hrbet")
	private Double hrbet;

	@Column(name = "origf")
	private String origf;

	@Column(name = "dallf")
	private String dallf;

	@Column(name = "pdc_usrup")
	private String pdcUsrup;
	

	@Column(name = "uname")
	private String uname;

	@Temporal(TemporalType.DATE)
	@Column(name = "aedtm")
	private Date aedtm;
	

	@Column(name = "zausw")
	private Long zausw;
	
	//add by wjk
	@Column(name = "process")
	private String process;
	
	@Column(name = "reason")
	private String reason;
	
	@Column(name = "trxcode")
	private String trxcode;


	@ManyToOne
	@JoinColumn(name = "submitted_by", referencedColumnName = "id")
	private User submittedBy;

	@Column(name = "submittedname_by")
	private transient String submittedNameBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "submitted_at")
	private Date submittedAt;

	@ManyToOne
	@JoinColumn(name = "approved1_by", referencedColumnName = "id")
	private User approved1By;
	
	@Column(name = "approvedname1_by")
	private transient String approvedName1By;


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved1_at")
	private Date approved1At;

	@ManyToOne
	@JoinColumn(name = "approved2_by", referencedColumnName = "id")
	private User approved2By;

	@Column(name = "approvedname2_by")
	private transient String approvedName2By;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved2_at")
	private Date approved2At;

	@ManyToOne
	@JoinColumn(name = "approved3_by", referencedColumnName = "id")
	private User approved3By;

	@Column(name = "approvedname3_by")
	private transient String approvedName3By;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved3_at")
	private Date approved3At;

	@ManyToOne
	@JoinColumn(name = "approved4_by", referencedColumnName = "id")
	private User approved4By;

	@Column(name = "approvedname4_by")
	private transient String approvedName4By;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved4_at")
	private Date approved4At;

	@ManyToOne
	@JoinColumn(name = "approved5_by", referencedColumnName = "id")
	private User approved5By;

	@Column(name = "approvedname5_by")
	private transient String approvedName5By;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved5_at")
	private Date approved5At;

	@ManyToOne
	@JoinColumn(name = "approved6_by", referencedColumnName = "id")
	private User approved6By;

	@Column(name = "approvedname6_by")
	private transient String approvedName6By;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved6_at")
	private Date approved6At;

	@Column(name = "approval_note")
	private String approvalNote;

	@Temporal(TemporalType.DATE)
	@Column(name = "agdate")
	private Date agdate;
	
	@Column(name = "agtime")
	private Date agtime;

	public String getTevenid() {
		return tevenid;
	}


	public void setTevenid(String tevenid) {
		this.tevenid = tevenid;
	}


	public Long getPdsnr() {
		return pdsnr;
	}


	public void setPdsnr(Long pdsnr) {
		this.pdsnr = pdsnr;
	}

/*
	public IT0001 getPernr() {
		return pernr;
	}


	public void setPernr(IT0001 pernr) {
		this.pernr = pernr;
	}
*/
	public Long getPernr() {
		return pernr;
	}

	public void setPernr(Long pernr) {
		this.pernr = pernr;
	}

	public String getZdivision() {
		return zdivision;
	}

	public void setZdivision(String zdivision) {
		this.zdivision = zdivision;
	}


	public Date getLdate() {
		return ldate;
	}


	public void setLdate(Date ldate) {
		this.ldate = ldate;
	}


	public Date getLtime() {
		return ltime;
	}


	public void setLtime(Date ltime) {
		this.ltime = ltime;
	}


	public Date getErdat() {
		return erdat;
	}


	public void setErdat(Date erdat) {
		this.erdat = erdat;
	}


	public Date getErtim() {
		return ertim;
	}


	public void setErtim(Date ertim) {
		this.ertim = ertim;
	}


	public String getPmbde() {
		return pmbde;
	}


	public void setPmbde(String pmbde) {
		this.pmbde = pmbde;
	}


	public T705P getSatza() {
		return satza;
	}


	public void setSatza(T705P satza) {
		this.satza = satza;
	}


	public String getTerid() {
		return terid;
	}


	public void setTerid(String terid) {
		this.terid = terid;
	}


	public T705H getAbwgr() {
		return abwgr;
	}


	public void setAbwgr(T705H abwgr) {
		this.abwgr = abwgr;
	}


	public String getExlga() {
		return exlga;
	}


	public void setExlga(String exlga) {
		this.exlga = exlga;
	}


	public String getZeinh() {
		return zeinh;
	}


	public void setZeinh(String zeinh) {
		this.zeinh = zeinh;
	}


	public Double getHrazl() {
		return hrazl;
	}


	public void setHrazl(Double hrazl) {
		this.hrazl = hrazl;
	}


	public Double getHrbet() {
		return hrbet;
	}


	public void setHrbet(Double hrbet) {
		this.hrbet = hrbet;
	}


	public String getOrigf() {
		return origf;
	}


	public void setOrigf(String origf) {
		this.origf = origf;
	}


	public String getDallf() {
		return dallf;
	}


	public void setDallf(String dallf) {
		this.dallf = dallf;
	}


	public String getPdcUsrup() {
		return pdcUsrup;
	}


	public void setPdcUsrup(String pdcUsrup) {
		this.pdcUsrup = pdcUsrup;
	}


	public String getUname() {
		return uname;
	}


	public void setUname(String uname) {
		this.uname = uname;
	}


	public Date getAedtm() {
		return aedtm;
	}


	public void setAedtm(Date aedtm) {
		this.aedtm = aedtm;
	}


	public Long getZausw() {
		return zausw;
	}


	public void setZausw(Long zausw) {
		this.zausw = zausw;
	}

	public String getProcess() {
		return process;
	}


	public void setProcess(String process) {
		this.process = process;
	}
	
	public String getReason() {
		return reason;
	}
	
	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getTrxcode() {
		return trxcode;
	}


	public void setTrxcode(String trxcode) {
		this.trxcode = trxcode;
	}


	public User getSubmittedBy() {
		return submittedBy;
	}

	public void setSubmittedBy(User submittedBy) {
		this.submittedBy = submittedBy;
	}

	public String getSubmittedNameBy() {
		return submittedBy != null && submittedBy.getEmployee() != null ? submittedBy.getEmployee().getEname() : null;
	}

	public void setSubmittedNameBy(String submittedNameBy) {
		this.submittedNameBy = submittedNameBy;
	}

	public Date getSubmittedAt() {
		return submittedAt;
	}

	public void setSubmittedAt(Date submittedAt) {
		this.submittedAt = submittedAt;
	}

	public User getApproved1By() {
		return approved1By;
	}

	public void setApproved1By(User approved1By) {
		this.approved1By = approved1By;
	}

	public Long getApprovedByPernr1() {
		return approved1By != null && approved1By.getEmployee() != null ? approved1By.getEmployee().getPernr() : null;
	}

	public String getApprovedName1By() {
		return approved1By != null && approved1By.getEmployee() != null ? approved1By.getEmployee().getEname() : null;
	}

	public void setApprovedName1By(String approvedName1By) {
		this.approvedName1By = approvedName1By;
	}

	public Date getApproved1At() {
		return approved1At;
	}

	public void setApproved1At(Date approved1At) {
		this.approved1At = approved1At;
	}

	public User getApproved2By() {
		return approved2By;
	}

	public void setApproved2By(User approved2By) {
		this.approved2By = approved2By;
	}

	public Long getApprovedByPernr2() {
		return approved2By != null && approved2By.getEmployee() != null ? approved2By.getEmployee().getPernr() : null;
	}

	public String getApprovedName2By() {
		return approved2By != null && approved2By.getEmployee() != null ? approved2By.getEmployee().getEname() : null;
	}
	
	public void setApprovedName2By(String approvedName2By) {
		this.approvedName2By = approvedName2By;
	}

	public Date getApproved2At() {
		return approved2At;
	}

	public void setApproved2At(Date approved2At) {
		this.approved2At = approved2At;
	}

	public User getApproved3By() {
		return approved3By;
	}

	public void setApproved3By(User approved3By) {
		this.approved3By = approved3By;
	}

	public Long getApprovedByPernr3() {
		return approved3By != null && approved3By.getEmployee() != null ? approved3By.getEmployee().getPernr() : null;
	}

	public String getApprovedName3By() {
		return approved3By != null && approved3By.getEmployee() != null ? approved3By.getEmployee().getEname() : null;
	}
	
	public void setApprovedName3By(String approvedName3By) {
		this.approvedName3By = approvedName3By;
	}

	public Date getApproved3At() {
		return approved3At;
	}

	public void setApproved3At(Date approved3At) {
		this.approved3At = approved3At;
	}

	public User getApproved4By() {
		return approved4By;
	}

	public void setApproved4By(User approved4By) {
		this.approved4By = approved4By;
	}

	public Long getApprovedByPernr4() {
		return approved4By != null && approved4By.getEmployee() != null ? approved4By.getEmployee().getPernr() : null;
	}

	public String getApprovedName4By() {
		return approved4By != null && approved4By.getEmployee() != null ? approved4By.getEmployee().getEname() : null;
	}
	
	public void setApprovedName4By(String approvedName4By) {
		this.approvedName4By = approvedName4By;
	}

	public Date getApproved4At() {
		return approved4At;
	}

	public void setApproved4At(Date approved4At) {
		this.approved4At = approved4At;
	}

	public User getApproved5By() {
		return approved5By;
	}

	public void setApproved5By(User approved5By) {
		this.approved5By = approved5By;
	}

	public Long getApprovedByPernr5() {
		return approved5By != null && approved5By.getEmployee() != null ? approved5By.getEmployee().getPernr() : null;
	}

	public String getApprovedName5By() {
		return approved5By != null && approved5By.getEmployee() != null ? approved5By.getEmployee().getEname() : null;
	}
	
	public void setApprovedName5By(String approvedName5By) {
		this.approvedName5By = approvedName5By;
	}

	public Date getApproved5At() {
		return approved5At;
	}

	public void setApproved5At(Date approved5At) {
		this.approved5At = approved5At;
	}

	public User getApproved6By() {
		return approved6By;
	}

	public void setApproved6By(User approved6By) {
		this.approved6By = approved6By;
	}

	public String getApprovedName6By() {
		return approved6By != null && approved6By.getEmployee() != null ? approved6By.getEmployee().getEname() : null;
	}
	
	public void setApprovedName6By(String approvedName6By) {
		this.approvedName6By = approvedName6By;
	}

	public Date getApproved6At() {
		return approved6At;
	}

	public void setApproved6At(Date approved6At) {
		this.approved6At = approved6At;
	}

	public String getApprovalNote() {
		return approvalNote;
	}

	public void setApprovalNote(String approvalNote) {
		this.approvalNote = approvalNote;
	}

	public Date getAgdate() {
		return agdate;
	}

	public void setAgdate(Date agdate) {
		this.agdate = agdate;
	}

	public Date getAgtime() {
		return agtime;
	}

	public void setAgtime(Date agltime) {
		this.agtime = agltime;
	}

}