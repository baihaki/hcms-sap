package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.IT2002;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface IT2002Repository extends CrudRepository<IT2002, ITCompositeKeys> {
	
	@Query("FROM IT2002 it2002 WHERE it2002.pernr = :pernr AND it2002.id.infty = :infty AND it2002.id.subty = :subty AND it2002.id.endda = :endda AND it2002.id.begda = :begda")
	IT2002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty,
			@Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM IT2002 it2002 WHERE it2002.pernr = :pernr AND it2002.id.infty = :infty ORDER BY it2002.id.subty ASC, it2002.id.endda DESC")
	Collection<IT2002> findByPernr(@Param("pernr") Long pernr, @Param("infty") String infty);
	
	@Query("FROM IT2002 it2002 WHERE it2002.pernr = :pernr AND it2002.id.infty = :infty AND it2002.id.subty = :subty ORDER BY it2002.id.endda DESC")
	Collection<IT2002> findByPernrAndSubty(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty);			
}