package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.constant.ApprovalAction;
import com.abminvestama.hcms.core.model.entity.ApprovalEventLog;

/**
 * 
 * @since 2.0.0
 * @version 2.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add lastCreatedDate parameter on findByEntityIdAndObjectIdAndApprovalAction</td></tr>
 *     <tr><td>2.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface ApprovalEventLogRepository extends PagingAndSortingRepository<ApprovalEventLog, String> {

	@Query("FROM ApprovalEventLog ael WHERE ael.hcmsEntity.id = :entityId AND ael.entityObjectId = :objectId ORDER BY ael.createdAt DESC")
	Collection<ApprovalEventLog> findByEntityIdAndObjectId(@Param("entityId") String entityId, 
			@Param("objectId") String objectId);
	
	@Query("FROM ApprovalEventLog ael WHERE ael.hcmsEntity.id = :entityId AND ael.entityObjectId = :objectId " + 
			"AND ael.approvalAction = :approvalAction AND ael.createdAt > :lastCreatedDate ORDER BY ael.createdAt DESC")
	Collection<ApprovalEventLog> findByEntityIdAndObjectIdAndApprovalAction(@Param("entityId") String entityId,
			@Param("objectId") String objectId, @Param("approvalAction") ApprovalAction approvalAction, @Param("lastCreatedDate") Date lastCreatedDate);
}
