package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.T527X;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.T527XRepository;
import com.abminvestama.hcms.core.service.api.business.command.T527XCommandService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("t527xCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class T527XCommandServiceImpl implements T527XCommandService {

	private T527XRepository t527xRepository;
	
	@Autowired
	T527XCommandServiceImpl(T527XRepository t527xRepository) {
		this.t527xRepository = t527xRepository;
	}
	
	@Override
	public Optional<T527X> save(T527X entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		try {
			return Optional.ofNullable(t527xRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist T527X (Organizational Unit).Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(T527X entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(T527X entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}
}