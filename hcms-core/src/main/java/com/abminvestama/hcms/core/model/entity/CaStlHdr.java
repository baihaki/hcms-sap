package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.abminvestama.hcms.core.model.constant.CashAdvanceConstant;


/**
 * 
 * Class that represents <strong>Cash Advance Settlement Header</strong> transaction.
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@Entity
@Table(name = CashAdvanceConstant.TABEL_CASHADVANCE_SETTLEMENT_HEADER)
public class CaStlHdr implements Serializable {
	
	private static final long serialVersionUID = -6159556408404797946L;

	@Id
	@Column(name = "req_no", nullable = false, insertable = false, updatable = false, length = 13)
	@GeneratedValue(
        strategy = GenerationType.SEQUENCE, 
        generator = "assigned-sequence"
    )
    @GenericGenerator(
        name = "assigned-sequence", 
        strategy = "com.abminvestama.hcms.core.model.entity.PostgreUUIDGenerationStrategy",
        parameters = @Parameter(
            name = "sequence_name",
            value = "hibernate_sequence"
        )
    )
    private String reqNo;

	@ManyToOne
	@JoinColumn(name = "tca_no", referencedColumnName = "req_no")
	private CaReqHdr tcaNo;
	
//	@Column(name = "doc_no")
//	private String docNo;

	@Column(name = "pernr")
	private Long pernr;

	@Column(name = "total_amount")
	private Double totalAmount;
	
	@Column(name = "diff_amount")
	private Double diffAmount;
	
	@Column(name = "currency")
	private String currency;
	
	@Column(name = "cas_status")
	private String casStatus;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	private Date createdAt;
	
	@ManyToOne
	@JoinColumn(name = "created_by", referencedColumnName = "id")
	private User createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	private Date updatedAt;
	
	@ManyToOne
	@JoinColumn(name = "updated_by", referencedColumnName = "id")
	private User updatedBy;
	
	@Column(name = "process")
	private Integer process;

	@Column(name = "notes")
	private String notes;

	@ManyToOne
	@JoinColumn(name = "bukrs", referencedColumnName = "bukrs")
	private T001 bukrs;

	@Column(name = "refund_amount")
	private Double refundAmount;

	@Column(name = "claim_amount")
	private Double claimAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_sync")
	private Date lastSync;

	@Column(name = "sap_message")
	private String sapMessage;

	@Column(name = "reason")
	private String reason;

	@Column(name = "attachment")
	private String attachment;

	@Temporal(TemporalType.DATE)
	@Column(name = "payment_date")
	private Date paymentDate;

	@Column(name = "payment_status")
	private String paymentStatus;

	@Column(name = "docno")
	private String docNo;

	@Column(name = "payment_status2")
	private String paymentStatus2;

	@Column(name = "docno2")
	private String docNo2;

	@Transient
	private List<CaStlDtl> stlDetail;

	@Transient
	private List<TrxApprovalProcess> approvalDetail;

	public CaStlHdr() {	}

	public CaStlHdr(String reqNo, CaReqHdr tcaNo, Long pernr, Double totalAmount, Double diffAmount,
			String currency, String casStatus, Date createdAt, User createdBy, Date updatedAt, User updatedBy,
			User approved1By, Date approved1At, User approved2By, Date approved2At, User approved3By, Date approved3At,
			User approved4By, Date approved4At, User approvedPoolingDeptBy, Date approvedPoolingDeptAt,
			User approvedAccountingBy, Date approvedAccountingAt, User approvedTreasuryBy, Date approvedTreasuryAt,
			Integer process, String notes, T001 bukrs, String reason, 
			Date paymentDate, String paymentStatus, String docNo, String paymentStatus2, String docNo2, 
			Double refundAmount, Double claimAmount,
			Date lastSync, String sapMessage, List<CaStlDtl> stlDetail, List<TrxApprovalProcess> approvalDetail) {
		this();
		this.reqNo = reqNo;
		this.tcaNo = tcaNo;
		this.pernr = pernr;
		this.totalAmount = totalAmount;
		this.diffAmount = diffAmount;
		this.currency = currency;
		this.casStatus = casStatus;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.updatedAt = updatedAt;
		this.updatedBy = updatedBy;
		this.process = process;
		this.notes = notes;
		this.bukrs = bukrs;
		this.reason = reason;
		this.paymentDate = paymentDate;
		this.paymentStatus = paymentStatus;
		this.docNo = docNo;
		this.paymentStatus2 = paymentStatus2;
		this.docNo2 = docNo2;
		this.refundAmount = refundAmount;
		this.claimAmount = claimAmount;
		this.lastSync = lastSync;
		this.sapMessage = sapMessage;
		this.stlDetail = stlDetail;
		this.approvalDetail = approvalDetail;
	}

	public String getReqNo() {
		return reqNo;
	}

	public void setReqNo(String reqNo) {
		this.reqNo = reqNo;
	}

	public CaReqHdr getTcaNo() {
		return tcaNo;
	}

	public void setTcaNo(CaReqHdr tcaNo) {
		this.tcaNo = tcaNo;
	}

	public Long getPernr() {
		return pernr;
	}

	public void setPernr(Long pernr) {
		this.pernr = pernr;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getDiffAmount() {
		return diffAmount;
	}

	public void setDiffAmount(Double diffAmount) {
		this.diffAmount = diffAmount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCasStatus() {
		return casStatus;
	}

	public void setCasStatus(String casStatus) {
		this.casStatus = casStatus;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getProcess() {
		return process;
	}

	public void setProcess(Integer process) {
		this.process = process;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public T001 getBukrs() {
		return bukrs;
	}

	public void setBukrs(T001 bukrs) {
		this.bukrs = bukrs;
	}

	public Double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public Double getClaimAmount() {
		return claimAmount;
	}

	public void setClaimAmount(Double claimAmount) {
		this.claimAmount = claimAmount;
	}

	public Date getLastSync() {
		return lastSync;
	}

	public void setLastSync(Date lastSync) {
		this.lastSync = lastSync;
	}

	public String getSapMessage() {
		return sapMessage;
	}

	public void setSapMessage(String sapMessage) {
		this.sapMessage = sapMessage;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getDocNo() {
		return docNo;
	}

	public String getPaymentStatus2() {
		return paymentStatus2;
	}

	public void setPaymentStatus2(String paymentStatus2) {
		this.paymentStatus2 = paymentStatus2;
	}

	public void setDocNo(String docNo) {
		this.docNo = docNo;
	}

	public String getDocNo2() {
		return docNo2;
	}

	public void setDocNo2(String docNo2) {
		this.docNo2 = docNo2;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public List<CaStlDtl> getStlDetail() {
		return stlDetail;
	}

	public void setStlDetail(List<CaStlDtl> stlDetail) {
		this.stlDetail = stlDetail;
	}

	public List<TrxApprovalProcess> getApprovalDetail() {
		return approvalDetail;
	}

	public void setApprovalDetail(List<TrxApprovalProcess> approvalDetail) {
		this.approvalDetail = approvalDetail;
	}

}

	