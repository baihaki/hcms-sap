package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.IT0001Trx;
import com.abminvestama.hcms.core.repository.IT0001TrxRepository;
import com.abminvestama.hcms.core.service.api.business.query.IT0001TrxQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("it0001TrxQueryService")
@Transactional(readOnly = true)
public class IT0001TrxQueryServiceImpl implements IT0001TrxQueryService {

	private IT0001TrxRepository it0001TrxRepository;
	
	@Autowired
	IT0001TrxQueryServiceImpl(IT0001TrxRepository it0001TrxRepository) {
		this.it0001TrxRepository = it0001TrxRepository;
	}
	
	@Override
	public Optional<IT0001Trx> findById(Optional<String> id) throws Exception {
		return id.map(pk -> it0001TrxRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<IT0001Trx>> fetchAll() {
		List<IT0001Trx> it0001TrxList = new ArrayList<>();
		it0001TrxRepository.findAll().forEach(it0001Trx -> {
			it0001TrxList.add(it0001Trx);
		});
		
		return Optional.of(it0001TrxList);
	}
}