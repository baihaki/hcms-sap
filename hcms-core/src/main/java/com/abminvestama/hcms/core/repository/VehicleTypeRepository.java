package com.abminvestama.hcms.core.repository;

import org.springframework.data.repository.CrudRepository;

import com.abminvestama.hcms.core.model.entity.VehicleType;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface VehicleTypeRepository extends CrudRepository<VehicleType, String> {

	VehicleType findByIdentName(String identName);
}