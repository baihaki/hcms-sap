package com.abminvestama.hcms.core.model.constant;

/**
 * 
 * @author wijanarko (wijanarko777@gmx.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public enum SettlementProcess {
	REJECTED(0), 
	WAITING_HEAD_RELEASE(1),
	WAITING_DIRECTOR_RELEASE(2),
	WAITING_GA_RELEASE(3),
	WAITING_HR_RELEASE(4),
	WAITING_POOLING_RELEASE(5),
	WAITING_ACCOUNTING_RELEASE(6),
	WAITING_TREASURY_RELEASE(7),
	READY_TO_SETTLE(8),
	SETTLED(9),
	REVERSED(10)
	;
	
	public int val; // convert enum to value
	
	private SettlementProcess(int val) {
		this.val = val;
	}

}
