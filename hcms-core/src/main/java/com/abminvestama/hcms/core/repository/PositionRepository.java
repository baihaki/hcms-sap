package com.abminvestama.hcms.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.abminvestama.hcms.core.model.entity.Position;

/**
 * 
 * This Repository interface is used along with class <strong>Position</strong>.
 * Latter on, this repository should be replacing T528TRepository on version 1.0.0.
 *
 * @see Position
 * 
 * @since 2.0.0
 * @version 2.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add findByCode method</td></tr>
 *     <tr><td>2.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface PositionRepository extends PagingAndSortingRepository<Position, String> {

	Page<Position> findAll(Pageable pageRequest);
	Position findByCode(String code);
}