package com.abminvestama.hcms.core.service.api.business.query;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.T591S;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface T591SQueryService extends DatabaseQueryService<T591S, T591SKey>, DatabasePaginationQueryService {
	
	Page<T591S> fetchAllWithPaging(int pageNumber);
	
	Page<T591S> fetchAllByInfotype(String infty, int pageNumber);
}