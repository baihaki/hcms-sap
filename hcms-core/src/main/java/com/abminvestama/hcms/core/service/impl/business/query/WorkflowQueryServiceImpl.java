package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.Workflow;
import com.abminvestama.hcms.core.repository.WorkflowRepository;
import com.abminvestama.hcms.core.service.api.business.query.WorkflowQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("workflowQueryService")
@Transactional(readOnly = true)
public class WorkflowQueryServiceImpl implements WorkflowQueryService {

	private WorkflowRepository workflowRepository;
	
	@Autowired
	WorkflowQueryServiceImpl(WorkflowRepository workflowRepository) {
		this.workflowRepository = workflowRepository;
	}
	
	@Override
	public Optional<Workflow> findById(Optional<String> id) throws Exception {
		return id.map(pk -> workflowRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<Workflow>> fetchAll() {
		List<Workflow> workflowList = new ArrayList<>();
		Optional<Iterable<Workflow>> workflowIterable = Optional.ofNullable(workflowRepository.findAll());
		return workflowIterable.map(iter -> {
			iter.forEach(workflow -> workflowList.add(workflow));
			return workflowList;
		});		
	}

	@Override
	public Page<Workflow> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return workflowRepository.findAll(pageRequest);
	}

	@Override
	public Optional<Workflow> findByName(String name) {
		return Optional.ofNullable(workflowRepository.findByName(name));
	}
}