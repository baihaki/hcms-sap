package com.abminvestama.hcms.core.service.helper;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.CaReqHdr;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public class CaReqHdrMapper {
	
	private CaReqHdr caReqHdr;
	
	private CaReqHdrMapper() {}
	
	public CaReqHdrMapper(Map<String, String> map) {
		if (map == null) {
			new CaReqHdrMapper();
			caReqHdr = new CaReqHdr();
		} else {
			caReqHdr = new CaReqHdr();
			
			caReqHdr.setPaymentStatus2(StringUtils.isNotEmpty(map.get("augbl")) ? "P" : "N");
			caReqHdr.setDocno2(map.get("augbl"));
			caReqHdr.setPaymentDate(map.get("augdt") != null && !map.get("augdt").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("augdt")) : null);
			if (StringUtils.isNotEmpty(map.get("belnr")))
				caReqHdr.setDocno(map.get("belnr"));
		}		
	}

	public CaReqHdr getCaReqHdr() {
		return caReqHdr;
	}
	
}
