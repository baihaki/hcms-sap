package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 * 
 * Class that represents master data transaction for <strong>Recurring Payments Deduction</strong> (i.e. Infotype IT0014 in SAP).
 *
 */
@Entity
@Table(name = "it0014")
public class IT0014 extends SAPAbstractEntity<ITCompositeKeys> {

	private static final long serialVersionUID = -1588142833461906815L;

	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "infty", referencedColumnName = "infty", insertable = false, updatable = false),
		@JoinColumn(name = "subty", referencedColumnName = "subty", insertable = false, updatable = false)
	})
	private T591S subty;
	
	public IT0014() {}
	
	public IT0014(ITCompositeKeys id) {
		this();
		this.id = id;
	}
	
	@Column(name = "seqnr")
	private Long seqnr;
	
	@ManyToOne
	@JoinColumn(name = "lgart", referencedColumnName = "lgart")
	private T512T lgart;
	
	@Column(name = "opken")
	private String opken;
	
	@Column(name = "betrg")
	private Double betrg;
	
	@Column(name = "waers")
	private String waers;
	
	/**
	 * GET Employee SSN/ID.
	 * 
	 * @return
	 */
	public Long getPernr() {
		return pernr;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	public T591S getSubty() {
		return subty;
	}
	
	/**
	 * GET Infotype Record No.
	 * 
	 * @return
	 */
	public Long getSeqnr() {
		return seqnr;
	}
	
	public void setSeqnr(Long seqnr) {
		this.seqnr = seqnr;
	}
	
	/**
	 * GET Wage Type.
	 * 
	 * @return
	 */
	public T512T getLgart() {
		return lgart;
	}
	
	public void setLgart(T512T lgart) {
		this.lgart = lgart;
	}
	
	/**
	 * GET Operation Indicator.
	 * 
	 * @return
	 */
	public String getOpken() {
		return opken;
	}
	
	public void setOpken(String opken) {
		this.opken = opken;
	}
	
	/**
	 * GET Amount.
	 * 
	 * @return
	 */
	public Double getBetrg() {
		return betrg;
	}
	
	public void setBetrg(Double betrg) {
		this.betrg = betrg;
	}
	
	/**
	 * GET Currency.
	 * 
	 * @return
	 */
	public String getWaers() {
		return waers;
	}
	
	public void setWaers(String waers) {
		this.waers = waers;
	}
}