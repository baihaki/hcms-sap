package com.abminvestama.hcms.core.service.api.business.query;

import com.abminvestama.hcms.core.model.entity.T706B1;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

public interface T706B1QueryService extends DatabaseQueryService<T706B1, String>, DatabasePaginationQueryService {
}