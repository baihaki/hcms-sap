package com.abminvestama.hcms.core.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Modify field attribute as part of composite key: clmno</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Entity
@Table(name = "zmed_trxitm")
public class ZmedTrxitm extends SAPAbstractEntityZmed<ZmedCompositeKeyTrxitm>{

	private static final long serialVersionUID = 2496232738510047249L;

	public ZmedTrxitm() {}


	public ZmedTrxitm(ZmedCompositeKeyTrxitm id) {
		this();
		this.id = id;
	}
	
	@Column(name = "itmno", nullable = false, insertable = false, updatable = false)
	private String itmno;
	
	@Column(name = "clmno", nullable = false, insertable = false, updatable = false)
	private String clmno;
	
	@ManyToOne
	@JoinColumn(name = "bukrs", referencedColumnName = "bukrs", nullable = false, insertable = false, updatable = false)
	private T001 bukrs;
	//@Column(name = "bukrs")
	//private String bukrs;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "billdt")
	private Date billdt;

	@Column(name = "kodemed")
	private String kodemed;
	
	@Column(name = "kodecos")
	private String kodecos;
	
	@Column(name = "zcostyp")
	private String zcostyp;
	
	@Column(name = "clmamt")
	private Double clmamt;
	
	@Column(name = "rejamt")
	private Double rejamt;
	
	@Column(name = "appamt")
	private Double appamt;
	
	@Column(name = "relat")
	private String relat;
	
	@Column(name = "umur")
	private String umur;
	
	@Column(name = "pasien")
	private String pasien;
	
	
	
	public String getItmno() {
		return itmno;
	}
	
	public void setItmno(String itmno) {
		this.itmno = itmno;
	}
	
	
	
	public T001 getBukrs() {
		return bukrs;
	}
	
	public void setBukrs(T001 bukrs) {
		this.bukrs = bukrs;
	}
	
	
	
	public String getClmno() {
		return clmno;
	}
	
	public void setClmno(String clmno) {
		this.clmno = clmno;
	}
	
	
	
	public Date getBilldt() {
		return billdt;
	}
	
	public void setBilldt(Date billdt) {
		this.billdt = billdt;
	}
	
	
	
	public String getKodemed() {
		return kodemed;
	}
	
	public void setKodemed(String kodemed) {
		this.kodemed = kodemed;
	}
	
	
	
	public String getKodecos() {
		return kodecos;
	}
	
	public void setKodecos(String kodecos) {
		this.kodecos = kodecos;
	}
	
	
	
	public String getZcostyp() {
		return zcostyp;
	}
	
	public void setZcostyp(String zcostyp) {
		this.zcostyp = zcostyp;
	}
	
	
	
	public Double getClmamt() {
		return clmamt;
	}
	
	public void setClmamt(Double clmamt) {
		this.clmamt = clmamt;
	}
	
	
	
	public Double getRejamt() {
		return rejamt;
	}
	
	public void setRejamt(Double rejamt) {
		this.rejamt = rejamt;
	}
	
	
	
	public Double getAppamt() {
		return appamt;
	}
	
	public void setAppamt(Double appamt) {
		this.appamt = appamt;
	}
	
	
	
	public String getRelat() {
		return relat;
	}
	
	public void setRelat(String relat) {
		this.relat = relat;
	}
	
	
	
	public String getUmur() {
		return umur;
	}
	
	public void setUmur(String umur) {
		this.umur = umur;
	}
	
	
	
	public String getPasien() {
		return pasien;
	}
	
	public void setPasien(String pasien) {
		this.pasien = pasien;
	}
}

