package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyQuotype;
import com.abminvestama.hcms.core.model.entity.ZmedQuotype;
import com.abminvestama.hcms.core.repository.ZmedQuotypeRepository;
import com.abminvestama.hcms.core.service.api.business.query.ZmedQuotypeQueryService;

@Service("zmedQuotypeQueryService")
@Transactional(readOnly = true)
public class ZmedQuotypeQueryServiceImpl implements ZmedQuotypeQueryService {

	private ZmedQuotypeRepository zmedQuotypeRepository;
	
	@Autowired
	ZmedQuotypeQueryServiceImpl(ZmedQuotypeRepository zmedQuotypeRepository) {
		this.zmedQuotypeRepository = zmedQuotypeRepository;
	}
	
	//@Override
	//public Optional<ZmedQuotype> findById(Optional<String> id) throws Exception {
	//	return id.map(pk -> zmedQuotypeRepository.findOne(pk));
	//}

	@Override
	public Optional<Collection<ZmedQuotype>> fetchAll() {
		List<ZmedQuotype> entities = new ArrayList<>();
		Optional<Iterable<ZmedQuotype>> entityIterable = Optional.ofNullable(zmedQuotypeRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}

	@Override
	public Optional<ZmedQuotype> findById(Optional<ZmedCompositeKeyQuotype> id)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<ZmedQuotype> findOneByCompositeKey(String bukrs,
			String kodequo, Date datab, Date datbi) {
		if (bukrs == null || kodequo == null || datab == null || datbi == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(zmedQuotypeRepository.findOneByCompositeKey(bukrs, kodequo, datab, datbi));
	}

	/*@Override
	public Optional<ZmedQuotype> findByEntityName(final String entityName) {
		return Optional.ofNullable(zmedQuotypeRepository.findByKodequo(entityName));
	}*/
}
