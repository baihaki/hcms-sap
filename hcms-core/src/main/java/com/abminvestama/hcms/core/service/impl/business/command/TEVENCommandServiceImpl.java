package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.TEVEN;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.TEVENRepository;
import com.abminvestama.hcms.core.service.api.business.command.TEVENCommandService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.00
 * @since 1.0.0
 *
 */
@Service("tevenCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class TEVENCommandServiceImpl implements TEVENCommandService {

	private TEVENRepository TEVENRepository;
	
	@Autowired
	TEVENCommandServiceImpl(TEVENRepository TEVENRepository) {
		this.TEVENRepository = TEVENRepository;
	}
	
	@Override
	public Optional<TEVEN> save(TEVEN entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setUname(user.getId());
		}
		
		try {
			return Optional.ofNullable(TEVENRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist TEVEN (Attendances).Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(TEVEN entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(TEVEN entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");				
	}
}