package com.abminvestama.hcms.core.model.entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "zmed_costype")
public class ZmedCostype extends SAPAbstractEntityZmed<ZmedCompositeKeyCostype> {

	private static final long serialVersionUID = 2496232738510047249L;

	public ZmedCostype() {}

	public ZmedCostype(ZmedCompositeKeyCostype id) {
		this();
		this.id = id;
	}
	
	
	@ManyToOne
	@JoinColumn(name = "bukrs", referencedColumnName = "bukrs", nullable = false, insertable = false, updatable = false)
	private T001 bukrs;
	//@Column(name = "bukrs", nullable = false, insertable = false, updatable = false)
	//private String bukrs;
	
	@Column(name = "kodemed", nullable = false, insertable = false, updatable = false)
	private String kodemed;
	
	@Column(name = "kodecos", nullable = false, insertable = false, updatable = false)
	private String kodecos;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datab", nullable = false, insertable = false, updatable = false)
	private Date datab;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datbi", nullable = false, insertable = false, updatable = false)
	private Date datbi;
	
	@Column(name = "descr")
	private String descr;
	
	
	
	public T001 getBukrs() {
		return bukrs;
	}
	
	public void setBukrs(T001 bukrs) {
		this.bukrs = bukrs;
	}
	
	
	public String getKodemed() {
		return kodemed;
	}
	
	public void setKodemed(String kodemed) {
		this.kodemed = kodemed;
	}
	
	
	public String getKodecos() {
		return kodecos;
	}
	
	public void setKodecos(String kodecos) {
		this.kodecos = kodecos;
	}
	
	
	public Date getDatab() {
		return datab;
	}
	
	public void setDatab(Date datab) {
		this.datab = datab;
	}
	
	
	public Date getDatbi() {
		return datbi;
	}
	
	public void setDatbi(Date datbi) {
		this.datbi = datbi;
	}
	
	
	public String getDescr() {
		return descr;
	}
	
	public void setDescr(String descr) {
		this.descr = descr;
	}
	
}

