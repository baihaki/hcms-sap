package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.abminvestama.hcms.core.model.entity.IT0077;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyCostype;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;


public interface ZmedCostypeQueryService extends DatabaseQueryService<ZmedCostype, ZmedCompositeKeyCostype>, DatabasePaginationQueryService{
	//Optional<ZmedCostype> findByEntityName(final String entityName);
	
	

	@NotNull
	Optional<ZmedCostype> findOneByCompositeKey(String bukrs, String kodemed, String kodecos, Date datab, Date datbi);

	@NotNull
	Collection<ZmedCostype> findByKodemed(String bukrs, String kodemed, Date datbi);

	@NotNull
	Collection<ZmedCostype> findByKodemed(String bukrs, Date datbi);
	
}

