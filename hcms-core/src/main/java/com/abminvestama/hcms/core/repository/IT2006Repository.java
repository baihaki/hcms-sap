package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.IT2006;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add employee's name (sname) on query result of findByBukrsAndSuperiorPositionWithPaging, findByBukrsWithPaging</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add methods: findByBukrsAndSuperiorPositionWithPaging, findByBukrsWithPaging</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add methods: findValidByPernr, findValidByPernrAndSubty</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT2006Repository extends CrudRepository<IT2006, ITCompositeKeys> {

	@Query("FROM IT2006 it2006 WHERE it2006.pernr = :pernr AND it2006.id.infty = '2006' AND it2006.id.subty = :subty AND it2006.id.endda = :endda AND it2006.id.begda = :begda")
	IT2006 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("subty") String subty,
			@Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM IT2006 it2006 WHERE it2006.pernr = :pernr AND it2006.id.infty = '2006' ORDER BY it2006.id.subty ASC, it2006.id.endda DESC")
	Collection<IT2006> findByPernr(@Param("pernr") Long pernr);
	
	@Query("FROM IT2006 it2006 WHERE it2006.desta <= current_date AND it2006.deend >= current_date AND "
			+ "it2006.pernr = :pernr AND it2006.id.infty = '2006' ORDER BY it2006.id.subty ASC, it2006.id.endda DESC")
	Collection<IT2006> findValidByPernr(@Param("pernr") Long pernr);
	
	@Query("FROM IT2006 it2006 WHERE it2006.pernr = :pernr AND it2006.id.infty = '2006' AND it2006.id.subty = :subty ORDER BY it2006.id.endda DESC")
	Collection<IT2006> findByPernrAndSubty(@Param("pernr") Long pernr, @Param("subty") String subty);
	
	@Query("FROM IT2006 it2006 WHERE it2006.desta <= current_date AND it2006.deend >= current_date AND "
			+ "it2006.pernr = :pernr AND it2006.id.infty = '2006' AND it2006.id.subty = :subty ORDER BY it2006.id.endda DESC")
	Collection<IT2006> findValidByPernrAndSubty(@Param("pernr") Long pernr, @Param("subty") String subty);

	String FIND_BY_SUPERIOR_COUNT = "SELECT COUNT(*) FROM it2006 INNER JOIN it0001 a ON it2006.pernr = a.pernr" +
	        " INNER JOIN position_relations b ON a.m_position_id = b.from_position_id " +
			" WHERE it2006.desta <= current_date AND it2006.deend >= current_date AND a.bukrs = :bukrs AND a.endda = '9999-12-31' " +
	        " AND (b.to_position_id = :superiorPosition OR a.pernr = :superiorPernr)";
	
	String FIND_BY_SUPERIOR_QUERY = "SELECT a.sname as uname, it2006.* FROM it2006 INNER JOIN it0001 a ON it2006.pernr = a.pernr" +
	        " INNER JOIN position_relations b ON a.m_position_id = b.from_position_id " +
			" WHERE it2006.desta <= current_date AND it2006.deend >= current_date AND a.bukrs = :bukrs AND a.endda = '9999-12-31' " +
	        " AND (b.to_position_id = :superiorPosition OR a.pernr = :superiorPernr) " +
	        " ORDER BY it2006.pernr, it2006.endda DESC, it2006.begda DESC LIMIT :limit OFFSET :offset";
	
	@Query(nativeQuery=true, value=FIND_BY_SUPERIOR_COUNT)
	int findByBukrsAndSuperiorPositionCount(@Param("bukrs") String bukrs, @Param("superiorPosition") String superiorPosition,
			@Param("superiorPernr") Long pernr);
	
	@Query(nativeQuery=true, value=FIND_BY_SUPERIOR_QUERY)
	Collection<IT2006> findByBukrsAndSuperiorPositionWithPaging(@Param("bukrs") String bukrs, @Param("superiorPosition") String superiorPosition,
			@Param("superiorPernr") Long pernr, @Param("limit") int limit, @Param("offset") int offset);

	String FIND_BY_BUKRS_COUNT = "SELECT COUNT(*) FROM it2006 INNER JOIN it0001 a ON it2006.pernr = a.pernr" +
			" WHERE it2006.desta <= current_date AND it2006.deend >= current_date AND a.bukrs = :bukrs AND a.endda = '9999-12-31'";
	
	String FIND_BY_BUKRS_QUERY = "SELECT a.sname as uname, it2006.* FROM it2006 INNER JOIN it0001 a ON it2006.pernr = a.pernr" +
			" WHERE it2006.desta <= current_date AND it2006.deend >= current_date AND a.bukrs = :bukrs AND a.endda = '9999-12-31' " +
	        " ORDER BY it2006.pernr, it2006.endda DESC, it2006.begda DESC LIMIT :limit OFFSET :offset";
	
	@Query(nativeQuery=true, value=FIND_BY_BUKRS_COUNT)
	int findByBukrsCount(@Param("bukrs") String bukrs);
	
	@Query(nativeQuery=true, value=FIND_BY_BUKRS_QUERY)
	Collection<IT2006> findByBukrsWithPaging(@Param("bukrs") String bukrs, @Param("limit") int limit, @Param("offset") int offset);

}