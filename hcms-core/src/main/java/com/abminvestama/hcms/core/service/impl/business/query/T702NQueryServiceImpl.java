package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T702N;
import com.abminvestama.hcms.core.repository.T702NRepository;
import com.abminvestama.hcms.core.service.api.business.query.T702NQueryService;

@Service("t702nQueryService")
@Transactional(readOnly = true)
public class T702NQueryServiceImpl implements T702NQueryService {

	private T702NRepository t702nRepository;
	
	@Autowired
	T702NQueryServiceImpl(T702NRepository t702nRepository) {
		this.t702nRepository = t702nRepository;
	}
	
	@Override
	public Optional<T702N> findById(Optional<String> id) throws Exception {
		return id.map(pk -> t702nRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<T702N>> fetchAll() {
		List<T702N> listOfT702N = new ArrayList<>();
		Optional<Iterable<T702N>> iterT702N = Optional.ofNullable(t702nRepository.findAll());
		return iterT702N.map(iter -> {
			iter.forEach(t702n -> {
				listOfT702N.add(t702n);
			});
			return listOfT702N;
		});
	}

	@Override
	public Page<T702N> findAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return t702nRepository.findAll(pageRequest);
	}
}