package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.WorkflowStep;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface WorkflowStepCommandService extends DatabaseCommandService<WorkflowStep> {
}