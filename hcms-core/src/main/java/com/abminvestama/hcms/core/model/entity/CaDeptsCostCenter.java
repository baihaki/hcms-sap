package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * Class that represents <strong>Departments Cost Center</strong> object.
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 3.0.0 (Cash Advance)
 * @since 3.0.0
 *
 */
@Entity
@Table(name = "ca_depts_cost_center")
public class CaDeptsCostCenter implements Serializable {
	
	private static final long serialVersionUID = -146877700635986056L;
	
	@Id
	@Column(name = "id", nullable = false)
	private Long id;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "orgeh", referencedColumnName = "orgeh"),
		@JoinColumn(name = "endda", referencedColumnName = "endda")
	})
	private T527X t527x;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "kokrs", referencedColumnName = "kokrs"),
		@JoinColumn(name = "kostl", referencedColumnName = "kostl")
	})
	private CSKT cskt; 
	
	public void setId(Long id){
		this.id = id;
	}
	
	/**
	 * GET Cost Center.
	 * 
	 * @return {@link CSKT} object
	 */
	public CSKT getCskt() {
		return cskt;
	}
	
	/**
	 * SET Cost Center.
	 * 
	 * @param cskt the Cost Center object
	 */
	public void setCskt(CSKT cskt) {
		this.cskt = cskt;
	}
	
	/**
	 * GET Organizational Unit.
	 * 
	 * @return {@link T527X} object
	 */
	public T527X getT527x() {
		return t527x;
	}
	
	/**
	 * SET Organizational Unit.
	 * 
	 * @param t527x the Organization object
	 */
	public void setT527x(T527X t527x) {
		this.t527x = t527x;
	}
	
	public CaDeptsCostCenter(){
	}

	public CaDeptsCostCenter( Long id,  T527X t527x,  CSKT cskt) {
	 	this.id = id;
	 	this.t527x = t527x;
	 	this.cskt = cskt;
	}
	
	public Long getId() {
		return  id;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		CaDeptsCostCenter caDeptsCostCenter = (CaDeptsCostCenter) o;
		 
		if (id != null ? !id.equals(caDeptsCostCenter.id) : caDeptsCostCenter.id != null) return false; 
		if (t527x != null ? !t527x.equals(caDeptsCostCenter.t527x) : caDeptsCostCenter.t527x != null) return false; 
		if (cskt != null ? !cskt.equals(caDeptsCostCenter.cskt) : caDeptsCostCenter.cskt != null) return false;
		return true; 
	}
	
	@Override
	public int hashCode() {
	    int result = (id != null ?  id.hashCode() : 0); 
		result = 31 * (t527x != null ?  t527x.hashCode() : 0); 
		result = 31 * (cskt != null ?  cskt.hashCode() : 0); 
	 
		return result; 
	}
	
	@Override
	public String toString() {
		return "caDeptsCostCenter(id="+id+",t527x="+t527x+",cskt="+cskt+')'; 
	}
 
}

	