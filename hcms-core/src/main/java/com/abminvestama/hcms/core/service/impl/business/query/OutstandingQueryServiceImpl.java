package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 2.0.0
 * @version 2.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("outstandingQueryService")
@Transactional(readOnly = true)
public class OutstandingQueryServiceImpl implements DatabaseQueryService<T001, String>, DatabasePaginationQueryService {

	@PersistenceContext(type = PersistenceContextType.TRANSACTION)	
	private EntityManager em;
	
	@Autowired
    Environment env;
	
	@Autowired
	private ApplicationContext appContext;
	
	private String query;
	
	@SuppressWarnings("unchecked")
	@Override
	public Optional<Collection<T001>> fetchAll() {
		
		String query = this.query;
		
		List<T001> listSuperiorOutstanding = new ArrayList<T001>();
		listSuperiorOutstanding = em.createNativeQuery(query, T001.class).getResultList();
		
		if (listSuperiorOutstanding.isEmpty()) {
		    return Optional.empty();
		}
		return Optional.of(listSuperiorOutstanding);
	}
	
	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
	
	@Override
	public Optional<T001> findById(Optional<String> id) throws Exception {
		throw new NoSuchMethodException("Method not implemented.");
	}
	
}