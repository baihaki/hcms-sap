package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyTrxhdr;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;

/**
 * 
 * @since 1.0.0
 * @version 1.0.6
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Change ORDER BY billdt to ORDER BY zclmno</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add parameter @Param("notStatus") to filter by paymentStatus or not (optional)</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessesWithPaging, findByPernrAndProcessesWithPaging</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: findByPernrWithPaging, findByPernrAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add methods: findByBukrsWithPaging, findByBukrsAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add methods: updateZclmno</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
//public interface ZmedTrxhdrRepository extends CrudRepository<ZmedTrxhdr, String> {
public interface ZmedTrxhdrRepository extends PagingAndSortingRepository<ZmedTrxhdr, ZmedCompositeKeyTrxhdr> {
	
	//ZmedTrxhdr findBy(String Name);	
	
	/*@Query("FROM ZmedTrxhdr zmed_trxhdr WHERE zmed_trxhdr.pernr = :pernr")
	Collection<ZmedTrxhdr> findByPernr(@Param("pernr") Long pernr);	*/
	
	@Query("FROM ZmedTrxhdr zmed_trxhdr WHERE zmed_trxhdr.bukrs.bukrs = :bukrs AND zmed_trxhdr.zclmno = :zclmno")
	ZmedTrxhdr findOneByCompositeKey(@Param("bukrs") String bukrs, 
			@Param("zclmno") String zclmno);
	

	@Query("FROM ZmedTrxhdr zmed_trxhdr WHERE zmed_trxhdr.pernr = :pernr")
	Collection<ZmedTrxhdr> findByPernr(@Param("pernr") Long pernr);

	@Query("FROM ZmedTrxhdr zmed_trxhdr WHERE zmed_trxhdr.bukrs.bukrs = :bukrs ")
	Collection<ZmedTrxhdr> findByBukrs(@Param("bukrs") String bukrs);


	
	@Query("FROM ZmedTrxhdr zmed_trxhdr WHERE zmed_trxhdr.zclmno = :zclmno")
	Collection<ZmedTrxhdr> findByClmno(@Param("zclmno") String zclmno);
	
	@Modifying(clearAutomatically = true)
	@Query(nativeQuery=true, value="UPDATE zmed_trxhdr SET zclmno = :newZclmno , process = :process, reason = :reason WHERE zclmno = :oldZclmno AND bukrs = :bukrs")
	int updateZclmno(@Param("newZclmno") String newZclmno, @Param("process") String process, @Param("reason") String reason, @Param("oldZclmno") String oldZclmno, @Param("bukrs") String bukrs);
	
	@Query("FROM ZmedTrxhdr zmed_trxhdr WHERE zmed_trxhdr.bukrs.bukrs = :bukrs AND (zmed_trxhdr.paymentStatus != :notStatus)"
			+ " ORDER BY zmed_trxhdr.clmdt DESC, zmed_trxhdr.zclmno DESC")
	Page<ZmedTrxhdr> findByBukrsWithPaging(@Param("bukrs") String bukrs, @Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM ZmedTrxhdr zmed_trxhdr WHERE zmed_trxhdr.bukrs.bukrs = :bukrs AND zmed_trxhdr.process = :process AND (zmed_trxhdr.paymentStatus != :notStatus)"
			+ " ORDER BY zmed_trxhdr.clmdt DESC, zmed_trxhdr.zclmno DESC")
	Page<ZmedTrxhdr> findByBukrsAndProcessWithPaging(@Param("bukrs") String bukrs, @Param("process") String process, @Param("notStatus") String notStatus, Pageable pageRequest);

	@Query("FROM ZmedTrxhdr zmed_trxhdr WHERE zmed_trxhdr.pernr = :pernr AND (zmed_trxhdr.paymentStatus != :notStatus)"
			+ " ORDER BY zmed_trxhdr.clmdt DESC, zmed_trxhdr.zclmno DESC")
	Page<ZmedTrxhdr> findByPernrWithPaging(@Param("pernr") Long pernr, @Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM ZmedTrxhdr zmed_trxhdr WHERE zmed_trxhdr.pernr = :pernr AND zmed_trxhdr.process = :process AND (zmed_trxhdr.paymentStatus != :notStatus)"
			+ " ORDER BY zmed_trxhdr.clmdt DESC, zmed_trxhdr.zclmno DESC")
	Page<ZmedTrxhdr> findByPernrAndProcessWithPaging(@Param("pernr") Long pernr, @Param("process") String process, @Param("notStatus") String notStatus, Pageable pageRequest);

	//@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.id.endda = :endda AND it0002.id.begda = :begda")
	//IT0002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
	//		@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM ZmedTrxhdr zmed_trxhdr WHERE zmed_trxhdr.bukrs.bukrs = :bukrs AND zmed_trxhdr.process in :processes AND (zmed_trxhdr.paymentStatus != :notStatus)"
			+ " ORDER BY zmed_trxhdr.clmdt DESC, zmed_trxhdr.zclmno DESC")
	Page<ZmedTrxhdr> findByBukrsAndProcessesWithPaging(@Param("bukrs") String bukrs, @Param("processes") String[] processes, @Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM ZmedTrxhdr zmed_trxhdr WHERE zmed_trxhdr.pernr = :pernr AND zmed_trxhdr.process in :processes AND (zmed_trxhdr.paymentStatus != :notStatus)"
			+ " ORDER BY zmed_trxhdr.clmdt DESC, zmed_trxhdr.zclmno DESC")
	Page<ZmedTrxhdr> findByPernrAndProcessesWithPaging(@Param("pernr") Long pernr, @Param("processes") String[] processes, @Param("notStatus") String notStatus, Pageable pageRequest);
	
	
}

