package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.SubT706B1;
import com.abminvestama.hcms.core.model.entity.SubT706B1Key;
import com.abminvestama.hcms.core.repository.SubT706B1Repository;
import com.abminvestama.hcms.core.service.api.business.query.SubT706B1QueryService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("subt706b1QueryService")
@Transactional(readOnly = true)
public class SubT706B1QueryServiceImpl implements SubT706B1QueryService {

	private SubT706B1Repository subt706b1Repository;
	
	@Autowired
	SubT706B1QueryServiceImpl(SubT706B1Repository subt706b1Repository) {
		this.subt706b1Repository = subt706b1Repository;
	}
	
	@Override
	public Optional<SubT706B1> findById(Optional<SubT706B1Key> id) throws Exception {
		return id.map(pk -> subt706b1Repository.findOne(pk));
	}

	@Override
	public Optional<Collection<SubT706B1>> fetchAll() {
		List<SubT706B1> listOfSubT706B1 = new ArrayList<>();
		Optional<Iterable<SubT706B1>> bunchOfSubT706B1 = Optional.ofNullable(subt706b1Repository.findAll());
		return bunchOfSubT706B1.map(subt706b1Iter -> {
			subt706b1Iter.forEach(subt706b1 -> {
				listOfSubT706B1.add(subt706b1);
			});
			return listOfSubT706B1;
		});
	}
	
	@Override
	public Collection<SubT706B1> findByMoreiAndSpkzlPrefixAndForm(String morei, String spkzlPrefix, Short form) {
		if (morei == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<SubT706B1>> list
			= Optional.ofNullable(form == null ? subt706b1Repository.findByMoreiAndSpkzlPrefix(morei, spkzlPrefix) 
					: subt706b1Repository.findByMoreiAndSpkzlPrefixAndForm(morei, spkzlPrefix, form));
		
		return (list.isPresent()
				? list.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}
}