package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0242;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0242Repository extends CrudRepository<IT0242, ITCompositeKeysNoSubtype> {

	@Query("FROM IT0242 it0242 WHERE it0242.pernr = :pernr AND it0242.id.endda = :endda AND it0242.id.begda = :begda")
	IT0242 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM IT0242 it0242 WHERE it0242.pernr = :pernr ORDER BY it0242.id.endda DESC")
	Collection<IT0242> findByPernr(@Param("pernr") Long pernr);		

	@Query("FROM IT0242 it0242 WHERE it0242.status = :status ORDER BY it0242.id.endda DESC")
	Page<IT0242> findByStatus(@Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0242 it0242 WHERE it0242.pernr = :pernr AND it0242.status = :status ORDER BY it0242.id.endda DESC")
	Page<IT0242> findByPernrAndStatus(@Param("pernr") Long pernr, @Param("status") DocumentStatus status, Pageable pageRequest);
}