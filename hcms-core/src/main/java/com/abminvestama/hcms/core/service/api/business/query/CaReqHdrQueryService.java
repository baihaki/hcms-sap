package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.CaReqHdr;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.2 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.2</td><td>Baihaki</td><td>Add parameter aprovalByUserId to findByBukrsAndProcessWithPaging</td></tr>
 *     <tr><td>3.0.1</td><td>Baihaki</td><td>Add parameter: dateBegin, dateEnd</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaReqHdrQueryService extends DatabaseQueryService<CaReqHdr, String>, DatabasePaginationQueryService{
	
	Page<CaReqHdr> findByBukrsAndProcessWithPaging(String bukrs, Integer process, String[] advanceCodes, String aprovalByUserId,
		Date dateBegin, Date dateEnd, String notStatus, String superiorPosition, int pageNumber);

	Page<CaReqHdr> findByPernrAndProcessWithPaging(long pernr, Integer process, String[] advanceCodes,
		Date dateBegin, Date dateEnd, String notStatus, int pageNumber);

	Page<CaReqHdr> findByBukrsAndProcessesWithPaging(String bukrs, Integer[] processes, String[] advanceCodes,
		Date dateBegin, Date dateEnd, String notStatus, int pageNumber);

	Page<CaReqHdr> findByPernrAndProcessesWithPaging(long pernr, Integer[] processes, String[] advanceCodes,
		Date dateBegin, Date dateEnd, String notStatus, int pageNumber);

	Optional<Collection<CaReqHdr>> findByPernrAndProcessesAndBetweenDate(Long pernr, Integer[] processes, String[] advanceCodes,
		Date dateBegin, Date dateEnd);
}

