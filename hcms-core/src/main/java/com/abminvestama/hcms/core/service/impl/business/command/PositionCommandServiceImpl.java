package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.Position;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.PositionRepository;
import com.abminvestama.hcms.core.service.api.business.command.PositionCommandService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("positionCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class PositionCommandServiceImpl implements PositionCommandService {

	private PositionRepository positionRepository;
	
	@Autowired
	PositionCommandServiceImpl(PositionRepository positionRepository) {
		this.positionRepository = positionRepository;
	}
	
	@Override
	public Optional<Position> save(Position entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setUpdatedById(user.getId());
		}
		try {
			return Optional.ofNullable(positionRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist Position (m_positions).Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(Position entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(Position entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}
}