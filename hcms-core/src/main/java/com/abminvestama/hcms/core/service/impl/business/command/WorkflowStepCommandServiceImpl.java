package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Date;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.WorkflowStep;
import com.abminvestama.hcms.core.repository.WorkflowStepRepository;
import com.abminvestama.hcms.core.service.api.business.command.WorkflowStepCommandService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("workflowStepCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class WorkflowStepCommandServiceImpl implements WorkflowStepCommandService {

	private WorkflowStepRepository workflowStepRepository;
	
	@Autowired
	WorkflowStepCommandServiceImpl(WorkflowStepRepository workflowStepRepository) {
		this.workflowStepRepository = workflowStepRepository;
	}
	
	@Override
	public Optional<WorkflowStep> save(WorkflowStep entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (entity.isNew()) {
			entity.setCreatedById(user.getId());
		}
		
		entity.setUpdatedById(user.getId());
		
		try {
			return Optional.ofNullable(workflowStepRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist WorkflowType Steps.Reason=" + e.getMessage());
		}		
	}

	@Override
	public boolean delete(WorkflowStep entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		entity.setDeletedById(user.getId());
		entity.setDeletedAt(new Date());
		
		return Optional.ofNullable(workflowStepRepository.save(entity)).isPresent() ? true : false;
	}

	@Override
	public boolean restore(WorkflowStep entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		entity.setDeletedAt(null);
		entity.setDeletedById(null);
		entity.setUpdatedById(user.getId());
		
		return Optional.ofNullable(workflowStepRepository.save(entity)).isPresent() ? true : false;		
	}
}