package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.PositionRelation;
import com.abminvestama.hcms.core.model.entity.Role;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.UserRepository;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.query.IT0001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.PositionRelationQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.1.0
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.1.0</td><td>Baihaki</td><td>Add methods: fetchByCompanyAndRole, findByPernrAndRelationRole</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add findByEmployeeName method with superiorPosition parameter</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add findByEmployeeName method</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add fetchAdminByCompany method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findByPernr method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("userQueryService")
@Transactional(readOnly = true)
public class UserQueryServiceImpl implements UserQueryService {
	
	private UserRepository userRepository;
	private CommonServiceFactory commonServiceFactory;
	
	@Autowired
	void setCommonServiceFactory(CommonServiceFactory commonServiceFactory) {
		this.commonServiceFactory = commonServiceFactory;
	}
	
	@Autowired
	UserQueryServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public Optional<User> findById(Optional<String> id) {
		return id.map(data -> Optional.ofNullable(userRepository.findOne(data)))
				.orElse(Optional.ofNullable(null));
	}

	@Override
	public Optional<Collection<User>> fetchAll() {
		return Optional.ofNullable(userRepository.fetchAllUsers());
	}

	@Override
	public Optional<User> findByUsername(Optional<String> username) {
		return username.map(data -> Optional.ofNullable(userRepository.findByUsername(data)))
				.orElse(Optional.ofNullable(null));
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> user = this.findByUsername(Optional.ofNullable(username));
		if (!user.isPresent()) {
			throw new UsernameNotFoundException("Cannot find user '" + username + "'");
		}
		
		return user.get();
	}

	@Override
	public Optional<User> findByPernr(Long pernr) {
		Optional<Collection<User>> listUser =  Optional.ofNullable(userRepository.findByPernr(pernr));
		return listUser.map(data -> data.stream().findFirst()).orElse(Optional.ofNullable(null));
	}
	
	@Override
	public Optional<Collection<User>> fetchAdminByCompany(String bukrs) {
		return Optional.ofNullable(userRepository.findAdminByBukrs(bukrs));
	}
	
	@Override
	public List<Long> fetchPernrByToPositionId(String toPositionId) {
		return userRepository.findPernrByToPositionId(toPositionId);
	}

	@Override
	public Optional<Collection<User>> fetchByCompanyAndRole(String bukrs, String roleName) {
		return Optional.ofNullable(userRepository.findByBukrsAndRole(bukrs, roleName));
	}

	@Override
	public Optional<User> findByPernrAndRelationRole(Long pernr, String roleName) {
		Optional<Collection<User>> listUser =  Optional.ofNullable(userRepository.findByPernr(pernr));
		Optional<User> user = listUser.map(data -> data.stream().findFirst()).orElse(Optional.ofNullable(null));
		Optional<Role> theRole = commonServiceFactory.getRoleQueryService().findByName(Optional.of(roleName));
		
		if (!user.isPresent() || !theRole.isPresent())
			return Optional.empty();
		
		while (true) {
			Collection<PositionRelation> listPositionRelation = 
				commonServiceFactory.getPositionRelationQueryService().findByFromPositionid(
					user.get().getEmployee().getPosition().getId());
			if (!listPositionRelation.isEmpty()) {
				PositionRelation positionRelation = listPositionRelation.stream().findFirst().get();
				Collection<IT0001> listIT0001 = commonServiceFactory.getIT0001QueryService().findByPositionId(
						positionRelation.getToPosition().getId());
				if (!listIT0001.isEmpty()) {
					IT0001 it0001 = listIT0001.stream().findFirst().get();
					Optional<User> superiorUser = findByPernr(it0001.getId().getPernr());
					if (superiorUser.isPresent() && superiorUser.get().getRoles().contains(theRole.get()))
						return superiorUser;
					user = superiorUser;
				} else {
					break;
				}
			} else {
				break;
			}
		}
		
		return Optional.empty();
		
	}
	
	@Override
	public Optional<Collection<User>> findByEmployeeName(String bukrs, String employeeName) {
		return Optional.ofNullable(bukrs.equals("ALL") ? userRepository.findAllByEmployeeName(employeeName) :
			userRepository.findByEmployeeName(bukrs, employeeName));
	}
	
	@Override
	public Optional<Collection<User>> findByEmployeeName(String bukrs, String employeeName, String superiorPosition) {
		return Optional.ofNullable(userRepository.findSubordinateByEmployeeName(bukrs, employeeName, superiorPosition));
	}
	
}