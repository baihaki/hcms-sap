package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;



@Embeddable
@Immutable
public class ZmedCompositeKeyTrxhdr implements Serializable {
	private static final long serialVersionUID = 5385850040856914239L;

	private String bukrs;
	private String zclmno;
	
	public ZmedCompositeKeyTrxhdr() {}
	
	public ZmedCompositeKeyTrxhdr(String bukrs, String zclmno) {
		this.zclmno = zclmno;
		this.bukrs = bukrs;
	}
	
	public String getZclmno() {
		return zclmno;
	}
	
	public String getBukrs() {
		return bukrs;
	}
		
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final ZmedCompositeKeyTrxhdr key = (ZmedCompositeKeyTrxhdr) o;
		
		if (key.getBukrs() != null ? key.getBukrs() != (bukrs != null ? bukrs: "") : bukrs != null) {
			return false;
		}
		
		if (key.getZclmno() != null ? key.getZclmno() != (zclmno != null ? zclmno: "") : zclmno != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.bukrs != null ? this.bukrs.hashCode() : 0;
		result = result * 31 + (this.zclmno != null ? this.zclmno.hashCode() : 0);
		return result;
	}
}
