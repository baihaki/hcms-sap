package com.abminvestama.hcms.core.repository;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.Workflow;
import com.abminvestama.hcms.core.model.entity.WorkflowStep;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface WorkflowStepRepository extends PagingAndSortingRepository<WorkflowStep, String> {

	@Query("FROM WorkflowStep ws WHERE ws.workflow.id=:workflowId AND ws.seq=:seq AND ws.position.id=:positionId")
	WorkflowStep findByWorkflowIdAndSeqAndPositionId(@Param("workflowId") String workflowId,
			@Param("seq") int seq, @Param("positionId") String positionId);

	@Query("FROM WorkflowStep ws WHERE ws.workflow.id=:workflowId ORDER BY ws.seq ASC")
	Collection<WorkflowStep> findByWorkflowId(@Param("workflowId") String workflowId);

	Collection<WorkflowStep> findByWorkflowAndSeqOrderByUpdatedAtDesc(Workflow workflow, int seq);
	
	@Query("FROM WorkflowStep ws WHERE ws.workflow = :workflow AND ws.seq > :seq")
	Collection<WorkflowStep> findByWorkflowAndGreaterThanSeq(@Param("workflow") Workflow workflow, @Param("seq") int seq);
	
	Page<WorkflowStep> findAll(Pageable pageRequest);
}