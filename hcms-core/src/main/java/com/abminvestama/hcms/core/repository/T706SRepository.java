package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.T706S;
import com.abminvestama.hcms.core.model.entity.T706SKey;

public interface T706SRepository extends CrudRepository<T706S, T706SKey> {

	
	@Query(nativeQuery = true, value = "SELECT * FROM t706s WHERE t706s.morei = :morei AND t706s.schem LIKE 'NE%'")
	Collection<T706S> findByMoreiNE(@Param("morei") String morei/*,
							@Param("schem") String schem*/);	
	
	

	@Query(nativeQuery = true, value = "SELECT * FROM t706s WHERE t706s.schem = :schem")
	T706S findBySchem(@Param("schem") String schem);	
}