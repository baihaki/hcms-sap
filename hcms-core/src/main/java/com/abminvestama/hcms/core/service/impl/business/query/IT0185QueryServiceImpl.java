package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.IT0185;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.repository.IT0185Repository;
import com.abminvestama.hcms.core.service.api.business.query.IT0185QueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Modify findByPernrAndStatus method: Set findByPernrForPublishedOrUpdated for PUBLISHED status</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add findByPernrAndStatus method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findByStatus method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("it0185QueryService")
@Transactional(readOnly = true)
public class IT0185QueryServiceImpl implements IT0185QueryService {
	
	private IT0185Repository it0185Repository;
	
	@Autowired
	IT0185QueryServiceImpl(IT0185Repository it0185Repository) {
		this.it0185Repository = it0185Repository;
	}

	@Override
	public Optional<IT0185> findById(Optional<ITCompositeKeys> id) throws Exception {
		throw new NoSuchMethodException("Method not implemented. Please use method findOneByCompositeKeys instead.");
	}

	@Override
	public Optional<Collection<IT0185>> fetchAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<IT0185> findOneByCompositeKey(Long pernr, String subty, Date endda, Date begda) {
		if (pernr == null || StringUtils.isBlank(subty) || endda == null || begda == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(it0185Repository.findOneByCompositeKey(pernr, SAPInfoType.PERSONAL_ID.infoType(), subty, endda, begda));		
	}

	@Override
	public Collection<IT0185> findByPernr(Long pernr) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT0185>> bunchOfIT0185 
			= Optional.ofNullable(it0185Repository.findByPernr(pernr, SAPInfoType.PERSONAL_ID.infoType()));
		
		return (bunchOfIT0185.isPresent()
				? bunchOfIT0185.get().stream().collect(Collectors.toList())
				: Collections.emptyList());		
	}

	@Override
	public Collection<IT0185> findByPernrAndSubty(Long pernr, String subty) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		if (StringUtils.isBlank(subty)) {
			subty = "01"; // default to subtype '01' (i.e. Identity Card)
		}
		
		Optional<Collection<IT0185>> bunchOfIT0185 
			= Optional.ofNullable(it0185Repository.findByPernrAndSubty(pernr, SAPInfoType.PERSONAL_ID.infoType(), subty));

		return (bunchOfIT0185.isPresent() 
					? bunchOfIT0185.get().stream().collect(Collectors.toList())
					: Collections.emptyList());				
	}

	@Override
	public Page<IT0185> findByStatus(String status, int pageNumber) {
		if (StringUtils.isEmpty(status)) {
			status = DocumentStatus.PUBLISHED.name();
		}
		
		Pageable pageRequest = createPageRequest(pageNumber);
		
		DocumentStatus documentStatus = DocumentStatus.valueOf(status);
		
		return it0185Repository.findByStatus(documentStatus, pageRequest);
	}

	@Override
	public Page<IT0185> findByPernrAndStatus(long pernr, String status, int pageNumber) {
		if (StringUtils.isEmpty(status)) {
			status = DocumentStatus.PUBLISHED.name();
		}
		
		Pageable pageRequest = createPageRequest(pageNumber);
		
		DocumentStatus documentStatus = DocumentStatus.valueOf(status);
		
		return (documentStatus == DocumentStatus.PUBLISHED) ? it0185Repository.findByPernrForPublishedOrUpdated(pernr, pageRequest)
				: it0185Repository.findByPernrAndStatus(pernr, documentStatus, pageRequest);
	}
}