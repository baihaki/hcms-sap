package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T528T;
import com.abminvestama.hcms.core.model.entity.T528TKey;
import com.abminvestama.hcms.core.repository.T528TRepository;
import com.abminvestama.hcms.core.service.api.business.query.T528TQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("t528tQueryService")
@Transactional(readOnly = true)
public class T528TQueryServiceImpl implements T528TQueryService {
	
	private T528TRepository t528tRepository;
	
	@Autowired
	T528TQueryServiceImpl(T528TRepository t528tRepository) {
		this.t528tRepository = t528tRepository;
	}

	@Override
	public Optional<T528T> findById(Optional<T528TKey> id) throws Exception {
		return id.map(pk -> t528tRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<T528T>> fetchAll() {
		List<T528T> listOfT528T = new ArrayList<>();
		Optional<Iterable<T528T>> bunchOfT528T = Optional.ofNullable(t528tRepository.findAll());
		return bunchOfT528T.map(iter -> {
			iter.forEach(t528t -> {
				listOfT528T.add(t528t);
			});
			return listOfT528T;
		});
	}
}