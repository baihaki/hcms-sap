package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.PtrvSrec;
import com.abminvestama.hcms.core.model.entity.PtrvSrecKey;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add updateTripnumber method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add method: findByTripnumber</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface PtrvSrecRepository extends CrudRepository<PtrvSrec, PtrvSrecKey> {



	@Query("FROM PtrvSrec ptrv_srec WHERE ptrv_srec.mandt = :mandt AND ptrv_srec.pernr = :pernr AND ptrv_srec.reinr = :reinr AND ptrv_srec.perio = :perio AND ptrv_srec.receiptno = :receiptno")
	PtrvSrec findOneByCompositeKey(@Param("mandt") String mandt, 
			@Param("pernr") Long pernr,
			@Param("reinr") Long reinr,
			@Param("perio") Integer perio,
			@Param("receiptno") String receiptno);
	
	@Query("FROM PtrvSrec ptrv_srec WHERE ptrv_srec.reinr = :reinr")
	Collection<PtrvSrec> findByTripnumber(@Param("reinr") Long tripnumber);
	
	@Modifying(clearAutomatically = true)
	@Query(nativeQuery=true, value="UPDATE ptrv_srec SET reinr = :newReinr WHERE reinr = :oldReinr AND receiptno = :receiptno AND pernr = :pernr")
	int updateTripnumber(@Param("newReinr") Long newReinr, @Param("oldReinr") Long oldReinr, @Param("receiptno") String receiptno, @Param("pernr") Long pernr);
	
	
}