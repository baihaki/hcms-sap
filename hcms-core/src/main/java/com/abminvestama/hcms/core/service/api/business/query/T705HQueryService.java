package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.T705H;
import com.abminvestama.hcms.core.model.entity.T705HKey;
import com.abminvestama.hcms.core.model.entity.T705P;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

public interface T705HQueryService extends DatabaseQueryService<T705H, T705HKey>, DatabasePaginationQueryService {

	@NotNull
	Optional<T705H> findByPinco(String pinco);
}