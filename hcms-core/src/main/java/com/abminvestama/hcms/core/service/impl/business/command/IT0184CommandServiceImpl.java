package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.IT0184;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.IT0184Repository;
import com.abminvestama.hcms.core.service.api.business.command.IT0184CommandService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("it0184CommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class IT0184CommandServiceImpl implements IT0184CommandService {

	private IT0184Repository it0184Repository;
	
	@Autowired
	IT0184CommandServiceImpl(IT0184Repository it0184Repository) {
		this.it0184Repository = it0184Repository;
	}
	
	@Override
	public Optional<IT0184> save(IT0184 entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setUname(user.getId());
		}
	
		try {
			return Optional.ofNullable(it0184Repository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist IT0184 (Resume Texts).Reason=" + e.getMessage());
		}					
	}

	@Override
	public boolean delete(IT0184 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(IT0184 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}
}