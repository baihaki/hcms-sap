package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;

/**
 *
 * Class that represents master data transaction for <strong>Communication</strong> (i.e. IT0105 in SAP).
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Extends {@link SAPSynchronizeEntity} for Synchronization fields</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add status (DocumentStatus) field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Entity
@Table(name = "it0105")
public class IT0105 extends SAPSynchronizeEntity<ITCompositeKeys> {

	private static final long serialVersionUID = -2677222667382347393L;
	
	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "infty", referencedColumnName = "infty", insertable = false, updatable = false),
		@JoinColumn(name = "subty", referencedColumnName = "subty", insertable = false, updatable = false)
	})
	private T591S subty;

	public IT0105() {}
	
	public IT0105(ITCompositeKeys id) {
		this();
		this.id = id;
	}
	
	@Column(name = "seqnr")
	private Long seqnr;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "comminfty", referencedColumnName = "infty", insertable = true, updatable = true),
		@JoinColumn(name = "usrty", referencedColumnName = "subty", insertable = true, updatable = true)
	})
	private T591S usrty;
	
	@Column(name = "usrid_long")
	private String usridLong;
	
	/**
	 * GET Employee SSN/ID.
	 * 
	 * @return
	 */
	public Long getPernr() {
		return pernr;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	public T591S getSubty() {
		return subty;
	}
	
	/**
	 * GET Infotype Record No. Incremental value on each update. 
	 * 
	 * @return
	 */
	public Long getSeqnr() {
		return seqnr;
	}
	
	public void setSeqnr(Long seqnr) {
		this.seqnr = seqnr;
	}
	
	/**
	 * GET Communication Type.
	 * 
	 * @return
	 */
	public T591S getUsrty() {
		return usrty;
	}
	
	public void setUsrty(T591S usrty) {
		this.usrty = usrty;
	}
	
	/**
	 * GET Long ID/Number.
	 * 
	 * @return
	 */
	public String getUsridLong() {
		return usridLong;
	}
	
	public void setUsridLong(String usridLong) {
		this.usridLong = usridLong;
	}
	
	

	@Column(name = "document_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private DocumentStatus status;
	
	
	
	public DocumentStatus getStatus() {
		return status;
	}
	
	public void setStatus(DocumentStatus status) {
		this.status = status;
	}
	
	
}