package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.CaStlHdr;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.CaStlHdrRepository;
import com.abminvestama.hcms.core.service.api.business.command.CaStlHdrCommandService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0
 * @author wijanarko (wijanarko@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("caStlHdrCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CaStlHdrCommandServiceImpl implements CaStlHdrCommandService {
	private CaStlHdrRepository caStlHdrRepository;

	@Autowired
	CaStlHdrCommandServiceImpl(CaStlHdrRepository caStlHdrRepository) {
		this.caStlHdrRepository = caStlHdrRepository;
	}

	@Override
	public Optional<CaStlHdr> save(CaStlHdr entity, User user) throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		try {
			return Optional.ofNullable(caStlHdrRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist CaStlHdr (Personal Information).Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(CaStlHdr entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean restore(CaStlHdr entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// TODO Auto-generated method stub
		return false;
	}

}
