package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;



@Embeddable
@Immutable
public class PtrvHeadKey implements Serializable {
	private static final long serialVersionUID = 5385850040856914239L;

	private Integer hdvrs;
	private Long pernr;
	private Long reinr;
	
	public Integer getHdvrs() {
		return hdvrs;
	}

	public Long getPernr() {
		return pernr;
	}

	public Long getReinr() {
		return reinr;
	}

	public PtrvHeadKey() {}
	
	public PtrvHeadKey(Long pernr, Long reinr, Integer hdvrs){
		this.pernr = pernr;
		this.reinr = reinr;
		this.hdvrs = hdvrs;
	}
	
	
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final PtrvHeadKey key = (PtrvHeadKey) o;
		
		if (key.getPernr() != null ? key.getPernr() != (pernr != null ? pernr: "") : pernr != null) {
			return false;
		}
		
		if (key.getReinr() != null ? key.getReinr() != (reinr != null ? reinr: "") : reinr != null) {
			return false;
		}
		
		if (key.getHdvrs() != null ? key.getHdvrs() != (hdvrs != null ? hdvrs: "") : hdvrs != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.pernr != null ? this.pernr.hashCode() : 0;
		result = result * 31 + (this.reinr != null ? this.reinr.hashCode() : 0);
		result = result * 31 + (this.hdvrs != null ? this.hdvrs.hashCode() : 0);
		return result;
	}
}
