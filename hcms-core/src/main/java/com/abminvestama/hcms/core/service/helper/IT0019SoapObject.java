package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0019;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0019SoapObject {
	
	private String employeenumber;
	private String validitybegin;
	private String validityend;
	private String infty;
	private String subty;
	private String seqnr;
	private String itxex;
	private String tmart;
	private String termn;
	private String mndat;
	private String bvmrk;
	private String tmjhr;
	private String tmmon;
	private String tmtag;
	private String mnjhr;
	private String mnmon;
	private String mntag;
	
	
	private IT0019SoapObject() {}
	
	public IT0019SoapObject(IT0019 object) {
		if (object == null) {
			new IT0019SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			infty = object.getSubty().getId().getInfty();
			subty = object.getSubty().getId().getSubty();
			seqnr = object.getSeqnr().toString();
			termn = CommonDateFunction.convertDateToStringYMD(object.getTermn());
			mndat = CommonDateFunction.convertDateToStringYMD(object.getMndat());
			itxex = object.getItxex();
			tmart = object.getTmart().getTmart();
			bvmrk = object.getBvmrk();
			tmjhr = object.getTmjhr().toString();
			tmmon = object.getTmmon().toString();
			tmtag = object.getTmtag().toString();
			mnjhr = object.getMnjhr().toString();
			mnmon = object.getMnmon().toString();
			mntag = object.getMntag().toString();
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}

	public String getInfty() {
		return infty;
	}

	public String getSubty() {
		return subty;
	}

	public String getSeqnr() {
		return seqnr;
	}

	public String getItxex() {
		return itxex;
	}

	public String getTmart() {
		return tmart;
	}

	public String getTermn() {
		return termn;
	}

	public String getMndat() {
		return mndat;
	}

	public String getBvmrk() {
		return bvmrk;
	}

	public String getTmjhr() {
		return tmjhr;
	}

	public String getTmmon() {
		return tmmon;
	}

	public String getTmtag() {
		return tmtag;
	}

	public String getMnjhr() {
		return mnjhr;
	}

	public String getMnmon() {
		return mnmon;
	}

	public String getMntag() {
		return mntag;
	}


}
