package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0009;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0009SoapObject {

	private String employeenumber;
	private String validitybegin;
	private String validityend;
	private String banktype;
	private String paymentmethod;
	private String payee;
	private String payeepostalcodecity;
	private String payeecity;
	private String bankcountry;
	private String bankkey;
	private String accountno;
	private String standardvalue;
	private String standardpercent;
	private String currency;
	private String purpose;
	
	private IT0009SoapObject() {}
	
	public IT0009SoapObject(IT0009 object) {
		if (object == null) {
			new IT0009SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			banktype = object.getBnksa() != null ? object.getBnksa().getId().getSubty() : object.getId().getSubty();
			paymentmethod = object.getZlsch().getZlsch();
			payee = object.getEmftx();
			payeepostalcodecity = object.getBkplz();
			payeecity = object.getBkort();
			bankcountry = object.getBanks().getLand1();
			bankkey = object.getBankl().getBankl();
			accountno = object.getBankn();
			standardvalue = object.getBetrg() != null && object.getBetrg() > 0 ? String.valueOf(object.getBetrg().intValue()) : null;
			standardpercent = object.getAnzhl() != null && object.getAnzhl() > 0 ? String.valueOf(object.getAnzhl().intValue()) : null;
			currency = object.getWaers();
			purpose = object.getZweck();
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}

	public String getBanktype() {
		return banktype;
	}

	public String getPaymentmethod() {
		return paymentmethod;
	}

	public String getPayee() {
		return payee;
	}

	public String getPayeepostalcodecity() {
		return payeepostalcodecity;
	}

	public String getPayeecity() {
		return payeecity;
	}

	public String getBankcountry() {
		return bankcountry;
	}

	public String getBankkey() {
		return bankkey;
	}

	public String getAccountno() {
		return accountno;
	}

	public String getStandardvalue() {
		return standardvalue;
	}

	public String getStandardpercent() {
		return standardpercent;
	}

	public String getCurrency() {
		return currency;
	}

	public String getPurpose() {
		return purpose;
	}

}
