package com.abminvestama.hcms.core.service.helper;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class ZmedTrxhdrSoapObject {
	
	private String bukrs;
	private String employeenumber;
    private String zclmno;
	private String orgtx;
	private String zdivision;
	private String ename;
	private String clmst;
	private String clmamt;
	private String rejamt;
	private String appamt;
	private String pagu1;
	private String pagu2;
	private String pagudent;
	private String pagu2byemp;
	private String totamt;
	private String pasien;
	private String penyakit;
	private String tglmasuk;
	private String tglkeluar;
	private String clmdt;
	private String clmby;
	private String aprdt;
	private String aprby;
	private String paydt;
	private String payby;
	private String paymethod;
	private String belnr;
	private String jamin;
	private String notex;
	private String billdt;
	private String kodemed;
	private String medtydesc;
	private String revdt;
	private String revby;
	private String sjaminan;

	private String process;
	private List<ZmedTrxitmSoapObject> trxItems;
	
	public static final String PROCESS_CREATE = "1";
	public static final String PROCESS_APPROVE = "2";
	public static final String PROCESS_REJECT = "3";
	
	private ZmedTrxhdrSoapObject() {}
	
	public ZmedTrxhdrSoapObject(ZmedTrxhdr object, String process) {
		if (object == null) {
			new ZmedTrxhdrSoapObject();
		} else {
			billdt = CommonDateFunction.convertDateToStringYMD(object.getBilldt());
			pasien = object.getPasien();
			kodemed = object.getKodemed();
			penyakit = object.getPenyakit();
			employeenumber = object.getPernr().toString();
			this.process = process;
			tglkeluar = object.getTglkeluar() != null ? CommonDateFunction.convertDateToStringYMD(object.getTglkeluar()) : null;
			tglmasuk = object.getTglmasuk() != null ? CommonDateFunction.convertDateToStringYMD(object.getTglmasuk()) : null;
			if (!process.equals("1")) {
				this.zclmno = object.getZclmno();
			}
			jamin = object.getJamin();
			if (jamin != null && !jamin.isEmpty()) {
				sjaminan = "X";
			}
		}		
	}
	
	public ZmedTrxhdrSoapObject(String bukrs, String zclmno, String employeenumber) {
		this.bukrs = bukrs;
		this.zclmno = zclmno;
		this.employeenumber = employeenumber;
	}

	public String getBukrs() {
		return bukrs;
	}

	public String getEmployeenumber() {
		return employeenumber;
	}

	public String getZclmno() {
		return zclmno;
	}

	public String getPasien() {
		return pasien;
	}

	public String getPenyakit() {
		return penyakit;
	}

	public String getTglmasuk() {
		return tglmasuk;
	}

	public String getTglkeluar() {
		return tglkeluar;
	}

	public String getBilldt() {
		return billdt;
	}

	public String getKodemed() {
		return kodemed;
	}

	public String getJamin() {
		return jamin;
	}

	public String getSjaminan() {
		return sjaminan;
	}

	public String getProcess() {
		return process;
	}

	public List<ZmedTrxitmSoapObject> getTrxItems() {
		return trxItems;
	}

	public void setTrxItems(List<ZmedTrxitmSoapObject> trxItems) {
		this.trxItems = trxItems;
	}
}
