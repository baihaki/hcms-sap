package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;



@Embeddable
@Immutable
public class TEVENKey implements Serializable {
	private static final long serialVersionUID = 5385850040856914239L;

	private Long tevenid;
	
	public TEVENKey() {}
	
	public TEVENKey(Long tevenid) {
		this.tevenid = tevenid;
	}
	
	public Long getTevenid() {
		return tevenid;
	}
			
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final TEVENKey key = (TEVENKey) o;
		
		if (key.getTevenid() != null ? key.getTevenid() != (tevenid != null ? tevenid: "") : tevenid != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.tevenid != null ? this.tevenid.hashCode() : 0;
		return result;
	}
}
