package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyQuotaused;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;
import com.abminvestama.hcms.core.repository.ZmedQuotausedRepository;
import com.abminvestama.hcms.core.service.api.business.query.ZmedQuotausedQueryService;

@Service("zmedQuotausedQueryService")
@Transactional(readOnly = true)
public class ZmedQuotausedQueryServiceImpl implements ZmedQuotausedQueryService {

	private ZmedQuotausedRepository zmedQuotausedRepository;
	
	@Autowired
	ZmedQuotausedQueryServiceImpl(ZmedQuotausedRepository zmedQuotausedRepository) {
		this.zmedQuotausedRepository = zmedQuotausedRepository;
	}
	
	//@Override
	//public Optional<ZmedQuotaused> findById(Optional<String> id) throws Exception {
	//	return id.map(pk -> zmedQuotausedRepository.findOne(pk));
	//}

	@Override
	public Optional<Collection<ZmedQuotaused>> fetchAll() {
		List<ZmedQuotaused> entities = new ArrayList<>();
		Optional<Iterable<ZmedQuotaused>> entityIterable = Optional.ofNullable(zmedQuotausedRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}

	@Override
	public Optional<ZmedQuotaused> findById(
			Optional<ZmedCompositeKeyQuotaused> id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<ZmedQuotaused> findOneByCompositeKey(int gjahr,
			String bukrs, long pernr, String kodequo) {
		if (bukrs == null || gjahr == 0 || kodequo == null || pernr == 0) {
			return Optional.empty();
		}
		return Optional.ofNullable(zmedQuotausedRepository.findOneByCompositeKey(gjahr, bukrs, pernr, kodequo));
	}

	@Override
	public List<ZmedQuotaused> findQuotas(int gjahr, String bukrs, long pernr) {
		if (bukrs == null || gjahr == 0 || pernr == 0) {
			return Collections.emptyList();
		}
		Optional<Collection<ZmedQuotaused>> bunchOfZmed
			= Optional.ofNullable(zmedQuotausedRepository.findQuotas(gjahr, bukrs, pernr));
		
		return (bunchOfZmed.isPresent()
				? bunchOfZmed.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}
		


	/*@Override
	public Optional<Collection<ZmedEmpquota>> findByEntityName(final Long entityName) {
		return Optional.ofNullable(zmedQuotausedRepository.findByPernr(entityName));
	}*/
}
