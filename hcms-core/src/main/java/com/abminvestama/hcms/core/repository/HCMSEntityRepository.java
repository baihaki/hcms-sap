package com.abminvestama.hcms.core.repository;

import org.springframework.data.repository.CrudRepository;

import com.abminvestama.hcms.core.model.entity.HCMSEntity;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface HCMSEntityRepository extends CrudRepository<HCMSEntity, String> {
	
	HCMSEntity findByEntityName(String entityName);	
}