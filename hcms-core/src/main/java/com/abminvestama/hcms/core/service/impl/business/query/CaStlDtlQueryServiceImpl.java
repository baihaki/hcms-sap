package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.CaStlDtl;
import com.abminvestama.hcms.core.repository.CaStlDtlRepository;
import com.abminvestama.hcms.core.service.api.business.query.CaStlDtlQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("caStlDtlQueryService")
@Transactional(readOnly = true)
public class CaStlDtlQueryServiceImpl implements CaStlDtlQueryService {
	private CaStlDtlRepository caStlDtlRepository;
	
	@Autowired
	CaStlDtlQueryServiceImpl(CaStlDtlRepository caStlDtlRepository) {
		this.caStlDtlRepository = caStlDtlRepository;
	}

	@Override
	public Optional<CaStlDtl> findById(Optional<String> id) throws Exception {
		return id.map(pk -> caStlDtlRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<CaStlDtl>> fetchAll() {
		List<CaStlDtl> listOfCaStlDtl = new ArrayList<>();
		Optional<Iterable<CaStlDtl>> bunchOfCaStlDtl = Optional.ofNullable(caStlDtlRepository.findAll());
		return bunchOfCaStlDtl.map(iter -> {
			iter.forEach(caStlDtl -> {
				listOfCaStlDtl.add(caStlDtl);
			});
			return listOfCaStlDtl;
		});
	}

	@Override
	public Page<CaStlDtl> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return caStlDtlRepository.findAll(pageRequest);
	}

	@Override
	public Optional<CaStlDtl> findByDetailId(String detailId) {
		if ( detailId == null ) return Optional.empty();
		return Optional.ofNullable(caStlDtlRepository.findByDetailId(detailId));
	}

	@Override
	public Collection<CaStlDtl> fetchByReqno(String reqNo) {
		if ( reqNo == null ) return Collections.emptyList();
		Optional<Collection<CaStlDtl>> bunchOfStlDtl 
			= Optional.ofNullable(caStlDtlRepository.fetchByReqno(reqNo));
		return (bunchOfStlDtl.isPresent() 
				? bunchOfStlDtl.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

	@Override
	public Optional<CaStlDtl> findOneByReqnoAndItemno(String reqNo, Integer itemNo) {
		if ( reqNo == null || itemNo == null ) return Optional.empty();
		return Optional.ofNullable(caStlDtlRepository.findOneByReqnoAndItemno(reqNo, itemNo.intValue()));
	}
}
