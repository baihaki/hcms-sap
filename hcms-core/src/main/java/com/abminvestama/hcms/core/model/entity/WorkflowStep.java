package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.abminvestama.hcms.core.model.constant.ApprovalType;
import com.abminvestama.hcms.core.model.constant.SequenceType;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Entity
@Table(name = "workflow_steps", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"m_workflow_id", "seq", "m_position_id"})
})
public class WorkflowStep extends AbstractEntity {

	private static final long serialVersionUID = 7627861647951667023L;

	@ManyToOne
	@JoinColumn(name = "m_workflow_id", insertable = true, updatable = true, nullable = false)
	private Workflow workflow;
	
	@Column(name = "seq", nullable = false)
	private int seq;

	@Column(name = "description", nullable = true)
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "m_position_id", insertable = true, updatable = true, nullable = false)
	private Position position;
	
	@Column(name = "sequence_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private SequenceType sequenceType;
	
	@Column(name = "approval_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private ApprovalType approvalType;
	
	public WorkflowStep() {}
	
	public Workflow getWorkflow() {
		return workflow;
	}
	
	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}
	
	public int getSeq() {
		return seq;
	}
	
	public void setSeq(int seq) {
		this.seq = seq;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Position getPosition() {
		return position;
	}
	
	public void setPosition(Position position) {
		this.position = position;
	}
	
	public SequenceType getSequenceType() {
		return sequenceType;
	}
	
	public void setSequenceType(SequenceType sequenceType) {
		this.sequenceType = sequenceType;
	}
	
	public ApprovalType getApprovalType() {
		return approvalType;
	}
	
	public void setApprovalType(ApprovalType approvalType) {
		this.approvalType = approvalType;
	}
}