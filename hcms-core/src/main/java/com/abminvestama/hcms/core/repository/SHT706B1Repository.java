package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.SHT706B1;
import com.abminvestama.hcms.core.model.entity.SHT706B1Key;

public interface SHT706B1Repository extends CrudRepository<SHT706B1, SHT706B1Key> {

	
	@Query(nativeQuery = true, value = "SELECT * FROM sh_t706b1 WHERE sh_t706b1.morei = :morei AND sh_t706b1.spkzl LIKE 'NE%'")
	Collection<SHT706B1> findByMoreiNE(@Param("morei") String morei/*,
							@Param("schem") String schem*/);	
}