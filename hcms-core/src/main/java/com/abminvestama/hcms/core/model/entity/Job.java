package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 * 
 * Master table for <strong>Job</strong>. Latter on, this table should 
 * replace table <strong>T513S</strong> on version 1.0.0.
 * 
 */
@Entity
@Table(name = "m_jobs", uniqueConstraints = { 
		@UniqueConstraint(columnNames = "code")
})
public class Job extends AbstractEntity {

	private static final long serialVersionUID = -6874048048507101048L;

	@Column(name = "code", nullable = false, unique = true)
	private String code;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "description", nullable = false)
	private String description;
	
	public Job() {}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
}