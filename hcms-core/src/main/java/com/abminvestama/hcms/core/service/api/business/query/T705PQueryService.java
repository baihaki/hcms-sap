package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.T705P;
import com.abminvestama.hcms.core.model.entity.T705PKey;
import com.abminvestama.hcms.core.model.entity.TEVEN;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

public interface T705PQueryService extends DatabaseQueryService<T705P, T705PKey>, DatabasePaginationQueryService {

	@NotNull
	Optional<T705P> findBySatza(String satza);
	
	@NotNull
	Page<T705P> fetchAllT705p(int pageNumber);
}