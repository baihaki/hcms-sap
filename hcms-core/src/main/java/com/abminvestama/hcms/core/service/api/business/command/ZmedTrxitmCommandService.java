package com.abminvestama.hcms.core.service.api.business.command;

import java.util.Optional;

import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add updateClmno method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface ZmedTrxitmCommandService extends DatabaseCommandService<ZmedTrxitm> {

	Optional<ZmedTrxitm> updateClmno(String newClmno, ZmedTrxitm entity,
			User user);
}