package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.Phdat;
import com.abminvestama.hcms.core.model.entity.PhdatKey;

/**
 * 
 * @since 2.0.0
 * @version 2.0.1
 * @author wijanarko (wijanarko777@gmx.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add findByDateAndType method</td></tr>
 *     <tr><td>2.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface PhdatRepository extends CrudRepository<Phdat, PhdatKey> {
	@Query(nativeQuery=true, value="SELECT * FROM phdat WHERE erdat = :erdat ")
	Phdat findByErdat(@Param("erdat") @Temporal(TemporalType.DATE) Date erdat);
	
	@Query("FROM Phdat phdat WHERE phdat.id.erdat >= :startdt AND phdat.id.erdat <= :enddt AND phdat.pinco LIKE :phtype ORDER BY phdat.id.erdat")
	Collection<Phdat> findByDateAndType(@Param("startdt") Date startDate, @Param("enddt") Date endDate, @Param("phtype") String phType);
}