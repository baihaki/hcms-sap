package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 *
 * Base entity model for Infotype SAP classes which store SAP Synchronization Status.
 *  
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 * 
 */

@MappedSuperclass
public abstract class SAPSynchronizeEntity<ID extends java.io.Serializable> extends SAPAbstractEntity<ID> {
	
	private static final long serialVersionUID = 8995879095834368688L;

	@Column(name = "sap_status", nullable = true, length=1)
	protected String sapStatus;
	
	@Column(name = "sap_message", nullable = true, length=255)
	protected String sapMessage;
	
	public String getSapStatus() {
		return sapStatus;
	}

	public void setSapStatus(String sapStatus) {
		this.sapStatus = sapStatus;
	}

	public String getSapMessage() {
		return sapMessage;
	}

	public void setSapMessage(String sapMessage) {
		this.sapMessage = sapMessage;
	}

	@Override
	public String toString() {
		String entityString = super.toString();
		return entityString.concat(", sap_status=").concat(sapStatus).concat(", sap_message=").concat(sapMessage);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		}
		
		if (obj == this) {
			return true;
		}
		
		if (obj == null || obj.getClass() != this.getClass()) {
			return false;
		}
		
		@SuppressWarnings("unchecked")
		final SAPSynchronizeEntity<ID> key = (SAPSynchronizeEntity<ID>) obj;
		
		if (key.getSapStatus() != null ? key.getSapStatus().compareTo(sapStatus) != 0 : sapStatus != null) {
			return false;
		}
		
		if (key.getSapMessage() != null ? !key.getSapMessage().equalsIgnoreCase(sapMessage) : sapMessage != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = result * 31 + (this.sapStatus != null ? this.sapStatus.hashCode() : 0);
		result = result * 31 + (this.sapMessage != null ? this.sapMessage.hashCode() : 0);
		return result;
	}	
}