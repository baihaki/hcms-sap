package com.abminvestama.hcms.core.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.EventLog;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface EventLogRepository extends PagingAndSortingRepository<EventLog, String> {

	@Query("FROM EventLog el WHERE el.hcmsEntity.id = :id AND el.entityObjectId = :objectId ORDER BY el.createdAt DESC")
	Collection<EventLog> findByEntityIdAndObjectId(@Param("id") String id, @Param("objectId") String objectId);
	
	@Query("FROM EventLog el WHERE el.hcmsEntity.id = :id AND el.entityObjectId = :objectId " + 
			"AND el.operationType = :operationType ORDER BY el.createdAt DESC")
	Collection<EventLog> findBYEntityIdAndObjectIdAndOperationType(@Param("id") String id, @Param("objectId") String objectId,
			@Param("operationType") String operationType);
}