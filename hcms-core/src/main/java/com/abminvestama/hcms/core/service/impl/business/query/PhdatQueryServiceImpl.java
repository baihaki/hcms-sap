package com.abminvestama.hcms.core.service.impl.business.query;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.Phdat;
import com.abminvestama.hcms.core.model.entity.PhdatKey;
import com.abminvestama.hcms.core.repository.PhdatRepository;
import com.abminvestama.hcms.core.service.api.business.query.PhdatQueryService;

/**
 * 
 * @since 2.0.0
 * @version 2.0.1
 * @author wijanarko (wijanarko777@gmx.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add findByDateAndType method</td></tr>
 *     <tr><td>2.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("phdatQueryService")
@Transactional(readOnly = true)
public class PhdatQueryServiceImpl implements PhdatQueryService {
	
	private PhdatRepository phdatRepository;

	@PersistenceContext(type = PersistenceContextType.TRANSACTION)	
	private EntityManager em;
	
	@Autowired
	PhdatQueryServiceImpl(PhdatRepository phdatRepository) {
		this.phdatRepository = phdatRepository;
	}

	@Override
	public Optional<Collection<Phdat>> fetchAll() {
		Optional<Iterable<Phdat>> tevenIterates = Optional.ofNullable(phdatRepository.findAll());
		if (!tevenIterates.isPresent()) {
			return Optional.empty();
		}
		
		List<Phdat> listOfPhdat = new ArrayList<>();
		tevenIterates.map(phdatIterable -> {			
			phdatIterable.forEach(teven -> {
				listOfPhdat.add(teven);
			});
			return listOfPhdat;
		});
		
		return Optional.of(listOfPhdat);
	}

	@Override
	public Optional<Phdat> findById(Optional<PhdatKey> id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<Phdat> fetchAllByStartDateAndEndDateWithOrdeBy(Date startDate, Date endDate, String phType, String orderBy, int pageNumber) {
		
		String DEFAULT_FORMAT_DATE = "yyyy-MM-dd";
		DateFormat formatDate = new SimpleDateFormat(DEFAULT_FORMAT_DATE);
		Pageable pageRequest = createPageRequest(pageNumber);
		
		String sql = "SELECT pinco, phdtxt, erdat, phtxt FROM phdat ";
		String sqlWhere = "";
		String sqlOrderBy = "ORDER BY " + orderBy;
		
		if(startDate != null && endDate != null) {
			sqlWhere += "erdat BETWEEN '" + formatDate.format(startDate) + "' AND '" + formatDate.format(endDate) + "' ";
		}
		if(phType != null && !phType.isEmpty()) {
			sqlWhere += sqlWhere.isEmpty() ? "pinco LIKE '" + phType + "%' " : "AND pinco LIKE '" + phType + "%' ";
		}
		
		sql += !sqlWhere.isEmpty() ? "WHERE " + sqlWhere : sqlWhere;
		sql += sqlOrderBy;
		Query q = em.createNativeQuery(sql);
		List<Object[]> data = q.getResultList();
		List<Phdat> phdats = new ArrayList<Phdat>();
		Phdat phdat;
		for(Object[] obj: data) {
			phdat = new Phdat();
			phdat.setPinco((String)obj[0]);
			phdat.setPhdtxt((String)obj[1]);
			phdat.setId(new PhdatKey((Date)obj[2]));
			phdat.setPhtxt((String)obj[3]);
			
			phdats.add(phdat);
		}
		return new PageImpl<Phdat>(phdats, pageRequest, phdats.size());
	}

	@Override
	public Optional<Phdat> findByErdat(Date erdat) {
		return Optional.ofNullable(phdatRepository.findByErdat(erdat));
	}

	@Override
	public Collection<Phdat> findByDateAndType(Date startDate, Date endDate, String phType) {
		if (startDate == null || endDate == null) {
			return Collections.emptyList();
		}
		
		phType = StringUtils.defaultString(phType).concat("%");
		Optional<Collection<Phdat>> bunchOfPhdat 
			= Optional.ofNullable(phdatRepository.findByDateAndType(startDate, endDate, phType));
		
		return (bunchOfPhdat.isPresent() ? bunchOfPhdat.get().stream().collect(Collectors.toList()) : Collections.emptyList());		
	}
}