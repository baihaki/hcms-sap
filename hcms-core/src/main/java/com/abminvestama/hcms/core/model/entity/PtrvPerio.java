package com.abminvestama.hcms.core.model.entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "ptrv_perio")
public class PtrvPerio extends SAPAbstractEntityZmed<PtrvPerioKey> {

	private static final long serialVersionUID = 2496232738510047249L;

	public PtrvPerio() {}

	public PtrvPerio(PtrvPerioKey id) {
		this();
		this.id = id;
	}
	
	
	@Column(name = "mandt", nullable = false, insertable = false, updatable = false)
	private String mandt;
	
	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;
	
	@Column(name = "reinr", nullable = false, insertable = false, updatable = false)
	private Long reinr;

	@Column(name = "perio", nullable = false, insertable = false, updatable = false)
	private Long perio;
	
	@Column(name = "pdvrs", nullable = false, insertable = false, updatable = false)
	private Long pdvrs;
	
	

	@Column(name = "hdvrs")
	private String hdvrs;

	@Temporal(TemporalType.DATE)
	@Column(name = "pdatv")
	private Date pdatv;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "puhrv")
	private Date puhrv;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "pdatb")
	private Date pdatb;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "puhrb")
	private Date puhrb;
	
	@Column(name = "antrg")
	private String antrg;

	@Column(name = "abrec")
	private String abrec;

	@Column(name = "uebrf")
	private String uebrf;

	@Column(name = "uebdt")
	private String uebdt;

	@Column(name = "tlock")
	private String tlock;

	@Column(name = "waers")
	private String waers;
	

	@Column(name = "abrj1")
	private Long abrj1;

	@Column(name = "abrp1")
	private Long abrp1;

	@Column(name = "perm1")
	private String perm1;

	@Column(name = "abkr1")
	private String abkr1;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "begp1")
	private Date begp1;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "endp1")
	private Date endp1;
	
	

	@Column(name = "abrj2")
	private Long abrj2;

	@Column(name = "abrp2")
	private Long abrp2;

	@Column(name = "perm2")
	private String perm2;

	@Column(name = "abkr2")
	private String abkr2;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "begp2")
	private Date begp2;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "endp2")
	private Date endp2;
	
	

	@Temporal(TemporalType.DATE)
	@Column(name = "accdt")
	private Date accdt;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "acctm")
	private Date acctm;
	
	@Column(name = "runid")
	private Long runid;

	@Column(name = "verpa")
	private String verpa;

	@Column(name = "uebkz")
	private String uebkz;
	
	@Column(name = "anuep")
	private Long anuep;

	@Column(name = "no_miles")
	private String noMiles;

	@Column(name = "lstay")
	private String lstay;

	public String getMandt() {
		return mandt;
	}

	public void setMandt(String mandt) {
		this.mandt = mandt;
	}

	public Long getPernr() {
		return pernr;
	}

	public void setPernr(Long pernr) {
		this.pernr = pernr;
	}

	public Long getReinr() {
		return reinr;
	}

	public void setReinr(Long reinr) {
		this.reinr = reinr;
	}

	public Long getPerio() {
		return perio;
	}

	public void setPerio(Long perio) {
		this.perio = perio;
	}

	public Long getPdvrs() {
		return pdvrs;
	}

	public void setPdvrs(Long pdvrs) {
		this.pdvrs = pdvrs;
	}

	public String getHdvrs() {
		return hdvrs;
	}

	public void setHdvrs(String hdvrs) {
		this.hdvrs = hdvrs;
	}

	public Date getPdatv() {
		return pdatv;
	}

	public void setPdatv(Date pdatv) {
		this.pdatv = pdatv;
	}

	public Date getPuhrv() {
		return puhrv;
	}

	public void setPuhrv(Date puhrv) {
		this.puhrv = puhrv;
	}

	public Date getPdatb() {
		return pdatb;
	}

	public void setPdatb(Date pdatb) {
		this.pdatb = pdatb;
	}

	public Date getPuhrb() {
		return puhrb;
	}

	public void setPuhrb(Date puhrb) {
		this.puhrb = puhrb;
	}

	public String getAntrg() {
		return antrg;
	}

	public void setAntrg(String antrg) {
		this.antrg = antrg;
	}

	public String getAbrec() {
		return abrec;
	}

	public void setAbrec(String abrec) {
		this.abrec = abrec;
	}

	public String getUebrf() {
		return uebrf;
	}

	public void setUebrf(String uebrf) {
		this.uebrf = uebrf;
	}

	public String getUebdt() {
		return uebdt;
	}

	public void setUebdt(String uebdt) {
		this.uebdt = uebdt;
	}

	public String getTlock() {
		return tlock;
	}

	public void setTlock(String tlock) {
		this.tlock = tlock;
	}

	public String getWaers() {
		return waers;
	}

	public void setWaers(String waers) {
		this.waers = waers;
	}

	public Long getAbrj1() {
		return abrj1;
	}

	public void setAbrj1(Long abrj1) {
		this.abrj1 = abrj1;
	}

	public Long getAbrp1() {
		return abrp1;
	}

	public void setAbrp1(Long abrp1) {
		this.abrp1 = abrp1;
	}

	public String getPerm1() {
		return perm1;
	}

	public void setPerm1(String perm1) {
		this.perm1 = perm1;
	}

	public String getAbkr1() {
		return abkr1;
	}

	public void setAbkr1(String abkr1) {
		this.abkr1 = abkr1;
	}

	public Date getBegp1() {
		return begp1;
	}

	public void setBegp1(Date begp1) {
		this.begp1 = begp1;
	}

	public Date getEndp1() {
		return endp1;
	}

	public void setEndp1(Date endp1) {
		this.endp1 = endp1;
	}

	public Long getAbrj2() {
		return abrj2;
	}

	public void setAbrj2(Long abrj2) {
		this.abrj2 = abrj2;
	}

	public Long getAbrp2() {
		return abrp2;
	}

	public void setAbrp2(Long abrp2) {
		this.abrp2 = abrp2;
	}

	public String getPerm2() {
		return perm2;
	}

	public void setPerm2(String perm2) {
		this.perm2 = perm2;
	}

	public String getAbkr2() {
		return abkr2;
	}

	public void setAbkr2(String abkr2) {
		this.abkr2 = abkr2;
	}

	public Date getBegp2() {
		return begp2;
	}

	public void setBegp2(Date begp2) {
		this.begp2 = begp2;
	}

	public Date getEndp2() {
		return endp2;
	}

	public void setEndp2(Date endp2) {
		this.endp2 = endp2;
	}

	public Date getAccdt() {
		return accdt;
	}

	public void setAccdt(Date accdt) {
		this.accdt = accdt;
	}

	public Date getAcctm() {
		return acctm;
	}

	public void setAcctm(Date acctm) {
		this.acctm = acctm;
	}

	public Long getRunid() {
		return runid;
	}

	public void setRunid(Long runid) {
		this.runid = runid;
	}

	public String getVerpa() {
		return verpa;
	}

	public void setVerpa(String verpa) {
		this.verpa = verpa;
	}

	public String getUebkz() {
		return uebkz;
	}

	public void setUebkz(String uebkz) {
		this.uebkz = uebkz;
	}

	public Long getAnuep() {
		return anuep;
	}

	public void setAnuep(Long anuep) {
		this.anuep = anuep;
	}

	public String getNoMiles() {
		return noMiles;
	}

	public void setNoMiles(String noMiles) {
		this.noMiles = noMiles;
	}

	public String getLstay() {
		return lstay;
	}

	public void setLstay(String lstay) {
		this.lstay = lstay;
	}
}

