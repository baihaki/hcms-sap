package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.VehicleType;
import com.abminvestama.hcms.core.repository.VehicleTypeRepository;
import com.abminvestama.hcms.core.service.api.business.query.VehicleTypeQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("vehicleTypeQueryService")
@Transactional(readOnly = true)
public class VehicleTypeQueryServiceImpl implements VehicleTypeQueryService {

	private VehicleTypeRepository vehicleTypeRepository;

	@Autowired
	void setVehicleTypeRepository(VehicleTypeRepository vehicleTypeRepository) {
		this.vehicleTypeRepository = vehicleTypeRepository;
	}
	
	@Override
	public Optional<VehicleType> findById(Optional<String> id) throws Exception {
		return id.map(key -> Optional.ofNullable(vehicleTypeRepository.findOne(key))).orElse(Optional.empty());
	}

	@Override
	public Optional<Collection<VehicleType>> fetchAll() {
		List<VehicleType> vehicleTypeList = new ArrayList<>();
		Optional<Iterable<VehicleType>> vehicleTypeIterable = Optional.ofNullable(vehicleTypeRepository.findAll());
		return vehicleTypeIterable.map(iter -> {
			iter.forEach(vehicleType -> vehicleTypeList.add(vehicleType));
			return vehicleTypeList;
		});
	}

	@Override
	public Optional<VehicleType> findByIdentName(String identName) {
		return Optional.ofNullable(vehicleTypeRepository.findByIdentName(identName));
	}
}