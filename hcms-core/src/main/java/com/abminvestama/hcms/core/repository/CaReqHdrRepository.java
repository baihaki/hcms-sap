package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.CaReqHdr;

/**
 * 
 * @since 3.0.0
 * @version 3.0.1 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.1</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessWithPagingForHead, findByBukrsAndProcessWithPagingForDirector</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaReqHdrRepository extends CrudRepository<CaReqHdr, String> {


	/*
	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.hdvrs = :hdvrs AND req_hdr.pernr = :pernr AND req_hdr.reinr = :reinr")
	CaReqHdr findOneByCompositeKey(@Param("hdvrs") Integer hdvrs, 
			@Param("pernr") Long pernr,
			@Param("reinr") Long reinr);
	*/
	
	@Modifying(clearAutomatically = true)
	@Query(nativeQuery=true, value="UPDATE req_hdr SET reinr = :newReinr, process = :process, approved1_by = :approved_by," +
	    " approved1_at = :approved_at, reason = :reason, last_sync = :last_sync WHERE reinr = :oldReinr AND pernr = :pernr")
	int updateReinr(@Param("newReinr") Long newReinr, @Param("process") String process,
			@Param("approved_by") String approvedBy, @Param("approved_at") Date approvedAt,
			@Param("reason") String reason, @Param("last_sync") Date lastSync,
			@Param("oldReinr") Long oldReinr, @Param("pernr") Long pernr);
	
	/*
	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.bukrs.bukrs = :bukrs AND (req_hdr.paymentStatus != :notStatus)"
			+ " ORDER BY req_hdr.dates DESC, req_hdr.times DESC")
	Page<CaReqHdr> findByBukrsWithPaging(@Param("bukrs") String bukrs, @Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.bukrs.bukrs = :bukrs AND req_hdr.process = :process AND (req_hdr.paymentStatus != :notStatus)"
			+ " ORDER BY req_hdr.dates DESC, req_hdr.times DESC")
	Page<CaReqHdr> findByBukrsAndProcessWithPaging(@Param("bukrs") String bukrs, @Param("process") String process, @Param("notStatus") String notStatus, Pageable pageRequest);

	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.pernr = :pernr AND (req_hdr.paymentStatus != :notStatus)"
			+ " ORDER BY req_hdr.dates DESC, req_hdr.times DESC")
	Page<CaReqHdr> findByPernrWithPaging(@Param("pernr") Long pernr, @Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.pernr = :pernr AND req_hdr.process = :process AND (req_hdr.paymentStatus != :notStatus)"
			+ " ORDER BY req_hdr.dates DESC, req_hdr.times DESC")
	Page<CaReqHdr> findByPernrAndProcessWithPaging(@Param("pernr") Long pernr, @Param("process") String process, @Param("notStatus") String notStatus, Pageable pageRequest);

	//@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.id.endda = :endda AND it0002.id.begda = :begda")
	//IT0002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
	//		@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.bukrs.bukrs = :bukrs AND req_hdr.process in :processes AND (req_hdr.paymentStatus != :notStatus)"
			+ " ORDER BY req_hdr.dates DESC, req_hdr.times DESC")
	Page<CaReqHdr> findByBukrsAndProcessesWithPaging(@Param("bukrs") String bukrs, @Param("processes") String[] processes, @Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.pernr = :pernr AND req_hdr.process in :processes AND (req_hdr.paymentStatus != :notStatus)"
			+ " ORDER BY req_hdr.dates DESC, req_hdr.times DESC")
	Page<CaReqHdr> findByPernrAndProcessesWithPaging(@Param("pernr") Long pernr, @Param("processes") String[] processes, @Param("notStatus") String notStatus, Pageable pageRequest);
	*/
	
	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.bukrs.bukrs = :bukrs AND req_hdr.advanceCode in :advancecodes"
			+ " AND req_hdr.createdAt >= :dateBegin AND req_hdr.createdAt <= :dateEnd" 
			+ " AND (req_hdr.paymentStatus != :notStatus) ORDER BY req_hdr.createdAt DESC")
	Page<CaReqHdr> findByBukrsWithPaging(@Param("bukrs") String bukrs, @Param("advancecodes") String[] advanceCodes,
			@Param("dateBegin") Date dateBegin, @Param("dateEnd") Date dateEnd,
			@Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.bukrs.bukrs = :bukrs AND req_hdr.process = :process AND req_hdr.advanceCode in :advancecodes"
			+ " AND req_hdr.createdAt >= :dateBegin AND req_hdr.createdAt <= :dateEnd"
			+ " AND (req_hdr.paymentStatus != :notStatus) ORDER BY req_hdr.createdAt DESC")
	Page<CaReqHdr> findByBukrsAndProcessWithPaging(@Param("bukrs") String bukrs, @Param("process") Integer process,
			@Param("advancecodes") String[] advanceCodes, @Param("dateBegin") Date dateBegin, @Param("dateEnd") Date dateEnd,
			@Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.bukrs.bukrs = :bukrs AND req_hdr.process = :process AND req_hdr.advanceCode in :advancecodes"
			+ " AND req_hdr.approved1By.id = :approvedById AND req_hdr.createdAt >= :dateBegin AND req_hdr.createdAt <= :dateEnd"
			+ " AND (req_hdr.paymentStatus != :notStatus) ORDER BY req_hdr.createdAt DESC")
	Page<CaReqHdr> findByBukrsAndProcessWithPagingForHead(@Param("bukrs") String bukrs, @Param("process") Integer process,
			@Param("advancecodes") String[] advanceCodes, @Param("approvedById") String approvedById, @Param("dateBegin") Date dateBegin, @Param("dateEnd") Date dateEnd,
			@Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.bukrs.bukrs = :bukrs AND req_hdr.process = :process AND req_hdr.advanceCode in :advancecodes"
			+ " AND req_hdr.approved2By.id = :approvedById AND req_hdr.createdAt >= :dateBegin AND req_hdr.createdAt <= :dateEnd"
			+ " AND (req_hdr.paymentStatus != :notStatus) ORDER BY req_hdr.createdAt DESC")
	Page<CaReqHdr> findByBukrsAndProcessWithPagingForDirector(@Param("bukrs") String bukrs, @Param("process") Integer process,
			@Param("advancecodes") String[] advanceCodes, @Param("approvedById") String approvedById, @Param("dateBegin") Date dateBegin, @Param("dateEnd") Date dateEnd,
			@Param("notStatus") String notStatus, Pageable pageRequest);

	String FIND_BY_SUPERIOR_COUNT = "SELECT COUNT(*) FROM ca_req_hdr req_hdr INNER JOIN it0001 a ON req_hdr.pernr = a.pernr" +
	        " INNER JOIN position_relations b ON a.m_position_id = b.from_position_id " +
			" WHERE req_hdr.bukrs = :bukrs AND req_hdr.process = :process AND req_hdr.advance_code in :advancecodes " +
	        " AND (req_hdr.payment_status != :notStatus) AND a.endda = '9999-12-31' AND b.to_position_id = :superiorPosition";
	
	String FIND_BY_SUPERIOR_QUERY = "SELECT req_hdr.* FROM ca_req_hdr req_hdr INNER JOIN it0001 a ON req_hdr.pernr = a.pernr" +
	        " INNER JOIN position_relations b ON a.m_position_id = b.from_position_id " +
			" WHERE req_hdr.bukrs = :bukrs AND req_hdr.process = :process AND req_hdr.advance_code in :advancecodes " +
	        " AND (req_hdr.payment_status != :notStatus) AND a.endda = '9999-12-31' AND b.to_position_id = :superiorPosition " +
			" ORDER BY req_hdr.created_at DESC LIMIT :limit OFFSET :offset";
	
	@Query(nativeQuery=true, value=FIND_BY_SUPERIOR_COUNT)
	int findByBukrsAndProcessAndSuperiorPositionCount(@Param("bukrs") String bukrs, @Param("process") Integer process,
			@Param("advancecodes") String[] advanceCodes, @Param("notStatus") String notStatus, @Param("superiorPosition") String superiorPosition);
	
	@Query(nativeQuery=true, value=FIND_BY_SUPERIOR_QUERY)
	Collection<CaReqHdr> findByBukrsAndProcessAndSuperiorPositionWithPaging(@Param("bukrs") String bukrs, @Param("process") Integer process,
			@Param("advancecodes") String[] advanceCodes, @Param("notStatus") String notStatus, @Param("superiorPosition") String superiorPosition,
			@Param("limit") int limit, @Param("offset") int offset);
	
	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.pernr = :pernr AND req_hdr.advanceCode in :advancecodes"
			+ " AND req_hdr.createdAt >= :dateBegin AND req_hdr.createdAt <= :dateEnd"
			+ " AND (req_hdr.paymentStatus != :notStatus) ORDER BY req_hdr.createdAt DESC")
	Page<CaReqHdr> findByPernrWithPaging(@Param("pernr") Long pernr, @Param("advancecodes") String[] advanceCodes,
			@Param("dateBegin") Date dateBegin, @Param("dateEnd") Date dateEnd,
			@Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.pernr = :pernr AND req_hdr.process = :process AND req_hdr.advanceCode in :advancecodes"
			+ " AND req_hdr.createdAt >= :dateBegin AND req_hdr.createdAt <= :dateEnd"
			+ " AND (req_hdr.paymentStatus != :notStatus) ORDER BY req_hdr.createdAt DESC")
	Page<CaReqHdr> findByPernrAndProcessWithPaging(@Param("pernr") Long pernr, @Param("process") Integer process,
			@Param("advancecodes") String[] advanceCodes, @Param("dateBegin") Date dateBegin, @Param("dateEnd") Date dateEnd,
			@Param("notStatus") String notStatus, Pageable pageRequest);

	//@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.id.endda = :endda AND it0002.id.begda = :begda")
	//IT0002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
	//		@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.bukrs.bukrs = :bukrs AND req_hdr.process in :processes AND req_hdr.advanceCode in :advancecodes"
			+ " AND req_hdr.createdAt >= :dateBegin AND req_hdr.createdAt <= :dateEnd"
			+ " AND (req_hdr.paymentStatus != :notStatus) ORDER BY req_hdr.createdAt DESC")
	Page<CaReqHdr> findByBukrsAndProcessesWithPaging(@Param("bukrs") String bukrs, @Param("processes") Integer[] processes,
			@Param("advancecodes") String[] advanceCodes, @Param("dateBegin") Date dateBegin, @Param("dateEnd") Date dateEnd,
			@Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.pernr = :pernr AND req_hdr.process in :processes AND req_hdr.advanceCode in :advancecodes"
			+ " AND req_hdr.createdAt >= :dateBegin AND req_hdr.createdAt <= :dateEnd"
			+ " AND (req_hdr.paymentStatus != :notStatus) ORDER BY req_hdr.createdAt DESC")
	Page<CaReqHdr> findByPernrAndProcessesWithPaging(@Param("pernr") Long pernr, @Param("processes") Integer[] processes,
			@Param("advancecodes") String[] advanceCodes, @Param("dateBegin") Date dateBegin, @Param("dateEnd") Date dateEnd,
			@Param("notStatus") String notStatus, Pageable pageRequest);
	
	@Query("FROM CaReqHdr req_hdr WHERE req_hdr.pernr = :pernr AND req_hdr.process in :processes AND req_hdr.advanceCode in :advancecodes"
			+ " AND req_hdr.createdAt >= :dateBegin AND req_hdr.createdAt <= :dateEnd ORDER BY req_hdr.createdAt DESC")
	Collection<CaReqHdr> findByPernrAndProcessesAndBetweenDate(@Param("pernr") Long pernr, @Param("processes") Integer[] processes,
			@Param("advancecodes") String[] advanceCodes, @Param("dateBegin") Date dateBegin, @Param("dateEnd") Date dateEnd);

	
}