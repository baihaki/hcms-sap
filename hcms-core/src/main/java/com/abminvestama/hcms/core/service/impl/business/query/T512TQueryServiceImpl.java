package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T512T;
import com.abminvestama.hcms.core.repository.T512TRepository;
import com.abminvestama.hcms.core.service.api.business.query.T512TQueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("t512tQueryService")
@Transactional(readOnly = true)
public class T512TQueryServiceImpl implements T512TQueryService {

	private T512TRepository t512tRepository;
	
	@Autowired
	T512TQueryServiceImpl(T512TRepository t512tRepository) {
		this.t512tRepository = t512tRepository;
	}
	
	@Override
	public Optional<T512T> findById(Optional<String> id) throws Exception {
		return id.map(pk -> {
			return Optional.ofNullable(t512tRepository.findOne(pk));
		}).orElse(Optional.empty());		
	}

	@Override
	public Optional<Collection<T512T>> fetchAll() {
		Optional<Iterable<T512T>> t512tIterable = Optional.ofNullable(t512tRepository.findAll());
		if (!t512tIterable.isPresent()) {
			return Optional.empty();
		}
		
		List<T512T> listOfT512T = new ArrayList<>();
		return t512tIterable.map(iteratesT512T -> {
			iteratesT512T.forEach(t512t -> {
				listOfT512T.add(t512t);
			});
			
			return listOfT512T;
		});		
	}
}