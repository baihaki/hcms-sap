package com.abminvestama.hcms.core.repository;

import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.ZmedQuotype;
import com.abminvestama.hcms.core.model.entity.ZmedQuotype;


public interface ZmedQuotypeRepository extends CrudRepository<ZmedQuotype, String> {
	
	//ZmedQuotype findByEntityName(String Name);
	
	/*ZmedQuotype findByKodequo(String kodequo);		*/
	
	@Query("FROM ZmedQuotype zmed_quotype WHERE zmed_quotype.bukrs.bukrs = :bukrs AND zmed_quotype.kodequo = :kodequo AND zmed_quotype.id.datab = :datab AND zmed_quotype.id.datbi = :datbi")
	ZmedQuotype findOneByCompositeKey(@Param("bukrs") String bukrs, 
			@Param("kodequo") String kodequo,
			@Param("datab") @Temporal(TemporalType.DATE) Date datab,
			@Param("datbi") @Temporal(TemporalType.DATE) Date datbi);
	
	

	//@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.id.endda = :endda AND it0002.id.begda = :begda")
	//IT0002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
	//		@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	
}