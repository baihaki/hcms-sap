package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Date;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.PositionRelation;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.PositionRelationRepository;
import com.abminvestama.hcms.core.service.api.business.command.PositionRelationCommandService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("positionRelationCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class PositionRelationCommandServiceImpl implements PositionRelationCommandService {

	private PositionRelationRepository positionRelationRepository;
	
	@Autowired
	PositionRelationCommandServiceImpl(PositionRelationRepository positionRelationRepository) {
		this.positionRelationRepository = positionRelationRepository;
	}
	
	@Override
	public Optional<PositionRelation> save(PositionRelation entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setCreatedById(user.getId());
		}
		
		try {
			return Optional.ofNullable(positionRelationRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist Position Relation.Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(PositionRelation entity, User user)
			throws NoSuchMethodException, ConstraintViolationException {
		if (user == null) {
			throw new ConstraintViolationException("Cannot delete Position Relation.Reason: Must provide current user ID!", null);
		}
		
		try {
			entity.setDeletedAt(new Date());
			entity.setDeletedById(user.getId());
			
			positionRelationRepository.save(entity);
		} catch (Exception e) {
			throw new ConstraintViolationException("Cannot delete Position Relation.Reason: " + e.getMessage(), null);
		}
		
		return true;
	}

	@Override
	public boolean restore(PositionRelation entity, User user)
			throws NoSuchMethodException, ConstraintViolationException {
		if (user == null) {
			throw new ConstraintViolationException("Cannot restore Position Relation.Reason: Must provide current User ID!", null);
		}
		
		try {
			entity.setDeletedAt(null);
			entity.setDeletedById(null);

			entity.setUpdatedById(user.getId());
		} catch (Exception e) {
			throw new ConstraintViolationException("Cannot restore Position Relation.Reason: " + e.getMessage(), null);
		}
		
		return true;
	}
}