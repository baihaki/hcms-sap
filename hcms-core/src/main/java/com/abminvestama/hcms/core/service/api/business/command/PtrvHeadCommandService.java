package com.abminvestama.hcms.core.service.api.business.command;

import java.util.Optional;

import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.PtrvHead;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Modify threadUpdate method: Add parameters (eventLogCommandService, eventLog, user)</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add threadUpdate method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add updateReinr method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface PtrvHeadCommandService extends DatabaseCommandService<PtrvHead> {

	Optional<PtrvHead> updateReinr(Long newReinr, PtrvHead entity, User user);

	void threadUpdate(PtrvHead entity, EventLogCommandService eventLogCommandService, EventLog eventLog, User user);
}