package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "t706b1")
public class T706B1 extends SAPAbstractEntityZmed<T706B1Key> {

	private static final long serialVersionUID = 3669795826077668803L;
	

	public T706B1() {}
	
	public T706B1(T706B1Key id) {
		this();
		this.id = id;
	}
	
	@Column(name = "morei", nullable = false, insertable = false, updatable = false, length = 2)
	private String morei;

	@ManyToOne
	@JoinColumn(name = "spkzl", referencedColumnName = "spkzl", nullable = false, insertable = false, updatable = false)
	private SHT706B1 spkzl;
	//@Column(name = "spkzl", nullable = false, insertable = false, updatable = false, length = 4)
	//private String spkzl;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "endda", nullable = false, insertable = false, updatable = false)
	private Date endda;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "begda", nullable = false, insertable = false, updatable = false)
	private Date begda;
	
	public void setMorei(String morei) {
		this.morei = morei;
	}

	public void setSpkzl(SHT706B1 spkzl) {
		this.spkzl = spkzl;
	}

	public void setEndda(Date endda) {
		this.endda = endda;
	}

	public void setBegda(Date begda) {
		this.begda = begda;
	}

	public String getMorei() {
		return morei;
	}

	public SHT706B1 getSpkzl() {
		return spkzl;
	}

	public Date getEndda() {
		return endda;
	}

	public Date getBegda() {
		return begda;
	}

	
	/*public T706B1(String morei, String spkzl, Date endda, Date begda) {
		this.morei = morei;
		this.spkzl = spkzl;
		this.endda = endda;
		this.begda = begda;
	}*/
	
	
}