package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 * Class that represents master data for <strong>Task Type</strong> (i.e. T531S in SAP).
 */
@Entity
@Table(name = "t531s")
public class T531S implements Serializable {

	private static final long serialVersionUID = -4344614033529166752L;
	
	@Id
	@Column(name = "tmart", unique = true, nullable = false)
	private String tmart;
	
	@Column(name = "tmtxt", nullable = false)
	private String tmtxt;
	
	public T531S() {}
	
	/**
	 * GET Task Type Code.
	 * 
	 * @return
	 */
	public String getTmart() {
		return tmart;
	}
	
	/**
	 * GET Task Type Name/Text.
	 * 
	 * @return
	 */
	public String getTmtxt() {
		return tmtxt;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final T531S obj = (T531S) o;
		
		if (obj.getTmart() != null ? !obj.getTmart().equalsIgnoreCase(tmart) : tmart != null) {
			return false;
		}
		
		if (obj.getTmtxt() != null ? !obj.getTmtxt().equalsIgnoreCase(tmtxt) : tmtxt != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.tmart != null ? this.tmart.hashCode() : 0;
		result = result * 31 + (this.tmtxt != null ? this.tmtxt.hashCode() : 0);
		return result;
	}
}