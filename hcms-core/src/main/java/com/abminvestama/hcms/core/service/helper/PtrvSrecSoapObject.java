package com.abminvestama.hcms.core.service.helper;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.PtrvSrec;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class PtrvSrecSoapObject {

	private String mandt;
	private String pernr;
	private String reinr;
	private String perio;
	private String receiptno;
	private String expType;
	private String recAmount;
	private String recCurr;
	private String recRate;
	private String locAmount;
	private String locCurr;
	private String shorttxt;
	private String paperReceipt;
	private String recDate;
	
	private PtrvSrecSoapObject() {}
	
	public PtrvSrecSoapObject(PtrvSrec object) {
		if (object == null) {
			new PtrvSrecSoapObject();
		} else {
			//mandt = object.getMandt();
			//pernr = object.getPernr().toString();
			//reinr = object.getReinr().toString();
			//perio = object.getPerio().toString();
			receiptno = object.getReceiptno();
			expType = object.getSubt706b1() != null ? object.getSubt706b1().getId().getSpkzl() : null; 
			recAmount = object.getRecAmount() != null && object.getRecAmount() > 0 ? String.valueOf(object.getRecAmount().intValue()) : "0";
			recCurr = object.getRecCurr();
			recRate = object.getRecRate() != null && object.getRecRate() > 0 ? String.valueOf(object.getRecRate().intValue()) : "0";
			shorttxt = object.getShorttxt();
			paperReceipt = object.getPaperReceipt();
			recDate = object.getRecDate() != null ? CommonDateFunction.convertDateToStringYMD(object.getRecDate()) : null;
			
			// modify mapping
			//if (expType.equals("NEOT")) {
				//expType = object.getSubt706b1().getId().getSubSpkzl();
			//}
		}		
	}

	public String getReceiptno() {
		return receiptno;
	}

	public String getExpType() {
		return expType;
	}

	public String getRecAmount() {
		return recAmount;
	}

	public void setRecAmount(String recAmount) {
		this.recAmount = recAmount;
	}

	public String getRecCurr() {
		return recCurr;
	}

	public String getRecRate() {
		return recRate;
	}

	public String getShorttxt() {
		return shorttxt;
	}

	public String getPaperReceipt() {
		return paperReceipt;
	}

	public String getRecDate() {
		return recDate;
	}

}
