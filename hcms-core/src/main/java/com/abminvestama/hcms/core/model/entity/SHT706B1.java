package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "sh_t706b1")
public class SHT706B1 extends SAPAbstractEntityZmed<SHT706B1Key> {

	private static final long serialVersionUID = 3669795826077668803L;
	

	public SHT706B1() {}
	
	public SHT706B1(SHT706B1Key id) {
		this();
		this.id = id;
	}
	
	@Column(name = "morei", nullable = false, insertable = false, updatable = false, length = 2)
	private String morei;
	
	@Column(name = "spkzl", nullable = false, insertable = false, updatable = false, length = 4)
	private String spkzl;

	@Column(name = "sptxt", nullable = false, insertable = false, updatable = false)
	private String sptxt;
	
	public void setMorei(String morei) {
		this.morei = morei;
	}

	public void setSpklz(String spkzl) {
		this.spkzl = spkzl;
	}

	public void setSptxt(String sptxt) {
		this.sptxt = sptxt;
	}

	public String getMorei() {
		return morei;
	}

	public String getSpklz() {
		return spkzl;
	}

	public String getSptxt() {
		return sptxt;
	}

	
}