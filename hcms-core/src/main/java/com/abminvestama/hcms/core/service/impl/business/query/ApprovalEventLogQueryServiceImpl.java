package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.ApprovalAction;
import com.abminvestama.hcms.core.model.entity.ApprovalEventLog;
import com.abminvestama.hcms.core.repository.ApprovalEventLogRepository;
import com.abminvestama.hcms.core.service.api.business.query.ApprovalEventLogQueryService;

/**
 * 
 * @since 2.0.0
 * @version 2.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add lastCreatedDate parameter on findByEntityIdAndObjectIdAndApprovalAction</td></tr>
 *     <tr><td>2.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("approvalEventLogQueryService")
@Transactional(readOnly = true)
public class ApprovalEventLogQueryServiceImpl implements ApprovalEventLogQueryService {
	
	private ApprovalEventLogRepository approvalEventLogRepository;
	
	@Autowired
	ApprovalEventLogQueryServiceImpl(ApprovalEventLogRepository approvalEventLogRepository) {
		this.approvalEventLogRepository = approvalEventLogRepository;
	}

	@Override
	public Optional<ApprovalEventLog> findById(Optional<String> id) throws Exception {
		return id.map(pk -> approvalEventLogRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<ApprovalEventLog>> fetchAll() {
		Optional<Iterable<ApprovalEventLog>> approvalEventLogs = Optional.ofNullable(approvalEventLogRepository.findAll());
		List<ApprovalEventLog> approvalEventLogList = new ArrayList<>();
		return approvalEventLogs.map(aelrecs -> {
			aelrecs.forEach(ael -> {
				approvalEventLogList.add(ael);
			});
			return approvalEventLogList;
		});
	}

	@Override
	public Collection<ApprovalEventLog> findByEntityIdAndObjectId(String entityId, String objectId) {
		if (StringUtils.isEmpty(entityId) || StringUtils.isEmpty(objectId)) {
			return Collections.emptyList();
		}
		return approvalEventLogRepository.findByEntityIdAndObjectId(entityId, objectId);
	}

	@Override
	public Collection<ApprovalEventLog> findByEntityIdAndObjectIdAndApprovalAction(String entityId, String objectId,
			ApprovalAction approvalAction, Date lastCreatedDate) {
		if (StringUtils.isEmpty(entityId) || StringUtils.isEmpty(objectId) || approvalAction == null) {
			return Collections.emptyList();
		}
		
		return approvalEventLogRepository.findByEntityIdAndObjectIdAndApprovalAction(entityId, objectId, approvalAction, lastCreatedDate);
	}
}