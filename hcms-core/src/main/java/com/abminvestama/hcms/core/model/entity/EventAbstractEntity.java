package com.abminvestama.hcms.core.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.domain.Persistable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 * Base entity model for 'Event Sourcing'. All <strong>Event Storing</strong> entities should be inherited from this abstract class, 
 * 
 */
@MappedSuperclass
public abstract class EventAbstractEntity implements Persistable<String> {

	private static final long serialVersionUID = 445900791778923712L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	protected String id;
	
	@Version
	@Column(name = "opt_lock", nullable = false)
	protected long version = 0l;
	
	@Column(name = "created_at", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@JsonProperty("created_at")
	protected Date createdAt;
	
	@Column(name = "created_by_id", nullable = false)
	@JsonProperty("created_by_id")
	protected String createdById;
	
	protected EventAbstractEntity() {}
	
	@Override
	public String getId() {
		return id;
	}
	
	public void setId(final String id) {
		this.id = id;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}
	
	public String getCreatedById() {
		return createdById;
	}
	
	public void setCreatedById(String createdById) {
		this.createdById = createdById;
	}
	
	@Override
	public boolean isNew() {
		return this.id == null;
	}
	
	@PrePersist
	final void prePersist() {
		this.createdAt = new Date();
	}
	
	@Override
	public String toString() {
		return String.format("Entity of type %s with id: %s", this.getClass().getName(), getId());
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (!getClass().equals(obj.getClass())) {
			return false;
		}
		
		final EventAbstractEntity entity = (EventAbstractEntity) obj;
		return this.id == null ? false : this.id.equalsIgnoreCase(entity.id);
	}
	
	@Override
	public int hashCode() {
		int hashCode = 17;
		hashCode += (this.id == null ? 0 : this.id.hashCode() * 31);
		return hashCode;
	}
}