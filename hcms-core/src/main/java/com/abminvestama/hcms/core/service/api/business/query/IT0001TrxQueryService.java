package com.abminvestama.hcms.core.service.api.business.query;

import com.abminvestama.hcms.core.model.entity.IT0001Trx;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface IT0001TrxQueryService extends DatabaseQueryService<IT0001Trx, String> {
}