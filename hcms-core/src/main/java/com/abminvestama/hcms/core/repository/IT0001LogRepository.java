package com.abminvestama.hcms.core.repository;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.abminvestama.hcms.core.model.entity.IT0001Log;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface IT0001LogRepository extends PagingAndSortingRepository<IT0001Log, String> {

	/*
	@Query("FROM IT0001_LOG it0001_log WHERE it0001_log.pernr = :pernr AND it0001_log.endda = :endda AND it0001_log.begda = :begda")
	Page<IT0001Log> findByCompositeKeys(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda, Pageable pageRequest);
	*/
	Page<IT0001Log> findByPernrAndEnddaAndBegda(Long pernr, Date endda, Date begda, Pageable pageRequest);
	
	Page<IT0001Log> findAll(Pageable pageRequest);
}