package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findExpenseQuotasByBukrs method, used for expense quota</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface ZmedEmpquotastfRepository extends CrudRepository<ZmedEmpquotastf, String> {
	
	//ZmedEmpquotastf findByEntityName(String Name);	

	/*ZmedEmpquotastf findByPersk(String persk);*/
	
	@Query("FROM ZmedEmpquotastf zmed_empquotastf WHERE zmed_empquotastf.bukrs.bukrs = :bukrs AND zmed_empquotastf.stell = :stell AND zmed_empquotastf.persg = :persg AND zmed_empquotastf.persk = :persk AND zmed_empquotastf.kodequo = :kodequo AND zmed_empquotastf.id.datab = :datab AND zmed_empquotastf.id.datbi = :datbi")
	ZmedEmpquotastf findOneByCompositeKey(@Param("stell") long stell,
			@Param("bukrs") String bukrs,
			@Param("persg") String persg, 
			@Param("persk") String persk,
			@Param("kodequo") String kodequo,
			@Param("datab") @Temporal(TemporalType.DATE) Date datab,
			@Param("datbi") @Temporal(TemporalType.DATE) Date datbi);
	

	@Query("FROM ZmedEmpquotastf zmed_empquotastf WHERE zmed_empquotastf.bukrs.bukrs = :bukrs AND zmed_empquotastf.stell = :stell AND zmed_empquotastf.persg = :persg AND zmed_empquotastf.persk = :persk AND zmed_empquotastf.id.datab = :datab AND zmed_empquotastf.id.datbi = :datbi")
	Collection<ZmedEmpquotastf> findQuotas(@Param("stell") long stell,
			@Param("bukrs") String bukrs,
			@Param("persg") String persg, 
			@Param("persk") String persk,
			@Param("datab") @Temporal(TemporalType.DATE) Date datab,
			@Param("datbi") @Temporal(TemporalType.DATE) Date datbi);
	

	//@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.id.endda = :endda AND it0002.id.begda = :begda")
	//IT0002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
	//		@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM ZmedEmpquotastf expense_quotas WHERE expense_quotas.bukrs.bukrs = :bukrs AND expense_quotas.persg = :persg AND expense_quotas.persk = :persk " +
	    "AND :searchDate >= expense_quotas.id.datab AND :searchDate <= expense_quotas.id.datbi")
	Collection<ZmedEmpquotastf> findExpenseQuotasByBukrs(
			@Param("bukrs") String bukrs,
			@Param("persg") String persg, 
			@Param("persk") String persk,
			@Param("searchDate") @Temporal(TemporalType.DATE) Date searchDate);
	
}
