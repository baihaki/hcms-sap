package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.BNKA;
import com.abminvestama.hcms.core.repository.BNKARepository;
import com.abminvestama.hcms.core.service.api.business.query.BNKAQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add fetchByKeywordWithPaging method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add fetchAllWithPaging method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("bnkaQueryService")
@Transactional(readOnly = true)
public class BNKAQueryServiceImpl implements BNKAQueryService {
	
	private BNKARepository bnkaRepository;
	
	@Autowired
	BNKAQueryServiceImpl(BNKARepository bnkaRepository) {
		this.bnkaRepository = bnkaRepository;
	}

	@Override
	public Optional<BNKA> findById(Optional<String> id) throws Exception {
		return id.map(pk -> {
			return Optional.ofNullable(bnkaRepository.findOne(pk));
		}).orElse(Optional.empty());
	}

	@Override
	public Optional<Collection<BNKA>> fetchAll() {
		Optional<Iterable<BNKA>> bnkaIterable = Optional.ofNullable(bnkaRepository.findAll());		
		
		List<BNKA> listOfBNKA = new ArrayList<>();
		
		return bnkaIterable.map(bnkaIterators -> {
			bnkaIterators.forEach(bnka -> {
				listOfBNKA.add(bnka);
			});
			
			return listOfBNKA;
		});
	}

	@Override
	public Page<BNKA> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return bnkaRepository.findAll(pageRequest);
	}
	
	@Override
	public Page<BNKA> fetchByKeywordWithPaging(int pageNumber, String keywords) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		int total = bnkaRepository.findByKeywordsCount(keywords);
		List<BNKA> listOfBNKA = bnkaRepository.findByKeywords(keywords, pageRequest.getPageSize(), pageRequest.getOffset()).
				stream().collect(Collectors.toList());
		Page<BNKA> page = new PageImpl<BNKA>(listOfBNKA, pageRequest, total);
		
		return page;
	}
}