package com.abminvestama.hcms.core.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.SubT706B1;
import com.abminvestama.hcms.core.model.entity.SubT706B1Key;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface SubT706B1Repository extends CrudRepository<SubT706B1, SubT706B1Key> {
	
	@Query("FROM SubT706B1 sub WHERE sub.id.morei = :morei AND sub.id.spkzl like :spkzl_prefix ORDER BY sub.subSptxt")
	Collection<SubT706B1> findByMoreiAndSpkzlPrefix(@Param("morei") String morei, @Param("spkzl_prefix") String spkzlPrefix);
	
	@Query("FROM SubT706B1 sub WHERE sub.id.morei = :morei AND sub.id.spkzl like :spkzl_prefix AND sub.subForm = :form ORDER BY sub.subSptxt")
	Collection<SubT706B1> findByMoreiAndSpkzlPrefixAndForm(@Param("morei") String morei, @Param("spkzl_prefix") String spkzlPrefix, @Param("form") Short form);
}