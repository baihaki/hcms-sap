package com.abminvestama.hcms.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.abminvestama.hcms.core.model.entity.T702N;

public interface T702NRepository extends CrudRepository<T702N, String> {
	
	Page<T702N> findAll(Pageable pageRequest);
}