package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.IT0077;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.IT0077Repository;
import com.abminvestama.hcms.core.service.api.business.command.IT0077CommandService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("it0077CommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class IT0077CommandServiceImpl implements IT0077CommandService {

	private IT0077Repository it0077Repository;
	
	@Autowired
	IT0077CommandServiceImpl(IT0077Repository it0077Repository) {
		this.it0077Repository = it0077Repository;
	}
	
	@Override
	public Optional<IT0077> save(IT0077 entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setUname(user.getId());
		}
		
		try {
			return Optional.ofNullable(it0077Repository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist IT0077 (Additional Personal Data).Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(IT0077 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete on this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(IT0077 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}
}