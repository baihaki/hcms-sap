package com.abminvestama.hcms.core.repository;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.abminvestama.hcms.core.model.entity.T517T;
import com.abminvestama.hcms.core.model.entity.T517Z;
import com.abminvestama.hcms.core.model.entity.T517ZKey;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface T517ZRepository extends CrudRepository<T517Z, T517ZKey> {
	
	Page<T517Z> findAll(Pageable pageRequest);
	Collection<T517Z> findBySlart(T517T slart);
}