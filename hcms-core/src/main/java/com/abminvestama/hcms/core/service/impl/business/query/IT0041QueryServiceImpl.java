package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.IT0041;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.repository.IT0041Repository;
import com.abminvestama.hcms.core.service.api.business.query.IT0041QueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("it0041QueryService")
@Transactional(readOnly = true)
public class IT0041QueryServiceImpl implements IT0041QueryService {

	private IT0041Repository it0041Repository;
	
	@Autowired
	IT0041QueryServiceImpl(IT0041Repository it0041Repository) {
		this.it0041Repository = it0041Repository;
	}
	
	@Override
	public Optional<IT0041> findById(Optional<ITCompositeKeysNoSubtype> id) throws Exception {
		throw new NoSuchMethodException("Method not implemented. Please use method findOneByCompositeKeys instead.");		
	}

	@Override
	public Optional<Collection<IT0041>> fetchAll() {
		Optional<Iterable<IT0041>> it0041Iterates = Optional.ofNullable(it0041Repository.findAll());
		
		List<IT0041> listOfIT0041 = new ArrayList<>();
		
		return it0041Iterates.map(it0041Iterable -> {			
			it0041Iterable.forEach(it0041 -> {
				listOfIT0041.add(it0041);
			});
			return listOfIT0041;
		});
	}

	@Override
	public Optional<IT0041> findOneByCompositeKey(Long pernr, Date endda, Date begda) {
		if (pernr == null || endda == null || begda == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(it0041Repository.findOneByCompositeKey(pernr, endda, begda));
	}

	@Override
	public Collection<IT0041> findByPernr(Long pernr) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT0041>> bunchOfIT0041 
			= Optional.ofNullable(it0041Repository.findByPernr(pernr));
		
		return (bunchOfIT0041.isPresent()
				? bunchOfIT0041.get().stream().collect(Collectors.toList())
				: Collections.emptyList());		
	}
}