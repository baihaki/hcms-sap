package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;

public interface ZmedMedtypeRepository extends CrudRepository<ZmedMedtype, String> {
	
	//ZmedMedtype findByEntityName(String Name);	

	/*ZmedMedtype findByKodemed(String kodemed);	*/
	@Query("FROM ZmedMedtype zmed_medtype WHERE zmed_medtype.bukrs.bukrs = :bukrs AND zmed_medtype.kodemed = :kodemed AND zmed_medtype.id.datab = :datab AND zmed_medtype.id.datbi = :datbi")
	ZmedMedtype findOneByCompositeKey(@Param("bukrs") String bukrs, 
			@Param("kodemed") String kodemed,
			@Param("datab") @Temporal(TemporalType.DATE) Date datab,
			@Param("datbi") @Temporal(TemporalType.DATE) Date datbi);
	
	@Query("FROM ZmedMedtype zmed_medtype WHERE zmed_medtype.bukrs.bukrs = :bukrs AND zmed_medtype.id.datbi = :datbi")
	Collection<ZmedMedtype> findByBukrs(@Param("bukrs") String bukrs, @Param("datbi")  @Temporal(TemporalType.DATE) Date datbi);
	

	//@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.id.endda = :endda AND it0002.id.begda = :begda")
	//IT0002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
	//		@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	
}