package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Date;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.VehicleType;
import com.abminvestama.hcms.core.repository.VehicleTypeRepository;
import com.abminvestama.hcms.core.service.api.business.command.VehicleTypeCommandService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("vehicleTypeCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class VehicleTypeCommandServiceImpl implements VehicleTypeCommandService {

	private VehicleTypeRepository vehicleTypeRepository;
	
	@Autowired
	void setVehicleTypeRepository(VehicleTypeRepository vehicleTypeRepository) {
		this.vehicleTypeRepository = vehicleTypeRepository;
	}
	
	@Override
	public Optional<VehicleType> save(VehicleType entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setUpdatedById(user.getId());
		} else {
			entity.setUpdatedById("superadmin");
		}
		try {
			return Optional.ofNullable(vehicleTypeRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist Vehicle Type (cfg_vehicle_types).Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(VehicleType entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setDeletedById(user.getId());
		}
		try {
			entity.setDeletedAt(new Date());
			return vehicleTypeRepository.save(entity) != null ? true : false;
		} catch (Exception e) {
			throw new CannotPersistException("Cannot delete Vehicle Type (cfg_vehicle_types).Reason=" + e.getMessage());
		}
	}

	@Override
	public boolean restore(VehicleType entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setUpdatedById(user.getId());
		}
		try {
			entity.setDeletedAt(null);
			entity.setDeletedById(null);
			return vehicleTypeRepository.save(entity) != null ? true : false;
		} catch (Exception e) {
			throw new CannotPersistException("Cannot restore Vehicle Type (cfg_vehicle_types).Reason=" + e.getMessage());
		}
	}
}