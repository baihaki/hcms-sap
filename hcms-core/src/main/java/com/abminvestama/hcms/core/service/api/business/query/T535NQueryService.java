package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.T535N;
import com.abminvestama.hcms.core.model.entity.T535NKey;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add fetchByArtWithPaging method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface T535NQueryService extends DatabaseQueryService<T535N, T535NKey>, DatabasePaginationQueryService {
	
	@NotNull
	Optional<T535N> findOneByCompositeKey(String art, String title);

	Page<T535N> fetchByArtWithPaging(String art, int pageNumber);
}