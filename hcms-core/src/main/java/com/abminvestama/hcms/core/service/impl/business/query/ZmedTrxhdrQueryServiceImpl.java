package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyTrxhdr;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;
import com.abminvestama.hcms.core.repository.ZmedTrxhdrRepository;
import com.abminvestama.hcms.core.service.api.business.query.ZmedTrxhdrQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.5
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add parameter "notStatus" to filter by paymentStatus or not (optional)</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessesWithPaging, findByPernrAndProcessesWithPaging</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: findByPernrAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add methods: updateZclmno</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("zmedTrxhdrQueryService")
@Transactional(readOnly = true)
public class ZmedTrxhdrQueryServiceImpl implements ZmedTrxhdrQueryService {

	private ZmedTrxhdrRepository zmedTrxhdrRepository;
	
	@Autowired
	ZmedTrxhdrQueryServiceImpl(ZmedTrxhdrRepository zmedTrxhdrRepository) {
		this.zmedTrxhdrRepository = zmedTrxhdrRepository;
	}
	
	//@Override
	//public Optional<ZmedTrxhdr> findById(Optional<String> id) throws Exception {
	//	return id.map(pk -> zmedTrxhdrRepository.findOne(pk));
	//}

	@Override
	public Optional<Collection<ZmedTrxhdr>> fetchAll() {
		List<ZmedTrxhdr> entities = new ArrayList<>();
		Optional<Iterable<ZmedTrxhdr>> entityIterable = Optional.ofNullable(zmedTrxhdrRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}

	@Override
	public Optional<ZmedTrxhdr> findById(Optional<ZmedCompositeKeyTrxhdr> id)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<ZmedTrxhdr> findOneByCompositeKey(String bukrs,
			String zclmno) {
		if (bukrs == null || zclmno == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(zmedTrxhdrRepository.findOneByCompositeKey(bukrs, zclmno));
	}

	@Override
	public Collection<ZmedTrxhdr> findByPernr(Long pernr) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		Optional<Collection<ZmedTrxhdr>> bunchOfZmed
			= Optional.ofNullable(zmedTrxhdrRepository.findByPernr(pernr));
		
		return (bunchOfZmed.isPresent()
				? bunchOfZmed.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

	@Override
	public Collection<ZmedTrxhdr> findByBukrs(String bukrs) {
		if (bukrs == null) {
			return Collections.emptyList();
		}
		Optional<Collection<ZmedTrxhdr>> bunchOfZmed
			= Optional.ofNullable(zmedTrxhdrRepository.findByBukrs(bukrs));
		
		return (bunchOfZmed.isPresent()
				? bunchOfZmed.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

	@Override
	public Collection<ZmedTrxhdr> findByClmno(String clmno) {
		if (clmno == null) {
			return Collections.emptyList();
		}
		Optional<Collection<ZmedTrxhdr>> bunchOfZmed
			= Optional.ofNullable(zmedTrxhdrRepository.findByClmno(clmno));
		
		return (bunchOfZmed.isPresent()
				? bunchOfZmed.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

	/*@Override
	public Optional<Collection<ZmedTrxhdr>> findByEntityName(final Long entityName) {
		return Optional.ofNullable(zmedTrxhdrRepository.findByPernr(entityName));
	}*/

	@Override
	public Page<ZmedTrxhdr> findByBukrsAndProcessWithPaging(String bukrs, String process, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (StringUtils.isEmpty(process)) ? zmedTrxhdrRepository.findByBukrsWithPaging(bukrs, notStatus, pageRequest) 
				: zmedTrxhdrRepository.findByBukrsAndProcessWithPaging(bukrs, process, notStatus, pageRequest);
	}

	@Override
	public Page<ZmedTrxhdr> findByPernrAndProcessWithPaging(long pernr, String process, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (StringUtils.isEmpty(process)) ? zmedTrxhdrRepository.findByPernrWithPaging(pernr, notStatus, pageRequest) 
				: zmedTrxhdrRepository.findByPernrAndProcessWithPaging(pernr, process, notStatus, pageRequest);
	}

	@Override
	public Page<ZmedTrxhdr> findByBukrsAndProcessesWithPaging(String bukrs, String[] processes, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (processes == null) ? zmedTrxhdrRepository.findByBukrsWithPaging(bukrs, notStatus, pageRequest) 
				: zmedTrxhdrRepository.findByBukrsAndProcessesWithPaging(bukrs, processes, notStatus, pageRequest);
	}

	@Override
	public Page<ZmedTrxhdr> findByPernrAndProcessesWithPaging(long pernr, String[] processes, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (processes == null) ? zmedTrxhdrRepository.findByPernrWithPaging(pernr, notStatus, pageRequest) 
				: zmedTrxhdrRepository.findByPernrAndProcessesWithPaging(pernr, processes, notStatus, pageRequest);
	}

}
