package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Entity
@Table(name = "it2002")
public class IT2002 extends SAPAbstractEntity<ITCompositeKeys> {

	private static final long serialVersionUID = 631708870034781751L;
	
	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "infty", referencedColumnName = "infty", insertable = false, updatable = false),
		@JoinColumn(name = "subty", referencedColumnName = "subty", insertable = false, updatable = false)
	})
	private T591S subty;
	
	public IT2002() {}
	
	public IT2002(ITCompositeKeys id) {
		this();
		this.id = id;
	}
	
	@Column(name = "seqnr")
	private Long seqnr;
	
	@ManyToOne
	@JoinColumn(name = "awart", referencedColumnName = "awart")
	private T554T awart;
	
	@Column(name = "abwtg", precision = 10, scale = 2)
	private Double abwtg;
	
	@Column(name = "stdaz", precision = 10, scale = 2)
	private Double stdaz;
	
	@Column(name = "kaltg", precision = 10, scale = 2)
	private Double kaltg;
	
	/**
	 * GET Employee ID/SSN.
	 * 
	 * @return
	 */
	public Long getPernr() {
		return pernr;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	public T591S getSubty() {
		return subty;
	}
	
	/**
	 * GET infotype Record no.
	 * 
	 * @return
	 */
	public Long getSeqnr() {
		return seqnr;
	}
	
	public void setSeqnr(Long seqnr) {
		this.seqnr = seqnr;
	}
	
	/**
	 * GET Attendance Type.
	 * 
	 * @return
	 */
	public T554T getAwart() {
		return awart;
	}
	
	public void setAwart(T554T awart) {
		this.awart = awart;
	}
	
	/**
	 * GET Attendance Days.
	 * 
	 * @return
	 */
	public Double getAbwtg() {
		return abwtg;
	}
	
	public void setAbwtg(Double abwtg) {
		this.abwtg = abwtg;
	}
	
	/**
	 * GET Attendance Hours.
	 * 
	 * @return
	 */
	public Double getStdaz() {
		return stdaz;
	}
	
	public void setStdaz(Double stdaz) {
		this.stdaz = stdaz;
	}
	
	/**
	 * GET Calendar Days.
	 * 
	 * @return
	 */
	public Double getKaltg() {
		return kaltg;
	}
	
	public void setKaltg(Double kaltg) {
		this.kaltg = kaltg;
	}	
}