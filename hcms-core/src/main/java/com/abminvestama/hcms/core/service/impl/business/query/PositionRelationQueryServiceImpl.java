package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.PositionRelation;
import com.abminvestama.hcms.core.repository.PositionRelationRepository;
import com.abminvestama.hcms.core.service.api.business.query.PositionRelationQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("positionRelationQueryService")
@Transactional(readOnly = true)
public class PositionRelationQueryServiceImpl implements PositionRelationQueryService {

	private PositionRelationRepository positionRelationRepository;

	@Autowired
	PositionRelationQueryServiceImpl(PositionRelationRepository positionRelationRepository) {
		this.positionRelationRepository = positionRelationRepository;
	}
	
	@Override
	public Optional<PositionRelation> findById(Optional<String> id) throws Exception {
		return id.map(pk -> positionRelationRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<PositionRelation>> fetchAll() {
		List<PositionRelation> positionRelations = new ArrayList<>();
		positionRelationRepository.findAll().forEach(pr -> {
			positionRelations.add(pr);
		});
		
		return Optional.of(positionRelations);
	}

	@Override
	public Page<PositionRelation> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return positionRelationRepository.findAll(pageRequest);
	}

	@Override
	public Page<PositionRelation> findByFromPositionId(String fromPositionId, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return positionRelationRepository.findByFromPositionId(fromPositionId, pageRequest);
	}

	@Override
	public Page<PositionRelation> findByToPositionId(String toPositionId, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return positionRelationRepository.findByToPositionId(toPositionId, pageRequest);
	}

	@Override
	public Collection<PositionRelation> findByFromPositionid(String fromPositionId) {
		return positionRelationRepository.findByFromPositionId(fromPositionId);
	}

	@Override
	public Collection<PositionRelation> findByToPositionId(String toPositionId) {
		return positionRelationRepository.findByToPositionId(toPositionId);
	}

	@Override
	public Collection<PositionRelation> findByFromPositionAndToPosition(String fromPositionId, String toPositionId) {
		if (StringUtils.isEmpty(fromPositionId) || StringUtils.isEmpty(toPositionId)) {
			return new ArrayList<>();
		}
		return positionRelationRepository.findByFromPositionIdAndToPositionId(fromPositionId, toPositionId);
	}

	@Override
	public Collection<PositionRelation> findByFromPositionAndToPositionAndRelationType(String fromPositionId,
			String toPositionId, String relationType) {
		if (StringUtils.isEmpty(fromPositionId) || StringUtils.isEmpty(toPositionId) || StringUtils.isEmpty(relationType)) {
			return new ArrayList<>();
		}
		
		return positionRelationRepository.findByFromPositionIdAndToPositionIdAndRelationType(fromPositionId, toPositionId, relationType);
	}
}