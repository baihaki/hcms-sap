package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.IT2002;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.IT2002Repository;
import com.abminvestama.hcms.core.service.api.business.command.IT2002CommandService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.00
 * @since 1.0.0
 *
 */
@Service("it2002CommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class IT2002CommandServiceImpl implements IT2002CommandService {

	private IT2002Repository it2002Repository;
	
	@Autowired
	IT2002CommandServiceImpl(IT2002Repository it2002Repository) {
		this.it2002Repository = it2002Repository;
	}
	
	@Override
	public Optional<IT2002> save(IT2002 entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setUname(user.getId());
		}
		
		try {
			return Optional.ofNullable(it2002Repository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist IT2002 (Attendances).Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(IT2002 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(IT2002 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");				
	}
}