package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Date;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.CaGlAccount;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.CaGlAccountRepository;
import com.abminvestama.hcms.core.service.api.business.command.CaGlAccountCommandService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("caGlAccountCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class CaGlAccountCommandServiceImpl implements CaGlAccountCommandService {

	private CaGlAccountRepository caGlAccountRepository;
	
	@Autowired
	CaGlAccountCommandServiceImpl(CaGlAccountRepository caGlAccountRepository) {
		this.caGlAccountRepository = caGlAccountRepository;
	}
	
	@Override
	public Optional<CaGlAccount> save(CaGlAccount entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			//entity.setCreatedById(user.getId());
		}
		
		try {
			return Optional.ofNullable(caGlAccountRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist GL Account. Reason: " + e.getMessage());
		}				
	}

	@Override
	public boolean delete(CaGlAccount entity, User user)
			throws NoSuchMethodException, ConstraintViolationException {
		if (user == null) {
			throw new ConstraintViolationException("Cannot delete GL Account. Reason: Must provide current user ID!", null);
		}
		
		try {
			entity.setEndda(new Date());
			//entity.setDeletedById(user.getId());
			
			caGlAccountRepository.save(entity);
		} catch (Exception e) {
			throw new ConstraintViolationException("Cannot delete GL Account. Reason: " + e.getMessage(), null);
		}
		
		return true;
	}

	@Override
	public boolean restore(CaGlAccount entity, User user)
			throws NoSuchMethodException, ConstraintViolationException {
		if (user == null) {
			throw new ConstraintViolationException("Cannot restore GL Account. Reason: Must provide current User ID!", null);
		}
		
		try {
			entity.setEndda(CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31"));
			//entity.setDeletedById(null);

			//entity.setUpdatedById(user.getId());
			caGlAccountRepository.save(entity);
		} catch (Exception e) {
			throw new ConstraintViolationException("Cannot restore GL Account. Reason: " + e.getMessage(), null);
		}
		
		return true;
	}
}