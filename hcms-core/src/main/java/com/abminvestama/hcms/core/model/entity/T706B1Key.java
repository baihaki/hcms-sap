package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;

@Embeddable
@Immutable
public class T706B1Key implements Serializable {

	private static final long serialVersionUID = 2987013141063448484L;
	
	/*@Column(name = "morei", nullable = false, insertable = false, updatable = false)
	private String morei;
	
	@Column(name = "spkzl", nullable = false, insertable = false, updatable = false)
	private String spkzl;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "endda", nullable = false, insertable = false, updatable = false)
	private Date endda;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "begda", nullable = false, insertable = false, updatable = false)
	private Date begda;
	*/

	private String morei;
	private String spkzl;
	private Date endda;
	private Date begda;
	
	public T706B1Key() {}
	
	public T706B1Key(String morei, String spkzl, Date endda, Date begda) {
		this.morei = morei;
		this.spkzl = spkzl;
		this.begda = begda;
		this.endda = endda;
	}
	
	public String getMorei() {
		return morei;
	}
	
	public String getSpkzl() {
		return spkzl;
	}
	
	public Date getEndda() {
		return endda;
	}
	
	public Date getBegda() {
		return begda;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final T706B1Key key = (T706B1Key) o;
		
		if (key.getMorei() != null ? !key.getMorei().equalsIgnoreCase(morei) : morei != null) {
			return false;
		}
		
		if (key.getSpkzl() != null ? !key.getSpkzl().equalsIgnoreCase(spkzl) : spkzl != null) {
			return false;
		}
		
		if (key.getEndda() != null ? !key.getEndda().equals(endda) : endda != null) {
			return false;
		}
		
		if (key.getBegda() != null ? !key.getBegda().equals(begda) : begda != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.morei != null ? this.morei.hashCode() : 0;
		result = result * 31 + (this.spkzl != null ? this.spkzl.hashCode() : 0);
		result = result * 31 + (this.endda != null ? this.endda.hashCode() : 0);
		result = result * 31 + (this.begda != null ? this.begda.hashCode() : 0);
		return result;
	}
}