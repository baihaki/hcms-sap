package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;



@Embeddable
@Immutable
public class ZmedCompositeKeyMedtype implements Serializable {
	private static final long serialVersionUID = 5385850040856914239L;

	private String bukrs;
	private String kodemed;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datab", nullable = false)
	private Date datab;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datbi", nullable = false)
	private Date datbi;
	
	public ZmedCompositeKeyMedtype() {}
	
	public ZmedCompositeKeyMedtype(String bukrs, String kodemed, Date datab, Date datbi) {
		this.bukrs = bukrs;
		this.kodemed = kodemed;
		this.datab = datab;
		this.datbi = datbi;
	}
	
	public String getBukrs() {
		return bukrs;
	}

	public String getKodemed() {
		return kodemed;
	}

	public Date getDatab() {
		return datab;
	}

	public Date getDatbi() {
		return datbi;
	}

	
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final ZmedCompositeKeyMedtype key = (ZmedCompositeKeyMedtype) o;
		
		if (key.getBukrs() != null ? key.getBukrs() != (bukrs != null ? bukrs: "") : bukrs != null) {
			return false;
		}
		
		if (key.getKodemed() != null ? key.getKodemed() != (kodemed != null ? kodemed: "") : kodemed != null) {
			return false;
		}
				
		if (key.getDatab() != null ? (key.getDatab().compareTo(datab != null ? datab : new Date()) != 0) : datab != null) {
			return false;
		}
		
		if (key.getDatbi() != null ? (key.getDatbi().compareTo(datbi != null ? datbi : new Date()) != 0) : datbi != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.bukrs != null ? this.bukrs.hashCode() : 0;
		result = result * 31 + (this.kodemed != null ? this.kodemed.hashCode() : 0);
		result = result * 31 + (this.datab != null ? this.datab.hashCode() : 0);
		result = result * 31 + (this.datbi != null ? this.datbi.hashCode() : 0);
		return result;
	}
}
