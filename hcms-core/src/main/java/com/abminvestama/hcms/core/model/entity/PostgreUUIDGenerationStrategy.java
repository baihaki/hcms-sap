package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.Statement;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 * 
 * Custom Identifier Generator (First 13 UUID characters).
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public class PostgreUUIDGenerationStrategy implements IdentifierGenerator {
	
	@Override
	public Serializable generate(SessionImplementor session, Object object) throws HibernateException {
    	String id = null;
    	try {
            Statement statement = session.connection().createStatement();
            ResultSet resultSet = statement.executeQuery("select left(uuid_generate_v4()::text, 13)");
            while (resultSet.next()) {
            	id = (String) resultSet.getObject(1);
            }
        } catch (Exception e) {
        	throw new IllegalArgumentException("Can't fetch a new UUID");
        }
    	return id;
	}
}
