package com.abminvestama.hcms.core.service.helper;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyTrxhdr;
import com.abminvestama.hcms.core.model.entity.PtrvHead;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class PtrvHeadMapper {
	
	private PtrvHead ptrvHead;
	
	private PtrvHeadMapper() {}
	
	public PtrvHeadMapper(Map<String, String> map) {
		if (map == null) {
			new PtrvHeadMapper();
			ptrvHead = new PtrvHead();
		} else {
			ptrvHead = new PtrvHead();
			
			ptrvHead.setPaymentStatus(map.get("paymentstatus"));
			ptrvHead.setPaymentDate(map.get("paymentdate") != null && !map.get("paymentdate").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("paymentdate")) : null);
			ptrvHead.setDocno(map.get("sapdocno"));
			
			ptrvHead.setPaymentStatus2(map.get("paymentstatus2"));
			ptrvHead.setDocno2(map.get("sapdocno2"));
		}		
	}

	public PtrvHead getPtrvHead() {
		return ptrvHead;
	}
	
}
