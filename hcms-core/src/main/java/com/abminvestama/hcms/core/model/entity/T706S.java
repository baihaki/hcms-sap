package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "t706s")
public class T706S extends SAPAbstractEntityZmed<T706SKey> {

	private static final long serialVersionUID = 3669795826077668803L;
	

	public T706S() {}
	
	public T706S(T706SKey id) {
		this();
		this.id = id;
	}
	
	@Column(name = "morei", nullable = false, insertable = false, updatable = false, length = 2)
	private String morei;
	
	@Column(name = "schem", nullable = false, insertable = false, updatable = false, length = 2)
	private String schem;

	@Column(name = "stext", nullable = false, insertable = false, updatable = false)
	private String stext;
	
	public void setMorei(String morei) {
		this.morei = morei;
	}

	public void setSchem(String schem) {
		this.schem = schem;
	}

	public void setStext(String stext) {
		this.stext = stext;
	}

	public String getMorei() {
		return morei;
	}

	public String getSchem() {
		return schem;
	}

	public String getStext() {
		return stext;
	}

	
}