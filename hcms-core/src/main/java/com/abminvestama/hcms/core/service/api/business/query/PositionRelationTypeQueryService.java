package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Optional;

import com.abminvestama.hcms.core.model.entity.PositionRelationType;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface PositionRelationTypeQueryService extends DatabaseQueryService<PositionRelationType, String> {

	Optional<PositionRelationType> findByName(final String name);
}