package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.TrxApprovalProcess;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.TrxApprovalProcessRepository;
import com.abminvestama.hcms.core.service.api.business.command.TrxApprovalProcessCommandService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0
 * @author wijanarko (wijanarko@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("trxApprovalProcessCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class TrxApprovalProcessCommandServiceImpl implements TrxApprovalProcessCommandService {
	private TrxApprovalProcessRepository trxApprovalProcessRepository;
	
	@Autowired
	TrxApprovalProcessCommandServiceImpl(TrxApprovalProcessRepository trxApprovalProcessRepository) {
		this.trxApprovalProcessRepository = trxApprovalProcessRepository;
	}

	@Override
	public Optional<TrxApprovalProcess> save(TrxApprovalProcess entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		try {
			return Optional.ofNullable(trxApprovalProcessRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist TrxApprovalProcess (Transaction Approval Process).Reason=" + e.getMessage());
		}	
	}

	@Override
	public boolean delete(TrxApprovalProcess entity, User user)
			throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(TrxApprovalProcess entity, User user)
			throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}

	@Override
	public boolean deleteList(List<TrxApprovalProcess> entities) {
		try {
			trxApprovalProcessRepository.delete(entities);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
