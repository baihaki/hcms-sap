package com.abminvestama.hcms.core.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.CaStlDtl;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaStlDtlRepository extends PagingAndSortingRepository<CaStlDtl, String> {
	@Query("FROM CaStlDtl ca_stl_dtl WHERE ca_stl_dtl.detailId = :detailId")
	CaStlDtl findByDetailId(@Param("detailId") String detailId);

	@Query("FROM CaStlDtl ca_stl_dtl WHERE ca_stl_dtl.reqNo = :reqNo")
	Collection<CaStlDtl> fetchByReqno(@Param("reqNo") String reqNo);

	@Query("FROM CaStlDtl ca_stl_dtl WHERE ca_stl_dtl.reqNo = :reqNo AND ca_stl_dtl.itemNo = :itemNo")
	CaStlDtl findOneByReqnoAndItemno(@Param("reqNo") String reqNo, @Param("itemNo") int itemNo);
}
