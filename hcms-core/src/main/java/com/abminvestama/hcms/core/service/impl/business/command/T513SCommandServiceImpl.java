package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.T513S;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.T513SRepository;
import com.abminvestama.hcms.core.service.api.business.command.T513SCommandService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("t513sCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class T513SCommandServiceImpl implements T513SCommandService {

	private T513SRepository t513sRepository;
	
	@Autowired
	T513SCommandServiceImpl(T513SRepository t513sRepository) {
		this.t513sRepository = t513sRepository;
	}
	
	@Override
	public Optional<T513S> save(T513S entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		try {
			return Optional.ofNullable(t513sRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist T513S (Job key).Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(T513S entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(T513S entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}
}