package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Map;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.ApprovalEventLog;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.ApprovalEventLogRepository;
import com.abminvestama.hcms.core.service.api.business.command.ApprovalEventLogCommandService;
import com.abminvestama.hcms.core.service.util.MailService;

/**
 * 
 * @since 2.0.0
 * @version 2.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add: Send notification email on Save</td></tr>
 *     <tr><td>2.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("approvalEventLogCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class ApprovalEventLogCommandServiceImpl implements ApprovalEventLogCommandService {

	private ApprovalEventLogRepository approvalEventLogRepository;
	private MailService mailService;
	
	@Autowired
	ApprovalEventLogCommandServiceImpl(ApprovalEventLogRepository approvalEventLogRepository) {
		this.approvalEventLogRepository = approvalEventLogRepository;
	}
	
	@Autowired
	void setMailService(MailService mailService) {
		this.mailService = mailService;
	}
	
	@Override
	public Optional<ApprovalEventLog> save(ApprovalEventLog entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setCreatedById(user.getId());
		}
		
		try {
			Optional<ApprovalEventLog> savedApprovalEventLog = Optional.ofNullable(approvalEventLogRepository.save(entity));
			if (savedApprovalEventLog.isPresent() && entity.isMailNotification() && entity.getSubmitUser() != null) {
				// send email notification to the Approver (HR Admin / Superior)
				/*
			    mailService.sendMail(user.getEmail(), entity.getApprovalAction().toString().concat(" ").
			    		concat(entity.getHcmsEntity().getEntityName()),
			    		"Dear <b>".concat(user.getEmployee().getSname()).concat("</b><br><br>You have succesfully ").
			    		concat(entity.getApprovalAction().toString()).concat(" an ").concat(entity.getHcmsEntity().getEntityName()).
			    		concat(" data.<br><br>Thank You."));*/
				Map<String, String> composeMap = mailService.composeEmail(savedApprovalEventLog.get().getEventLog(), savedApprovalEventLog.get().getSubmitUser());
				if (StringUtils.isNotEmpty(composeMap.get("subject"))) {
					String mailBody = composeMap.get("body").replace("Dear <b>".concat(savedApprovalEventLog.get().getSubmitUser().getEmployee().getSname()),
							"Dear <b>".concat(user.getEmployee().getSname()).concat(" [HR ADMIN]"))
							.replace("Your data request", "The data request")
							.replace(mailService.getMailMasterDataUpdate(),"has been approved by you.");
					if (composeMap.get("footer") != null) {
						// for Admin, exclude the email footer which contains url of the approved/rejected master data transaction 
						mailBody = mailBody.replace(composeMap.get("footer"), StringUtils.EMPTY);
					}
					mailService.sendMail(user.getEmail(), composeMap.get("subject"), mailBody);
				}
			}
			return savedApprovalEventLog;
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist ApprovalEventLog.Reason=" + e.getMessage());
		}						
	}

	@Override
	public boolean delete(ApprovalEventLog entity, User user)
			throws NoSuchMethodException, ConstraintViolationException {
		throw new NoSuchMethodException("Delete is prohibited. Cannot delete an ApprovalEventLog!");		
	}

	@Override
	public boolean restore(ApprovalEventLog entity, User user)
			throws NoSuchMethodException, ConstraintViolationException {
		throw new NoSuchMethodException("Invalid Operation.No ApprovalEventLog to be restored!");				
	}

}
