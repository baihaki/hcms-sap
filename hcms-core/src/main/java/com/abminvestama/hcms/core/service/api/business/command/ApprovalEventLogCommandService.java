package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.ApprovalEventLog;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface ApprovalEventLogCommandService extends DatabaseCommandService<ApprovalEventLog> {

}