package com.abminvestama.hcms.core.service.helper;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyTrxhdr;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class ZmedTrxhdrMapper {
	
	private ZmedTrxhdr zmedTrxhdr;
	
	private ZmedTrxhdrMapper() {}
	
	public ZmedTrxhdrMapper(Map<String, String> map) {
		if (map == null) {
			new ZmedTrxhdrMapper();
			zmedTrxhdr = new ZmedTrxhdr();
		} else {
			zmedTrxhdr = new ZmedTrxhdr();
			zmedTrxhdr.setAppamt(map.get("appamt") != null ? Double.valueOf(map.get("appamt")) : 0.00);
			zmedTrxhdr.setAprby(map.get("aprby"));
			zmedTrxhdr.setAprdt(map.get("aprdt") != null && !map.get("aprdt").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("aprdt")) : null);
			zmedTrxhdr.setBelnr(map.get("belnr"));
			zmedTrxhdr.setBilldt(map.get("billdt") != null ? CommonDateFunction.convertDateRequestParameterIntoDate(map.get("billdt")) : null);
			//Optional<T001> bukrs = t001QueryService.findByBukrs(map.get("bukrs"));				
			//zmedTrxhdr.setBukrs(bukrs.isPresent() ? bukrs.get() : null);
			zmedTrxhdr.setClmamt(map.get("clmamt") != null ? Double.valueOf(map.get("clmamt")) : 0.00);
			zmedTrxhdr.setClmby(map.get("clmby"));
			zmedTrxhdr.setClmdt(map.get("clmdt") != null && !map.get("clmdt").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("clmdt")) : null);
			zmedTrxhdr.setClmst(map.get("clmst"));
			zmedTrxhdr.setEname(map.get("ename"));
			zmedTrxhdr.setId(new ZmedCompositeKeyTrxhdr(map.get("bukrs"), map.get("zclmno")));
			zmedTrxhdr.setJamin(map.get("jamin"));
			zmedTrxhdr.setKodemed(map.get("kodemed"));
			zmedTrxhdr.setMedtydesc(map.get("medtydesc"));
			zmedTrxhdr.setNotex(map.get("notex"));
			zmedTrxhdr.setOrgtx(map.get("orgtx"));
			zmedTrxhdr.setPagu1(map.get("pagu1") != null ? Double.valueOf(map.get("pagu1")) : 0.00);
			zmedTrxhdr.setPagu2(map.get("pagu2") != null ? Double.valueOf(map.get("pagu2")) : 0.00);
			zmedTrxhdr.setPagu2byemp(map.get("pagu2byemp") != null ? Double.valueOf(map.get("pagu2byemp")) : 0.00);
			zmedTrxhdr.setPagudent(map.get("pagudent") != null ? Double.valueOf(map.get("pagudent")) : 0.00);
			zmedTrxhdr.setPasien(map.get("pasien"));
			zmedTrxhdr.setPayby(map.get("payby"));
			zmedTrxhdr.setPaydt(map.get("paydt") != null && !map.get("paydt").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("paydt")) : null);
			zmedTrxhdr.setPaymethod(map.get("paymethod"));
			zmedTrxhdr.setPenyakit(map.get("penyakit"));
			Long pernr = map.get("employeenumber") != null ? Long.valueOf(map.get("employeenumber")) : null;
			zmedTrxhdr.setPernr(pernr);
			zmedTrxhdr.setRejamt(map.get("rejamt") != null ? Double.valueOf(map.get("rejamt")) : 0.00);
			zmedTrxhdr.setRevby(map.get("revby"));
			zmedTrxhdr.setRevdt(map.get("revdt") != null && !map.get("revdt").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("revdt")) : null);
			zmedTrxhdr.setTglkeluar(map.get("tglkeluar") != null && !map.get("tglkeluar").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("tglkeluar")) : null);
			zmedTrxhdr.setTglmasuk(map.get("tglmasuk") != null && !map.get("tglmasuk").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("tglmasuk")) : null);
			zmedTrxhdr.setTotamt(map.get("totamt") != null ? Double.valueOf(map.get("totamt")) : 0.00);
			zmedTrxhdr.setZclmno(map.get("zclmno"));
			zmedTrxhdr.setZdivision(map.get("zdivision"));
			
			zmedTrxhdr.setPaymentStatus(map.get("paymentstatus"));
			zmedTrxhdr.setPaymentDate(map.get("paymentdate") != null && !map.get("paymentdate").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("paymentdate")) : null);
			zmedTrxhdr.setDocno(map.get("sapdocno"));
			
			zmedTrxhdr.setPaymentStatus2(map.get("paymentstatus2"));
			zmedTrxhdr.setDocno2(map.get("sapdocno2"));
		}		
	}

	public ZmedTrxhdr getZmedTrxhdr() {
		return zmedTrxhdr;
	}
	
}
