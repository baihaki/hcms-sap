package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.IT0242;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0242CommandService extends DatabaseCommandService<IT0242> {
}