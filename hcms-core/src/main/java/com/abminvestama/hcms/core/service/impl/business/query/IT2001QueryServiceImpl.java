package com.abminvestama.hcms.core.service.impl.business.query;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.IT2001;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.repository.IT2001Repository;
import com.abminvestama.hcms.core.service.api.business.query.IT2001QueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.5
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add methods: findByPernrAndProcessWithPaging, findByPernrAndProcessesWithPaging with additional parameters</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessesWithPaging, findByPernrAndProcessesWithPaging</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: findByPernrAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add method: findOneByCompositeKeyWithInfotype, findByPernrWithInfotype, findByPernrAndSubtyWithInfotype</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("it2001QueryService")
@Transactional(readOnly = true)
public class IT2001QueryServiceImpl implements IT2001QueryService {

	private IT2001Repository it2001Repository;

	@PersistenceContext(type = PersistenceContextType.TRANSACTION)	
	private EntityManager em;
	
	String queryFindByPernr = "SELECT * FROM it2001 WHERE it2001.pernr = :pernr "; 
	String orderBy = " ORDER BY it2001.created_at DESC, it2001.endda DESC, it2001.begda DESC";
	String limitOffset = " LIMIT :limit OFFSET :offset";
	
	@Autowired
	IT2001QueryServiceImpl(IT2001Repository it2001Repository) {
		this.it2001Repository = it2001Repository;
	}
	
	@Override
	public Optional<IT2001> findById(Optional<ITCompositeKeys> id) throws Exception {
		throw new NoSuchMethodException("Method not implemented. Please use method findOneByCompositeKeys instead.");
	}

	@Override
	public Optional<Collection<IT2001>> fetchAll() {
		Optional<Iterable<IT2001>> it2001Iterates = Optional.ofNullable(it2001Repository.findAll());
		if (!it2001Iterates.isPresent()) {
			return Optional.empty();
		}
		
		List<IT2001> listOfIT2001 = new ArrayList<>();
		it2001Iterates.map(it2001Iterable -> {			
			it2001Iterable.forEach(it2001 -> {
				listOfIT2001.add(it2001);
			});
			return listOfIT2001;
		});
		
		return Optional.of(listOfIT2001);
	}

	@Override
	public Optional<IT2001> findOneByCompositeKey(Long pernr, String subty, Date endda, Date begda) {
		if (pernr == null || StringUtils.isBlank(subty) || endda == null || begda == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(it2001Repository.findOneByCompositeKey(pernr, SAPInfoType.ABSENCES.infoType(), subty, endda, begda));
	}

	@Override
	public Optional<IT2001> findOneByCompositeKeyWithInfotype(Long pernr, String infty, String subty, Date endda, Date begda) {
		if (pernr == null || StringUtils.isBlank(infty) || StringUtils.isBlank(subty) || endda == null || begda == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(it2001Repository.findOneByCompositeKey(pernr, infty, subty, endda, begda));
	}

	@Override
	public Collection<IT2001> findByPernr(Long pernr) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT2001>> bunchOfIT2001
			= Optional.ofNullable(it2001Repository.findByPernr(pernr, SAPInfoType.ABSENCES.infoType()));
		
		return (bunchOfIT2001.isPresent()
				? bunchOfIT2001.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}
	
	@Override
	public Collection<IT2001> findByPernrWithInfotype(Long pernr, String infty) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT2001>> bunchOfIT2001 = Optional.ofNullable(StringUtils.isEmpty(infty) ?
				it2001Repository.findByPernrAllInfotype(pernr) :
				it2001Repository.findByPernr(pernr, infty));
		
		return (bunchOfIT2001.isPresent()
				? bunchOfIT2001.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

	@Override
	public Collection<IT2001> findByPernrAndSubty(Long pernr, String subty) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		if (StringUtils.isBlank(subty)) {
			subty = "0100"; // default to subtype '0100' (i.e. Cuti Tahunan)
		}
		
		Optional<Collection<IT2001>> bunchOfIT2001
			= Optional.ofNullable(it2001Repository.findByPernrAndSubty(pernr, SAPInfoType.ABSENCES.infoType(), subty));

		return (bunchOfIT2001.isPresent() 
					? bunchOfIT2001.get().stream().collect(Collectors.toList())
					: Collections.emptyList());		
	}

	@Override
	public Collection<IT2001> findByPernrAndSubtyWithInfotype(Long pernr, String subty, String infty) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT2001>> bunchOfIT2001 = Optional.empty();
		
		if (StringUtils.isEmpty(infty)) {
			bunchOfIT2001 = Optional.ofNullable(it2001Repository.findByPernrAllInfotype(pernr));
		} else {
			bunchOfIT2001 = Optional.ofNullable(StringUtils.isEmpty(subty) ? it2001Repository.findByPernr(pernr, infty) :
				it2001Repository.findByPernrAndSubty(pernr, infty, subty));
		}

		return (bunchOfIT2001.isPresent() 
					? bunchOfIT2001.get().stream().collect(Collectors.toList())
					: Collections.emptyList());		
	}
	
	@Override
	public Page<IT2001> findByBukrsAndProcessWithPaging(String bukrs, String process, String superiorPosition, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		if (!StringUtils.isEmpty(superiorPosition)) {
			String[] processes = StringUtils.isEmpty(process) ? null : process.split(",");
			int total = it2001Repository.findByBukrsAndProcessesAndSuperiorPositionCount(bukrs, processes, superiorPosition);
			List<IT2001> listOfIT2001 = it2001Repository.findByBukrsAndProcessesAndSuperiorPositionWithPaging(
					bukrs, processes, superiorPosition, pageRequest.getPageSize(), pageRequest.getOffset()).
					stream().collect(Collectors.toList());
			Page<IT2001> page = new PageImpl<IT2001>(listOfIT2001, pageRequest, total);
			return page;
		}
		
		return (StringUtils.isEmpty(process)) ? it2001Repository.findByBukrsWithPaging(bukrs, pageRequest) 
				: it2001Repository.findByBukrsAndProcessWithPaging(bukrs, process, pageRequest);
	}

	@Override
	public Page<IT2001> findByPernrAndProcessWithPaging(long pernr, String process, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (StringUtils.isEmpty(process)) ? it2001Repository.findByPernrWithPaging(pernr, pageRequest) 
				: it2001Repository.findByPernrAndProcessWithPaging(pernr, process, pageRequest);
	}

	@Override
	public Page<IT2001> findByBukrsAndProcessesWithPaging(String bukrs, String[] processes, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (processes == null) ? it2001Repository.findByBukrsWithPaging(bukrs, pageRequest) 
				: it2001Repository.findByBukrsAndProcessesWithPaging(bukrs, processes, pageRequest);
	}

	@Override
	public Page<IT2001> findByPernrAndProcessesWithPaging(long pernr, String[] processes, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (processes == null) ? it2001Repository.findByPernrWithPaging(pernr, pageRequest) 
				: it2001Repository.findByPernrAndProcessesWithPaging(pernr, processes, pageRequest);
	}
	
	private String addFilter(String infty, String subty, String periodBegin, String periodEnd) {
		
		String query = "";
		
		if (!StringUtils.isEmpty(infty)) {
			query = query.concat(" AND it2001.infty = '").concat(infty).concat("'");
		}
		
		if (!StringUtils.isEmpty(subty)) {
			query = query.concat(" AND it2001.subty = '").concat(subty).concat("'");
		}
		
		if (periodBegin != null && periodEnd != null) {
			query = query
						.concat(" AND ( (it2001.begda BETWEEN '").concat(periodBegin).concat("' AND '").concat(periodEnd).concat("') ")
							.concat(" OR ")
								.concat("(it2001.endda BETWEEN '").concat(periodBegin).concat("' AND '").concat(periodEnd).concat("') )");
		}
		else {
			if (periodBegin != null) {
				query = query.concat(" AND it2001.begda >= '").concat(periodBegin).concat("'");
			}
			
			if (periodEnd != null) {
				query = query.concat(" AND it2001.endda <= '").concat(periodEnd).concat("'");
			}
		}
		
		return query;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<IT2001> findByPernrAndProcessWithPaging(long pernr, String process, int pageNumber,
			String infty, String subty, String periodBegin, String periodEnd) {
		
		String query = StringUtils.isEmpty(process) ? queryFindByPernr :
			queryFindByPernr.concat(" AND it2001.process = '").concat(process).concat("'");
		query = query.concat(addFilter(infty, subty, periodBegin, periodEnd));
		String queryCount = query.replace("SELECT * FROM", "SELECT COUNT(*) FROM");
		
		Pageable pageRequest = createPageRequest(pageNumber);
		BigInteger total = BigInteger.ZERO;
		List<IT2001> listOfIT2001 = new ArrayList<IT2001>();
		
		total =  (BigInteger) em.createNativeQuery(queryCount).setParameter("pernr", pernr).getSingleResult();
		listOfIT2001 = em.createNativeQuery(query.concat(orderBy).concat(limitOffset), IT2001.class).setParameter("pernr", pernr).
				setParameter("limit", pageRequest.getPageSize()).setParameter("offset", pageRequest.getOffset()).getResultList();
		
		Page<IT2001> page = new PageImpl<IT2001>(listOfIT2001, pageRequest, total.longValue());
		
		return page;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Page<IT2001> findByPernrAndProcessesWithPaging(long pernr, String[] processes, int pageNumber,
			String infty, String subty, String periodBegin, String periodEnd) {
		
		String query = (processes == null) ? queryFindByPernr :
			queryFindByPernr.concat(" AND it2001.process in :processes");
		query = query.concat(addFilter(infty, subty, periodBegin, periodEnd));
		String queryCount = query.replaceFirst("\\*", "COUNT(*)");
		
		Pageable pageRequest = createPageRequest(pageNumber);
		BigInteger total = BigInteger.ZERO;
		List<IT2001> listOfIT2001 = new ArrayList<IT2001>();
		
		Query sqlQueryCount = em.createNativeQuery(queryCount).setParameter("pernr", pernr);
		Query sqlQuery = em.createNativeQuery(query.concat(orderBy).concat(limitOffset), IT2001.class).setParameter("pernr", pernr);
		if (processes != null) {
			List<String> listProcess = new ArrayList<String>();
			for (int i = 0; i < processes.length; i++) {
				listProcess.add(processes[i]);
			}
			sqlQueryCount = sqlQueryCount.setParameter("processes", listProcess);
			sqlQuery = sqlQuery.setParameter("processes", listProcess);
		}
		total =  (BigInteger) sqlQueryCount.getSingleResult();
		listOfIT2001 = sqlQuery.setParameter("limit", pageRequest.getPageSize()).setParameter("offset", pageRequest.getOffset()).getResultList();
		
		Page<IT2001> page = new PageImpl<IT2001>(listOfIT2001, pageRequest, total.longValue());
		
		return page;
	}
	
}