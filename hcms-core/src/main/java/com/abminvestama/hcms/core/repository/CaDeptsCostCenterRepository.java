package com.abminvestama.hcms.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.abminvestama.hcms.core.model.entity.CaDeptsCostCenter;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaDeptsCostCenterRepository extends CrudRepository<CaDeptsCostCenter, Long> {
	Page<CaDeptsCostCenter> findAll(Pageable pageRequest);
}
