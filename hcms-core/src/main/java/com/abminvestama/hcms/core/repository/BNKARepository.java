package com.abminvestama.hcms.core.repository;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.BNKA;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Modify findByKeywordsCount, findByKeywords query: add find one by bank key (bankl) and order by bankl</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add findByKeywordsCount, findByKeywords query method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findAll query method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface BNKARepository extends CrudRepository<BNKA, String> {
	
	/* version 1.0.2
	String FIND_BY_KEYWORDS_COUNT = "SELECT count(*) FROM (SELECT bankl, banka, stras, ort01, adrnr, brnch, " +
			"to_tsvector(regexp_replace(bankl, '[^a-zA-Z]', '', 'g')) || to_tsvector(banka) || to_tsvector(coalesce(stras, 'jalankosong')) || " +
			"to_tsvector(coalesce(ort01, 'kotakosong')) || to_tsvector(brnch) as document FROM bnka) p_search WHERE p_search.document @@ to_tsquery(:keywords)";
	
	String FIND_BY_KEYWORDS_QUERY = "SELECT bankl, banka, stras, ort01, adrnr, brnch FROM (SELECT bankl, banka, stras, ort01, adrnr, brnch, " +
		    "to_tsvector(regexp_replace(bankl, '[^a-zA-Z]', '', 'g')) || to_tsvector(banka) || to_tsvector(coalesce(stras, 'jalankosong')) || " +
			"to_tsvector(coalesce(ort01, 'kotakosong')) || to_tsvector(brnch) as document FROM bnka) p_search WHERE p_search.document @@ to_tsquery(:keywords) " +
		    "ORDER BY banka LIMIT :limit OFFSET :offset";
	*/
	
	String FIND_BY_KEYWORDS_COUNT = "SELECT count(*) FROM (SELECT bankl, banka, stras, ort01, adrnr, brnch, " +
			"to_tsvector(regexp_replace(bankl, '[^a-zA-Z]', '', 'g')) || to_tsvector(bankl) || to_tsvector(banka) || to_tsvector(coalesce(stras, 'jalankosong')) || " +
			"to_tsvector(coalesce(ort01, 'kotakosong')) || to_tsvector(brnch) as document FROM bnka) p_search WHERE p_search.document @@ to_tsquery(:keywords)";
	
	String FIND_BY_KEYWORDS_QUERY = "SELECT bankl, banka, stras, ort01, adrnr, brnch FROM (SELECT bankl, banka, stras, ort01, adrnr, brnch, " +
		    "to_tsvector(regexp_replace(bankl, '[^a-zA-Z]', '', 'g')) || to_tsvector(bankl) || to_tsvector(banka) || to_tsvector(coalesce(stras, 'jalankosong')) || " +
			"to_tsvector(coalesce(ort01, 'kotakosong')) || to_tsvector(brnch) as document FROM bnka) p_search WHERE p_search.document @@ to_tsquery(:keywords) " +
		    "ORDER BY banka, bankl LIMIT :limit OFFSET :offset";
	
	Page<BNKA> findAll(Pageable pageRequest);
	
	@Query(nativeQuery=true, value=FIND_BY_KEYWORDS_COUNT)
	int findByKeywordsCount(@Param("keywords") String keywords);
	
	@Query(nativeQuery=true, value=FIND_BY_KEYWORDS_QUERY)
	Collection<BNKA> findByKeywords(@Param("keywords") String keywords, @Param("limit") int limit, @Param("offset") int offset);
}