package com.abminvestama.hcms.core.model.constant;

/**
 * 
 * @author wijanarko (wijanarko777@gmx.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class TimeManagementConstants {
	public static final String FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
	public static final String FORMAT_HH24_MM = "HH:mm";
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_SUPERIOR = "ROLE_SUPERIOR";
	public static final String ROLE_USER = "ROLE_USER";
	public static final String ROLE_ADMIN_WITH_HR_ADMIN = "ROLE_ADMIN_WITH_HR_ADMIN";
	public static final String ROLE_ADMIN_WITHOUT_HR_ADMIN = "ROLE_ADMIN_WITHOUT_HR_ADMIN";
	public static final String ROLE_SUPERIOR_WITH_SUPERIOR = "ROLE_SUPERIOR_WITH_SUPERIOR";
	public static final String ROLE_SUPERIOR_WITHOUT_SUPERIOR = "ROLE_SUPERIOR_WITHOUT_SUPERIOR";
	
	/** TEVEN Mail Submit To Employee */
	public static final String TEVEN_MAIL_SUBMIT_TO_EMP = "TEVEN_MAIL_SUBMIT_TO_EMP";
	/** TEVEN Mail Submit To Superior */
	public static final String TEVEN_MAIL_SUBMIT_TO_SPV = "TEVEN_MAIL_SUBMIT_TO_SPV";
	/** TEVEN Mail Superior To Employee */
	public static final String TEVEN_MAIL_SPV_TO_EMP = "TEVEN_MAIL_SPV_TO_EMP";
	/** TEVEN Mail Superior To HR ADMIN */
	public static final String TEVEN_MAIL_SPV_TO_HR = "TEVEN_MAIL_SPV_TO_HR";
	/** TEVEN Mail HR ADMIN To Employee */
	public static final String TEVEN_MAIL_HR_TO_EMP = "TEVEN_MAIL_HR_TO_EMP";
	
	public static final String LIST_SUMMARY_CODE = "lsm";
	public static final String TASK_SUMMARY_CODE = "tsm";
}
