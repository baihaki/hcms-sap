package com.abminvestama.hcms.core.model.entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Class that represents master data for <strong>Employee Cost Receipt (Detail)</strong> (i.e. ptrv_srec in SAP).
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add fields: startVolume, endVolume, sumVolume</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add field: subt706b1</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Modify field: perio</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 * 
 */
@Entity
@Table(name = "ptrv_srec")
public class PtrvSrec extends SAPAbstractEntityZmed<PtrvSrecKey> {

	private static final long serialVersionUID = 2496232738510047249L;

	public PtrvSrec() {}

	public PtrvSrec(PtrvSrecKey id) {
		this();
		this.id = id;
	}
	
	
	@Column(name = "mandt", nullable = false, insertable = false, updatable = false)
	private String mandt;
	
	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;
	
	@Column(name = "reinr", nullable = false, insertable = false, updatable = false)
	private Long reinr;

	@Column(name = "perio", nullable = false, insertable = false, updatable = false)
	private Integer perio;
	
	@Column(name = "receiptno", nullable = false, insertable = false, updatable = false)
	private String receiptno;
	
	

	//@Column(name = "exp_type")
	//private String expType;

	@Column(name = "rec_amount")
	private Double recAmount;

	@Column(name = "rec_curr")
	private String recCurr;

	@Column(name = "rec_rate")
	private Double recRate;

	@Column(name = "loc_amount")
	private Double locAmount;

	@Column(name = "loc_curr")
	private String locCurr;

	@Column(name = "shorttxt")
	private String shorttxt;

	@Column(name = "paper_receipt")
	private String paperReceipt;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "rec_date")
	private Date recDate;
	

	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "morei", referencedColumnName = "morei"),
		@JoinColumn(name = "exp_type", referencedColumnName = "spkzl"),
		@JoinColumn(name = "sub_exp_type", referencedColumnName = "sub_spkzl")
	})
	private SubT706B1 subt706b1;

	@Column(name = "start_volume")
	private Double startVolume;

	@Column(name = "end_volume")
	private Double endVolume;

	@Column(name = "sum_volume")
	private Double sumVolume;

	public String getMandt() {
		return mandt;
	}

	public void setMandt(String mandt) {
		this.mandt = mandt;
	}

	public Long getPernr() {
		return pernr;
	}

	public void setPernr(Long pernr) {
		this.pernr = pernr;
	}

	public Long getReinr() {
		return reinr;
	}

	public void setReinr(Long reinr) {
		this.reinr = reinr;
	}

	public Integer getPerio() {
		return perio;
	}

	public void setPerio(Integer perio) {
		this.perio = perio;
	}

	public String getReceiptno() {
		return receiptno;
	}

	public void setReceiptno(String receiptno) {
		this.receiptno = receiptno;
	}

	/*
	public String getExpType() {
		return expType;
	}

	public void setExpType(String expType) {
		this.expType = expType;
	}
	*/
	
	public Double getRecAmount() {
		return recAmount;
	}

	public void setRecAmount(Double recAmount) {
		this.recAmount = recAmount;
	}

	public String getRecCurr() {
		return recCurr;
	}

	public void setRecCurr(String recCurr) {
		this.recCurr = recCurr;
	}

	public Double getRecRate() {
		return recRate;
	}

	public void setRecRate(Double recRate) {
		this.recRate = recRate;
	}

	public Double getLocAmount() {
		return locAmount;
	}

	public void setLocAmount(Double locAmount) {
		this.locAmount = locAmount;
	}

	public String getLocCurr() {
		return locCurr;
	}

	public void setLocCurr(String locCurr) {
		this.locCurr = locCurr;
	}

	public String getShorttxt() {
		return shorttxt;
	}

	public void setShorttxt(String shorttxt) {
		this.shorttxt = shorttxt;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	public String getPaperReceipt() {
		return paperReceipt;
	}
	
	public void setPaperReceipt(String paperReceipt) {
		this.paperReceipt = paperReceipt;
	}

	public SubT706B1 getSubt706b1() {
		return subt706b1;
	}

	public void setSubt706b1(SubT706B1 subt706b1) {
		this.subt706b1 = subt706b1;
	}

	public Double getStartVolume() {
		return startVolume;
	}

	public void setStartVolume(Double startVolume) {
		this.startVolume = startVolume;
	}

	public Double getEndVolume() {
		return endVolume;
	}

	public void setEndVolume(Double endVolume) {
		this.endVolume = endVolume;
	}

	public Double getSumVolume() {
		return sumVolume;
	}

	public void setSumVolume(Double sumVolume) {
		this.sumVolume = sumVolume;
	}
	
}

