package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;



@Embeddable
@Immutable
public class ZmedCompositeKeyHeaderPost implements Serializable {
	private static final long serialVersionUID = 5385850040856914239L;

	
	@Column(name = "obj_type", nullable = false)
	private String objType;
	
	@Column(name = "obj_key", nullable = false)
	private String objKey;
	
	@Column(name = "obj_sys", nullable = false)
	private String objSys;
	
	public ZmedCompositeKeyHeaderPost() {}
	
	public ZmedCompositeKeyHeaderPost(String objType, String objKey, String objSys) {
		this.objType = objType;
		this.objKey = objKey;
		this.objSys = objSys;
	}
	
	public String getObjType() {
		return objType;
	}
	
	public String getObjKey() {
		return objKey;
	}

	public String getObjSys() {
		return objSys;
	}
		
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final ZmedCompositeKeyHeaderPost key = (ZmedCompositeKeyHeaderPost) o;
		
		if (key.getObjType() != null ? key.getObjType() != (objType != null ? objType: "") : objType != null) {
			return false;
		}
		
		if (key.getObjKey() != null ? key.getObjKey() != (objKey != null ? objKey: "") : objKey != null) {
			return false;
		}
		
		if (key.getObjSys() != null ? key.getObjSys() != (objSys != null ? objSys: "") : objSys != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.objType != null ? this.objType.hashCode() : 0;
		result = result * 31 + (this.objSys != null ? this.objSys.hashCode() : 0);
		result = result * 31 + (this.objKey != null ? this.objKey.hashCode() : 0);
		return result;
	}
}
