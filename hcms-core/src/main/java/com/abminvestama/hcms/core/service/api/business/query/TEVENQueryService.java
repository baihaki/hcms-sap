package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.T705P;
import com.abminvestama.hcms.core.model.entity.TEVEN;
import com.abminvestama.hcms.core.model.entity.TEVENKey;
import com.abminvestama.hcms.core.model.entity.Tevenvw;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

public interface TEVENQueryService extends DatabaseQueryService<TEVEN, TEVENKey>, DatabasePaginationQueryService {

	
	@NotNull
	Optional<TEVEN> findOneByCompositeKey(Long pdsnr);

	@NotNull
	Optional<TEVEN> findOneByCompositeKeys(Long pernr, Date ldate, T705P satza);
	
	@NotNull
	Page<TEVEN> findByPernr(Long pernr, int pageNumber);

	int deleteItem(TEVEN teven);
	
	@NotNull
	Page<TEVEN> findByPernr(Long pernr, Date startDate, Date endDate, int pageNumber);
	
	/**
	 * get All Employee(List TevenView)
	 * 	1)	"RoleName":
	 * 		a. ROLE_ADMIN_WITH_HR_ADMIN, get All Employee by CompanyCode
	 * 		b. ROLE_ADMIN_WITHOUT_HR_ADMIN, get All Employee by CompanyCode(Without PERNR - HR ADMIN)
	 * 		c. ROLE_SUPERIOR_WITH_SUPERIOR, get All Employee by Superior
	 * 		d. ROLE_SUPERIOR_WITHOUT_SUPERIOR, get All Employee by Superior(Without PERNR - SUPERIOR)
	 * 		e. (ROLE_USER, ROLE_ADMIN, ROLE_SUPERIOR), get Employee by SSN
	 * 	2)	"BegLDate", Begin Logic Date
	 * 	3)	"EndLDate", End Logic Date
	 * 	4)	"Process", 0: Create / Editable State; 1: Submitted; 2: Approved 1,...etc
	 * 	5)	"TrxCode(Transaction Code)", 	
	 * 			AC: Auto-Create; AE:Auto-Editable; AS1: Auto-Submitted/Waiting Superior; AS2: Auto-Waiting HRAdmin; AF: Auto-Finished/Released
	 * 			MC: Manual-Create; ME: Manual-Editable; MS1: Manual-Submitted/Waiting Superior; MS2: Auto-Waiting HRAdmin; MF: Manual-Finished/Released
	 *  6)	"showLate", default "FALSE"
	 * 	7)	"OrderBy", default "LDATE DESC, PERNR".
	 * 	8)	"PageNumber", PageNumber to show by LIMIT(default 10) - OFFSET
	 */
	Page<Tevenvw> fetchAllEmpByCriteria(String roleName, User currentUser, User ssn, Date begldate, Date endldate,
			String process, String trxCode, String showLate, String orderBy, int pageNumber);

	Page<TEVEN> findByPernr(Long pernr, Date startDate, Date endDate, String satza, int pageNumber);
}