package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.IT0019;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.repository.IT0019Repository;
import com.abminvestama.hcms.core.service.api.business.query.IT0019QueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("it0019QueryService")
@Transactional(readOnly = true)
public class IT0019QueryServiceImpl implements IT0019QueryService {

	private IT0019Repository it0019Repository;
	
	@Autowired
	IT0019QueryServiceImpl(IT0019Repository it0019Repository) {
		this.it0019Repository = it0019Repository;
	}
	
	@Override
	public Optional<IT0019> findById(Optional<ITCompositeKeys> id) throws Exception {
		throw new NoSuchMethodException("Method not implemented. Please use method findOneByCompositeKeys instead.");		
	}

	@Override
	public Optional<Collection<IT0019>> fetchAll() {
		Optional<Iterable<IT0019>> it0019Iterates = Optional.ofNullable(it0019Repository.findAll());
		if (!it0019Iterates.isPresent()) {
			return Optional.empty();
		}
		
		List<IT0019> listOfIT0019 = new ArrayList<>();
		it0019Iterates.map(it0019Iterable -> {			
			it0019Iterable.forEach(it0019 -> {
				listOfIT0019.add(it0019);
			});
			return listOfIT0019;
		});
		
		return Optional.of(listOfIT0019);
	}

	@Override
	public Optional<IT0019> findOneByCompositeKey(Long pernr, String subty, Date endda, Date begda) {
		if (pernr == null || StringUtils.isBlank(subty) || endda == null || begda == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(it0019Repository.findOneByCompositeKey(pernr, SAPInfoType.TASK_MONITORING.infoType(), subty, endda, begda));
	}

	@Override
	public Collection<IT0019> findByPernr(Long pernr) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT0019>> bunchOfIT0019 
			= Optional.ofNullable(it0019Repository.findByPernr(pernr, SAPInfoType.TASK_MONITORING.infoType()));
		
		return (bunchOfIT0019.isPresent()
				? bunchOfIT0019.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

	@Override
	public Collection<IT0019> findByPernrAndSubty(Long pernr, String subty) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		if (StringUtils.isBlank(subty)) {
			subty = "03"; // default to subtype '03' (i.e. Expire of 1st Warning)
		}
		
		Optional<Collection<IT0019>> bunchOfIT0019 
			= Optional.ofNullable(it0019Repository.findByPernrAndSubty(pernr, SAPInfoType.TASK_MONITORING.infoType(), subty));

		return (bunchOfIT0019.isPresent() 
					? bunchOfIT0019.get().stream().collect(Collectors.toList())
					: Collections.emptyList());		
	}
}