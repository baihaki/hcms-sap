package com.abminvestama.hcms.core.service.api.business.command;

import javax.validation.ConstraintViolationException;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.IT0041;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add saveListFromMulesoft method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0041CommandService extends DatabaseCommandService<IT0041> {

	String saveListFromMulesoft(MuleConsumerService muleConsumerService,
			CommonServiceFactory commonServiceFactory, Long pernr, User user)
			throws ConstraintViolationException, CannotPersistException,
			NoSuchMethodException;
}