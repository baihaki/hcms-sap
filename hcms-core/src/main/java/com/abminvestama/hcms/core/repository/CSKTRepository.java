package com.abminvestama.hcms.core.repository;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.CSKT;
import com.abminvestama.hcms.core.model.entity.CSKTKey;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CSKTRepository extends CrudRepository<CSKT, CSKTKey> {

	Page<CSKT> findAll(Pageable pageRequest);	
	
	String FIND_BY_EMPLOYEE_CSKT_COUNT = "SELECT COUNT(*) " +
			"FROM cskt " +
			"WHERE (cskt.kokrs, cskt.kostl) " + 
				"IN (SELECT kokrs, kostl_default FROM ca_emp_cskt WHERE kokrs = cskt.kokrs AND kostl_default = cskt.kostl) ";
	
	String FIND_BY_EMPLOYEE_CSKT_QUERY = "SELECT cskt.* " +
			"FROM cskt " +
			"WHERE (cskt.kokrs, cskt.kostl) " + 
				"IN (SELECT kokrs, kostl_default FROM ca_emp_cskt WHERE kokrs = cskt.kokrs AND kostl_default = cskt.kostl) " +
			"ORDER BY cskt.kostl " + 
			"LIMIT :limit OFFSET :offset";
	@Query(nativeQuery=true, value=FIND_BY_EMPLOYEE_CSKT_COUNT)
	int findAllEmployeeCSKTCount();

	@Query(nativeQuery=true, value=FIND_BY_EMPLOYEE_CSKT_QUERY)
	Collection<CSKT> findAllEmployeeCSKTQuery(@Param("limit") int limit, @Param("offset") int offset);
}