package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyEmpquotastf;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findExpenseQuotasByBukrs method, used for expense quota</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface ZmedEmpquotastfQueryService extends DatabaseQueryService<ZmedEmpquotastf, ZmedCompositeKeyEmpquotastf>, DatabasePaginationQueryService{
	//Optional<ZmedEmpquotastf> findByEntityName(final String entityName);
	

	@NotNull
	Optional<ZmedEmpquotastf> findOneByCompositeKey(long stell, String bukrs, String persg, String persk, String kodequo, Date datab, Date datbi);
	

	@NotNull
	List<ZmedEmpquotastf> findQuotas(Long stell, String bukrs, String persg,
			String persk, Date datab, Date datbi);


	Optional<Collection<ZmedEmpquotastf>> findExpenseQuotasByBukrs(
			String bukrs, String persg, String persk, Date searchDate);
}

