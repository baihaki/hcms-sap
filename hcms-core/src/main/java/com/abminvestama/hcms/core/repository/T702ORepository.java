package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.T702O;
import com.abminvestama.hcms.core.model.entity.T702OKey;
import com.abminvestama.hcms.core.model.entity.T706S;

public interface T702ORepository extends CrudRepository<T702O, T702OKey> {


	@Query(nativeQuery = true, value = "SELECT * FROM t702o WHERE t702o.land1 = :land1")
	T702O findByLand1(@Param("land1") String land1);	
	
}