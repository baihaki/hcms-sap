package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0006;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add methods: findByPernrAndSubtyForPublishedOrUpdated</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add methods: findByStatus, findByPernrAndStatus, findByPernrAndSubty</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0006Repository extends CrudRepository<IT0006, ITCompositeKeys> {
		 
	@Query("FROM IT0006 it0006 WHERE it0006.pernr = :pernr AND it0006.id.infty = :infty AND it0006.id.subty = :subty AND it0006.id.endda = :endda AND it0006.id.begda = :begda")
	IT0006 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty,
			@Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM IT0006 it0006 WHERE it0006.pernr = :pernr AND it0006.id.infty = :infty ORDER BY it0006.id.subty ASC, it0006.id.endda DESC")
	Collection<IT0006> findByPernr(@Param("pernr") Long pernr, @Param("infty") String infty);
	
	//@Query("FROM IT0006 it0006 WHERE it0006.pernr = :pernr AND it0006.id.infty = :infty AND it0006.id.subty = :subty ORDER BY it0006.id.endda DESC")
	//Collection<IT0006> findByPernrAndSubty(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty);

	@Query("FROM IT0006 it0006 WHERE it0006.status = :status ORDER BY it0006.id.endda DESC")
	Page<IT0006> findByStatus(@Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0006 it0006 WHERE it0006.pernr = :pernr AND it0006.status = :status ORDER BY it0006.id.endda DESC")
	Page<IT0006> findByPernrAndStatus(@Param("pernr") Long pernr, @Param("status") DocumentStatus status, Pageable pageRequest);

	

	@Query("FROM IT0006 it0006 WHERE it0006.pernr = :pernr AND it0006.status = :status AND it0006.id.infty = :infty AND it0006.id.subty = :subty ORDER BY it0006.id.endda DESC")
	Page<IT0006> findByPernrAndSubty(@Param("pernr") Long pernr, @Param("status") DocumentStatus status, 
			Pageable pageRequest, @Param("infty") String infty, @Param("subty") String subty);
	

	@Query("FROM IT0006 it0006 WHERE it0006.pernr = :pernr AND " +
			"(it0006.status = 'PUBLISHED' OR it0006.status = 'UPDATE_IN_PROGRESS' OR it0006.status = 'UPDATE_REJECTED') " +
			" AND it0006.id.infty = :infty AND it0006.id.subty = :subty ORDER BY it0006.id.endda DESC")
	Page<IT0006> findByPernrAndSubtyForPublishedOrUpdated(@Param("pernr") Long pernr, 
			Pageable pageRequest, @Param("infty") String infty, @Param("subty") String subty);
	

	@Query("FROM IT0006 it0006 WHERE it0006.pernr = :pernr AND it0006.id.infty = :infty AND it0006.id.subty = :subty ORDER BY it0006.id.endda DESC")
	Page<IT0006> findByPernrAndSubty(@Param("pernr") Long pernr, 
			Pageable pageRequest, @Param("infty") String infty, @Param("subty") String subty);

}