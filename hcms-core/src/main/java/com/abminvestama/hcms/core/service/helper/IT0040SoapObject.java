package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0040;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0040SoapObject {
	
	private String employeenumber;
	private String validitybegin;
	private String validityend;
	private String infty;
	private String subty;
	private String seqnr;
	private String itext;
	private String leihg;
	private String infty1;
	private String anzkl;
	private String zeinh;
	private String lobnr;
	
	private IT0040SoapObject() {}
	
	public IT0040SoapObject(IT0040 object) {
		if (object == null) {
			new IT0040SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			infty = object.getSubty().getId().getInfty();
			subty = object.getSubty().getId().getSubty();
			seqnr = object.getSeqnr().toString();
			itext = object.getItxex();
			leihg = object.getLeihg().getId().getSubty();
			infty1 = object.getLeihg().getId().getInfty();
			anzkl = object.getAnzkl().toString();
			zeinh = object.getZeinh().getZeinh();
			lobnr = object.getLobnr();
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}


	public String getInfty() {
		return infty;
	}

	public String getSubty() {
		return subty;
	}

	public String getSeqnr() {
		return seqnr;
	}

	public String getItext() {
		return itext;
	}

	public String getLeihg() {
		return leihg;
	}

	public String getInfty1() {
		return infty1;
	}

	public String getAnzkl() {
		return anzkl;
	}

	public String getZeinh() {
		return zeinh;
	}

	public String getLobnr() {
		return lobnr;
	}


}
