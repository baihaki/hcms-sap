package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;

public interface ZmedEmpquotaRepository extends CrudRepository<ZmedEmpquota, String> {

//	ZmedEmpquota findByPersk(String persk);
	@Query("FROM ZmedEmpquota zmed_empquota WHERE zmed_empquota.bukrs.bukrs = :bukrs AND zmed_empquota.persg = :persg AND zmed_empquota.persk = :persk AND zmed_empquota.fatxt = :fatxt AND zmed_empquota.kodequo = :kodequo AND zmed_empquota.id.datab = :datab AND zmed_empquota.id.datbi = :datbi")
	ZmedEmpquota findOneByCompositeKey(@Param("bukrs") String bukrs,
			@Param("persg") String persg, 
			@Param("persk") String persk,
			@Param("fatxt") String fatxt,
			@Param("kodequo") String kodequo,
			@Param("datab") @Temporal(TemporalType.DATE) Date datab,
			@Param("datbi") @Temporal(TemporalType.DATE) Date datbi);
	
	
	@Query("FROM ZmedEmpquota zmed_empquota WHERE zmed_empquota.bukrs.bukrs = :bukrs AND zmed_empquota.persg = :persg AND zmed_empquota.persk = :persk AND zmed_empquota.id.datab = :datab AND zmed_empquota.id.datbi = :datbi")
	Collection<ZmedEmpquota> findQuotas(@Param("bukrs") String bukrs,
			@Param("persg") String persg, 
			@Param("persk") String persk,
			@Param("datab") @Temporal(TemporalType.DATE) Date datab,
			@Param("datbi") @Temporal(TemporalType.DATE) Date datbi);
	
	//@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.id.endda = :endda AND it0002.id.begda = :begda")
	//IT0002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
	//		@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	
}
