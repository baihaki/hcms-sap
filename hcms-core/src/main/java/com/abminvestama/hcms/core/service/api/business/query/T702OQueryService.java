package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.T702O;
import com.abminvestama.hcms.core.model.entity.T702OKey;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

public interface T702OQueryService extends DatabaseQueryService<T702O, T702OKey>, DatabasePaginationQueryService {

	Optional<T702O> findById(String zland);
	
}