package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.Phdat;
import com.abminvestama.hcms.core.model.entity.PhdatKey;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 2.0.0
 * @version 2.0.1
 * @author wijanarko (wijanarko777@gmx.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add findByDateAndType method</td></tr>
 *     <tr><td>2.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface PhdatQueryService extends DatabaseQueryService<Phdat, PhdatKey>, DatabasePaginationQueryService {

	public Page<Phdat> fetchAllByStartDateAndEndDateWithOrdeBy(Date startDate, Date endDate, String phType, String orderBy, int pageNumber);

	public Optional<Collection<Phdat>> fetchAll();

	public Optional<Phdat> findById(Optional<PhdatKey> id) throws Exception;

	public Optional<Phdat> findByErdat(Date erdat);

	Collection<Phdat> findByDateAndType(Date startDate, Date endDate, String phType);
}