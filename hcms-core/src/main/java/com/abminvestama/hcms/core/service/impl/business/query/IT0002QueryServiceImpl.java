package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0002;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.repository.IT0002Repository;
import com.abminvestama.hcms.core.service.api.business.query.IT0002QueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Modify findByPernrAndStatus method: Set findByPernrForPublishedOrUpdated for PUBLISHED status</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add methods: countByGenderAndBukrs, countByAgeAndBukrs</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("it0002QueryService")
@Transactional(readOnly = true)
public class IT0002QueryServiceImpl implements IT0002QueryService {

	private IT0002Repository it0002Repository;
	
	@Autowired
	IT0002QueryServiceImpl(IT0002Repository it0002Repository) {
		this.it0002Repository = it0002Repository;
	}
	
	@Override
	public Optional<IT0002> findById(Optional<ITCompositeKeysNoSubtype> id) throws Exception {
		return id.map(pk -> it0002Repository.findOneByCompositeKey(pk.getPernr(), pk.getEndda(), pk.getBegda()));
	}

	@Override
	public Optional<Collection<IT0002>> fetchAll() {
		Optional<Iterable<IT0002>> it0002Iterates = Optional.ofNullable(it0002Repository.findAll());
		
		List<IT0002> listOfIT0002 = new ArrayList<>();
		
		return it0002Iterates.map(it0002Iterable -> {			
			it0002Iterable.forEach(it0002 -> {
				listOfIT0002.add(it0002);
			});
			return listOfIT0002;
		});
	}

	@Override
	public Optional<IT0002> findOneByCompositeKey(Long pernr, Date endda, Date begda) {
		if (pernr == null || endda == null || begda == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(it0002Repository.findOneByCompositeKey(pernr, endda, begda));		
	}

	@Override
	public Collection<IT0002> findByPernr(Long pernr) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT0002>> bunchOfIT0002 
			= Optional.ofNullable(it0002Repository.findByPernr(pernr));
		
		return (bunchOfIT0002.isPresent()
				? bunchOfIT0002.get().stream().collect(Collectors.toList())
				: Collections.emptyList());				
	}
	
	
	@Override
	public Page<IT0002> findByPernrAndStatus(long pernr, String status, int pageNumber) {
		if (StringUtils.isEmpty(status)) {
			status = DocumentStatus.PUBLISHED.name();
		}
		
		Pageable pageRequest = createPageRequest(pageNumber);
		
		DocumentStatus documentStatus = DocumentStatus.valueOf(status);
		
		return (documentStatus == DocumentStatus.PUBLISHED) ? it0002Repository.findByPernrForPublishedOrUpdated(pernr, pageRequest)
				: it0002Repository.findByPernrAndStatus(pernr, documentStatus, pageRequest);
	}
	
	
	
	@Override
	public Long countByGenderAndBukrs(String gesch, String bukrs) {
		if(bukrs == null){
			bukrs = "9900";
		}
		Long answer;
		if(gesch == null){
			answer = it0002Repository.countByGenderAndBukrs( bukrs);
		}else{
			answer = it0002Repository.countByGenderAndBukrs(gesch, bukrs);
		}
		return answer;
	}
	
	
	
	
	@Override
	public Long countByAgeAndBukrs(String dat1, String dat2, String bukrs) {
		if(bukrs == null){
			bukrs = "9900";
		}
		Long answer;
		if((dat1 == null || dat1.isEmpty())&& !dat2.isEmpty()){
			answer = it0002Repository.countByAgeAndBukrsLower(dat2, bukrs);
		}else if((dat2 == null || dat2.isEmpty())&& !dat1.isEmpty()){
			answer = it0002Repository.countByAgeAndBukrsUpper(dat1, bukrs);
		}else if( !dat2.isEmpty() && !dat1.isEmpty()){
			answer = it0002Repository.countByAgeAndBukrs(dat1, dat2, bukrs);
		}else {
			answer = it0002Repository.countByGenderAndBukrs(bukrs);
		}
		return answer;
	}
}