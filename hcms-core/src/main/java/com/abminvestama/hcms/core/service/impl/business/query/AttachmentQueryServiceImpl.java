package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.Attachment;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.repository.AttachmentRepository;
import com.abminvestama.hcms.core.service.api.business.query.AttachmentQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 *
 */
@Service("attachmentQueryService")
@Transactional(readOnly = true)
public class AttachmentQueryServiceImpl implements AttachmentQueryService {
	
	private AttachmentRepository attachmentRepository;
	
	@Autowired
	AttachmentQueryServiceImpl(AttachmentRepository attachmentRepository) {
		this.attachmentRepository = attachmentRepository;
	}

	@Override
	public Optional<Attachment> findById(Optional<T591SKey> id) throws Exception {
		return id.map(pk -> {
			return Optional.ofNullable(attachmentRepository.findOne(pk));
		}).orElse(Optional.empty());
	}

	@Override
	public Optional<Collection<Attachment>> fetchAll() {
		Optional<Iterable<Attachment>> attachmentIterable = Optional.ofNullable(attachmentRepository.findAll());
		if (!attachmentIterable.isPresent()) {
			return Optional.empty();
		}
		
		List<Attachment> listOfAttachment = new ArrayList<>();
		return attachmentIterable.map(iteratesAttachment -> {
			iteratesAttachment.forEach(attachment -> {
				listOfAttachment.add(attachment);
			});
			
			return listOfAttachment;
		});		
	}

	@Override
	public Page<Attachment> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return attachmentRepository.findAll(pageRequest);
	}

	@Override
	public Page<Attachment> fetchAllByInfotype(String infty, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return attachmentRepository.findAllByInfotype(pageRequest, infty);
	}
}