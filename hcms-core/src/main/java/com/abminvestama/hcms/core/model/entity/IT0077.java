package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 * Class that represents master data transaction for <strong>Additional Personal Data</strong> (i.e. IT0077 in SAP).
 */
@Entity
@Table(name = "it0077")
public class IT0077 extends SAPAbstractEntity<ITCompositeKeysNoSubtype> {

	private static final long serialVersionUID = -4636532782889687849L;
	
	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;
	
	public IT0077() {}
	
	public IT0077(ITCompositeKeysNoSubtype id) {
		this();
		this.id = id;
	}
	
	@Column(name = "seqnr")
	private Long seqnr;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "molga", referencedColumnName = "molga", insertable = true, updatable = true),
		@JoinColumn(name = "racky", referencedColumnName = "racky", insertable = true, updatable = true)
	})
	private T505S racky;
	
	/**
	 * GET Employee SSN/ID.
	 * 
	 * @return
	 */
	public Long getPernr() {
		return pernr;
	}
	
	public void setPernr(Long pernr) {
		this.pernr = pernr;
	}
	
	/**
	 * GET Infotype Record No.
	 * 
	 * @return
	 */
	public Long getSeqnr() {
		return seqnr;
	}
	
	public void setSeqnr(Long seqnr) {
		this.seqnr = seqnr;
	}
	
	/**
	 * GET Ethnicity.
	 * 
	 * @return
	 */
	public T505S getRacky() {
		return racky;
	}
	
	public void setRacky(T505S racky) {
		this.racky = racky;
	}
}