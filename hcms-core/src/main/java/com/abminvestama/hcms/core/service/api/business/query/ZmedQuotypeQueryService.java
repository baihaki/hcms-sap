package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Date;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyQuotype;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedQuotype;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;


public interface ZmedQuotypeQueryService extends DatabaseQueryService<ZmedQuotype, ZmedCompositeKeyQuotype>, DatabasePaginationQueryService{
	//Optional<ZmedQuotype> findByEntityName(final String entityName);
	

	@NotNull
	Optional<ZmedQuotype> findOneByCompositeKey(String bukrs, String kodequo, Date datab, Date datbi);
	
}

