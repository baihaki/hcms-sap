package com.abminvestama.hcms.core.service.impl.business.query;

import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.IT0241;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.repository.IT0241Repository;
import com.abminvestama.hcms.core.service.api.business.query.IT0241QueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add findByStatus with employee parameter</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add findByPernrAndStatus method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findByStatus method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("it0241QueryService")
@Transactional(readOnly = true)
public class IT0241QueryServiceImpl implements IT0241QueryService {

	private IT0241Repository it0241Repository;
	
	@PersistenceContext(type = PersistenceContextType.TRANSACTION)	
	private EntityManager em;
	
	@Autowired
    Environment env;
	
	@Autowired
	IT0241QueryServiceImpl(IT0241Repository it0241Repository) {
		this.it0241Repository = it0241Repository;
	}
	
	@Override
	public Optional<IT0241> findById(Optional<ITCompositeKeysNoSubtype> id) throws Exception {
		throw new NoSuchMethodException("Method not implemented. Please use method findOneByCompositeKeys instead.");		
	}

	@Override
	public Optional<Collection<IT0241>> fetchAll() {
		Optional<Iterable<IT0241>> it0241Iterates = Optional.ofNullable(it0241Repository.findAll());
		
		List<IT0241> listOfIT0241 = new ArrayList<>();
		
		return it0241Iterates.map(it0241Iterable -> {			
			it0241Iterable.forEach(it0241 -> {
				listOfIT0241.add(it0241);
			});
			return listOfIT0241;
		});
	}

	@Override
	public Optional<IT0241> findOneByCompositeKey(Long pernr, Date endda, Date begda) {
		if (pernr == null || endda == null || begda == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(it0241Repository.findOneByCompositeKey(pernr, endda, begda));
	}

	@Override
	public Collection<IT0241> findByPernr(Long pernr) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT0241>> bunchOfIT0241 
			= Optional.ofNullable(it0241Repository.findByPernr(pernr));
		
		return (bunchOfIT0241.isPresent()
				? bunchOfIT0241.get().stream().collect(Collectors.toList())
				: Collections.emptyList());		
	}

	@Override
	public Page<IT0241> findByStatus(String status, int pageNumber) {
		if (StringUtils.isEmpty(status)) {
			status = DocumentStatus.PUBLISHED.name();
		}
		
		Pageable pageRequest = createPageRequest(pageNumber);
		
		DocumentStatus documentStatus = DocumentStatus.valueOf(status);
		
		return it0241Repository.findByStatus(documentStatus, pageRequest);
	}	
	@SuppressWarnings("unchecked")
	@Override
	public Page<IT0241> findByStatus(String status, int pageNumber, IT0001 employee) {
		if (StringUtils.isEmpty(status)) {
			status = DocumentStatus.PUBLISHED.name();
		}
		
		String queryByBukrsCount = "SELECT COUNT(*) FROM ( " +
				"SELECT DISTINCT ON (A.pernr) A.*, B.bukrs, B.endda, B.begda " +
		        "FROM it0241 A INNER JOIN it0001 B ON A.pernr = B.pernr " +
				"WHERE B.bukrs = :bukrs and B.endda >= A.endda and document_status = :documentStatus " +
		        "ORDER BY A.pernr, B.pernr, A.endda desc, A.begda desc, B.endda desc, B.begda desc " +
		        ") AS rows";
		
		String queryByBukrs = "SELECT DISTINCT ON (A.pernr) A.* " +
		        "FROM it0241 A INNER JOIN it0001 B ON A.pernr = B.pernr " +
				"WHERE B.bukrs = :bukrs and B.endda >= A.endda and document_status = :documentStatus " +
		        "ORDER BY A.pernr, B.pernr, A.endda desc, A.begda desc, B.endda desc, B.begda desc " +
				"LIMIT :limit OFFSET :offset";
		
		DocumentStatus documentStatus = DocumentStatus.valueOf(status);
		String bukrs = employee.getT500p().getBukrs().getBukrs();
		
		Pageable pageRequest = createPageRequest(pageNumber);
		BigInteger total = BigInteger.ZERO;
		
		List<IT0241> listOfITObject = new ArrayList<IT0241>();

		
		// check if there is customize workflow admin task
		if (env.getProperty("workflow.filter.admin.".concat(bukrs)) != null) {
			String filter = env.getProperty("workflow.filter.admin.".concat(bukrs));
			String[] params = filter.split(",");
			String whereClause = "WHERE B.bukrs = :bukrs ";
			for (int i = 0; i < params.length; i++) {
				String[] subParams = params[i].split("\\.");
				Object object = employee;
				for (int j=0; j < subParams.length; j++) {
					try {
						Method getMethod = object.getClass().getMethod(
								"get".concat(subParams[j]));
						object = getMethod.invoke(object);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				String paramValue = (String) object;
				whereClause = whereClause.concat("and B.").concat(subParams[subParams.length-1].toLowerCase()).
						concat("='").concat(paramValue).concat("' ");
			}
			queryByBukrsCount = queryByBukrsCount.replace("WHERE B.bukrs = :bukrs ", whereClause);
			queryByBukrs = queryByBukrs.replace("WHERE B.bukrs = :bukrs ", whereClause);
		}
		
		total =  (BigInteger) em.createNativeQuery(queryByBukrsCount).
				setParameter("bukrs", bukrs).setParameter("documentStatus", status).getSingleResult();
		listOfITObject = em.createNativeQuery(queryByBukrs, IT0241.class).
				setParameter("bukrs", bukrs).setParameter("documentStatus", status).
				setParameter("limit", pageRequest.getPageSize()).setParameter("offset", pageRequest.getOffset()).getResultList();
		
		Page<IT0241> page = new PageImpl<IT0241>(listOfITObject, pageRequest, total.longValue());
		
		return page;
	}

	@Override
	public Page<IT0241> findByPernrAndStatus(long pernr, String status, int pageNumber) {
		if (StringUtils.isEmpty(status)) {
			status = DocumentStatus.PUBLISHED.name();
		}
		
		Pageable pageRequest = createPageRequest(pageNumber);
		
		DocumentStatus documentStatus = DocumentStatus.valueOf(status);
		
		return it0241Repository.findByPernrAndStatus(pernr, documentStatus, pageRequest);
	}
}