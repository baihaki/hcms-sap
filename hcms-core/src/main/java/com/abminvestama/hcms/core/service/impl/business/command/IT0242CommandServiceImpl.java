package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.IT0242;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.IT0242Repository;
import com.abminvestama.hcms.core.service.api.business.command.IT0242CommandService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("it0242CommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class IT0242CommandServiceImpl implements IT0242CommandService {

	private IT0242Repository it0242Repository;
	
	@Autowired
	IT0242CommandServiceImpl(IT0242Repository it0242Repository) {
		this.it0242Repository = it0242Repository;
	}
	
	@Override
	public Optional<IT0242> save(IT0242 entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setUname(user.getId());
		}
		
		try {
			return Optional.ofNullable(it0242Repository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist IT0242 (Tax Data Indonesia).Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(IT0242 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete on this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(IT0242 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}
}