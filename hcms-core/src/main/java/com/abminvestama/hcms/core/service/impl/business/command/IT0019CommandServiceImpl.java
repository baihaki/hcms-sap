package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.IT0019;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.IT0019Repository;
import com.abminvestama.hcms.core.service.api.business.command.IT0019CommandService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("it0019CommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class IT0019CommandServiceImpl implements IT0019CommandService {

	private IT0019Repository it0019Repository;
	
	@Autowired
	IT0019CommandServiceImpl(IT0019Repository it0019Repository) {
		this.it0019Repository = it0019Repository;
	}
	
	@Override
	public Optional<IT0019> save(IT0019 entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setUname(user.getId());
		}
		
		try {
			return Optional.ofNullable(it0019Repository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist IT0019 (Monitoring of Tasks).Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(IT0019 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");		
	}

	@Override
	public boolean restore(IT0019 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");		
	}
}