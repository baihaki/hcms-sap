package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.IT0105;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.repository.IT0105Repository;
import com.abminvestama.hcms.core.service.api.business.query.IT0105QueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Modify findByPernrAndStatus method: Set findByPernrForPublishedOrUpdated for PUBLISHED status</td></tr>
 *     <tr><td>1.0.2</td><td>Anasuya</td><td>Add findByPernrAndStatus method</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add findByStatus method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("it0105QueryService")
@Transactional(readOnly = true)
public class IT0105QueryServiceImpl implements IT0105QueryService {

	private IT0105Repository it0105Repository;
	
	@Autowired
	IT0105QueryServiceImpl(IT0105Repository it0105Repository) {
		this.it0105Repository = it0105Repository;
	}
	
	@Override
	public Optional<IT0105> findById(Optional<ITCompositeKeys> id) throws Exception {
		throw new NoSuchMethodException("Method not implemented. Please use method findOneByCompositeKeys instead.");		
	}

	@Override
	public Optional<Collection<IT0105>> fetchAll() {
		Optional<Iterable<IT0105>> it0105Iterates = Optional.ofNullable(it0105Repository.findAll());
		if (!it0105Iterates.isPresent()) {
			return Optional.empty();
		}
		
		List<IT0105> listOfIT0105 = new ArrayList<>();
		it0105Iterates.map(it0105Iterable -> {			
			it0105Iterable.forEach(it0105 -> {
				listOfIT0105.add(it0105);
			});
			return listOfIT0105;
		});
		
		return Optional.of(listOfIT0105);
	}

	@Override
	public Optional<IT0105> findOneByCompositeKey(Long pernr, String subty, Date endda, Date begda) {
		if (pernr == null || StringUtils.isBlank(subty) || endda == null || begda == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(it0105Repository.findOneByCompositeKey(pernr, SAPInfoType.COMMUNICATION_TYPE.infoType(), subty, endda, begda));
	}

	@Override
	public Collection<IT0105> findByPernr(Long pernr) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT0105>> bunchOfIT0105 
			= Optional.ofNullable(it0105Repository.findByPernr(pernr, SAPInfoType.COMMUNICATION_TYPE.infoType()));
		
		return (bunchOfIT0105.isPresent()
				? bunchOfIT0105.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

	@Override
	public Collection<IT0105> findByPernrAndSubty(Long pernr, String subty) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		if (StringUtils.isBlank(subty)) {
			subty = "0010"; // default to subtype '0010' (Email)
		}
		
		Optional<Collection<IT0105>> bunchOfIT0105 
			= Optional.ofNullable(it0105Repository.findByPernrAndSubty(pernr, SAPInfoType.COMMUNICATION_TYPE.infoType(), subty));

		return (bunchOfIT0105.isPresent() 
					? bunchOfIT0105.get().stream().collect(Collectors.toList())
					: Collections.emptyList());		
	}
	
	
	
	
	@Override
	public Page<IT0105> findByStatus(String status, int pageNumber) {
		if (StringUtils.isEmpty(status)) {
			status = DocumentStatus.PUBLISHED.name();
		}
		
		Pageable pageRequest = (Pageable) createPageRequest(pageNumber);
		
		DocumentStatus documentStatus = DocumentStatus.valueOf(status);
		
		return it0105Repository.findByStatus(documentStatus, pageRequest);
		
		/*Optional<Collection<IT0006>> bunchOfIT0006
			= Optional.ofNullable(it0006Repository.findByStatus(documentStatus));
		
		return (bunchOfIT0006.isPresent()
				? bunchOfIT0006.get().stream().collect(Collectors.toList())
				: Collections.emptyList());*/
	}
	
	
	
	@Override
	public Page<IT0105> findByPernrAndStatus(long pernr, String status, int pageNumber) {
		if (StringUtils.isEmpty(status)) {
			status = DocumentStatus.PUBLISHED.name();
		}
		
		Pageable pageRequest = createPageRequest(pageNumber);
		
		DocumentStatus documentStatus = DocumentStatus.valueOf(status);
		
		return (documentStatus == DocumentStatus.PUBLISHED) ? it0105Repository.findByPernrForPublishedOrUpdated(pernr, pageRequest)
				: it0105Repository.findByPernrAndStatus(pernr, documentStatus, pageRequest);
	}
}