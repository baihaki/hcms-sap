package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T531S;
import com.abminvestama.hcms.core.repository.T531SRepository;
import com.abminvestama.hcms.core.service.api.business.query.T531SQueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("t531sQueryService")
@Transactional(readOnly = true)
public class T531SQueryServiceImpl implements T531SQueryService {

	private T531SRepository t531sRepository;
	
	@Autowired
	T531SQueryServiceImpl(T531SRepository t531sRepository) {
		this.t531sRepository = t531sRepository;
	}
	
	@Override
	public Optional<T531S> findById(Optional<String> id) throws Exception {
		return id.map(pk -> t531sRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<T531S>> fetchAll() {
		Optional<Iterable<T531S>> t531sIterable = Optional.ofNullable(t531sRepository.findAll());
		if (!t531sIterable.isPresent()) {
			return Optional.empty();
		}
		
		List<T531S> listOfT531S = new ArrayList<>();
		return t531sIterable.map(iteratesT531S -> {
			iteratesT531S.forEach(t531s -> {
				listOfT531S.add(t531s);
			});
			
			return listOfT531S;
		});				
	}
}