package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyTrxitm;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;
import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Modify findOneByCompositeKey method: Add clmno</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface ZmedTrxitmQueryService extends DatabaseQueryService<ZmedTrxitm, ZmedCompositeKeyTrxitm>, DatabasePaginationQueryService{
	//Optional<ZmedTrxitm> findByEntityName(final String entityName);
	

	@NotNull
	Optional<ZmedTrxitm> findOneByCompositeKey(String itmno, String clmno);

	@NotNull
	String findLastId();
	

	@NotNull
	Collection<ZmedTrxitm> findByClmno(String clmno);
}

