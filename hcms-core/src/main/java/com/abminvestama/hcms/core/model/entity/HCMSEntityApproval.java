package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.abminvestama.hcms.core.model.constant.OperationType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Entity
@Table(name = "cfg_entity_approval", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"cfg_entity_id", "cfg_approval_category_id"} )
})
public class HCMSEntityApproval extends AbstractEntity {

	private static final long serialVersionUID = 6667910030867266736L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cfg_entity_id", referencedColumnName = "id", nullable = false)
	private HCMSEntity hcmsEntity;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cfg_approval_category_id", referencedColumnName = "id", nullable = false)
	private ApprovalCategory approvalCategory;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "operation_type", nullable = false)
	private OperationType operationType;
	
	protected HCMSEntityApproval() {}
	
	public HCMSEntityApproval(HCMSEntity hcmsEntity, ApprovalCategory approvalCategory, OperationType operationType) {
		this.hcmsEntity = hcmsEntity;
		this.approvalCategory = approvalCategory;
		this.operationType = operationType;
	}
	
	public HCMSEntity getHcmsEntity() {
		return hcmsEntity;
	}
	
	public ApprovalCategory getApprovalCategory() {
		return approvalCategory;
	}
	
	public OperationType getOperationType() {
		return operationType;
	}
}