package com.abminvestama.hcms.core.service.impl.business.command;


import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.CaReqHdr;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.CaReqHdrRepository;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.CaReqHdrCommandService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("caReqHdrCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CaReqHdrCommandServiceImpl implements CaReqHdrCommandService {
	
	private CaReqHdrRepository caReqHdrRepository;
	
    class UpdateThread implements Runnable {
    	
    	CaReqHdr caReqHdr;
    	EventLogCommandService eventLogCommandService;
    	EventLog eventLog;
    	User user;
    	
    	UpdateThread(CaReqHdr caReqHdr, EventLogCommandService eventLogCommandService, EventLog eventLog, User user) {
    		this.caReqHdr = caReqHdr;
    		this.eventLogCommandService = eventLogCommandService;
    		this.eventLog = eventLog;
    		this.user = user;
    	}
    	
		@Override
		public void run() {
			try {
				Thread.sleep(10000);
				String caReqHdrKey = caReqHdr.getId();
				caReqHdr.setReqNo(caReqHdrKey);
				caReqHdr = caReqHdrRepository.save(caReqHdr);
				eventLog.setStatus("newReqNo=" + caReqHdr.getReqNo());
				eventLogCommandService.save(eventLog, user);
				System.out.println("UpdateThread succesfully saved " + caReqHdr.toString());
			} catch (Exception e) {
	        	System.out.println("UpdateThread Exception:" + e.getMessage());
			}
		}
		
    }
    
    @Autowired
	CaReqHdrCommandServiceImpl(CaReqHdrRepository caReqHdrRepository) {
		this.caReqHdrRepository = caReqHdrRepository;
	}
	
	@Override
	public Optional<CaReqHdr> save(CaReqHdr entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			if (entity.getCreatedBy() == null) {
				entity.setCreatedBy(user);
			}
			entity.setUpdatedBy(user);
		}
		
		try {
			return Optional.ofNullable(caReqHdrRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist CaReqHdr (Cash Advance Request Header). Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(CaReqHdr entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(CaReqHdr entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}
	
	/*
	@Override
	public Optional<CaReqHdr> updateReinr(Long newReinr, CaReqHdr entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		
		try {
			int row = caReqHdrRepository.updateReinr(newReinr, entity.getProcess(), entity.getSuperiorApprovedBy().getId(),
					entity.getSuperiorApprovedAt(), entity.getReason(), entity.getLastSync(), entity.getId().getReinr(), entity.getId().getPernr());
			if (row > 0) {
			    entity.setReinr(newReinr);
			}
			return Optional.ofNullable(entity);
			//return Optional.ofNullable(caReqHdrRepository.save(entity));
		} catch (Exception e) {
			entity.setReinr(newReinr);
			System.out.println("updateReinr Exception: " + e.getMessage());
			return Optional.ofNullable(entity);
		}				
	}
	
	@Override
	public void threadUpdate(CaReqHdr entity, EventLogCommandService eventLogCommandService, EventLog eventLog, User user) {
		new Thread(new UpdateThread(entity, eventLogCommandService, eventLog, user)).start();
	}
	*/
}