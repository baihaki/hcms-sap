package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T548T;
import com.abminvestama.hcms.core.repository.T548TRepository;
import com.abminvestama.hcms.core.service.api.business.query.T548TQueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("t548tQueryService")
@Transactional(readOnly = true)
public class T548TQueryServiceImpl implements T548TQueryService {

	private T548TRepository t548tRepository;
	
	@Autowired
	T548TQueryServiceImpl(T548TRepository t548tRepository) {
		this.t548tRepository = t548tRepository;
	}
	
	@Override
	public Optional<T548T> findById(Optional<String> id) throws Exception {
		return id.map(pk -> t548tRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<T548T>> fetchAll() {
		Optional<Iterable<T548T>> t548tIterable = Optional.ofNullable(t548tRepository.findAll());
		if (!t548tIterable.isPresent()) {
			return Optional.empty();
		}
		
		List<T548T> listOfT548T = new ArrayList<>();
		return t548tIterable.map(iteratesT548T -> {
			iteratesT548T.forEach(t548t -> {
				listOfT548T.add(t548t);
			});
			
			return listOfT548T;
		});				
	}
}