package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t500l")
public class T500L implements Serializable {

	private static final long serialVersionUID = 4747706564427397557L;

	@Id
	@Column(name = "molga", nullable = false, unique = true, length = 2)
	private String molga;
	
	@Column(name = "ltext", nullable = false, length = 40)
	private String ltext;
	
	/**
	 * Get Marrital Status Code
	 * 
	 * @return
	 */
	public String getMolga() {
		return molga;
	}
	
	/**
	 * Get Marrital Status Text (Description)
	 * 
	 * @return
	 */
	public String getLtext() {
		return ltext;
	}
}