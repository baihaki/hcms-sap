package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyMedtype;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.abminvestama.hcms.core.repository.ZmedMedtypeRepository;
import com.abminvestama.hcms.core.service.api.business.query.ZmedMedtypeQueryService;

@Service("zmedMedtypeQueryService")
@Transactional(readOnly = true)
public class ZmedMedtypeQueryServiceImpl implements ZmedMedtypeQueryService {

	private ZmedMedtypeRepository zmedMedtypeRepository;
	
	@Autowired
	ZmedMedtypeQueryServiceImpl(ZmedMedtypeRepository zmedMedtypeRepository) {
		this.zmedMedtypeRepository = zmedMedtypeRepository;
	}
	
	//@Override
	//public Optional<ZmedMedtype> findById(Optional<String> id) throws Exception {
	//	return id.map(pk -> zmedMedtypeRepository.findOne(pk));
	//}

	@Override
	public Optional<Collection<ZmedMedtype>> fetchAll() {
		List<ZmedMedtype> entities = new ArrayList<>();
		Optional<Iterable<ZmedMedtype>> entityIterable = Optional.ofNullable(zmedMedtypeRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}

	@Override
	public Optional<ZmedMedtype> findById(Optional<ZmedCompositeKeyMedtype> id)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<ZmedMedtype> findOneByCompositeKey(String bukrs,
			String kodemed, Date datab, Date datbi) {
		if (bukrs == null || kodemed == null || datab == null || datbi == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(zmedMedtypeRepository.findOneByCompositeKey(bukrs, kodemed, datab, datbi));
	}

	@Override
	public Collection<ZmedMedtype> findByBukrs(String bukrs, Date datbi) {
		if (bukrs == null || datbi == null) {
			return Collections.emptyList();
		}
		

		Optional<Collection<ZmedMedtype>> bunchOfZmed
			= Optional.ofNullable(zmedMedtypeRepository.findByBukrs(bukrs, datbi));
		
		return (bunchOfZmed.isPresent()
				? bunchOfZmed.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

	/*@Override
	public Optional<ZmedMedtype> findByEntityName(final String entityName) {
		return Optional.ofNullable(zmedMedtypeRepository.findByKodemed(entityName));
	}*/
}
