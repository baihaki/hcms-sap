package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.IT0014;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.repository.IT0014Repository;
import com.abminvestama.hcms.core.service.api.business.query.IT0014QueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("it0014QueryService")
@Transactional(readOnly = true)
public class IT0014QueryServiceImpl implements IT0014QueryService {

	private IT0014Repository it0014Repository;
	
	@Autowired
	IT0014QueryServiceImpl(IT0014Repository it0014Repository) {
		this.it0014Repository = it0014Repository;
	}
	
	@Override
	public Optional<IT0014> findById(Optional<ITCompositeKeys> id) throws Exception {
		throw new NoSuchMethodException("Method not implemented. Please use method findOneByCompositeKeys instead.");
	}

	@Override
	public Optional<Collection<IT0014>> fetchAll() {
		Optional<Iterable<IT0014>> it0014Iterates = Optional.ofNullable(it0014Repository.findAll());
		if (!it0014Iterates.isPresent()) {
			return Optional.empty();
		}
		
		List<IT0014> listOfIT0014 = new ArrayList<>();
		it0014Iterates.map(it0014Iterable -> {			
			it0014Iterable.forEach(it0014 -> {
				listOfIT0014.add(it0014);
			});
			return listOfIT0014;
		});
		
		return Optional.of(listOfIT0014);
	}

	@Override
	public Optional<IT0014> findOneByCompositeKey(Long pernr, String subty, Date endda, Date begda) {
		if (pernr == null || StringUtils.isBlank(subty) || endda == null || begda == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(it0014Repository.findOneByCompositeKey(pernr, SAPInfoType.RECURRING_PAYMENTS.infoType(), subty, endda, begda));
	}

	@Override
	public Collection<IT0014> findByPernr(Long pernr) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT0014>> bunchOfIT0014 
			= Optional.ofNullable(it0014Repository.findByPernr(pernr, SAPInfoType.RECURRING_PAYMENTS.infoType()));
		
		return (bunchOfIT0014.isPresent()
				? bunchOfIT0014.get().stream().collect(Collectors.toList())
				: Collections.emptyList());		
	}

	@Override
	public Collection<IT0014> findByPernrAndSubty(Long pernr, String subty) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		if (StringUtils.isBlank(subty)) {
			subty = "4ZE5"; // default to subtype '4ZE5'
		}
		
		Optional<Collection<IT0014>> bunchOfIT0014
			= Optional.ofNullable(it0014Repository.findByPernrAndSubty(pernr, SAPInfoType.RECURRING_PAYMENTS.infoType(), subty));

		return (bunchOfIT0014.isPresent() 
					? bunchOfIT0014.get().stream().collect(Collectors.toList())
					: Collections.emptyList());				
	}
}