package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0002;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add methods: findByPernrForPublishedOrUpdated</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add methods: countByGenderAndBukrs, countByAgeAndBukrs</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0002Repository extends CrudRepository<IT0002, ITCompositeKeysNoSubtype> {
	
	@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.id.endda = :endda AND it0002.id.begda = :begda")
	IT0002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr ORDER BY it0002.id.endda DESC")
	Collection<IT0002> findByPernr(@Param("pernr") Long pernr);	
	

	@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.status = :status ORDER BY it0002.id.endda DESC, it0002.id.begda DESC")
	Page<IT0002> findByPernrAndStatus(@Param("pernr") Long pernr, @Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND " +
	"(it0002.status = 'PUBLISHED' OR it0002.status = 'UPDATE_IN_PROGRESS' OR it0002.status = 'UPDATE_REJECTED') " + 
	" ORDER BY it0002.id.endda DESC, it0002.id.begda DESC")
	Page<IT0002> findByPernrForPublishedOrUpdated(@Param("pernr") Long pernr, Pageable pageRequest);

	@Query(nativeQuery=true, value="SELECT count(*) FROM it0001 WHERE bukrs = :bukrs")
	Long countByGenderAndBukrs(@Param("bukrs") String bukrs);

	@Query(nativeQuery=true, value="SELECT count(*) FROM it0002 WHERE gesch = :gesch AND pernr IN (SELECT pernr FROM it0001 WHERE bukrs = :bukrs)")
	Long countByGenderAndBukrs(@Param("gesch") String gesch, 
							@Param("bukrs") String bukrs);
	
	
	@Query(nativeQuery=true, value="SELECT count(*) FROM it0002 WHERE year(gbdat) >= :dat1 AND pernr IN (SELECT pernr FROM it0001 WHERE bukrs = :bukrs)")
	Long countByAgeAndBukrsLower(@Param("dat1")String dat1, 
								@Param("bukrs")String bukrs);

	@Query(nativeQuery=true, value="SELECT count(*) FROM it0002 WHERE year(gbdat) <= :dat1 AND pernr IN (SELECT pernr FROM it0001 WHERE bukrs = :bukrs)")
	Long countByAgeAndBukrsUpper(@Param("dat1")String dat1, 
								@Param("bukrs")String bukrs);

	@Query(nativeQuery=true, value="SELECT count(*) FROM it0002 WHERE year(gbdat) >= :dat1 AND year(gbdat) >= :dat2 AND pernr IN (SELECT pernr FROM it0001 WHERE bukrs = :bukrs)")
	Long countByAgeAndBukrs(String dat1, String dat2, String bukrs);

}