package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;



@Embeddable
@Immutable
public class ZmedCompositeKeyQuotype implements Serializable {
	private static final long serialVersionUID = 5385850040856914239L;

	private String bukrs;
	private String kodequo;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datab", nullable = false)
	private Date datab;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datbi", nullable = false)
	private Date datbi;
	
	public ZmedCompositeKeyQuotype() {}
	
	public ZmedCompositeKeyQuotype(String bukrs, String kodequo, Date datab, Date datbi) {
		
		this.bukrs = bukrs;
		this.kodequo = kodequo;
		this.datab = datab;
		this.datbi = datbi;
	}
	
	
	public String getBukrs() {
		return bukrs;
	}

	public String getKodequo() {
		return kodequo;
	}
	
	public Date getDatab() {
		return datab;
	}

	public Date getDatbi() {
		return datbi;
	}

	
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final ZmedCompositeKeyQuotype key = (ZmedCompositeKeyQuotype) o;
		
		if (key.getBukrs() != null ? key.getBukrs() != (bukrs != null ? bukrs: "") : bukrs != null) {
			return false;
		}
		
		if (key.getKodequo() != null ? key.getKodequo() != (kodequo != null ? kodequo: "") : kodequo != null) {
			return false;
		}
		
		if (key.getDatab() != null ? (key.getDatab().compareTo(datab != null ? datab : new Date()) != 0) : datab != null) {
			return false;
		}
		
		if (key.getDatbi() != null ? (key.getDatbi().compareTo(datbi != null ? datbi : new Date()) != 0) : datbi != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.bukrs != null ? this.bukrs.hashCode() : 0;
		result = result * 31 + (this.kodequo != null ? this.kodequo.hashCode() : 0);
		result = result * 31 + (this.datab != null ? this.datab.hashCode() : 0);
		result = result * 31 + (this.datbi != null ? this.datbi.hashCode() : 0);
		return result;
	}
}
