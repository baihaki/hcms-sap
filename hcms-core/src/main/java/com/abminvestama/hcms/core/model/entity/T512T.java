package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 * 
 * Class that represents <strong>Wage Type</strong> (i.e. T512T in SAP).
 *
 */
@Entity
@Table(name = "t512t")
public class T512T implements Serializable {

	private static final long serialVersionUID = -4529730583673801758L;
	
	@Id
	@Column(name = "lgart", nullable = false, unique = true)
	private String lgart;
	
	@Column(name = "lgtxt", nullable = false)
	private String lgtxt;
	
	public T512T() {}
	
	public String getLgart() {
		return lgart;
	}
	
	public String getLgtxt() {
		return lgtxt;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final T512T obj = (T512T) o;
		
		if (obj.getLgart() != null ? !obj.getLgart().equalsIgnoreCase(lgart) : lgart != null) {
			return false;
		}
		
		if (obj.getLgtxt() != null ? !obj.getLgtxt().equalsIgnoreCase(lgtxt) : lgtxt != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.getLgart() != null ? this.getLgart().hashCode() : 0;
		result = result * 31 + (this.getLgtxt() != null ? this.getLgtxt().hashCode() : 0);
		return result;
	}
}