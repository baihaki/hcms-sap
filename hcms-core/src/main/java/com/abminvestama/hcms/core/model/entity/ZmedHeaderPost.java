package com.abminvestama.hcms.core.model.entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "zmed_header_post")
public class ZmedHeaderPost extends SAPAbstractEntityZmed<ZmedCompositeKeyHeaderPost>{

	private static final long serialVersionUID = 2496232738510047249L;

	public ZmedHeaderPost() {}


	public ZmedHeaderPost(ZmedCompositeKeyHeaderPost id) {
		this();
		this.id = id;
	}



	@Column(name = "obj_type", nullable = false, insertable = false, updatable = false)
	private String objType;
	
	@Column(name = "obj_key", nullable = false, insertable = false, updatable = false)
	private String objKey;
	
	@Column(name = "obj_sys", nullable = false, insertable = false, updatable = false)
	private String objSys;
	
	@Column(name = "bus_act")
	private String busAct;
	
	@Column(name = "username")
	private String username;
	
	@Column(name = "header_txt")
	private String headerTxt;
	

	@ManyToOne
	@JoinColumn(name = "compCode", referencedColumnName = "bukrs", nullable = false, insertable = false, updatable = false)
	private T001 compCode;
	//@Column(name = "compCode")
	//private String compCode;

	@Temporal(TemporalType.DATE)
	@Column(name = "doc_date")
	private Date docDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "pstng_date")
	private Date pstngDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "trans_date")
	private Date transDate;
	
	@Column(name = "fisc_year")
	private Byte fiscYear;
	
	@Column(name = "fis_period")
	private Byte fisPeriod;

	@Column(name = "doc_type")
	private String docType;
	
	@Column(name = "ref_doc_no")
	private String refDocNo;
	
	@Column(name = "ac_doc_no")
	private String acDocNo;
	
	@Column(name = "obj_key_r")
	private String objKeyR;
	
	@Column(name = "reason_rev")
	private String reasonRev;
	
	@Column(name = "compo_acc")
	private String compoAcc;

	@Column(name = "ref_doc_no_long")
	private String refDocNoLong;
	
	@Column(name = "acc_principle")
	private String accPrinciple;
	
	@Column(name = "neg_posting")
	private String negPosting;
	
	@Column(name = "obj_key_inv")
	private String objKeyInv;
	
	@Column(name = "bill_category")
	private String billCategory;
	
	@Column(name = "logsys")
	private String logsys;

	@Temporal(TemporalType.DATE)
	@Column(name = "vatdate")
	private Date vatdate;

	public String getObjType() {
		return objType;
	}

	public void setObjType(String objType) {
		this.objType = objType;
	}

	public String getObjKey() {
		return objKey;
	}

	public void setObjKey(String objKey) {
		this.objKey = objKey;
	}

	public String getObjSys() {
		return objSys;
	}

	public void setObjSys(String objSys) {
		this.objSys = objSys;
	}

	public String getBusAct() {
		return busAct;
	}

	public void setBusAct(String busAct) {
		this.busAct = busAct;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getHeaderTxt() {
		return headerTxt;
	}

	public void setHeaderTxt(String headerTxt) {
		this.headerTxt = headerTxt;
	}

	public T001 getCompCode() {
		return compCode;
	}

	public void setCompCode(T001 compCode) {
		this.compCode = compCode;
	}

	public Date getDocDate() {
		return docDate;
	}

	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}

	public Date getPstngDate() {
		return pstngDate;
	}

	public void setPstngDate(Date pstngDate) {
		this.pstngDate = pstngDate;
	}

	public Date getTransDate() {
		return transDate;
	}

	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}

	public Byte getFiscYear() {
		return fiscYear;
	}

	public void setFiscYear(Byte fiscYear) {
		this.fiscYear = fiscYear;
	}

	public Byte getFisPeriod() {
		return fisPeriod;
	}

	public void setFisPeriod(Byte fisPeriod) {
		this.fisPeriod = fisPeriod;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getRefDocNo() {
		return refDocNo;
	}

	public void setRefDocNo(String refDocNo) {
		this.refDocNo = refDocNo;
	}

	public String getAcDocNo() {
		return acDocNo;
	}

	public void setAcDocNo(String acDocNo) {
		this.acDocNo = acDocNo;
	}

	public String getObjKeyR() {
		return objKeyR;
	}

	public void setObjKeyR(String objKeyR) {
		this.objKeyR = objKeyR;
	}

	public String getReasonRev() {
		return reasonRev;
	}

	public void setReasonRev(String reasonRev) {
		this.reasonRev = reasonRev;
	}

	public String getCompoAcc() {
		return compoAcc;
	}

	public void setCompoAcc(String compoAcc) {
		this.compoAcc = compoAcc;
	}

	public String getRefDocNoLong() {
		return refDocNoLong;
	}

	public void setRefDocNoLong(String refDocNoLong) {
		this.refDocNoLong = refDocNoLong;
	}

	public String getAccPrinciple() {
		return accPrinciple;
	}

	public void setAccPrinciple(String accPrinciple) {
		this.accPrinciple = accPrinciple;
	}

	public String getNegPosting() {
		return negPosting;
	}

	public void setNegPosting(String negPosting) {
		this.negPosting = negPosting;
	}

	public String getObjKeyInv() {
		return objKeyInv;
	}

	public void setObjKeyInv(String objKeyInv) {
		this.objKeyInv = objKeyInv;
	}

	public String getBillCategory() {
		return billCategory;
	}

	public void setBillCategory(String billCategory) {
		this.billCategory = billCategory;
	}

	public String getLogsys() {
		return logsys;
	}

	public void setLogsys(String logsys) {
		this.logsys = logsys;
	}

	public Date getVatdate() {
		return vatdate;
	}

	public void setVatdate(Date vatdate) {
		this.vatdate = vatdate;
	}
}

