package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.IT0014;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface IT0014Repository extends CrudRepository<IT0014, ITCompositeKeys> {
	
	@Query("FROM IT0014 it0014 WHERE it0014.pernr = :pernr AND it0014.id.infty = :infty AND it0014.id.subty = :subty AND it0014.id.endda = :endda AND it0014.id.begda = :begda")
	IT0014 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty,
			@Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM IT0014 it0014 WHERE it0014.pernr = :pernr AND it0014.id.infty = :infty ORDER BY it0014.id.subty ASC, it0014.id.endda DESC")
	Collection<IT0014> findByPernr(@Param("pernr") Long pernr, @Param("infty") String infty);
	
	@Query("FROM IT0014 it0014 WHERE it0014.pernr = :pernr AND it0014.id.infty = :infty AND it0014.id.subty = :subty ORDER BY it0014.id.endda DESC")
	Collection<IT0014> findByPernrAndSubty(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty);		
}