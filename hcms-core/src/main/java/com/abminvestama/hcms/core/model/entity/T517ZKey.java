package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;

import org.hibernate.annotations.Immutable;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Immutable
public class T517ZKey implements Serializable {

	private static final long serialVersionUID = 6661653037461899358L;

	private String slart;
	
	private String faart;
	
	public T517ZKey() {}
	
	public T517ZKey(String slart, String faart) {
		this.slart = slart;
		this.faart = faart;
	}
	
	public String getSlart() {
		return slart;
	}
	
	public String getFaart() {
		return faart;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final T517ZKey key = (T517ZKey) o;
		
		if (slart != null ? !slart.equalsIgnoreCase(key.getSlart()) : key.slart != null) {
			return false;
		}
		
		if (faart != null ? !faart.equalsIgnoreCase(key.getFaart()) : key.faart != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = slart != null ? slart.hashCode() : 0;
		result = result * 31 + (faart != null ? faart.hashCode() : 0);
		return result;
	}
}