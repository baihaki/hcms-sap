package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.IT0077;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.repository.IT0077Repository;
import com.abminvestama.hcms.core.service.api.business.query.IT0077QueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("it0077QueryService")
@Transactional(readOnly = true)
public class IT0077QueryServiceImpl implements IT0077QueryService {

	private IT0077Repository it0077Repository;
	
	@Autowired
	IT0077QueryServiceImpl(IT0077Repository it0077Repository) {
		this.it0077Repository = it0077Repository;
	}
	
	@Override
	public Optional<IT0077> findById(Optional<ITCompositeKeysNoSubtype> id) throws Exception {
		throw new NoSuchMethodException("Method not implemented. Please use method findOneByCompositeKeys instead.");		
	}

	@Override
	public Optional<Collection<IT0077>> fetchAll() {
		Optional<Iterable<IT0077>> it0077Iterates = Optional.ofNullable(it0077Repository.findAll());
		
		List<IT0077> listOfIT0077 = new ArrayList<>();
		
		return it0077Iterates.map(it0077Iterable -> {			
			it0077Iterable.forEach(it0077 -> {
				listOfIT0077.add(it0077);
			});
			return listOfIT0077;
		});
	}

	@Override
	public Optional<IT0077> findOneByCompositeKey(Long pernr, Date endda, Date begda) {
		if (pernr == null || endda == null || begda == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(it0077Repository.findOneByCompositeKey(pernr, endda, begda));
	}

	@Override
	public Collection<IT0077> findByPernr(Long pernr) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT0077>> bunchOfIT0077 
			= Optional.ofNullable(it0077Repository.findByPernr(pernr));
		
		return (bunchOfIT0077.isPresent()
				? bunchOfIT0077.get().stream().collect(Collectors.toList())
				: Collections.emptyList());				
	}
}