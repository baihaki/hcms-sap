package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0241;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add findByPernrAndStatus method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findByStatus method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0241Repository extends CrudRepository<IT0241, ITCompositeKeysNoSubtype> {

	@Query("FROM IT0241 it0241 WHERE it0241.pernr = :pernr AND it0241.id.endda = :endda AND it0241.id.begda = :begda")
	IT0241 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM IT0241 it0241 WHERE it0241.pernr = :pernr ORDER BY it0241.id.endda DESC")
	Collection<IT0241> findByPernr(@Param("pernr") Long pernr);		

	@Query("FROM IT0241 it0241 WHERE it0241.status = :status ORDER BY it0241.id.endda DESC")
	Page<IT0241> findByStatus(@Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0241 it0241 WHERE it0241.pernr = :pernr AND it0241.status = :status ORDER BY it0241.id.endda DESC")
	Page<IT0241> findByPernrAndStatus(@Param("pernr") Long pernr, @Param("status") DocumentStatus status, Pageable pageRequest);
}