package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.CaOrder;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaOrderRepository extends CrudRepository<CaOrder, Long> {
	
	@Query("FROM CaOrder ca_order WHERE ca_order.id = :orderId")
	Optional<CaOrder> findByOrderId(Long orderId);

	String FIND_BY_ORGEH_COUNT = "SELECT COUNT(*) "
			+ "FROM ca_order AS cao "
			+ "INNER JOIN ca_depts_cost_center AS ca_depts_cc "
				+ "ON ca_depts_cc.kostl = cao.kostl AND ca_depts_cc.kokrs = '1000' "
			+ "WHERE ca_depts_cc.orgeh = :orgeh ";
	
	String FIND_BY_ORGEH_QUERY = "SELECT cao.* "
			+ "FROM ca_order AS cao "
			+ "INNER JOIN ca_depts_cost_center AS ca_depts_cc "
				+ "ON ca_depts_cc.kostl = cao.kostl AND ca_depts_cc.kokrs = '1000' "
			+ "WHERE ca_depts_cc.orgeh = :orgeh "
			+ "LIMIT :limit OFFSET :offset";
	
	@Query(nativeQuery=true, value=FIND_BY_ORGEH_COUNT)
	int findByOrgehCount(@Param("orgeh") Long orgeh);

	@Query(nativeQuery=true, value=FIND_BY_ORGEH_QUERY)
	Collection<CaOrder> findByOrgehWithPaging(@Param("orgeh") Long orgeh, @Param("limit") int limit, @Param("offset") int offset);
	
	String FIND_BY_CRITERIA_COUNT = "SELECT COUNT(*) "
			+ "FROM ca_order AS cao "
			+ "INNER JOIN ca_depts_cost_center AS ca_depts_cc "
				+ "ON ca_depts_cc.kostl = cao.kostl AND ca_depts_cc.kokrs = '1000' "
			+ "INNER JOIN ca_gl_account AS glacc "
				+ "ON glacc.id = cao.gl_account_id "
			+ "WHERE ca_depts_cc.orgeh = :orgeh AND glacc.id = :glAccountId ";
	
	String FIND_BY_CRITERIA_QUERY = "SELECT cao.* "
			+ "FROM ca_order AS cao "
			+ "INNER JOIN ca_depts_cost_center AS ca_depts_cc "
				+ "ON ca_depts_cc.kostl = cao.kostl AND ca_depts_cc.kokrs = '1000' "
			+ "INNER JOIN ca_gl_account AS glacc "
				+ "ON glacc.id = cao.gl_account_id "
			+ "WHERE ca_depts_cc.orgeh = :orgeh AND glacc.id = :glAccountId "
			+ "LIMIT :limit OFFSET :offset";
	
	@Query(nativeQuery=true, value=FIND_BY_CRITERIA_COUNT)
	int findByCriteriaCount(@Param("orgeh") Long orgeh, @Param("glAccountId") Long glAccountId);

	@Query(nativeQuery=true, value=FIND_BY_CRITERIA_QUERY)
	Collection<CaOrder> findByCriteriaWithPaging(@Param("orgeh") Long orgeh, @Param("glAccountId") Long glAccountId, 
			@Param("limit") int limit, @Param("offset") int offset);

	Page<CaOrder> findAll(Pageable pageRequest);
}
