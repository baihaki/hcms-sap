package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.SHT706B1;
import com.abminvestama.hcms.core.model.entity.SHT706B1Key;
import com.abminvestama.hcms.core.repository.SHT706B1Repository;
import com.abminvestama.hcms.core.service.api.business.query.SHT706B1QueryService;

@Service("sht706b1QueryService")
@Transactional(readOnly = true)
public class SHT706B1QueryServiceImpl implements SHT706B1QueryService {

	private SHT706B1Repository sht706b1Repository;
	
	@Autowired
	SHT706B1QueryServiceImpl(SHT706B1Repository sht706b1Repository) {
		this.sht706b1Repository = sht706b1Repository;
	}
	
	/*@Override
	public Optional<SHT706B1> findById(Optional<String> id) throws Exception {
		return id.map(pk -> sht706b1Repository.findOne(pk));
	}*/

	@Override
	public Optional<Collection<SHT706B1>> fetchAll() {
		Optional<Iterable<SHT706B1>> sht706b1Iterable = Optional.ofNullable(sht706b1Repository.findAll());
		if (!sht706b1Iterable.isPresent()) {
			return Optional.empty();
		}
		
		List<SHT706B1> listOfSHT706B1 = new ArrayList<>();
		return sht706b1Iterable.map(iteratesSHT706B1 -> {
			iteratesSHT706B1.forEach(sht706b1 -> {
				listOfSHT706B1.add(sht706b1);
			});
			
			return listOfSHT706B1;
		});				
	}

	@Override
	public Optional<SHT706B1> findById(Optional<SHT706B1Key> id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<SHT706B1> findByMoreiNE(String morei) {
		if (morei == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<SHT706B1>> empCost
			= Optional.ofNullable(sht706b1Repository.findByMoreiNE(morei));
		
		return (empCost.isPresent()
				? empCost.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}
}