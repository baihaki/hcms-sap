package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;



@Embeddable
@Immutable
public class T705HKey implements Serializable {
	private static final long serialVersionUID = 5385850040856914239L;

	/*
	private String spras;
	private String grawg;
	private String zeity;
	private Long moabw;
	*/
	private String pinco;
	
	/*
	public String getSpras() {
		return spras;
	}

	public String getGrawg() {
		return grawg;
	}

	public String getZeity() {
		return zeity;
	}

	public Long getMoabw() {
		return moabw;
	}
	*/

	public String getPinco() {
		return pinco;
	}

	public T705HKey() {}
	
	public T705HKey(String spras, String grawg, String zeity, Long moabw, String pinco) {
		/*
		this.spras = spras;
		this.grawg = grawg;
		this.zeity = zeity;
		this.moabw = moabw;
		*/
		this.pinco = pinco;
	}
	
	
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final T705HKey key = (T705HKey) o;

		/*
		if (key.getSpras() != null ? key.getSpras() != (spras != null ? spras: "") : spras != null) {
			return false;
		}
		
		if (key.getGrawg() != null ? key.getGrawg() != (grawg != null ? grawg: "") : grawg != null) {
			return false;
		}
		
		if (key.getZeity() != null ? key.getZeity() != (zeity != null ? zeity: "") : zeity != null) {
			return false;
		}
		

		if (key.getMoabw() != null ? key.getMoabw() != (moabw != null ? moabw: "") : moabw != null) {
			return false;
		}
		*/
		
		if (key.getPinco() != null ? key.getPinco() != (pinco != null ? pinco: "") : pinco != null) {
			return false;
		}
		
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = 0;
		/*
		int result = this.spras != null ? this.spras.hashCode() : 0;
		result = result * 31 + (this.grawg != null ? this.grawg.hashCode() : 0);
		result = result * 31 + (this.zeity != null ? this.zeity.hashCode() : 0);
		result = result * 31 + (this.moabw != null ? this.moabw.hashCode() : 0);
		*/
		result = result * 31 + (this.pinco != null ? this.pinco.hashCode() : 0);
		return result;
	}
}
