package com.abminvestama.hcms.core.model.constant;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public enum WorkflowType {

	UPDATE_HR_MASTER,
	CREATE_HR_MASTER;
}
