package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.ZmedQuotype;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;


public interface ZmedQuotypeCommandService extends DatabaseCommandService<ZmedQuotype> {
}