package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;


/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add field on composite key: clmno</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Embeddable
@Immutable
public class ZmedCompositeKeyTrxitm implements Serializable {
	private static final long serialVersionUID = 5385850040856914239L;

	private String itmno;
	private String clmno;
	
	public ZmedCompositeKeyTrxitm() {}
	
	public ZmedCompositeKeyTrxitm(String itmno, String clmno) {
		this.itmno = itmno;
		this.clmno = clmno;
	}
	
	public String getItmno() {
		return itmno;
	}
	
	public String getClmno() {
		return clmno;
	}
			
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final ZmedCompositeKeyTrxitm key = (ZmedCompositeKeyTrxitm) o;
		
		if (key.getItmno() != null ? key.getItmno() != (itmno != null ? itmno: "") : itmno != null) {
			return false;
		}
		
		if (key.getClmno() != null ? key.getClmno() != (clmno != null ? clmno: "") : clmno != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.itmno != null ? this.itmno.hashCode() : 0;
		return result;
	}
}
