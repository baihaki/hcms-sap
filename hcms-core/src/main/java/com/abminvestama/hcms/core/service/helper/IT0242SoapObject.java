package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0242;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0242SoapObject {
	
	private String employeenumber;
	private String validitybegin;
	private String validityend;
	private String jamid;
	private String marst;
	
	private IT0242SoapObject() {}
	
	public IT0242SoapObject(IT0242 object) {
		if (object == null) {
			new IT0242SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			jamid = object.getJamId();
			marst = object.getMarst();
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}

	public String getJamid() {
		return jamid;
	}

	public String getMarst() {
		return marst;
	}


}
