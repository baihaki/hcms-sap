package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Entity
@Table(name = "cfg_region_types", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "ident_name" })
})
public class RegionType extends AbstractEntity {

	private static final long serialVersionUID = -1769220255116935637L;

	@Column(name = "ident_name", unique = true, nullable = false)
	private String identName;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "description", nullable = false)
	private String description;
	
	@Column(name = "system", nullable = true)
	private boolean system;
	
	public String getIdentName() {
		return identName;
	}
	
	public void setIdentName(String identName) {
		this.identName = identName;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean isSystem() {
		return system;
	}
	
	public void setSystem(boolean system) {
		this.system = system;
	}
}