package com.abminvestama.hcms.core.service.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.User;

/**
 * 
 * @author baihaki.pru@gmail.com
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("mailService")
public class MailService {
	
	//private String clientUrl = "http://localhost:3000/";
	private String clientUrl = "http://10.1.14.18:3000/";
	
	private String mailHeader = "Dear <b>EMPLOYEE_NAME</b><br><br>";
	private String mailContent = "Your ENTITY_DESCRIPTION below :<br><br>Name = EMPLOYEE_NAME<br>SN = EMPLOYEE_SSN<br>ENTITY_ID = ENTITY_KEY<br><br>";
	private String mailFooter = "<br>To view this request please click this <a href=\"CLIENT_URL\">Show Request</a>";
	private String mailFooterApproval = "<br>To view your INFOTYPE_DESCRIPTION please <a href=\"CLIENT_URL\">Click Here</a>";
	
	private String mailMasterDataUpdate = "has been created, waiting for approval from HR Admin or Your Superior.";
	private String mailMasterDataApproved = "has been approved by HR Admin or Your Superior.";
	private String mailMasterDataRejected = "has been rejected by HR Admin or Your Superior.";
	private String mailMedicalUpdate = "has been created, and waiting for the following process.";
	private String mailExpenseUpdate = "has been created, waiting for your released and submitted required data to HR Admin or Your Superior.";
	private String mailCashAdvanceUpdate = "has been created, waiting for approval from Department Head or Director.";
	
	public String getMailMasterDataUpdate() {
		return mailMasterDataUpdate;
	}

	public String getMailMasterDataApproved() {
		return mailMasterDataApproved;
	}

	public String getMailMasterDataRejected() {
		return mailMasterDataRejected;
	}

	public String getMailMedicalUpdate() {
		return mailMedicalUpdate;
	}

	public String getMailExpenseUpdate() {
		return mailExpenseUpdate;
	}
	
	public String getMailCashAdvanceUpdate() {
		return mailCashAdvanceUpdate;
	}

	public Map<String, String> composeEmail(EventLog entity, User user) {
		Map<String, String> composeMap = new HashMap<String, String>();
		String mailSubject = "";
		String mailBody = mailHeader.replace("EMPLOYEE_NAME", user.getEmployee().getSname())
				.concat(mailContent.replace("EMPLOYEE_NAME", user.getEmployee().getSname()));

		String[] keys = entity.getEntityObjectId().split(",");
		Map<String, String> keyMap = new HashMap<String, String>();
		for (int i=0; i<keys.length; i++) {
			String[] pair = keys[i].split("=");
			keyMap.put(pair[0], pair[1]);
		}
		
		String fullUrl = "";
		boolean includeUrl = true;
		
		clientUrl = ((JavaMailSenderImpl) mailSender).getJavaMailProperties().getProperty("mail.notification.url", "http://10.1.14.18:3000/");
		
		if (entity.getHcmsEntity().getEntityName().startsWith("IT")) {
			String entityKey = entity.getHcmsEntity().getEntityName().concat("_").concat(StringUtils.defaultString(keyMap.get("pernr"), "-"))
					.concat(keyMap.get("infty") == null ? "" : "_".concat(keyMap.get("infty")))
					.concat(keyMap.get("subty") == null ? "" : "_".concat(keyMap.get("subty")))
					.concat("_").concat(StringUtils.defaultString(keyMap.get("endda"), "-"))
			        .concat("_").concat(StringUtils.defaultString(keyMap.get("begda"), "-"));
			
			boolean isAbsence = keyMap.get("infty") != null && ("2001".equals(keyMap.get("infty")) || "2002".equals(keyMap.get("infty"))); 
			
			mailSubject = entity.getOperationType().toString().concat(" ").concat(
					StringUtils.defaultString(entity.getHcmsEntity().getEntityDesc(), entity.getHcmsEntity().getEntityName()));
			mailBody = mailBody.replace("ENTITY_DESCRIPTION", entity.getOperationType().equals(OperationType.UPDATE) ?
					"data update request" : "data request").replace("EMPLOYEE_SSN", user.getEmployee().getPernr().toString())
					.replace("ENTITY_ID", "Request Number").replace("ENTITY_KEY", entityKey).concat(mailMasterDataUpdate);
			
			String status = isAbsence ? "" : StringUtils.defaultString(entity.getStatus(), DocumentStatus.INITIAL_DRAFT.toString());
			String entityParams = "ssn=".concat(StringUtils.defaultString(keyMap.get("pernr")))
					.concat("&infty=").concat(keyMap.get("infty") == null ? entity.getHcmsEntity().getEntityName().substring(2) : keyMap.get("infty"))
					.concat(keyMap.get("subty") == null ? "" : "&subty=".concat(keyMap.get("subty")))
					.concat("&endda=").concat(StringUtils.defaultString(keyMap.get("endda")))
			        .concat("&begda=").concat(StringUtils.defaultString(keyMap.get("begda")))
			        .concat(isAbsence ? "" : "&status=").concat(status);
			
			//fullUrl = "http://localhost:3000/my_workflows/master_data?ssn=39954&infty=it0105&subty=CELL&endda=9999-12-31&begda=2016-11-01&status=UPDATE_IN_PROGRESS";
			fullUrl = clientUrl.concat(isAbsence ?  "absence/summary?" : "my_workflows/master_data?").concat(entityParams);
					
		} else if (entity.getHcmsEntity().getEntityName().equals("ZmedTrxhdr")) {
			String entityKey = StringUtils.defaultString(keyMap.get("zclmno"), "-");
			String bukrs = StringUtils.defaultString(keyMap.get("bukrs"), "-");
			
			mailSubject = entity.getOperationType().toString().concat(" ").concat("Medical Claim");
			mailBody = mailBody.replace("ENTITY_DESCRIPTION", "medical claim").replace("EMPLOYEE_SSN", user.getEmployee().getPernr().toString())
					.replace("ENTITY_ID", "Claim Number").replace("ENTITY_KEY", entityKey).concat(mailMedicalUpdate);
			
			if (StringUtils.isNotEmpty(entity.getStatus())) {
				//fullUrl = "http://localhost:3000/tasks/tasks_medical/page/1?status=menunggu-persetujuan&bukrs=9900&clmno=9900000541";
				fullUrl = clientUrl.concat("medical/page/1?status=waiting-for-approve&bukrs=").concat(bukrs).
						concat("&clmno=").concat(entityKey);
			} else {
				mailBody = mailBody.replace("Claim Number = ", "Temporary Claim Number = ");
				includeUrl = false;
			}
			
		} else if (entity.getHcmsEntity().getEntityName().equals("PtrvHead")) {
			String entityKey = StringUtils.defaultString(keyMap.get("reinr"), "-");
			//String ssn = user.getEmployee().getPernr().toString();
			String ssn = StringUtils.defaultString(keyMap.get("pernr"), "-");
			String hdvrs = StringUtils.defaultString(keyMap.get("hdvrs"), "-");
			
			mailSubject = entity.getOperationType().toString().concat(" ").concat("Expense Claim");
			mailBody = mailBody.replace("ENTITY_DESCRIPTION", "expense claim").replace("EMPLOYEE_SSN", ssn)
					.replace("ENTITY_ID", "Expense Claim Number").replace("ENTITY_KEY", entityKey).concat(mailExpenseUpdate);
			
			//fullUrl = "http://localhost:3000/employee_cost/claim/summary/reinr/1476695544140?ssn=32291&hdvrs=1&form=0";
			fullUrl = clientUrl.concat("employee_cost/claim/summary/reinr/").concat(entityKey).
					concat("?ssn=").concat(ssn).concat("&hdvrs=").concat(hdvrs).concat("&form=0");
		} else if (entity.getHcmsEntity().getEntityName().equals("CaReqHdr")) {
			String entityKey = StringUtils.defaultString(keyMap.get("req_no"), "-");
			//String ssn = user.getEmployee().getPernr().toString();
			String ssn = StringUtils.defaultString(keyMap.get("pernr"), "-");
			String advanceCode = StringUtils.defaultString(keyMap.get("ac"), "-");
			String advanceDesc = "Request of ";
			if ("PC".equals(advanceCode)) {
				advanceDesc = advanceDesc.concat("Petty Cash Advance");
			} else if ("BT".equals(advanceCode)) {
				advanceDesc = advanceDesc.concat("Bank Transfer Advance");
			} else if ("TA".equals(advanceCode)) {
				advanceDesc = advanceDesc.concat("Travel Advance");
			}
			
			mailSubject = entity.getOperationType().toString().concat(" ").concat(advanceDesc);
			mailBody = mailBody.replace("ENTITY_DESCRIPTION", advanceDesc).replace("EMPLOYEE_SSN", ssn)
					.replace("ENTITY_ID", "Advance Request Number").replace("ENTITY_KEY", entityKey).concat(mailCashAdvanceUpdate);
			
			//fullUrl = "http://localhost:3000/cash_advance/claim/summary/req_no/ab01ac02";
			fullUrl = clientUrl.concat("cash_advance/claim/summary/req_no/").concat(entityKey);
		}
		
		if (includeUrl) {
			String footer = mailFooter.replace("CLIENT_URL", fullUrl);
			if (entity.getHcmsEntity().getEntityName().startsWith("IT") && 
					(entity.getOperationType().equals(OperationType.APPROVAL) || entity.getOperationType().equals(OperationType.DOCUMENT_REJECT)) ) {
				String infotypeDesc = StringUtils.defaultString(entity.getHcmsEntity().getEntityDesc(), entity.getHcmsEntity().getEntityName());
				String tabEmployee = "tab_7";
				if (entity.getHcmsEntity().getEntityName().equals("IT0023")) {
					tabEmployee = "tab_6";
				} else if (entity.getHcmsEntity().getEntityName().equals("IT0022")) {
					tabEmployee = "tab_5";
				} else if (entity.getHcmsEntity().getEntityName().equals("IT0021")) {
					tabEmployee = keyMap.get("subty").equals("7") ? "tab_3" : "tab_4";
					if (keyMap.get("subty").equals("7"))
						infotypeDesc = "Emergency Information";
				} else if (entity.getHcmsEntity().getEntityName().equals("IT0009")) {
					tabEmployee = "tab_2";
				}
				fullUrl = clientUrl.concat("employee_data#").concat(tabEmployee);
				footer = mailFooterApproval.replace("INFOTYPE_DESCRIPTION", infotypeDesc).replace("CLIENT_URL", fullUrl);
			}
			mailBody = mailBody.concat(footer);
			composeMap.put("footer", footer);
		}
		
		composeMap.put("subject", mailSubject);
		composeMap.put("body", mailBody);
		return composeMap;
	}
    
    @Autowired
    private JavaMailSender mailSender;
    
    @Autowired
    private Session mailSession;
    
    class MailThread implements Runnable {
    	
    	MimeMessage message;
    	
    	MailThread(MimeMessage message) {
    		this.message = message;
    	}
    	
		@Override
		public void run() {
			try {
				//Transport.send(message);
				mailSender.send(message);
				/* Remove "modify mail properties" (after re-send email)
				if (((JavaMailSenderImpl) mailSender).getPort() == 587) {
	        	    ((JavaMailSenderImpl) mailSender).setPort((int) ((JavaMailSenderImpl) mailSender).getJavaMailProperties().get("mail.smtp.old.port"));
	        	    ((JavaMailSenderImpl) mailSender).getJavaMailProperties().put("mail.smtp.socketFactory.port", String.valueOf(
	        	    		((JavaMailSenderImpl) mailSender).getPort()));
	        	}
				*/
			} catch (MailException e) {
	        	System.out.println("Thread MailException:" + e.getMessage());
	        	/* Remove re-send email
	        	if (((JavaMailSenderImpl) mailSender).getPort() != 587) {
	        		((JavaMailSenderImpl) mailSender).getJavaMailProperties().put("mail.smtp.old.port", ((JavaMailSenderImpl) mailSender).getPort());
	        	    ((JavaMailSenderImpl) mailSender).setPort(587);
	        	    ((JavaMailSenderImpl) mailSender).getJavaMailProperties().put("mail.smtp.socketFactory.port", "587");
	        	    try {
						sendMail(message.getSubject().split(" : ")[0], message.getSubject().split(" : ")[1], (String) message.getContent());
					} catch (MessagingException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
	        	}
	        	*/
			}
		}
		
    }
    
    public void sendMail(String to, String subject, String body) {
    	 
    	body = body.concat("<br><br>Regards,<br>ABM HCMS Application<br>http://abmhcms.abm-investama.co.id<br><br>").
    			concat("<i>This email automatic generated by system, please do not reply</i>");
    	/*body = body.concat("<br><br><i>sent from ").concat(System.getProperty("user.name")).concat("\'s ").
    			concat(System.getProperty("os.name")).concat(" machine</i>");*/
    	MimeMessage message = new MimeMessage(mailSession);
    	try {
    		//message.setFrom(new InternetAddress(((JavaMailSenderImpl) mailSender).getUsername()));
    		message.setFrom(new InternetAddress(StringUtils.defaultString(((JavaMailSenderImpl) mailSender).getUsername(), "hcms@abm-investama.co.id")));
    		
        	message.setRecipients(RecipientType.TO, to);
        	message.setSubject(subject);
        	
    		//message.setRecipients(RecipientType.TO, "ahmad@baihaki.net");
        	//message.setRecipients(RecipientType.TO, "devid.akmalhadi@abm-investama.co.id");
    		//message.setRecipients(RecipientType.TO, "andang.tirta@gmail.com");
        	message.setRecipients(RecipientType.BCC,
        			((JavaMailSenderImpl) mailSender).getJavaMailProperties().getProperty("mail.bcc", "baihaki.pru@gmail.com"));
        	//message.setSubject(to + " : " + subject);        	
    		message.setContent(body, "text/html");
    		new Thread(new MailThread(message)).start();
		} catch (MessagingException e) {
        	System.out.println("MessagingException:" + e.getMessage());
		}
    }
    
}