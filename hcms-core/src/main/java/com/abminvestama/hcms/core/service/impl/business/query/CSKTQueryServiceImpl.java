package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.CSKT;
import com.abminvestama.hcms.core.model.entity.CSKTKey;
import com.abminvestama.hcms.core.repository.CSKTRepository;
import com.abminvestama.hcms.core.service.api.business.query.CSKTQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("csktQueryService")
@Transactional(readOnly = true)
public class CSKTQueryServiceImpl implements CSKTQueryService {
	
	private CSKTRepository csktRepository;
	
	@Autowired
	CSKTQueryServiceImpl(CSKTRepository csktRepository) {
		this.csktRepository = csktRepository;
	}

	@Override
	public Optional<CSKT> findById(Optional<CSKTKey> id) throws Exception {
		return id.map(pk -> csktRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<CSKT>> fetchAll() {
		List<CSKT> listOfCSKT = new ArrayList<>();
		Optional<Iterable<CSKT>> bunchOfCSKT = Optional.ofNullable(csktRepository.findAll());
		return bunchOfCSKT.map(iter -> {
			iter.forEach(cskt -> {
				listOfCSKT.add(cskt);
			});
			return listOfCSKT;
		});
	}
	
	@Override
	public Page<CSKT> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return csktRepository.findAll(pageRequest);
	}	

	@Override
	public Page<CSKT> fetchAllEmpCSKTPage(int pageNumber, int pageSize) {
		Pageable pageRequest;
		int total = csktRepository.findAllEmployeeCSKTCount();
		if (total < 1) total = 1;
		
		if (pageSize == -1) {
			pageRequest = createPageRequest(pageNumber, total);
		}
		else {
			pageRequest = createPageRequest(pageNumber, pageSize);
		}
		List<CSKT> listOfCSKT = csktRepository
				.findAllEmployeeCSKTQuery(total, pageRequest.getOffset()).stream()
				.collect(Collectors.toList());
		return new PageImpl<CSKT>(listOfCSKT, pageRequest, total);
	}
}