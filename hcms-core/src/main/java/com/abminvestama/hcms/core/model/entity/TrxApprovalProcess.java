package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.abminvestama.hcms.core.model.constant.MainConstants;

@Entity
@Table(name = MainConstants.TRANSACTION_APPROVAL_PROCESS)
public class TrxApprovalProcess implements Serializable {
	
	private static final long serialVersionUID = -9156508271638640906L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	protected String id;
	
	@ManyToOne
	@JoinColumn(name = "cfg_entity_id", referencedColumnName = "id")
	private HCMSEntity cfgEntityId;
	
	@Column(name = "entity_object_id")
	private String entityObjectId;
	
	@Column(name = "seq")
	private Integer seq;
	
	@Column(name = "process")
	private Integer process;
	
	@ManyToOne
	@JoinColumn(name = "m_role_id", referencedColumnName = "id")
	private Role roleId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approval_at")
	private Date approvalAt;
	
	@ManyToOne
	@JoinColumn(name = "approval_by", referencedColumnName = "id")
	private User approvalBy;

	@Column(name = "approval_action")
	private String approvalAction;
	
	@Column(name = "approval_note")
	private String approvalNote;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public HCMSEntity getCfgEntityId() {
		return cfgEntityId;
	}

	public void setCfgEntityId(HCMSEntity cfgEntityId) {
		this.cfgEntityId = cfgEntityId;
	}

	public String getEntityObjectId() {
		return entityObjectId;
	}

	public void setEntityObjectId(String entityObjectId) {
		this.entityObjectId = entityObjectId;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Integer getProcess() {
		return process;
	}

	public void setProcess(Integer process) {
		this.process = process;
	}

	public Role getRoleId() {
		return roleId;
	}

	public void setRoleId(Role roleId) {
		this.roleId = roleId;
	}

	public Date getApprovalAt() {
		return approvalAt;
	}

	public void setApprovalAt(Date approvalAt) {
		this.approvalAt = approvalAt;
	}

	public User getApprovalBy() {
		return approvalBy;
	}

	public void setApprovalBy(User approvalBy) {
		this.approvalBy = approvalBy;
	}

	public String getApprovalAction() {
		return approvalAction;
	}

	public void setApprovalAction(String approvalAction) {
		this.approvalAction = approvalAction;
	}

	public String getApprovalNote() {
		return approvalNote;
	}

	public void setApprovalNote(String approvalNote) {
		this.approvalNote = approvalNote;
	}
	
}
