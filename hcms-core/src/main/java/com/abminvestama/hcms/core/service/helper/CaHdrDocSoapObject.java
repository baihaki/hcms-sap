package com.abminvestama.hcms.core.service.helper;

import java.util.List;

/**
 * 
 * @since 2.0.0
 * @version 2.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public class CaHdrDocSoapObject {
	
	private List<CaReqHdrSoapObject> listCaReqHdr;

	public List<CaReqHdrSoapObject> getListCaReqHdr() {
		return listCaReqHdr;
	}

	public void setListCaReqHdr(List<CaReqHdrSoapObject> listCaReqHdr) {
		this.listCaReqHdr = listCaReqHdr;
	}
	
}
