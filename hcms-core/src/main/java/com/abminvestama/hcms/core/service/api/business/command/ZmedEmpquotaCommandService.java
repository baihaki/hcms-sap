package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;


public interface ZmedEmpquotaCommandService extends DatabaseCommandService<ZmedEmpquota> {
}