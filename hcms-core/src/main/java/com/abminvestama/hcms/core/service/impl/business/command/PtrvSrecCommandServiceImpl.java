package com.abminvestama.hcms.core.service.impl.business.command;


import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.PtrvSrec;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.PtrvSrecRepository;
import com.abminvestama.hcms.core.service.api.business.command.PtrvSrecCommandService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add deleteList method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add updateTripnumber method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("ptrvSrecCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class PtrvSrecCommandServiceImpl implements PtrvSrecCommandService {
	
	private PtrvSrecRepository ptrvSrecRepository;
		
		@Autowired
		PtrvSrecCommandServiceImpl(PtrvSrecRepository ptrvSrecRepository) {
			this.ptrvSrecRepository = ptrvSrecRepository;
		}
		
		@Override
		public Optional<PtrvSrec> save(PtrvSrec entity, User user)
				throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
			//if (user != null) {
			//	entity.setUname(user.getId());
			//}
			
			try {
				return Optional.ofNullable(ptrvSrecRepository.save(entity));
			} catch (Exception e) {
				throw new CannotPersistException("Cannot persist PtrvSrec (Personal Information).Reason=" + e.getMessage());
			}				
		}

		@Override
		public boolean delete(PtrvSrec entity, User user) throws NoSuchMethodException, ConstraintViolationException {
			// no delete at this version.
			throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
		}

		@Override
		public boolean restore(PtrvSrec entity, User user) throws NoSuchMethodException, ConstraintViolationException {
			// since we don't have delete operation, then we don't need the 'restore' as well
			throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
		}
		
		@Override
		public Optional<PtrvSrec> updateTripnumber(Long newReinr, PtrvSrec entity, User user)
				/*throws CannotPersistException, NoSuchMethodException, ConstraintViolationException*/ {
			
			try {
				int row = ptrvSrecRepository.updateTripnumber(newReinr, entity.getId().getReinr(), entity.getId().getReceiptno(), entity.getId().getPernr()); 
				if (row > 0) {
				    entity.setReinr(newReinr);
				}
				return Optional.ofNullable(entity);
				//return Optional.ofNullable(ptrvSrecRepository.save(entity));
			} catch (Exception e) {
				entity.setReinr(newReinr);
				System.out.println("updateTripnumber Exception: " + e.getMessage());
				return Optional.ofNullable(entity);
			}				
		}

		@Override
		public boolean deleteList(List<PtrvSrec> entities) {
			try {
			    ptrvSrecRepository.delete(entities);
			} catch (Exception e) {
				return false;
			}
			return true;
		}
	}