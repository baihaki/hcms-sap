package com.abminvestama.hcms.core.service.helper;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.StringUtils;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.T500L;
import com.abminvestama.hcms.core.model.entity.T702N;
import com.abminvestama.hcms.core.model.entity.T702O;
import com.abminvestama.hcms.core.model.entity.T706S;
import com.abminvestama.hcms.core.model.entity.PtrvHead;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class PtrvHeadSoapObject {

	private String hdvrs;
	private String pernr;
	private String reinr;
	private String molga;
	private String morei;
	private String schem;
	private String zland;
	private String zort1;
	private String datv1;
	private String uhrv1;
	private String datb1;
	private String uhrb1;
	private String dath1;
	private String uhrh1;
	private String datr1;
	private String uhrr1;
	private String endrg;
	private String dates;
	private String times;
	private String uname;
	private String repid;
	private String dantn;
	private String fintn;
	private String request;
	private String travelPlan;
	private String expenses;
	private String stTrgtg;
	private String stTrgall;
	private String datReduc1;
	private String datReduc2;
	private String datv1Dienst;
	private String urhv1Dienst;
	private String datb1Dienst;
	private String uhrb1Dienst;
	private String abordnung;
	private String exchangeDate;
	private String kunde;
	
	private List<PtrvSrecSoapObject> tripReceipts;
	
	private PtrvHeadSoapObject() {}
	
	public PtrvHeadSoapObject(PtrvHead object) {
		if (object == null) {
			new PtrvHeadSoapObject();
		} else {
			pernr = object.getId().getPernr().toString();
			//zland = object.getZland().getId().getLand1();
			zland = object.getZland();
			datv1 = object.getDatv1() != null ? CommonDateFunction.convertDateToStringYMD(object.getDatv1()) : null;
			datb1 = object.getDatb1() != null ? CommonDateFunction.convertDateToStringYMD(object.getDatb1()) : null;
			kunde = object.getKunde();
			//schem = object.getSchem().getId().getSchem();
			schem = object.getSchem();
		}		
	}
	
	public PtrvHeadSoapObject(Integer hdvrs, Long reinr, Long pernr) {
		this.hdvrs = hdvrs != null ? hdvrs.toString() : StringUtils.EMPTY; 
		this.reinr = reinr != null ? reinr.toString() : StringUtils.EMPTY;
		this.pernr = pernr != null ? pernr.toString() : StringUtils.EMPTY;
	}
	
	public String getHdvrs() {
		return hdvrs;
	}

	public String getPernr() {
		return pernr;
	}

	public String getReinr() {
		return reinr;
	}

	public String getSchem() {
		return schem;
	}

	public String getZland() {
		return zland;
	}

	public String getDatv1() {
		return datv1;
	}

	public String getDatb1() {
		return datb1;
	}

	public String getKunde() {
		return kunde;
	}

	public List<PtrvSrecSoapObject> getTripReceipts() {
		return tripReceipts;
	}

	public void setTripReceipts(List<PtrvSrecSoapObject> tripReceipts) {
		this.tripReceipts = tripReceipts;
	}
	
	
}
