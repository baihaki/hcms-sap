package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class MasterDataSoapObject {
	
	private String cocd;
	private String empnm;
	private String empsn;
	
	private MasterDataSoapObject() {}
	
	public MasterDataSoapObject(String cocd, String empnm, String empsn) {
		if (cocd == null && empnm == null && empsn == null) {
			new MasterDataSoapObject();
		} else {
			this.cocd = cocd;
			this.empnm = empnm;
			this.empsn = empsn;
		}		
	}

	public String getCocd() {
		return cocd;
	}

	public String getEmpnm() {
		return empnm;
	}

	public String getEmpsn() {
		return empsn;
	}
	
}
