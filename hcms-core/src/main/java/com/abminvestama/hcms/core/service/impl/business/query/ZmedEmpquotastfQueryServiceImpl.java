package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyEmpquotastf;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotastf;
import com.abminvestama.hcms.core.repository.ZmedEmpquotastfRepository;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotastfQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findExpenseQuotasByBukrs method, used for expense quota</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("zmedEmpquotastfQueryService")
@Transactional(readOnly = true)
public class ZmedEmpquotastfQueryServiceImpl implements ZmedEmpquotastfQueryService {

	private ZmedEmpquotastfRepository zmedEmpquotastfRepository;
	
	@Autowired
	ZmedEmpquotastfQueryServiceImpl(ZmedEmpquotastfRepository zmedEmpquotastfRepository) {
		this.zmedEmpquotastfRepository = zmedEmpquotastfRepository;
	}
	
	//@Override
	//public Optional<ZmedEmpquotastf> findById(Optional<String> id) throws Exception {
	//	return id.map(pk -> zmedEmpquotastfRepository.findOne(pk));
	//}

	@Override
	public Optional<Collection<ZmedEmpquotastf>> fetchAll() {
		List<ZmedEmpquotastf> entities = new ArrayList<>();
		Optional<Iterable<ZmedEmpquotastf>> entityIterable = Optional.ofNullable(zmedEmpquotastfRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}

	@Override
	public Optional<ZmedEmpquotastf> findById(
			Optional<ZmedCompositeKeyEmpquotastf> id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<ZmedEmpquotastf> findOneByCompositeKey(long stell,
			String bukrs, String persg, String persk, String kodequo,
			Date datab, Date datbi) {
		if (bukrs == null || kodequo == null || persg == null || persk == null || stell == 0 || datab == null || datbi == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(zmedEmpquotastfRepository.findOneByCompositeKey(stell, bukrs, persg, persk, kodequo, datab, datbi));
	}

	@Override
	public List<ZmedEmpquotastf> findQuotas(Long stell, String bukrs,
			String persg, String persk, Date datab, Date datbi) {
		if (bukrs == null || persg == null || persk == null || datab == null || datbi == null || stell == null) {
			return Collections.emptyList();
		}
		Optional<Collection<ZmedEmpquotastf>> bunchOfZmed
			= Optional.ofNullable(zmedEmpquotastfRepository.findQuotas(stell, bukrs, persg, persk, datab, datbi));
		
		return (bunchOfZmed.isPresent()
				? bunchOfZmed.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

	/*@Override
	public Optional<ZmedEmpquotastf> findByEntityName(final String entityName) {
		return Optional.ofNullable(zmedEmpquotastfRepository.findByPersk(entityName));
	}*/
	


	@Override
	public Optional<Collection<ZmedEmpquotastf>> findExpenseQuotasByBukrs(String bukrs, String persg, String persk, Date searchDate) {
		if (bukrs == null || persg == null || persk == null || searchDate == null) {
			return Optional.empty();
		}
		
		Optional<Collection<ZmedEmpquotastf>> bunchOfExpenseQuotas
			= Optional.ofNullable(zmedEmpquotastfRepository.findExpenseQuotasByBukrs(bukrs, persg, persk, searchDate));	
		return bunchOfExpenseQuotas;
	}


}
