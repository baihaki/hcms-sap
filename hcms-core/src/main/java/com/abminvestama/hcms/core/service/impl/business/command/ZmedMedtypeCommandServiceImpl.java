package com.abminvestama.hcms.core.service.impl.business.command;


import java.util.Date;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.ZmedMedtypeRepository;
import com.abminvestama.hcms.core.service.api.business.command.ZmedMedtypeCommandService;


@Service("ZmedMedtypeCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ZmedMedtypeCommandServiceImpl implements ZmedMedtypeCommandService {
	
	private ZmedMedtypeRepository zmedMedtypeRepository;
		
		@Autowired
		ZmedMedtypeCommandServiceImpl(ZmedMedtypeRepository zmedMedtypeRepository) {
			this.zmedMedtypeRepository = zmedMedtypeRepository;
		}
		
		@Override
		public Optional<ZmedMedtype> save(ZmedMedtype entity, User user)
				throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
			//if (user != null) {
				//entity.setUname(user.getId());
			//}
			
			try {
				return Optional.ofNullable(zmedMedtypeRepository.save(entity));
			} catch (Exception e) {
				throw new CannotPersistException("Cannot persist ZmedMedtype (Personal Information).Reason=" + e.getMessage());
			}				
		}

		@Override
		public boolean delete(ZmedMedtype entity, User user) throws NoSuchMethodException, ConstraintViolationException {
			// no delete at this version.
			throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
		}

		@Override
		public boolean restore(ZmedMedtype entity, User user) throws NoSuchMethodException, ConstraintViolationException {
			// since we don't have delete operation, then we don't need the 'restore' as well
			throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
		}
	}