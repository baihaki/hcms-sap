package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.T705P;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.T705PKey;
import com.abminvestama.hcms.core.repository.T705PRepository;
import com.abminvestama.hcms.core.service.api.business.query.T705PQueryService;


@Service("t705pQueryService")
@Transactional(readOnly = true)
public class T705PQueryServiceImpl implements T705PQueryService {

	private T705PRepository t705pRepository;
	
	@Autowired
	T705PQueryServiceImpl(T705PRepository t705pRepository) {
		this.t705pRepository = t705pRepository;
	}
	
	@Override
	public Page<T705P> fetchAllT705p(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		return t705pRepository.findAllT705P(pageRequest);
	}
	
	@Override
	public Optional<Collection<T705P>> fetchAll() {
		Optional<Iterable<T705P>> t705pIterates = Optional.ofNullable(t705pRepository.findAll());
		if (!t705pIterates.isPresent()) {
			return Optional.empty();
		}
		
		List<T705P> listOfT705P = new ArrayList<>();
		t705pIterates.map(t705pIterable -> {			
			t705pIterable.forEach(t705p -> {
				listOfT705P.add(t705p);
			});
			return listOfT705P;
		});
		
		return Optional.of(listOfT705P);
	}
/*
	@Override
	public Optional<T705P> findOneByCompositeKey(Long pdsnr) {
		if (pdsnr == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(t705pRepository.findOneByCompositeKey(pdsnr));
	}*/

	
	@Override
	public Optional<T705P> findById(Optional<T705PKey> id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<T705P> findBySatza(String satza) {

		if (satza == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(t705pRepository.findBySatza(satza));
	}

}