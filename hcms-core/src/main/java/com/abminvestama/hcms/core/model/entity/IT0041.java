package com.abminvestama.hcms.core.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 * Class that represents master data transaction for <strong>Date Specifications</strong> (i.e. Infotype IT0041 in SAP).
 * 
 */
@Entity
@Table(name = "it0041")
public class IT0041 extends SAPAbstractEntity<ITCompositeKeysNoSubtype> {

	private static final long serialVersionUID = 4850112090782760898L;

	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;
	
	public IT0041() {}
	
	public IT0041(ITCompositeKeysNoSubtype id) {
		this();
		this.id = id;
	}
	
	@Column(name = "seqnr")
	private Long seqnr;
	
	@ManyToOne
	@JoinColumn(name = "dar01", referencedColumnName = "datar")
	private T548T dar01;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "dat01")
	private Date dat01;
	
	@ManyToOne
	@JoinColumn(name = "dar02", referencedColumnName = "datar")
	private T548T dar02;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "dat02")
	private Date dat02;

	@ManyToOne
	@JoinColumn(name = "dar03", referencedColumnName = "datar")
	private T548T dar03;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "dat03")
	private Date dat03;
	
	@ManyToOne
	@JoinColumn(name = "dar04", referencedColumnName = "datar")
	private T548T dar04;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "dat04")
	private Date dat04;
	
	@ManyToOne
	@JoinColumn(name = "dar05", referencedColumnName = "datar")
	private T548T dar05;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "dat05")
	private Date dat05;
	
	@ManyToOne
	@JoinColumn(name = "dar06", referencedColumnName = "datar")
	private T548T dar06;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "dat06")
	private Date dat06;
	
	@ManyToOne
	@JoinColumn(name = "dar07", referencedColumnName = "datar")
	private T548T dar07;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "dat07")	
	private Date dat07;
	
	@ManyToOne
	@JoinColumn(name = "dar08", referencedColumnName = "datar")
	private T548T dar08;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "dat08")
	private Date dat08;
	
	@ManyToOne
	@JoinColumn(name = "dar09", referencedColumnName = "datar")
	private T548T dar09;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "dat09")
	private Date dat09;
	
	@ManyToOne
	@JoinColumn(name = "dar10", referencedColumnName = "datar")
	private T548T dar10;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "dat10")
	private Date dat10;
	
	@ManyToOne
	@JoinColumn(name = "dar11", referencedColumnName = "datar")
	private T548T dar11;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "dat11")
	private Date dat11;
	
	@ManyToOne
	@JoinColumn(name = "dar12", referencedColumnName = "datar")
	private T548T dar12;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "dat12")
	private Date dat12;
	
	/**
	 * GET Employee ID/SSN.
	 * 
	 * @return
	 */
	public Long getPernr() {
		return pernr;
	}
	
	/**
	 * GET Date Type 1.
	 *  
	 * @return
	 */
	public T548T getDar01() {
		return dar01;
	}
	
	public void setDar01(T548T dar01) {
		this.dar01 = dar01;
	}
	
	/**
	 * GET Date of Date Type 1.
	 * 
	 * @return
	 */
	public Date getDat01() {
		return dat01;
	}
	
	public void setDat01(Date dat01) {
		this.dat01 = dat01;
	}

	/**
	 * GET Infotype Record No.
	 * 
	 * @return
	 */
	public Long getSeqnr() {
		return seqnr;
	}

	public void setSeqnr(Long seqnr) {
		this.seqnr = seqnr;
	}

	/**
	 * GET Date Type 2.
	 * 
	 * @return
	 */
	public T548T getDar02() {
		return dar02;
	}

	public void setDar02(T548T dar02) {
		this.dar02 = dar02;
	}

	/**
	 * GET Date of Date Type 2.
	 * 
	 * @return
	 */
	public Date getDat02() {
		return dat02;
	}

	public void setDat02(Date dat02) {
		this.dat02 = dat02;
	}

	/**
	 * GET Date Type 3.
	 * 
	 * @return
	 */
	public T548T getDar03() {
		return dar03;
	}

	public void setDar03(T548T dar03) {
		this.dar03 = dar03;
	}

	/**
	 * GEt Date of Date Type 3.
	 * 
	 * @return
	 */
	public Date getDat03() {
		return dat03;
	}

	public void setDat03(Date dat03) {
		this.dat03 = dat03;
	}

	/**
	 * GET Date Type 4.
	 * 
	 * @return
	 */
	public T548T getDar04() {
		return dar04;
	}

	public void setDar04(T548T dar04) {
		this.dar04 = dar04;
	}

	/**
	 * GET Date of Date Type 4.
	 * 
	 * @return
	 */
	public Date getDat04() {
		return dat04;
	}

	public void setDat04(Date dat04) {
		this.dat04 = dat04;
	}

	/**
	 * GET Date Type 5.
	 * 
	 * @return
	 */
	public T548T getDar05() {
		return dar05;
	}

	public void setDar05(T548T dar05) {
		this.dar05 = dar05;
	}

	/**
	 * GET Date of Date Type 5.
	 * 
	 * @return
	 */
	public Date getDat05() {
		return dat05;
	}

	public void setDat05(Date dat05) {
		this.dat05 = dat05;
	}

	/**
	 * GET Date Type 6.
	 * 
	 * @return
	 */
	public T548T getDar06() {
		return dar06;
	}

	public void setDar06(T548T dar06) {
		this.dar06 = dar06;
	}

	/**
	 * GET Date of Date Type 6.
	 * 
	 * @return
	 */
	public Date getDat06() {
		return dat06;
	}

	public void setDat06(Date dat06) {
		this.dat06 = dat06;
	}

	/**
	 * GET Date Type 7.
	 * 
	 * @return
	 */
	public T548T getDar07() {
		return dar07;
	}

	public void setDar07(T548T dar07) {
		this.dar07 = dar07;
	}

	/**
	 * GET Date of Date Type 7.
	 * 
	 * @return
	 */
	public Date getDat07() {
		return dat07;
	}

	public void setDat07(Date dat07) {
		this.dat07 = dat07;
	}

	/**
	 * GET Date Type 8.
	 * 
	 * @return
	 */
	public T548T getDar08() {
		return dar08;
	}

	public void setDar08(T548T dar08) {
		this.dar08 = dar08;
	}

	/**
	 * GET Date of Date Type 8.
	 * 
	 * @return
	 */
	public Date getDat08() {
		return dat08;
	}

	public void setDat08(Date dat08) {
		this.dat08 = dat08;
	}

	/**
	 * GET Date Type 9.
	 * 
	 * @return
	 */
	public T548T getDar09() {
		return dar09;
	}

	public void setDar09(T548T dar09) {
		this.dar09 = dar09;
	}

	/**
	 * GET Date of Date Type 9.
	 * 
	 * @return
	 */
	public Date getDat09() {
		return dat09;
	}

	public void setDat09(Date dat09) {
		this.dat09 = dat09;
	}

	/**
	 * GET Date Type 10.
	 * 
	 * @return
	 */
	public T548T getDar10() {
		return dar10;
	}

	public void setDar10(T548T dar10) {
		this.dar10 = dar10;
	}

	/**
	 * GET Date of Date Type 10.
	 * 
	 * @return
	 */
	public Date getDat10() {
		return dat10;
	}

	public void setDat10(Date dat10) {
		this.dat10 = dat10;
	}

	/**
	 * GET Date Type 11.
	 * 
	 * @return
	 */
	public T548T getDar11() {
		return dar11;
	}

	public void setDar11(T548T dar11) {
		this.dar11 = dar11;
	}

	/**
	 * GET Date of Date Type 11.
	 * 
	 * @return
	 */
	public Date getDat11() {
		return dat11;
	}

	public void setDat11(Date dat11) {
		this.dat11 = dat11;
	}

	/**
	 * GET Date Type 12.
	 * 
	 * @return
	 */
	public T548T getDar12() {
		return dar12;
	}

	public void setDar12(T548T dar12) {
		this.dar12 = dar12;
	}

	/**
	 * GET Date of Date Type 12.
	 * 
	 * @return
	 */
	public Date getDat12() {
		return dat12;
	}

	public void setDat12(Date dat12) {
		this.dat12 = dat12;
	}
}