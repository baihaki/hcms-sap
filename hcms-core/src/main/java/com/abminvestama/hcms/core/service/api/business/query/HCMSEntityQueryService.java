package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Optional;

import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface HCMSEntityQueryService extends DatabaseQueryService<HCMSEntity, String>, DatabasePaginationQueryService {

	Optional<HCMSEntity> findByEntityName(final String entityName);
}