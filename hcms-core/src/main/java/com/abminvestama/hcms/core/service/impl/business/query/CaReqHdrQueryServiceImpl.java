package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.CaReqHdr;
import com.abminvestama.hcms.core.repository.CaReqHdrRepository;
import com.abminvestama.hcms.core.service.api.business.query.CaReqHdrQueryService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.2 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.2</td><td>Baihaki</td><td>Add parameter aprovalByUserId to findByBukrsAndProcessWithPaging</td></tr>
 *     <tr><td>3.0.1</td><td>Baihaki</td><td>Add parameter: dateBegin, dateEnd</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("caReqHdrQueryService")
@Transactional(readOnly = true)
public class CaReqHdrQueryServiceImpl implements CaReqHdrQueryService {

	private CaReqHdrRepository caReqHdrRepository;
	
	@Autowired
	CaReqHdrQueryServiceImpl(CaReqHdrRepository caReqHdrRepository) {
		this.caReqHdrRepository = caReqHdrRepository;
	}
	
	@Override
	public Optional<Collection<CaReqHdr>> fetchAll() {
		List<CaReqHdr> entities = new ArrayList<>();
		Optional<Iterable<CaReqHdr>> entityIterable = Optional.ofNullable(caReqHdrRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}


	@Override
	public Optional<CaReqHdr> findById(Optional<String> id) throws Exception {
		return id.map(pk -> caReqHdrRepository.findOne(pk));
	}

	/*@Override
	public Collection<CaReqHdr> findByKodemed(String bukrs, String kodemed,
			Date datbi) {
		if (bukrs == null || kodemed == null || datbi == null) {
			return Collections.emptyList();
		}
		

		Optional<Collection<CaReqHdr>> bunchOfZmed
			= Optional.ofNullable(caReqHdrRepository.findByKodemed(bukrs, kodemed, datbi));
		
		return (bunchOfZmed.isPresent()
				? bunchOfZmed.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}*/
	

	/*
	@Override
	public Page<CaReqHdr> findByBukrsAndProcessWithPaging(String bukrs, String process, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (StringUtils.isEmpty(process)) ? caReqHdrRepository.findByBukrsWithPaging(bukrs, notStatus, pageRequest) 
				: caReqHdrRepository.findByBukrsAndProcessWithPaging(bukrs, process, notStatus, pageRequest);
	}

	@Override
	public Page<CaReqHdr> findByPernrAndProcessWithPaging(long pernr, String process, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (StringUtils.isEmpty(process)) ? caReqHdrRepository.findByPernrWithPaging(pernr, notStatus, pageRequest) 
				: caReqHdrRepository.findByPernrAndProcessWithPaging(pernr, process, notStatus, pageRequest);
	}

	@Override
	public Page<CaReqHdr> findByBukrsAndProcessesWithPaging(String bukrs, String[] processes, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (processes == null) ? caReqHdrRepository.findByBukrsWithPaging(bukrs, notStatus, pageRequest) 
				: caReqHdrRepository.findByBukrsAndProcessesWithPaging(bukrs, processes, notStatus, pageRequest);
	}

	@Override
	public Page<CaReqHdr> findByPernrAndProcessesWithPaging(long pernr, String[] processes, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return (processes == null) ? caReqHdrRepository.findByPernrWithPaging(pernr, notStatus, pageRequest) 
				: caReqHdrRepository.findByPernrAndProcessesWithPaging(pernr, processes, notStatus, pageRequest);
	}
	*/
	

	@Override
	public Page<CaReqHdr> findByBukrsAndProcessWithPaging(String bukrs, Integer process, String[] advanceCodes, String aprovalByUserId,
		Date dateBegin, Date dateEnd, String notStatus, String superiorPosition, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		if (dateBegin == null) {
			dateBegin = CommonDateFunction.convertDateRequestParameterIntoDate("1980-01-01");
		}
		if (dateEnd == null) {
			dateEnd = CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31");
		}
		
		if (!StringUtils.isEmpty(superiorPosition)) {
			int total = caReqHdrRepository.findByBukrsAndProcessAndSuperiorPositionCount(bukrs, process, advanceCodes, notStatus, superiorPosition);
			List<CaReqHdr> listOfCaReqHdr = caReqHdrRepository.findByBukrsAndProcessAndSuperiorPositionWithPaging(
					bukrs, process, advanceCodes, notStatus, superiorPosition, pageRequest.getPageSize(), pageRequest.getOffset()).
					stream().collect(Collectors.toList());
			Page<CaReqHdr> page = new PageImpl<CaReqHdr>(listOfCaReqHdr, pageRequest, total);
			return page;
		}
		
		if (process.intValue() == 1 && aprovalByUserId != null) {
			// ROLE_HEAD, fetch only his subordinate, filter by approved1byUserId = aprovalByUserId (the Head's user id)
			return caReqHdrRepository.findByBukrsAndProcessWithPagingForHead(bukrs, process, advanceCodes, aprovalByUserId, dateBegin, dateEnd, notStatus, pageRequest);
		}
		
		if (process.intValue() == 2 && aprovalByUserId != null) {
			// ROLE_DIRECTOR, fetch only his subordinate, filter by approved2byUserId = aprovalByUserId (the Director's user id)
			return caReqHdrRepository.findByBukrsAndProcessWithPagingForDirector(bukrs, process, advanceCodes, aprovalByUserId, dateBegin, dateEnd, notStatus, pageRequest);
		}
		
		return (process == null) ? caReqHdrRepository.findByBukrsWithPaging(bukrs, advanceCodes, dateBegin, dateEnd, notStatus, pageRequest) 
				: caReqHdrRepository.findByBukrsAndProcessWithPaging(bukrs, process, advanceCodes, dateBegin, dateEnd, notStatus, pageRequest);
	}

	@Override
	public Page<CaReqHdr> findByPernrAndProcessWithPaging(long pernr, Integer process, String[] advanceCodes,
		Date dateBegin, Date dateEnd, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);

		if (dateBegin == null) {
			dateBegin = CommonDateFunction.convertDateRequestParameterIntoDate("1980-01-01");
		}
		if (dateEnd == null) {
			dateEnd = CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31");
		}
		
		return (process == null) ? caReqHdrRepository.findByPernrWithPaging(pernr, advanceCodes, dateBegin, dateEnd, notStatus, pageRequest) 
				: caReqHdrRepository.findByPernrAndProcessWithPaging(pernr, process, advanceCodes, dateBegin, dateEnd, notStatus, pageRequest);
	}

	@Override
	public Page<CaReqHdr> findByBukrsAndProcessesWithPaging(String bukrs, Integer[] processes, String[] advanceCodes,
		Date dateBegin, Date dateEnd, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);

		if (dateBegin == null) {
			dateBegin = CommonDateFunction.convertDateRequestParameterIntoDate("1980-01-01");
		}
		if (dateEnd == null) {
			dateEnd = CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31");
		}
		
		return (processes == null) ? caReqHdrRepository.findByBukrsWithPaging(bukrs, advanceCodes, dateBegin, dateEnd, notStatus, pageRequest) 
				: caReqHdrRepository.findByBukrsAndProcessesWithPaging(bukrs, processes, advanceCodes, dateBegin, dateEnd, notStatus, pageRequest);
	}

	@Override
	public Page<CaReqHdr> findByPernrAndProcessesWithPaging(long pernr, Integer[] processes, String[] advanceCodes,
		Date dateBegin, Date dateEnd, String notStatus, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);

		if (dateBegin == null) {
			dateBegin = CommonDateFunction.convertDateRequestParameterIntoDate("1980-01-01");
		}
		if (dateEnd == null) {
			dateEnd = CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31");
		}
		
		return (processes == null) ? caReqHdrRepository.findByPernrWithPaging(pernr, advanceCodes, dateBegin, dateEnd, notStatus, pageRequest) 
				: caReqHdrRepository.findByPernrAndProcessesWithPaging(pernr, processes, advanceCodes, dateBegin, dateEnd, notStatus, pageRequest);
	}

	@Override
	public Optional<Collection<CaReqHdr>> findByPernrAndProcessesAndBetweenDate(Long pernr, Integer[] processes, String[] advanceCodes,
			Date dateBegin, Date dateEnd) {
		if (pernr == null || processes == null) {
			return Optional.empty();
		}
		
		if (dateBegin == null) {
			dateBegin = CommonDateFunction.convertDateRequestParameterIntoDate("1980-01-01");
		}
		
		if (dateEnd == null) {
			dateEnd = CommonDateFunction.convertDateRequestParameterIntoDate("9999-12-31");
		}
		
		Optional<Collection<CaReqHdr>> bunchOfCaReqHdr
			= Optional.ofNullable(caReqHdrRepository.findByPernrAndProcessesAndBetweenDate(pernr, processes, advanceCodes, dateBegin, dateEnd));
		
		return bunchOfCaReqHdr;
	}
	
}
