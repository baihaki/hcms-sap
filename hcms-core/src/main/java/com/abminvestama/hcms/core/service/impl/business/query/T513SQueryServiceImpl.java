package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T513S;
import com.abminvestama.hcms.core.model.entity.T513SKey;
import com.abminvestama.hcms.core.repository.T513SRepository;
import com.abminvestama.hcms.core.service.api.business.query.T513SQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("t513sQueryService")
@Transactional(readOnly = true)
public class T513SQueryServiceImpl implements T513SQueryService {
	
	private T513SRepository t513sRepository;
	
	@Autowired
	T513SQueryServiceImpl(T513SRepository t513sRepository) {
		this.t513sRepository = t513sRepository;
	}

	@Override
	public Optional<T513S> findById(Optional<T513SKey> id) throws Exception {
		return id.map(pk -> t513sRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<T513S>> fetchAll() {
		List<T513S> listOfT513S = new ArrayList<>();
		Optional<Iterable<T513S>> bunchOfT513S = Optional.ofNullable(t513sRepository.findAll());
		return bunchOfT513S.map(iter -> {
			iter.forEach(t513s -> {
				listOfT513S.add(t513s);
			});
			return listOfT513S;
		});
	}
}