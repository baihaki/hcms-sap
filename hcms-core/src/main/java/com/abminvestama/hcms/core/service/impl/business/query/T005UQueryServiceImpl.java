package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T005U;
import com.abminvestama.hcms.core.repository.T005URepository;
import com.abminvestama.hcms.core.service.api.business.query.T005UQueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("t005uQueryService")
@Transactional(readOnly = true)
public class T005UQueryServiceImpl implements T005UQueryService {

	private T005URepository t005uRepository;
	
	@Autowired
	T005UQueryServiceImpl(T005URepository t005uRepository) {
		this.t005uRepository = t005uRepository;
	}
	
	@Override
	public Optional<T005U> findById(Optional<String> id) throws Exception {
		return id.map(pk -> {
			return Optional.ofNullable(t005uRepository.findOne(pk));
		}).orElse(Optional.empty());
	}

	@Override
	public Optional<Collection<T005U>> fetchAll() {
		Optional<Iterable<T005U>> iterableT005U = Optional.ofNullable(t005uRepository.findAll());
		
		List<T005U> listOfT005U = new ArrayList<>();
		
		return iterableT005U.map(t005uIterators -> {
			t005uIterators.forEach(t005u -> {
				listOfT005U.add(t005u);
			});
			return listOfT005U;
		});		
	}

	@Override
	public Page<T005U> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return t005uRepository.findAll(pageRequest);
	}
}