package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.T517T;
import com.abminvestama.hcms.core.model.entity.T517A;
import com.abminvestama.hcms.core.model.entity.T517AKey;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface T517AQueryService extends DatabaseQueryService<T517A, T517AKey>, DatabasePaginationQueryService {
	
	Page<T517A> fetchAllWithPaging(int pageNumber);
	Collection<T517A> findBySlart(T517T slart);
}