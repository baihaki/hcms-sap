package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T702O;
import com.abminvestama.hcms.core.model.entity.T702OKey;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.repository.T702ORepository;
import com.abminvestama.hcms.core.service.api.business.query.T702OQueryService;

@Service("t702oQueryService")
@Transactional(readOnly = true)
public class T702OQueryServiceImpl implements T702OQueryService {

	private T702ORepository t702oRepository;
	
	@Autowired
	T702OQueryServiceImpl(T702ORepository t702oRepository) {
		this.t702oRepository = t702oRepository;
	}
	
	/*@Override
	public Optional<T702O> findById(Optional<String> id) throws Exception {
		return id.map(pk -> t702oRepository.findOne(pk));
	}*/

	@Override
	public Optional<Collection<T702O>> fetchAll() {
		Optional<Iterable<T702O>> t702oIterable = Optional.ofNullable(t702oRepository.findAll());
		if (!t702oIterable.isPresent()) {
			return Optional.empty();
		}
		
		List<T702O> listOfT702O = new ArrayList<>();
		return t702oIterable.map(iteratesT702O -> {
			iteratesT702O.forEach(t702o -> {
				listOfT702O.add(t702o);
			});
			
			return listOfT702O;
		});				
	}

	@Override
	public Optional<T702O> findById(Optional<T702OKey> id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<T702O> findById(String zland) {
		if (zland == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(t702oRepository.findByLand1(zland));
	}

	
}