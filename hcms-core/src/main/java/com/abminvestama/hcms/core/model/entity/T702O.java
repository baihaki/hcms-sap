package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "t702o")
public class T702O extends SAPAbstractEntityZmed<T702OKey> {

	private static final long serialVersionUID = 3669795826077668803L;
	

	public T702O() {}
	
	public T702O(T702OKey id) {
		this();
		this.id = id;
	}
	
	@Column(name = "morei", nullable = false, insertable = false, updatable = false, length = 2)
	private String morei;
	
	@Column(name = "land1", nullable = false, insertable = false, updatable = false, length = 2)
	private String land1;

	@Column(name = "text25", nullable = false, insertable = false, updatable = false)
	private String text25;
	
	public void setMorei(String morei) {
		this.morei = morei;
	}

	public void setLand1(String land1) {
		this.land1 = land1;
	}

	public void setText25(String text25) {
		this.text25 = text25;
	}

	public String getMorei() {
		return morei;
	}

	public String getLand1() {
		return land1;
	}

	public String getText25() {
		return text25;
	}

	
}