package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.CaReqDtl;
import com.abminvestama.hcms.core.model.entity.CaReqDtlKey;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaReqDtlRepository extends CrudRepository<CaReqDtl, CaReqDtlKey> {

	@Query("FROM CaReqDtl req_dtl WHERE req_dtl.reqNo = :reqno AND req_dtl.itemNo = :itemno")
	CaReqDtl findOneByCompositeKey(@Param("reqno") String reqNo, @Param("itemno") Short itemNo);
	
	@Query("FROM CaReqDtl req_dtl WHERE req_dtl.reqNo = :reqno")
	Collection<CaReqDtl> findByReqNo(@Param("reqno") String reqNo);
	
	@Modifying(clearAutomatically = true)
	@Query(nativeQuery=true, value="UPDATE ca_req_dtl SET req_no = :newRecNo WHERE req_no = :oldRecNo AND item_no = :itemno")
	int updateRecNo(@Param("newRecNo") String newRecNo, @Param("oldRecNo") String oldRecNo, @Param("itemno") Short itemNo);
	
	
}