package com.abminvestama.hcms.core.service.helper;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.T556B;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyTrxhdr;
import com.abminvestama.hcms.core.model.entity.IT2006;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT2006Mapper {
	
	private IT2006 it2006;
	
	private IT2006Mapper() {}
	
	public IT2006Mapper(Map<String, String> map, CommonServiceFactory commonServiceFactory) {
		if (map == null) {
			new IT2006Mapper();
			it2006 = new IT2006();
		} else {
			String endda = map.get("endda") != null && !map.get("endda").equals("0000-00-00") ?
					map.get("endda") : "1999-12-31";
			String begda = map.get("begda") != null && !map.get("begda").equals("0000-00-00") ?
					map.get("begda") : "1980-01-01";
			String pernr = StringUtils.defaultString(map.get("pernr"), "0");
			String subty = StringUtils.defaultString(map.get("subty"), "10");
			ITCompositeKeys id = new ITCompositeKeys(Long.parseLong(pernr), "2006", subty,
					CommonDateFunction.convertDateRequestParameterIntoDate(endda), CommonDateFunction.convertDateRequestParameterIntoDate(begda));
			it2006 = new IT2006(id);
			Integer ktartId = map.get("ktart") != null ? Integer.parseInt(map.get("ktart")) : 0;
			Optional<T556B> ktart;
			try {
				ktart = commonServiceFactory.getT556BQueryService().findById(Optional.of(ktartId));
				if (ktart.isPresent()) {
				    it2006.setKtart(ktart.get());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			it2006.setAnzhl(map.get("anzhl") != null ? Double.valueOf(map.get("anzhl")) : 0.00);
			it2006.setKverb(map.get("kverb") != null ? Double.valueOf(map.get("kverb")) : 0.00);
			it2006.setDesta(map.get("desta") != null && !map.get("desta").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("desta")) : null);
			it2006.setDeend(map.get("deend") != null && !map.get("deend").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("deend")) : null);
		}		
	}

	public IT2006 getIT2006() {
		return it2006;
	}
	
}
