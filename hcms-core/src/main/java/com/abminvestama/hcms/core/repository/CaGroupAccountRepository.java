package com.abminvestama.hcms.core.repository;

import org.springframework.data.repository.CrudRepository;

import com.abminvestama.hcms.core.model.entity.CaGroupAccount;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaGroupAccountRepository extends CrudRepository<CaGroupAccount, Long> {
}