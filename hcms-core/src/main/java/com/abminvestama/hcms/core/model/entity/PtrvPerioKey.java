package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;



@Embeddable
@Immutable
public class PtrvPerioKey implements Serializable {
	private static final long serialVersionUID = 5385850040856914239L;

	private String mandt;
	private Long pernr;
	private Long reinr;
	private Long perio;
	private Long pdvrs;
		
	public String getMandt() {
		return mandt;
	}

	public Long getPernr() {
		return pernr;
	}

	public Long getReinr() {
		return reinr;
	}

	public Long getPerio() {
		return perio;
	}

	public Long getPdvrs() {
		return pdvrs;
	}

	public PtrvPerioKey() {}
	
	public PtrvPerioKey(String mandt, Long pernr, Long reinr, Long perio, Long pdvrs) {
		this.mandt = mandt;
		this.pernr = pernr;
		this.reinr = reinr;
		this.perio = perio;
		this.pdvrs = pdvrs;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final PtrvPerioKey key = (PtrvPerioKey) o;
		
		if (key.getMandt() != null ? key.getMandt() != (mandt != null ? mandt: "") : mandt != null) {
			return false;
		}
		
		if (key.getPernr() != null ? key.getPernr() != (pernr != null ? pernr: "") : pernr != null) {
			return false;
		}
		
		if (key.getReinr() != null ? key.getReinr() != (reinr != null ? reinr: "") : reinr != null) {
			return false;
		}
		
		if (key.getPerio() != null ? key.getPerio() != (perio != null ? perio: "") : perio != null) {
			return false;
		}
		
		if (key.getPdvrs() != null ? key.getPdvrs() != (pdvrs != null ? pdvrs: "") : pdvrs != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.mandt != null ? this.mandt.hashCode() : 0;
		result = result * 31 + (this.pernr != null ? this.pernr.hashCode() : 0);
		result = result * 31 + (this.reinr != null ? this.reinr.hashCode() : 0);
		result = result * 31 + (this.perio != null ? this.perio.hashCode() : 0);
		result = result * 31 + (this.pdvrs != null ? this.pdvrs.hashCode() : 0);
		return result;
	}
}
