package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.IT0077;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyCostype;
import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.abminvestama.hcms.core.repository.ZmedCostypeRepository;
import com.abminvestama.hcms.core.service.api.business.query.ZmedCostypeQueryService;

@Service("zmedCostypeQueryService")
@Transactional(readOnly = true)
public class ZmedCostypeQueryServiceImpl implements ZmedCostypeQueryService {

	private ZmedCostypeRepository zmedCostypeRepository;
	
	@Autowired
	ZmedCostypeQueryServiceImpl(ZmedCostypeRepository zmedCostypeRepository) {
		this.zmedCostypeRepository = zmedCostypeRepository;
	}
	
	//@Override
	//public Optional<ZmedCostype> findById(Optional<ZmedCompositeKeyCostype> id) throws Exception {
		//return id.map(pk -> zmedCostypeRepository.findOne(pk));
	//	throw new NoSuchMethodException("Method not implemented. Please use method findOneByCompositeKeys instead.");		
	//}

	@Override
	public Optional<Collection<ZmedCostype>> fetchAll() {
		List<ZmedCostype> entities = new ArrayList<>();
		Optional<Iterable<ZmedCostype>> entityIterable = Optional.ofNullable(zmedCostypeRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}
	
	
	
	@Override
	public Optional<ZmedCostype> findOneByCompositeKey(String bukrs, String kodemed, String kodecos, Date datab, Date datbi) {
		if (bukrs == null || kodemed == null || kodecos == null || datab == null || datbi == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(zmedCostypeRepository.findOneByCompositeKey(bukrs, kodemed, kodecos, datab, datbi));
	}


	@Override
	public Optional<ZmedCostype> findById(Optional<ZmedCompositeKeyCostype> id)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<ZmedCostype> findByKodemed(String bukrs, String kodemed,
			Date datbi) {
		if (bukrs == null || kodemed == null || datbi == null) {
			return Collections.emptyList();
		}
		Optional<Collection<ZmedCostype>> bunchOfZmed
			= Optional.ofNullable(zmedCostypeRepository.findByKodemed(bukrs, kodemed, datbi));
		
		return (bunchOfZmed.isPresent()
				? bunchOfZmed.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}
	
	
	@Override
	public Collection<ZmedCostype> findByKodemed(String bukrs, Date datbi) {
		if (bukrs == null || datbi == null) {
			return Collections.emptyList();
		}
		Optional<Collection<ZmedCostype>> bunchOfZmed
			= Optional.ofNullable(zmedCostypeRepository.findByKodemed(bukrs, datbi));
		
		return (bunchOfZmed.isPresent()
				? bunchOfZmed.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}
		
}
