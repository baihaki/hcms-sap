package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;

/**
 * Class that represents master data for <strong>Addresses</strong> (i.e. IT0006 in SAP).
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Extends {@link SAPSynchronizeEntity} for Synchronization fields</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add attachment, attachmentPath field</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add status (DocumentStatus) field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Entity
@Table(name = "it0006")
public class IT0006 extends SAPSynchronizeEntity<ITCompositeKeys> {

	private static final long serialVersionUID = -589157851250104698L;

	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "infty", referencedColumnName = "infty", insertable = false, updatable = false),
		@JoinColumn(name = "subty", referencedColumnName = "subty", insertable = false, updatable = false)
	})
	private T591S subty;
	
	public IT0006() {}
	
	public IT0006(ITCompositeKeys id) {
		this();
		this.id = id;
	}
	
	@Column(name = "seqnr")
	private Long seqnr;
	
	@Column(name = "anssa", length = 4)
	private String anssa;
	
	@Column(name = "name2", length = 40)
	private String name2;
	
	@Column(name = "stras", length = 60)
	private String stras;
	
	@Column(name = "ort01", length = 40)
	private String ort01;
	
	@Column(name = "ort02", length = 40)
	private String ort02;
	
	@Column(name = "pstlz", length = 10)
	private String pstlz;
	
	@ManyToOne
	@JoinColumn(name = "land1", referencedColumnName = "land1")
	private T005T t005t;
	
	@Column(name = "telnr", length = 14)
	private String telnr;
	
	@Column(name = "entkm", precision = 5, scale = 2)
	private Double entkm;
	
	@Column(name = "locat", length = 40)
	private String locat;
	
	@ManyToOne
	@JoinColumn(name = "state", referencedColumnName = "bland")
	private T005U t005u;
	
	@Column(name = "entk2", precision = 5, scale = 2)
	private Double entk2;
	
	@Column(name = "com01", length = 4)
	private String com01;
	
	@Column(name = "num01", length = 20)
	private String num01;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "document_status", nullable = false)
	private DocumentStatus status;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "infty", referencedColumnName = "infty", insertable = false, updatable = false),
		@JoinColumn(name = "attach1_subty", referencedColumnName = "subty", insertable = false, updatable = false)
	})
	private Attachment attachment;
	
	@Column(name = "attach1_path")
	private String attachmentPath;
	
	/**
	 * GET Infotype Record No.
	 * 
	 * @return
	 */
	public Long getSeqnr() {
		return seqnr;
	}
	
	public void setSeqnr(Long seqnr) {
		this.seqnr = seqnr;
	}
	
	/**
	 * GET Address Subtype.
	 * 
	 * @return
	 */
	public T591S getSubty() {
		return subty;
	}
	
	/**
	 * Get Address Type.
	 * 
	 * @return
	 */
	public String getAnssa() {
		return anssa;
	}
	
	public void setAnssa(String anssa) {
		this.anssa = anssa;
	}
	
	/**
	 * GET Care Of.
	 * 
	 * @return
	 */
	public String getName2() {
		return name2;
	}
	
	public void setName2(String name2) {
		this.name2 = name2;
	}
	
	/**
	 * GET Street and House No.
	 * 
	 * @return
	 */
	public String getStras() {
		return stras;
	}
	
	public void setStras(String stras) {
		this.stras = stras;
	}
	
	/**
	 * GET City.
	 * 
	 * @return
	 */
	public String getOrt01() {
		return ort01;
	}
	
	public void setOrt01(String ort01) {
		this.ort01 = ort01;
	}
	
	/**
	 * GET District.
	 * 
	 * @return
	 */
	public String getOrt02() {
		return ort02;
	}
	
	public void setOrt02(String ort02) {
		this.ort02 = ort02;
	}
	
	/**
	 * GET Postal Code.
	 * 
	 * @return
	 */
	public String getPstlz() {
		return pstlz;
	}
	
	public void setPstlz(String pstlz) {
		this.pstlz = pstlz;
	}
	
	/**
	 * GET Country Key.
	 * 
	 * @return
	 */
	public T005T getT005t() {
		return t005t;
	}
	
	public void setT005t(T005T t005t) {
		this.t005t = t005t;
	}
	
	/**
	 * GET Telephone Number.
	 * 
	 * @return
	 */
	public String getTelnr() {
		return telnr;
	}
	
	public void setTelnr(String telnr) {
		this.telnr = telnr;
	}
	
	/**
	 * GET Distaince in Km.
	 * 
	 * @return
	 */
	public Double getEntkm() {
		return entkm;
	}
	
	public void setEntkm(Double entkm) {
		this.entkm = entkm;
	}
	
	/**
	 * GET 2nd Address Line.
	 * 
	 * @return
	 */
	public String getLocat() {
		return locat;
	}
	
	public void setLocat(String locat) {
		this.locat = locat;
	}
	
	/**
	 * GET Region.
	 * 
	 * @return
	 */
	public T005U getT005u() {
		return t005u;
	}
	
	public void setT005u(T005U t005u) {
		this.t005u = t005u;
	}
	
	/**
	 * GET Distaince in Km.
	 * 
	 * @return
	 */
	public Double getEntk2() {
		return entk2;
	}
	
	public void setEntk2(Double entk2) {
		this.entk2 = entk2;
	}
	
	/**
	 * GET Communication Type.
	 * 
	 * @return
	 */
	public String getCom01() {
		return com01;
	}
	
	public void setCom01(String com01) {
		this.com01 = com01;
	}
	
	/**
	 * GET Number.
	 * 
	 * @return
	 */
	public String getNum01() {
		return num01;
	}
	
	public void setNum01(String num01) {
		this.num01 = num01;
	}
	
	/**
	 * GET Document Status.
	 * 
	 * @return
	 */
	public DocumentStatus getStatus() {
		return status;
	}
	
	public void setStatus(DocumentStatus status) {
		this.status = status;
	}

	/**
	 * GET Attachment Document Type.
	 * 
	 * @return
	 */
	public Attachment getAttachment() {
		return attachment;
	}
	
	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	/**
	 * GET Attachment Document file path.
	 * 
	 * @return
	 */
	public String getAttachmentPath() {
		return attachmentPath;
	}
	
	public void  setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}
}