package com.abminvestama.hcms.core.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 * 
 * Class that represents table IT0001_TRX in the database. IT0001_TRX is used to 
 * record all transactions for IT0001.
 *
 */
@Entity
@Table(name = "it0001_trx")
public class IT0001Trx extends AbstractEntity {

	private static final long serialVersionUID = 7403795185277165052L;

	@Column(name = "pernr", nullable = false)
	private long pernr;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "endda", nullable = false)
	private Date endda;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "begda", nullable = false)
	private Date begda;
	
	@Column(name = "seqnr")
	private long seqnr;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "aedtm", nullable = true)
	private Date aedtm;
	
	@Column(name = "uname", nullable = true)
	private String uname;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({
		@JoinColumn(name = "werks", referencedColumnName = "persa"),
		@JoinColumn(name = "bukrs", referencedColumnName = "bukrs")
	})
	private T500P t500p;
	
	@ManyToOne
	@JoinColumn(name = "persg", referencedColumnName = "persg")
	private T501T t501t;
	
	@ManyToOne
	@JoinColumn(name = "persk", referencedColumnName = "mandt")
	private T503K t503k;
	
	@Column(name = "vdsk1", length = 14)
	private String vdsk1;
	
	@Column(name = "gsber", length = 10)
	private String gsber;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "werks2", referencedColumnName = "werks"),
		@JoinColumn(name = "btrtl", referencedColumnName = "btrtl")
	})
	private V001PAll v001pall;
	
	@ManyToOne
	@JoinColumn(name = "abkrs", referencedColumnName = "abkrs")
	private T549T t549t;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "kokrs", referencedColumnName = "kokrs"),
		@JoinColumn(name = "kostl", referencedColumnName = "kostl")
	})
	private CSKT cskt;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "orgeh", referencedColumnName = "orgeh"),
		@JoinColumn(name = "endda_orgunit", referencedColumnName = "endda")
	})
	private T527X t527x;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({
		@JoinColumn(name = "plans", referencedColumnName = "plans"),
		@JoinColumn(name = "endda_emposition", referencedColumnName = "endda")
	})
	private T528T t528t;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "m_position_id", referencedColumnName = "id")
	private Position position;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "stell", referencedColumnName = "stell"),
		@JoinColumn(name = "endda_empjob", referencedColumnName = "endda")
	})
	private T513S t513s;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "m_job_id", referencedColumnName = "id")
	private Job job;
	
	@Column(name = "mstbr", length = 8)
	private String mstbr;
	
	@Column(name = "sacha", length = 3)
	private String sacha;
	
	@Column(name = "sachp", length = 3)
	private String sachp;
	
	@Column(name = "sachz", length = 3)
	private String sachz;
	
	@Column(name = "sname", length = 30)
	private String sname;
	
	@Column(name = "ename", length = 40)
	private String ename;
	
	@Column(name = "otype", length = 2)
	private String otype;
	
	@Column(name = "sbmod", length = 4)
	private String sbmod;
	
	public IT0001Trx() {}
	
	public IT0001Trx(String id) {
		this();
		this.id = id;
	}

	/**
	 * GET Employee Number.
	 * 
	 * @return
	 */
	public long getPernr() {
		return pernr;
	}

	public void setPernr(long pernr) {
		this.pernr = pernr;
	}

	/**
	 * GET End Date.
	 * 
	 * @return
	 */
	public Date getEndda() {
		return endda;
	}

	public void setEndda(Date endda) {
		this.endda = endda;
	}

	/**
	 * GET Begin Date.
	 * 
	 * @return
	 */
	public Date getBegda() {
		return begda;
	}

	public void setBegda(Date begda) {
		this.begda = begda;
	}

	/**
	 * GET Infotype Record No.
	 * 
	 * @return
	 */
	public long getSeqnr() {
		return seqnr;
	}

	public void setSeqnr(long seqnr) {
		this.seqnr = seqnr;
	}

	/**
	 * GET Changed On.
	 * 
	 * @return
	 */
	public Date getAedtm() {
		return aedtm;
	}

	public void setAedtm(Date aedtm) {
		this.aedtm = aedtm;
	}

	/**
	 * GET Changed By.
	 * 
	 * @return
	 */
	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	
	/**
	 * GET Personnel Area.
	 * 
	 * @return
	 */
	public T500P getT500p() {
		return t500p;
	}

	public void setT500p(T500P t500p) {
		this.t500p = t500p;
	}

	/**
	 * GET Employee Group.
	 * 
	 * @return
	 */
	public T501T getT501t() {
		return t501t;
	}

	public void setT501t(T501T t501t) {
		this.t501t = t501t;
	}

	/**
	 * GET Employee Sub Group.
	 * 
	 * @return
	 */
	public T503K getT503k() {
		return t503k;
	}

	public void setT503k(T503K t503k) {
		this.t503k = t503k;
	}

	/**
	 * GET Organizational Key.
	 * 
	 * @return
	 */
	public String getVdsk1() {
		return vdsk1;
	}

	public void setVdsk1(String vdsk1) {
		this.vdsk1 = vdsk1;
	}

	/**
	 * GET Business Area.
	 * 
	 * @return
	 */
	public String getGsber() {
		return gsber;
	}

	public void setGsber(String gsber) {
		this.gsber = gsber;
	}

	/**
	 * GET Personnel Sub Area.
	 * 
	 * @return
	 */
	public V001PAll getV001pall() {
		return v001pall;
	}

	public void setV001pall(V001PAll v001pall) {
		this.v001pall = v001pall;
	}

	/**
	 * GET Payroll Area.
	 * 
	 * @return
	 */
	public T549T getT549t() {
		return t549t;
	}

	public void setT549t(T549T t549t) {
		this.t549t = t549t;
	}

	/**
	 * GET Cost Center.
	 * 
	 * @return
	 */
	public CSKT getCskt() {
		return cskt;
	}

	public void setCskt(CSKT cskt) {
		this.cskt = cskt;
	}

	/**
	 * GET Organizational Unit.
	 * 
	 * @return
	 */
	public T527X getT527x() {
		return t527x;
	}

	public void setT527x(T527X t527x) {
		this.t527x = t527x;
	}

	/**
	 * GET Position.
	 * 
	 * @return
	 */
	public T528T getT528t() {
		return t528t;
	}

	public void setT528t(T528T t528t) {
		this.t528t = t528t;
	}

	/**
	 * GET Position.
	 * 
	 * @return
	 */
	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	/**
	 * GET Job Key.
	 * 
	 * @return
	 */
	public T513S getT513s() {
		return t513s;
	}

	public void setT513s(T513S t513s) {
		this.t513s = t513s;
	}

	/**
	 * GET Job Key.
	 * 
	 * @return
	 */
	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	/**
	 * GET Supervisor Area.
	 * 
	 * @return
	 */
	public String getMstbr() {
		return mstbr;
	}

	public void setMstbr(String mstbr) {
		this.mstbr = mstbr;
	}

	/**
	 * GET Payroll Administrator.
	 * 
	 * @return
	 */
	public String getSacha() {
		return sacha;
	}

	public void setSacha(String sacha) {
		this.sacha = sacha;
	}

	/**
	 * GET Pers.Administrator.
	 * 
	 * @return
	 */
	public String getSachp() {
		return sachp;
	}

	public void setSachp(String sachp) {
		this.sachp = sachp;
	}

	/**
	 * GET Time Administrator.
	 * 
	 * @return
	 */
	public String getSachz() {
		return sachz;
	}

	public void setSachz(String sachz) {
		this.sachz = sachz;
	}

	/**
	 * GET Last Name First Name.
	 * 
	 * @return
	 */
	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	/**
	 * GET Employee/Appl.Name.
	 * 
	 * @return
	 */
	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	/**
	 * GET Object Type.
	 * 
	 * @return
	 */
	public String getOtype() {
		return otype;
	}

	public void setOtype(String otype) {
		this.otype = otype;
	}

	/**
	 * GET Administrator Group.
	 * 
	 * @return
	 */
	public String getSbmod() {
		return sbmod;
	}

	public void setSbmod(String sbmod) {
		this.sbmod = sbmod;
	}			
}