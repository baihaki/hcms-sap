package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 * 
 * Configuration table for <strong>Vehicle Types</strong>. 
 * Represents table <strong>cfg_vehicle_types</strong> in the database.
 */
@Entity
@Table(name = "cfg_vehicle_types", uniqueConstraints = {
		@UniqueConstraint(columnNames = "ident_name")
})
public class VehicleType extends AbstractEntity {

	private static final long serialVersionUID = 4532056139533550115L;

	@Column(name = "ident_name", unique = true, nullable = false)
	private String identName;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "description", nullable = true)
	private String description;
	
	public VehicleType() {}
	
	public String getIdentName() {
		return identName;
	}
	
	public void setIdentName(String identName) {
		this.identName = identName;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
}
