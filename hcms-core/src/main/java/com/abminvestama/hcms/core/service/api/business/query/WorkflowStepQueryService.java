package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.Workflow;
import com.abminvestama.hcms.core.model.entity.WorkflowStep;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface WorkflowStepQueryService extends DatabaseQueryService<WorkflowStep, String>, DatabasePaginationQueryService {

	Optional<WorkflowStep> findByWorkflowIdAndSeqAndPositionId(String workflowId, int seq, String positionId);

	Optional<Collection<WorkflowStep>> findByWorkflowId(String workflowId);
	
	Optional<Collection<WorkflowStep>> findByWorkflowAndSeq(final Workflow workflow, final int seq);
	
	Optional<Collection<WorkflowStep>> findByWorkflowAndGreaterThanSeq(final Workflow workflow, final int seq);
	
	Page<WorkflowStep> fetchAllWithPaging(int pageNumber);
}