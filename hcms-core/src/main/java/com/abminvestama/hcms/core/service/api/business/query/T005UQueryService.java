package com.abminvestama.hcms.core.service.api.business.query;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.T005U;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface T005UQueryService extends DatabaseQueryService<T005U, String>, DatabasePaginationQueryService {

	Page<T005U> fetchAllWithPaging(int pageNumber);
}