package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * Class that represents <strong>Group Account</strong> object.
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 3.0.0 (Cash Advance)
 * @since 3.0.0
 * 
 */
@Entity
@Table(name = "ca_group_account")
public class CaGroupAccount implements Serializable {
	
	private static final long serialVersionUID = 5038712132562166442L;
	
	@Id
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "group_acct_text")
	private String groupAcctText;
	
	@Column(name = "blocked_for_posting")
	private boolean blockedForPosting;
	
	@Column(name = "valuation_group")
	private String valuationGroup;
	
	public void setId(Long id){
		this.id = id;
	}
 
	public String getGroupAcctText(){
		return groupAcctText;
	}
	
	public void setGroupAcctText(String groupAcctText){
		this.groupAcctText = groupAcctText;
	}
 
	public boolean getBlockedForPosting(){
		return blockedForPosting;
	}
	
	public void setBlockedForPosting(boolean blockedForPosting){
		this.blockedForPosting = blockedForPosting;
	}
 
	public String getValuationGroup(){
		return valuationGroup;
	}
	
	public void setValuationGroup(String valuationGroup){
		this.valuationGroup = valuationGroup;
	}
	
	public CaGroupAccount(){
	}

	public CaGroupAccount( Long id,  String groupAcctText,  boolean blockedForPosting,  String valuationGroup ){
	 	this.id = id;
	 	this.groupAcctText = groupAcctText;
	 	this.blockedForPosting = blockedForPosting;
	 	this.valuationGroup = valuationGroup;
	}
	
	
	public Long getId() {
		return  id;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		CaGroupAccount caGroupAccount = (CaGroupAccount) o;
		 
		if (id != null ? !id.equals(caGroupAccount.id) : caGroupAccount.id != null) return false; 
		if (groupAcctText != null ? !groupAcctText.equals(caGroupAccount.groupAcctText) : caGroupAccount.groupAcctText != null) return false; 
		if (blockedForPosting != caGroupAccount.blockedForPosting) return false;  
		if (valuationGroup != null ? !valuationGroup.equals(caGroupAccount.valuationGroup) : caGroupAccount.valuationGroup != null) return false;
		return true; 
	}
	
	@Override
	public int hashCode() {
	    int result = (id != null ?  id.hashCode() : 0); 
		result = 31 * (groupAcctText != null ?  groupAcctText.hashCode() : 0); 
		result = 31 * result + (blockedForPosting ? 1 : 0); 	result = 31 * (valuationGroup != null ?  valuationGroup.hashCode() : 0); 
	 
		return result; 
	}
	
	@Override
	public String toString() {
		return "caGroupAccount(id="+id+",groupAcctText="+groupAcctText+",blockedForPosting="+blockedForPosting+",valuationGroup="+valuationGroup+')'; 
	}
 
}

	