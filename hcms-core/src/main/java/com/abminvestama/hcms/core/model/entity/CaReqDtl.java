package com.abminvestama.hcms.core.model.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * Class that represents <strong>Cash Advance Request Detail</strong> transaction.
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Entity
@Table(name = "ca_req_dtl")
public class CaReqDtl extends SAPAbstractEntityZmed<CaReqDtlKey> {
	
	private static final long serialVersionUID = -5647958853047927512L;

	@Column(name = "item_no", nullable = false, insertable = false, updatable = false)
	private Short itemNo;
	
	@Column(name = "req_no", nullable = false, insertable = false, updatable = false)
	private String reqNo;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "price_per_unit")
	private Double pricePerUnit;
	
	@Column(name = "quantity")
	private Integer quantity;
	
	@Column(name = "amount")
	private Double amount;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	private Date createdAt;
	
	@ManyToOne
	@JoinColumn(name = "created_by", referencedColumnName = "id")
	private User createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	private Date updatedAt;
	
	@ManyToOne
	@JoinColumn(name = "updated_by", referencedColumnName = "id")
	private User updatedBy;
 
	public Short getItemNo(){
		return itemNo;
	}
	
	public void setItemNo(Short itemNo){
		this.itemNo = itemNo;
	}
 
	public String getReqNo(){
		return reqNo;
	}
	
	public void setReqNo(String reqNo){
		this.reqNo = reqNo;
	}
 
	public String getDescription(){
		return description;
	}
	
	public void setDescription(String description){
		this.description = description;
	}
 
	public Double getPricePerUnit(){
		return pricePerUnit;
	}
	
	public void setPricePerUnit(Double pricePerUnit){
		this.pricePerUnit = pricePerUnit;
	}
 
	public Integer getQuantity(){
		return quantity;
	}
	
	public void setQuantity(Integer quantity){
		this.quantity = quantity;
	}
 
	public Double getAmount(){
		return amount;
	}
	
	public void setAmount(Double amount){
		this.amount = amount;
	}
 
	public String getRemarks(){
		return remarks;
	}
	
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
 
	public Date getCreatedAt(){
		return createdAt;
	}
	
	public void setCreatedAt(Date createdAt){
		this.createdAt = createdAt;
	}
 
	public User getCreatedBy(){
		return createdBy;
	}
	
	public void setCreatedBy(User createdBy){
		this.createdBy = createdBy;
	}
 
	public Date getUpdatedAt(){
		return updatedAt;
	}
	
	public void setUpdatedAt(Date updatedAt){
		this.updatedAt = updatedAt;
	}
 
	public User getUpdatedBy(){
		return updatedBy;
	}
	
	public void setUpdatedBy(User updatedBy){
		this.updatedBy = updatedBy;
	}
	
	public CaReqDtl(){
	}

	public CaReqDtl(CaReqDtlKey id) {
		this();
		this.id = id;
	}


	public CaReqDtl(Short itemNo, String reqNo, String description, Double pricePerUnit, Integer quantity, Double amount,
			String remarks, Date createdAt, User createdBy, Date updatedAt, User updatedBy ){
	 	this.itemNo = itemNo;
	 	this.reqNo = reqNo;
	 	this.description = description;
	 	this.pricePerUnit = pricePerUnit;
	 	this.quantity = quantity;
	 	this.amount = amount;
	 	this.remarks = remarks;
	 	this.createdAt = createdAt;
	 	this.createdBy = createdBy;
	 	this.updatedAt = updatedAt;
	 	this.updatedBy = updatedBy;
	}
	
	@PrePersist
	final void prePersist() {
		this.createdAt = this.updatedAt = new Date();
	}
	
	@PreUpdate
	final void preUpdate() {
		this.updatedAt = new Date();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		CaReqDtl caReqDtl = (CaReqDtl) o;
		 
		if (itemNo != null ? !itemNo.equals(caReqDtl.itemNo) : caReqDtl.itemNo != null) return false; 
		if (reqNo != null ? !reqNo.equals(caReqDtl.reqNo) : caReqDtl.reqNo != null) return false; 
		if (description != null ? !description.equals(caReqDtl.description) : caReqDtl.description != null) return false; 
		if (pricePerUnit != null ? !pricePerUnit.equals(caReqDtl.pricePerUnit) : caReqDtl.pricePerUnit != null) return false; 
		if (quantity != caReqDtl.quantity) return false;  
		if (amount != null ? !amount.equals(caReqDtl.amount) : caReqDtl.amount != null) return false; 
		if (remarks != null ? !remarks.equals(caReqDtl.remarks) : caReqDtl.remarks != null) return false; 
		if (createdAt != null ? !createdAt.equals(caReqDtl.createdAt) : caReqDtl.createdAt != null) return false; 
		if (createdBy != null ? !createdBy.equals(caReqDtl.createdBy) : caReqDtl.createdBy != null) return false; 
		if (updatedAt != null ? !updatedAt.equals(caReqDtl.updatedAt) : caReqDtl.updatedAt != null) return false; 
		if (updatedBy != null ? !updatedBy.equals(caReqDtl.updatedBy) : caReqDtl.updatedBy != null) return false;
		return true; 
	}
	
	@Override
	public int hashCode() {
	    int result = (itemNo != null ?  itemNo.hashCode() : 0); 
		result = 31 * (reqNo != null ?  reqNo.hashCode() : 0); 
		result = 31 * (description != null ?  description.hashCode() : 0); 
		result = 31 * (pricePerUnit != null ?  pricePerUnit.hashCode() : 0); 
		result = 31 * result + quantity; 	result = 31 * (amount != null ?  amount.hashCode() : 0); 
		result = 31 * (remarks != null ?  remarks.hashCode() : 0); 
		result = 31 * (createdAt != null ?  createdAt.hashCode() : 0); 
		result = 31 * (createdBy != null ?  createdBy.hashCode() : 0); 
		result = 31 * (updatedAt != null ?  updatedAt.hashCode() : 0); 
		result = 31 * (updatedBy != null ?  updatedBy.hashCode() : 0); 
	 
		return result; 
	}
	
	@Override
	public String toString() {
		return "caReqDtl(itemNo="+itemNo+",reqNo="+reqNo+",description="+description+",pricePerUnit="+pricePerUnit+",quantity="+quantity+",amount="+amount+",remarks="+remarks+",createdAt="+createdAt+",createdBy="+createdBy+",updatedAt="+updatedAt+",updatedBy="+updatedBy+')'; 
	}
 
}

	