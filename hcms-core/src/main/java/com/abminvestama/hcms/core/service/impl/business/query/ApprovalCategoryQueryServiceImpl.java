package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.EntityCategory;
import com.abminvestama.hcms.core.model.entity.ApprovalCategory;
import com.abminvestama.hcms.core.model.entity.Workflow;
import com.abminvestama.hcms.core.repository.ApprovalCategoryRepository;
import com.abminvestama.hcms.core.service.api.business.query.ApprovalCategoryQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("approvalCategoryQueryService")
@Transactional(readOnly = true)
public class ApprovalCategoryQueryServiceImpl implements ApprovalCategoryQueryService {

	private ApprovalCategoryRepository approvalCategoryRepository;
	
	@Autowired
	ApprovalCategoryQueryServiceImpl(ApprovalCategoryRepository approvalCategoryRepository) {
		this.approvalCategoryRepository = approvalCategoryRepository;
	}
	
	@Override
	public Optional<ApprovalCategory> findById(Optional<String> id) throws Exception {
		return id.map(pk -> approvalCategoryRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<ApprovalCategory>> fetchAll() {
		List<ApprovalCategory> approvalCategoryList = new ArrayList<>();
		Optional<Iterable<ApprovalCategory>> approvalCategoryIterable = Optional.ofNullable(approvalCategoryRepository.findAll());
		return approvalCategoryIterable.map(iter -> {
			iter.forEach(approvalCategory -> approvalCategoryList.add(approvalCategory));
			return approvalCategoryList;
		});				
	}

	@Override
	public Optional<ApprovalCategory> findByWorkflowAndEntityCategory(Workflow workflow,
			EntityCategory entityCategory) {
		return Optional.ofNullable(approvalCategoryRepository.findByWorkflowAndEntityCategory(workflow, entityCategory));
	}
}