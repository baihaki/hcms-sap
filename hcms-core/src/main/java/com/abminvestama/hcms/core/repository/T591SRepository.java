package com.abminvestama.hcms.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.T591S;
import com.abminvestama.hcms.core.model.entity.T591SKey;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface T591SRepository extends CrudRepository<T591S, T591SKey> {
	
	Page<T591S> findAll(Pageable pageRequest);
	
	@Query("FROM T591S t591s WHERE t591s.id.infty = :infty")
	Page<T591S> findAllByInfotype(Pageable pageRequest, @Param("infty") String infty);
}