package com.abminvestama.hcms.core.exception;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class EntityNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1237132455912398436L;
	
	public EntityNotFoundException(final String message) {
		super(message);
	}
	
	public EntityNotFoundException(final String message, Throwable throwable) {
		super(message, throwable);
	}
}