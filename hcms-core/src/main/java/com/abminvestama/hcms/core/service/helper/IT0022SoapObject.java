package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0022;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0022SoapObject {
	
	private String employeenumber;
	private String validitybegin;
	private String validityend;
	private String infty;
	private String subty;
	private String seqnr;
	private String slart;
	private String insti;
	private String emark;
	private String sland;
	private String ausbi;
	private String slabs;
	private String anzkl;
	private String anzeh;
	private String sltp1;
	private String sltp2;
	
	private IT0022SoapObject() {}
	
	public IT0022SoapObject(IT0022 object) {
		if (object == null) {
			new IT0022SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			//infty = object.getSubty().getId().getSubty();
			subty = object.getId().getSubty();
			//seqnr = object.getSeqnr().toString();
			slart = object.getSlart().getSlart();
			insti = object.getInsti();
			emark = object.getEmark();
			sland = object.getSland().getLand1();
			ausbi = object.getAusbi().getAusbi().toString();
			slabs = object.getSlabs() != null ? object.getSlabs().getSlabs() : "";
			anzkl = object.getAnzkl().toString();
			anzeh = object.getAnzeh().getZeinh();
			sltp1 = object.getSltp1() != null ? object.getSltp1().getFaart() : null;
			sltp2 = object.getSltp2() != null ? object.getSltp2().getFaart() : null;
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}


	public String getInfty() {
		return infty;
	}

	public String getSubty() {
		return subty;
	}

	public String getSeqnr() {
		return seqnr;
	}

	public String getSlart() {
		return slart;
	}

	public String getInsti() {
		return insti;
	}

	public String getEmark() {
		return emark;
	}

	public String getSland() {
		return sland;
	}

	public String getAusbi() {
		return ausbi;
	}

	public String getSlabs() {
		return slabs;
	}

	public String getAnzkl() {
		return anzkl;
	}

	public String getAnzeh() {
		return anzeh;
	}

	public String getSltp1() {
		return sltp1;
	}

	public String getSltp2() {
		return sltp2;
	}
	

}
