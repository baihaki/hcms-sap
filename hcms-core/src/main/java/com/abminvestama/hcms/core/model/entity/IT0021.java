package com.abminvestama.hcms.core.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;

/**
 * Class that represents master data for <strong>Family and Emergency Info</strong> (i.t. IT0021 in SAP).
 * 
 * @since 1.0.0
 * @version 1.0.5
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add fields: address, phone</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add infty2 field, set to famsa JoinColumn</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Extends {@link SAPSynchronizeEntity} for Synchronization fields</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add attachment, attachmentPath field</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add status (DocumentStatus) field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 * 
 */
@Entity
@Table(name = "it0021")
public class IT0021 extends SAPSynchronizeEntity<ITCompositeKeys> {

	private static final long serialVersionUID = 8047464623923365661L;
	
	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "infty", referencedColumnName = "infty", insertable = false, updatable = false),
		@JoinColumn(name = "subty", referencedColumnName = "subty", insertable = false, updatable = false)
	})
	private T591S subty;
	
	public IT0021() {}
	
	public IT0021(ITCompositeKeys id) {
		this();
		this.id = id;
	}
	
	@Column(name = "seqnr")
	private Long seqnr;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "infty2", referencedColumnName = "infty"),
		@JoinColumn(name = "famsa", referencedColumnName = "subty")		
	})
	private T591S famsa;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "fgbdt")
	private Date fgbdt;
	
	@ManyToOne
	@JoinColumn(name = "fgbld", referencedColumnName = "land1")
	private T005T fgbld;
	
	@ManyToOne
	@JoinColumn(name = "fanat", referencedColumnName = "land1")
	private T005T fanat;
	
	@Column(name = "fasex")
	private String fasex;
	
	@Column(name = "favor")
	private String favor;
	
	@Column(name = "fanam")
	private String fanam;
	
	@Column(name = "fgbot")
	private String fgbot;
	
	@Column(name = "fcnam")
	private String fcnam;
	
	@Column(name = "fknzn")
	private Integer fknzn;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "fnmzu", referencedColumnName = "art"),
		@JoinColumn(name = "title", referencedColumnName = "title")
	})
	private T535N t535n;
	
	@Column(name = "document_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private DocumentStatus status;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "infty", referencedColumnName = "infty", insertable = false, updatable = false),
		@JoinColumn(name = "attach1_subty", referencedColumnName = "subty", insertable = false, updatable = false)
	})
	private Attachment attachment;
	
	@Column(name = "attach1_path")
	private String attachmentPath;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "phone")
	private String phone;
	
	/**
	 * GET Employee SSN.
	 * 
	 * @return
	 */
	public Long getPernr() {
		return pernr;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	public T591S getSubty() {
		return subty;
	}
	
	/**
	 * GET Infotype Record No.
	 * 
	 * @return
	 */
	public Long getSeqnr() {
		return seqnr;
	}
	
	public void setSeqnr(Long seqnr) {
		this.seqnr = seqnr;
	}
	
	/**
	 * GET Family Member.
	 * 
	 * @return
	 */
	public T591S getFamsa() {
		return famsa;
	}
	
	public void setFamsa(T591S famsa) {
		this.famsa = famsa;
	}
	
	/**
	 * GET Date of Birth.
	 * 
	 * @return
	 */
	public Date getFgbdt() {
		return fgbdt;
	}
	
	public void setFgbdt(Date fgbdt) {
		this.fgbdt = fgbdt;
	}
	
	/**
	 * GET Country of Birth.
	 * 
	 * @return
	 */
	public T005T getFgbld() {
		return fgbld;
	}
	
	public void setFgbld(T005T fgbld) {
		this.fgbld = fgbld;
	}
	
	/**
	 * GET Nationality.
	 * 
	 * @return
	 */
	public T005T getFanat() {
		return fanat;
	}
	
	public void setFanat(T005T fanat) {
		this.fanat = fanat;
	}
	
	/**
	 * GET Gender (Sex).
	 * 
	 * @return
	 */
	public String getFasex() {
		return fasex;
	}
	
	public void setFasex(String fasex) {
		this.fasex = fasex;
	}
	
	/**
	 * GET First Name.
	 * 
	 * @return
	 */
	public String getFavor() {
		return favor;
	}
	
	public void setFavor(String favor) {
		this.favor = favor;
	}
	
	/**
	 * GET Last Name.
	 * 
	 * @return
	 */
	public String getFanam() {
		return fanam;
	}
	
	public void setFanam(String fanam) {
		this.fanam = fanam;
	}
	
	/**
	 * GET Birth place.
	 * 
	 * @return
	 */
	public String getFgbot() {
		return fgbot;
	}
	
	public void setFgbot(String fgbot) {
		this.fgbot = fgbot;
	}
	
	/**
	 * GET Full name.
	 * 
	 * @return
	 */
	public String getFcnam() {
		return fcnam;
	}
	
	public void setFcnam(String fcnam) {
		this.fcnam = fcnam;
	}
	
	/**
	 * GET Name Format Ind.
	 * 
	 * @return
	 */
	public Integer getFknzn() {
		return fknzn;
	}
	
	public void setFknzn(Integer fknzn) {
		this.fknzn = fknzn;
	}
	
	/**
	 * GET Title.
	 * 
	 * @return
	 */
	public T535N getT535n() {
		return t535n;
	}
	
	public void setT535n(T535N t535n) {
		this.t535n = t535n;
	}
	
	/**
	 * GET Document Status.
	 * 
	 * @return
	 */
	public DocumentStatus getStatus() {
		return status;
	}
	
	public void setStatus(DocumentStatus status) {
		this.status = status;
	}

	/**
	 * GET Attachment Document Type.
	 * 
	 * @return
	 */
	public Attachment getAttachment() {
		return attachment;
	}
	
	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	/**
	 * GET Attachment Document file path.
	 * 
	 * @return
	 */
	public String getAttachmentPath() {
		return attachmentPath;
	}
	
	public void  setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}

	/**
	 * GET Emergency Contact Address.
	 * 
	 * @return
	 */
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * GET Emergency Contact Phone Number.
	 * 
	 * @return
	 */
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}