package com.abminvestama.hcms.core.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "zmed_empquotastf")
public class ZmedEmpquotastf extends SAPAbstractEntityZmed<ZmedCompositeKeyEmpquotastf>{

	private static final long serialVersionUID = 2496232738510047249L;

	public ZmedEmpquotastf() {}


	public ZmedEmpquotastf(ZmedCompositeKeyEmpquotastf id) {
		this();
		this.id = id;
	}
	@Column(name = "stell", nullable = false, insertable = false, updatable = false)
	private Long stell;
	

	@ManyToOne
	@JoinColumn(name = "bukrs", referencedColumnName = "bukrs", nullable = false, insertable = false, updatable = false)
	private T001 bukrs;
	//@Column(name = "bukrs", nullable = false, insertable = false, updatable = false)
	//private String bukrs;

	@Column(name = "persg", nullable = false, insertable = false, updatable = false)
	private String persg;

	@Column(name = "persk", nullable = false, insertable = false, updatable = false)
	private String persk;

	@Column(name = "kodequo", nullable = false, insertable = false, updatable = false)
	private String kodequo;

	@Temporal(TemporalType.DATE)
	@Column(name = "datab", nullable = false, insertable = false, updatable = false)
	private Date datab;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datbi", nullable = false, insertable = false, updatable = false)
	private Date datbi;

	@Column(name = "quotamt")
	private Double quotamt;

	public Long getStell() {
		return stell;
	}

	public void setStell(Long stell) {
		this.stell = stell;
	}

	public T001 getBukrs() {
		return bukrs;
	}

	public void setBukrs(T001 bukrs) {
		this.bukrs = bukrs;
	}

	public String getPersg() {
		return persg;
	}

	public void setPersg(String persg) {
		this.persg = persg;
	}

	public String getPersk() {
		return persk;
	}

	public void setPersk(String persk) {
		this.persk = persk;
	}

	public String getKodequo() {
		return kodequo;
	}

	public void setKodequo(String kodequo) {
		this.kodequo = kodequo;
	}

	public Date getDatab() {
		return datab;
	}

	public void setDatab(Date datab) {
		this.datab = datab;
	}

	public Date getDatbi() {
		return datbi;
	}

	public void setDatbi(Date datbi) {
		this.datbi = datbi;
	}

	public Double getQuotamt() {
		return quotamt;
	}

	public void setQuotamt(Double quotamt) {
		this.quotamt = quotamt;
	}
	
}