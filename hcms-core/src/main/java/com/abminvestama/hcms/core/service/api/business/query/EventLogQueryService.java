package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;

import javax.validation.constraints.NotNull;

import com.abminvestama.hcms.core.exception.DataViolationException;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface EventLogQueryService extends DatabaseQueryService<EventLog, String> {
	
	Collection<EventLog> findByEntityIdAndObjectId(final String id, final String objectId) throws DataViolationException;
	
	@NotNull
	Collection<EventLog> findByEntityIdAndObjectIdAndOperationType(final String id, final String objectId,
			final OperationType operationType);
}