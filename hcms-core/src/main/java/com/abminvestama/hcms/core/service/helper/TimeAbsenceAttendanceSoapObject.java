package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.IT2001;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class TimeAbsenceAttendanceSoapObject {

	private String employeenumber;
	private String validitybegin;
	private String validityend;

	private String infotype;
	private String absencetype;
	private String absencehours;
	private String start;
	private String end;
	//private String nocommit;
	
	private TimeAbsenceAttendanceSoapObject() {}
	
	public TimeAbsenceAttendanceSoapObject(IT2001 object) {
		if (object == null) {
			new TimeAbsenceAttendanceSoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			infotype = object.getSubty() != null ? object.getSubty().getId().getInfty() : object.getId().getInfty();
			absencetype = object.getSubty() != null ? object.getSubty().getId().getSubty() : object.getId().getSubty();
			absencehours = object.getStdaz() != null && object.getStdaz() > 0 ? String.valueOf(object.getStdaz().intValue()) : null;
			start = object.getStart() != null ? CommonDateFunction.convertDateToStringHMS(object.getStart()) : null;
			end = object.getEnd() != null ? CommonDateFunction.convertDateToStringHMS(object.getEnd()) : null;
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}

	public String getInfotype() {
		return infotype;
	}

	public String getAbsencehours() {
		return absencehours;
	}

	public String getAbsencetype() {
		return absencetype;
	}

	public String getStart() {
		return start;
	}

	public String getEnd() {
		return end;
	}
	
}
