package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;

import com.abminvestama.hcms.core.model.entity.TrxApprovalProcess;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface TrxApprovalProcessQueryService extends DatabaseQueryService<TrxApprovalProcess, String>, DatabasePaginationQueryService {
	Collection<TrxApprovalProcess> fetchByEntityObjectId(String entityObjectId, String orderBy);
}
