package com.abminvestama.hcms.core.service.helper;

import java.util.Map;
import java.util.Optional;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.IT0021;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.T005T;
import com.abminvestama.hcms.core.model.entity.T535N;
import com.abminvestama.hcms.core.model.entity.T535NKey;
import com.abminvestama.hcms.core.model.entity.T591S;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 3.0.0
 * @since 3.0.0
 *
 */
public class IT0021Mapper {
	
	private IT0021 it0021;
	
	private IT0021Mapper() {}
	
	public IT0021Mapper(Map<String, String> map, CommonServiceFactory commonServiceFactory) {
		if (map == null) {
			new IT0021Mapper();
			it0021 = new IT0021();
		} else {
			ITCompositeKeys key = new ITCompositeKeys(Long.valueOf(map.get("field1")), SAPInfoType.EMERGENCY_INFO.infoType(), map.get("field2"),
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("field3")),
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("field4")));
			it0021 = new IT0021(key);
			it0021.setSeqnr(map.get("field5") != null ? Long.valueOf(map.get("field5")) : 0);
			Optional<T591SKey> famsaKey = Optional.ofNullable(new T591SKey(SAPInfoType.EMERGENCY_INFO.infoType(), map.get("field8")));
			Optional<T591S> famsaObject;
			try {
				famsaObject = commonServiceFactory.getT591SQueryService().findById(famsaKey);
			} catch (Exception e) {
				famsaObject = Optional.empty();
			}
			it0021.setFamsa(famsaObject.isPresent() ? famsaObject.get() : null);
			it0021.setFgbdt(map.get("field9") != null && !map.get("field9").equals("0000-00-00") ?
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("field9")) : null);
			Optional<T005T> fgbldObject;
			try {
				fgbldObject = commonServiceFactory.getT005TQueryService().findById(Optional.ofNullable(map.get("field10")));
			} catch (Exception e1) {
				fgbldObject = Optional.empty();
			}
			it0021.setFgbld(fgbldObject.isPresent() ? fgbldObject.get() : null);
			Optional<T005T> fanatObject;
			try {
				fanatObject = commonServiceFactory.getT005TQueryService().findById(Optional.ofNullable(map.get("field11")));
			} catch (Exception e1) {
				fanatObject = Optional.empty();
			}
			it0021.setFanat(fanatObject.isPresent() ? fanatObject.get() : null);
			it0021.setFasex(map.get("field12"));
			it0021.setFavor(map.get("field13"));
			it0021.setFanam(map.get("field14"));
			it0021.setFgbot(map.get("field15"));
			it0021.setFcnam(map.get("field14"));
			it0021.setFknzn(map.get("field17") != null ? Integer.valueOf(map.get("field17")) : 0);
			if (map.get("field18") != null && map.get("field19") != null) {
				T535NKey t555nKey = new T535NKey(map.get("field18"), map.get("field19"));
				T535N t535n = new T535N(t555nKey);
				it0021.setT535n(t535n);
			}
			it0021.setStatus(DocumentStatus.PUBLISHED);
		}		
	}

	public IT0021 getIT0021() {
		return it0021;
	}
	
}
