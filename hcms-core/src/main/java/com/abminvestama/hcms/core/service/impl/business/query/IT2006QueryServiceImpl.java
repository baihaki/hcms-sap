package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.IT2006;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.IT2006Repository;
import com.abminvestama.hcms.core.service.api.business.query.IT2006QueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add method: findByBukrsWithPaging</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Change call findByPernr to findValidByPernr, findByPernrAndSubty to findValidByPernrAndSubty</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("it2006QueryService")
@Transactional(readOnly = true)
public class IT2006QueryServiceImpl implements IT2006QueryService {

	private IT2006Repository it2006Repository;
	
	@Autowired
	IT2006QueryServiceImpl(IT2006Repository it2006Repository) {
		this.it2006Repository = it2006Repository;
	}
	
	@Override
	public Optional<IT2006> findById(Optional<ITCompositeKeys> id) throws Exception {
		throw new NoSuchMethodException("Method not implemented. Please use method findOneByCompositeKeys instead.");		
	}

	@Override
	public Optional<Collection<IT2006>> fetchAll() {
		Optional<Iterable<IT2006>> it2006Iterates = Optional.ofNullable(it2006Repository.findAll());
		if (!it2006Iterates.isPresent()) {
			return Optional.empty();
		}
		
		List<IT2006> listOfIT2006 = new ArrayList<>();
		it2006Iterates.map(it2006Iterable -> {			
			it2006Iterable.forEach(it2006 -> {
				listOfIT2006.add(it2006);
			});
			return listOfIT2006;
		});
		
		return Optional.of(listOfIT2006);
	}

	@Override
	public Optional<IT2006> findOneByCompositeKey(Long pernr, String subty, Date endda, Date begda) {
		if (pernr == null || StringUtils.isBlank(subty) || endda == null || begda == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(it2006Repository.findOneByCompositeKey(pernr, subty, endda, begda));
	}

	@Override
	public Collection<IT2006> findByPernr(Long pernr) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT2006>> bunchOfIT2006 
			= Optional.ofNullable(it2006Repository.findValidByPernr(pernr));
		
		return (bunchOfIT2006.isPresent()
				? bunchOfIT2006.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

	@Override
	public Collection<IT2006> findByPernrAndSubty(Long pernr, String subty) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		if (StringUtils.isBlank(subty)) {
			subty = "10"; // default to subtype '1' (Quota Cuti Tahunan)
		}
		
		Optional<Collection<IT2006>> bunchOfIT2006 
			= Optional.ofNullable(it2006Repository.findValidByPernrAndSubty(pernr, subty));

		return (bunchOfIT2006.isPresent() 
					? bunchOfIT2006.get().stream().collect(Collectors.toList())
					: Collections.emptyList());		
	}
	
	@Override
	public Page<IT2006> findByBukrsWithPaging(String bukrs, User superiorUser, int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		List<IT2006> listOfIT2006 = Collections.emptyList();
		int total = 0;
		
		if (superiorUser != null) {
			total = it2006Repository.findByBukrsAndSuperiorPositionCount(bukrs, superiorUser.getEmployee().getPosition().getId(),
					superiorUser.getEmployee().getId().getPernr());
			listOfIT2006 = it2006Repository.findByBukrsAndSuperiorPositionWithPaging(
					bukrs, superiorUser.getEmployee().getPosition().getId(), superiorUser.getEmployee().getId().getPernr(),
					pageRequest.getPageSize(), pageRequest.getOffset()).
					stream().collect(Collectors.toList());
		} else {
			total = it2006Repository.findByBukrsCount(bukrs);
			listOfIT2006 = it2006Repository.findByBukrsWithPaging(
					bukrs, pageRequest.getPageSize(), pageRequest.getOffset()).
					stream().collect(Collectors.toList());
		}
		
		Page<IT2006> page = new PageImpl<IT2006>(listOfIT2006, pageRequest, total);
		return page;
	}
}