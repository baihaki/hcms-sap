package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 * Class that represents master data <strong>Date Type</strong> (i.e. T548T in SAP).
 * 
 */
@Entity
@Table(name = "t548t")
public class T548T implements Serializable {

	private static final long serialVersionUID = 477134939064500813L;
	
	@Id
	@Column(name = "datar", unique = true, nullable = false)
	private String datar;
	
	@Column(name = "dtext", nullable = false)
	private String dtext;
	
	/**
	 * GET Date Type Code.
	 * 
	 * @return
	 */
	public String getDatar() {
		return datar;
	}
	
	/**
	 * GET Date Type Name/Text.
	 * 
	 * @return
	 */
	public String getDtext() {
		return dtext;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final T548T obj = (T548T) o;
		
		if (obj.getDatar() != null ? !obj.getDatar().equalsIgnoreCase(datar) : datar != null) {
			return false;
		}
		
		if (obj.getDtext() != null ? !obj.getDtext().equalsIgnoreCase(dtext) : dtext != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.datar != null ? this.datar.hashCode() : 0;
		result = result * 31 + (this.dtext != null ? this.dtext.hashCode() : 0);
		return result;
	}
}