package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.IT2001;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.5
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add methods: findByPernrAndProcessWithPaging, findByPernrAndProcessesWithPaging with additional parameters</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessesWithPaging, findByPernrAndProcessesWithPaging</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: findByPernrAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add method: findOneByCompositeKeyWithInfotype, findByPernrWithInfotype, findByPernrAndSubtyWithInfotype</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 * 
 */
public interface IT2001QueryService extends DatabaseQueryService<IT2001, ITCompositeKeys>, DatabasePaginationQueryService {

	
	@NotNull
	Optional<IT2001> findOneByCompositeKey(Long pernr, String subty, Date endda, Date begda);
	
	@NotNull
	Collection<IT2001> findByPernr(Long pernr);
	
	@NotNull
	Collection<IT2001> findByPernrAndSubty(Long pernr, String subty);

	Optional<IT2001> findOneByCompositeKeyWithInfotype(Long pernr,
			String infty, String subty, Date endda, Date begda);

	Collection<IT2001> findByPernrWithInfotype(Long pernr, String infty);

	Collection<IT2001> findByPernrAndSubtyWithInfotype(Long pernr,
			String subty, String infty);

	Page<IT2001> findByBukrsAndProcessWithPaging(String bukrs, String process,
			String superiorPosition, int pageNumber);

	Page<IT2001> findByPernrAndProcessWithPaging(long pernr, String process,
			int pageNumber);

	Page<IT2001> findByBukrsAndProcessesWithPaging(String bukrs,
			String[] processes, int pageNumber);

	Page<IT2001> findByPernrAndProcessesWithPaging(long pernr,
			String[] processes, int pageNumber);

	Page<IT2001> findByPernrAndProcessWithPaging(long pernr, String process,
			int pageNumber, String infty, String subty, String periodBegin,
			String periodEnd);

	Page<IT2001> findByPernrAndProcessesWithPaging(long pernr,
			String[] processes, int pageNumber, String infty, String subty,
			String periodBegin, String periodEnd);
	
}