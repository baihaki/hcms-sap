package com.abminvestama.hcms.core.repository;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.PositionRelation;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface PositionRelationRepository extends CrudRepository<PositionRelation, String> {

	Page<PositionRelation> findAll(Pageable pageRequest);
	
	@Query(
			"FROM PositionRelation positionRelation WHERE positionRelation.fromPosition.id = :fromPositionId ORDER BY " + 
			"positionRelation.positionRelationType.name, positionRelation.id desc"
	)
	Collection<PositionRelation> findByFromPositionId(@Param("fromPositionId") String fromPositionId);
	
	@Query(
			"FROM PositionRelation positionRelation WHERE positionRelation.toPosition.id = :toPositionId ORDER BY " + 
			"positionRelation.positionRelationType.name, positionRelation.id desc"
	)	
	Collection<PositionRelation> findByToPositionId(@Param("toPositionId") String toPositionId);
	
	@Query(
		"FROM PositionRelation positionRelation WHERE positionRelation.fromPosition.id = :fromPositionId ORDER BY " + 
		"positionRelation.positionRelationType.name, positionRelation.id desc"
	)
	Page<PositionRelation> findByFromPositionId(@Param("fromPositionId") String fromPositionId, Pageable pageRequest);
	
	@Query(
			"FROM PositionRelation positionRelation WHERE positionRelation.toPosition.id = :toPositionId ORDER BY " + 
			"positionRelation.positionRelationType.name, positionRelation.id desc"
	)
	Page<PositionRelation> findByToPositionId(@Param("toPositionId") String toPositionId, Pageable pageRequest);	
	
	@Query("FROM PositionRelation positionRelation WHERE positionRelation.fromPosition.id = :fromPositionId AND " + 
			"positionRelation.toPosition.id = :toPositionId ORDER BY positionRelation.positionRelationType.name, positionRelation.id DESC")
	Collection<PositionRelation> findByFromPositionIdAndToPositionId(@Param("fromPositionId") String fromPositionId,
			@Param("toPositionId") String toPositionId);
	
	@Query("FROM PositionRelation positionRelation WHERE positionRelation.fromPosition.id = :fromPositionId AND " + 
			"positionRelation.toPosition.id = :toPositionId AND positionRelation.positionRelationType.name = :relationType")
	Collection<PositionRelation> findByFromPositionIdAndToPositionIdAndRelationType(@Param("fromPositionId") String fromPositionId,
			@Param("toPositionId") String toPositionId, @Param("relationType") String relationType);
}