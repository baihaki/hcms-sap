package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.T705H;
import com.abminvestama.hcms.core.model.entity.T705HKey;
import com.abminvestama.hcms.core.model.entity.T705P;

public interface T705HRepository extends CrudRepository<T705H, T705HKey> {



	@Query("FROM T705H t705h WHERE t705h.pinco = :pinco ")
	T705H findByPinco(@Param("pinco") String pinco);
	
	
}