package com.abminvestama.hcms.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.Attachment;
import com.abminvestama.hcms.core.model.entity.T591SKey;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 *
 */
public interface AttachmentRepository extends CrudRepository<Attachment, T591SKey> {
	
	Page<Attachment> findAll(Pageable pageRequest);
	
	@Query("FROM Attachment attachment WHERE attachment.id.infty = :infty")
	Page<Attachment> findAllByInfotype(Pageable pageRequest, @Param("infty") String infty);
}