package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.IT2002;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.repository.IT2002Repository;
import com.abminvestama.hcms.core.service.api.business.query.IT2002QueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("it2002QueryService")
@Transactional(readOnly = true)
public class IT2002QueryServiceImpl implements IT2002QueryService {

	private IT2002Repository it2002Repository;
	
	@Autowired
	IT2002QueryServiceImpl(IT2002Repository it2002Repository) {
		this.it2002Repository = it2002Repository;
	}
	
	@Override
	public Optional<IT2002> findById(Optional<ITCompositeKeys> id) throws Exception {
		throw new NoSuchMethodException("Method not implemented. Please use method findOneByCompositeKeys instead.");
	}

	@Override
	public Optional<Collection<IT2002>> fetchAll() {
		Optional<Iterable<IT2002>> it2002Iterates = Optional.ofNullable(it2002Repository.findAll());
		if (!it2002Iterates.isPresent()) {
			return Optional.empty();
		}
		
		List<IT2002> listOfIT2002 = new ArrayList<>();
		it2002Iterates.map(it2002Iterable -> {			
			it2002Iterable.forEach(it2002 -> {
				listOfIT2002.add(it2002);
			});
			return listOfIT2002;
		});
		
		return Optional.of(listOfIT2002);
	}

	@Override
	public Optional<IT2002> findOneByCompositeKey(Long pernr, String subty, Date endda, Date begda) {
		if (pernr == null || StringUtils.isBlank(subty) || endda == null || begda == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(it2002Repository.findOneByCompositeKey(pernr, SAPInfoType.ATTENDANCES.infoType(), subty, endda, begda));
	}

	@Override
	public Collection<IT2002> findByPernr(Long pernr) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT2002>> bunchOfIT2002
			= Optional.ofNullable(it2002Repository.findByPernr(pernr, SAPInfoType.ATTENDANCES.infoType()));
		
		return (bunchOfIT2002.isPresent()
				? bunchOfIT2002.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

	@Override
	public Collection<IT2002> findByPernrAndSubty(Long pernr, String subty) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		if (StringUtils.isBlank(subty)) {
			subty = "0005"; // default to subtype '0001' (i.e. Direct Attendance)
		}
		
		Optional<Collection<IT2002>> bunchOfIT2002
			= Optional.ofNullable(it2002Repository.findByPernrAndSubty(pernr, SAPInfoType.ATTENDANCES.infoType(), subty));

		return (bunchOfIT2002.isPresent() 
					? bunchOfIT2002.get().stream().collect(Collectors.toList())
					: Collections.emptyList());		
	}
}