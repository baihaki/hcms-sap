package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0006;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0006SoapObject {

	private String employeenumber;
	private String validitybegin;
	private String validityend;
	
	private String subty;
	private String anssa;
	private String name2;
	private String stras;
	private String ort01;
	private String ort02;
	private String pstlz;
	private String land1;
	private String telnr;
	private String entkm;
	private String locat;
	private String state;
	private String entk2;
	private String com01;
	private String num01;
	
	private IT0006SoapObject() {}
	
	public IT0006SoapObject(IT0006 object) {
		if (object == null) {
			new IT0006SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			subty = object.getId().getSubty();
			anssa = object.getAnssa();
			name2 = object.getName2();
			stras = object.getStras();
			ort01 = object.getOrt01();
			ort02 = object.getOrt02();
			pstlz = object.getPstlz();
			land1 = object.getT005t() != null ? object.getT005t().getLand1() : null;
			telnr = object.getTelnr();
			entkm = object.getEntkm() != null && object.getEntkm() > 0 ? String.valueOf(object.getEntkm().intValue()) : null;
			locat = object.getLocat();
			state = object.getT005u() != null ? object.getT005u().getBland() : null;
			entk2 = object.getEntk2() != null && object.getEntk2() > 0 ? String.valueOf(object.getEntk2().intValue()) : null;
			com01 = object.getCom01();
			num01 = object.getNum01();
			
			
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}

	public String getSubty() {
		return subty;
	}

	public String getAnssa() {
		return anssa;
	}

	public String getName2() {
		return name2;
	}

	public String getStras() {
		return stras;
	}

	public String getOrt01() {
		return ort01;
	}

	public String getOrt02() {
		return ort02;
	}

	public String getPstlz() {
		return pstlz;
	}

	public String getLand1() {
		return land1;
	}

	public String getTelnr() {
		return telnr;
	}

	public String getEntkm() {
		return entkm;
	}

	public String getLocat() {
		return locat;
	}

	public String getState() {
		return state;
	}

	public String getEntk2() {
		return entk2;
	}

	public String getCom01() {
		return com01;
	}

	public String getNum01() {
		return num01;
	}
	
}
