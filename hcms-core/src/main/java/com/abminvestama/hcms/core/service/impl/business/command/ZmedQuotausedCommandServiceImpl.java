package com.abminvestama.hcms.core.service.impl.business.command;


import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.ZmedQuotausedRepository;
import com.abminvestama.hcms.core.service.api.business.command.ZmedQuotausedCommandService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add UpdateThread, threadUpdate method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("ZmedQuotausedCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ZmedQuotausedCommandServiceImpl implements ZmedQuotausedCommandService {
	
	private ZmedQuotausedRepository zmedQuotausedRepository;
    
    class UpdateThread implements Runnable {
    	
    	List<ZmedQuotaused> listZmedQuotaused;
    	
    	UpdateThread(List<ZmedQuotaused> listZmedQuotaused) {
    		this.listZmedQuotaused = listZmedQuotaused;
    	}
    	
		@Override
		public void run() {
			try {
				Thread.sleep(1000);
				for (ZmedQuotaused zmedQuotaused : listZmedQuotaused) {
					try {
				        zmedQuotaused = zmedQuotausedRepository.save(zmedQuotaused);
				        System.out.println("UpdateThread succesfully saved -> gjahr=" + zmedQuotaused.getId().getGjahr() + " bukrs=" + zmedQuotaused.getId().getBukrs() +
				    		" pernr=" + zmedQuotaused.getId().getPernr() + " kodequo=" + zmedQuotaused.getId().getKodequo() + " quotamt=" + zmedQuotaused.getQuotamt());
					} catch (Exception e) {
				        System.out.println("UpdateThread Exception on save -> [gjahr=" + zmedQuotaused.getId().getGjahr() + " bukrs=" + zmedQuotaused.getId().getBukrs() +
				    		" pernr=" + zmedQuotaused.getId().getPernr() + " kodequo=" + zmedQuotaused.getId().getKodequo() + " quotamt=" + zmedQuotaused.getQuotamt());
					}
				}
			} catch (Exception e) {
	        	System.out.println("UpdateThread Exception:" + e.getMessage());
			}
		}
		
    }
		
		@Autowired
		ZmedQuotausedCommandServiceImpl(ZmedQuotausedRepository zmedQuotausedRepository) {
			this.zmedQuotausedRepository = zmedQuotausedRepository;
		}
		
		@Override
		public Optional<ZmedQuotaused> save(ZmedQuotaused entity, User user)
				throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
			//if (user != null) {
				//entity.setUname(user.getId());
			//}
			
			try {
				return Optional.ofNullable(zmedQuotausedRepository.save(entity));
			} catch (Exception e) {
				throw new CannotPersistException("Cannot persist ZmedQuotaused (Personal Information).Reason=" + e.getMessage());
			}				
		}

		@Override
		public boolean delete(ZmedQuotaused entity, User user) throws NoSuchMethodException, ConstraintViolationException {
			// no delete at this version.
			throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
		}

		@Override
		public boolean restore(ZmedQuotaused entity, User user) throws NoSuchMethodException, ConstraintViolationException {
			// since we don't have delete operation, then we don't need the 'restore' as well
			throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
		}
		
		@Override
		public void threadUpdate(List<ZmedQuotaused> listZmedQuotaused) {
			new Thread(new UpdateThread(listZmedQuotaused)).start();
		}
	}