package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.PtrvHeadKey;
import com.abminvestama.hcms.core.model.entity.PtrvHead;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.1.0
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.1.0</td><td>Baihaki</td><td>Add methods: findByPernrAndProcessesAndDatv1</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Add parameter "notStatus" to filter by paymentStatus or not (optional)</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Add superiorPosition parameter on methods: findByBukrsAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add forms parameter</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessesWithPaging, findByPernrAndProcessesWithPaging</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add methods: findByPernrAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add methods: findByBukrsAndProcessWithPaging</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface PtrvHeadQueryService extends DatabaseQueryService<PtrvHead, PtrvHeadKey>, DatabasePaginationQueryService{

	Optional<PtrvHead> findOneByCompositeKey(Integer hdvrs, Long pernr, Long reinr);
	/*
	@NotNull
	Collection<PtrvHead> findByKodemed(String bukrs, String kodemed, Date datbi);
	*/

	Page<PtrvHead> findByBukrsAndProcessWithPaging(String bukrs, String process, short[] forms, String notStatus, String superiorPosition, int pageNumber);

	Page<PtrvHead> findByPernrAndProcessWithPaging(long pernr, String process, short[] forms, String notStatus, int pageNumber);

	Page<PtrvHead> findByBukrsAndProcessesWithPaging(String bukrs, String[] processes, short[] forms, String notStatus, int pageNumber);

	Page<PtrvHead> findByPernrAndProcessesWithPaging(long pernr, String[] processes, short[] forms, String notStatus, int pageNumber);

	Optional<Collection<PtrvHead>> findByPernrAndProcessesAndDatv1(Long pernr, String[] processes, Date datv1);
}

