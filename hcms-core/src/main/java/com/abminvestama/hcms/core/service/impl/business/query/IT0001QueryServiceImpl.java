package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.repository.IT0001Repository;
import com.abminvestama.hcms.core.service.api.business.query.IT0001QueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add methods: countByPostAndBukrs, countByJobAndBukrs, countByEmpAndBukrs</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("it0001QueryService")
@Transactional(readOnly = true)
public class IT0001QueryServiceImpl implements IT0001QueryService {
	
	private IT0001Repository it0001Repository;
	
	@Autowired
	IT0001QueryServiceImpl(IT0001Repository it0001Repository) {
		this.it0001Repository = it0001Repository;
	}

	@Override
	public Optional<IT0001> findById(Optional<ITCompositeKeysNoSubtype> id) throws Exception {
		return id.map(pk -> it0001Repository.findOneByCompositeKey(pk.getPernr(), pk.getEndda(), pk.getBegda()));
	}

	@Override
	public Optional<Collection<IT0001>> fetchAll() {
		Optional<Iterable<IT0001>> it0001Iterates = Optional.ofNullable(it0001Repository.findAll());
		
		List<IT0001> listOfIT0001 = new ArrayList<>();
		
		return it0001Iterates.map(it0001Iterable -> {			
			it0001Iterable.forEach(it0001 -> {
				listOfIT0001.add(it0001);
			});
			return listOfIT0001;
		});		
	}

	@Override
	public Optional<IT0001> findOneByCompositeKey(Long pernr, Date endda, Date begda) {
		if (pernr == null || endda == null || begda == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(it0001Repository.findOneByCompositeKey(pernr, endda, begda));		
	}

	@Override
	public Collection<IT0001> findByPernr(Long pernr) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT0001>> bunchOfIT0001 
			= Optional.ofNullable(it0001Repository.findByPernr(pernr));
		
		return (bunchOfIT0001.isPresent()
				? bunchOfIT0001.get().stream().collect(Collectors.toList())
				: Collections.emptyList());				
	}

	@Override
	public Collection<IT0001> findByPositionId(String positionId) {
		if (StringUtils.isBlank(positionId)) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT0001>> bunchOfIT0001 = Optional.ofNullable(it0001Repository.findByPositionId(positionId));
		
		return (bunchOfIT0001.isPresent() ? bunchOfIT0001.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

	@Override
	public Long countByPostAndBukrs(String persa, String bukrs) {
		if(bukrs == null){
			bukrs = "9900";
		}
		Long answer;
		if(persa == null){
			answer = it0001Repository.countByPostAndBukrs( bukrs);
		}else{
			answer = it0001Repository.countByPostAndBukrs(persa, bukrs);
		}
		return answer;
	}
	
	
	@Override
	public Long countByJobAndBukrs(String stell, String bukrs) {
		if(bukrs == null){
			bukrs = "9900";
		}
		Long answer;
		if(stell == null){
			answer = it0001Repository.countByPostAndBukrs( bukrs);
		}else{
			answer = it0001Repository.countByJobAndBukrs(stell, bukrs);
		}
		return answer;
	}
	
	
	
	@Override
	public Long countByEmpAndBukrs(String persg, String bukrs) {
		if(bukrs == null){
			bukrs = "9900";
		}
		Long answer;
		if(persg == null){
			answer = it0001Repository.countByPostAndBukrs( bukrs);
		}else{
			answer = it0001Repository.countByEmpAndBukrs(persg, bukrs);
		}
		return answer;
	}
	
}