package com.abminvestama.hcms.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.abminvestama.hcms.core.model.entity.T519T;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface T519TRepository extends CrudRepository<T519T, String> {
	
	Page<T519T> findAll(Pageable pageRequest);
}