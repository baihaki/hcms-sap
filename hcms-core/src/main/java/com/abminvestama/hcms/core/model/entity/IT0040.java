package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;

/**
 * 
 * Class that represents master data transaction for <strong>Employee Inventory</strong> (i.e. IT0040 in SAP)
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add text1 and status (DocumentStatus) field</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Entity
@Table(name = "it0040")
public class IT0040 extends SAPAbstractEntity<ITCompositeKeys> {

	private static final long serialVersionUID = -675297254875896766L;
	
	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "infty", referencedColumnName = "infty", insertable = false, updatable = false),
		@JoinColumn(name = "subty", referencedColumnName = "subty", insertable = false, updatable = false)
	})
	private T591S subty;
	
	public IT0040() {}
	
	public IT0040(ITCompositeKeys id) {
		this();
		this.id = id;
	}
	
	@Column(name = "seqnr")
	private Long seqnr;
	
	@Column(name = "itxex")
	private String itxex;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "infty", referencedColumnName = "infty", insertable = false, updatable = false),
		@JoinColumn(name = "leihg", referencedColumnName = "subty", insertable = false, updatable = false)
	})
	private T591S leihg;
	
	@Column(name = "anzkl")
	private Double anzkl;
	
	@ManyToOne
	@JoinColumn(name = "zeinh", referencedColumnName = "zeinh")
	private T538T zeinh;
	
	@Column(name = "lobnr")
	private String lobnr;
	
	@Column(name = "document_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private DocumentStatus status;
	
	/**
	 * GET Employee SSN/ID.
	 * 
	 * @return
	 */
	public Long getPernr() {
		return pernr;
	}
	
	public void setPernr(Long pernr) {
		this.pernr = pernr;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	public T591S getSubty() {
		return subty;
	}
	
	public void setSubty(T591S subty) {
		this.subty = subty;
	}
	
	/**
	 * GET Infotype Record No.
	 * 
	 * @return
	 */
	public Long getSeqnr() {
		return seqnr;
	}
	
	public void setSeqnr(Long seqnr) {
		this.seqnr = seqnr;
	}
	
	/**
	 * GET Text Exists.
	 * 
	 * @return
	 */
	public String getItxex() {
		return itxex;
	}
	
	public void setItxex(String itxex) {
		this.itxex = itxex;
	}
	
	/**
	 * GET Object on Loan.
	 * 
	 * @return
	 */
	public T591S getLeihg() {
		return leihg;
	}
	
	public void setLeihg(T591S leihg) {
		this.leihg = leihg;
	}
	
	/**
	 * Get Number /unit.
	 * 
	 * @return
	 */
	public Double getAnzkl() {
		return anzkl;
	}
	
	public void setAnzkl(Double anzkl) {
		this.anzkl = anzkl;
	}
	
	/**
	 * GET Unit of time/meas.
	 * 
	 * @return
	 */
	public T538T getZeinh() {
		return zeinh;
	}
	
	public void setZeinh(T538T zeinh) {
		this.zeinh = zeinh;
	}
	
	/**
	 * GET Loan Object Number.
	 * 
	 * @return
	 */
	public String getLobnr() {
		return lobnr;
	}
	
	public void setLobnr(String lobnr) {
		this.lobnr = lobnr;
	}
	
	/**
	 * GET Document Status.
	 * 
	 * @return
	 */
	public DocumentStatus getStatus() {
		return status;
	}
	
	public void setStatus(DocumentStatus status) {
		this.status = status;
	}
}