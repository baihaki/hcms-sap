package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.abminvestama.hcms.core.model.entity.IT0077;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.PtrvPerioKey;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyCostype;
import com.abminvestama.hcms.core.model.entity.PtrvPerio;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;


public interface PtrvPerioQueryService extends DatabaseQueryService<PtrvPerio, PtrvPerioKey>, DatabasePaginationQueryService{
	//Optional<PtrvPerio> findByEntityName(final String entityName);
	
	
/*
	@NotNull
	Optional<PtrvPerio> findOneByCompositeKey(String bukrs, String kodemed, String kodecos, Date datab, Date datbi);

	@NotNull
	Collection<PtrvPerio> findByKodemed(String bukrs, String kodemed, Date datbi);
	*/
}

