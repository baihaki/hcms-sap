package com.abminvestama.hcms.core.service.api.business.query;


import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;


public interface ZmedEmpquotasmQueryService extends DatabaseQueryService<ZmedEmpquotasm, ZmedCompositeKeyEmpquotasm>, DatabasePaginationQueryService{
	//Optional<ZmedEmpquotasm> findByEntityName(final String entityName);
	
	//Optional<Collection<ZmedEmpquotasm>> findByEntityName(final Long entityName);
	

	@NotNull
	Optional<ZmedEmpquotasm> findOneByCompositeKey(long pernr, String bukrs, String persg, String persk, String fatxt, String kodequo, Date datab, Date datbi);

	@NotNull
	List<ZmedEmpquotasm> findQuotas(Long pernr, String bukrs, String persg,
			String persk, Date datab, Date datbi);
	
}

