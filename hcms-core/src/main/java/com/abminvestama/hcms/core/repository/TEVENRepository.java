package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.TEVEN;
import com.abminvestama.hcms.core.model.entity.TEVENKey;

public interface TEVENRepository extends CrudRepository<TEVEN, TEVENKey> {

	@Query("FROM TEVEN teven WHERE teven.pdsnr = :pdsnr ")
	TEVEN findOneByCompositeKey(@Param("pdsnr") Long pdsnr);

	@Query(nativeQuery=true, value="SELECT * FROM TEVEN teven WHERE teven.pernr = :pernr AND teven.ldate = :ldate AND teven.satza = :satza ")
	TEVEN findOneByCompositeKeys(@Param("pernr") Long pernr, 
			@Param("ldate") @Temporal(TemporalType.DATE) Date ldate,
			@Param("satza") String satza);

	@Query("FROM TEVEN teven WHERE teven.pernr = :pernr ")
	Page<TEVEN> findByPernr(@Param("pernr") Long pernr, Pageable pageRequest);
	
	

	@Query("FROM TEVEN teven WHERE teven.pernr = :pernr AND teven.ldate >= :endDate AND teven.ldate <= :startDate")
	Page<TEVEN> findByPernr(@Param("pernr") Long pernr,
			@Param("endDate") @Temporal(TemporalType.DATE) Date endDate,
			@Param("startDate") @Temporal(TemporalType.DATE) Date startDate,
			Pageable pageRequest);
				
}