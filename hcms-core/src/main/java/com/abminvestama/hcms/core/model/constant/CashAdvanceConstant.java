package com.abminvestama.hcms.core.model.constant;

/**
 * 
 * @author wijanarko (wijanarko777@gmx.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class CashAdvanceConstant {
	public static final String TABEL_EMPLOYEE_CSKT_POOL = "ca_emp_cskt";
	public static final String TABEL_CASHADVANCE_SETTLEMENT_HEADER = "ca_stl_hdr";
	public static final String TABEL_CASHADVANCE_SETTLEMENT_DETAIL = "ca_stl_dtl";
	
	public static final String MESSAGE_INFO = "[1]";
	public static final String MESSAGE_ERROR = "[2]";
	public static final String MESSAGE_SAP = "[3]";
	
	public static final String APPROVAL_BY_HEAD = "HEAD";
	public static final String APPROVAL_BY_DIRECTOR = "DIRECTOR";
	public static final String APPROVAL_BY_GA = "GA";
	public static final String APPROVAL_BY_HR = "HR";
	public static final String APPROVAL_BY_POOLING = "POOLING";
	public static final String APPROVAL_BY_ACCOUNTING = "ACCOUNTING";
	public static final String APPROVAL_BY_TREASURY = "TREASURY";
	
	public static final String MAIL_TO = "MAIL_TO";
	public static final String MAIL_SUBJECT = "MAIL_SUBJECT";
	public static final String MAIL_BODY = "MAIL_BODY";
	
	public static final String PAYMENT_STATUS_IS_PAID = "P";
	public static final String PAYMENT_STATUS_IS_WAITING = "N";
	
	public static final String SETTLEMENT_CAS_STATUS_BL = "Balance";
	public static final String SETTLEMENT_CAS_STATUS_OP = "Overpay";
	public static final String SETTLEMENT_CAS_STATUS_UP = "Underpay";
	public static final String SETTLEMENT_APPROVAL_NOTE_TEMPLATE = 
			"APPROVAL_CREATED_AT " +
			"by APPROVAL_NAME (STATUS_APPROVAL):" + System.lineSeparator() + 
			"APPROVAL_MSG" + System.lineSeparator();
	
	public static final String TRANSACTION_CODE_SUBMITED = "S";
	public static final String TRANSACTION_CODE_REJECTED = "R";
	
	public static final String TRANSACTION_APPROVED = "APPROVED";
	public static final String TRANSACTION_REJECTED = "REJECTED";
	public static final String TRANSACTION_SUBMITED = "SUBMITED";
	public static final String TRANSACTION_SETTLED = "SETTLED";
	
	public static final Double PETTY_CASH_LIMIT = 1000000.00;
	public static final Double APPROVAL_DIRECTOR_LIMIT = 10000000.00;
	
	public static final String CSKT_CONTROLLING_AREA_USED = "1000";
	public static final Integer EMP_CSKT_ROOT_LEVEL = new Integer(1);
	public static final String EMP_CSKT_POOLING_CODE_TOP_DOWN = "TD";
	public static final String EMP_CSKT_POOLING_CODE_BOTTOM_UP = "BU";
	
	public static final String CURRENCY_IDR = "IDR";
}
