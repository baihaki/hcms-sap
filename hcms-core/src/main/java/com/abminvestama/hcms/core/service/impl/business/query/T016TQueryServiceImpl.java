package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T016T;
import com.abminvestama.hcms.core.repository.T016TRepository;
import com.abminvestama.hcms.core.service.api.business.query.T016TQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add fetchAllWithPaging method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("t016tQueryService")
@Transactional(readOnly = true)
public class T016TQueryServiceImpl implements T016TQueryService {

	private T016TRepository t016tRepository;
	
	@Autowired
	public T016TQueryServiceImpl(T016TRepository t016tRepository) {
		this.t016tRepository = t016tRepository;
	}
	
	@Override
	public Optional<T016T> findById(Optional<String> id) throws Exception {
		return id.map(pk -> t016tRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<T016T>> fetchAll() {
		List<T016T> listOfT016T = new ArrayList<>();
		Optional<Iterable<T016T>> bunchOfT016T = Optional.ofNullable(t016tRepository.findAll());
		return bunchOfT016T.map(iter -> {
			iter.forEach(t016t -> {
				listOfT016T.add(t016t);
			});
			return listOfT016T;
		});
	}

	@Override
	public Page<T016T> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return t016tRepository.findAllByOrderByBrtxtAsc(pageRequest);
	}
}