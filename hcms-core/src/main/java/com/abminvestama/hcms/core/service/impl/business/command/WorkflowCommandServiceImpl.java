package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Date;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.model.entity.Workflow;
import com.abminvestama.hcms.core.repository.WorkflowRepository;
import com.abminvestama.hcms.core.service.api.business.command.WorkflowCommandService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("workflowCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class WorkflowCommandServiceImpl implements WorkflowCommandService {

	private WorkflowRepository workflowRepository;
	
	@Autowired
	WorkflowCommandServiceImpl(WorkflowRepository workflowRepository) {
		this.workflowRepository = workflowRepository;
	}
	
	@Override
	public Optional<Workflow> save(Workflow entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (entity.isNew()) {
			entity.setCreatedById(user.getId());
		}
		
		entity.setUpdatedById(user.getId());
		
		try {
			return Optional.ofNullable(workflowRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist WorkflowType.Reason=" + e.getMessage());
		}		
	}

	@Override
	public boolean delete(Workflow entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		entity.setDeletedById(user.getId());
		entity.setDeletedAt(new Date());
		
		return Optional.ofNullable(workflowRepository.save(entity)).isPresent() ? true : false;
	}

	@Override
	public boolean restore(Workflow entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		entity.setDeletedAt(null);
		entity.setDeletedById(null);
		entity.setUpdatedById(user.getId());
		
		return Optional.ofNullable(workflowRepository.save(entity)).isPresent() ? true : false;
	}
}