package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.IT2001;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.IT2001Repository;
import com.abminvestama.hcms.core.service.api.business.command.IT2001CommandService;

/**
 * 
 * @since 1.0.0
 * @version 1.1.0
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.1.0</td><td>Baihaki</td><td>Changed method save from save() to saveAndFlush() to catch any database exception immediately</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("it2001CommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class IT2001CommandServiceImpl implements IT2001CommandService {
	
	private IT2001Repository it2001Repository;
	
	@Autowired
	IT2001CommandServiceImpl(IT2001Repository it2001Repository) {
		this.it2001Repository = it2001Repository;
	}

	@Override
	public Optional<IT2001> save(IT2001 entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setUname(user.getId());
		}
		
		//try {
			return Optional.ofNullable(it2001Repository.saveAndFlush(entity));
		//} catch (Exception e) {
			//throw new CannotPersistException("Cannot persist IT2001 (Absences).Reason=" + e.getMessage());
		//}		
	}

	@Override
	public boolean delete(IT2001 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(IT2001 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");		
	}
}