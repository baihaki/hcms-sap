package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Immutable;

/***
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Embeddable
@Immutable
public class T505SKey implements Serializable {

	private static final long serialVersionUID = -7157359314241200325L;
	
	@Column(name = "molga", nullable = false, length = 5)
	private String molga;
	
	@Column(name = "racky", nullable = false, length = 5)
	private String racky;
	
	public T505SKey() {}
	
	public T505SKey(String molga, String racky) {
		this.molga = molga;
		this.racky = racky;
	}
	
	public String getMolga() {
		return molga;
	}
	
	public String getRacky() {
		return racky;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final T505SKey key = (T505SKey) o;
		
		if (key.getMolga() != null ? !key.getMolga().equalsIgnoreCase(molga) : molga != null) {
			return false;
		}
		
		if (key.getRacky() != null ? !key.getRacky().equalsIgnoreCase(racky) : racky != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.molga != null ? this.molga.hashCode() : 0;
		result = result * 31 + (this.racky != null ? this.racky.hashCode() : 0);
		return result;
	}
}