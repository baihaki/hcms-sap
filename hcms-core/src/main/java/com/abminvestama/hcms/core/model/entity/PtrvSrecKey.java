package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;



@Embeddable
@Immutable
public class PtrvSrecKey implements Serializable {
	private static final long serialVersionUID = 5385850040856914239L;

	private String mandt;
	private Long pernr;
	private Long reinr;
	private Integer perio;
	private String receiptno;
		
	public String getMandt() {
		return mandt;
	}

	public Long getPernr() {
		return pernr;
	}

	public Long getReinr() {
		return reinr;
	}

	public Integer getPerio() {
		return perio;
	}

	public String getReceiptno() {
		return receiptno;
	}

	public PtrvSrecKey() {}
	
	public PtrvSrecKey(String mandt, Long pernr, Long reinr, Integer perio, String receiptno) {
		this.mandt = mandt;
		this.pernr = pernr;
		this.reinr = reinr;
		this.perio = perio;
		this.receiptno = receiptno;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final PtrvSrecKey key = (PtrvSrecKey) o;
		
		if (key.getMandt() != null ? key.getMandt() != (mandt != null ? mandt: "") : mandt != null) {
			return false;
		}
		
		if (key.getPernr() != null ? key.getPernr() != (pernr != null ? pernr: "") : pernr != null) {
			return false;
		}
		
		if (key.getReinr() != null ? key.getReinr() != (reinr != null ? reinr: "") : reinr != null) {
			return false;
		}
		
		if (key.getPerio() != null ? key.getPerio() != (perio != null ? perio: "") : perio != null) {
			return false;
		}
		
		if (key.getReceiptno() != null ? key.getReceiptno() != (receiptno != null ? receiptno: "") : receiptno != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.mandt != null ? this.mandt.hashCode() : 0;
		result = result * 31 + (this.pernr != null ? this.pernr.hashCode() : 0);
		result = result * 31 + (this.reinr != null ? this.reinr.hashCode() : 0);
		result = result * 31 + (this.perio != null ? this.perio.hashCode() : 0);
		result = result * 31 + (this.receiptno != null ? this.receiptno.hashCode() : 0);
		return result;
	}
}
