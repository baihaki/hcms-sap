package com.abminvestama.hcms.core.service.helper;

import java.util.Map;
import java.util.Optional;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.BNKA;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.T005T;
import com.abminvestama.hcms.core.model.entity.T042Z;
import com.abminvestama.hcms.core.model.entity.T591S;
import com.abminvestama.hcms.core.model.entity.T591SKey;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 3.0.0
 * @since 3.0.0
 *
 */
public class IT0009Mapper {
	
	private IT0009 it0009;
	
	private IT0009Mapper() {}
	
	public IT0009Mapper(Map<String, String> map, CommonServiceFactory commonServiceFactory) {
		if (map == null) {
			new IT0009Mapper();
			it0009 = new IT0009();
		} else {
			ITCompositeKeys key = new ITCompositeKeys(Long.valueOf(map.get("field1")), SAPInfoType.BANK_DETAILS.infoType(), map.get("field2"),
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("field3")),
					CommonDateFunction.convertDateRequestParameterIntoDate(map.get("field4")));
			it0009 = new IT0009(key);
			it0009.setSeqnr(map.get("field5") != null ? Long.valueOf(map.get("field5")) : 0);
			it0009.setBetrg(map.get("field8") != null ? Double.valueOf(map.get("field8")) : 0);
			it0009.setWaers(map.get("field9"));
			it0009.setAnzhl(map.get("field10") != null ? Double.valueOf(map.get("field10")) : 0);
			Optional<T591SKey> bnksaKey = Optional.ofNullable(new T591SKey(SAPInfoType.BANK_DETAILS.infoType(), map.get("field11")));
			Optional<T591S> bnksaObject;
			try {
				bnksaObject = commonServiceFactory.getT591SQueryService().findById(bnksaKey);
			} catch (Exception e) {
				bnksaObject = Optional.empty();
			}
			it0009.setBnksa(bnksaObject.isPresent() ? bnksaObject.get() : null);
			Optional<T042Z> zlschObject;
			try {
				zlschObject = commonServiceFactory.getT042ZQueryService().findById(Optional.ofNullable(map.get("field12")));
			} catch (Exception e1) {
				zlschObject = Optional.empty();
			}
			it0009.setZlsch(zlschObject.isPresent() ? zlschObject.get() : null);
			it0009.setEmftx(map.get("field13"));
			it0009.setBkplz(map.get("field14"));
			it0009.setBkort(map.get("field15"));
			Optional<T005T> banksObject;
			try {
				banksObject = commonServiceFactory.getT005TQueryService().findById(Optional.ofNullable(map.get("field16")));
			} catch (Exception e1) {
				banksObject = Optional.empty();
			}
			it0009.setBanks(banksObject.isPresent() ? banksObject.get() : null);
			Optional<BNKA> banklObject;
			try {
				banklObject = commonServiceFactory.getBNKAQueryService().findById(Optional.ofNullable(map.get("field17")));
			} catch (Exception e1) {
				banklObject = Optional.empty();
			}
			it0009.setBankl(banklObject.isPresent() ? banklObject.get() : null);
			it0009.setBankn(map.get("field18"));
			it0009.setZweck(map.get("field19"));
			it0009.setStatus(DocumentStatus.PUBLISHED);
		}		
	}

	public IT0009 getIT0009() {
		return it0009;
	}
	
}
