package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;



@Embeddable
@Immutable
public class ZmedCompositeKeyQuotaused implements Serializable {
	private static final long serialVersionUID = 5385850040856914239L;

	private String bukrs;
	private Integer gjahr;
	private String kodequo;
	private Long pernr;
	
	public ZmedCompositeKeyQuotaused() {}
	
	public ZmedCompositeKeyQuotaused(Integer gjahr, String bukrs, Long pernr, String kodequo) {
		this.bukrs = bukrs;
		this.gjahr = gjahr;
		this.kodequo = kodequo;
		this.pernr = pernr;
	}
	
	public String getBukrs() {
		return bukrs;
	}

	public Integer getGjahr() {
		return gjahr;
	}

	public String getKodequo() {
		return kodequo;
	}

	public Long getPernr() {
		return pernr;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final ZmedCompositeKeyQuotaused key = (ZmedCompositeKeyQuotaused) o;
		
		if (key.getBukrs() != null ? key.getBukrs() != (bukrs != null ? bukrs: "") : bukrs != null) {
			return false;
		}
		
		if (key.getKodequo() != null ? key.getKodequo() != (kodequo != null ? kodequo: "") : kodequo != null) {
			return false;
		}
		
		if (key.getGjahr() != 0 ? key.getGjahr() != (gjahr != 0 ? gjahr: 0) : gjahr != 0) {
			return false;
		}
		
		if (key.getPernr() != 0 ? key.getPernr() != (pernr != 0 ? pernr: 0) : pernr != 0) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.bukrs != null ? this.bukrs.hashCode() : 0;
		result = result * 31 + (this.kodequo != null ? this.kodequo.hashCode() : 0);
		result = result * 31 + (this.gjahr != 0 ? this.gjahr.toString().hashCode() : 0);
		result = result * 31 + (this.pernr != 0 ? this.pernr.toString().hashCode() : 0);
		return result;
	}
}
