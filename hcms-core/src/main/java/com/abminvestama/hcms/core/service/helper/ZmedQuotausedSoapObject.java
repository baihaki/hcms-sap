package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class ZmedQuotausedSoapObject {

	private String gjahr;
	private String bukrs;
	private String pernr;
	private String kodequo;
	private String quotamt;
	
	private ZmedQuotausedSoapObject() {}
	
	public ZmedQuotausedSoapObject(ZmedQuotaused object) {
		if (object == null) {
			new ZmedQuotausedSoapObject();
		} else {
			gjahr = object.getId().getGjahr() != null ? object.getId().getGjahr().toString() : null;
			bukrs = object.getId().getBukrs();
			pernr = object.getId().getPernr() != null ? object.getId().getPernr().toString() : null;
			kodequo = object.getId().getKodequo();
		}		
	}

	public String getGjahr() {
		return gjahr;
	}

	public String getBukrs() {
		return bukrs;
	}

	public String getPernr() {
		return pernr;
	}

	public String getKodequo() {
		return kodequo;
	}

	public String getQuotamt() {
		return quotamt;
	}
	
}
