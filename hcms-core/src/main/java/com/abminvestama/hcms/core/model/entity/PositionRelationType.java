package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Entity
@Table(name = "m_position_relation_types")
public class PositionRelationType extends AbstractEntity {

	private static final long serialVersionUID = 2541257339421581563L;

	@Column(name = "name", nullable = false, unique = true)
	private String name;
	
	@Column(name = "description", nullable = true)
	private String description;
	
	@Column(name = "system", nullable = true)
	private boolean system;
	
	public PositionRelationType() {}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean isSystem() {
		return system;
	}
	
	public void setSystem(boolean system) {
		this.system = system;
	}
}