package com.abminvestama.hcms.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.T705P;
import com.abminvestama.hcms.core.model.entity.T705PKey;

public interface T705PRepository extends CrudRepository<T705P, T705PKey> {


	@Query("FROM T705P t705p WHERE t705p.satza = :satza ")
	T705P findBySatza(@Param("satza") String satza);

	@Query("FROM T705P t705P ORDER BY pmbde, satza ")
	Page<T705P> findAllT705P(Pageable pageRequest);
}