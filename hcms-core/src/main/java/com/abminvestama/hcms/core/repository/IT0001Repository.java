package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add methods: countByPostAndBukrs, countByJobAndBukrs, countByEmpAndBukrs</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0001Repository extends CrudRepository<IT0001, ITCompositeKeysNoSubtype> {

	@Query("FROM IT0001 it0001 WHERE it0001.pernr = :pernr AND it0001.id.endda = :endda AND it0001.id.begda = :begda")
	IT0001 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM IT0001 it0001 WHERE it0001.pernr = :pernr ORDER BY it0001.id.endda DESC, it0001.id.begda DESC")
	Collection<IT0001> findByPernr(@Param("pernr") Long pernr);			
	
	@Query("FROM IT0001 it0001 WHERE it0001.position.id = :positionId ORDER BY it0001.id.endda DESC, it0001.id.begda DESC")
	Collection<IT0001> findByPositionId(@Param("positionId") String positionId);

	@Query(nativeQuery=true, value="SELECT count(*) FROM it0001 WHERE bukrs = :bukrs")
	Long countByPostAndBukrs(@Param("bukrs") String bukrs);

	@Query(nativeQuery=true, value="SELECT count(*) FROM it0001 WHERE bukrs = :bukrs AND persa = :persa")
	Long countByPostAndBukrs(@Param("persa") String persa, 
							@Param("bukrs") String bukrs);

	@Query(nativeQuery=true, value="SELECT count(*) FROM it0001 WHERE bukrs = :bukrs AND stell = :stell")
	Long countByJobAndBukrs(@Param("stell") String stell, 
							@Param("bukrs") String bukrs);


	@Query(nativeQuery=true, value="SELECT count(*) FROM it0001 WHERE bukrs = :bukrs AND persg = :persg")
	Long countByEmpAndBukrs(@Param("persg") String persg, 
							@Param("bukrs") String bukrs);
}