package com.abminvestama.hcms.core.model.constant;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public enum ApprovalType {

	ALL_SUPERIOR, ONE_SUPERIOR;
}
