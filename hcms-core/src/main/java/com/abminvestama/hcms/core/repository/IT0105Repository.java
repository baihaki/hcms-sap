package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0105;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;

/**
 * 
 * @since 1.0.0
 * @version 1.0.3
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add methods: findByPernrForPublishedOrUpdated</td></tr>
 *     <tr><td>1.0.2</td><td>Anasuya</td><td>Add findByPernrAndStatus method</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add findByStatus method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0105Repository extends CrudRepository<IT0105, ITCompositeKeys> {
	
	@Query("FROM IT0105 it0105 WHERE it0105.pernr = :pernr AND it0105.id.infty = :infty AND it0105.id.subty = :subty AND it0105.id.endda = :endda AND it0105.id.begda = :begda")
	IT0105 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty,
			@Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM IT0105 it0105 WHERE it0105.pernr = :pernr AND it0105.id.infty = :infty ORDER BY it0105.id.subty ASC, it0105.id.endda DESC")
	Collection<IT0105> findByPernr(@Param("pernr") Long pernr, @Param("infty") String infty);
	
	@Query("FROM IT0105 it0105 WHERE it0105.pernr = :pernr AND it0105.id.infty = :infty AND it0105.id.subty = :subty ORDER BY it0105.id.endda DESC")
	Collection<IT0105> findByPernrAndSubty(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty);
	
	@Modifying
	@Query("UPDATE IT0105 it0105 SET it0105.usrty = :usrty, it0105.uname = :uname WHERE it0105.pernr = :pernr AND it0105.id.infty = :infty AND it0105.id.subty = :subty AND it0105.id.endda = :endda AND it0105.id.begda = :begda")
	int updateByUsrty(@Param("usrty") String usrty, @Param("uname") String uname, @Param("pernr") Long pernr, @Param("infty") String infty,
			@Param("subty") String subty, @Param("endda") @Temporal(TemporalType.DATE) Date endda, @Param("begda") @Temporal(TemporalType.DATE) Date begda);




	@Query("FROM IT0105 it0105 WHERE it0105.status = :status ORDER BY it0105.id.endda DESC")
	Page<IT0105> findByStatus(@Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0105 it0105 WHERE it0105.pernr = :pernr AND it0105.status = :status ORDER BY it0105.id.endda DESC, it0105.id.begda DESC")
	Page<IT0105> findByPernrAndStatus(@Param("pernr") Long pernr, @Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0105 it0105 WHERE it0105.pernr = :pernr AND " +
	"(it0105.status = 'PUBLISHED' OR it0105.status = 'UPDATE_IN_PROGRESS' OR it0105.status = 'UPDATE_REJECTED') " +
	" ORDER BY it0105.id.endda DESC, it0105.id.begda DESC")
	Page<IT0105> findByPernrForPublishedOrUpdated(@Param("pernr") Long pernr, Pageable pageRequest);


}