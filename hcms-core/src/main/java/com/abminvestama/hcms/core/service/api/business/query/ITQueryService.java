package com.abminvestama.hcms.core.service.api.business.query;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.SAPAbstractEntity;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Modify method: fetchDistinctByCriteriaWithPaging (add employee)</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add method: fetchDistinctByBukrsWithPaging</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface ITQueryService extends DatabaseQueryService<SAPAbstractEntity<?>, ITCompositeKeys>, DatabasePaginationQueryService {

	Page<?> fetchDistinctByCriteriaWithPaging(int pageNumber, String bukrs, Long pernr, String[] excludeStatus, Object itObject, IT0001 employee);

	Page<?> fetchDistinctByBukrsWithPaging(int pageNumber, String bukrs, String superiorPosition);
}