package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.IT0077;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeysNoSubtype;
import com.abminvestama.hcms.core.model.entity.PtrvPerioKey;
import com.abminvestama.hcms.core.model.entity.PtrvPerio;
import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.abminvestama.hcms.core.repository.PtrvPerioRepository;
import com.abminvestama.hcms.core.service.api.business.query.PtrvPerioQueryService;

@Service("ptrvPerioQueryService")
@Transactional(readOnly = true)
public class PtrvPerioQueryServiceImpl implements PtrvPerioQueryService {

	private PtrvPerioRepository ptrvPerioRepository;
	
	@Autowired
	PtrvPerioQueryServiceImpl(PtrvPerioRepository ptrvPerioRepository) {
		this.ptrvPerioRepository = ptrvPerioRepository;
	}
	
	@Override
	public Optional<Collection<PtrvPerio>> fetchAll() {
		List<PtrvPerio> entities = new ArrayList<>();
		Optional<Iterable<PtrvPerio>> entityIterable = Optional.ofNullable(ptrvPerioRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}
	
	
	
	/*@Override
	public Optional<PtrvPerio> findOneByCompositeKey(String bukrs, String kodemed, String kodecos, Date datab, Date datbi) {
		if (bukrs == null || kodemed == null || kodecos == null || datab == null || datbi == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(ptrvPerioRepository.findOneByCompositeKey(bukrs, kodemed, kodecos, datab, datbi));
	}*/


	@Override
	public Optional<PtrvPerio> findById(Optional<PtrvPerioKey> id)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/*@Override
	public Collection<PtrvPerio> findByKodemed(String bukrs, String kodemed,
			Date datbi) {
		if (bukrs == null || kodemed == null || datbi == null) {
			return Collections.emptyList();
		}
		

		Optional<Collection<PtrvPerio>> bunchOfZmed
			= Optional.ofNullable(ptrvPerioRepository.findByKodemed(bukrs, kodemed, datbi));
		
		return (bunchOfZmed.isPresent()
				? bunchOfZmed.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}*/
		
}
