package com.abminvestama.hcms.core.repository;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.abminvestama.hcms.core.model.entity.T517T;
import com.abminvestama.hcms.core.model.entity.T517A;
import com.abminvestama.hcms.core.model.entity.T517AKey;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface T517ARepository extends CrudRepository<T517A, T517AKey> {
	
	Page<T517A> findAll(Pageable pageRequest);
	Collection<T517A> findBySlart(T517T slart);
}