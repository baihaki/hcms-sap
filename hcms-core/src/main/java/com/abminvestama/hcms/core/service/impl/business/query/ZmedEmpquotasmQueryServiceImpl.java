package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.T001;
import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.repository.T001Repository;
import com.abminvestama.hcms.core.repository.ZmedEmpquotasmRepository;
import com.abminvestama.hcms.core.service.api.business.query.ZmedEmpquotasmQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Fix findQuotas method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("zmedEmpquotasmQueryService")
@Transactional(readOnly = true)
public class ZmedEmpquotasmQueryServiceImpl implements ZmedEmpquotasmQueryService {

	private ZmedEmpquotasmRepository zmedEmpquotasmRepository;
	
	@Autowired
	ZmedEmpquotasmQueryServiceImpl(ZmedEmpquotasmRepository zmedEmpquotasmRepository) {
		this.zmedEmpquotasmRepository = zmedEmpquotasmRepository;
	}
	
	

	private T001Repository t001Repository;
	
	@Autowired
	public void setT001Repository(T001Repository t001Repository) {
		this.t001Repository = t001Repository;
	}
	
	//@Override
	//public Optional<ZmedEmpquotasm> findById(Optional<String> id) throws Exception {
	//	return id.map(pk -> zmedEmpquotasmRepository.findOne(pk));
	//}

	@Override
	public Optional<Collection<ZmedEmpquotasm>> fetchAll() {
		List<ZmedEmpquotasm> entities = new ArrayList<>();
		Optional<Iterable<ZmedEmpquotasm>> entityIterable = Optional.ofNullable(zmedEmpquotasmRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}

	@Override
	public Optional<ZmedEmpquotasm> findById(
			Optional<ZmedCompositeKeyEmpquotasm> id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<ZmedEmpquotasm> findOneByCompositeKey(long pernr,
			String bukrs, String persg, String persk, String fatxt,
			String kodequo, Date datab, Date datbi) {
		if (bukrs == null || pernr == 0 || kodequo == null ||  persk == null || persg == null || fatxt == null ||datab == null || datbi == null) {
			return Optional.empty();
		}
		//String p = ""+pernr;
		return Optional.ofNullable(zmedEmpquotasmRepository.findOneByCompositeKey(pernr, bukrs, persg, persk, fatxt, kodequo, datab, datbi));
	}

	@Override
	public List<ZmedEmpquotasm> findQuotas(Long pernr, String bukrs,
			String persg, String persk, Date datab, Date datbi) {
		if (bukrs == null || persg == null || persk == null || datab == null || datbi == null || pernr == 0) {
			return Collections.emptyList();
		}
		
		T001 b = t001Repository.findByBukrs(bukrs);
		
		Optional<Collection<ZmedEmpquotasm>> bunchOfZmed
			= Optional.ofNullable(zmedEmpquotasmRepository.findQuotas(pernr, bukrs, persg, persk, datab, datbi));
		
		/*
		 * Fix findQuotas method
		if (!bunchOfZmed.isPresent() || bunchOfZmed.get().size() < 1) {
			bunchOfZmed	= Optional.ofNullable(zmedEmpquotasmRepository.findByPernrAndBukrsAndPerskAndPersg(pernr, b, persk, persg));
		}
		*/
		
		return (bunchOfZmed.isPresent()
				? bunchOfZmed.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}

	


	/*@Override
	public Optional<Collection<ZmedEmpquotasm>> findByEntityName(final Long entityName) {
		return Optional.ofNullable(zmedEmpquotasmRepository.findByPernr(entityName));
	}*/

}
