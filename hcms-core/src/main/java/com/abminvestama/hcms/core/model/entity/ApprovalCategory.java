package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.abminvestama.hcms.core.model.constant.EntityCategory;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Entity
@Table(name = "cfg_approval_category", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"ent_category", "m_workflow_id"} )
})
public class ApprovalCategory extends AbstractEntity {

	private static final long serialVersionUID = 2252636401698972661L;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "ent_category", nullable = false)
	private EntityCategory entityCategory;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "m_workflow_id", referencedColumnName = "id", nullable = false)
	private Workflow workflow;
	
	ApprovalCategory() {}
	
	public ApprovalCategory(EntityCategory entityCategory, Workflow workflow) {
		this.entityCategory = entityCategory;
		this.workflow = workflow;
	}
	
	public EntityCategory getEntityCategory() {
		return entityCategory;
	}
	
	public Workflow getWorkflow() {
		return workflow;
	}
}