package com.abminvestama.hcms.core.service.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * Service Utility for File Attachment operation (e.g. Save/Open file attachment) 
 * 
 * @author baihaki.pru@gmail.com
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("fileAttachmentService")
public class FileAttachmentService {
	
    @Autowired
    String attachmentBasePath;
    
    /**
     * Save the base64 encoded string to persistent file
     * 
     * @param filename the full filename and extension, excluding path directory
     * @param base64 the base64 encoded string
     * @return the filename, or <b>null</b> if save operation was failed
     * @throws IOException 
     */
    public String save(String filename, String base64) throws IOException {
    	String result = null;
		byte[] data = Base64.getDecoder().decode(base64);
		String path = attachmentBasePath.concat(filename);
		OutputStream stream = new FileOutputStream(path);
		stream.write(data);
		stream.close();
		result = filename;
		return result;
    }
    
    /**
     * Open the persistent file and encode to base64 string  
     * 
     * @param filename the filename to open
     * @return the base64 string
     * @throws IOException
     */
    public String open(String filename) throws IOException {
    	File file = new File(attachmentBasePath.concat(filename));
        int length = (int) file.length();
        BufferedInputStream reader = new BufferedInputStream(new FileInputStream(file));
        byte[] bytes = new byte[length];
        reader.read(bytes, 0, length);
        reader.close();
        return Base64.getEncoder().encodeToString(bytes);
    }
    
}