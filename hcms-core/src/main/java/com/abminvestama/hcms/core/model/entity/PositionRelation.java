package com.abminvestama.hcms.core.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 * Class that represents <strong>position_relations</strong>.
 */
@Entity
@Table(name = "position_relations")
public class PositionRelation extends AbstractEntity {

	private static final long serialVersionUID = -5737107924663604863L;

	@ManyToOne
	@JoinColumn(name = "position_relation_type_id", referencedColumnName = "id")
	private PositionRelationType positionRelationType;
	
	@ManyToOne
	@JoinColumn(name = "from_position_id", referencedColumnName = "id")
	private Position fromPosition;

	@ManyToOne
	@JoinColumn(name = "to_position_id", referencedColumnName = "id")	
	private Position toPosition;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "valid_from", nullable = true)
	private Date validFrom;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "valid_thru", nullable = true)	
	private Date validThru;
	
	public PositionRelation() {}
	
	/**
	 * GET Position Relation Type.
	 * 
	 * @return
	 */
	public PositionRelationType getPositionRelationType() {
		return positionRelationType;
	}
	
	public void setPositionRelationType(PositionRelationType positionRelationType) {
		this.positionRelationType = positionRelationType;
	}
	
	/**
	 * GET From Position.
	 * 
	 * @return
	 */
	public Position getFromPosition() {
		return fromPosition;
	}
	
	public void setFromPosition(Position fromPosition) {
		this.fromPosition = fromPosition;
	}
	
	/**
	 * GET To Position.
	 * 
	 * @return
	 */
	public Position getToPosition() {
		return toPosition;
	}
	
	public void setToPosition(Position toPosition) {
		this.toPosition = toPosition;
	}
	
	/**
	 * GET Valid From.
	 * 
	 * @return
	 */
	public Date getValidFrom() {
		return validFrom;
	}
	
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	
	/**
	 * GET Valid Thru.
	 * 
	 * @return
	 */
	public Date getValidThru() {
		return validThru;
	}
	
	public void setValidThru(Date validThru) {
		this.validThru = validThru;
	}
}