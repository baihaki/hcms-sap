package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;

import org.hibernate.annotations.Immutable;



@Embeddable
@Immutable
public class PhdatKey implements Serializable {

	private static final long serialVersionUID = -378941625335670343L;
	
	/*
	 * public holiday type
	 */
	private Date erdat;
	
	public PhdatKey() {}
	
	public PhdatKey(Date erdat) {
		this.erdat = erdat;
	}
	
	public Date getErdat() {
		return erdat;
	}
			
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final PhdatKey key = (PhdatKey) o;
		
		if (key.getErdat() == null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.erdat != null ? this.erdat.hashCode() : 0;
		return result;
	}
}
