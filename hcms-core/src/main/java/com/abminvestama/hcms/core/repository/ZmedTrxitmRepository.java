package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;
import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;
import com.abminvestama.hcms.core.model.entity.ZmedTrxitm;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author anasuya (anasuyahirai@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add updateClmno method</td></tr>
 *     <tr><td>1.0.0</td><td>Anasuya</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface ZmedTrxitmRepository extends CrudRepository<ZmedTrxitm, String> {

	/*ZmedTrxitm findByKodemed(String kodemed);	*/
	
	@Query("FROM ZmedTrxitm zmed_trxitm WHERE zmed_trxitm.itmno = :itmno AND zmed_trxitm.clmno = :clmno")
	ZmedTrxitm findOneByCompositeKey(@Param("itmno") String itmno, @Param("clmno") String clmno);

	
	@Query(nativeQuery = true, value = "SELECT MAX(itmno) FROM zmed_trxitm")
	String findLastId();
	

	@Query("FROM ZmedTrxitm zmed_trxitm WHERE zmed_trxitm.clmno = :clmno")
	Collection<ZmedTrxitm> findByClmno(@Param("clmno") String clmno);
	

	//@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.id.endda = :endda AND it0002.id.begda = :begda")
	//IT0002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
	//		@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Modifying(clearAutomatically = true)
	@Query(nativeQuery=true, value="UPDATE zmed_trxitm SET clmno = :newClmno WHERE clmno = :oldClmno AND itmno = :itmno")
	int updateClmno(@Param("newClmno") String newClmno, @Param("oldClmno") String oldClmno, @Param("itmno") String itmno);
	
	
}