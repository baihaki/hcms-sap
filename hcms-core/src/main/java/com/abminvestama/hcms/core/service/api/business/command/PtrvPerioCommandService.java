package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.PtrvPerio;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;


public interface PtrvPerioCommandService extends DatabaseCommandService<PtrvPerio> {
}