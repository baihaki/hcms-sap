package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyQuotaused;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquotasm;
import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;


public interface ZmedQuotausedQueryService extends DatabaseQueryService<ZmedQuotaused, ZmedCompositeKeyQuotaused>, DatabasePaginationQueryService{
	//Optional<Collection<ZmedEmpquota>> findByEntityName(final Long entityName);
	

	@NotNull
	Optional<ZmedQuotaused> findOneByCompositeKey(int gjahr, String bukrs, long pernr, String kodequo);
	

	@NotNull
	List<ZmedQuotaused> findQuotas(int gjahr, String bukrs, long pernr);
}

