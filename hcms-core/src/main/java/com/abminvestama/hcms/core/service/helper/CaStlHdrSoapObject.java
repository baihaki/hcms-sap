package com.abminvestama.hcms.core.service.helper;

import java.util.List;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.CaStlHdr;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 3.0.0
 * @since 3.0.0
 *
 */
public class CaStlHdrSoapObject {
	
	private String stlno;
	private String pernr;
	private String bukrs;
	private String approvedtreasuryat;
	private String text;
	private String amount;
	
	private List<CaStlDtlSoapObject> listCaStlDtl;

	private CaStlHdrSoapObject() {}
	
	public CaStlHdrSoapObject(CaStlHdr object) {
		if (object == null) {
			new CaStlHdrSoapObject();
		} else {
			stlno = object.getTcaNo().getReqNo();
			pernr = object.getPernr().toString();
			bukrs = object.getBukrs().getBukrs();
			approvedtreasuryat = object.getUpdatedAt() != null ? CommonDateFunction.convertDateToStringYMD(object.getUpdatedAt()) : null;
			text = object.getNotes();
			amount = object.getTotalAmount() != null && object.getTotalAmount() > 0 ? String.valueOf(object.getTotalAmount().intValue()) : "0";
		}		
	}

	public String getStlno() {
		return stlno;
	}

	public String getPernr() {
		return pernr;
	}

	public String getBukrs() {
		return bukrs;
	}

	public String getApprovedtreasuryat() {
		return approvedtreasuryat;
	}

	public List<CaStlDtlSoapObject> getListCaStlDtl() {
		return listCaStlDtl;
	}

	public void setListCaStlDtl(List<CaStlDtlSoapObject> listCaStlDtl) {
		this.listCaStlDtl = listCaStlDtl;
	}

	public String getText() {
		return text;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
}
