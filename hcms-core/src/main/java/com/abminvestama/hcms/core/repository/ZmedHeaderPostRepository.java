package com.abminvestama.hcms.core.repository;

import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.ZmedCostype;
import com.abminvestama.hcms.core.model.entity.ZmedHeaderPost;

public interface ZmedHeaderPostRepository extends CrudRepository<ZmedHeaderPost, String> {
	
	//ZmedHeaderPost findByEntityName(String Name);	

	/*ZmedHeaderPost findByObjKey(String objKey);	*/
	@Query("FROM ZmedHeaderPost zmed_header_post WHERE zmed_header_post.objType = :obj_type AND zmed_header_post.objSys = :obj_sys AND zmed_header_post.objKey = :obj_key")
	ZmedHeaderPost findOneByCompositeKey(@Param("obj_type") String objType, 
			@Param("obj_key") String objKey,
			@Param("obj_sys") String objSys);
	
	

	//@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.id.endda = :endda AND it0002.id.begda = :begda")
	//IT0002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
	//		@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	
}
