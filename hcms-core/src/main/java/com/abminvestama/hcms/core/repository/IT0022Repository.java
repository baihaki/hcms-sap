package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0022;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>anasuya (anasuyahirai@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add methods: findByPernrForPublishedOrUpdated</td></tr>
 *     <tr><td>1.0.1</td><td>Anasuya</td><td>Add methods: countByEduAndBukrs</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0022Repository extends CrudRepository<IT0022, ITCompositeKeys> {
	
	@Query("FROM IT0022 it0022 WHERE it0022.pernr = :pernr AND it0022.id.infty = '0022' AND it0022.id.subty = :subty AND it0022.id.endda = :endda AND it0022.id.begda = :begda")
	IT0022 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("subty") String subty,
			@Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query("FROM IT0022 it0022 WHERE it0022.pernr = :pernr AND it0022.id.infty = '0022' ORDER BY it0022.id.subty ASC, it0022.id.endda DESC")
	Collection<IT0022> findByPernr(@Param("pernr") Long pernr);
	
	@Query("FROM IT0022 it0022 WHERE it0022.pernr = :pernr AND it0022.id.infty = '0022' AND it0022.id.subty = :subty ORDER BY it0022.id.endda DESC")
	Collection<IT0022> findByPernrAndSubty(@Param("pernr") Long pernr, @Param("subty") String subty);		
	

	@Query("FROM IT0022 it0022 WHERE it0022.status = :status ORDER BY it0022.id.endda DESC")
	Page<IT0022> findByStatus(@Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0022 it0022 WHERE it0022.pernr = :pernr AND it0022.status = :status ORDER BY it0022.id.endda DESC")
	Page<IT0022> findByPernrAndStatus(@Param("pernr") Long pernr, @Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0022 it0022 WHERE it0022.pernr = :pernr AND " +
	"(it0022.status = 'PUBLISHED' OR it0022.status = 'UPDATE_IN_PROGRESS' OR it0022.status = 'UPDATE_REJECTED') " +
	" ORDER BY it0022.id.endda DESC")
	Page<IT0022> findByPernrForPublishedOrUpdated(@Param("pernr") Long pernr, Pageable pageRequest);

	@Query(nativeQuery=true, value="SELECT count(*) FROM it0001 WHERE bukrs = :bukrs")
	Long countByEduAndBukrs(@Param("bukrs") String bukrs);

	@Query(nativeQuery=true, value="SELECT count(*) FROM it0022 WHERE slart = :slart AND pernr IN (SELECT pernr FROM it0001 WHERE bukrs = :bukrs)")
	Long countByEduAndBukrs(@Param("slart") String slart, 
							@Param("bukrs") String bukrs);
}