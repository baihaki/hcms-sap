package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.List;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.CaStlDtl;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.CaStlDtlRepository;
import com.abminvestama.hcms.core.service.api.business.command.CaStlDtlCommandService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author wijanarko (wijanarko@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("caStlDtlCommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CaStlDtlCommandServiceImpl implements CaStlDtlCommandService {
	private CaStlDtlRepository caStlDtlRepository;
	
	@Autowired
	CaStlDtlCommandServiceImpl(CaStlDtlRepository caStlDtlRepository) {
		this.caStlDtlRepository = caStlDtlRepository;
		
	}

	@Override
	public Optional<CaStlDtl> save(CaStlDtl entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		try {
			return Optional.ofNullable(caStlDtlRepository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist CaStlDtl (Personal Information).Reason=" + e.getMessage());
		}
	}

	@Override
	public boolean delete(CaStlDtl entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean restore(CaStlDtl entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteList(List<CaStlDtl> entities) {
		try {
			caStlDtlRepository.delete(entities);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
