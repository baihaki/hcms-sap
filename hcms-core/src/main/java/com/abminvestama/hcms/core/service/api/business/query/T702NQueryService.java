package com.abminvestama.hcms.core.service.api.business.query;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.T702N;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

public interface T702NQueryService extends DatabaseQueryService<T702N, String>, DatabasePaginationQueryService {
	
	Page<T702N> findAllWithPaging(int pageNumber);
}