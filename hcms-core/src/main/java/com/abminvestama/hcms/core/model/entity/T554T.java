package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 * Class that represents master data for <strong>Absence Types</strong> (i.e. T554T in SAP).
 */
@Entity
@Table(name = "t554t")
public class T554T implements Serializable {

	private static final long serialVersionUID = 4748860779059341046L;

	@Id
	@Column(name = "awart", nullable = false, unique = true)
	private String awart;
	
	@Column(name = "atext", nullable = false)
	private String atext;
	
	public T554T() {}
	
	public String getAwart() {
		return awart;
	}
	
	public String getAtext() {
		return atext;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final T554T obj = (T554T) o;
		
		if (obj.getAwart() != null ? !obj.getAwart().equalsIgnoreCase(awart) : awart != null) {
			return false;
		}
		
		if (obj.getAtext() != null ? !obj.getAtext().equalsIgnoreCase(atext) : atext != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.awart != null ? this.awart.hashCode() : 0;
		result = result * 31 + (this.atext != null ? this.atext.hashCode() : 0);
		return result;
	}
}