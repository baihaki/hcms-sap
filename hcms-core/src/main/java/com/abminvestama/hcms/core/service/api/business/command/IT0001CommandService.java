package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.IT0001;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface IT0001CommandService extends DatabaseCommandService<IT0001> {
}