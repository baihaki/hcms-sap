package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.abminvestama.hcms.core.model.constant.CashAdvanceConstant;

/**
 * 
 * Class that represents <strong>Cash Advance Settlement Header</strong> transaction.
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@Entity
@Table(name = CashAdvanceConstant.TABEL_EMPLOYEE_CSKT_POOL)
public class CaEmpCSKT implements Serializable {
	
	private static final long serialVersionUID = 5722290313984397719L;

	@Id
	@Column(name = "emp_id", nullable = false)
	private Long empId;

	@Column(name = "pernr")
	private Long pernr;

	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "kokrs", referencedColumnName = "kokrs"),
		@JoinColumn(name = "kostl_default", referencedColumnName = "kostl")
	})
	private CSKT cskt;

	@Column(name = "kostl_level")
	private Integer kostlLevel;

	@Column(name = "kostl_pool")
	private String kostlPool;

	@Column(name = "pooling_code")
	private String poolingCode;

	public CaEmpCSKT() {}
	
	public CaEmpCSKT(Long empId, Long pernr, CSKT cskt, Integer kostlLevel, String kostlPool, String poolingCode) {
		this.empId = empId;
		this.pernr = pernr;
		this.cskt = cskt;
		this.kostlLevel = kostlLevel;
		this.kostlPool = kostlPool;
		this.poolingCode = poolingCode;
	}

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}

	public Long getPernr() {
		return pernr;
	}

	public void setPernr(Long pernr) {
		this.pernr = pernr;
	}

	public CSKT getCskt() {
		return cskt;
	}

	public void setCskt(CSKT cskt) {
		this.cskt = cskt;
	}

	public Integer getKostlLevel() {
		return kostlLevel;
	}

	public void setKostlLevel(Integer kostlLevel) {
		this.kostlLevel = kostlLevel;
	}

	public String getKostlPool() {
		return kostlPool;
	}

	public void setKostlPool(String kostlPool) {
		this.kostlPool = kostlPool;
	}

	public String getPoolingCode() {
		return poolingCode;
	}

	public void setPoolingCode(String poolingCode) {
		this.poolingCode = poolingCode;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		CaEmpCSKT caEmpCSKT = (CaEmpCSKT) o;
		 
		if (empId != null ? !empId.equals(caEmpCSKT.empId) : caEmpCSKT.empId != null) return false; 
		if (pernr != null ? !pernr.equals(caEmpCSKT.pernr) : caEmpCSKT.pernr != null) return false; 
		if (cskt != null ? !cskt.equals(caEmpCSKT.cskt) : caEmpCSKT.cskt != null) return false;
		if (kostlLevel != null ? !kostlLevel.equals(caEmpCSKT.kostlLevel) : caEmpCSKT.kostlLevel != null) return false;
		if (kostlPool != null ? !kostlPool.equals(caEmpCSKT.kostlPool) : caEmpCSKT.kostlPool != null) return false;
		if (poolingCode != null ? !poolingCode.equals(caEmpCSKT.poolingCode) : caEmpCSKT.poolingCode != null) return false;
		return true; 
	}

	@Override
	public int hashCode() {
	    int result = (empId != null ?  empId.hashCode() : 0); 
		result = 31 * (pernr != null ?  pernr.hashCode() : 0); 
		result = 31 * (cskt != null ?  cskt.hashCode() : 0); 
		result = 31 * (kostlLevel != null ?  kostlLevel.hashCode() : 0); 
		result = 31 * (kostlPool != null ?  kostlPool.hashCode() : 0); 
		result = 31 * (poolingCode != null ?  poolingCode.hashCode() : 0); 
	 
		return result; 
	}

	@Override
	public String toString() {
		return "caEmpCSKT(empId="+empId+",pernr="+pernr+",cskt="+cskt+",kostlLevel="+kostlLevel+",kostlPool="+kostlPool+",poolingCode="+poolingCode+")"; 
	}

}
