package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.ZmedMedtype;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;


public interface ZmedMedtypeCommandService extends DatabaseCommandService<ZmedMedtype> {
}