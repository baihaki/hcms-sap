package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.HCMSEntity;
import com.abminvestama.hcms.core.repository.HCMSEntityRepository;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("hcmsEntityQueryService")
@Transactional(readOnly = true)
public class HCMSEntityQueryServiceImpl implements HCMSEntityQueryService {

	private HCMSEntityRepository hcmsEntityRepository;
	
	@Autowired
	HCMSEntityQueryServiceImpl(HCMSEntityRepository hcmsEntityRepository) {
		this.hcmsEntityRepository = hcmsEntityRepository;
	}
	
	@Override
	public Optional<HCMSEntity> findById(Optional<String> id) throws Exception {
		return id.map(pk -> hcmsEntityRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<HCMSEntity>> fetchAll() {
		List<HCMSEntity> entities = new ArrayList<>();
		Optional<Iterable<HCMSEntity>> entityIterable = Optional.ofNullable(hcmsEntityRepository.findAll());
		return entityIterable.map(iter -> {
			iter.forEach(entity -> entities.add(entity));
			return entities;
		});		
	}

	@Override
	public Optional<HCMSEntity> findByEntityName(final String entityName) {
		return Optional.ofNullable(hcmsEntityRepository.findByEntityName(entityName));
	}
}