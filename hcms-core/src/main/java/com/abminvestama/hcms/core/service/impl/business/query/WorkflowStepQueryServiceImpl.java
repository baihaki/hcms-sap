package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.Workflow;
import com.abminvestama.hcms.core.model.entity.WorkflowStep;
import com.abminvestama.hcms.core.repository.WorkflowStepRepository;
import com.abminvestama.hcms.core.service.api.business.query.WorkflowStepQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
@Service("workflowStepQueryService")
@Transactional(readOnly = true)
public class WorkflowStepQueryServiceImpl implements WorkflowStepQueryService {
	
	private WorkflowStepRepository workflowStepRepository;
	
	@Autowired
	WorkflowStepQueryServiceImpl(WorkflowStepRepository workflowStepRepository) {
		this.workflowStepRepository = workflowStepRepository;
	}

	@Override
	public Optional<WorkflowStep> findById(Optional<String> id) throws Exception {
		return id.map(pk -> workflowStepRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<WorkflowStep>> fetchAll() {
		List<WorkflowStep> workflowSteps = new ArrayList<>();
		workflowStepRepository.findAll().forEach(ws -> {
			workflowSteps.add(ws);
		});
		return Optional.of(workflowSteps);
	}

	@Override
	public Page<WorkflowStep> fetchAllWithPaging(int pageNumber) {
		Pageable pageRequest = createPageRequest(pageNumber);
		
		return workflowStepRepository.findAll(pageRequest);
	}

	@Override
	public Optional<WorkflowStep> findByWorkflowIdAndSeqAndPositionId(String workflowId, int seq,
			String positionId) {
		return Optional.ofNullable(
				workflowStepRepository.findByWorkflowIdAndSeqAndPositionId(workflowId, seq, positionId));
	}

	@Override
	public Optional<Collection<WorkflowStep>> findByWorkflowId(String workflowId) {
		return Optional.ofNullable(workflowStepRepository.findByWorkflowId(workflowId));
	}

	@Override
	public Optional<Collection<WorkflowStep>> findByWorkflowAndSeq(Workflow workflow, int seq) {
		return Optional.ofNullable(workflowStepRepository.findByWorkflowAndSeqOrderByUpdatedAtDesc(workflow, seq));
	}

	@Override
	public Optional<Collection<WorkflowStep>> findByWorkflowAndGreaterThanSeq(Workflow workflow, int seq) {
		return Optional.ofNullable(workflowStepRepository.findByWorkflowAndGreaterThanSeq(workflow, seq));
	}
}