package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.abminvestama.hcms.core.model.constant.ApprovalAction;
import com.abminvestama.hcms.core.model.entity.ApprovalEventLog;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 2.0.0
 * @version 2.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add lastCreatedDate parameter on findByEntityIdAndObjectIdAndApprovalAction</td></tr>
 *     <tr><td>2.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface ApprovalEventLogQueryService extends DatabaseQueryService<ApprovalEventLog, String> {

	@NotNull
	Collection<ApprovalEventLog> findByEntityIdAndObjectId(final String entityId, final String objectId);
	
	@NotNull
	Collection<ApprovalEventLog> findByEntityIdAndObjectIdAndApprovalAction(final String entityId,
			final String objectId, ApprovalAction approvalAction, Date lastCreatedDate);
}