package com.abminvestama.hcms.core.exception;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 * This class is an exception that should be thrown when a workflow can't be found in the database.
 */
public class WorkflowNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 8560136768532906035L;
	
	protected String field;
	
	public WorkflowNotFoundException(final String message) {
		super(message);
	}
	
	public WorkflowNotFoundException(final String message, final String field) {
		this.field = field;
	}
	
	public WorkflowNotFoundException(final String message, final Throwable e) {
		super(message, e);
	}
	
	public WorkflowNotFoundException(final String message, final String field, final Throwable e) {
		super(message, e);
		this.field = field;		
	}
	
	public String getField() {
		return field;
	}
}