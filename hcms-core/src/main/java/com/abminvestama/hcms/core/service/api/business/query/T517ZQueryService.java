package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.T517T;
import com.abminvestama.hcms.core.model.entity.T517Z;
import com.abminvestama.hcms.core.model.entity.T517ZKey;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface T517ZQueryService extends DatabaseQueryService<T517Z, T517ZKey>, DatabasePaginationQueryService {
	
	Page<T517Z> fetchAllWithPaging(int pageNumber);
	Collection<T517Z> findBySlart(T517T slart);
}