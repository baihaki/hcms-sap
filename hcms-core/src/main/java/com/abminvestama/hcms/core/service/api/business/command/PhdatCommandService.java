package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.Phdat;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;

public interface PhdatCommandService extends DatabaseCommandService<Phdat> {
}