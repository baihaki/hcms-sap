package com.abminvestama.hcms.core.support.dataseed;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import com.abminvestama.hcms.core.model.entity.RegionType;
import com.abminvestama.hcms.core.model.entity.VehicleType;


/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 * This class should be invoked via gradle task command (e.g. gradle seedInitial).
 * This class will populate initial data those needed by the system.
 */
@Component
public final class TravelDataSeed {

	private static final Logger LOGGER = LoggerFactory.getLogger(TravelDataSeed.class);
	
	private static final String DEFAULT_USER = "superadmin";

	private static ApplicationContext context;
	
	private static EntityManagerFactory emf;
	private static EntityManager em;
	
	public static void main(String... args) throws Exception {

		LOGGER.warn("Starting travel seed child process...");
		//marking the start
		long start = System.currentTimeMillis();
		
		context = new AnnotationConfigApplicationContext(CoreAppConfig.class);
		
		emf = (EntityManagerFactory) context.getBean("entityManagerFactory");
		em = emf.createEntityManager();

		populateVehicleType("TRAIN", "Train");
		populateVehicleType("AIRPLANE", "Air Plane");
		populateVehicleType("SHIP", "Ship");
		populateVehicleType("BUS", "Bus");
		populateVehicleType("CAR_RENTAL", "Car Rental");
		
		populateRegionType("COUNTRY", "Country");
		populateRegionType("PROVINCE", "Province");
		populateRegionType("DISTRICT", "District");
		populateRegionType("SUBDISTRICT", "Subdistrict");
		populateRegionType("VILLAGE", "Village");
		
		long end = System.currentTimeMillis();
		
		LOGGER.info("Finished travel data seed within " + (end - start) + "ms");
	}
	
	private static void populateVehicleType(final String identName, final String name) throws Exception {
		Query query = em.createQuery("FROM VehicleType vt WHERE vt.identName = :identName");
		query.setParameter("identName", identName);
		if (query.getResultList().isEmpty()) {
			LOGGER.debug("Inserting new vehicle type");
			em.getTransaction().begin();
			VehicleType vehicleType = new VehicleType();
			vehicleType.setIdentName(identName);
			vehicleType.setName(name);
			vehicleType.setCreatedById(DEFAULT_USER);
			vehicleType.setUpdatedById(DEFAULT_USER);
			em.persist(vehicleType);
			em.flush();
			em.getTransaction().commit();
		}		
	}
	
	private static void populateRegionType(final String identName, String name) throws Exception {
		Query query = em.createQuery("FROM RegionType rt WHERE rt.identName = :identName");
		query.setParameter("identName", identName);
		if (query.getResultList().isEmpty()) {
			LOGGER.debug("Inserting new region type");
			em.getTransaction().begin();
			RegionType regionType = new RegionType();
			regionType.setIdentName(identName);
			regionType.setName(name);
			regionType.setCreatedById(DEFAULT_USER);
			regionType.setUpdatedById(DEFAULT_USER);
			em.persist(regionType);
			em.flush();
			em.getTransaction().commit();
		}
	}
}