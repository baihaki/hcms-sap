package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.IT0001Log;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface IT0001LogQueryService extends DatabaseQueryService<IT0001Log, String>, DatabasePaginationQueryService {

	@NotNull
	Page<IT0001Log> findByPernrAndEnddaAndBegda(Long pernr, Date endda, Date begda, int pageNumber);
	
	Page<IT0001Log> fetchAllWithPaging(int pageNumber);
}