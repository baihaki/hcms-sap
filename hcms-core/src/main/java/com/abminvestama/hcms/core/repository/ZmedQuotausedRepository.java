package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedQuotaused;
import com.abminvestama.hcms.core.model.entity.ZmedTrxhdr;

public interface ZmedQuotausedRepository extends CrudRepository<ZmedQuotaused, String> {
	
	//ZmedQuotaused findByEntityName(String Name);	
	
	/*@Query("FROM ZmedEmpquota zmed_empquota WHERE zmed_empquota.pernr = :pernr")
	Collection<ZmedEmpquota> findByPernr(@Param("pernr") Long pernr);	*/
	
	@Query("FROM ZmedQuotaused zmed_quotaused WHERE zmed_quotaused.gjahr = :gjahr AND zmed_quotaused.bukrs.bukrs = :bukrs AND zmed_quotaused.pernr = :pernr AND zmed_quotaused.kodequo = :kodequo")
	ZmedQuotaused findOneByCompositeKey(@Param("gjahr") int gjahr, 
			@Param("bukrs") String bukrs, 
			@Param("pernr") long pernr,
			@Param("kodequo") String kodequo);
	
	

	@Query("FROM ZmedQuotaused zmed_quotaused WHERE zmed_quotaused.gjahr = :gjahr AND zmed_quotaused.bukrs.bukrs = :bukrs AND zmed_quotaused.pernr = :pernr")
	Collection<ZmedQuotaused> findQuotas(@Param("gjahr") int gjahr, 
			@Param("bukrs") String bukrs, 
			@Param("pernr") long pernr);
	

	//@Query("FROM IT0002 it0002 WHERE it0002.pernr = :pernr AND it0002.id.endda = :endda AND it0002.id.begda = :begda")
	//IT0002 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("endda") @Temporal(TemporalType.DATE) Date endda,
	//		@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	
}
