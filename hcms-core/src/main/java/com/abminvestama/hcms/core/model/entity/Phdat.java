package com.abminvestama.hcms.core.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "phdat")
public class Phdat extends SAPAbstractEntityZmed<PhdatKey> {

	private static final long serialVersionUID = -1303527839443833492L;

	public Phdat() {}

	public Phdat(PhdatKey id) {
		this();
		this.id = id;
	}

	/*
	 * public holiday type
	 */
	@Column(name = "pinco")
	private String pinco;

	/*
	 * public holiday type description
	 */
	@Column(name = "phdtxt")
	private String phdtxt;

	/*
	 * public holiday description
	 */
	@Column(name = "phtxt")
	private String phtxt;

	public String getPinco() {
		return pinco;
	}

	public void setPinco(String pinco) {
		this.pinco = pinco;
	}

	public String getPhdtxt() {
		return phdtxt;
	}

	public void setPhdtxt(String phdtxt) {
		this.phdtxt = phdtxt;
	}

	public String getPhtxt() {
		return phtxt;
	}

	public void setPhtxt(String phtxt) {
		this.phtxt = phtxt;
	}
	
}
