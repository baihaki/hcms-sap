package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.entity.IT0021;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.IT0021Repository;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.IT0021CommandService;
import com.abminvestama.hcms.core.service.helper.IT0021Mapper;
import com.abminvestama.hcms.core.service.helper.MasterDataSoapObject;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add saveListFromMulesoft method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("it0021CommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class IT0021CommandServiceImpl implements IT0021CommandService {

	private IT0021Repository it0021Repository;
	
	@Autowired
	IT0021CommandServiceImpl(IT0021Repository it0021Repository) {
		this.it0021Repository = it0021Repository;
	}
	
	@Override
	public Optional<IT0021> save(IT0021 entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setUname(user.getId());
		}
		
		try {
			return Optional.ofNullable(it0021Repository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist IT0021 (Emergency Info).Reason=" + e.getMessage());
		}				
	}

	@Override
	public boolean delete(IT0021 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(IT0021 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}
	
	@Override
	public String saveListFromMulesoft(MuleConsumerService muleConsumerService, CommonServiceFactory commonServiceFactory, Long pernr, User user)
			throws ConstraintViolationException, CannotPersistException, NoSuchMethodException {
		String result = "";
		MasterDataSoapObject masterDataSoapObject = new MasterDataSoapObject(null, null, pernr.toString());
		
		List<Map<String, String>> listIT = muleConsumerService.getFromSAP(BAPIFunction.INFOTYPE_FETCH.service(),
				BAPIFunction.INFOTYPE_FETCH_FAMILY_URI.service(), masterDataSoapObject);
		
		if (!listIT.isEmpty()) {
			Set<String> setIT = new HashSet<String>();
			for (int it21 = 0; it21 < listIT.size(); it21++) {
				if (listIT.get(it21).get("field1") != null && Long.valueOf(listIT.get(it21).get("field1")).longValue() == pernr.longValue()) {
					IT0021 it0021 = new IT0021Mapper(listIT.get(it21), commonServiceFactory).getIT0021();
					String key = it0021.getId().toStringId();
					if (setIT.contains(key)) {
						// duplicate key, increment begda based on seqnr (SAP objectid)
						Calendar cal = Calendar.getInstance();
						cal.setTime(it0021.getId().getBegda());
						int objectId = it0021.getSeqnr().intValue();
						cal.add(Calendar.DATE, objectId);
						ITCompositeKeys newId = new ITCompositeKeys(it0021.getId().getPernr(), it0021.getId().getInfty(),
								it0021.getId().getSubty(), it0021.getId().getEndda(), cal.getTime());
						it0021.setId(newId);
					} else {
						setIT.add(key);
					}
					Optional<IT0021> savedITEntity = save(it0021, user);
					if (!savedITEntity.isPresent()) {
						result = result.concat("Failed to save IT0021 object: ".concat(it0021.getId().toStringId()).concat(". "));
					}
				}
			}
		}
		
		return result;
	}
	
}