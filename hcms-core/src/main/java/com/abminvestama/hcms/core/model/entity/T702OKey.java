package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Immutable;

@Embeddable
@Immutable
public class T702OKey implements Serializable {

	private static final long serialVersionUID = 2987013141063448484L;
	
	private String morei;
	private String land1;
	
	public T702OKey() {}
	
	public T702OKey(String morei, String land1) {
		this.morei = morei;
		this.land1 = land1;
	}
	
	public String getMorei() {
		return morei;
	}
	
	public String getLand1() {
		return land1;
	}
	
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final T702OKey key = (T702OKey) o;
		
		if (key.getMorei() != null ? !key.getMorei().equalsIgnoreCase(morei) : morei != null) {
			return false;
		}
		
		if (key.getLand1() != null ? !key.getLand1().equalsIgnoreCase(land1) : land1 != null) {
			return false;
		}
		
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.morei != null ? this.morei.hashCode() : 0;
		result = result * 31 + (this.land1 != null ? this.land1.hashCode() : 0);
		return result;
	}
}