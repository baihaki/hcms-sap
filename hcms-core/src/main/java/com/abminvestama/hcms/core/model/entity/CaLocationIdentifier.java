package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 
 * Class that represents <strong>Location Identifier</strong> object.
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 3.0.0 (Cash Advance)
 * @since 3.0.0
 *
 */
@Entity
@Table(name = "ca_location_identifier")
public class CaLocationIdentifier implements Serializable {
	
	private static final long serialVersionUID = 8499990990508806189L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@Column(name = "id", nullable = false, insertable = false, updatable = false, length = 8)
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	private String id;
	
	@Column(name = "location_code", nullable = false, length = 5, unique = true)
	private String locationCode;
	
	@Column(name = "location_text", nullable = false, length = 255)
	private String locationText;
	
	@ManyToOne
	@JoinColumn(name = "country_code", referencedColumnName = "land1")
	private T005T country;
	
	@ManyToOne
	@JoinColumn(name = "location_type_id", referencedColumnName = "id")
	private CaLocationType locationType;
	
	@Column(name = "operate")
	private boolean operate; 
	
	public void setId(String id){
		this.id = id;
	}
 
	public String getLocationCode(){
		return locationCode;
	}
	
	public void setLocationCode(String locationCode){
		this.locationCode = locationCode;
	}
 
	public String getLocationText(){
		return locationText;
	}
	
	public void setLocationText(String locationText){
		this.locationText = locationText;
	}
 
	public T005T getCountry(){
		return country;
	}
	
	public void setCountry(T005T country){
		this.country = country;
	}
 
	public CaLocationType getLocationType(){
		return locationType;
	}
	
	public void setLocationTypeId(CaLocationType locationType){
		this.locationType = locationType;
	}
 
	public boolean getOperate(){
		return operate;
	}
	
	public void setOperate(boolean operate){
		this.operate = operate;
	}
	
	public CaLocationIdentifier(){
	}

	public CaLocationIdentifier( String id,  String locationCode,  String locationText,  T005T countryCode,  CaLocationType locationType,  boolean operate ){
	 	this.id = id;
	 	this.locationCode = locationCode;
	 	this.locationText = locationText;
	 	this.country = countryCode;
	 	this.locationType = locationType;
	 	this.operate = operate;
	}
	
	public Object getId() {
		return  id;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		CaLocationIdentifier caLocationIdentifier = (CaLocationIdentifier) o;
		 
		if (id != null ? !id.equals(caLocationIdentifier.id) : caLocationIdentifier.id != null) return false; 
		if (locationCode != null ? !locationCode.equals(caLocationIdentifier.locationCode) : caLocationIdentifier.locationCode != null) return false; 
		if (locationText != null ? !locationText.equals(caLocationIdentifier.locationText) : caLocationIdentifier.locationText != null) return false; 
		if (country != null ? !country.equals(caLocationIdentifier.country) : caLocationIdentifier.country != null) return false; 
		if (locationType != null ? !locationType.equals(caLocationIdentifier.locationType) : caLocationIdentifier.locationType != null) return false; 
		if (operate != caLocationIdentifier.operate) return false; 
		return true; 
	}
	
	@Override
	public int hashCode() {
	    int result = (id != null ?  id.hashCode() : 0); 
		result = 31 * (locationCode != null ?  locationCode.hashCode() : 0); 
		result = 31 * (locationText != null ?  locationText.hashCode() : 0); 
		result = 31 * (country != null ?  country.hashCode() : 0); 
		result = 31 * (locationType != null ?  locationType.hashCode() : 0); 
		result = 31 * result + (operate ? 1 : 0);  
		return result; 
	}
	
	@Override
	public String toString() {
		return "caLocationIdentifier(id="+id+",locationCode="+locationCode+",locationText="+locationText+",countryCode="+country+",locationTypeId="+locationType+",operate="+operate+')'; 
	}
 
}

	