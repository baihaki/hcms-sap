package com.abminvestama.hcms.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.entity.T535N;
import com.abminvestama.hcms.core.model.entity.T535NKey;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findByArt query method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface T535NRepository extends CrudRepository<T535N, T535NKey> {
	
	@Query("FROM T535N t535n WHERE t535n.id.art = :art AND t535n.id.title = :title")
	T535N findOneByCompositeKey(@Param("art") String art, @Param("title") String title);
	
	@Query("FROM T535N t535n WHERE t535n.id.art = :art ORDER BY t535n.id.title")
	Page<T535N> findByArt(@Param("art") String art, Pageable pageRequest);
}