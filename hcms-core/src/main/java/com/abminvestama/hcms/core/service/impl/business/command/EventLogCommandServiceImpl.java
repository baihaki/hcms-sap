package com.abminvestama.hcms.core.service.impl.business.command;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.constant.OperationType;
import com.abminvestama.hcms.core.model.entity.EventLog;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.EventLogRepository;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.util.MailService;

/**
 * 
 * @since 2.0.0
 * @version 2.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>2.0.2</td><td>Baihaki</td><td>Add: Send notification email to Admin(s) on Save</td></tr>
 *     <tr><td>2.0.1</td><td>Baihaki</td><td>Add: Send notification email on Save</td></tr>
 *     <tr><td>2.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("eventLogCommandService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class EventLogCommandServiceImpl implements EventLogCommandService {

	private EventLogRepository eventLogRepository;
	
	private MailService mailService;

	private UserQueryService userQueryService;
	
	@Autowired
    Environment env;
	
	@Autowired
	EventLogCommandServiceImpl(EventLogRepository eventLogRepository) {
		this.eventLogRepository = eventLogRepository;
	}
	
	@Autowired
	void setMailService(MailService mailService) {
		this.mailService = mailService;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Override
	public Optional<EventLog> save(EventLog entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setCreatedById(user.getId());
		}
		
		try {
			Optional<EventLog> savedEventLog = Optional.ofNullable(eventLogRepository.save(entity));
			if (savedEventLog.isPresent() && entity.isMailNotification()) {/*
			    mailService.sendMail(user.getEmail(), entity.getOperationType().toString().concat(" ").
			    		concat(entity.getHcmsEntity().getEntityName()),
			    		"Dear <b>".concat(user.getEmployee().getSname()).concat("</b><br><br>You have succesfully ").
			    		concat(entity.getOperationType().toString()).concat(" your ").concat(entity.getHcmsEntity().getEntityName()).
			    		concat(" data. Your HR Admin or Superior will review your submitted data prior to approval.<br><br>Thank You."));*/
				Map<String, String> composeMap = mailService.composeEmail(entity, user);
				if (StringUtils.isNotEmpty(composeMap.get("subject"))) {
				    mailService.sendMail(user.getEmail(), composeMap.get("subject"), composeMap.get("body"));
				    
				    // Send notification email to Admin(s), only for Master Data (IT*) transactions
				    if (entity.getHcmsEntity().getEntityName().startsWith("IT")) {
					    Optional<Collection<User>> users = userQueryService.fetchAdminByCompany(user.getEmployee().getT500p().getBukrs().getBukrs());
					    if (users.isPresent()) {
					    	List<User> adminUsers = new ArrayList<>();
					    	adminUsers = users.get().stream().collect(Collectors.toList());
					    	String mailBody = composeMap.get("body").replace("Dear <b>".concat(user.getEmployee().getSname()), "Dear <b>HR ADMIN")
					    			.replace("Your data request below", "User data request below")
					    			.replace("Your data update request below", "User data update request below")
					    			.replace(mailService.getMailMasterDataUpdate(), "has been created, waiting for your approval.")
					    			.replace("my_workflows", "tasks");

					    	for (User adminUser : adminUsers) {
					    		boolean sendNotification = true;
					    		String bukrs = user.getEmployee().getT500p().getBukrs().getBukrs();

								// check if there is customize workflow admin task
								if (env.getProperty("workflow.filter.admin.".concat(bukrs)) != null) {
									String filter = env.getProperty("workflow.filter.admin.".concat(bukrs));
									String[] params = filter.split(",");
									
									for (int i = 0; i < params.length; i++) {
										String[] subParams = params[i].split("\\.");
										Object object = user.getEmployee();
										Object adminObject = adminUser.getEmployee();
										for (int j=0; j < subParams.length; j++) {
											try {
												Method getMethod = object.getClass().getMethod(
														"get".concat(subParams[j]));
												object = getMethod.invoke(object);
												getMethod = adminObject.getClass().getMethod(
														"get".concat(subParams[j]));
												adminObject = getMethod.invoke(adminObject);
											} catch (Exception e) {
												e.printStackTrace();
											}
										}
										String value = (String) object;
										String adminValue = (String) adminObject;
										if (!adminValue.equals(value)) {
											sendNotification = false;
											break;
										}
									}
								}
						    	
					    		if (sendNotification)
					    		    mailService.sendMail(adminUser.getEmail(), composeMap.get("subject"), mailBody);
					    	}
					    }
				    }
				    
				}
			}
			return savedEventLog;
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist EventLog.Reason=" + e.getMessage());
		}						
	}

	@Override
	public boolean delete(EventLog entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		throw new NoSuchMethodException("Delete is prohibited. Cannot delete an EventLog!");		
	}

	@Override
	public boolean restore(EventLog entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		throw new NoSuchMethodException("Invalid Operation.No EventLog to be restored!");				
	}
}