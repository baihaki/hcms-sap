package com.abminvestama.hcms.core.service.api.business.command;

import java.util.List;

import com.abminvestama.hcms.core.model.entity.TrxApprovalProcess;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;

/**
 * 
 * @since 3.0.0
 * @version 3.0.0
 * @author wijanarko (wijanarko@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface TrxApprovalProcessCommandService extends DatabaseCommandService<TrxApprovalProcess> {
	boolean deleteList(List<TrxApprovalProcess> entities);
}
