package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0185;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0185SoapObject {
	
	private String employeenumber;
	private String validitybegin;
	private String validityend;
	private String ictyp;
	private String icnum;
	private String auth1;
	private String fpdat;
	private String expid;
	private String isspl;
	private String iscot;
	
	private IT0185SoapObject() {}
	
	public IT0185SoapObject(IT0185 object) {
		if (object == null) {
			new IT0185SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			ictyp = object.getIctyp().getIctyp();
			icnum = object.getIcnum();
			auth1 = object.getAuth1();
			fpdat = CommonDateFunction.convertDateToStringYMD(object.getFpdat());
			expid = CommonDateFunction.convertDateToStringYMD(object.getExpid());
			isspl = object.getIsspl();
			iscot = object.getIscot().getLand1();
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}

	public String getIctyp() {
		return ictyp;
	}

	public String getIcnum() {
		return icnum;
	}

	public String getAuth1() {
		return auth1;
	}

	public String getFpdat() {
		return fpdat;
	}

	public String getExpid() {
		return expid;
	}

	public String getIsspl() {
		return isspl;
	}

	public String getIscot() {
		return iscot;
	}	

}
