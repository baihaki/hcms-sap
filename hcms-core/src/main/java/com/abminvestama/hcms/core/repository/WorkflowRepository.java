package com.abminvestama.hcms.core.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.abminvestama.hcms.core.model.entity.Workflow;

/**
 * 
 * @author yauritux@gmail.com
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public interface WorkflowRepository extends PagingAndSortingRepository<Workflow, String> {

	Workflow findByName(String name);
	Page<Workflow> findAll(Pageable pageRequest);
}