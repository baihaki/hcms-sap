package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * Class that represents <strong>GL Order</strong> object.
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 3.0.0 (Cash Advance)
 * @since 3.0.0
 *
 */
@Entity
@Table(name = "ca_order")
public class CaOrder implements Serializable {
	
	private static final long serialVersionUID = 772126009460034804L;
	
	@Id
	@Column(name = "id", nullable = false)
	private Long id;
	
	@Column(name = "description")
	private String description;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "kokrs", referencedColumnName = "kokrs"),
		@JoinColumn(name = "kostl", referencedColumnName = "kostl")
	})
	private CSKT cskt;
	
	@ManyToOne
	@JoinColumn(name = "gl_account_id", referencedColumnName = "id")
	private CaGlAccount glAccount;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "begda")
	private Date begda;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "endda")
	private Date endda; 
	
	public void setId(Long id){
		this.id = id;
	}
 
	public String getDescription(){
		return description;
	}
	
	public void setDescription(String description){
		this.description = description;
	}
 
	/**
	 * GET Departments Cost Center.
	 * 
	 * @return {@link CSKT} object
	 */
	public CSKT getCskt(){
		return cskt;
	}

	/**
	 * SET Departments Cost Center.
	 * 
	 * @param cskt The Departments Cost Center object
	 */
	public void setCskt(CSKT cskt){
		this.cskt = cskt;
	}
 
	/**
	 * GET GL Account.
	 * 
	 * @return {@link CaGlAccount} object
	 */
	public CaGlAccount getGlAccount(){
		return glAccount;
	}
	
	/**
	 * SET GL Account.
	 * 
	 * @param glAccount the GL Account object
	 */
	public void setGlAccount(CaGlAccount glAccount){
		this.glAccount = glAccount;
	}
 
	public Date getBegda(){
		return begda;
	}
	
	public void setBegda(Date begda){
		this.begda = begda;
	}
 
	public Date getEndda(){
		return endda;
	}
	
	public void setEndda(Date endda){
		this.endda = endda;
	}
	
	public CaOrder(){
	}

	public CaOrder( Long id,  String description,  CSKT cskt,  CaGlAccount glAccount,  Date begda,  Date endda ){
	 	this.id = id;
	 	this.description = description;
	 	this.cskt = cskt;
	 	this.glAccount = glAccount;
	 	this.begda = begda;
	 	this.endda = endda;
	}
	
	public Long getId() {
		return  id;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		CaOrder caOrder = (CaOrder) o;
		 
		if (id != null ? !id.equals(caOrder.id) : caOrder.id != null) return false; 
		if (description != null ? !description.equals(caOrder.description) : caOrder.description != null) return false; 
		if (cskt != null ? !cskt.equals(caOrder.cskt) : caOrder.cskt != null) return false; 
		if (glAccount != null ? !glAccount.equals(caOrder.glAccount) : caOrder.glAccount != null) return false; 
		if (begda != null ? !begda.equals(caOrder.begda) : caOrder.begda != null) return false; 
		if (endda != null ? !endda.equals(caOrder.endda) : caOrder.endda != null) return false;
		return true; 
	}
	
	@Override
	public int hashCode() {
	    int result = (id != null ?  id.hashCode() : 0); 
		result = 31 * (description != null ?  description.hashCode() : 0); 
		result = 31 * (cskt != null ?  cskt.hashCode() : 0); 
		result = 31 * (glAccount != null ?  glAccount.hashCode() : 0); 
		result = 31 * (begda != null ?  begda.hashCode() : 0); 
		result = 31 * (endda != null ?  endda.hashCode() : 0); 
	 
		return result; 
	}
	
	@Override
	public String toString() {
		return "caOrder(id="+id+",description="+description+",deptsCostCenterId="+cskt+",glAccountId="+glAccount+",begda="+begda+",endda="+endda+')'; 
	}
 
}

	