package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.TrxApprovalProcess;
import com.abminvestama.hcms.core.repository.TrxApprovalProcessRepository;
import com.abminvestama.hcms.core.service.api.business.query.TrxApprovalProcessQueryService;

@Service("trxApprovalProcessService")
@Transactional(readOnly = true)
public class TrxApprovalProcessQueryServiceImpl implements TrxApprovalProcessQueryService {
	
	private TrxApprovalProcessRepository trxApprovalProcessRepository;
	
	@Autowired
	TrxApprovalProcessQueryServiceImpl(TrxApprovalProcessRepository trxApprovalProcessRepository) {
		this.trxApprovalProcessRepository = trxApprovalProcessRepository;
	}

	@Override
	public Optional<TrxApprovalProcess> findById(Optional<String> id) throws Exception {
		return id.map(pk -> trxApprovalProcessRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<TrxApprovalProcess>> fetchAll() {
		List<TrxApprovalProcess> listOfTrxApprovalProcess = new ArrayList<>();
		Optional<Iterable<TrxApprovalProcess>> bunchOfApprovalProcess = Optional
				.ofNullable(trxApprovalProcessRepository.findAll());
		return bunchOfApprovalProcess.map(iter -> {
			iter.forEach(item -> {
				listOfTrxApprovalProcess.add(item);
			});
			return listOfTrxApprovalProcess;
		});
	}

	@Override
	public Collection<TrxApprovalProcess> fetchByEntityObjectId(String entityObjectId, String orderBy) {
		if (entityObjectId == null) return Collections.emptyList();
		if (orderBy == null || orderBy.isEmpty()) orderBy = "seq ASC";
		Optional<Collection<TrxApprovalProcess>> bunchOfTrxApprovalProcess 
			= Optional.ofNullable(trxApprovalProcessRepository.fetchByEntityObjectId(entityObjectId, orderBy));
		return (bunchOfTrxApprovalProcess.isPresent() 
				? bunchOfTrxApprovalProcess.get().stream().collect(Collectors.toList())
				: Collections.emptyList());
	}
}
