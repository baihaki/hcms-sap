package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.CaStlDtl;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaStlDtlQueryService extends DatabaseQueryService<CaStlDtl, String>, DatabasePaginationQueryService {
	
	Page<CaStlDtl> fetchAllWithPaging(int pageNumber);
	
	Optional<CaStlDtl> findByDetailId(String detailId);
	
	Collection<CaStlDtl> fetchByReqno(String reqNo);
	
	Optional<CaStlDtl> findOneByReqnoAndItemno(String reqNo, Integer itemNo);
}
