package com.abminvestama.hcms.core.service.helper;

import com.abminvestama.hcms.common.util.CommonDateFunction;
import com.abminvestama.hcms.core.model.entity.IT0077;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 2.0.0
 * @since 2.0.0
 *
 */
public class IT0077SoapObject {
	
	private String employeenumber;
	private String validitybegin;
	private String validityend;
	private String seqnr;
	private String racky;
	private String molga;


	private IT0077SoapObject() {}
	
	public IT0077SoapObject(IT0077 object) {
		if (object == null) {
			new IT0077SoapObject();
		} else {
			employeenumber = object.getId().getPernr().toString();
			validitybegin = CommonDateFunction.convertDateToStringYMD(object.getId().getBegda());
			validityend = CommonDateFunction.convertDateToStringYMD(object.getId().getEndda());
			seqnr = object.getSeqnr().toString();
			racky = object.getRacky().getId().getRacky();
			molga = object.getRacky().getId().getMolga();
		}		
	}

	public String getEmployeenumber() {
		return employeenumber;
	}
	
	public String getValiditybegin() {
		return validitybegin;
	}

	public String getValidityend() {
		return validityend;
	}

	
	public String getSeqnr() {
		return seqnr;
	}

	public String getRacky() {
		return racky;
	}

	public String getMolga() {
		return molga;
	}

}
