package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.constant.BAPIFunction;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.IT0009Repository;
import com.abminvestama.hcms.core.service.api.CommonServiceFactory;
import com.abminvestama.hcms.core.service.api.business.command.IT0009CommandService;
import com.abminvestama.hcms.core.service.helper.IT0009Mapper;
import com.abminvestama.hcms.core.service.helper.MasterDataSoapObject;
import com.abminvestama.hcms.core.service.util.MuleConsumerService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.1
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add saveListFromMulesoft method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("it0009CommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class IT0009CommandServiceImpl implements IT0009CommandService {
	
	private IT0009Repository it0009Repository;
	
	@Autowired
	IT0009CommandServiceImpl(IT0009Repository it0009Repository) {
		this.it0009Repository = it0009Repository;
	}

	@Override
	public Optional<IT0009> save(IT0009 entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setUname(user.getId());
		}
		
		try {
			return Optional.ofNullable(it0009Repository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist IT0009 (Bank Details).Reason=" + e.getMessage());
		}		
	}

	@Override
	public boolean delete(IT0009 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete at this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");
	}

	@Override
	public boolean restore(IT0009 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}
	
	@Override
	public String saveListFromMulesoft(MuleConsumerService muleConsumerService, CommonServiceFactory commonServiceFactory, Long pernr, User user)
			throws ConstraintViolationException, CannotPersistException, NoSuchMethodException {
		String result = "";
		MasterDataSoapObject masterDataSoapObject = new MasterDataSoapObject(null, null, pernr.toString());
		
		List<Map<String, String>> listIT = muleConsumerService.getFromSAP(BAPIFunction.INFOTYPE_FETCH.service(),
				BAPIFunction.INFOTYPE_FETCH_BANK_URI.service(), masterDataSoapObject);
		if (!listIT.isEmpty()) {
			Set<String> setIT = new HashSet<String>();
			for (int it = 0; it < listIT.size(); it++) {
				if (listIT.get(it).get("field1") != null && Long.valueOf(listIT.get(it).get("field1")).longValue() == pernr.longValue()) {
					IT0009 it0009 = new IT0009Mapper(listIT.get(it), commonServiceFactory).getIT0009();
					String key = it0009.getId().toStringId();
					if (setIT.contains(key)) {
						// duplicate key, increment begda based on seqnr (SAP objectid)
						Calendar cal = Calendar.getInstance();
						cal.setTime(it0009.getId().getBegda());
						int objectId = it0009.getSeqnr().intValue();
						cal.add(Calendar.DATE, objectId);
						ITCompositeKeys newId = new ITCompositeKeys(it0009.getId().getPernr(), it0009.getId().getInfty(),
								it0009.getId().getSubty(), it0009.getId().getEndda(), cal.getTime());
						it0009.setId(newId);
					} else {
						setIT.add(key);
					}
					Optional<IT0009> savedITEntity = save(it0009, user);
					if (!savedITEntity.isPresent()) {
						result = result.concat("Failed to save IT0009 object: ".concat(it0009.getId().toStringId()).concat(". "));
					}
				}
			}
		}
	
		return result;
	}
	
}