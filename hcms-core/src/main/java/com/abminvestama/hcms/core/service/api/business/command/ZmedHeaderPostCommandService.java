package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.ZmedHeaderPost;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;


public interface ZmedHeaderPostCommandService extends DatabaseCommandService<ZmedHeaderPost> {
}