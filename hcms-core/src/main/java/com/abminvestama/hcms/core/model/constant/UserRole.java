package com.abminvestama.hcms.core.model.constant;

/**
 * 
 * @author yauritux@gmail.com
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public enum UserRole {

	ROLE_EMPLOYEE(AccessRole.EMPLOYEE_GROUP, "Employee Role", "Role for Employee"),
	ROLE_ADMIN(AccessRole.ADMIN_GROUP, "Admin Role", "Role for Admin"),
	ROLE_PUBLIC(AccessRole.PUBLIC_GROUP, "Public Role", "Role for Public"),
	ROLE_USER(AccessRole.USER_GROUP, "User Role", "Role for User"),
	ROLE_SUPERIOR(AccessRole.SUPERIOR_GROUP, "Superior Role", "Role for Superior"),
	ROLE_HEAD(AccessRole.HEAD_GROUP, "Head Role", "Role for Department Head"),
	ROLE_DIRECTOR(AccessRole.DIRECTOR_GROUP, "Director Role", "Role for Director"),
	ROLE_GA(AccessRole.GA_GROUP, "GA Role", "Role for GA"),
	ROLE_POOLING(AccessRole.POOLING_GROUP, "Pooling Role", "Role for Pooling"),
	ROLE_ACCOUNTING(AccessRole.ACCOUNTING_GROUP, "Accounting Role", "Role for Accounting"),
	ROLE_TREASURY(AccessRole.TREASURY_GROUP, "Treasury Role", "Role for Treasury");
	
	public final String roleIdent;
	public final String roleName;
	public final String description;
	
	private UserRole(String roleIdent, String roleName, String description) {
		this.roleIdent = roleIdent;
		this.roleName = roleName;
		this.description = description;
	}
		
	@Override
	public String toString() {
		return this.roleIdent;
	}
	
	public static class AccessRole {
		public static final String EMPLOYEE_GROUP = "ROLE_EMPLOYEE";
		public static final String ADMIN_GROUP = "ROLE_ADMIN";
		public static final String PUBLIC_GROUP = "ROLE_PUBLIC";
		public static final String USER_GROUP = "ROLE_USER";
		public static final String SUPERIOR_GROUP = "ROLE_SUPERIOR";
		
		public static final String HEAD_GROUP = "ROLE_HEAD";
		public static final String DIRECTOR_GROUP = "ROLE_DIRECTOR";
		public static final String GA_GROUP = "ROLE_GA";
		public static final String POOLING_GROUP = "ROLE_POOLING";
		public static final String ACCOUNTING_GROUP = "ROLE_ACCOUNTING";
		public static final String TREASURY_GROUP = "ROLE_TREASURY";
	}
}