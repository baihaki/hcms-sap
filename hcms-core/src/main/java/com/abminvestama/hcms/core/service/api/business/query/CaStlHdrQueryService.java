package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Optional;

import org.springframework.data.domain.Page;

import com.abminvestama.hcms.core.model.entity.CaStlHdr;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface CaStlHdrQueryService extends DatabaseQueryService<CaStlHdr, String>, DatabasePaginationQueryService {
	
	Optional<CaStlHdr> findByReqno(String reqNo);

	Optional<CaStlHdr> findByTcano(String tcaNo);

	Page<CaStlHdr> findByPernr(Long pernr, int pageNumber);

	Page<CaStlHdr> fetchAllWithPaging(int pageNumber);
	
	Page<CaStlHdr> findAllByCriteriaWithPernrs(Long[] pernr, Integer process, int pageNumber);

	Page<CaStlHdr> findAllByCriteriaWithProcesses(Long pernr, Integer[] processes, int pageNumber);

	Page<CaStlHdr> findAllByCriteria(Long pernr, Integer process, int pageNumber);

	Page<CaStlHdr> findSubOrdinatesByCriteria(Long pernr, Integer process, String toPositionId, int pageNumber);
}
