package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Immutable;

/**
 * 
 * @author baihaki (baihaki.pru@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Embeddable
@Immutable
public class SubT706B1Key implements Serializable {
	
	private static final long serialVersionUID = -5357266260007381724L;
	
	private String morei;
	private String spkzl;
	
	@Column(name = "sub_spkzl", nullable = false)
	private String subSpkzl;
	
	public SubT706B1Key() {}
	
	public SubT706B1Key(String morei, String spkzl, String subSpkzl) {
		this.morei = morei;
		this.spkzl = spkzl;
		this.subSpkzl = subSpkzl;
	}
	
	public String getMorei() {
		return morei;
	}
	
	public String getSpkzl() {
		return spkzl;
	}
	
	public String getSubSpkzl() {
		return subSpkzl;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final SubT706B1Key key = (SubT706B1Key) o;
		
		if (key.getMorei() != null ? !key.getMorei().equalsIgnoreCase(morei) : morei != null) {
			return false;
		}
		
		if (key.getSpkzl() != null ? !key.getSpkzl().equalsIgnoreCase(spkzl) : spkzl != null) {
			return false;
		}
		
		if (key.getSubSpkzl() != null ? !key.getSubSpkzl().equalsIgnoreCase(subSpkzl) : subSpkzl != null) {
			return false;
		}
		
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.morei != null ? this.morei.hashCode() : 0;
		result = result * 31 + (this.spkzl != null ? this.spkzl.hashCode() : 0);
		result = result * 31 + (this.subSpkzl != null ? this.subSpkzl.hashCode() : 0);
		return result;
	}
}