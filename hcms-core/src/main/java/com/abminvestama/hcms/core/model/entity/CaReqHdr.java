package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 * 
 * Class that represents <strong>Cash Advance Request Header</strong> transaction.
 * 
 * @since 3.0.0
 * @version 3.0.0 (Cash Advance)
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>3.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
@Entity
@Table(name = "ca_req_hdr")
public class CaReqHdr implements Serializable {
	
	private static final long serialVersionUID = -6483188908959360439L;

	@Id
	@Column(name = "req_no", nullable = true, insertable = false, updatable = false, length = 13)
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE, 
        generator = "assigned-sequence"
    )
    @GenericGenerator(
        name = "assigned-sequence", 
        strategy = "com.abminvestama.hcms.core.model.entity.PostgreUUIDGenerationStrategy",
        parameters = @Parameter(
            name = "sequence_name",
            value = "hibernate_sequence"
        )
    )
	private String reqNo;
	
	@Column(name = "pernr")
	private Long pernr;
	
	@Column(name = "amount")
	private Double amount;
	
	@Column(name = "currency")
	private String currency;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "attachment")
	private String attachment;
	
	@Column(name = "bank_name")
	private String bankName;
	
	@Column(name = "bank_account")
	private String bankAccount;
	
	@Column(name = "process")
	private Integer process;
	
	@Column(name = "notes")
	private String notes;
	
	@Column(name = "advance_code")
	private String advanceCode;
	
	@ManyToOne
	@JoinColumn(name = "bukrs", referencedColumnName = "bukrs", nullable = false)
	private T001 bukrs;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	private Date createdAt;
	
	@ManyToOne
	@JoinColumn(name = "created_by", referencedColumnName = "id")
	private User createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_at")
	private Date updatedAt;
	
	@ManyToOne
	@JoinColumn(name = "updated_by", referencedColumnName = "id")
	private User updatedBy;
	
	@ManyToOne
	@JoinColumn(name = "approved1_by", referencedColumnName = "id")
	private User approved1By;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved1_at")
	private Date approved1At;
	
	@ManyToOne
	@JoinColumn(name = "approved2_by", referencedColumnName = "id")
	private User approved2By;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved2_at")
	private Date approved2At;
	
	@ManyToOne
	@JoinColumn(name = "approved3_by", referencedColumnName = "id")
	private User approved3By;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved3_at")
	private Date approved3At;
	
	@ManyToOne
	@JoinColumn(name = "approved4_by", referencedColumnName = "id")
	private User approved4By;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved4_at")
	private Date approved4At;
	
	@ManyToOne
	@JoinColumn(name = "approved_accounting_by", referencedColumnName = "id")
	private User approvedAccountingBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved_accounting_at")
	private Date approvedAccountingAt;
	
	@ManyToOne
	@JoinColumn(name = "approved_treasury_by", referencedColumnName = "id")
	private User approvedTreasuryBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved_treasury_at")
	private Date approvedTreasuryAt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "received_at")
	private Date receivedAt;
	
	@Column(name = "acct_ap")
	private String acctAp;
	
	@Column(name = "reason")
	private String reason;
	
	@Column(name = "payment_status")
	private String paymentStatus;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "payment_date")
	private Date paymentDate;
	
	@Column(name = "docno")
	private String docno;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_sync")
	private Date lastSync;
	
	@Column(name = "payment_status2")
	private String paymentStatus2;
	
	@Column(name = "docno2")
	private String docno2;
	
	@Column(name = "sap_message")
	private String sapMessage;

 
	public String getReqNo(){
		return reqNo;
	}
	
	public void setReqNo(String reqNo){
		this.reqNo = reqNo;
	}
 
	public Long getPernr(){
		return pernr;
	}
	
	public void setPernr(Long pernr){
		this.pernr = pernr;
	}
 
	public Double getAmount(){
		return amount;
	}
	
	public void setAmount(Double amount){
		this.amount = amount;
	}
 
	public String getCurrency(){
		return currency;
	}
	
	public void setCurrency(String currency){
		this.currency = currency;
	}
 
	public String getDescription(){
		return description;
	}
	
	public void setDescription(String description){
		this.description = description;
	}
 
	public String getAttachment(){
		return attachment;
	}
	
	public void setAttachment(String attachment){
		this.attachment = attachment;
	}
 
	public String getBankName(){
		return bankName;
	}
	
	public void setBankName(String bankName){
		this.bankName = bankName;
	}
 
	public String getBankAccount(){
		return bankAccount;
	}
	
	public void setBankAccount(String bankAccount){
		this.bankAccount = bankAccount;
	}
 
	public Integer getProcess(){
		return process;
	}
	
	public void setProcess(Integer process){
		this.process = process;
	}
 
	public String getNotes(){
		return notes;
	}
	
	public void setNotes(String notes){
		this.notes = notes;
	}
 
	public String getAdvanceCode(){
		return advanceCode;
	}
	
	public void setAdvanceCode(String advanceCode){
		this.advanceCode = advanceCode;
	}
 
	public T001 getBukrs(){
		return bukrs;
	}
	
	public void setBukrs(T001 bukrs){
		this.bukrs = bukrs;
	}
 
	public Date getCreatedAt(){
		return createdAt;
	}
	
	public void setCreatedAt(Date createdAt){
		this.createdAt = createdAt;
	}
 
	public User getCreatedBy(){
		return createdBy;
	}
	
	public void setCreatedBy(User createdBy){
		this.createdBy = createdBy;
	}
 
	public Date getUpdatedAt(){
		return updatedAt;
	}
	
	public void setUpdatedAt(Date updatedAt){
		this.updatedAt = updatedAt;
	}
 
	public User getUpdatedBy(){
		return updatedBy;
	}
	
	public void setUpdatedBy(User updatedBy){
		this.updatedBy = updatedBy;
	}
 
	public User getApproved1By(){
		return approved1By;
	}
	
	public void setApproved1By(User approved1By){
		this.approved1By = approved1By;
	}
 
	public Date getApproved1At(){
		return approved1At;
	}
	
	public void setApproved1At(Date approved1At){
		this.approved1At = approved1At;
	}
 
	public User getApproved2By(){
		return approved2By;
	}
	
	public void setApproved2By(User approved2By){
		this.approved2By = approved2By;
	}
 
	public Date getApproved2At(){
		return approved2At;
	}
	
	public void setApproved2At(Date approved2At){
		this.approved2At = approved2At;
	}
 
	public User getApproved3By(){
		return approved3By;
	}
	
	public void setApproved3By(User approved3By){
		this.approved3By = approved3By;
	}
 
	public Date getApproved3At(){
		return approved3At;
	}
	
	public void setApproved3At(Date approved3At){
		this.approved3At = approved3At;
	}
 
	public User getApproved4By(){
		return approved4By;
	}
	
	public void setApproved4By(User approved4By){
		this.approved4By = approved4By;
	}
 
	public Date getApproved4At(){
		return approved4At;
	}
	
	public void setApproved4At(Date approved4At){
		this.approved4At = approved4At;
	}
 
	public User getApprovedAccountingBy(){
		return approvedAccountingBy;
	}
	
	public void setApprovedAccountingBy(User approvedAccountingBy){
		this.approvedAccountingBy = approvedAccountingBy;
	}
 
	public Date getApprovedAccountingAt(){
		return approvedAccountingAt;
	}
	
	public void setApprovedAccountingAt(Date approvedAccountingAt){
		this.approvedAccountingAt = approvedAccountingAt;
	}
 
	public User getApprovedTreasuryBy(){
		return approvedTreasuryBy;
	}
	
	public void setApprovedTreasuryBy(User approvedTreasuryBy){
		this.approvedTreasuryBy = approvedTreasuryBy;
	}
 
	public Date getApprovedTreasuryAt(){
		return approvedTreasuryAt;
	}
	
	public void setApprovedTreasuryAt(Date approvedTreasuryAt){
		this.approvedTreasuryAt = approvedTreasuryAt;
	}
 
	public Date getReceivedAt(){
		return receivedAt;
	}
	
	public void setReceivedAt(Date receivedAt){
		this.receivedAt = receivedAt;
	}
 
	public String getAcctAp(){
		return acctAp;
	}
	
	public void setAcctAp(String acctAp){
		this.acctAp = acctAp;
	}
 
	public String getReason(){
		return reason;
	}
	
	public void setReason(String reason){
		this.reason = reason;
	}
 
	public String getPaymentStatus(){
		return paymentStatus;
	}
	
	public void setPaymentStatus(String paymentStatus){
		this.paymentStatus = paymentStatus;
	}
 
	public Date getPaymentDate(){
		return paymentDate;
	}
	
	public void setPaymentDate(Date paymentDate){
		this.paymentDate = paymentDate;
	}
 
	public String getDocno(){
		return docno;
	}
	
	public void setDocno(String docno){
		this.docno = docno;
	}
 
	public Date getLastSync(){
		return lastSync;
	}
	
	public void setLastSync(Date lastSync){
		this.lastSync = lastSync;
	}
 
	public String getPaymentStatus2(){
		return paymentStatus2;
	}
	
	public void setPaymentStatus2(String paymentStatus2){
		this.paymentStatus2 = paymentStatus2;
	}
 
	public String getDocno2(){
		return docno2;
	}
	
	public void setDocno2(String docno2){
		this.docno2 = docno2;
	}
 
	public String getSapMessage(){
		return sapMessage;
	}
	
	public void setSapMessage(String sapMessage){
		this.sapMessage = sapMessage;
	}
	
	public CaReqHdr(){
	}

	public CaReqHdr( String reqNo,  Long pernr,  Double amount,  String currency,  String description,  String attachment,
			String bankName,  String bankAccount,  Integer process,  String notes,  String advanceCode,  T001 bukrs,
			Date createdAt,  User createdBy,  Date updatedAt,  User updatedBy,  User approved1By,  Date approved1At,
			User approved2By,  Date approved2At,  User approved3By,  Date approved3At,  User approved4By,  Date approved4At,
			User approvedAccountingBy,  Date approvedAccountingAt,  User approvedTreasuryBy,  Date approvedTreasuryAt,
			Date receivedAt,  String acctAp,  String reason,  String paymentStatus,  Date paymentDate,  String docno,
			Date lastSync,  String paymentStatus2,  String docno2,  String sapMessage ){
	 	this.reqNo = reqNo;
	 	this.pernr = pernr;
	 	this.amount = amount;
	 	this.currency = currency;
	 	this.description = description;
	 	this.attachment = attachment;
	 	this.bankName = bankName;
	 	this.bankAccount = bankAccount;
	 	this.process = process;
	 	this.notes = notes;
	 	this.advanceCode = advanceCode;
	 	this.bukrs = bukrs;
	 	this.createdAt = createdAt;
	 	this.createdBy = createdBy;
	 	this.updatedAt = updatedAt;
	 	this.updatedBy = updatedBy;
	 	this.approved1By = approved1By;
	 	this.approved1At = approved1At;
	 	this.approved2By = approved2By;
	 	this.approved2At = approved2At;
	 	this.approved3By = approved3By;
	 	this.approved3At = approved3At;
	 	this.approved4By = approved4By;
	 	this.approved4At = approved4At;
	 	this.approvedAccountingBy = approvedAccountingBy;
	 	this.approvedAccountingAt = approvedAccountingAt;
	 	this.approvedTreasuryBy = approvedTreasuryBy;
	 	this.approvedTreasuryAt = approvedTreasuryAt;
	 	this.receivedAt = receivedAt;
	 	this.acctAp = acctAp;
	 	this.reason = reason;
	 	this.paymentStatus = paymentStatus;
	 	this.paymentDate = paymentDate;
	 	this.docno = docno;
	 	this.lastSync = lastSync;
	 	this.paymentStatus2 = paymentStatus2;
	 	this.docno2 = docno2;
	 	this.sapMessage = sapMessage;
	}
	
	@PrePersist
	final void prePersist() {
		this.createdAt = this.updatedAt = new Date();
	}
		
	@PreUpdate
	final void preUpdate() {
		this.updatedAt = new Date();
	}

	public String getId() {
		return  reqNo;
	}
		
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		CaReqHdr caReqHdr = (CaReqHdr) o;
		 
		if (reqNo != null ? !reqNo.equals(caReqHdr.reqNo) : caReqHdr.reqNo != null) return false; 
		if (pernr != null ? !pernr.equals(caReqHdr.pernr) : caReqHdr.pernr != null) return false; 
		if (amount != null ? !amount.equals(caReqHdr.amount) : caReqHdr.amount != null) return false; 
		if (currency != null ? !currency.equals(caReqHdr.currency) : caReqHdr.currency != null) return false; 
		if (description != null ? !description.equals(caReqHdr.description) : caReqHdr.description != null) return false; 
		if (attachment != null ? !attachment.equals(caReqHdr.attachment) : caReqHdr.attachment != null) return false; 
		if (bankName != null ? !bankName.equals(caReqHdr.bankName) : caReqHdr.bankName != null) return false; 
		if (bankAccount != null ? !bankAccount.equals(caReqHdr.bankAccount) : caReqHdr.bankAccount != null) return false; 
		if (process != caReqHdr.process) return false;  
		if (notes != null ? !notes.equals(caReqHdr.notes) : caReqHdr.notes != null) return false; 
		if (advanceCode != null ? !advanceCode.equals(caReqHdr.advanceCode) : caReqHdr.advanceCode != null) return false; 
		if (bukrs != null ? !bukrs.equals(caReqHdr.bukrs) : caReqHdr.bukrs != null) return false; 
		if (createdAt != null ? !createdAt.equals(caReqHdr.createdAt) : caReqHdr.createdAt != null) return false; 
		if (createdBy != null ? !createdBy.equals(caReqHdr.createdBy) : caReqHdr.createdBy != null) return false; 
		if (updatedAt != null ? !updatedAt.equals(caReqHdr.updatedAt) : caReqHdr.updatedAt != null) return false; 
		if (updatedBy != null ? !updatedBy.equals(caReqHdr.updatedBy) : caReqHdr.updatedBy != null) return false; 
		if (approved1By != null ? !approved1By.equals(caReqHdr.approved1By) : caReqHdr.approved1By != null) return false; 
		if (approved1At != null ? !approved1At.equals(caReqHdr.approved1At) : caReqHdr.approved1At != null) return false; 
		if (approved2By != null ? !approved2By.equals(caReqHdr.approved2By) : caReqHdr.approved2By != null) return false; 
		if (approved2At != null ? !approved2At.equals(caReqHdr.approved2At) : caReqHdr.approved2At != null) return false; 
		if (approved3By != null ? !approved3By.equals(caReqHdr.approved3By) : caReqHdr.approved3By != null) return false; 
		if (approved3At != null ? !approved3At.equals(caReqHdr.approved3At) : caReqHdr.approved3At != null) return false; 
		if (approved4By != null ? !approved4By.equals(caReqHdr.approved4By) : caReqHdr.approved4By != null) return false; 
		if (approved4At != null ? !approved4At.equals(caReqHdr.approved4At) : caReqHdr.approved4At != null) return false; 
		if (approvedAccountingBy != null ? !approvedAccountingBy.equals(caReqHdr.approvedAccountingBy) : caReqHdr.approvedAccountingBy != null) return false; 
		if (approvedAccountingAt != null ? !approvedAccountingAt.equals(caReqHdr.approvedAccountingAt) : caReqHdr.approvedAccountingAt != null) return false; 
		if (approvedTreasuryBy != null ? !approvedTreasuryBy.equals(caReqHdr.approvedTreasuryBy) : caReqHdr.approvedTreasuryBy != null) return false; 
		if (approvedTreasuryAt != null ? !approvedTreasuryAt.equals(caReqHdr.approvedTreasuryAt) : caReqHdr.approvedTreasuryAt != null) return false; 
		if (receivedAt != null ? !receivedAt.equals(caReqHdr.receivedAt) : caReqHdr.receivedAt != null) return false; 
		if (acctAp != null ? !acctAp.equals(caReqHdr.acctAp) : caReqHdr.acctAp != null) return false; 
		if (reason != null ? !reason.equals(caReqHdr.reason) : caReqHdr.reason != null) return false; 
		if (paymentStatus != null ? !paymentStatus.equals(caReqHdr.paymentStatus) : caReqHdr.paymentStatus != null) return false; 
		if (paymentDate != null ? !paymentDate.equals(caReqHdr.paymentDate) : caReqHdr.paymentDate != null) return false; 
		if (docno != null ? !docno.equals(caReqHdr.docno) : caReqHdr.docno != null) return false; 
		if (lastSync != null ? !lastSync.equals(caReqHdr.lastSync) : caReqHdr.lastSync != null) return false; 
		if (paymentStatus2 != null ? !paymentStatus2.equals(caReqHdr.paymentStatus2) : caReqHdr.paymentStatus2 != null) return false; 
		if (docno2 != null ? !docno2.equals(caReqHdr.docno2) : caReqHdr.docno2 != null) return false; 
		if (sapMessage != null ? !sapMessage.equals(caReqHdr.sapMessage) : caReqHdr.sapMessage != null) return false;
		return true; 
	}
	
	@Override
	public int hashCode() {
	    int result = (reqNo != null ?  reqNo.hashCode() : 0); 
		result = 31 * (pernr != null ?  pernr.hashCode() : 0); 
		result = 31 * (amount != null ?  amount.hashCode() : 0); 
		result = 31 * (currency != null ?  currency.hashCode() : 0); 
		result = 31 * (description != null ?  description.hashCode() : 0); 
		result = 31 * (attachment != null ?  attachment.hashCode() : 0); 
		result = 31 * (bankName != null ?  bankName.hashCode() : 0); 
		result = 31 * (bankAccount != null ?  bankAccount.hashCode() : 0); 
		result = 31 * result + process; 	result = 31 * (notes != null ?  notes.hashCode() : 0); 
		result = 31 * (advanceCode != null ?  advanceCode.hashCode() : 0); 
		result = 31 * (bukrs != null ?  bukrs.hashCode() : 0); 
		result = 31 * (createdAt != null ?  createdAt.hashCode() : 0); 
		result = 31 * (createdBy != null ?  createdBy.hashCode() : 0); 
		result = 31 * (updatedAt != null ?  updatedAt.hashCode() : 0); 
		result = 31 * (updatedBy != null ?  updatedBy.hashCode() : 0); 
		result = 31 * (approved1By != null ?  approved1By.hashCode() : 0); 
		result = 31 * (approved1At != null ?  approved1At.hashCode() : 0); 
		result = 31 * (approved2By != null ?  approved2By.hashCode() : 0); 
		result = 31 * (approved2At != null ?  approved2At.hashCode() : 0); 
		result = 31 * (approved3By != null ?  approved3By.hashCode() : 0); 
		result = 31 * (approved3At != null ?  approved3At.hashCode() : 0); 
		result = 31 * (approved4By != null ?  approved4By.hashCode() : 0); 
		result = 31 * (approved4At != null ?  approved4At.hashCode() : 0); 
		result = 31 * (approvedAccountingBy != null ?  approvedAccountingBy.hashCode() : 0); 
		result = 31 * (approvedAccountingAt != null ?  approvedAccountingAt.hashCode() : 0); 
		result = 31 * (approvedTreasuryBy != null ?  approvedTreasuryBy.hashCode() : 0); 
		result = 31 * (approvedTreasuryAt != null ?  approvedTreasuryAt.hashCode() : 0); 
		result = 31 * (receivedAt != null ?  receivedAt.hashCode() : 0); 
		result = 31 * (acctAp != null ?  acctAp.hashCode() : 0); 
		result = 31 * (reason != null ?  reason.hashCode() : 0); 
		result = 31 * (paymentStatus != null ?  paymentStatus.hashCode() : 0); 
		result = 31 * (paymentDate != null ?  paymentDate.hashCode() : 0); 
		result = 31 * (docno != null ?  docno.hashCode() : 0); 
		result = 31 * (lastSync != null ?  lastSync.hashCode() : 0); 
		result = 31 * (paymentStatus2 != null ?  paymentStatus2.hashCode() : 0); 
		result = 31 * (docno2 != null ?  docno2.hashCode() : 0); 
		result = 31 * (sapMessage != null ?  sapMessage.hashCode() : 0); 
	 
		return result; 
	}
	
	@Override
	public String toString() {
		return "caReqHdr(reqNo="+reqNo+",pernr="+pernr+",amount="+amount+",currency="+currency+",description="+description+",attachment="+attachment+",bankName="+bankName+",bankAccount="+bankAccount+",process="+process+",notes="+notes+",advanceCode="+advanceCode+",bukrs="+bukrs+",createdAt="+createdAt+",createdBy="+createdBy+",updatedAt="+updatedAt+",updatedBy="+updatedBy+",approved1By="+approved1By+",approved1At="+approved1At+",approved2By="+approved2By+",approved2At="+approved2At+",approved3By="+approved3By+",approved3At="+approved3At+",approved4By="+approved4By+",approved4At="+approved4At+",approvedAccountingBy="+approvedAccountingBy+",approvedAccountingAt="+approvedAccountingAt+",approvedTreasuryBy="+approvedTreasuryBy+",approvedTreasuryAt="+approvedTreasuryAt+",receivedAt="+receivedAt+",acctAp="+acctAp+",reason="+reason+",paymentStatus="+paymentStatus+",paymentDate="+paymentDate+",docno="+docno+",lastSync="+lastSync+",paymentStatus2="+paymentStatus2+",docno2="+docno2+",sapMessage="+sapMessage+')'; 
	}
 
}

	