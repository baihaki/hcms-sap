package com.abminvestama.hcms.core.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Immutable;


@Embeddable
@Immutable
public class CaReqDtlKey implements Serializable {
	private static final long serialVersionUID = 3432214567564043241L;
	
	@Column(name = "item_no")
	private Short itemNo;
	
	@Column(name = "req_no")
	private String reqNo;

	public Short getItemNo() {
		return itemNo;
	}

	public String getReqNo() {
		return reqNo;
	}

	public CaReqDtlKey() {}
	
	public CaReqDtlKey(String reqNo, Short itemNo) {
		this.reqNo = reqNo;
		this.itemNo = itemNo;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		
		if (o == null || o.getClass() != this.getClass()) {
			return false;
		}
		
		final CaReqDtlKey key = (CaReqDtlKey) o;
		
		if (key.getItemNo() != null ? key.getItemNo() != (itemNo != null ? itemNo: "") : itemNo != null) {
			return false;
		}
		
		if (key.getReqNo() != null ? key.getReqNo() != (reqNo != null ? reqNo: "") : reqNo != null) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int result = this.itemNo != null ? this.itemNo.hashCode() : 0;
		result = result * 31 + (this.reqNo != null ? this.reqNo.hashCode() : 0);
		return result;
	}
}
