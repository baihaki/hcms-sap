package com.abminvestama.hcms.core.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * Class that represents master data transaction for <strong>Absences</strong> (i.e. infotype IT2001 in SAP).
 * <br>Implement as master data transaction for <strong>Attendances</strong> too (i.e. infotype IT2002 in SAP) since version 1.0.1
 * 
 * @since 1.0.0
 * @version 1.0.6
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Add fields: approved4By, approved4At, approved5By, approved5At</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Rename fields: adminApprovedBy, adminApprovedAt to superior3ApprovedBy, superior3ApprovedAt</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Add fields: createdBy, createdAt, superiorApprovedBy, superiorApprovedAt, superior2ApprovedBy, superior2ApprovedAt, adminApprovedBy, adminApprovedAt</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Add fields: bukrs, process</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add field: address, phone, mobile, notes, reason</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add field: start, end</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 * 
 *
 */
@Entity
@Table(name = "it2001")
public class IT2001 extends SAPAbstractEntity<ITCompositeKeys> {

	private static final long serialVersionUID = 4361126096820085149L;

	@Column(name = "pernr", nullable = false, insertable = false, updatable = false)
	private Long pernr;
	
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name = "infty", referencedColumnName = "infty", insertable = false, updatable = false),
		@JoinColumn(name = "subty", referencedColumnName = "subty", insertable = false, updatable = false)
	})
	private T591S subty;
	
	public IT2001() {}
	
	public IT2001(ITCompositeKeys id) {
		this();
		this.id = id;
	}
	
	@Column(name = "seqnr")
	private Long seqnr;
	
	@ManyToOne
	@JoinColumn(name = "awart", referencedColumnName = "awart")
	private T554T awart;
	
	@Column(name = "abwtg", precision = 10, scale = 2)
	private Double abwtg;
	
	@Column(name = "stdaz", precision = 10, scale = 2)
	private Double stdaz;
	
	@Column(name = "kaltg", precision = 10, scale = 2)
	private Double kaltg;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "start_time")
	private Date start;
	
	@Temporal(TemporalType.TIME)
	@Column(name = "end_time")
	private Date end;
	
	@Column(name = "address", length = 255)
	private String address;
	
	@Column(name = "phone", length = 15)
	private String phone;
	
	@Column(name = "mobile", length = 15)
	private String mobile;
	
	@Column(name = "notes", length = 255)
	private String notes;
	
	@Column(name = "reason", length = 255)
	private String reason;
	
	@ManyToOne
	@JoinColumn(name = "bukrs", referencedColumnName = "bukrs", nullable = false)
	private T001 bukrs;

	@Column(name = "process")
	private String process;
	
	@ManyToOne
	@JoinColumn(name = "created_by", referencedColumnName = "id")
	private User createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	private Date createdAt;
	
	@ManyToOne
	@JoinColumn(name = "approved1_by", referencedColumnName = "id")
	private User superiorApprovedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved1_at")
	private Date superiorApprovedAt;
	
	@ManyToOne
	@JoinColumn(name = "approved2_by", referencedColumnName = "id")
	private User superior2ApprovedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved2_at")
	private Date superior2ApprovedAt;
	
	@ManyToOne
	@JoinColumn(name = "approved3_by", referencedColumnName = "id")
	private User superior3ApprovedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved3_at")
	private Date superior3ApprovedAt;
	
	@ManyToOne
	@JoinColumn(name = "approved4_by", referencedColumnName = "id")
	private User approved4By;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved4_at")
	private Date approved4At;
	
	@ManyToOne
	@JoinColumn(name = "approved5_by", referencedColumnName = "id")
	private User approved5By;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "approved5_at")
	private Date approved5At;
	
	/**
	 * GET Employee ID/SSN.
	 * 
	 * @return
	 */
	public Long getPernr() {
		return pernr;
	}
	
	/**
	 * GET Subtype.
	 * 
	 * @return
	 */
	public T591S getSubty() {
		return subty;
	}
	
	/**
	 * GET infotype Record no.
	 * 
	 * @return
	 */
	public Long getSeqnr() {
		return seqnr;
	}
	
	public void setSeqnr(Long seqnr) {
		this.seqnr = seqnr;
	}
	
	/**
	 * GET Absence Type.
	 * 
	 * @return
	 */
	public T554T getAwart() {
		return awart;
	}
	
	public void setAwart(T554T awart) {
		this.awart = awart;
	}
	
	/**
	 * GET Absence Days.
	 * 
	 * @return
	 */
	public Double getAbwtg() {
		return abwtg;
	}
	
	public void setAbwtg(Double abwtg) {
		this.abwtg = abwtg;
	}
	
	/**
	 * GET Absence Hours.
	 * 
	 * @return
	 */
	public Double getStdaz() {
		return stdaz;
	}
	
	public void setStdaz(Double stdaz) {
		this.stdaz = stdaz;
	}
	
	/**
	 * GET Calendar Days.
	 * 
	 * @return
	 */
	public Double getKaltg() {
		return kaltg;
	}
	
	public void setKaltg(Double kaltg) {
		this.kaltg = kaltg;
	}

	/**
	 * GET Start Time.
	 * 
	 * @return
	 */
	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	/**
	 * GET End Time.
	 * 
	 * @return
	 */
	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}



	/**
	 * GET Address on Leave.
	 * 
	 * @return
	 */
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}



	/**
	 * GET Phone Number.
	 * 
	 * @return
	 */
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}



	/**
	 * GET Mobile Phone Number.
	 * 
	 * @return
	 */
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}



	/**
	 * GET Notes.
	 * 
	 * @return
	 */
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}



	/**
	 * GET Reason for Revision.
	 * 
	 * @return
	 */
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public T001 getBukrs() {
		return bukrs;
	}

	public void setBukrs(T001 bukrs) {
		this.bukrs = bukrs;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public User getSuperiorApprovedBy() {
		return superiorApprovedBy;
	}

	public void setSuperiorApprovedBy(User superiorApprovedBy) {
		this.superiorApprovedBy = superiorApprovedBy;
	}

	public Date getSuperiorApprovedAt() {
		return superiorApprovedAt;
	}

	public void setSuperiorApprovedAt(Date superiorApprovedAt) {
		this.superiorApprovedAt = superiorApprovedAt;
	}

	public User getSuperior2ApprovedBy() {
		return superior2ApprovedBy;
	}

	public void setSuperior2ApprovedBy(User superior2ApprovedBy) {
		this.superior2ApprovedBy = superior2ApprovedBy;
	}

	public Date getSuperior2ApprovedAt() {
		return superior2ApprovedAt;
	}

	public void setSuperior2ApprovedAt(Date superior2ApprovedAt) {
		this.superior2ApprovedAt = superior2ApprovedAt;
	}

	public User getSuperior3ApprovedBy() {
		return superior3ApprovedBy;
	}

	public void setSuperior3ApprovedBy(User superior3ApprovedBy) {
		this.superior3ApprovedBy = superior3ApprovedBy;
	}

	public Date getSuperior3ApprovedAt() {
		return superior3ApprovedAt;
	}

	public void setSuperior3ApprovedAt(Date superior3ApprovedAt) {
		this.superior3ApprovedAt = superior3ApprovedAt;
	}

	public User getApproved4By() {
		return approved4By;
	}

	public void setApproved4By(User approved4By) {
		this.approved4By = approved4By;
	}

	public Date getApproved4At() {
		return approved4At;
	}

	public void setApproved4At(Date approved4At) {
		this.approved4At = approved4At;
	}

	public User getApproved5By() {
		return approved5By;
	}

	public void setApproved5By(User approved5By) {
		this.approved5By = approved5By;
	}

	public Date getApproved5At() {
		return approved5At;
	}

	public void setApproved5At(Date approved5At) {
		this.approved5At = approved5At;
	}
	
}