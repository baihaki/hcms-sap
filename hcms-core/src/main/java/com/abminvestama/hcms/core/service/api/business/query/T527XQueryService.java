package com.abminvestama.hcms.core.service.api.business.query;

import com.abminvestama.hcms.core.model.entity.T527X;
import com.abminvestama.hcms.core.model.entity.T527XKey;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0
 * @author baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Baihaki</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface T527XQueryService extends DatabaseQueryService<T527X, T527XKey>, DatabasePaginationQueryService {
}