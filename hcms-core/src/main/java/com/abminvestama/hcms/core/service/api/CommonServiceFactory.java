package com.abminvestama.hcms.core.service.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abminvestama.hcms.core.service.api.business.command.ApprovalEventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.CSKTCommandService;
import com.abminvestama.hcms.core.service.api.business.command.EventLogCommandService;
import com.abminvestama.hcms.core.service.api.business.command.IT0009CommandService;
import com.abminvestama.hcms.core.service.api.business.command.JobCommandService;
import com.abminvestama.hcms.core.service.api.business.command.PositionCommandService;
import com.abminvestama.hcms.core.service.api.business.command.T513SCommandService;
import com.abminvestama.hcms.core.service.api.business.command.T527XCommandService;
import com.abminvestama.hcms.core.service.api.business.command.T528TCommandService;
import com.abminvestama.hcms.core.service.api.business.command.UserCommandService;
import com.abminvestama.hcms.core.service.api.business.command.VehicleTypeCommandService;
import com.abminvestama.hcms.core.service.api.business.command.WorkflowCommandService;
import com.abminvestama.hcms.core.service.api.business.command.WorkflowStepCommandService;
import com.abminvestama.hcms.core.service.api.business.query.ApprovalCategoryQueryService;
import com.abminvestama.hcms.core.service.api.business.query.ApprovalEventLogQueryService;
import com.abminvestama.hcms.core.service.api.business.query.AttachmentQueryService;
import com.abminvestama.hcms.core.service.api.business.query.BNKAQueryService;
import com.abminvestama.hcms.core.service.api.business.query.CSKTQueryService;
import com.abminvestama.hcms.core.service.api.business.query.EventLogQueryService;
import com.abminvestama.hcms.core.service.api.business.query.HCMSEntityQueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.IT0009QueryService;
import com.abminvestama.hcms.core.service.api.business.query.JobQueryService;
import com.abminvestama.hcms.core.service.api.business.query.PositionQueryService;
import com.abminvestama.hcms.core.service.api.business.query.PositionRelationQueryService;
import com.abminvestama.hcms.core.service.api.business.query.PositionRelationTypeQueryService;
import com.abminvestama.hcms.core.service.api.business.query.RoleQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T001QueryService;
import com.abminvestama.hcms.core.service.api.business.query.T002TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T005TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T005UQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T042ZQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T502TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T512TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T513SQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T516TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T517TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T517XQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T518BQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T519TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T527XQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T528TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T531SQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T538TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T548TQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T556BQueryService;
import com.abminvestama.hcms.core.service.api.business.query.T591SQueryService;
import com.abminvestama.hcms.core.service.api.business.query.UserQueryService;
import com.abminvestama.hcms.core.service.api.business.query.VehicleTypeQueryService;
import com.abminvestama.hcms.core.service.api.business.query.WorkflowQueryService;
import com.abminvestama.hcms.core.service.api.business.query.WorkflowStepQueryService;
import com.abminvestama.hcms.core.service.util.MailService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.8
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.8</td><td>Baihaki</td><td>Register services: t512tQueryService</td></tr>
 *     <tr><td>1.0.7</td><td>Baihaki</td><td>Register services: t531sQueryService, mailService, t556bQueryService, </td></tr>
 *     <tr><td>1.0.6</td><td>Baihaki</td><td>Register services: csktQueryService, csktCommandService, jobQueryService, jobCommandService, userCommandService</td></tr>
 *     <tr><td>1.0.5</td><td>Baihaki</td><td>Register services: t528tQueryService, t528tCommandService, positionQueryService, positionCommandService</td></tr>
 *     <tr><td>1.0.4</td><td>Baihaki</td><td>Register services: t001QueryService, t513sQueryService, t513sCommandService, t527xQueryService, t527xCommandService</td></tr>
 *     <tr><td>1.0.3</td><td>Baihaki</td><td>Register services: t005uQueryService, t002tQueryService, t516tQueryService, t502tQueryService</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Register services: t517tQueryService, t518bQueryService, t519tQueryService, t538tQueryService, t517xQueryService</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Register services: t005tQueryService, attachmentQueryService</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("commonServiceFactory")
public class CommonServiceFactory {

	private ApprovalEventLogCommandService approvalEventLogCommandService;
	private ApprovalEventLogQueryService approvalEventLogQueryService;
	private EventLogCommandService eventLogCommandService;
	private EventLogQueryService eventLogQueryService;
	private IT0001QueryService it0001QueryService;
	private IT0009CommandService it0009CommandService;
	private IT0009QueryService it0009QueryService;
	private T591SQueryService t591sQueryService;
	private T042ZQueryService t042zQueryService;	
	private BNKAQueryService bnkaQueryService;	
	private RoleQueryService roleQueryService;
	private UserQueryService userQueryService;
	private ApprovalCategoryQueryService approvalCategoryQueryService;
	private HCMSEntityQueryService hcmsEntityQueryService;
	private PositionRelationQueryService positionRelationQueryService;
	private PositionRelationTypeQueryService positionRelationTypeQueryService;
	private WorkflowCommandService workflowCommandService;
	private WorkflowQueryService workflowQueryService;
	private WorkflowStepCommandService workflowStepCommandService;
	private WorkflowStepQueryService workflowStepQueryService;
	
	private T005TQueryService t005tQueryService;
	private AttachmentQueryService attachmentQueryService;
	private T517TQueryService t517tQueryService;
	private T518BQueryService t518bQueryService;
	private T519TQueryService t519tQueryService;
	private T538TQueryService t538tQueryService;
	private T517XQueryService t517xQueryService;
	
	private T005UQueryService t005uQueryService;
	private T002TQueryService t002tQueryService;
	private T516TQueryService t516tQueryService;
	private T502TQueryService t502tQueryService;
	
	private T001QueryService t001QueryService;
	private T513SQueryService t513sQueryService;
	private T513SCommandService t513sCommandService;
	private T527XQueryService t527xQueryService;
	private T527XCommandService t527xCommandService;
	private T528TQueryService t528tQueryService;
	private T528TCommandService t528tCommandService;
	private PositionQueryService positionQueryService;
	private PositionCommandService positionCommandService;
	private CSKTQueryService csktQueryService;
	private CSKTCommandService csktCommandService;
	private JobQueryService jobQueryService;
	private JobCommandService jobCommandService;
	private UserCommandService userCommandService;

	private T531SQueryService t531sQueryService;
	private MailService mailService;
	private T556BQueryService t556bQueryService;
	private T548TQueryService t548tQueryService;
	
	private T512TQueryService t512tQueryService;
	
	private VehicleTypeQueryService vehicleTypeQueryService;
	private VehicleTypeCommandService vehicleTypeCommandService;
	
	@Autowired
	void setApprovalEventLogCommandService(ApprovalEventLogCommandService approvalEventLogCommandService) {
		this.approvalEventLogCommandService = approvalEventLogCommandService;
	}
	
	@Autowired
	void setApprovalEventLogQueryService(ApprovalEventLogQueryService approvalEventLogQueryService) {
		this.approvalEventLogQueryService = approvalEventLogQueryService;
	}
	
	@Autowired
	void setEventLogCommandService(EventLogCommandService eventLogCommandService) {
		this.eventLogCommandService = eventLogCommandService;
	}
	
	@Autowired
	void setEventLogQueryService(EventLogQueryService eventLogQueryService) {
		this.eventLogQueryService = eventLogQueryService;
	}
	
	@Autowired
	void setIT0001QueryService(IT0001QueryService it0001QueryService) {
		this.it0001QueryService = it0001QueryService;
	}
	
	@Autowired
	void setIT0009CommandService(IT0009CommandService it0009CommandService) {
		this.it0009CommandService = it0009CommandService;
	}
	
	@Autowired
	void setIT0009QueryService(IT0009QueryService it0009QueryService) {
		this.it0009QueryService = it0009QueryService;
	}
	
	@Autowired
	void setT591SQueryService(T591SQueryService t591sQueryService) {
		this.t591sQueryService = t591sQueryService;
	}
	
	@Autowired
	void setT042ZQueryService(T042ZQueryService t042zQueryService) {
		this.t042zQueryService = t042zQueryService;
	}
	
	@Autowired
	void setBNKAQueryService(BNKAQueryService bnkaQueryService) {
		this.bnkaQueryService = bnkaQueryService;
	}
	
	@Autowired
	void setRoleQueryService(RoleQueryService roleQueryService) {
		this.roleQueryService = roleQueryService;
	}
	
	@Autowired
	void setUserQueryService(UserQueryService userQueryService) {
		this.userQueryService = userQueryService;
	}
	
	@Autowired
	void setApprovalCategoryQueryService(ApprovalCategoryQueryService approvalCategoryQueryService) {
		this.approvalCategoryQueryService = approvalCategoryQueryService;
	}
	
	@Autowired
	void setHCMSEntityQueryService(HCMSEntityQueryService hcmsEntityQueryService) {
		this.hcmsEntityQueryService = hcmsEntityQueryService;
	}
	
	@Autowired
	void setPositionRelationQueryService(PositionRelationQueryService positionRelationQueryService) {
		this.positionRelationQueryService = positionRelationQueryService;
	}
	
	@Autowired
	void setPositionRelationTypeQueryService(PositionRelationTypeQueryService positionRelationTypeQueryService) {
		this.positionRelationTypeQueryService = positionRelationTypeQueryService;
	}
	
	@Autowired
	void setWorkflowCommandService(WorkflowCommandService workflowCommandService) {
		this.workflowCommandService = workflowCommandService;
	}
	
	@Autowired
	void setWorkflowQueryService(WorkflowQueryService workflowQueryService) {
		this.workflowQueryService = workflowQueryService;
	}
	
	@Autowired
	void setWorkflowStepCommandService(WorkflowStepCommandService workflowStepCommandService) {
		this.workflowStepCommandService = workflowStepCommandService;
	}
	
	@Autowired
	void setWorkflowStepQueryService(WorkflowStepQueryService workflowStepQueryService) {
		this.workflowStepQueryService = workflowStepQueryService;
	}
	
	public ApprovalEventLogCommandService getApprovalEventLogCommandService() {
		return approvalEventLogCommandService;
	}
	
	public ApprovalEventLogQueryService getApprovalEventLogQueryService() {
		return approvalEventLogQueryService;
	}
	
	public EventLogCommandService getEventLogCommandService() {
		return eventLogCommandService;
	}
	
	public EventLogQueryService getEventLogQueryService() {
		return eventLogQueryService;
	}
	
	public IT0001QueryService getIT0001QueryService() {
		return it0001QueryService;
	}
	
	public IT0009CommandService getIT0009CommandService() {
		return it0009CommandService;
	}
	
	public IT0009QueryService getIT0009QueryService() {
		return it0009QueryService;
	}
	
	public T591SQueryService getT591SQueryService() {
		return t591sQueryService;
	}
	
	public T042ZQueryService getT042ZQueryService() {
		return t042zQueryService;
	}
	
	public BNKAQueryService getBNKAQueryService() {
		return bnkaQueryService;
	}
	
	public RoleQueryService getRoleQueryService() {
		return roleQueryService;
	}
	
	public UserQueryService getUserQueryService() {
		return userQueryService;
	}
	
	public ApprovalCategoryQueryService getApprovalCategoryQueryService() {
		return approvalCategoryQueryService;
	}
	
	public HCMSEntityQueryService getHCMSEntityQueryService() {
		return hcmsEntityQueryService;
	}
	
	public PositionRelationQueryService getPositionRelationQueryService() {
		return positionRelationQueryService;
	}
	
	public PositionRelationTypeQueryService getPositionRelationTypeQueryService() {
		return positionRelationTypeQueryService;
	}
	
	public WorkflowCommandService getWorkflowCommandService() {
		return workflowCommandService;
	}
	
	public WorkflowQueryService getWorkflowQueryService() {
		return workflowQueryService;
	}
	
	public WorkflowStepCommandService getWorkflowStepCommandService() {
		return workflowStepCommandService;
	}
	
	public WorkflowStepQueryService getWorkflowStepQueryService() {
		return workflowStepQueryService;
	}
	
	public T005TQueryService getT005TQueryService() {
		return t005tQueryService;
	}
	
	@Autowired
	void setT005TQueryService(T005TQueryService t005tQueryService) {
		this.t005tQueryService = t005tQueryService;
	}

	public AttachmentQueryService getAttachmentQueryService() {
		return attachmentQueryService;
	}

	@Autowired
	public void setAttachmentQueryService(AttachmentQueryService attachmentQueryService) {
		this.attachmentQueryService = attachmentQueryService;
	}
	

	@Autowired
	void setT517TQueryService(T517TQueryService t517tQueryService) {
		this.t517tQueryService = t517tQueryService;
	}
	
	@Autowired
	void setT518BQueryService(T518BQueryService t518bQueryService) {
		this.t518bQueryService = t518bQueryService;
	}
	
	@Autowired
	void setT519TQueryService(T519TQueryService t519tQueryService) {
		this.t519tQueryService = t519tQueryService;
	}
	
	@Autowired
	void setT538TQueryService(T538TQueryService t538tQueryService) {
		this.t538tQueryService = t538tQueryService;
	}
	
	@Autowired
	void setT517XQueryService(T517XQueryService t517xQueryService) {
		this.t517xQueryService = t517xQueryService;
	}
	
	@Autowired
	void setT005UQueryService(T005UQueryService t005uQueryService) {
		this.t005uQueryService = t005uQueryService;
	}
	
	@Autowired
	void setT002TQueryService(T002TQueryService t002tQueryService) {
		this.t002tQueryService = t002tQueryService;
	}
	
	@Autowired
	void setT516TQueryService(T516TQueryService t516tQueryService) {
		this.t516tQueryService = t516tQueryService;
	}
	
	@Autowired
	void setT502TQueryService(T502TQueryService t502tQueryService) {
		this.t502tQueryService = t502tQueryService;
	}
	
	@Autowired
	void setT001QueryService(T001QueryService t001QueryService) {
		this.t001QueryService = t001QueryService;
	}
	
	@Autowired
	void setT513SQueryService(T513SQueryService t513sQueryService) {
		this.t513sQueryService = t513sQueryService;
	}
	
	@Autowired
	void setT513SCommandService(T513SCommandService t513sCommandService) {
		this.t513sCommandService = t513sCommandService;
	}

	@Autowired
	public void setT527XQueryService(T527XQueryService t527xQueryService) {
		this.t527xQueryService = t527xQueryService;
	}

	@Autowired
	public void setT527XCommandService(T527XCommandService t527xCommandService) {
		this.t527xCommandService = t527xCommandService;
	}

	@Autowired
	public void setT528TQueryService(T528TQueryService t528tQueryService) {
		this.t528tQueryService = t528tQueryService;
	}

	@Autowired
	public void setT528TCommandService(T528TCommandService t528tCommandService) {
		this.t528tCommandService = t528tCommandService;
	}

	@Autowired
	public void setPositionQueryService(PositionQueryService positionQueryService) {
		this.positionQueryService = positionQueryService;
	}

	@Autowired
	public void setPositionCommandService(PositionCommandService positionCommandService) {
		this.positionCommandService = positionCommandService;
	}

	@Autowired
	public void setCSKTQueryService(CSKTQueryService csktQueryService) {
		this.csktQueryService = csktQueryService;
	}

	@Autowired
	public void setCSKTCommandService(CSKTCommandService csktCommandService) {
		this.csktCommandService = csktCommandService;
	}

	@Autowired
	public void setJobQueryService(JobQueryService jobQueryService) {
		this.jobQueryService = jobQueryService;
	}
	
	@Autowired
	public void setJobCommandService(JobCommandService jobCommandService) {
		this.jobCommandService = jobCommandService;
	}
	
	@Autowired
	void setVehicleTypeCommandService(VehicleTypeCommandService vehicleTypeCommandService) {
		this.vehicleTypeCommandService = vehicleTypeCommandService;
	}

	@Autowired
	void setVehicleTypeQueryService(VehicleTypeQueryService vehicleTypeQueryService) {
		this.vehicleTypeQueryService = vehicleTypeQueryService;
	}

	@Autowired
	public void setUserCommandService(UserCommandService userCommandService) {
		this.userCommandService = userCommandService;
	}

	@Autowired
	public void setT531SQueryService(T531SQueryService t531sQueryService) {
		this.t531sQueryService = t531sQueryService;
	}

	@Autowired
	void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

	@Autowired
	public void setT556BQueryService(T556BQueryService t556bQueryService) {
		this.t556bQueryService = t556bQueryService;
	}

	@Autowired
	public void setT548TQueryService(T548TQueryService t548tQueryService) {
		this.t548tQueryService = t548tQueryService;
	}

	@Autowired
	public void setT512TQueryService(T512TQueryService t512tQueryService) {
		this.t512tQueryService = t512tQueryService;
	}

	public T517TQueryService getT517TQueryService() {
		return t517tQueryService;
	}

	public T518BQueryService getT518BQueryService() {
		return t518bQueryService;
	}

	public T519TQueryService getT519TQueryService() {
		return t519tQueryService;
	}

	public T538TQueryService getT538TQueryService() {
		return t538tQueryService;
	}

	public T517XQueryService getT517XQueryService() {
		return t517xQueryService;
	}

	public T005UQueryService getT005UQueryService() {
		return t005uQueryService;
	}

	public T002TQueryService getT002TQueryService() {
		return t002tQueryService;
	}

	public T516TQueryService getT516TQueryService() {
		return t516tQueryService;
	}

	public T502TQueryService getT502TQueryService() {
		return t502tQueryService;
	}
	
	public T001QueryService getT001QueryService() {
		return t001QueryService;
	}

	public T513SQueryService getT513SQueryService() {
		return t513sQueryService;
	}

	public T513SCommandService getT513SCommandService() {
		return t513sCommandService;
	}

	public T527XQueryService getT527XQueryService() {
		return t527xQueryService;
	}

	public T527XCommandService getT527XCommandService() {
		return t527xCommandService;
	}

	public T528TQueryService getT528TQueryService() {
		return t528tQueryService;
	}

	public T528TCommandService getT528TCommandService() {
		return t528tCommandService;
	}

	public PositionQueryService getPositionQueryService() {
		return positionQueryService;
	}

	public PositionCommandService getPositionCommandService() {
		return positionCommandService;
	}

	public CSKTQueryService getCSKTQueryService() {
		return csktQueryService;
	}

	public CSKTCommandService getCSKTCommandService() {
		return csktCommandService;
	}

	public JobQueryService getJobQueryService() {
		return jobQueryService;
	}

	public JobCommandService getJobCommandService() {
		return jobCommandService;
	}

	public UserCommandService getUserCommandService() {
		return userCommandService;
	}

	public T531SQueryService getT531SQueryService() {
		return t531sQueryService;
	}

	public MailService getMailService() {
		return mailService;
	}

	public T556BQueryService getT556BQueryService() {
		return t556bQueryService;
	}

	public T548TQueryService getT548TQueryService() {
		return t548tQueryService;
	}

	public T512TQueryService getT512TQueryService() {
		return t512tQueryService;
	}

	
	public VehicleTypeQueryService getVehicleTypeQueryService() {
		return vehicleTypeQueryService;
	}
	
	public VehicleTypeCommandService getVehicleTypeCommandService() {
		return vehicleTypeCommandService;
	}
}