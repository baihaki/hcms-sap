package com.abminvestama.hcms.core.service.impl.business.command;

import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.exception.CannotPersistException;
import com.abminvestama.hcms.core.model.entity.IT0040;
import com.abminvestama.hcms.core.model.entity.User;
import com.abminvestama.hcms.core.repository.IT0040Repository;
import com.abminvestama.hcms.core.service.api.business.command.IT0040CommandService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("it0040CommandService")
@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
public class IT0040CommandServiceImpl implements IT0040CommandService {

	private IT0040Repository it0040Repository;
	
	@Autowired
	IT0040CommandServiceImpl(IT0040Repository it0040Repository) {
		this.it0040Repository = it0040Repository;
	}
	
	@Override
	public Optional<IT0040> save(IT0040 entity, User user)
			throws CannotPersistException, NoSuchMethodException, ConstraintViolationException {
		if (user != null) {
			entity.setUname(user.getId());
		}
		
		try {
			return Optional.ofNullable(it0040Repository.save(entity));
		} catch (Exception e) {
			throw new CannotPersistException("Cannot persist IT0040 (Employee Inventory).Reason=" + e.getMessage());
		}						
	}

	@Override
	public boolean delete(IT0040 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// no delete on this version.
		throw new NoSuchMethodException("There's no deletion on this version. Please consult to your Consultant.");		
	}

	@Override
	public boolean restore(IT0040 entity, User user) throws NoSuchMethodException, ConstraintViolationException {
		// since we don't have delete operation, then we don't need the 'restore' as well
		throw new NoSuchMethodException("Invalid Operation. Please consult to your Consultant.");
	}
}
