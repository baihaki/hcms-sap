package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.entity.CaOrder;
import com.abminvestama.hcms.core.repository.CaOrderRepository;
import com.abminvestama.hcms.core.service.api.business.query.CaOrderQueryService;

/**
 * 
 * @since 1.0.0
 * @version 1.0.0 (Cash Advance)
 * @author wijanarko (wijanarko777@gmx.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.0</td><td>Wijanarko</td><td>Development release</td></tr>
 * </table>
 *
 */
@Service("caOrderQueryService")
@Transactional(readOnly = true)
public class CaOrderQueryServiceImpl implements CaOrderQueryService {

	private CaOrderRepository caOrderRepository;

	@Autowired
	CaOrderQueryServiceImpl(CaOrderRepository caOrderRepository) {
		this.caOrderRepository = caOrderRepository;
	}

	@Override
	public Optional<CaOrder> findById(Optional<Long> id) throws Exception {
		return id.map(pk -> caOrderRepository.findOne(pk));
	}

	@Override
	public Optional<Collection<CaOrder>> fetchAll() {
		List<CaOrder> listOfCaOrder = new ArrayList<>();
		Optional<Iterable<CaOrder>> bunchOfCaOrder = Optional.ofNullable(caOrderRepository.findAll());
		return bunchOfCaOrder.map(iter -> {
			iter.forEach(caOrder -> {
				listOfCaOrder.add(caOrder);
			});
			return listOfCaOrder;
		});
	}

	@Override
	public Optional<CaOrder> findByOrderId(Long orderId) {
		return caOrderRepository.findByOrderId(orderId);
	}

	@Override
	public Page<CaOrder> findByOrgeh(Long orgeh, int pageNumber) {
		int total = 0;
		List<CaOrder> listOfCaOrder = null;
		Pageable pageRequest = createPageRequest(pageNumber);
		
		total = caOrderRepository.findByOrgehCount(orgeh);
		listOfCaOrder = caOrderRepository.findByOrgehWithPaging(orgeh, pageRequest.getPageSize(), pageRequest.getOffset())
				.stream().collect(Collectors.toList());
		Page<CaOrder> page = new PageImpl<CaOrder>(listOfCaOrder, pageRequest, total);
		
		return page;
	}

	@Override
	public Page<CaOrder> findByCriteria(Long orgeh, Long glAccountId, int pageNumber) {
		int total = 0;
		List<CaOrder> listOfCaOrder = null;
		Pageable pageRequest = createPageRequest(pageNumber);
		
		total = caOrderRepository.findByCriteriaCount(orgeh, glAccountId);
		listOfCaOrder = caOrderRepository.findByCriteriaWithPaging(orgeh, glAccountId, pageRequest.getPageSize(), pageRequest.getOffset())
				.stream().collect(Collectors.toList());
		Page<CaOrder> page = new PageImpl<CaOrder>(listOfCaOrder, pageRequest, total);
		
		return page;
	}

	@Override
	public Page<CaOrder> fetchAllWithPaging(int pageNumber) {
		Page<CaOrder> page = null;
		if (pageNumber == -1) {
			List<CaOrder> listOfCaOrder = fetchAll().get().stream().collect(Collectors.toList());
			int total = listOfCaOrder.size();
			page = new PageImpl<CaOrder>(listOfCaOrder, new PageRequest(1, 1), total);
		}
		else {
			page = caOrderRepository.findAll(createPageRequest(pageNumber));
		}
		
		return page;
	}
}
