package com.abminvestama.hcms.core.repository;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TemporalType;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.abminvestama.hcms.core.model.constant.DocumentStatus;
import com.abminvestama.hcms.core.model.entity.IT0009;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;

/**
 * 
 * @since 1.0.0
 * @version 1.0.2
 * @author yauri (yauritux@gmail.com)<br>baihaki (baihaki.pru@gmail.com)
 * <br><br>
 * <table>
 *     <tr><td><b>Version</td><td><b>Author</td><td><b>Description</td></tr>
 *     <tr><td>1.0.2</td><td>Baihaki</td><td>Add findDistinctLatestByPernrAndExcludeStatus method</td></tr>
 *     <tr><td>1.0.1</td><td>Baihaki</td><td>Add findDistinctLatestByBukrsAndExcludeStatus method</td></tr>
 *     <tr><td>1.0.0</td><td>Yauri</td><td>Development release</td></tr>
 * </table>
 *
 */
public interface IT0009Repository extends PagingAndSortingRepository<IT0009, ITCompositeKeys> {
		
	String FIND_ONE_BY_COMPOSITE_KEY = "FROM IT0009 it0009 WHERE it0009.pernr = :pernr AND it0009.id.infty = :infty AND " + 
			"it0009.id.subty = :subty AND it0009.id.endda = :endda AND it0009.id.begda = :begda";
	
	String FIND_ONE_BY_COMPOSITE_KEY_FOR_PUBLISHED_OR_UPDATED = "FROM IT0009 it0009 WHERE it0009.pernr = :pernr AND " + 
			"it0009.id.infty = :infty AND it0009.id.subty = :subty AND it0009.id.endda = :endda AND it0009.id.begda = :begda AND " +
			"(it0009.status = 'PUBLISHED' OR it0009.status = 'UPDATE_IN_PROGRESS' OR it0009.status = 'UPDATE_REJECTED')";
	
	String FIND_BY_PERNR_FOR_PUBLISHED_OR_UPDATED = "FROM IT0009 it0009 WHERE it0009.pernr = :pernr AND it0009.id.infty = '0009' " + 
			"AND (it0009.status = 'PUBLISHED' OR it0009.status = 'UPDATE_IN_PROGRESS' OR it0009.status = 'UPDATE_REJECTED') " + 
			"ORDER BY it0009.id.subty ASC, it0009.id.endda DESC";
	
	String FIND_BY_PERNR_AND_SUBTY_FOR_PUBLISHED_OR_UPDATED = "FROM IT0009 it0009 WHERE it0009.pernr = :pernr AND " + 
			"it0009.id.infty = :infty AND it0009.id.subty = :subty " +
			"AND (it0009.status = 'PUBLISHED' OR it0009.status = 'UPDATE_IN_PROGRESS' OR it0009.status = 'UPDATE_REJECTED') " +
			"ORDER BY it0009.id.endda DESC";

	@Query(FIND_ONE_BY_COMPOSITE_KEY)
	IT0009 findOneByCompositeKey(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty,
			@Param("endda") @Temporal(TemporalType.DATE) Date endda,
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query(FIND_ONE_BY_COMPOSITE_KEY_FOR_PUBLISHED_OR_UPDATED)
	IT0009 findOneByCompositeKeyForPublishedOrUpdated(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty,
			@Param("endda") @Temporal(TemporalType.DATE) Date endda, 
			@Param("begda") @Temporal(TemporalType.DATE) Date begda);
	
	@Query(FIND_BY_PERNR_FOR_PUBLISHED_OR_UPDATED)
	Page<IT0009> findByPernrForPublishedOrUpdated(@Param("pernr") Long pernr, Pageable pageRequest);
		
	@Query(FIND_BY_PERNR_AND_SUBTY_FOR_PUBLISHED_OR_UPDATED)
	Collection<IT0009> findByPernrAndSubtyForPublishedOrUpdated(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty);
	
	@Query("FROM IT0009 it0009 WHERE it0009.pernr = :pernr AND it0009.id.infty = :infty ORDER BY it0009.id.subty ASC, it0009.id.endda DESC, it0009.id.begda DESC")
	Collection<IT0009> findByPernr(@Param("pernr") Long pernr, @Param("infty") String infty);
	
	@Query("FROM IT0009 it0009 WHERE it0009.pernr = :pernr AND it0009.id.infty = :infty AND it0009.id.subty = :subty ORDER BY it0009.id.endda DESC, it0009.id.begda DESC")
	Collection<IT0009> findByPernrAndSubty(@Param("pernr") Long pernr, @Param("infty") String infty, @Param("subty") String subty);
	
	@Query("FROM IT0009 it0009 WHERE it0009.status = :status ORDER BY it0009.id.endda DESC, it0009.id.begda DESC")
	Page<IT0009> findByStatus(@Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query("FROM IT0009 it0009 WHERE it0009.pernr = :pernr AND it0009.status = :status ORDER BY it0009.id.endda DESC, it0009.id.begda DESC")
	Page<IT0009> findByPernrAndStatus(@Param("pernr") Long pernr, @Param("status") DocumentStatus status, Pageable pageRequest);
	
	@Query(nativeQuery=true, value="SELECT COUNT(*) FROM ( " +
			"SELECT DISTINCT ON (A.pernr, A.subty, B.pernr) A.*, B.bukrs, B.endda, B.begda " +
			"FROM it0009 A INNER JOIN it0001 B ON A.pernr = B.pernr " +
			"WHERE B.bukrs = :bukrs and B.endda = A.endda and document_status not in :excludeStatus " +
			"ORDER BY A.pernr, A.subty, B.pernr, A.endda desc, A.begda desc, B.endda desc, B.begda desc " +
			") AS rows")
	int findDistinctLatestByBukrsAndExcludeStatusCount(@Param("bukrs") String bukrs, @Param("excludeStatus") String[] excludeStatus);
	
	@Query(nativeQuery=true, value="SELECT DISTINCT ON (A.pernr, A.subty, B.pernr) A.*, B.bukrs, B.endda, B.begda " +
			"FROM it0009 A INNER JOIN it0001 B ON A.pernr = B.pernr " +
			"WHERE B.bukrs = :bukrs and B.endda = A.endda and document_status not in :excludeStatus " +
			"ORDER BY A.pernr, A.subty, B.pernr, A.endda desc, A.begda desc, B.endda desc, B.begda desc " +
			"LIMIT :limit OFFSET :offset")
	Collection<IT0009> findDistinctLatestByBukrsAndExcludeStatus(@Param("bukrs") String bukrs, @Param("excludeStatus") String[] excludeStatus,
			@Param("limit") int limit, @Param("offset") int offset);
	
	@Query(nativeQuery=true, value="SELECT COUNT(*) FROM ( " +
	        "SELECT DISTINCT ON (pernr, subty) * FROM it0009 WHERE pernr = :pernr and document_status not in :excludeStatus " +
			"ORDER BY pernr, subty, endda desc, begda desc ) AS rows")
	int findDistinctLatestByPernrAndExcludeStatusCount(@Param("pernr") Long pernr, @Param("excludeStatus") String[] excludeStatus);
	
	@Query(nativeQuery=true, value="SELECT DISTINCT ON (pernr, subty) * " +
            "FROM it0009 WHERE pernr = :pernr and document_status not in :excludeStatus " +
			"ORDER BY pernr, subty, endda desc, begda desc " +
			"LIMIT :limit OFFSET :offset")
	Collection<IT0009> findDistinctLatestByPernrAndExcludeStatus(@Param("pernr") Long pernr, @Param("excludeStatus") String[] excludeStatus,
			@Param("limit") int limit, @Param("offset") int offset);
}