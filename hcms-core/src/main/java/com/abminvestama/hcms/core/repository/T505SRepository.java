package com.abminvestama.hcms.core.repository;

import org.springframework.data.repository.CrudRepository;

import com.abminvestama.hcms.core.model.entity.T505S;
import com.abminvestama.hcms.core.model.entity.T505SKey;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public interface T505SRepository extends CrudRepository<T505S, T505SKey> {
}