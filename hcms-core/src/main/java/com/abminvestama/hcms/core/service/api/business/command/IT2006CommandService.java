package com.abminvestama.hcms.core.service.api.business.command;

import com.abminvestama.hcms.core.model.entity.IT2006;
import com.abminvestama.hcms.core.service.api.DatabaseCommandService;

public interface IT2006CommandService extends DatabaseCommandService<IT2006> {
}