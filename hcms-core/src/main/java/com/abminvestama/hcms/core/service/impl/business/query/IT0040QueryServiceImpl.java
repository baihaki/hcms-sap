package com.abminvestama.hcms.core.service.impl.business.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abminvestama.hcms.core.model.constant.SAPInfoType;
import com.abminvestama.hcms.core.model.entity.IT0040;
import com.abminvestama.hcms.core.model.entity.ITCompositeKeys;
import com.abminvestama.hcms.core.repository.IT0040Repository;
import com.abminvestama.hcms.core.service.api.business.query.IT0040QueryService;

/**
 * 
 * @author yauri (yauritux@gmail.com)
 * @version 1.0.0
 * @since 1.0.0
 *
 */
@Service("it0040QueryService")
@Transactional(readOnly = true)
public class IT0040QueryServiceImpl implements IT0040QueryService {

	private IT0040Repository it0040Repository;
	
	@Autowired
	IT0040QueryServiceImpl(IT0040Repository it0040Repository) {
		this.it0040Repository = it0040Repository;
	}
	
	@Override
	public Optional<IT0040> findById(Optional<ITCompositeKeys> id) throws Exception {
		throw new NoSuchMethodException("Method not implemented. Please use method findOneByCompositeKeys instead.");
	}

	@Override
	public Optional<Collection<IT0040>> fetchAll() {
		Optional<Iterable<IT0040>> it0040Iterates = Optional.ofNullable(it0040Repository.findAll());
		if (!it0040Iterates.isPresent()) {
			return Optional.empty();
		}
		
		List<IT0040> listOfIT0040 = new ArrayList<>();
		it0040Iterates.map(it0040Iterable -> {			
			it0040Iterable.forEach(it0040 -> {
				listOfIT0040.add(it0040);
			});
			return listOfIT0040;
		});
		
		return Optional.of(listOfIT0040);
	}

	@Override
	public Optional<IT0040> findOneByCompositeKey(Long pernr, String subty, Date endda, Date begda) {
		if (pernr == null || StringUtils.isBlank(subty) || endda == null || begda == null) {
			return Optional.empty();
		}
		
		return Optional.ofNullable(it0040Repository.findOneByCompositeKey(pernr, SAPInfoType.EMPLOYEE_INVENTORY.infoType(), subty, endda, begda));
	}

	@Override
	public Collection<IT0040> findByPernr(Long pernr) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		Optional<Collection<IT0040>> bunchOfIT0040 
			= Optional.ofNullable(it0040Repository.findByPernr(pernr, SAPInfoType.EMPLOYEE_INVENTORY.infoType()));
		
		return (bunchOfIT0040.isPresent()
				? bunchOfIT0040.get().stream().collect(Collectors.toList())
				: Collections.emptyList());		
	}

	@Override
	public Collection<IT0040> findByPernrAndSubty(Long pernr, String subty) {
		if (pernr == null) {
			return Collections.emptyList();
		}
		
		if (StringUtils.isBlank(subty)) {
			subty = "01"; // default to subtype '01' (Key|s)
		}
		
		Optional<Collection<IT0040>> bunchOfIT0040 
			= Optional.ofNullable(it0040Repository.findByPernrAndSubty(pernr, SAPInfoType.EMPLOYEE_INVENTORY.infoType(), subty));

		return (bunchOfIT0040.isPresent() 
					? bunchOfIT0040.get().stream().collect(Collectors.toList())
					: Collections.emptyList());		
	}
}