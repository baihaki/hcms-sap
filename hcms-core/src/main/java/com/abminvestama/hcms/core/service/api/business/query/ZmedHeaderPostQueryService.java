package com.abminvestama.hcms.core.service.api.business.query;

import java.util.Date;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.abminvestama.hcms.core.model.entity.ZmedCompositeKeyHeaderPost;
import com.abminvestama.hcms.core.model.entity.ZmedEmpquota;
import com.abminvestama.hcms.core.model.entity.ZmedHeaderPost;
import com.abminvestama.hcms.core.service.api.DatabasePaginationQueryService;
import com.abminvestama.hcms.core.service.api.DatabaseQueryService;


public interface ZmedHeaderPostQueryService extends DatabaseQueryService<ZmedHeaderPost, ZmedCompositeKeyHeaderPost>, DatabasePaginationQueryService{
	//Optional<ZmedHeaderPost> findByEntityName(final String entityName);
	

	@NotNull
	Optional<ZmedHeaderPost> findOneByCompositeKey(String objType, String objKey, String objSys);
	
}

